package com.ifeng.newvideo;

import android.content.Context;
import android.content.Intent;
import com.ifeng.newvideo.ui.ActivityMainTab;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by gongling on 2016/8/31.
 */
public class IfengCrashHandler implements Thread.UncaughtExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(IfengCrashHandler.class);

    private static IfengCrashHandler INSTANCE = new IfengCrashHandler();
    private Context mContext;
    private Thread.UncaughtExceptionHandler mDefaultHandler;

    private IfengCrashHandler() {
    }

    public static IfengCrashHandler getInstance() {
        return INSTANCE;
    }

    public void init(Context context) {
        mContext = context;
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable exception) {

        printException(thread, exception);

        if (mDefaultHandler == null || exception == null) {
            return;
        }
        // mDefaultHandler.uncaughtException(thread,exception);
        restartApp();//发生崩溃异常时,重启应用
    }


    public void printException(Thread thread, Throwable exception) {

        // StringBuilder sb = new StringBuilder();
        // String temp = exception.getMessage();
        // if (temp != null) {
        //     sb.append(temp);
        // }
        // sb.append("\r\n");
        // sb.append(thread.getName());
        // sb.append(" Trace: \r\n");
        // StackTraceElement[] elements = exception.getStackTrace();
        // if (elements != null) {
        //     for (StackTraceElement element : elements) {
        //         temp = element.toString();
        //         if (temp != null) {
        //             sb.append(temp);
        //         }
        //         sb.append("\r\n");
        //     }
        // }
        logger.error(" fatal !! thread name: {} ,thread id:{}, \r\n uncaughtException:{}" , thread.getName(), thread.getId(), exception);
    }


    public void restartApp() {

        ((IfengApplication) mContext).killAllActivity();

        Intent i = new Intent(mContext.getApplicationContext(), ActivityMainTab.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);

      /*  Intent intent = new Intent(mContext.getApplicationContext(), ActivityMainTab.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent restartIntent = PendingIntent.getActivity(
                mContext.getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        //退出程序
        AlarmManager mgr = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, restartIntent); // 1秒钟后重启应用*/

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }


}
