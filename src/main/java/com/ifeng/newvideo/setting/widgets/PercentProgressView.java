package com.ifeng.newvideo.setting.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * 自定义的文件占用大小显示条
 */
public class PercentProgressView extends View {

    private static final Logger logger = LoggerFactory.getLogger(PercentProgressView.class);

    private static final int HANDLER_START_DRAW = 0;
    private ArrayList<Category> mCategories;
    private Paint mPaint;
    private float[] destPercent;
    private float[] increase;
    private boolean isDrawing;

    public PercentProgressView(Context context) {
        super(context);
        init();
    }

    public PercentProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PercentProgressView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mCategories = new ArrayList<Category>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float left = 0;
        float top = 0;
        float width = getWidth();
        float bottom = getHeight();
        if (!isDrawing) return;
        // Draw every percent part
        for (int i = mCategories.size() - 1; i >= 0; i--) {
            int color = mCategories.get(i).resColorId;
            mPaint.setColor(getContext().getResources().getColor(color));
            canvas.drawRect(left, top, width * increase[i], bottom, mPaint);
        }
        if (increase[increase.length - 1] == 1)
            isDrawing = false;
    }

    public static class Category {
        final int resColorId;
        final float percentage;

        public Category(int resColorId, float percentage) {
            this.resColorId = resColorId;
            this.percentage = percentage;
            logger.debug("construct Category resColorId={},percentage={}", resColorId, percentage);
        }
    }

    public void setPercent(ArrayList<Category> categories) {
        if (null == categories || categories.size() == 0) {
            logger.error("data is null !");
            return;
        }
        if (isDrawing) {
            logger.error("No is Drawing!!!!!!wait for 1 seconds");
            return;
        }
        float percentages = 0;
        for (Category category : categories) {
            percentages += category.percentage;
        }
        if (percentages > 1) {
            throw new RuntimeException("你设置的百分比综合超过了1");
        }
        mCategories.clear();
        this.mCategories.addAll(categories);
        accelerated = 0.01f;
        destPercent = new float[mCategories.size()];
        increase = new float[mCategories.size()];
        float sum = 0;
        for (int i = 0; i < mCategories.size(); i++) {
            sum = destPercent[i] = 1 * mCategories.get(i).percentage + sum;
            logger.debug("destPercent[{}]={}", i, destPercent[i]);
        }
        handler.sendEmptyMessage(HANDLER_START_DRAW);
    }

    private float accelerated = 0.01f;
    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case HANDLER_START_DRAW:
                    isDrawing = true;
                    for (int i = 0; i < destPercent.length; i++) {
                        increase[i] += destPercent[i] * accelerated;
                    }
                    accelerated = 0.001f + accelerated;
                    if (increase[increase.length - 1] >= destPercent[destPercent.length - 1]) {
                        System.arraycopy(destPercent, 0, increase, 0, increase.length);
                        invalidate();
                    } else {
                        invalidate();
                        sendEmptyMessageDelayed(HANDLER_START_DRAW, 10);
                    }
                    break;
                default:
                    break;
            }
        }

    };

    protected void onDetachedFromWindow() {
        handler.removeMessages(HANDLER_START_DRAW);
    }

}
