package com.ifeng.newvideo.setting.value;


/**
 * 设置页面的配置信息
 */
public class SettingConfig {

    /**
     * “允许2G/3G/4G网络播放/缓存” 开关
     */
    public static final String WIFI_CONDITION = "wifi_onoff";
    /**
     * “允运营商网络缓存” 开关
     */
    public static final String DOWNLOAD_VIDEO_ONLY_WIFI = "download_video_only_wifi";
    /**
     * "定时关闭音频"
     */
    public static final String PAUSE_AUDIO = "pause_audio";
    /**
     * 推送开关
     */
    public static final String PUSH_ON = "push_on";
    /**
     * DLNA开关
     */
    public static final String DLNA_ON = "dlna_on";
    /**
     * 画中画开关
     */
    public static final String PIP_ON = "pip_on";

    /**
     * 缓存清晰度
     */
    public static final String CACHE_CLARITY = "cache_clarity";

}
