package com.ifeng.newvideo.setting.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ActivityNewVerGuide;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.utils.PackageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 关于页面
 * Created by hucw on 2014/8/11.
 */
public class AboutActivity extends BaseFragmentActivity {

    private static final Logger logger = LoggerFactory.getLogger(AboutActivity.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_about);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        String version;
        try {
            version = PackageUtils.getAppVersion(this);
        } catch (PackageManager.NameNotFoundException e) {
            logger.error(e.toString(), e);
            version = "0.0";
        }
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.setting_about));

        ((TextView) findViewById(R.id.ifeng_version_tv)).setText("V" + version);
        ImageView aboutBack = (ImageView) findViewById(R.id.back);
        aboutBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_SETUP_ABOUT);
                finish();
            }
        });

        TextView welcomePageBtn = (TextView) findViewById(R.id.welcome_page_view);
        welcomePageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_ABOUT_WELCOME, PageIdConstants.MY_SETUP_ABOUT);
                IntentUtils.launchActByAction(AboutActivity.this, IntentUtils.ACTION_SPLASHGUIDESELF, ActivityNewVerGuide.class);
            }
        });

    }

}
