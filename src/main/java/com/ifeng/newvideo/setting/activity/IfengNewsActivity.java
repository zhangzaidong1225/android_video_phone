package com.ifeng.newvideo.setting.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 凤凰快讯页面
 */
public class IfengNewsActivity extends BaseFragmentActivity {

    private static final Logger logger = LoggerFactory.getLogger(IfengNewsActivity.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ifeng_news);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.IFENG_NEWS);
                finish();
            }
        });

        ((TextView) findViewById(R.id.title)).setText(getString(R.string.ifeng_news));
        String id = getIntent().getStringExtra(IntentKey.IFENG_NEWS_ID);
        String content = getIntent().getStringExtra(IntentKey.IFENG_NEWS_CONTENT);
        ((TextView) findViewById(R.id.ifeng_news_content)).setText(content != null ? content : "");

    }


}
