package com.ifeng.newvideo.setting.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.PopupWindow;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.cache.NetSettingChangedListener;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.dialogUI.DialogUtilsFor3G;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.ActivityUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.NotificationCheckUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.PushUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.net.VolleyQueue;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.FileUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.StorageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.upgrade.IntentConfig;
import com.ifeng.video.upgrade.model.UpgradeInfo;
import com.ifeng.video.upgrade.ui.UpgradePopupWindow;
import imageload.ImageLoaderManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Locale;


/**
 * 设置页面
 */
public class SettingActivity extends BaseFragmentActivity implements OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(SettingActivity.class);

    private View loginQut;
    private View loginQutLayout;
    private View clearCacheView;
    private View viewDivider;//关于和退出登录之间的分割线

    private ImageSwitcher wifiSwitch;
    private ImageSwitcher pushSwitch;
    private ImageSwitcher pipSwitch;

    private TextView defaultPlayType;
    private TextView clearCacheText;
    private TextView mVersionCode;
    private TextView versionInfo;
    private String curVersionCode;

    private User user;
    private Thread measureCacheSizeThread;

    private boolean isLogin = false;

    private final int MSG_CACHE_SIZE = 100;
    private static final int HANDLE_CLEAR_CACHE_START = 9;
    private static final int HANDLE_CLEAR_CACHE_END = 10;

    private double cacheFileSize;

    private PopupWindow popupWindow;
    private SharePreUtils sharePreUtils;

    private View cacheClarityView;
    private TextView cacheClarityTv;
    private PopupWindow cacheClarityPop;

    private Dialog dialog, cacheDialog, showOpenNotificationDialog;
    private String cacheFilePath = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        cacheFilePath = SettingActivity.this.getCacheDir() + File.separator + VolleyQueue.DEFAULT_CACHE_VOLLEY_DIR;
        enableExitWithSlip(true);
        user = new User(this);
        startMeasureCacheSizeThread();
        initViews();

        // boolean isAlreadyCheckUpdate = false;
        // if (!isAlreadyCheckUpdate) {
        //  registerUpgradeBroadcast();
        // }
        registerUpgradeBroadcast();
    }

    private void registerUpgradeBroadcast() {
        if (PhoneConfig.isGooglePlay()) {
            return;
        }
        IntentFilter intentFilter = new IntentFilter(IntentConfig.UPGRADE_YES_ACTION);
        intentFilter.addAction(IntentConfig.UPGRADE_IS_LAST_ACTION);
        LocalBroadcastManager.getInstance(getApplication()).registerReceiver(upgradeBroadcastReceiver, intentFilter);
        IntentConfig.startCheckUpgrade(this);
    }

    private void unregisterUpgradeBroadcast() {
        if (upgradeBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this.getApplicationContext()).unregisterReceiver(upgradeBroadcastReceiver);
            upgradeBroadcastReceiver = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCachePath();
    }

    private void initViews() {
        isLogin = User.isLogin();

        sharePreUtils = SharePreUtils.getInstance();
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.setting));
        findViewById(R.id.title).setOnClickListener(this);
        //允许2G/3G/4G开关按钮
        wifiSwitch = (ImageSwitcher) findViewById(R.id.setting_wifiselector);
        wifiSwitch.setOnClickListener(this);
        boolean hasCacheVideo = sharePreUtils.getCacheVideoState();
        if (!hasCacheVideo) {
            wifiSwitch.showNext();
        }
        wifiSwitch.setContentDescription(hasCacheVideo ? getString(R.string.auto_test_content_des_selected) : getString(R.string.auto_test_content_des_not_selected));

        //消息推送开关按钮
        pushSwitch = (ImageSwitcher) findViewById(R.id.setting_pushselector);
        pushSwitch.setOnClickListener(this);
        boolean push_on = sharePreUtils.getPushMessageState();
        if (!push_on) {
            pushSwitch.showNext();
        }
        pushSwitch.setContentDescription(push_on ? getString(R.string.auto_test_content_des_selected) : getString(R.string.auto_test_content_des_not_selected));

        //画中画按钮
        pipSwitch = (ImageSwitcher) findViewById(R.id.setting_pipselector);
        pipSwitch.setOnClickListener(this);
        boolean pip_on = sharePreUtils.getPiPModeState();
        if (!pip_on) {
            pipSwitch.showNext();
        }
        pipSwitch.setContentDescription(pip_on ? getString(R.string.auto_test_content_des_selected) : getString(R.string.auto_test_content_des_not_selected));

        //后退键
        findViewById(R.id.back).setOnClickListener(this);

        //允许2G/3G/4G
        View wifiFunction = findViewById(R.id.wifi_switch_container);
        wifiFunction.setOnClickListener(this);

        //消息推送
        View pushMessage = findViewById(R.id.push_switch_container);
        pushMessage.setOnClickListener(this);

        //视频路径缓存
        View videoCachePath = findViewById(R.id.video_cache_path_ll);
        videoCachePath.setOnClickListener(this);

        //默认播放
        findViewById(R.id.default_play_ll).setOnClickListener(this);
        defaultPlayType = (TextView) findViewById(R.id.setting_defaultplayer_selector);
        defaultPlayType.setOnClickListener(this);
        boolean playAudio = sharePreUtils.getDefaultPlayMode();
        defaultPlayType.setText(playAudio ? R.string.setting_audio : R.string.setting_video);

        //默认缓存清晰度
        View cacheClarity = findViewById(R.id.cache_clarity_container);
        cacheClarity.setOnClickListener(this);

        cacheClarityTv = (TextView) findViewById(R.id.setting_cache_clarity_selector);
        String tmpCacheClarity = sharePreUtils.getCacheClarityState();
        if (!TextUtils.isEmpty(tmpCacheClarity)) {
            cacheClarityTv.setText(tmpCacheClarity);
        }

        //检测新版本
        View newVersion = findViewById(R.id.check_new_version);
        newVersion.setOnClickListener(this);
        if (PhoneConfig.isGooglePlay()) {
            newVersion.setVisibility(View.GONE);
        }
        mVersionCode = (TextView) findViewById(R.id.setting_version_info);
        versionInfo = (TextView) findViewById(R.id.setting_versionupgrade);
        String currentVersionCode = "(V" + PhoneConfig.softversion + ")";
        mVersionCode.setText(currentVersionCode);


        //清除缓存
        View clearCache = findViewById(R.id.setting_clearcache);
        clearCache.setOnClickListener(this);
        clearCacheText = (TextView) findViewById(R.id.setting_cache);

        //关于
        View about = findViewById(R.id.setting_about);
        about.setOnClickListener(this);

        //退出登录
        loginQut = findViewById(R.id.setting_login_out);
        loginQut.setOnClickListener(this);
        loginQutLayout = findViewById(R.id.login_out_layout);
        viewDivider = findViewById(R.id.view_divider);
        loginQutLayout.setVisibility(isLogin ? View.VISIBLE : View.GONE);
        viewDivider.setVisibility(isLogin ? View.VISIBLE : View.GONE);
    }

    private int clickTitleCount = 0;
    private long clickTitleTime = 0;

    @Override
    public void onClick(View v) {
        int vId = v.getId();
        switch (vId) {
            case R.id.title: // 标题，连点五次进入设备信息页面
                long curTime = System.currentTimeMillis();
                if (clickTitleTime == 0 || curTime - clickTitleTime < 1000) {
                    clickTitleCount++;
                    clickTitleTime = System.currentTimeMillis();
                    if (clickTitleCount == 5) {
                        IntentUtils.startSetInfoActivity(this);
                        clickTitleCount = 0;
                        clickTitleTime = 0;
                    }
                } else {
                    clickTitleCount = 0;
                    clickTitleTime = 0;
                }
                break;
            case R.id.wifi_switch_container: // ＂允许运营商网络缓存＂设置
                wifiSwitch.performClick();
                break;
            case R.id.setting_wifiselector://＂允许运营商网络缓存＂设置
                if (wifiSwitch.getCurrentView().getId() == R.id.wifiimageOn) {//如果是开启状态
                    setWifiSwitchState();
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_MOBILE_CACHE, ActionIdConstants.ACT_NO, PageIdConstants.MY_SETUP);
                } else {
                    cacheDialog = DialogUtilsFor3G.showCacheVideo4OperatorDialog(this, new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setWifiSwitchState();
                            PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_MOBILE_CACHE, ActionIdConstants.ACT_YES, PageIdConstants.MY_SETUP);
                            if (cacheDialog != null) {
                                cacheDialog.dismiss();
                            }
                        }
                    }, new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (cacheDialog != null) {
                                cacheDialog.dismiss();
                            }
                            PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_MOBILE_CACHE, ActionIdConstants.ACT_CANCEL, PageIdConstants.MY_SETUP);
                        }
                    });
                }

                break;
            case R.id.push_switch_container:// 推送开关
                pushSwitch.performClick();
                break;
            case R.id.setting_pushselector://消息推送
                pushSwitch.showNext();
                boolean isPushOn = pushSwitch.getCurrentView().getId() == R.id.setting_push_on;
                if (isPushOn) {
                    PushUtils.openPush();
                } else {
                    PushUtils.closePush();
                }
                sharePreUtils.setPushMessageState(isPushOn);
                pushSwitch.setContentDescription(isPushOn ? getString(R.string.auto_test_content_des_selected) :
                        getString(R.string.auto_test_content_des_not_selected));

                if (sharePreUtils.getPushMessageState()) {
                    if (!NotificationCheckUtils.notificationIsOpen(this)) {
                        showOpenNotificationDialog();
                    }
                }

                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_PUSH, isPushOn, PageIdConstants.MY_SETUP);
                break;
            case R.id.video_cache_path_ll:   //视频缓存路径
                IntentUtils.startCachePathActivity(this);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_CACHE_PATH, PageIdConstants.MY_SETUP);
                break;
            case R.id.default_play_ll://默认播放
                showDefaultPlayTypeDialog();
                break;
            case R.id.check_new_version: // 检测新版本
                handleVersionUpgrade();
                break;

            case R.id.setting_clearcache://清除缓存
                if (cacheFileSize == 0.0f && clearCacheText.getText().toString().contains("0.0M")) {
                    //ToastUtils.getInstance().showLongToast(getString(R.string.setting_clear_cache_not_necessary));
                } else {
//                    int OPERATION_CLEAR_CACHE = 0;
//                    showOperationWindow(OPERATION_CLEAR_CACHE);
                    cacheClear();
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_CACHE_CLEAR, PageIdConstants.MY_SETUP);
                }
                break;
            case R.id.setting_about://关于
                IntentUtils.startAboutActivity(this);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_ABOUT, PageIdConstants.MY_SETUP);
                break;

            case R.id.setting_login_out://退出登录
                if (!ClickUtils.isFastDoubleClick()) {
                    showExitDialog();
                }
                break;
            case R.id.back://返回
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_SETUP);
                finish();
                break;
            case R.id.cache_clarity_container: //默认缓存清晰度
                showCacheClarityPop();
                PageActionTracker.clickBtn(ActionIdConstants.DEFAULT_VIDEO_RESOLUTION, PageIdConstants.MY_SETUP);
                break;
            case R.id.pip_switch_container:
                pipSwitch.performClick();
                break;
            case R.id.setting_pipselector://画中画
                pipSwitch.showNext();
                boolean isPipOn = pipSwitch.getCurrentView().getId() == R.id.setting_pip_on;
                sharePreUtils.setPipModeState(isPipOn);
                pipSwitch.setContentDescription(isPipOn ? getString(R.string.auto_test_content_des_selected) :
                        getString(R.string.auto_test_content_des_not_selected));
                PageActionTracker.clickBtn(ActionIdConstants.PIP_SETTING_SWITCH, isPipOn, PageIdConstants.MY_SETUP);
                break;
            default:
                break;
        }

    }

    public void setWifiSwitchState() {
        wifiSwitch.showNext();
        boolean currentHasCacheVideo4Operator = wifiSwitch.getCurrentView().getId() == R.id.wifiimageOn;
        sharePreUtils.setCacheVideoState(currentHasCacheVideo4Operator);
        NetSettingChangedListener listener = CacheManager.getNetSettingChangedListener();
        if (listener != null) {
            wifiSwitch.setContentDescription(currentHasCacheVideo4Operator ? getString(R.string.auto_test_content_des_selected) : getString(R.string.auto_test_content_des_not_selected));
            listener.onNetSettingSwitchChanged(SettingActivity.this, currentHasCacheVideo4Operator ? NetSettingChangedListener.SETTING_SWITCH_NET_ON :
                    NetSettingChangedListener.SETTING_SWITCH_NET_OFF);
        }
    }

    private void handleVersionUpgrade() {
        if (PhoneConfig.isGooglePlay()) {
            return;
        }

        if (!NetUtils.isNetAvailable(this)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        } else {
            if (theUpgradeInfo != null) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_NOW_UPDATE, PageIdConstants.MY_SETUP);
                UpgradePopupWindow.showUpgradePopuWin(this, theUpgradeInfo, getWindow().getDecorView(), Gravity.NO_GRAVITY, 0, 0);
            } else {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SET_CHECK_VER, PageIdConstants.MY_SETUP);
                IntentUtils.startAboutActivity(this);
//                IntentConfig.startCheckUpgrade(this);

            }
        }
    }

    private void showOpenNotificationDialog() {

        if (null != showOpenNotificationDialog && showOpenNotificationDialog.isShowing()) {
            showOpenNotificationDialog.dismiss();
        }

        showOpenNotificationDialog = AlertUtils.getInstance().showConfirmBtnDialog(this, getString(R.string.custom_dialog_title), getString(R.string.custom_dialog_content), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOpenNotificationDialog.dismiss();
            }
        }, getString(R.string.custom_dialog_confirm_btn), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.openAndroidAppSet(SettingActivity.this);
                showOpenNotificationDialog.dismiss();
            }
        });
    }

    private final Handler clearCacheHandler = new Handler() {
        private Dialog dialog;

        @Override
        public void handleMessage(android.os.Message msg) {
            if (dialog == null) {
                dialog = new Dialog(SettingActivity.this, R.style.common_Theme_Dialog);
                dialog.setContentView(R.layout.setting_download_add_progress);
                dialog.setCancelable(false);
                TextView text = (TextView) dialog.findViewById(R.id.text);
                text.setText(SettingActivity.this.getString(R.string.setting_clear_cache_running));
            }

            switch (msg.what) {
                case HANDLE_CLEAR_CACHE_START:
                    dialog.show();
                    break;
                case HANDLE_CLEAR_CACHE_END:
                    cacheFileSize = 0.0f;
                    //dialog.dismiss();
                    cacheTextInit("0.0");
                    ToastUtils.getInstance().showShortToast(SettingActivity.this.getString(R.string.setting_clear_cache_success));
                    break;
                case MSG_CACHE_SIZE:
                    String cacheSizes = msg.getData().getString("cacheFileSizes");
                    cacheTextInit(cacheSizes);
            }
        }
    };

    private void showDefaultPlayTypeDialog() {
        int checkedItem = sharePreUtils.getDefaultPlayMode() ? 1 : 0;

        AlertUtils.getInstance().showSingleChoiceItemsDialog(this, getString(R.string.setting_default_player),
                R.array.setting_default_play_type, checkedItem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            sharePreUtils.setDefaultPlayMode(false);
                            defaultPlayType.setText(R.string.setting_video);
                        } else {
                            sharePreUtils.setDefaultPlayMode(true);
                            defaultPlayType.setText(R.string.setting_audio);
                        }
                        dialog.dismiss();
                    }
                }, getString(R.string.setting_btn_cancel), new DefaultPlayModeDialogOnClickListener(),
                false);
    }

    private void cacheTextInit(String text) {
        String formatter = getResources().getString(R.string.setting_cache_size);
        String cacheSize = "0.0M";
        try {
            cacheSize = String.format(formatter, text);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        clearCacheText.setText(cacheSize);
    }

    private void setCachePath() {
        TextView videoCachePathTv = (TextView) findViewById(R.id.setting_pathselector);
        boolean sp_cache_sdCard = sharePreUtils.getCachePathState();
        boolean isSDCardAvailable = StorageUtils.getInstance().isExternalMemoryAvailable();
        if (sp_cache_sdCard && isSDCardAvailable) {
            videoCachePathTv.setText(getString(R.string.setting_sd_card));
            sharePreUtils.setCachePathState(true);
        } else {
            videoCachePathTv.setText(getString(R.string.setting_inner_card));
            sharePreUtils.setCachePathState(false);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.common_slide_right_in, R.anim.common_slide_right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterUpgradeBroadcast();
    }

    private void showExitDialog() {
        dialog = AlertUtils.getInstance().showTwoBtnDialog(this, getString(R.string.setting_exit_login_confirm), getString(R.string.common_ok), new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                user.clearUserInfo();//2
                isLogin = false;
                Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);//3
                loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.UNLOGIN);
                sendBroadcast(loginBroad);
                loginQutLayout.setVisibility(View.GONE);
                viewDivider.setVisibility(View.GONE);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGOUT, true, PageIdConstants.MY_SETUP);
                SharePreUtils.getInstance().setMsgTimer(0l);
                finish();
            }
        }, getString(R.string.setting_cancel), new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGOUT, false, PageIdConstants.MY_SETUP);
                }
            }
        });
    }

    private void showOperationWindow(int operationType) {

        if (clearCacheView == null) {
            clearCacheView = LayoutInflater.from(this).inflate(R.layout.setting_cache_delete_popup_window, null);
            TextView redLongBtn = (TextView) clearCacheView.findViewById(R.id.tv_del_c);
            redLongBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    cacheClear();
                    dismissPopu();
                }
            });
            View whiteLongBtn = clearCacheView.findViewById(R.id.ll_cancel);
            whiteLongBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismissPopu();
                }
            });
        }

        if (popupWindow == null) {
            popupWindow = new PopupWindow(clearCacheView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.argb(122, 0, 0, 0)));
            popupWindow.setFocusable(true);
            // popupWindow.setAnimationStyle(R.style.PopupAnimation);
//             popupWindow.setTouchInterceptor(new View.OnTouchListener() {
//                 @Override
//                 public boolean onTouch(View v, MotionEvent event) {
//                     Rect rect = new Rect();
//                     clearCacheView.findViewById(R.id.actionpanel).getHitRect(rect);
//                     if (!rect.contains((int) event.getX(), (int) event.getY()) && event.getAction() == MotionEvent.ACTION_UP) {
//                         dismissPopu();
//                         return true;
//                     }
//                     return false;
//                 }
//             });
        }
        // 监听back键事件，必须要设置这个属性
        clearCacheView.setFocusableInTouchMode(true);
        clearCacheView.findViewById(R.id.actionpanel).startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_popup_enter));
        popupWindow.setOutsideTouchable(false);
        popupWindow.showAtLocation(this.getWindow().getDecorView().findViewById(android.R.id.content), Gravity.BOTTOM, 0, 0);
        popupWindow.update();
    }

    private void dismissPopu() {
        if (popupWindow != null) {
            popupWindow.dismiss();
            popupWindow = null;
        }
    }

    private void dismissCachePop() {
        if (null != cacheClarityPop) {
            cacheClarityPop.dismiss();
            cacheClarityPop = null;
        }
    }

    private void showCacheClarityPop() {

        if (cacheClarityView == null) {
            cacheClarityView = LayoutInflater.from(this).inflate(R.layout.setting_cache_clarity_popup_window, null);
            TextView superHdTv = (TextView) cacheClarityView.findViewById(R.id.super_hd_tv);
            superHdTv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    cacheClarityTv.setText(getResources().getString(R.string.setting_cache_clarity));
                    sharePreUtils.setCacheClarityState(getResources().getString(R.string.setting_cache_clarity));
                    dismissCachePop();
                }
            });
            TextView hdTv = (TextView) cacheClarityView.findViewById(R.id.hd_tv);
            hdTv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    cacheClarityTv.setText(getResources().getString(R.string.setting_cache_clarity_hd));
                    sharePreUtils.setCacheClarityState(getResources().getString(R.string.setting_cache_clarity_hd));
                    dismissCachePop();
                }
            });
            TextView standardTv = (TextView) cacheClarityView.findViewById(R.id.standard_tv);
            standardTv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    cacheClarityTv.setText(getResources().getString(R.string.setting_cache_clarity_standard));
                    sharePreUtils.setCacheClarityState(getResources().getString(R.string.setting_cache_clarity_standard));
                    dismissCachePop();
                }
            });
            View whiteLongBtn = cacheClarityView.findViewById(R.id.ll_cancel);
            whiteLongBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismissCachePop();
                }
            });
            View popLayout = cacheClarityView.findViewById(R.id.pop_layout);
            popLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismissCachePop();
                }
            });
        }

        if (cacheClarityPop == null) {
            cacheClarityPop = new PopupWindow(cacheClarityView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//            cacheClarityPop.setBackgroundDrawable(new ColorDrawable(Color.argb(122, 0, 0, 0)));
            cacheClarityPop.setBackgroundDrawable(new ColorDrawable());
            cacheClarityPop.setFocusable(true);
            cacheClarityPop.setOutsideTouchable(true);
        }
        // 监听back键事件，必须要设置这个属性
        cacheClarityView.setFocusableInTouchMode(true);
        cacheClarityView.setFocusable(true);
        cacheClarityView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (cacheClarityPop != null) {
                        dismissCachePop();
                    }
                }
                return false;
            }
        });

        cacheClarityView.findViewById(R.id.cache_clarity_pop).startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_popup_enter));
        cacheClarityPop.showAtLocation(this.getWindow().getDecorView().findViewById(android.R.id.content), Gravity.BOTTOM, 0, 0);
        cacheClarityPop.update();
    }

    private void startMeasureCacheSizeThread() {
        if (measureCacheSizeThread == null || !measureCacheSizeThread.isAlive()) {
            measureCacheSizeThread = new Thread(cacheSizeInitRunnable);
            measureCacheSizeThread.start();
        }
    }

    private UpgradeInfo theUpgradeInfo;
    private final Runnable cacheSizeInitRunnable = new Runnable() {
        @Override
        public void run() {
            File file = new File(cacheFilePath);
            if (!file.exists()) {
                return;
            }
            cacheFileSize = FileUtils.getFileSize(cacheFilePath);
            cacheFileSize = cacheFileSize / 1024 / 1024;
            String cacheFileSizes = "0.0";
            if (cacheFileSize != 0.0) {
                cacheFileSizes = String.format(Locale.US, "%.2f", cacheFileSize);
            }
            Message message = Message.obtain();
            message.what = MSG_CACHE_SIZE;
            Bundle bundleData = new Bundle();
            bundleData.putString("cacheFileSizes", cacheFileSizes);
            message.setData(bundleData);
            clearCacheHandler.sendMessage(message);
        }
    };

    private void cacheClear() {
        new Thread(new ClearCacheRunnable()).start();
        ImageLoaderManager.getInstance().cleanMemory(IfengApplication.getInstance());
    }

    private static class DefaultPlayModeDialogOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    private static class OnCancelClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    private class ClearCacheRunnable implements Runnable {

        @Override
        public void run() {
            //clearCacheHandler.sendEmptyMessage(HANDLE_CLEAR_CACHE_START);
            File file = new File(cacheFilePath);
            FileUtils.delete(file);
            clearCacheHandler.sendEmptyMessage(HANDLE_CLEAR_CACHE_END);
        }

    }

    private BroadcastReceiver upgradeBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }
            UpgradeInfo upgradeInfo;
            if (IntentConfig.UPGRADE_YES_ACTION.equals(intent.getAction())) {//yes,I get New Version
                if ((upgradeInfo = intent.getParcelableExtra(IntentConfig.INTENT_EXTRA_UPGRADE_INFO)) != null) {
                    theUpgradeInfo = upgradeInfo;
//                    TextView updateText = (TextView) findViewById(R.id.setting_versionupgrade);
//                    View view = findViewById(R.id.update_img_button);
//                    view.setVisibility(View.VISIBLE);
                    String version;
                    try {
                        version = PackageUtils.getAppVersion(context);
                    } catch (PackageManager.NameNotFoundException e) {
                        logger.error(e.toString(), e);
                        version = "0.0";
                    }
                    String newVersion = "(V" + version + ")";
                    mVersionCode.setText(newVersion);
                    versionInfo.setTextColor(Color.parseColor("#F54343"));
                    versionInfo.setText(getString(R.string.setting_now_update));
//                    updateText.setText(String.format(getString(R.string.cur_version), version));
                }
            } else if (IntentConfig.UPGRADE_IS_LAST_ACTION.equals(intent.getAction())) {
//                TextView updateText = (TextView) findViewById(R.id.setting_versionupgrade);
                versionInfo.setText(getString(R.string.latest_version));

//                View view = findViewById(R.id.update_img_button);
//                if (updateText.getVisibility() == View.VISIBLE) {
//                    ToastUtils.getInstance().showShortToast(R.string.setting_already_new_version);
//                } else {
//                    view.setVisibility(View.GONE);
//                    updateText.setText(getString(R.string.latest_version));
//                }
            }
        }
    };
}
