package com.ifeng.newvideo.setting.activity;

import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.utils.DistributionInfo;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.player.ChoosePlayerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * 应用设置，展示版本信息
 */
public class SettingInfoActivity extends BaseFragmentActivity {

    private static final Logger logger = LoggerFactory.getLogger(SettingInfoActivity.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_info);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.app_setting));

        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        UUID deviceUuid = UUID.randomUUID();
        try {
            if (PhoneConfig.IMEI != null && !PhoneConfig.IMEI.contains("00000")) {
                deviceUuid = UUID.nameUUIDFromBytes(PhoneConfig.IMEI.getBytes("utf8"));
            } else if (androidId != null && !androidId.contains("00000")) {
                deviceUuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String uniqueId = deviceUuid.toString();

        StringBuilder sb = new StringBuilder();
        sb.append("Push : ").append(Boolean.valueOf(DistributionInfo.isPushTest) ? "test" : "normal").append("\n\n");
        sb.append("USER KEY : ").append(PhoneConfig.userKey).append("\n\n");
        sb.append("PublishId : ").append(PhoneConfig.publishid).append("\n\n");
        sb.append("App Version : ").append(PhoneConfig.softversion).append("\n\n");
        sb.append("Player : ").append(ChoosePlayerUtils.useIJKPlayer(this) ? "ijk" : "zy").append("\n\n");
        sb.append("System Version : ").append(PhoneConfig.mos).append("\n\n");
        sb.append("Device : ").append(PhoneConfig.MANUFACTURER + " / " + Build.BRAND + " / " + android.os.Build.MODEL).append("\n\n");
        sb.append("Screen : ").append(PhoneConfig.getRe()).append("   Destiny : ").append(PhoneConfig.screenDensity).append("\n\n");
        sb.append("UserID : ").append(new User(this).getUid()).append("\n\n");
        sb.append("Current Network : ").append(NetUtils.getNetType(this)).append("\n\n");
        sb.append("UUID : " + uniqueId).append("\n\n");
        sb.append("ANDROID_ID : ").append(androidId).append("\n\n");
        sb.append("IMEI : ").append(PhoneConfig.IMEI).append("\n\n");
        // sb.append("MAC : ").append(PhoneConfig.getDeviceMac(this)).append("\n\n");

        ((TextView) findViewById(R.id.setting_text)).setText(sb.toString());

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }

}
