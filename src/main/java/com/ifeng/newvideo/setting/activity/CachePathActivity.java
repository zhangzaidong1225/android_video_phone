package com.ifeng.newvideo.setting.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.setting.widgets.PercentProgressView;
import com.ifeng.newvideo.setting.widgets.PercentProgressView.Category;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.FileUtils;
import com.ifeng.video.core.utils.StorageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * 设置视频缓存路径页面
 */
public class CachePathActivity extends BaseFragmentActivity implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(CachePathActivity.class);

    private TextView tvSdCacheTotalSpace;
    private TextView tvSDCacheOtherSpace;
    private TextView tvSDCacheVideosSpace;
    private TextView tvSDCacheRemainSpace;

    private PercentProgressView pbSdcard;

    private TextView tvInnerCacheTotalSpace;
    private TextView tvInnerCacheOtherSpace;
    private TextView tvInnerCacheVideosSpace;
    private TextView tvInnerCacheRemainSpace;

    private PercentProgressView pbInnerCache;

    private BroadcastReceiver sdcardBR;
    private SharePreUtils sharePreUtils;

    private ImageView ivCacheSd;
    private ImageView ivInnerCache;

    private boolean isSDCard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_cachepath);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        sharePreUtils = SharePreUtils.getInstance();
        isSDCard = sharePreUtils.getCachePathState();
        initViews();
    }

    private void initViews() {
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.setting_cache_path_text));

        pbSdcard = (PercentProgressView) findViewById(R.id.pbSdcard);
        tvSdCacheTotalSpace = (TextView) findViewById(R.id.tvSdCacheTotalSpace);
        tvSDCacheOtherSpace = (TextView) findViewById(R.id.tvSDCacheOtherSpace);
        tvSDCacheVideosSpace = (TextView) findViewById(R.id.tvSDCacheVideosSpace);
        tvSDCacheRemainSpace = (TextView) findViewById(R.id.tvSDCacheRemainSpace);

        pbInnerCache = (PercentProgressView) findViewById(R.id.pbInnerCache);
        tvInnerCacheTotalSpace = (TextView) findViewById(R.id.tvInnerCacheTotalSpace);
        tvInnerCacheOtherSpace = (TextView) findViewById(R.id.tvInnerCacheOtherSpace);
        tvInnerCacheVideosSpace = (TextView) findViewById(R.id.tvInnerCacheVideosSpace);
        tvInnerCacheRemainSpace = (TextView) findViewById(R.id.tvInnerCacheRemainSpace);

        ivCacheSd = (ImageView) findViewById(R.id.ivCacheSd);
        findViewById(R.id.sdcard_ll).setOnClickListener(this);
        ivInnerCache = (ImageView) findViewById(R.id.ivInnerCache);
        findViewById(R.id.inner_ll).setOnClickListener(this);

        ImageView ivBack = (ImageView) findViewById(R.id.back);
        ivBack.setOnClickListener(this);

        setCatchSelectImage();
    }

    private void setCatchSelectImage() {
        boolean catch_sdcard = sharePreUtils.getCachePathState();
        boolean sdcardAvailable = StorageUtils.getInstance().isExternalMemoryAvailable();
        logger.debug("catch_sdcard      =  {}", catch_sdcard);
        logger.debug("sdcardAvailable   =  {}", sdcardAvailable);

        if (catch_sdcard && sdcardAvailable) {
            findViewById(R.id.sdcard_ll).setVisibility(View.VISIBLE);
            ivCacheSd.setImageResource(R.drawable.setting_cache_path_select);
            ivInnerCache.setImageResource(R.drawable.setting_cache_path_nomal);
            sharePreUtils.setCachePathState(true);
        } else if (sdcardAvailable) {
            findViewById(R.id.sdcard_ll).setVisibility(View.VISIBLE);
            ivCacheSd.setImageResource(R.drawable.setting_cache_path_nomal);
            ivInnerCache.setImageResource(R.drawable.setting_cache_path_select);
            sharePreUtils.setCachePathState(false);
        } else {
            findViewById(R.id.sdcard_ll).setVisibility(View.GONE);
            ivInnerCache.setImageResource(R.drawable.setting_cache_path_select);
            sharePreUtils.setCachePathState(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        capacityInit();
        initSDCardBR();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(sdcardBR);
    }

    @Override
    public void onClick(View v) {
        boolean catch_sdcard = sharePreUtils.getCachePathState();
        switch (v.getId()) {
            case R.id.sdcard_ll:
                if (!catch_sdcard) {
                    sharePreUtils.setCachePathState(true);
                    setCatchSelectImage();
                    ToastUtils.getInstance().showShortToast(R.string.setting_success);
                }
                break;
            case R.id.inner_ll:
                if (catch_sdcard) {
                    sharePreUtils.setCachePathState(false);
                    setCatchSelectImage();
                    ToastUtils.getInstance().showShortToast(R.string.setting_success);
                }
                break;
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_SETUP_CACHE_PATH);
                finish();
                break;
            default:
                break;
        }
    }

    private void initSDCardBR() {
        sdcardBR = new SDCardBR();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addDataScheme("file");
        registerReceiver(sdcardBR, filter);
    }

    private void capacityInit() {
        if (StorageUtils.getInstance().isExternalMemoryAvailable()) {
            // 总空间
            float totalSize = getFormatFloat((StorageUtils.getInstance().getTotalSdCardMemorySize()) / 1024 / 1024 / 1024f);// 单位G
            // 剩余空间
            float availableSize = getFormatFloat((StorageUtils.getInstance().getAvailableSdCardMemorySize()) / 1024 / 1024 / 1024f);// 单位G
            // 视频缓存
            float videosSize = getFormatFloat((FileUtils.getExternalVideoSize(this)) / 1024 / 1024 / 1024f);
            float otherSize = getFormatFloat(totalSize - availableSize - videosSize);

            logger.debug("totalSize      =  {}", totalSize);
            logger.debug("availableSize  =  {}", availableSize);
            logger.debug("videosSize     =  {}", videosSize);
            logger.debug("otherSize      =  {}", otherSize);
            ArrayList<PercentProgressView.Category> mCategories = new ArrayList<>();
            mCategories.add(new Category(R.color.setting_sdcard_other_bg, otherSize / totalSize));
            mCategories.add(new Category(R.color.setting_sdcard_cachevideo_bg, videosSize / totalSize));
            mCategories.add(new Category(R.color.setting_sdcard_surplus_bg, 1 - (otherSize / totalSize) - (videosSize / totalSize)));
            pbSdcard.setPercent(mCategories);

            tvSdCacheTotalSpace.setText(String.format("%.2f", totalSize) + "G");
            tvSDCacheOtherSpace.setText(String.format("%.2f", otherSize) + "G");
            tvSDCacheVideosSpace.setText(String.format("%.2f", videosSize) + "G");
            tvSDCacheRemainSpace.setText(String.format("%.2f", availableSize) + "G");
        }
        // 总空间
        float innerTotalSize = getFormatFloat(StorageUtils.getInstance().getTotalInternalMemorySize() / 1024 / 1024 / 1024f);// 单位G
        // 剩余空间
        float innerAvailSize = getFormatFloat(StorageUtils.getInstance().getAvailableInternalMemorySize() / 1024 / 1024 / 1024f);// 单位G
        // 缓存视频大小
        float innerVideoSize = getFormatFloat(FileUtils.getInnerVideoFileSize(this) / 1024 / 1024 / 1024f);// 单位G
        // 其他程序大小
        float innerOtherSize = getFormatFloat(innerTotalSize - innerAvailSize - innerVideoSize);

        logger.debug("innerTotalSize      =  {}", innerTotalSize);
        logger.debug("innerAvailSize      =  {}", innerAvailSize);
        logger.debug("innerVideoSize      =  {}", innerVideoSize);
        logger.debug("innerOtherSize      =  {}", innerOtherSize);

        ArrayList<Category> mCategories = new ArrayList<Category>();
        mCategories.add(new PercentProgressView.Category(R.color.setting_sdcard_other_bg, 1 - (innerAvailSize / innerTotalSize) - (innerVideoSize / innerTotalSize)));
        mCategories.add(new Category(R.color.setting_sdcard_cachevideo_bg, innerVideoSize / innerTotalSize));
        mCategories.add(new Category(R.color.setting_sdcard_surplus_bg, innerAvailSize / innerTotalSize));
        pbInnerCache.setPercent(mCategories);

        tvInnerCacheTotalSpace.setText(String.format("%.2f", innerTotalSize) + "G");
        tvInnerCacheOtherSpace.setText(String.format("%.2f", innerOtherSize) + "G");
        tvInnerCacheVideosSpace.setText(String.format("%.2f", innerVideoSize) + "G");
        tvInnerCacheRemainSpace.setText(String.format("%.2f", innerAvailSize) + "G");
    }

    private float getFormatFloat(float f) {
        DecimalFormat df = new DecimalFormat("#.##");
        return Float.valueOf(df.format(f));
    }

    private class SDCardBR extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            setCatchSelectImage();
            capacityInit();
        }
    }

    @Override
    public void finish() {
        if (isSDCard != sharePreUtils.getCachePathState()) {
            CacheManager.getStorageSettingChangedListener().onStorageSettingChanged(this);
        }
        super.finish();
    }
}
