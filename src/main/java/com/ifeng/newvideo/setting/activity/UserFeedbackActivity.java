package com.ifeng.newvideo.setting.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.*;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import com.alibaba.fastjson.JSON;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.*;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.player.ChoosePlayerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

/**
 * 吐槽页面，反馈意见
 */
public class UserFeedbackActivity extends BaseFragmentActivity implements TextWatcher, View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(UserFeedbackActivity.class);

    private int editStart;
    private int editEnd;
    private final int MAX_COUNT = 500;

    private EditText etContent;
    private EditText etContact;

    private RelativeLayout emailAddress;
    private TextView tvCounter;
    private RelativeLayout qqGroupNum;
    //private ImageView btnSubmit;
    private ImageView back;
    private TextView title_right_text;//提交
    private boolean hasSubmitted = false;

    private final int MSG_SUBMIT_SUCCESS = 0;
    private final int MSG_SUBMIT_FAILED = 1;
    private long lastClickTime = 0;
    private long clickTime = 0;
    private Dialog netInvalidDialog;
    private Dialog submittingDialog;

    private ScrollView mScrollView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_user_feedback);

        hasSubmitted = false;
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);

        ((TextView) findViewById(R.id.title)).setText(getString(R.string.setting_feedback));

        //btnSubmit = (ImageView) findViewById(R.id.editor);
        //btnSubmit.setImageResource(R.drawable.setting_submit_selector);
        //btnSubmit.setVisibility(View.VISIBLE);
        //btnSubmit.setOnClickListener(this);

        title_right_text = (TextView) findViewById(R.id.title_right_text);
        title_right_text.setText(getString(R.string.commit));
        //title_right_text.setTextColor(getResources().getColor(R.drawable.right_title_text_color_select));
        title_right_text.setVisibility(View.VISIBLE);
        title_right_text.setOnClickListener(this);

        mScrollView = (ScrollView) findViewById(R.id.user_advices_scroll);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);

        etContent = (EditText) findViewById(R.id.user_advices_et);
        etContact = (EditText) findViewById(R.id.user_contact_way);
        etContent.addTextChangedListener(this);
        etContact.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String contact = etContact.getText().toString();

                    if (contact.length() > 0 && !StringUtils.isEmail(contact) && !StringUtils.isMobileNumberAll(contact) && !StringUtils.isQQNum(contact)) {
                        AlertUtils.getInstance().showOneBtnDialog(UserFeedbackActivity.this, getString(R.string.setting_contact_info_error),
                                getString(R.string.setting_i_know), null);
                    }
                }
            }
        });

        etContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        mScrollView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mScrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        qqGroupNum = (RelativeLayout) findViewById(R.id.qq_group_num);
        emailAddress = (RelativeLayout) findViewById(R.id.email_address);
        tvCounter = (TextView) findViewById(R.id.text_counter);
        qqGroupNum.setOnClickListener(this);
        emailAddress.setOnClickListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void afterTextChanged(Editable s) {
        editStart = etContent.getSelectionStart();
        editEnd = etContent.getSelectionEnd();
        // 先去掉监听器，否则会出现栈溢出
        etContent.removeTextChangedListener(this);

        // 注意这里只能每次都对整个EditText的内容求长度，不能对删除的单个字符求长度
        // 因为是中英文混合，单个字符而言，calculateLength函数都会返回1
        /*
         * while (calculateLength(s.toString()) > MAX_COUNT) { // 当输入字符个数超过限制的大小时，进行截断操作 s.delete(editStart - 1, editEnd);
		 * editStart--; editEnd--; }
		 */
        // mEditText.setText(s);将这行代码注释掉就不会出现后面所说的输入法在数字界面自动跳转回主界面的问题了，多谢@ainiyidiandian的提醒
        etContent.setSelection(editStart);

        if (s.toString().length() != 0) {
            title_right_text.setTextColor(Color.parseColor("#f54343"));
        } else {
            title_right_text.setTextColor(Color.parseColor("#262626"));
        }
        // 恢复监听器
        etContent.addTextChangedListener(this);
        setLeftCount();
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    private void setLeftCount() {
        int remain = MAX_COUNT - getInputCount();
        String text;
        tvCounter.setTextColor(remain > 0 ? 0xffcccccc : 0xffff0000);
        if (remain < 0) {
            text = "已超过" + Math.abs(remain) + "个字";
            tvCounter.setText(text);
        } else {
            text = "还可以输入" + remain + "个字";
            SpannableStringBuilder builder = new SpannableStringBuilder(text);
            ForegroundColorSpan redSpan = new ForegroundColorSpan(0xffff0000);
            builder.setSpan(redSpan, 5, 5 + String.valueOf(remain).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvCounter.setText(builder);
        }
    }

    private int getInputCount() {
        return StringUtils.calculateLength(etContent.getText().toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_FEEDBACK);
                finish();
                break;
            case R.id.title_right_text:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_FEEDBACK_SUBMIT, PageIdConstants.MY_FEEDBACK);
                submit();
                break;
            case R.id.email_address:
                sendEmail();
                break;
            case R.id.qq_group_num:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_QQGROP, PageIdConstants.MY_FEEDBACK);
                joinQQGroup();
                break;
        }
    }

    private void joinQQGroup() {
        if (System.currentTimeMillis() - clickTime < 1500) {
            return;
        }
        clickTime = System.currentTimeMillis();

        boolean hasInstalledQQ = PackageUtils.hasInstalled(this, this.getString(R.string.qq_package_name1))
                || PackageUtils.hasInstalled(this, this.getString(R.string.qq_package_name2));
        if (hasInstalledQQ) {
            boolean joinQQGroup = IntentUtils.joinQQGroup(this, this.getString(R.string.qq_group_key));
            if (!joinQQGroup) {
                ToastUtils.getInstance().showShortToast(R.string.not_install_qq);
            }
        } else {
            logger.info("phone has not installed qq app");
            ToastUtils.getInstance().showShortToast(R.string.not_install_qq);
        }
    }

    private void sendEmail() {
        if (System.currentTimeMillis() - lastClickTime < 1500) {
            return;
        }
        lastClickTime = System.currentTimeMillis();

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        String[] tos = {"videoapp.feedback@ifeng.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, tos);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "凤凰视频反馈");
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent, "请选择电子邮件客户端"));
    }

    private void submit() {
        if (!NetUtils.isNetAvailable(this)) {
//            showNetInvalidDialog();
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }

        if (getInputCount() == 0) {
            AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.setting_input_suggestion_please), getString(R.string.setting_i_know), null);
            return;
        } else if (getInputCount() > 500) {
            AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.setting_suggestion_beyond_limit), getString(R.string.setting_i_know), null);
            return;
        }
        String contact = etContact.getText().toString();
        if (!StringUtils.isBlank(contact)) {
            if (!(StringUtils.isEmail(contact) || StringUtils.isMobileNumberAll(contact) || StringUtils.isQQNum(contact))) {
                AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.setting_contact_info_error), getString(R.string.setting_i_know), null);
                return;
            }
        }
        if (hasSubmitted) {
            return;
        }
        hasSubmitted = true;

        sendFeedback();
    }

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissSubmittingDialog();

            switch (msg.what) {
                case MSG_SUBMIT_SUCCESS:
                    ToastUtils.getInstance().showShortToast(UserFeedbackActivity.this.getResources().getString(R.string.setting_suggestion_submit_success));
                    finish();
                    break;
                case MSG_SUBMIT_FAILED:
                    showSubmitFailToast();
                    break;
            }
        }
    };

    private void showSubmitFailToast() {
        ToastUtils.getInstance().showShortToast(UserFeedbackActivity.this.getResources().getString(R.string.setting_suggestion_submit_failed));
        hasSubmitted = false;
    }

    private void showNetInvalidDialog() {
        if (netInvalidDialog == null) {
            netInvalidDialog = AlertUtils.getInstance().showTwoBtnDialog(this, getString(R.string.common_net_useless),
                    getString(R.string.setting_btn_nice), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (netInvalidDialog != null && netInvalidDialog.isShowing()) {
                                netInvalidDialog.dismiss();
                            }
                        }
                    }, getString(R.string.setting_brief),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (netInvalidDialog != null && netInvalidDialog.isShowing()) {
                                netInvalidDialog.dismiss();
                            }
                            IntentUtils.startSystemWifiSetActivity(UserFeedbackActivity.this);
                        }
                    });
        }
        netInvalidDialog.show();
        hasSubmitted = false;
    }

    private void showSubmittingDialog() {
        if (submittingDialog == null) {
            submittingDialog = AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.setting_suggestion_submitting),
                    getString(R.string.cancel), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dismissSubmittingDialog();
                        }
                    }
            );
        }
        submittingDialog.show();
    }

    private void dismissSubmittingDialog() {
        VolleyHelper.getRequestQueue().cancelAll("feedback");
        hasSubmitted = false;
        if (submittingDialog != null && submittingDialog.isShowing()) {
            submittingDialog.dismiss();
        }
    }

    @SuppressWarnings("unchecked")
    private void sendFeedback() {

        showSubmittingDialog();

        final UserFeed uf = new UserFeed();
        StringBuilder content = new StringBuilder();
        content.append(etContent.getText().toString());
        content.append(" [publishId=").append(PhoneConfig.publishid)
                .append(",player=").append(ChoosePlayerUtils.useIJKPlayer(this) ? "ijk" : "zy").append(']');
        String feedbackContent = content.toString();
        // TODO  后台反馈页面没有decode，所以不encode      http://feedback.ifeng.com/feedback/Login.jsp
        // try {
        //     feedbackContent = URLEncoderUtils.encodeInUTF8(content.toString());
        // } catch (UnsupportedEncodingException e) {
        //     logger.error("sendFeedback URLEncoderUtils error ! {}", e);
        // }
        uf.syncFillParams(feedbackContent, etContact.getText().toString(), UserFeed.CLIENT_TYPE_VIDEO, new UserFeed.CallBack() {
            @Override
            public void callBack() {
                String json = JSON.toJSONString(uf);
                if (TextUtils.isEmpty(json)) {
                    return;
                }
                String url = DataInterface.USER_FEED_BACK_STATISTIC_URL;
                RequestQueue requestQueue = VolleyHelper.getRequestQueue();
                logger.debug("submit feedback begin ...\nurl = {}, \ncontent = {}", url, json);
                JsonRequest jsonRequest = new JsonRequest(Request.Method.POST, url, json,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                logger.debug("submit feedback success! {}", response);
                                Message msg = handler.obtainMessage();
                                msg.what = MSG_SUBMIT_SUCCESS;
                                msg.obj = response;
                                handler.sendMessage(msg);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                logger.error("submit feedback fail! ", error);
                                Message msg = handler.obtainMessage();
                                msg.what = MSG_SUBMIT_FAILED;
                                handler.sendMessage(msg);
                            }
                        }) {
                    @Override
                    protected Response parseNetworkResponse(NetworkResponse response) {
                        try {
                            hasSubmitted = false;
                            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        } catch (UnsupportedEncodingException e) {
                            return Response.error(new ParseError(e));
                        }
                    }
                };
                jsonRequest.setTag("feedback");
                requestQueue.add(jsonRequest);
            }
        });
    }

}
