package com.ifeng.newvideo.setting.entity;


/**
 * 用户反馈DNS
 */
class FeedDns {

    public String HTTP_X_FORWARDED_FOR;
    public String REMOTE_ADDR;
    public String SERVER_HOST;
    public String DNS_RESOLVE;

}
