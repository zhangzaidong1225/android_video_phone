package com.ifeng.newvideo.setting.entity;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.ifeng.IfengHttpURLConnectionUtils;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.net.RequestString;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 用户反馈信息
 */
public class UserFeed {

    private static final Logger logger = LoggerFactory.getLogger(UserFeed.class);
    /**
     * 反馈的缓存，包括播放记录、网络状态、时间
     */
    private static UserFeed uf;
    public static final String USER_FEED_SP = "UserFeedSp";
    public static final String USER_FEED_STR = "UserFeedStr";
    /**
     * 失败统计上报到feedback中
     */
    public static final String CLIENT_TYPE_VIDEO_ERR = "errvideoClient";
    /**
     * 用户反馈（吐槽提意见）上报到feedback中
     */
    public static final String CLIENT_TYPE_VIDEO = "videoClient";
    public String content;
    public String clientPlatform;
    public String clientVersion;
    public String phoneType;
    public String phoneSystem;
    public String phoneVersion;
    public String network;
    public String netname;
    public String contact;
    public String clientType;
    public String lastItemIp;
    public String DNS;
    public List<IfengAddress> vIfengAddress;
    public static List<IfengAddress> ifengAddress = Collections.synchronizedList(new ArrayList<IfengAddress>());
    public static String lastIp;

    public static class IfengAddress {
        public String item;
        public String startTime;
        public String netstatus;
    }

    public interface CallBack {
        void callBack();
    }

    public void syncFillParams(String content, String contact, String clientType, CallBack callback) {
        this.content = content;
        this.contact = contact + "_*_" + PhoneConfig.userKey;
        this.clientPlatform = "androidPhone";
        this.clientVersion = PhoneConfig.softversion;
        this.phoneType = PhoneConfig.ua_for_feedback;
        this.phoneSystem = "android";
        this.phoneVersion = android.os.Build.VERSION.RELEASE;
        this.network = NetUtils.getNetType(IfengApplication.getInstance());
        String netName = NetUtils.getNetName(IfengApplication.getInstance());
        if (!TextUtils.isEmpty(netName)) {
            if (netName.equals("cucc")) {
                netName = "中国联通";
            } else if (netName.equals("cmcc")) {
                netName = "中国移动";
            } else if (netName.equals("ctcc")) {
                netName = "中国电信";
            } else {
                netName = "Unknow";
            }
        } else {
            netName = "Unknow";
        }
        this.netname = netName;
        this.clientType = clientType;
        getDns(callback);
        vIfengAddress = new ArrayList<IfengAddress>(ifengAddress);
        this.lastItemIp = lastIp;
    }

    public static String getUrlIp(final String srcurl) {
        if (!TextUtils.isEmpty(srcurl) && !srcurl.contains("http")) {
            return srcurl;
        }
        URL url = null;
        HttpURLConnection connection = null;
        String result = null;
        try {
            url = new URL(srcurl);
            connection = IfengHttpURLConnectionUtils.getConnection(srcurl);
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5 * 1000);
            connection.setReadTimeout(5 * 1000);
            connection.setInstanceFollowRedirects(true);
            result = connection.getURL().getHost().toString();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        if (!TextUtils.isEmpty(srcurl) && !TextUtils.isEmpty(result)) {
            if (url != null) {
                String host = url.getHost();
                for (int i = 0; i < host.length(); i++) {
                    if (Character.isLetter(host.charAt(i))) {
                        result = getInetAddress(result);
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static String getInetAddress(String host) {
        String ipAddress = "";
        InetAddress returnStr1 = null;
        try {
            returnStr1 = InetAddress.getByName(host);
            ipAddress = returnStr1.getHostAddress();
        } catch (Exception e) {
            logger.error("getInetAddress error ! {}", e.getMessage());
        }
        return ipAddress;
    }

    public static void initIfengAddress(Context context, final String item) {
        final IfengAddress address = new IfengAddress();
        address.item = item;
        address.startTime = System.currentTimeMillis() + "";
        address.netstatus = NetUtils.getNetType(context);

        new Thread(new Runnable() {
            @Override
            public void run() {
                lastIp = getUrlIp(item);
                address.netstatus = address.netstatus + "_" + lastIp;
            }
        }).start();

        if (UserFeed.ifengAddress.size() > 4) {
            UserFeed.ifengAddress.remove(0);
        }
        UserFeed.ifengAddress.add(address);
    }

    @SuppressWarnings("unchecked ")
    private void getDns(final CallBack callBack) {
        String url = DataInterface.USER_FEED_BACK_DNS_URL;
        RequestQueue requestQueue = VolleyHelper.getRequestQueue();
        RequestString requestString = new RequestString(Request.Method.GET, url, null
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                FeedDns feedDns = null;
                if (!TextUtils.isEmpty(response)) {
                    try {
                        feedDns = JSON.parseObject(response, FeedDns.class);
                        setDNS(feedDns);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    } finally {
                        callBack.callBack();
                    }

                }
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    logger.error(error.toString(), error);
                }
            }
        });
        requestQueue.add(requestString);
    }

    private void setDNS(FeedDns fd) {
        this.DNS = new String(fd.DNS_RESOLVE);
    }

    public static void cacheIfengAddress(Context context) {
        String ifengStr = "";
        try {
            ifengStr = JSON.toJSONString(ifengAddress);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        SharedPreferences sp = context.getSharedPreferences(USER_FEED_SP, Context.MODE_PRIVATE);
        sp.edit().putString(USER_FEED_STR, ifengStr).commit();
    }

    public static void getIfengAddress(Context context) {
        SharedPreferences sp = context.getSharedPreferences(USER_FEED_SP, Context.MODE_PRIVATE);
        String result = sp.getString(USER_FEED_STR, "");
        List<IfengAddress> list = null;
        if (!TextUtils.isEmpty(result)) {
            try {
                list = JSON.parseArray(result, UserFeed.IfengAddress.class);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        } else {
            list = null;
        }

        if (list != null) {
            ifengAddress = list;
        }
    }

    public static void cleanIfengAddress() {
        SharedPreferences sp = IfengApplication.getInstance().getSharedPreferences(USER_FEED_SP, Context.MODE_PRIVATE);
        sp.edit().clear();
        ifengAddress.clear();
    }

    /**
     * 发送下载失败反馈
     *
     * @param downLoadInfo 下载失败的信息
     */
    public static void sendDownLoadFailInfo(String downLoadInfo) {
        uf = new UserFeed();
        uf.syncFillParams(downLoadInfo, "", CLIENT_TYPE_VIDEO_ERR, new UserFeed.CallBack() {
            @Override
            public void callBack() {
                String json = JSON.toJSONString(uf);
                requestStatisticsUrl(json);
            }
        });
    }

    @SuppressWarnings("unchecked ")
    private static void requestStatisticsUrl(String json) {
        if (json == null) {
            return;
        }
        String url = DataInterface.USER_FEED_BACK_STATISTIC_URL;
        RequestQueue requestQueue = VolleyHelper.getRequestQueue();
        JsonRequest jsonRequest = new JsonRequest(Request.Method.POST, url, json,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        logger.debug("UserFeedback onResponse  {}", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("UserFeedback onErrResponse Exception! {}", error);
                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                try {
                    String responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                }
            }
        };
        requestQueue.add(jsonRequest);
    }
}
/*
Json 格式为：

{
"content" : "yangzhj" ,
"clientPlatform" : "客户端平台" ,
"clientVersion" : "客户端版本号" ,
"phoneType" : "手机型号" ,
"phoneSystem" : "手机系统名称" ,
"phoneVersion" : "手机系统版本" ,
"network" : "联网方式wifi、2G、3G" ,
"netname" : "运营商" ,
"contact" : "用户联系方式" ,
"clientType" : "videoClient" ,  // errvideoClient
"lastItemIp" : "192.168.1.107" ,
"DNS" : "114.114.114.114" ,
"vIfengAddress" : [
{
"item" :
"http://v.ifeng.com/news/world/2014005/01fc6747-f615-41bc-b06f-a40ca2e8c3a8.shtml"
,
"startTime" : "1400597855799" ,
"netstatus" : "2G/3G/4G/wifi"
},
{
"item" : "http://v.ifeng.com/program/supermami/index.shtml" ,
"startTime" : "1400597855799" ,
"netstatus" : "2G/3G/4G/wifi"
},
{
"item" :
"http://v.ifeng.com/ent/yllbt/special/20140520/index.shtml#0181eb4c-bb13-43c3-942c-72d25873e4cf"
,
"startTime" : "1400597855799" ,
"netstatus" : "2G/3G/4G/wifi"
},
{
"item" : "http://v.ifeng.com/v/vblog/2014may/index.shtml" ,
"startTime" : "1400597855799" ,
"netstatus" : "2G/3G/4G/wifi"
},
{
"item" : "http://v.ifeng.com/ent/special/cinema/c124/index.shtml" ,
"startTime" : "1400597855799" ,
"netstatus" : "2G/3G/4G/wifi"
}
]
}*/
