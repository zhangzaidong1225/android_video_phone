package com.ifeng.newvideo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase.Mode;

/**
 * 该Layout用于处理UI界面状态展示
 * Created by Administrator on 2016/8/18.
 */
public class UIStatusLayout extends RelativeLayout implements View.OnClickListener {
    private static final Logger logger = LoggerFactory.getLogger(UIStatusLayout.class);

    public static final int LOADING = 1;//加载
    public static final int NORMAL = 2;//正常
    public static final int ERROR = 3;//错误(接口请求失败，数据解析错误)
    public static final int NO_NET = 4;//无网
    public static final int ALL_GONE = 5;//全部隐藏

    private View loading;
    private View error;
    private View noNet;
    private TextView loadingText;
    private PullToRefreshListView listView;
    private Mode mode = Mode.PULL_FROM_END;//默认模式为上拉加载更多

    public UIStatusLayout(Context context) {
        this(context, null);
    }

    public UIStatusLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UIStatusLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.common_ui_status_layout, this, true);
        loading = findViewById(R.id.loading_layout);
        loadingText = (TextView) loading.findViewById(R.id.loading_text);
        error = findViewById(R.id.load_fail_layout);
        error.setOnClickListener(this);
        noNet = findViewById(R.id.no_net_layer);
        noNet.setOnClickListener(this);
        listView = (PullToRefreshListView) findViewById(R.id.listView);
        //判断ListView是否可以进行上拉加载更多，如果数据一屏幕没展示完，则可以上拉加载
        listView.getRefreshableView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount > visibleItemCount) {
                    listView.setMode(mode);
                } else {
                    switch (mode) {
                        case DISABLED:
                        case PULL_FROM_END:
                            listView.setMode(Mode.DISABLED);
                            break;
                        case BOTH:
                        case PULL_FROM_START:
                            listView.setMode(Mode.PULL_FROM_START);
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.load_fail_layout:
            case R.id.no_net_layer:
                if (request != null) {
                    request.requestData();
                }
                break;
        }
    }

    /**
     * 设置PullToRefresh模式
     */
    public void setPullToRefreshMode(Mode mode) {
        this.mode = mode;
    }

    /**
     * 设置LoadingView文字
     */
    public void setLoadingText(int resId) {
        loadingText.setText(this.getContext().getString(resId));
    }

    public void setLoadingText(String text) {
        loadingText.setText(text);
    }

    /**
     * 根据状态显示相应的布局
     *
     * @param status 状态
     */
    public void setStatus(int status) {
        switch (status) {
            case LOADING:
                loading.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                break;
            case NORMAL:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                break;
            case ERROR:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                noNet.setVisibility(View.GONE);
                break;
            case NO_NET:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.VISIBLE);
                break;
            case ALL_GONE:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                break;
        }
    }

    public PullToRefreshListView getListView() {
        return listView;
    }

    private RequestData request;

    public void setRequest(RequestData request) {
        this.request = request;
    }
}
