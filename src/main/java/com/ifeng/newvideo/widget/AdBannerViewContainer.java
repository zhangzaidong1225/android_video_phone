package com.ifeng.newvideo.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.util.AdsExposureSesssion;

/**
 * 播放底页 信息流banner广告 view
 * Created by ll on 2017/5/5.
 */
public class AdBannerViewContainer extends RelativeLayout {
    public AdBannerViewContainer(Context context) {
        super(context);
    }

    public AdBannerViewContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdBannerViewContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public AdBannerViewContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void sendExposure(MainAdInfoModel.AdMaterial adMaterial) {
        Rect localRect = new Rect();
        boolean localVisibility = getLocalVisibleRect(localRect);
        if (!AdsExposureSesssion.getInstance().containsADItemRecord(adMaterial.getAdId())) {
            if (localVisibility) {
                AdsExposureSesssion.getInstance().addADItemRecord(adMaterial.getAdId());
                AdvertExposureDao.addIfengAdvExposureForChannel(adMaterial.getAdId(), null,
                        adMaterial.getAdAction().getPvurl(), adMaterial.getAdAction().getAdpvurl());
            }
        } else {
            if (!localVisibility) {
                AdsExposureSesssion.getInstance().removeADItemRecord(adMaterial.getAdId());
            }
        }

    }
}
