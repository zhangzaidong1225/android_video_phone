package com.ifeng.newvideo.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.newvideo.videoplayer.bean.ADvertItem;
import com.ifeng.video.core.net.VolleyHelper;


public class AdvertViewContainer extends LinearLayout {

    private TextView mTitle, mAd_des, mTag, mDownload;
    private NetworkImageView mRight_pic;

    public AdvertViewContainer(Context context) {
        this(context, null);
    }

    public AdvertViewContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdvertViewContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.ad_mix_text_pic, this, true);
        mTitle = (TextView) findViewById(R.id.tv_left_title);
        mAd_des = (TextView) findViewById(R.id.tv_ad_desc);
        mTag = (TextView) findViewById(R.id.tv_ad_tag);
        mDownload = (TextView) findViewById(R.id.tv_ad_download);
        mRight_pic = (NetworkImageView) findViewById(R.id.niv_right_picture);
    }

    protected void configAdMixTextPicConvertView(final View convertView, final ADvertItem advertItem) {

        if ("adapp".equalsIgnoreCase(advertItem.getClickType())) {
            mDownload.setVisibility(View.VISIBLE);
            if (mDownload.getParent() instanceof View) {
                ((View) mDownload.getParent()).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 相应的下载地址
                        DownLoadUtils.download(IfengApplication.getAppContext(), advertItem.getAppId(), advertItem.getAppUrl(),
                                null, null);
                    }
                });
            }
        } else {
            mDownload.setVisibility(View.GONE);
            if (mDownload.getParent() != null) {
                mDownload.setOnClickListener(null);
            }
        }
        if (TextUtils.isEmpty(advertItem.getTitle())) {
            mTitle.setVisibility(View.GONE);
        } else {
            mTitle.setText(advertItem.getTitle());
            mTitle.setVisibility(View.VISIBLE);
        }
        setImageUrl(mRight_pic, advertItem.getImage(), R.drawable.bg_default_mid);
        mAd_des.setVisibility(View.GONE);
        mTag.setText(TextUtils.isEmpty(advertItem.getTag()) ? "广告" : advertItem.getTag());
    }

    protected void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
    }

    public void updateView(final ADvertItem advertItem) {
        setTag(advertItem);
        configAdMixTextPicConvertView(this, advertItem);

    }

}
