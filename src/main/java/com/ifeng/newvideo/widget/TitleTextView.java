package com.ifeng.newvideo.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.fontsOverride.TypefaceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

public class TitleTextView extends TextView {

    private final Logger logger = LoggerFactory.getLogger(TitleTextView.class);
    private final float textSize;
    private final Paint paint = new Paint();
    private float maxWidth;
    private final float LineSpacing = 1.1f;
    private int maxLine;
    private float dotWidth = 0;
    private int line;
    private int lineCount = 1;

    public TitleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        textSize = super.getTextSize();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.common_TitleTextView);
        maxLine = a.getInt(R.styleable.common_TitleTextView_maxLines, Integer.MAX_VALUE);
        boolean singleLine = a.getBoolean(R.styleable.common_TitleTextView_singleLine, false);
        // FIXME 文字为 “网红谈 Web Red” 时，显示有问题，加右padding临时解决一下
        setPadding(0, 0, 1, 0);

        if (singleLine) {
            maxLine = 1;
        }
        a.recycle();
        paint.setTextSize(textSize);
        paint.setColor(super.getCurrentTextColor());
        paint.setAntiAlias(true);

        if (Build.MANUFACTURER.toLowerCase(Locale.US).contains(getContext().getString(R.string.manufacturer_xiaomi))) {
            Typeface mFont = TypefaceUtils.load(getContext().getAssets(), getContext().getString(R.string.font_for_xiaomi));
            paint.setTypeface(mFont);
        }


        dotWidth = paint.measureText(".");
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int endIndex = 0;
        maxWidth = getMeasuredWidth();
        lineCount = 0;
        String text = this.getText().toString();
        if (TextUtils.isEmpty(text)) {
            return;
        }

        char[] textCharArray = text.toCharArray();
        float drawedWidth = 0;
        float charWidth;
        endIndex = measureTotalWidth(textCharArray);
        draw:
        for (int i = 0; i < textCharArray.length; i++) {
            charWidth = paint.measureText(textCharArray, i, 1);
            if (textCharArray[i] == '\n') {
                lineCount++;
                drawedWidth = 0;
                continue;
            }
            if (maxWidth - drawedWidth < charWidth) {
                lineCount++;
                drawedWidth = 0;
            }

            if (endIndex != 0 && i >= endIndex) {
//                canvas.drawText(textCharArray, i, 1, drawedWidth, (lineCount + 1) * textSize * LineSpacing, paint);
                canvas.drawText("...".toCharArray(), 0, 3, drawedWidth, (lineCount + 1) * textSize * LineSpacing, paint);
                canvas.save();
                break draw;
            }
            // 第一行不要上间距，第二、三...行要上间距
            if (lineCount == 0) {
                float firstLineY = (lineCount + 1) * textSize * 1;
                canvas.drawText(textCharArray, i, 1, drawedWidth, firstLineY, paint);
            } else {
                float otherLineY = (lineCount + 1) * textSize * LineSpacing;
                canvas.drawText(textCharArray, i, 1, drawedWidth, otherLineY, paint);
            }
            drawedWidth += charWidth;
        }
    }

    @Override
    public void setHeight(int pixels) {
        super.setHeight(pixels);
        if (line > 0) {
            setHeight((int) ((line) * (int) textSize * LineSpacing + 10));
        } else {
            setHeight((int) ((lineCount + 1) * (int) textSize * LineSpacing + 10));
        }
    }

    public void setText(String text) {
        super.setText(text);
        resetLines();
        invalidate();
    }


    private int measureTotalWidth(char[] arr) {
        int lineCount = 0;
        float drawedWidth = 0;
        float charWidth;

        for (int i = 0; i < arr.length; i++) {
            charWidth = paint.measureText(arr, i, 1);
            if (arr[i] == '\n') {
                lineCount++;
                drawedWidth = 0;
                continue;
            }

            if (maxWidth - drawedWidth < charWidth) {
                lineCount++;
                drawedWidth = 0;
                if (lineCount >= maxLine) {
                    // if (i != arr.length - 1) {
                    float restWidth = (maxWidth - drawedWidth);
                    float needWidth = dotWidth * 3;
                    float singleWidth = paint.measureText(arr, i - 1, 1);
                    restWidth += singleWidth;
                    if (restWidth > needWidth) {
                        return i - 1;
                    } else {
                        float nextTWidth = paint.measureText(arr, i - 2, 1);
                        restWidth += nextTWidth;
                        if (restWidth > needWidth) {
                            return i - 2;
                        } else {
                            return i - 3;
                        }
                    }
                    // }
                }
            }
            drawedWidth += charWidth;
        }
        return 0;
    }

    /**
     * 自定义的TextView 改写了onDraw此处直接设置画笔颜色
     */
    public void setTextColor(int color) {
        paint.setColor(color);
        invalidate();
    }

    /**
     * 设置为单行显示
     */
    public void setSingleLine() {
        maxLine = 1;
    }

    /**
     * 调用该方法，如果在ListView中的复用机制中使用了，请确保在调用resetLines方法重置状态
     */
    public void setLines(int lines) {
        line = lines;
        invalidate();
    }

    private void resetLines() {
        line = -1;
    }

}
