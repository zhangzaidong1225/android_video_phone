package com.ifeng.newvideo.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by android-dev on 2016/10/17.
 */
public class CustomRecyclerView extends RecyclerView {
    private float downX = 0, downY = 0;

    public CustomRecyclerView(Context context) {
        super(context);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        LayoutManager layoutManager = getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            int action = e.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    downX = e.getX();
                    downY = e.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    final float moveX = e.getX();
                    final float moveY = e.getY();
                    if (Math.abs(moveX - downX) > Math.abs(moveY - downY)) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                        final int first = linearLayoutManager.findFirstVisibleItemPosition();
                        final int last = linearLayoutManager.findLastVisibleItemPosition();
                        if (first == 0 && moveX - downX > 0 && getChildAt(0) != null && getChildAt(0).getLeft() >= 0) {
                            return false;
                        } else if (null != getAdapter() && getAdapter().getItemCount() - 1 <= last && downX - moveX > 0 &&
                                getChildAt(last - first) != null && getChildAt(last - first).getRight() <= getWidth()) {
                            return false;
                        }
                        break;
                    }
            }
        }
        return super.onInterceptTouchEvent(e);
    }
}
