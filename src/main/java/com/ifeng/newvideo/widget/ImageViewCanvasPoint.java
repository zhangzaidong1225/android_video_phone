package com.ifeng.newvideo.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.DisplayUtils;

/**
 * 轮播图的左下角导航小圆点控件
 */
public class ImageViewCanvasPoint extends ImageView {
    /**
     * 所有圆点的宽
     */
    private int canvasLocWidth;

    /**
     * 所有圆点的高
     */
    private int canvasLocHeight;

    /**
     * 焦点图的position
     */
    private int pos;

    /**
     * 焦点图数量
     */
    private int size;

    /**
     * 圆点间的距离
     */
    private int offset = 0;

    /**
     * 圆点的半径
     */
    private float radius = 6f;

    /**
     * 圆点
     */
    private int m = 0;

    /**
     * 红色paint
     */
    private final Paint grayPaint = new Paint();

    /**
     * 白色paint
     */
    private final Paint whitePaint = new Paint();
    private static final int MAX_SCREEN_WIDTH = 1080;//大屏的宽度
    private static final int MID_SCREEN_WIDTH = 720;//中等屏的宽度

    public ImageViewCanvasPoint(Context context) {
        super(context);
    }

    public ImageViewCanvasPoint(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewCanvasPoint(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setData(int pos, int size) {
        this.pos = pos;
        this.size = size;
    }

    {
        grayPaint.setAntiAlias(true);
        grayPaint.setColor(getResources().getColor(R.color.home_gray_point));
        grayPaint.setStrokeWidth((float) 5.0);
        whitePaint.setAntiAlias(true);
        whitePaint.setColor(Color.WHITE);
        whitePaint.setStrokeWidth((float) 5.0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initOffset() {
        canvasLocWidth = getWidth();// - DisplayUtils.convertDipToPixel( 4);
        canvasLocHeight = getHeight() * 4 / 5;
        offset = DisplayUtils.convertDipToPixel(3);

        if (size >= 6) {
            m = offset * 2;
        } else if (size >= 4) {
            m = offset * 3;
        } else {
            m = offset * 4;
        }
        //2dp
        radius = DisplayUtils.convertDipToPixel(2) + 1;


        if (DisplayUtils.getWindowWidth() >= MAX_SCREEN_WIDTH) {
            //radius = 6f;
            m = offset * 4;
        } else if (DisplayUtils.getWindowWidth() >= MID_SCREEN_WIDTH) {
            //radius = 4f;
            m = offset * 3;
        } else {
            //radius = 2f;
            canvasLocWidth = getWidth();// - DisplayUtils.convertDipToPixel(, 3);
        }

    }

    @Override
    public void draw(Canvas canvas) {
        if (offset == 0) {
            initOffset();
        }
        // 所有点的总宽度
        float totalWidth = (size - 1) * m;
        int[] location = new int[2];
        getLocationOnScreen(location);
        float x = canvasLocWidth / 2 + totalWidth / 2;
        float y = canvasLocHeight;
        for (int i = size - 1; i >= 0; i--) {
            if (i == pos) {
                canvas.drawCircle(x, y, radius, whitePaint);
            } else {
                canvas.drawCircle(x, y, radius, grayPaint);
            }
            x = x - m;
        }
        setContentDescription(size + "," + pos);
        super.draw(canvas);
    }
}
