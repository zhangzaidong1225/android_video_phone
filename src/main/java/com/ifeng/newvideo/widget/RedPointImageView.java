package com.ifeng.newvideo.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Looper;
import android.util.AttributeSet;
import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by guolc on 2016/7/20.
 */
public class RedPointImageView extends NetworkImageView{
    private Paint mPaint = new Paint();

    private boolean showRedPoint = true;

    public boolean isShowRedPoint() {
        return showRedPoint;
    }

    public void setShowRedPoint(boolean showRedPoint) {
        this.showRedPoint = showRedPoint;
    }

    public RedPointImageView(Context context) {
        this(context, null);
    }

    public RedPointImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RedPointImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public RedPointImageView(Context context, boolean showRedPoint) {
        this(context, null);
        this.showRedPoint = showRedPoint;
        init();
    }

    private void init() {
        mPaint.setColor(Color.RED);
        mPaint.setStrokeWidth(55);
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (showRedPoint) {
            // 2小了，3大了 int r = DisplayUtils.convertDipToPixel(getContext(), 3);
            float density = getContext().getResources().getDisplayMetrics().densityDpi / 160f;
            int r = (int) (2.5 * density + 0.5);
            canvas.drawCircle(getWidth() * 0.71f, getHeight() * 0.31f, r, mPaint);
        }
    }

    public void showRedPoint() {
        showRedPoint = true;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            invalidate();
            return;
        }
        postInvalidate();
    }

    public void hideRedPoint() {
        showRedPoint = false;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            invalidate();
            return;
        }
        postInvalidate();
    }
}
