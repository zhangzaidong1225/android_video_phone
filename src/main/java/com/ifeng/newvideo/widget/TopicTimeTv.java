package com.ifeng.newvideo.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.DisplayUtils;


/**
 * 视频时长View
 */
public class TopicTimeTv extends TextView {
    private static int screenHeight;
    private static int viewWidth, viewHeight;

    private static final float ratio = 6.4f;
    private static final int ID = 0x30011;

    public TopicTimeTv(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public TopicTimeTv(Context context) {
        super(context);
        if (viewWidth == 0) {
            int screenWidth = DisplayUtils.getWindowWidth();
            screenHeight = DisplayUtils.getWindowHeight();
            viewWidth = (int) ((1080 - 92) * screenWidth / 1080 / 2 / 3.5);
            viewHeight = (int) (ShortVideoImageView.viewHeight / ratio);
        }
        initLayoutParams();
        setId(ID);
    }

    private void initLayoutParams() {
        setPadding(0, 0, viewWidth / 10, 0);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, screenHeight * 34 / 1960);
        setGravity(Gravity.CENTER);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(viewWidth, viewHeight);
        params.gravity = Gravity.BOTTOM | Gravity.LEFT;
        setLayoutParams(params);
        setTextColor(getResources().getColor(R.color.common_ifengwhite));
        setBackgroundResource(R.drawable.player_bg_tv_longvideo_time);
    }

    public void setTimeText(String text) {
        if (!TextUtils.isEmpty(text)) {
            if (text.length() <= 5) {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (viewWidth / 3.5));
            } else {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (viewWidth / 5));
            }
        }
        setText(text);
    }
}
