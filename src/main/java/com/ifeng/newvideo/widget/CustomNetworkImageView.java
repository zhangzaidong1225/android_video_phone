package com.ifeng.newvideo.widget;

import android.content.Context;
import android.util.AttributeSet;
import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by guolc on 2016/8/10.
 */
public class CustomNetworkImageView extends NetworkImageView {

    public CustomNetworkImageView(Context context) {
        this(context,null,0);
    }

    public CustomNetworkImageView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public CustomNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width=MeasureSpec.getSize(widthMeasureSpec);
        int height= (int) (width*9f/16);
        int mHeightMeasureSpec=MeasureSpec.makeMeasureSpec(height,MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, mHeightMeasureSpec);
    }
}
