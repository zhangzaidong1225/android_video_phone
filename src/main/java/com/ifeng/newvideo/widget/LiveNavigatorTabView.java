package com.ifeng.newvideo.widget;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * for liveFragment 有台标image
 * Created by antboyqi on 14-9-10.
 */
public class LiveNavigatorTabView extends LinearLayout {

    private static final Logger logger = LoggerFactory.getLogger(LiveNavigatorTabView.class);
    private final ImageView imageView;
    private final TextView textView;

    public LiveNavigatorTabView(Context context) {
        super(context);
        inflate(getContext(), R.layout.live_channel_navigator_lay, this);
        imageView = (ImageView) findViewById(R.id.live_channel_img);
        textView = (TextView) findViewById(R.id.live_channel_name);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTextView() {
        return textView;
    }
}
