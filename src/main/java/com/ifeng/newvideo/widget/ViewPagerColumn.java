package com.ifeng.newvideo.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 主要是通过写自定义的方法，来实现Viewpager的滑动禁止
 * Created by Vincent on 2014/9/30.
 */
public class ViewPagerColumn extends ViewPager {

    private static final Logger logger = LoggerFactory.getLogger(ViewPagerColumn.class);
    private boolean isCanScroll = true;

    public ViewPagerColumn(Context context) {
        super(context);
    }

    public ViewPagerColumn(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        try {
            return isCanScroll && super.onInterceptTouchEvent(arg0);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent arg0) {
        try {
            return isCanScroll && super.onTouchEvent(arg0);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}