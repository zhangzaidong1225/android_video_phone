package com.ifeng.newvideo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.android.volley.toolbox.NetworkImageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 可以设定宽高比例的ImageView
 * 以保证最大尺寸的情况下，保持设定好的比例
 * Created by cuihz on 2014/10/21.
 */
public class RatioImageView extends NetworkImageView {

    private static final Logger logger = LoggerFactory.getLogger(RatioImageView.class);

    private double ratio;

    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(ratio <= 0){
            return;
        }
        double mRatio = (double) w / h;
        if (mRatio > ratio) {
            w = (int) (h * ratio);
        } else {
            h = (int) ((double) w / ratio);
        }
        ViewGroup.LayoutParams params = this.getLayoutParams();
        params.width = w;
        params.height = h;
    }

}
