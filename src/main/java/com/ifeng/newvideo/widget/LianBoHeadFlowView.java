package com.ifeng.newvideo.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.adapter.LianBoHeaderViewPagerAdapter;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

/**
 * 轮播图控件，包括下面的导航小圆点
 */
public class LianBoHeadFlowView extends LinearLayout {

    public HeaderFlowViewPager headerFlowViewPager;
    private TextView tv_title;
    private TextView tv_tag;
    private ImageViewCanvasPoint imageViewCanvasPoint;
    private final int TEXT_MAX_NUM = 17;

    public LianBoHeadFlowView(Context context) {
        super(context);
    }

    public LianBoHeadFlowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LianBoHeadFlowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        headerFlowViewPager = (HeaderFlowViewPager) findViewById(R.id.headerFlowViewPager);
        imageViewCanvasPoint = (ImageViewCanvasPoint) findViewById(R.id.iv_points);
        tv_title = (TextView) findViewById(R.id.header_title);
        tv_tag = (TextView) findViewById(R.id.header_tag);
        float ration = 0.5625f;
        int width;
        if (DisplayUtils.getWindowWidth() < DisplayUtils.getWindowHeight()) {
            width = DisplayUtils.getWindowWidth();
        } else {
            width = DisplayUtils.getWindowHeight();
        }
        headerFlowViewPager.setLayoutParams(new FrameLayout.LayoutParams(width, (int) (width * ration)));
    }

    public void setViewPagerAdapter(PagerAdapter pagerAdapter) {
        if (pagerAdapter instanceof LianBoHeaderViewPagerAdapter) {
            ((LianBoHeaderViewPagerAdapter) pagerAdapter).onDataChanged = new OnDataChangedListenerImp();
        }
        headerFlowViewPager.setAdapter(pagerAdapter);
    }

    public void setCurrentItem(int position) {
        headerFlowViewPager.setCurrentItem(position, false);
    }

    private class OnDataChangedListenerImp implements LianBoHeaderViewPagerAdapter.OnDataChanged {

        @Override
        public void onDataChanged(HomePageBeanBase header, int size, int pos) {
            if (header == null) {
                return;
            }
            setTitleText(header);
            setTagText(header);
            setPointView(size, pos);
        }
    }

    public void setPointView(int size, int pos) {
        if (size > 1) {
            imageViewCanvasPoint.setData(pos, size);
            imageViewCanvasPoint.postInvalidate();
        } else {
            imageViewCanvasPoint.setData(0, 0);
            imageViewCanvasPoint.postInvalidate();
        }
    }

    private void setTagText(HomePageBeanBase header) {
        String tag = TagUtils.getTagTextForList(header.getTag(), header.getMemberType(),
                header.getMemberItem().getClickType());

        if (CheckIfengType.isAdBackend(header.getMemberType())) {
            boolean isShowAdIcon = header.getMemberItem().icon.showIcon == 1;
            tag = isShowAdIcon ? header.getMemberItem().icon.text : "";
        }

        if (TextUtils.isEmpty(tag)) {
            tv_tag.setVisibility(View.GONE);
        } else {
            tv_tag.setText(tag);
            tv_tag.setVisibility(View.VISIBLE);
        }
    }

    private void setTitleText(HomePageBeanBase header) {
        if (header != null) {
            tv_title.setText(header.getTitle());
            // tv_title.setBackgroundResource(R.drawable.shadow_player_top);
        }
    }
}
