/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ifeng.newvideo.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.fontsOverride.CalligraphyUtils;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.FragmentHomePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * 频道View
 */
@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class PagerSlidingTabStrip extends HorizontalScrollView {
    private final String TAG = this.getClass().getSimpleName();

    private static final Logger logger = LoggerFactory.getLogger(PagerSlidingTabStrip.class);

    public interface IconTabProvider {
        void initIcon(LiveNavigatorTabView liveNavigatorTabView, int position);
    }

    // @formatter:off
    private static final int[] ATTRS = new int[]{android.R.attr.textSize, android.R.attr.textColor};
    // @formatter:on

    private final LinearLayout.LayoutParams defaultTabLayoutParams;
    private final LinearLayout.LayoutParams expandedTabLayoutParams;

    private final PageListener pageListener = new PageListener();
    private OnPageChangeListener delegatePageListener;

    private final LinearLayout tabsContainer;
    private ViewPager pager;

    private int tabCount;

    private int currentPosition = 0;
    private int currentSelectPosition = 0;//只用于直播fragment跳转（非滑动）时判断当前position
    private float currentPositionOffset = 0f;

    private final Paint rectPaint;

    private int indicatorColor = 0xFF666666;
    private int underlineColor = 0xCCCCCC; // 0x1A000000
    private int tabTextColorNormal = 0x363636; // 0x9a9a9a; 加深文字颜色
    private int tabTextColorSelected = 0xF54343;

    private int dividerColor = 0x1A000000;

    private boolean shouldExpand = false;
    private boolean textAllCaps = true;

    private int currentDividerWidth = 0;
    private int mScrollOffset = 0;
    private int indicatorHeight = 8;
    private int underlineHeight = 1;
    private int dividerPadding = 12;

    private int tabPaddingLeftRight = 24;
    private int tabPaddingVertical = 15;
    private final int tabPaddingTop = 5;
    private final int tabPaddingBottom = 5;

    private int expandViewWidth = 20;
    // private int dividerWidth = 1;
    private int dividerWidth = 0; // if you dont want a divider set its width to zero

    private int tabTextSize = 17;
    private int tabTextColor = 0xF54343; // 加深文字颜色
    private Typeface tabTypeface = null;

    // private int tabTypefaceStyle = Typeface.BOLD;
    private int tabTypefaceStyle = Typeface.NORMAL;

    private int lastScrollX = 0;

    private int tabBackgroundResId = R.drawable.background_tab;

    private Locale locale;

    public PagerSlidingTabStrip(Context context) {
        this(context, null);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOverScrollMode(OVER_SCROLL_NEVER);
        setHorizontalScrollBarEnabled(false);
        setHorizontalFadingEdgeEnabled(false);
        setFillViewport(true);
        setWillNotDraw(false);

        tabsContainer = new LinearLayout(context);
        tabsContainer.setOrientation(LinearLayout.HORIZONTAL);
        tabsContainer.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        tabsContainer.setGravity(Gravity.CENTER_VERTICAL); // 纵向居中
        addView(tabsContainer);

        DisplayMetrics dm = getResources().getDisplayMetrics();

        //scrollOffset = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, scrollOffset, dm);
        mScrollOffset = dm.widthPixels / 2;
        indicatorHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, indicatorHeight, dm);
        underlineHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, underlineHeight, dm);
        dividerPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dividerPadding, dm);
        tabPaddingLeftRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, tabPaddingLeftRight, dm);

        // add by hangl begin
        tabPaddingVertical = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, tabPaddingVertical, dm);
        // add by hangl end

        dividerWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dividerWidth, dm);
        tabTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, tabTextSize, dm); // change sp to dp

        // get system attrs (android:textSize and android:textColor)

        TypedArray a = context.obtainStyledAttributes(attrs, ATTRS);
        tabTextSize = a.getDimensionPixelSize(0, tabTextSize);
        tabTextColor = a.getColor(1, tabTextColor);
        a.recycle();

        // get custom attrs

        a = context.obtainStyledAttributes(attrs, R.styleable.PagerSlidingTabStrip);

        indicatorColor = a.getColor(R.styleable.PagerSlidingTabStrip_pstsIndicatorColor, indicatorColor);
        underlineColor = a.getColor(R.styleable.PagerSlidingTabStrip_pstsUnderlineColor, underlineColor);
        dividerColor = a.getColor(R.styleable.PagerSlidingTabStrip_pstsDividerColor, dividerColor);

        // add by hangl begin
        tabTextColorNormal = a.getColor(R.styleable.PagerSlidingTabStrip_pstsTextNormalColor, tabTextColorNormal);
        tabTextColorSelected = a.getColor(R.styleable.PagerSlidingTabStrip_pstsTextSelectedColor, tabTextColorSelected);
        tabPaddingVertical = a.getDimensionPixelOffset(R.styleable.PagerSlidingTabStrip_pstsTabPaddingVertical, tabPaddingVertical);
        expandViewWidth = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstsExpandViewWidth, expandViewWidth);
        // add by hangl end

        indicatorHeight = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstsIndicatorHeight, indicatorHeight);
        underlineHeight = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstsUnderlineHeight, underlineHeight);
        dividerPadding = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstsDividerPadding, dividerPadding);
        tabPaddingLeftRight = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstsTabPaddingLeftRight, tabPaddingLeftRight);
        tabBackgroundResId = a.getResourceId(R.styleable.PagerSlidingTabStrip_pstsTabBackground, tabBackgroundResId);
        shouldExpand = a.getBoolean(R.styleable.PagerSlidingTabStrip_pstsShouldExpand, shouldExpand);
        mScrollOffset = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstsScrollOffset, mScrollOffset);
        textAllCaps = a.getBoolean(R.styleable.PagerSlidingTabStrip_pstsTextAllCaps, textAllCaps);

        a.recycle();

        rectPaint = new Paint();
        rectPaint.setAntiAlias(true);
        rectPaint.setStyle(Style.FILL);

        Paint dividerPaint = new Paint();
        dividerPaint.setAntiAlias(true);
        dividerPaint.setStrokeWidth(dividerWidth);

        defaultTabLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        expandedTabLayoutParams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1.0f);

        if (locale == null) {
            locale = getResources().getConfiguration().locale;
        }
    }

    public void setViewPager(ViewPager pager) {
        this.pager = pager;

        if (pager.getAdapter() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }

        pager.setOnPageChangeListener(pageListener);

        notifyDataSetChanged();

    }

    public void setOnPageChangeListener(OnPageChangeListener listener) {
        this.delegatePageListener = listener;
    }

    public void notifyDataSetChanged() {

        tabsContainer.removeAllViews();

        tabCount = pager.getAdapter().getCount();

        for (int i = 0; i < tabCount; i++) {
            if (pager.getAdapter() instanceof IconTabProvider) {
                LiveNavigatorTabView liveNavigatorTabView = addIconTab(i);
                ((IconTabProvider) pager.getAdapter()).initIcon(liveNavigatorTabView, i);
                layoutImageTab(liveNavigatorTabView);
            } else {
                addTextTab(i, pager.getAdapter().getPageTitle(i).toString());
            }
        }

        // add by hangl
        if (!shouldExpand && expandViewWidth != 0) {
            ImageView spaceView = new ImageView(getContext());
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(expandViewWidth, LinearLayout.LayoutParams.MATCH_PARENT);
            tabsContainer.addView(spaceView, tabCount, llp);
        }

        updateTabStyles();

        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                currentPosition = pager.getCurrentItem();
                scrollToChild(currentPosition, 0);
            }
        });
        selectTab(pager.getCurrentItem()); // focus current selected tab
    }

    private void addTextTab(final int position, String title) {
        TextView tab = new TextView(getContext());
        tab.setContentDescription(title);
        tab.setText(title);
        tab.setGravity(Gravity.CENTER);
        tab.setSingleLine();
        tab.setTextColor(tabTextColorNormal); // add by hangl
        addTab(position, tab);
    }

    private LiveNavigatorTabView addIconTab(final int position) {
        LiveNavigatorTabView tab = new LiveNavigatorTabView(getContext());
        tab.setFocusable(true);
        tab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(position);
            }
        });
        tabsContainer.addView(tab, position, shouldExpand ? expandedTabLayoutParams : defaultTabLayoutParams);
        return tab;
    }

    private void addTab(final int position, View tab) {
        tab.setFocusable(true);
        tab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (pager.getAdapter() instanceof FragmentHomePage.ViewPagerAdapterMain) {
                    FragmentHomePage.ViewPagerAdapterMain adapter = (FragmentHomePage.ViewPagerAdapterMain) pager.getAdapter();
                    String channelId = adapter.getChannelIdByPosition(position);
                    if (position == getCurrentPosition() && !TextUtils.isEmpty(channelId)) {
                        // 频道点击刷新当前页面数据
                        adapter.refreshCurrentFragmentData(false);
                        // 频道点击action统计，点击当前item时上报
                        PageActionTracker.clickCh();

                        logger.debug("channelId={}  title={}  position={}", channelId, pager.getAdapter().getPageTitle(position), position);
                    }
                }

                pager.setCurrentItem(position, false);
            }
        });
        tab.setPadding(tabPaddingLeftRight, tabPaddingTop, tabPaddingLeftRight, tabPaddingBottom);
        tabsContainer.addView(tab, position, shouldExpand ? expandedTabLayoutParams : defaultTabLayoutParams);
    }

    private void updateTabStyles() {
        for (int i = 0; i < tabCount; i++) {
            View v = tabsContainer.getChildAt(i);
            v.setBackgroundResource(tabBackgroundResId);
            if (v instanceof TextView) {
                TextView tab = (TextView) v;
                tab.setTextSize(TypedValue.COMPLEX_UNIT_PX, tabTextSize);
                tab.setTypeface(tabTypeface, tabTypefaceStyle);
                tab.setTextColor(tabTextColor);
                tab.setText(tab.getText().toString());

                CalligraphyUtils.applyFontToTextView(getContext(), tab, getContext().getString(R.string.font_for_xiaomi));

//                if (textAllCaps) {
                // setAllCaps() is only available from API 14, so the upper case is made manually if we are on a pre-ICS-build
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
//                        tab.setAllCaps(true);
//                    } else {
//                        tab.setText(tab.getText().toString().toUpperCase(locale));
//                    }
//                }
            }
        }
    }

    private void scrollToChild(int position, int offset) {
        if (tabCount == 0) {
            return;
        }
        int newScrollX = tabsContainer.getChildAt(position).getLeft() + offset;
        int scrollOffset = mScrollOffset - currentDividerWidth / 2;
        if (position > 0 || offset > 0) {
            newScrollX -= scrollOffset;
        }
        if (newScrollX != lastScrollX) {
            lastScrollX = newScrollX;
            scrollTo(newScrollX, 0);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isInEditMode() || tabCount == 0) {
            return;
        }
        final int height = getHeight();

        // draw underline
        rectPaint.setColor(underlineColor);
        canvas.drawRect(0, height - underlineHeight, tabsContainer.getWidth(), height, rectPaint);

        // draw indicator line
        // default: line below current tab
        View currentTab = tabsContainer.getChildAt(currentPosition);
        float lineLeft = currentTab.getLeft();
        float lineRight = currentTab.getRight();

        // if there is an offset, start interpolating left and right coordinates between current and next tab
        if (currentPositionOffset > 0f && currentPosition < tabCount - 1) {
            View nextTab = tabsContainer.getChildAt(currentPosition + 1);
            final float nextTabLeft = nextTab.getLeft();
            final float nextTabRight = nextTab.getRight();

            lineLeft = (currentPositionOffset * nextTabLeft + (1f - currentPositionOffset) * lineLeft);
            lineRight = (currentPositionOffset * nextTabRight + (1f - currentPositionOffset) * lineRight);

            currentDividerWidth = (int) (lineRight - lineLeft);
        }

        rectPaint.setColor(indicatorColor);
        canvas.drawRect(lineLeft, height - indicatorHeight, lineRight, height, rectPaint);

        // I dont want divider so delete code under :hangl
        // draw divider
        /*
         * dividerPaint.setColor(dividerColor); for (int i = 0; i < tabCount - 1; i++) { View tab = tabsContainer.getChildAt(i);
		 * canvas.drawLine(tab.getRight(), dividerPadding, tab.getRight(), height - dividerPadding, dividerPaint); }
		 */
    }

    private class PageListener implements OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (tabsContainer.getChildAt(position) == null) {
                return;
            }
            currentPosition = position;
            currentPositionOffset = positionOffset;
            scrollToChild(position, (int) (positionOffset * tabsContainer.getChildAt(position).getWidth()));

            invalidate();

            if (delegatePageListener != null) {
                delegatePageListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                scrollToChild(pager.getCurrentItem(), 0);
            }
            invalidate();
            if (delegatePageListener != null) {
                delegatePageListener.onPageScrollStateChanged(state);
            }
        }

        @Override
        public void onPageSelected(int position) {
            selectTab(position);

            currentSelectPosition = position;
            if (delegatePageListener != null) {
                delegatePageListener.onPageSelected(position);
            }
        }

    }

    /**
     * Runs through all tabs and sets if they are currently selected.
     *
     * @param position The position of the currently selected tab.
     */
    private void selectTab(int position) {
        for (int i = 0; i < tabsContainer.getChildCount(); i++) {
            View childView = tabsContainer.getChildAt(i);
            if (childView instanceof TextView) {
                CharSequence text = ((TextView) childView).getText();
                if (i != position) {
                    ((TextView) childView).setTextColor(tabTextColorNormal);
                    // unBold(((TextView) childView));
                    childView.setContentDescription(text);
                } else {
                    ((TextView) childView).setTextColor(tabTextColorSelected);
                    // bold(((TextView) childView));
                    childView.setContentDescription("(" + text + ")");

                    // 默认位置及非点击Tab时的上报
                    if (pager.getAdapter() instanceof FragmentHomePage.ViewPagerAdapterMain) {
                        FragmentHomePage.ViewPagerAdapterMain adapter = (FragmentHomePage.ViewPagerAdapterMain) pager.getAdapter();

                        PageActionTracker.clickCh();

                    }

                }
            } else if (childView instanceof LiveNavigatorTabView) {
                CharSequence text = ((LiveNavigatorTabView) childView).getTextView().getText();
                if (i != position) {
                    childView.setSelected(false);
                    childView.setContentDescription(text);
                } else {
                    childView.setSelected(true);
                    childView.setContentDescription("(" + text + ")");
                }
            }
        }

    }

    private static void bold(TextView tv) {
        TextPaint tp = tv.getPaint();
        tp.setFakeBoldText(true);
    }

    private static void unBold(TextView tv) {
        TextPaint tp = tv.getPaint();
        tp.setFakeBoldText(false);
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public int getCurrentSelectPosition() {
        return currentSelectPosition;
    }

    public void setIndicatorColor(int indicatorColor) {
        this.indicatorColor = indicatorColor;
        invalidate();
    }

    public void setIndicatorColorResource(int resId) {
        this.indicatorColor = getResources().getColor(resId);
        invalidate();
    }

    public int getIndicatorColor() {
        return this.indicatorColor;
    }

    public void setIndicatorHeight(int indicatorLineHeightPx) {
        this.indicatorHeight = indicatorLineHeightPx;
        invalidate();
    }

    public int getIndicatorHeight() {
        return indicatorHeight;
    }

    public void setUnderlineColor(int underlineColor) {
        this.underlineColor = underlineColor;
        invalidate();
    }

    public void setUnderlineColorResource(int resId) {
        this.underlineColor = getResources().getColor(resId);
        invalidate();
    }

    public int getUnderlineColor() {
        return underlineColor;
    }

    public void setDividerColor(int dividerColor) {
        this.dividerColor = dividerColor;
        invalidate();
    }

    public void setDividerColorResource(int resId) {
        this.dividerColor = getResources().getColor(resId);
        invalidate();
    }

    public int getDividerColor() {
        return dividerColor;
    }

    public void setUnderlineHeight(int underlineHeightPx) {
        this.underlineHeight = underlineHeightPx;
        invalidate();
    }

    public int getUnderlineHeight() {
        return underlineHeight;
    }

    public void setDividerPadding(int dividerPaddingPx) {
        this.dividerPadding = dividerPaddingPx;
        invalidate();
    }

    public int getDividerPadding() {
        return dividerPadding;
    }

    public void setScrollOffset(int scrollOffsetPx) {
        this.mScrollOffset = scrollOffsetPx;
        invalidate();
    }

    public int getScrollOffset() {
        return mScrollOffset;
    }

    public void setShouldExpand(boolean shouldExpand) {
        this.shouldExpand = shouldExpand;
        requestLayout();
    }

    public boolean getShouldExpand() {
        return shouldExpand;
    }

    public boolean isTextAllCaps() {
        return textAllCaps;
    }

    public void setAllCaps(boolean textAllCaps) {
        this.textAllCaps = textAllCaps;
    }

    public void setTextSize(int textSizePx) {
        this.tabTextSize = textSizePx;
        updateTabStyles();
    }

    public int getTextSize() {
        return tabTextSize;
    }

    public void setTextColor(int textColor) {
        this.tabTextColor = textColor;
        updateTabStyles();
    }

    public void setTextColorResource(int resId) {
        this.tabTextColor = getResources().getColor(resId);
        updateTabStyles();
    }

    public int getTextColor() {
        return tabTextColor;
    }

    public void setTypeface(Typeface typeface, int style) {
        this.tabTypeface = typeface;
        this.tabTypefaceStyle = style;
        updateTabStyles();
    }

    public void setTabBackground(int resId) {
        this.tabBackgroundResId = resId;
    }

    public int getTabBackground() {
        return tabBackgroundResId;
    }

    public void setTabPaddingLeftRight(int paddingPx) {
        this.tabPaddingLeftRight = paddingPx;
        updateTabStyles();
    }

    public int getTabPaddingLeftRight() {
        return tabPaddingLeftRight;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        currentPosition = savedState.currentPosition;
        requestLayout();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.currentPosition = currentPosition;
        return savedState;
    }

    static class SavedState extends BaseSavedState {
        int currentPosition;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            currentPosition = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(currentPosition);
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    /**
     * set LiveNavigatorTabView
     */
    private void layoutImageTab(LiveNavigatorTabView liveNavigatorTabView) {

        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();

        int deviceWidth = displayMetrics.widthPixels < displayMetrics.heightPixels ? displayMetrics.widthPixels : displayMetrics.heightPixels;
        int itemWidth = (int) ((float) deviceWidth / (tabCount > 5 ? 5 : tabCount));

        // set live navigator tabView
        ViewGroup.LayoutParams layoutParams = liveNavigatorTabView.getLayoutParams();
        layoutParams.width = itemWidth;
        liveNavigatorTabView.setLayoutParams(layoutParams);

        // set live navigator tab imageView
        ImageView liveNaviTabImageView = liveNavigatorTabView.getImageView();
        int logoHeight = itemWidth * 62 / 210;
        LinearLayout.LayoutParams imageLp = (LinearLayout.LayoutParams) liveNaviTabImageView.getLayoutParams();
        imageLp.height = logoHeight;
        imageLp.topMargin = itemWidth * 19 / 210;
        imageLp.width = itemWidth;
        liveNaviTabImageView.setLayoutParams(imageLp);

        // set live navigator tab textView
        TextView liveNaviTabTextView = liveNavigatorTabView.getTextView();
        int textSize = itemWidth * 40 / 210;
        LinearLayout.LayoutParams textLp = (LinearLayout.LayoutParams) liveNaviTabTextView.getLayoutParams();
        textLp.topMargin = itemWidth * 9 / 210;
        liveNaviTabTextView.setTextSize(textSize);
        liveNaviTabTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        liveNaviTabTextView.setLayoutParams(textLp);
    }

}
