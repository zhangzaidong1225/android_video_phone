package com.ifeng.newvideo.widget;


import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 焦点图
 */
public class HeaderFlowViewPager extends ViewPager {
    private static final Logger logger = LoggerFactory.getLogger(HeaderFlowViewPager.class);
    private long timeSpace = 5000;

    private static float ration = 0.5625f;

    public HeaderFlowViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        logger.debug("HeaderFlowViewPager  oncreate");
    }

    public HeaderFlowViewPager(Context context) {
        super(context);
    }


//    private float downX = 0;
//    private float downY = 0;
//    private boolean stop = false;
//    private boolean scrollDown = false;
//    private boolean isBeingDragged = false;
//    private MotionEvent downEventCache;

//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//
////        getParent().requestDisallowInterceptTouchEvent(true);
//        logger.debug("onInterceptTouchEvent" + " " + ev.getAction());
//        if (getAdapter() != null && getAdapter().getCount() == 1) {
//            return super.onInterceptTouchEvent(ev);
//        }
//        ViewParent parent=getParent();
//        getParent().requestDisallowInterceptTouchEvent(true);
//        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//            logger.debug("onInterceptTouchEvent" + " : MotionEvent.ACTION_DOWN");
//            stop = false;
//            downX = ev.getRawX();
//            downY = ev.getRawY();
//            downEventCache = ev;
//            Log.d("ak47","downEventCache:"+downEventCache.getY());
//        } else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//            logger.debug("onInterceptTouchEvent" + " : MotionEvent.ACTION_MOVE");
//            float x = ev.getRawX();
//            float y = ev.getRawY();
//            float dx = Math.abs(x - downX);
//            float dy = Math.abs(y - downY);
//            PullToRefreshListView plv = getPullRefreshListView();
////            ListView listView = getListView();
//            logger.debug("onInterceptTouchEvent" + " downX" + downX + "downY" + downY + "x" + x + "y" + y);
//            if (dy > 20 && dy > dx && plv != null) {
//                logger.debug("onInterceptTouchEvent" + " dy" + dy + "dx" + dx);
//                if (y - downY > 0) {
//                    scrollDown = true;
//                    isBeingDragged = false;
//                } else {
//                    scrollDown = false;
////					 if (listView != null){
////                         listView.onInterceptTouchEvent(ev);
////                     }
//                }
//                plv.onInterceptTouchEvent(downEventCache);
//                plv.onTouchEvent(downEventCache);
//                stop = true;
//                logger.debug("onInterceptTouchEvent" + " : true");
//                return true;
//            }
//        }
//        return super.onInterceptTouchEvent(ev);
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        if (stop) {
//            logger.debug("onTouchEvent" + " : onStop");
//            PullToRefreshListView plv = getPullRefreshListView();
//            if (plv == null) {
//                return true;
//            }
////          int[] loc = new int[2];
////			getLocationOnScreen(loc);
////			ev.setLocation(ev.getX(), loc[1] + ev.getY());
//            if (scrollDown && !isBeingDragged) {
//                isBeingDragged = plv.onInterceptTouchEvent(ev);
//            }
//            plv.onTouchEvent(ev);
//            return true;
//        }
//        logger.debug("onTouchEvent" + " : onTouchEvent");
//        return super.onTouchEvent(ev);
//    }

//    private PullToRefreshListView getPullRefreshListView() {
//        PullToRefreshListView listView = null;
//        ViewParent vp = getParent();
//        while (vp != null) {
//            if (vp instanceof PullToRefreshListView) {
//                listView = (PullToRefreshListView) vp;
//                break;
//            }
//            vp = vp.getParent();
//        }
//        return listView;
//    }

//    private ListView getListView() {
//        ListView listView = null;
//        ViewParent vp = getParent();
//        while (vp != null) {
//            if (vp instanceof ListView) {
//                listView = (ListView) vp;
//                break;
//            }
//            vp = vp.getParent();
//        }
//        return listView;
//    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();

        if (action == MotionEvent.ACTION_DOWN) {
            stopAutoFlow();
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
//            autoHandler.sendEmptyMessageDelayed(0, timeSpace);
            startAutoFlow();
        }
        return super.dispatchTouchEvent(ev);
    }

    private final Handler autoHandler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            autoHandler.removeMessages(0);
            setCurrentItem(getCurrentItem() + 1);
//            startAutoFlow();
            sendEmptyMessageDelayed(0, timeSpace);
        }

    };

//    public void setAutoFlow(long timeSpace) {
//        this.timeSpace = timeSpace;
//    }

    public void startAutoFlow() {
        if (getAdapter() != null && getAdapter().getCount() > 1) {
            autoHandler.removeCallbacksAndMessages(null);
            autoHandler.sendEmptyMessageDelayed(0, timeSpace);
        }
    }

    public void stopAutoFlow() {
        if (getAdapter() != null && getAdapter().getCount() > 1) {
            autoHandler.removeCallbacksAndMessages(null);
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAutoFlow();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAutoFlow();
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        stopAutoFlow();
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == VISIBLE) {
            startAutoFlow();
        } else {
            stopAutoFlow();
        }

    }

    @Override
    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        stopAutoFlow();
    }

    @Override
    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        startAutoFlow();
    }
}
