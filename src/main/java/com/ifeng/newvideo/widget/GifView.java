package com.ifeng.newvideo.widget;

import android.content.Context;
import android.graphics.*;
import android.os.Build;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;


public class GifView extends View {
    private static final Logger logger = LoggerFactory.getLogger(GifView.class);

    private Context mContext;
    private int mImageWidth; //GIF图片的宽度
    private int mImageHeight; //GIF图片的高度
    private Movie mMovie; //播放GIF动画的Movie类
    private long mMovieStart; //记录动画开始的时间
    private int mMovieDuration; //动画总时长
    private float scale;
    private int mAlpha = 250; //80% ，黑色背景透明度
    private Paint paint;
    private Paint blackBgPaint;

    public GifView(Context context) {
        super(context);
        this.mContext = context;
        initPaint();
    }

    public GifView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        initPaint();
    }

    private void initPaint() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(DisplayUtils.convertDipToPixel(1));
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);

        blackBgPaint = new Paint();
        blackBgPaint.setStyle(Paint.Style.FILL);
        blackBgPaint.setAntiAlias(true);
        blackBgPaint.setColor(Color.BLACK);
        blackBgPaint.setAlpha(mAlpha);
    }


    private void initResource() {
        // 获取该资源的流
        InputStream is = getResources().openRawResource(R.raw.float_audio);
        // 使用Movie类对流进行解码
        mMovie = Movie.decodeStream(is);
        if (mMovie != null) {
            mMovieDuration = mMovie.duration();
            // 如果返回值不等于null，就说明这是一个GIF图片，下面获取是否自动播放的属性
            mImageWidth = DisplayUtils.convertDipToPixel(80);
            mImageHeight = DisplayUtils.convertDipToPixel(80);
        }
        scale = mContext.getResources().getDisplayMetrics().densityDpi / (160f);
        scale = scale / 3;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mMovie == null) {
            initResource();
        }
        if (mMovie != null) {
            // 如果是GIF图片则重写设定GifSurfaceView的大小
            setMeasuredDimension(mImageWidth, mImageHeight);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }


    /**
     * 开始播放GIF动画
     */
    private void playMovie(Canvas canvas) {
        long now = SystemClock.uptimeMillis();
        if (mMovieStart == 0) {
            mMovieStart = now;
        }
        if (mMovieDuration == 0) {
            mMovieDuration = 1000;
        }
        int relTime = (int) ((now - mMovieStart) % mMovieDuration);
        mMovie.setTime(relTime);
        canvas.scale(scale, scale);

        mMovie.draw(canvas, DisplayUtils.convertDipToPixel(28) / scale, DisplayUtils.convertDipToPixel(28) / scale);

        canvas.restore();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mMovie != null) {

            canvas.drawColor(Color.TRANSPARENT);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            canvas.drawPaint(paint);
            //paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
            //canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
            // canvas.drawCircle(mImageWidth / 2, mImageHeight / 2, mImageHeight / 2 - DisplayUtils.convertDipToPixel(mContext, 3), blackBgPaint);

            playMovie(canvas);

            invalidateView();

        }
    }

    private void invalidateView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            postInvalidateOnAnimation();
        } else {
            invalidate();
        }

    }

}