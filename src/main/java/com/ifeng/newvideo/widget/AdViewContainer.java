package com.ifeng.newvideo.widget;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.util.AdsExposureSesssion;

import java.util.ArrayList;
import java.util.List;

import static com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter.AD_TYPE_APP;

/**
 * 播放底页 信息流广告view
 * Created by ll on 2017/4/19.
 */
public class AdViewContainer extends LinearLayout {

    private TextView mTitle, mAd_des, mTag, mDownload;
    private NetworkImageView mRight_pic;

    public AdViewContainer(Context context) {
        this(context, null);
    }

    public AdViewContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdViewContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.ad_mix_text_pic, this, true);
        mTitle = (TextView) findViewById(R.id.tv_left_title);
        mAd_des = (TextView) findViewById(R.id.tv_ad_desc);
        mTag = (TextView) findViewById(R.id.tv_ad_tag);
        mDownload = (TextView) findViewById(R.id.tv_ad_download);
        mRight_pic = (NetworkImageView) findViewById(R.id.niv_right_picture);
    }

    protected void configAdMixTextPicConvertView(final View convertView, final MainAdInfoModel.AdMaterial adMaterial) {

        if (AD_TYPE_APP.equalsIgnoreCase(adMaterial.getAdConditions().getShowType())) {
            mDownload.setVisibility(View.VISIBLE);
            if (mDownload.getParent() instanceof View) {
                ((View) mDownload.getParent()).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<String> downloadCompleteUrl = new ArrayList<>();
                        if (!ListUtils.isEmpty(adMaterial.getAdAction().getAsync_downloadCompletedurl())) {
                            downloadCompleteUrl.addAll(adMaterial.getAdAction().getAsync_downloadCompletedurl());
                        }
                        if (!ListUtils.isEmpty(adMaterial.getAdAction().getDownloadCompletedurl())) {
                            downloadCompleteUrl.addAll(adMaterial.getAdAction().getDownloadCompletedurl());
                        }
                        AdvertExposureDao.sendAdvertClickReq(adMaterial.getAdId(), adMaterial.getAdAction().getAsync_click());
                        DownLoadUtils.download(IfengApplication.getAppContext(), adMaterial.getAdId(), adMaterial.getAdAction().getLoadingurl(),
                                (ArrayList<String>) adMaterial.getAdAction().getAsync_download(), (ArrayList<String>) downloadCompleteUrl
                        );
                    }
                });
            }
        } else {
            mDownload.setVisibility(View.GONE);
            if (mDownload.getParent() != null) {
                mDownload.setOnClickListener(null);
            }
        }
        if (TextUtils.isEmpty(adMaterial.getText())) {
            mTitle.setVisibility(View.GONE);
        } else {
            mTitle.setText(adMaterial.getText());
            mTitle.setVisibility(View.VISIBLE);
        }
        setImageUrl(mRight_pic, adMaterial.getImageURL(), R.drawable.bg_default_mid);
        if (TextUtils.isEmpty(adMaterial.getVdescription())) {
            mAd_des.setVisibility(View.GONE);
        } else {
            mAd_des.setText(adMaterial.getVdescription());
            mAd_des.setVisibility(View.VISIBLE);
        }

        MainAdInfoModel.AdMaterial.Icon icon = adMaterial.getIcon();
        if (icon != null) {
            String text = icon.getText();
            mTag.setText(TextUtils.isEmpty(text) ? "广告" : text);
            mTag.setVisibility(icon.getShowIcon() == 1 ? View.VISIBLE : View.GONE);
        }
    }

    protected void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
    }

    public void updateView(final MainAdInfoModel.AdMaterial adMaterial) {
        setTag(adMaterial);
        configAdMixTextPicConvertView(this, adMaterial);
        sendExposure(adMaterial);
        MainAdInfoModel.AdMaterial.CheckForExposureListener listener;
        adMaterial.setCheckForExposureListener(
                listener = new MainAdInfoModel.AdMaterial.CheckForExposureListener() {
                    @Override
                    public void onViewScroll() {
                        sendExposure(adMaterial);
                    }
                });
    }

    private void sendExposure(MainAdInfoModel.AdMaterial adMaterial) {
        Rect localRect = new Rect();
        boolean localVisibility = getLocalVisibleRect(localRect);
        if (!AdsExposureSesssion.getInstance().containsADItemRecord(adMaterial.getAdId())) {
            if (localVisibility) {
                AdsExposureSesssion.getInstance().addADItemRecord(adMaterial.getAdId());
                AdvertExposureDao.addIfengAdvExposureForChannel(adMaterial.getAdId(), null,
                        adMaterial.getAdAction().getPvurl(), adMaterial.getAdAction().getAdpvurl());
            }
        } else {
            if (!localVisibility) {
                AdsExposureSesssion.getInstance().removeADItemRecord(adMaterial.getAdId());
            }
        }

    }
}
