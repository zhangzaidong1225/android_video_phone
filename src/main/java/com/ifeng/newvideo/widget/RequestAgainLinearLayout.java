package com.ifeng.newvideo.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import com.ifeng.newvideo.statistics.ActionIdConstants;

/**
 * Created by jieyz on 2015/6/18.
 */
public class RequestAgainLinearLayout extends LinearLayout {
    private GestureDetector mGestureDetector;
    private final Context mContext;

    public RequestAgainLinearLayout(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public RequestAgainLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public RequestAgainLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init() {
        mGestureDetector = new GestureDetector(new PanelOnGestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    public class PanelOnGestureListener implements GestureDetector.OnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            //向下滑
            boolean isScrollDown = velocityY > 0 && Math.abs(velocityY) > Math.abs(velocityX);
            if (isScrollDown) {
                sendScrollDownBroadcast();
            }
            return false;
        }

        private void sendScrollDownBroadcast() {
            Intent changeUI = new Intent();
            changeUI.setAction(ActionIdConstants.ACTION_PULL_SCROLL_DOWN);
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
            localBroadcastManager.sendBroadcast(changeUI);
        }
    }
}
