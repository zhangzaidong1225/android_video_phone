package com.ifeng.newvideo.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 主要是解决栏目动画时，滑动listView时图片被回收出现的崩溃问题。
 * Created by Vincent on 2014/9/30.
 */
public class ImageViewColumn extends ImageView {

    private static final Logger logger = LoggerFactory.getLogger(ImageViewColumn.class);

    public ImageViewColumn(Context context) {
        super(context);
    }

    public ImageViewColumn(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ImageViewColumn(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
        } catch (Exception e) {
            logger.error("trying to use a recycled bitmap", e);
        }
    }

}
