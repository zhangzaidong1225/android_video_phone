package com.ifeng.newvideo.widget;

import android.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Vincent on 2014/10/20.
 */
public class FavoriteItemClicking {

    private static final Logger logger = LoggerFactory.getLogger(FavoriteItemClicking.class);

    private View view;

    private FavoriteItemClicking() {

    }

    private static final FavoriteItemClicking instance = new FavoriteItemClicking();

    public static FavoriteItemClicking getInstance() {
        return instance;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

}
