package com.ifeng.newvideo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.dao.db.constants.DataInterface;

/**
 * Created by ll on 2017/7/7.
 */
public class VipPrivilegeView extends LinearLayout implements View.OnClickListener{
    private LinearLayout mPoint;
    private LinearLayout mAD;
    private LinearLayout mSign;
    private LinearLayout mWeal;
    public VipPrivilegeView(Context context) {
        super(context);
        initialize(context);
    }

    public VipPrivilegeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public VipPrivilegeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        inflate(context, R.layout.vip_member_privilege, this);
        mPoint = (LinearLayout) findViewById(R.id.ll_point);
        mPoint.setOnClickListener(this);
        mAD = (LinearLayout) findViewById(R.id.ll_ad);
        mAD.setOnClickListener(this);
        mSign = (LinearLayout) findViewById(R.id.ll_sign);
        mSign.setOnClickListener(this);
        mWeal = (LinearLayout) findViewById(R.id.ll_weal);
        mWeal.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_point:
                IntentUtils.startADActivity(getContext(), null, String.format(DataInterface.VIP_MEMBER_PRIVILEGE, "tab1", User.isVip()?"1":"0"), DataInterface.VIP_MEMBER_PRIVILEGE_HOME, ADActivity.FUNCTION_VALUE_H5_STATIC,IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_title), IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_share_content), null,null,null,null,null);
            break;
            case R.id.ll_ad:
                IntentUtils.startADActivity(getContext(), null, String.format(DataInterface.VIP_MEMBER_PRIVILEGE, "tab2", User.isVip()?"1":"0"), DataInterface.VIP_MEMBER_PRIVILEGE_HOME,ADActivity.FUNCTION_VALUE_H5_STATIC,IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_title), IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_share_content), null,null,null,null,null);
                break;
            case R.id.ll_sign:
                IntentUtils.startADActivity(getContext(), null, String.format(DataInterface.VIP_MEMBER_PRIVILEGE, "tab3", User.isVip()?"1":"0"), DataInterface.VIP_MEMBER_PRIVILEGE_HOME,ADActivity.FUNCTION_VALUE_H5_STATIC,IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_title), IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_share_content), null,null,null,null,null);
                break;
            case R.id.ll_weal:
                IntentUtils.startADActivity(getContext(), null, String.format(DataInterface.VIP_MEMBER_PRIVILEGE, "tab4", User.isVip()?"1":"0"), DataInterface.VIP_MEMBER_PRIVILEGE_HOME,ADActivity.FUNCTION_VALUE_H5_STATIC,IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_title), IfengApplication.getInstance().getResources().getString(R.string.vip_open_member_privilege_share_content), null,null,null,null,null);
                break;
        }
    }
}
