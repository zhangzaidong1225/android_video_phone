package com.ifeng.newvideo.widget;

import android.app.Dialog;
import android.net.http.SslError;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.utils.AlertUtils;

/**
 * 上架GooglePlay 统一处理onReceivedSslError
 */
public class BaseWebViewClient extends WebViewClient {

    private Dialog mSslErrorDialog;

    @Override
    public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
        if (!PhoneConfig.isGooglePlay()) {
            handler.proceed();
            return;
        }

        try {
            dismissSslDialog();

            mSslErrorDialog = AlertUtils.getInstance().showTwoBtnDialog(
                    view.getContext(),
                    IfengApplication.getInstance().getString(R.string.certificate_invalid_message),
                    IfengApplication.getInstance().getString(R.string.certificate_btn_proceed), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            handler.proceed();
                            dismissSslDialog();
                        }
                    },
                    IfengApplication.getInstance().getString(R.string.certificate_btn_cancel), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            handler.cancel();
                            dismissSslDialog();
                        }
                    });
        } catch (Exception e) {
            super.onReceivedSslError(view, handler, error);
        }
    }

    private void dismissSslDialog() {
        if (mSslErrorDialog != null) {
            mSslErrorDialog.dismiss();
        }
    }
}
