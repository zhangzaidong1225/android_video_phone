package com.ifeng.newvideo.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 短视频图片
 */
public class ShortVideoImageView extends ImageView {
    private static final Logger logger = LoggerFactory.getLogger(ShortVideoImageView.class);
    private static final int ID = 0x1001;
    private static int viewWidth;
    public static int viewHeight;

    public ShortVideoImageView(Context context) {
        super(context);
        if (viewWidth == 0) {
            int screenWidth = DisplayUtils.getWindowWidth();
            int screenHeight = DisplayUtils.getWindowHeight();
            viewWidth = screenWidth * (1080 - 92) / 1080 / 2;
            viewHeight = viewWidth * 3 / 4;
        }
        initLayoutParams();
        setId(ID);
    }

    private void initLayoutParams() {
        setLayoutParams(new LinearLayout.LayoutParams(viewWidth, viewHeight));
        setScaleType(ScaleType.FIT_XY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
