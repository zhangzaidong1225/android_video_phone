package com.ifeng.newvideo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * 重写 ScrollView
 */
public class ObservableScrollView extends ScrollView {

    public OnObservableScorllViewListener mOnObservableScrollViewListener;

    public ObservableScrollView(Context context) {
        super(context);
    }

    public ObservableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservableScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public interface OnObservableScorllViewListener {
        void onObservableScrollViewListener(int l, int t, int oldl, int oldt);
    }

    public void setOnObservableScrollViewListener(OnObservableScorllViewListener listener) {
        this.mOnObservableScrollViewListener = listener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (null != mOnObservableScrollViewListener) {
            mOnObservableScrollViewListener.onObservableScrollViewListener(l, t, oldl, oldt);
        }
    }
}
