package com.ifeng.newvideo.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.adapter.FmHeaderPagerAdapter;
import com.ifeng.newvideo.ui.adapter.ListAdapterFMPage;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.widget.HeadFlowView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.utils.JsonUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.FMDao;
import com.ifeng.video.dao.db.model.FMInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class FmChannelFragment extends BaseFragment implements PullToRefreshBase.OnRefreshListener {

    private static final Logger logger = LoggerFactory.getLogger(FmChannelFragment.class);
    private View headView;
    private HeadFlowView mHeadFlowView;
    private FmHeaderPagerAdapter mFmPagerAdapter;
    private MyPullToRefreshListView mPullToRefreshListView;
    private boolean isLoading = false;
    private ListAdapterFMPage mChannelFMListAdapter;
    private FMInfoModel mLastInfoModel;
    private String mChannel_id;
    private String mChannel_Type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mChannel_id = bundle.getString(IntentKey.CHANNELID);
            mChannel_Type = bundle.getString(IntentKey.CHANNEL_TYPE);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        headView = inflater.inflate(R.layout.fm_headview, null);
        mHeadFlowView = (HeadFlowView) headView.findViewById(R.id.fm_headFlowView);

        mPullToRefreshListView = (MyPullToRefreshListView) inflater.inflate(R.layout.phone_childfragment_video, null);
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mPullToRefreshListView.setShowIndicator(false);
        mPullToRefreshListView.setOnRefreshListener(this);
        mPullToRefreshListView.getRefreshableView().addHeaderView(headView);
        mPullToRefreshListView.hideFootView();
        mChannelFMListAdapter = new ListAdapterFMPage(getActivity(), mChannel_id);
        mPullToRefreshListView.setAdapter(mChannelFMListAdapter);
        return mPullToRefreshListView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestNet();
        mFocusList = new ArrayList<>(20);
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        requestNet(true);
    }

    @Override
    protected void requestNet() {
        requestNet(false);
    }

    protected void requestNet(Boolean isPullRefresh) {
        if (isLoading) {
            return;
        } else {
            isLoading = true;
        }
        if (isPullRefresh == null) {
            mPullToRefreshListView.setRefreshing();
        } else if (!isPullRefresh) {
            updateViewStatus(Status.LOADING);
        }
        //获取数据
        FMDao.getFMInfoModel(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                refreshUI(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("getHomePageData onErrorResponse {}", error);
                isLoading = false;
                mPullToRefreshListView.onRefreshComplete();
                String cacheString = CommonDao.getCache(DataInterface.getFmListUrl());
                if (!TextUtils.isEmpty(cacheString)) {
                    refreshUI(JsonUtils.parseObject(cacheString, FMInfoModel.class));
                    return;
                }

                if (getActivity() == null) {
                    return;
                }
                if (!NetUtils.isNetAvailable(getActivity())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                }
                if (error instanceof NetworkError) {
                    updateViewStatus(Status.REQUEST_NET_FAIL);
                } else {
                    updateViewStatus(Status.DATA_ERROR);
                }
                mPullToRefreshListView.changeState(MyPullToRefreshListView.FootViewState.FAIL_TO_RELOAD);
                mPullToRefreshListView.hideFootView();

            }
        });

        PageActionTracker.pullCh(isPullRefresh == null || !isPullRefresh, mChannel_id);

    }


    private <T> void refreshUI(T response) {
        isLoading = false;
        if (getActivity() == null || response == null) {
            return;
        }
        updateViewStatus(Status.REQUEST_NET_SUCCESS);
        if (!NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
        mPullToRefreshListView.onRefreshComplete();
        FMInfoModel fmInfoModel = (FMInfoModel) response;
        //容错，去除关键字段缺失的数据
        fmInfoModel = filterFMPageModel(fmInfoModel);
        if (fmInfoModel == null) {
            logger.debug("in getHomePage  the data not change ");
            return;
        }

        mLastInfoModel = fmInfoModel;
        List<FMInfoModel.Header> header = fmInfoModel.getHeader();
        List<FMInfoModel.Body> body = fmInfoModel.getBody();

        bindData2Head(header, null);
        mChannelFMListAdapter.setData(transBodyToResourceInfoList(body));
        mChannelFMListAdapter.notifyDataSetChanged();
        mPullToRefreshListView.getRefreshableView().setSelection(0);

    }

    private void bindData2Head(List<FMInfoModel.Header> headers, Object o) {
        if (ListUtils.isEmpty(headers)) {
            mHeadFlowView.setVisibility(View.GONE);
        } else {
            mHeadFlowView.setVisibility(View.VISIBLE);
            mFmPagerAdapter = new FmHeaderPagerAdapter(getActivity(), mHeadFlowView, mChannel_id);
            mHeadFlowView.setViewPagerAdapter(mFmPagerAdapter);
            mFmPagerAdapter.setData(headers);
            mFmPagerAdapter.notifyDataSetChanged();
            if (headers.size() > 1) {
                mHeadFlowView.setCurrentItem(20 * headers.size());
            }
        }
    }


    /**
     * 接口数据过滤 遍历一次
     */
    private FMInfoModel filterFMPageModel(FMInfoModel fmInfoModel) {
        // 过滤header
        List<FMInfoModel.Header> headerList = fmInfoModel.getHeader();
        if (!ListUtils.isEmpty(headerList)) {
            FMInfoModel.Header header;
            for (int i = 0; i < headerList.size(); i++) {
                header = headerList.get(i);
                if (header == null || TextUtils.isEmpty(header.getImage()) || header.getResourceInfo() == null ||
                        TextUtils.isEmpty(header.getResourceInfo().getProgramId())) {
                    headerList.remove(i);
                    i--;
                }
            }
        }
        fmInfoModel.setHeader(headerList);

        // 过滤body
        List<FMInfoModel.Body> bodyList = fmInfoModel.getBody();
        if (!ListUtils.isEmpty(bodyList)) {
            FMInfoModel.Body body;
            List<FMInfoModel.ResourceInfo> resourceInfoList;
            for (int j = 0; j < bodyList.size(); j++) {
                body = bodyList.get(j);
                if (body == null || TextUtils.isEmpty(body.getNodeId()) || TextUtils.isEmpty(body.getNodeName())
                        || ListUtils.isEmpty(body.getResourceInfo())) {
                    bodyList.remove(j);
                    j--;
                    continue;
                }
                // 过滤body中的resourceInfo
                resourceInfoList = body.getResourceInfo();
                FMInfoModel.ResourceInfo resourceInfo;
                for (int k = 0; k < resourceInfoList.size(); k++) {
                    resourceInfo = resourceInfoList.get(k);
                    if (resourceInfo == null || TextUtils.isEmpty(resourceInfo.getProgramId()) || TextUtils.isEmpty(resourceInfo.getImg370_370())) {
                        resourceInfoList.remove(k);
                        k--;
                    }
                }

                body.setResourceInfo(resourceInfoList);
            }
            fmInfoModel.setBody(bodyList);
        }

        return fmInfoModel;
    }

    /**
     * 数据转化，转化为列表展示格式
     * 给title栏加nodeId，非title栏不加，用nodeId来区分是否显示title栏
     */
    private List<FMInfoModel.ResourceInfo> transBodyToResourceInfoList(List<FMInfoModel.Body> bodyList) {
        List<FMInfoModel.ResourceInfo> newList = new ArrayList<>();
        for (FMInfoModel.Body body : bodyList) {
            List<FMInfoModel.ResourceInfo> oldList = body.getResourceInfo();
            boolean isTitleView = false;
            for (FMInfoModel.ResourceInfo resourceInfo : oldList) {
                if (!isTitleView) {
                    resourceInfo.setNodeId(body.getNodeId());
                }
                resourceInfo.setNodeName(body.getNodeName());
                resourceInfo.setCardTitle(body.getCardTitle());
                if (!newList.contains(resourceInfo)) {
                    newList.add(resourceInfo);
                    isTitleView = true;
                }
            }
        }
        return newList;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFocusList = CommonStatictisListUtils.fmFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(CommonStatictisListUtils.fmFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.fmFocusList.clear();
    }

}
