package com.ifeng.newvideo.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.mine.adapter.RecommendListAdapter;
import com.ifeng.newvideo.ui.mine.bean.WebPushData;
import com.ifeng.newvideo.ui.subscribe.adapter.SubscribeListAdapter;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.NameLengthFilterUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.PictureUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.UploadUtil;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.widget.GetImagePath;
import com.ifeng.newvideo.widget.ListViewForScrollView;
import com.ifeng.newvideo.widget.UnScrollGridView;
import com.ifeng.video.core.net.RequestJson;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BitmapUtils;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLDecoderUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.FavoritesDAO;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.dao.LoginDao;
import com.ifeng.video.dao.db.dao.MobileDao;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.CommentsModel;
import com.ifeng.video.dao.db.model.LaunchAppModel;
import com.ifeng.video.dao.db.model.MobileUserModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeList;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

/**
 * 我的
 * Created by wangqing1 on 2014/10/14.
 */
public class FragmentMine extends FragmentBase {
    private static final Logger logger = LoggerFactory.getLogger(FragmentMine.class);

    private static final int RESULT_CODE_CAPTURE = 0x20; // 拍照上传
    private static final int RESULT_CODE_ALBUM = 0x21; // 相册上传
    private static final int RESULT_CODE_CROP_BY_CAPTURE = 0x22; // 通过相机取出的照片裁剪
    private static final int RESULT_CODE_CROP_BY_ALBUM = 0x23; // 通过相册取出的照片裁剪
    private static final int RESULT_FILE_UPLOAD = 0x24;

    private TextView tv_viewed;//看过的
    private TextView tv_cached;//缓存的
    private TextView tv_stored;//收藏的
    private TextView tv_msg; //消息
    private TextView tv_red;
    private NetworkImageView mUserHeadImg; // 用户头像ImageView
    private View rl_user_name;
    private EditText mUserNameText; // 用户名TextView
    private ImageView iv_edit_nick_name;
    private TextView tv_login_info;//登录后更加精彩文字
    private ViewGroup mMarkViewGroup;//打分布局
    private ViewGroup mShowSubscribeUnLogin;//展示订阅内容及未登录提示布局
    private TextView tv_unlogin_info;//未登录订阅布局提示信息
    private View rl_recommend_separate;//推荐布局底部高24px的分割线
    private View view_subscribe_underline;//我的订阅下方一条细线
    private View mark_under_line;//我来打分下方分割线
    private TextView mSign, mPoint;
    private RelativeLayout mSignAndPoint;

    private UnScrollGridView gridView;//展示订阅信息GridView
    private SubscribeListAdapter subscribeAdapter;

    private ListViewForScrollView listView_recommend;//推荐布局ListView
    private RecommendListAdapter recommendListAdapter;

    private List<WebPushData.WebPushEntity> recommendList = new ArrayList<>();
    private List<SubscribeList.WeMediaListEntity> subscribeList = new ArrayList<>();

    private User user;
    private boolean mHasLogin;
    private TextView mVipDes, mVipTitle;
    private boolean mHasSign;
    private NetworkImageView mHeaderImageBg;
    private ImageView mHeaderVipSign;
    private ActivityMainTab mActivity;
    private boolean msgStatus;
    private onClickPointListener mListener;
    private final String[] uploadResult = new String[1];
    private String fileId;
    private String filePath;
    private String preUserName;
    private String lastImagePath; //文件最终存储地址

    public FragmentMine() {
        super();
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == RESULT_FILE_UPLOAD) {
                try {
                    if (!TextUtils.isEmpty(uploadResult[0])) {
                        org.json.JSONObject jsonObject = new org.json.JSONObject(uploadResult[0]);
                        if (jsonObject.getString("success").equals("true")) {
                            ToastUtils.getInstance().showShortToast("上传成功");
                            requestFileUploadPath();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ActivityMainTab) {
            mActivity = (ActivityMainTab) activity;
            mActivity.setFragmentMine(this);
            try {
                mListener = (onClickPointListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + "must implement OnClickPointListener");
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mine, container, false);
        initView(root);
        initAdapterData();
        getWebPush();
        requestPointCount();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerLoginBroadcast();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mHasLogin) {
            mySubscribeWeMedia();
            requestPointCount();
            requestMsgCount();
        }
        showLayout();
        initData();
        checkIsNickName();
        // handleSignInVisible();
        updateMarkVisibility();
        user = new User(getActivity());
        mHasLogin = User.isLogin();
        userIsLogin(mHasLogin);
        if (!isHidden()) {
            PageActionTracker.enterPage();
        }
        if (User.isVip()) {
            mVipTitle.setText(getResources().getString(R.string.vip_mine_item_member_is_vip));
            mVipDes.setText(String.format(getResources().getString(R.string.vip_open_member_due_date), User.getVipdate()));
        } else {
            mVipTitle.setText(getResources().getString(R.string.vip_mine_item_member_is_not_vip));
            mVipDes.setText(getString(R.string.vip_mine_item_member_no_ad_hint));
        }
        preUserName = mUserNameText.getText().toString();

//        requestMobileOrderStatus();
    }

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(mLoginReceiver);
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initData();
            if (mHasLogin) {
                mySubscribeWeMedia();
                requestPointCount();
            }
            PageActionTracker.enterPage();
        } else {
            if (mUserNameText.isFocusable()) {
                initUserEditText();
                mUserNameText.setText(preUserName);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
                }
            }
            PageActionTracker.endPageMine();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isHidden()) {
            PageActionTracker.endPageMine();
            initUserEditText();
        }
    }


    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        getActivity().registerReceiver(mLoginReceiver, filter);
    }

    private void initView(View v) {
        v.findViewById(R.id.ll_login).setOnClickListener(this);
        tv_viewed = (TextView) v.findViewById(R.id.tv_viewed);
        tv_cached = (TextView) v.findViewById(R.id.tv_cachenum);
        tv_stored = (TextView) v.findViewById(R.id.tv_store);
        tv_msg = (TextView) v.findViewById(R.id.tv_msg);
        tv_red = (TextView) v.findViewById(R.id.iv_red_point);
        tv_red.setVisibility(View.INVISIBLE);

        v.findViewById(R.id.ll_store).setOnClickListener(this);
        v.findViewById(R.id.ll_cache).setOnClickListener(this);
        v.findViewById(R.id.ll_watched).setOnClickListener(this);
        v.findViewById(R.id.ll_msg).setOnClickListener(this);

        v.findViewById(R.id.rl_login_icon).setOnClickListener(this);
        mUserHeadImg = (NetworkImageView) v.findViewById(R.id.img_header);
        mUserHeadImg.setOnClickListener(this);

        mUserNameText = (EditText) v.findViewById(R.id.tv_login);
        rl_user_name = v.findViewById(R.id.rl_user_name);
        iv_edit_nick_name = (ImageView) v.findViewById(R.id.iv_edit_nick_name);
        iv_edit_nick_name.setVisibility(View.INVISIBLE);
        iv_edit_nick_name.setOnClickListener(this);
        tv_login_info = (TextView) v.findViewById(R.id.tv_login_info);

        mUserNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mUserNameText.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            }
        });
        mUserNameText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                Log.d("mine", "--" + i);
                if (i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_ACTION_GO || i == EditorInfo.IME_ACTION_NEXT
                        || i == EditorInfo.IME_ACTION_SEND) {
                    requestNickNameUrl(mUserNameText.getText().toString(), false);
                }

                return false;
            }
        });
        mUserNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    if (User.isLogin()) {
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_USER_NAME, PageIdConstants.PAGE_MY);
                        rl_user_name.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                        mUserNameText.setCursorVisible(true);
                        mUserNameText.setSelection(mUserNameText.getText().toString().length());
                    } else {
                        IntentUtils.startLoginActivity(getActivity(), true);
                        mUserNameText.clearFocus();
                    }
                }
            }
        });

//        mUserNameText.setFilters((new InputFilter[]{new InputFilter.LengthFilter(24)}));
        NameLengthFilterUtils filter = new NameLengthFilterUtils(24);
        mUserNameText.setFilters(new InputFilter[]{filter});


        v.findViewById(R.id.mine_subscribe_container).setOnClickListener(this);
        v.findViewById(R.id.mine_feedback_container).setOnClickListener(this);
        v.findViewById(R.id.mine_setting_container).setOnClickListener(this);
        v.findViewById(R.id.mine_member_container).setOnClickListener(this);

        mMarkViewGroup = (ViewGroup) v.findViewById(R.id.mine_mark_container);
        if (PhoneConfig.isGooglePlay()) {
            mMarkViewGroup.setVisibility(View.GONE);
        }
        mMarkViewGroup.setOnClickListener(this);
        mark_under_line = v.findViewById(R.id.mark_under_line);

        rl_recommend_separate = v.findViewById(R.id.rl_recommend_separate);
        view_subscribe_underline = v.findViewById(R.id.view_subscribe_underline);
        listView_recommend = (ListViewForScrollView) v.findViewById(R.id.listView_recommend);

        mShowSubscribeUnLogin = (ViewGroup) v.findViewById(R.id.rl_show_subscribe_unlogin);
        gridView = (UnScrollGridView) v.findViewById(R.id.mine_sub_gridView);

        tv_unlogin_info = (TextView) v.findViewById(R.id.tv_unlogin_info);
        tv_unlogin_info.setOnClickListener(this);
        mVipDes = (TextView) v.findViewById(R.id.tv_vip_des);
        mVipTitle = (TextView) v.findViewById(R.id.tv_member);
        mSign = (TextView) v.findViewById(R.id.txt_user_sign_in);
        mPoint = (TextView) v.findViewById(R.id.txt_user_coin_cnt);
        mSignAndPoint = (RelativeLayout) v.findViewById(R.id.rl_sign_point);
        mSign.setOnClickListener(this);
        mPoint.setOnClickListener(this);
        mHeaderVipSign = (ImageView) v.findViewById(R.id.img_header_mask);
        mHeaderImageBg = (NetworkImageView) v.findViewById(R.id.img_banner);
    }

    private void initData() {
        try {
            tv_viewed.setText(String.valueOf(HistoryDAO.getInstance(IfengApplication.getInstance()).getAllHistoryDataCount()));
        } catch (Exception e) {
            tv_viewed.setText("0");
            logger.error("getAllHistoryDataCount()  error  -->{}", e.toString(), e);
        }
        try {
            tv_cached.setText(String.valueOf(CacheManager.getCacheCount(IfengApplication.getInstance())));
        } catch (Exception e) {
            tv_cached.setText("0");
            logger.error("getCacheCount()  error  -->{}", e.toString(), e);
        }
        try {
            tv_stored.setText(String.valueOf(FavoritesDAO.getInstance(IfengApplication.getInstance()).getAllFavoritesDataCount()));
        } catch (Exception e) {
            tv_stored.setText("0");
            logger.error("getAllFavoritesDataCount()  error  -->{}", e.toString(), e);
        }
    }

    private void initAdapterData() {
        subscribeAdapter = new SubscribeListAdapter();
        subscribeAdapter.setSize(SubscribeListAdapter.SIZE_THREE);//只展示三条信息
        subscribeAdapter.setShowSubscribeBtn(false);//不显示订阅按钮
        subscribeAdapter.setClickListener(new SubscribeListAdapter.ClickListener() {
            @Override
            public void onItemClickListener(SubscribeList.WeMediaListEntity entity) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_SUB_ITEM, PageIdConstants.PAGE_MY);
                IntentUtils.startWeMediaHomePageActivity(getActivity(), entity.getWeMediaID() + "", "");
            }

            @Override
            public void onSubscribeClickListener(SubscribeList.WeMediaListEntity entity) {
            }
        });
        gridView.setAdapter(subscribeAdapter);

        recommendListAdapter = new RecommendListAdapter(getActivity());
        listView_recommend.setAdapter(recommendListAdapter);
    }

    /**
     * 用户是否登录
     */
    private void userIsLogin(boolean isLogin) {
        if (isLogin) {
            VolleyHelper.getImageLoader().get(user.getUserIcon(), new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response != null && response.getBitmap() != null) {
                        mUserHeadImg.setImageBitmap(BitmapUtils.makeRoundCorner(response.getBitmap()));
                        mHeaderImageBg.setVisibility(View.VISIBLE);
                        mHeaderImageBg.setImageBitmap(BitmapUtils.gsBlurFilter(response.getBitmap()));
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    mUserHeadImg.setImageResource(R.drawable.icon_unlogin_header);
                    mHeaderImageBg.setVisibility(View.GONE);
                }
            });
            mSignAndPoint.setVisibility(View.VISIBLE);
            tv_login_info.setVisibility(View.GONE);
            mUserNameText.setText(user.getUserName());
            if (User.isVip()) {
                mHeaderVipSign.setVisibility(View.VISIBLE);
            } else {
                mHeaderVipSign.setVisibility(View.GONE);
            }
            iv_edit_nick_name.setVisibility(View.VISIBLE);
        } else {
            tv_login_info.setVisibility(View.VISIBLE);
            mSignAndPoint.setVisibility(View.GONE);
            mHeaderImageBg.setVisibility(View.GONE);
            mHeaderVipSign.setVisibility(View.GONE);
            iv_edit_nick_name.setVisibility(View.INVISIBLE);
        }
    }

    private void handleSignInVisible() {
        RequestJson<LaunchAppModel> myReq = new RequestJson<LaunchAppModel>(
                Request.Method.GET,
                DataInterface.getLaunchAppUrl(),
                LaunchAppModel.class,
                new Response.Listener<LaunchAppModel>() {
                    @Override
                    public void onResponse(LaunchAppModel response) {
                        // int visible = response.getSignin() == 1 ? View.VISIBLE : View.GONE;
                        // ll_signIn.setVisibility(visible);
                    }
                },
                null);
        myReq.setTag(hashCode());
        VolleyHelper.getRequestQueue().add(myReq);
    }

    @Override
    public void onClick(View v) {
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.rl_login_icon:
            case R.id.img_header:
//            case R.id.ll_login:
                if (v.getId() == R.id.ll_login) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_LOGIN_TEXT, PageIdConstants.PAGE_MY);
                } else {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_AVATAR, PageIdConstants.PAGE_MY);
                }
                if (mHasLogin) {
                    //IntentUtils.startSettingActivity(getActivity());
                    createDialog();
                } else {
                    IntentUtils.startLoginActivity(getActivity(), true);
                }
                break;
            case R.id.ll_watched:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_HISTORY, PageIdConstants.PAGE_MY);
                IntentUtils.ActivityHistory(getActivity());
                break;
            case R.id.ll_cache:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_CACHE, PageIdConstants.PAGE_MY);
                IntentUtils.startCacheAllActivity(getActivity());
                break;
            case R.id.ll_store:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_FAVORITE, PageIdConstants.PAGE_MY);
                IntentUtils.startActivityFavorites(getActivity());
                break;
            case R.id.ll_msg:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_MSG, PageIdConstants.PAGE_MY);
                if (mHasLogin) {
                    if (!TextUtils.isEmpty(tv_msg.getText())) {
                        if ("0".equals(tv_msg.getText())) {
                            msgStatus = false;
                            IntentUtils.startActivityMsg(getActivity(), msgStatus);
                        } else {
                            msgStatus = true;
                            IntentUtils.startActivityMsg(getActivity(), msgStatus);
                            mActivity.getMyTabView().hideRedPoint();
                        }
                    }

                } else {
                    IntentUtils.startLoginActivity(getActivity(), true);
                }
                break;
//            case R.id.mine_free_container:
//                IntentUtils.startADActivity(getActivity(), null, SharePreUtils.getInstance().getMobileEnterUrl(), null, null, "任我看", "", "", "", null, null);
//                break;
            case R.id.mine_feedback_container:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_FEEDBACK, PageIdConstants.PAGE_MY);
                IntentUtils.startUserFeedbackActivity(getActivity());
                break;
            case R.id.mine_mark_container:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_MARK, PageIdConstants.PAGE_MY);
                Intent intent = verifyMarketIntent();
                if (intent == null) {
                    return;
                }
                startActivity(verifyMarketIntent());
                break;
            case R.id.mine_setting_container:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_SETTING, PageIdConstants.PAGE_MY);
                IntentUtils.startSettingActivity(getActivity());
                break;
            case R.id.mine_subscribe_container:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_SUB, PageIdConstants.PAGE_MY);
                if (!mHasLogin) {
                    IntentUtils.startLoginActivity(getActivity(), true);
                } else {
                    IntentUtils.startMySubscriptionActivity(getActivity());
                }
                break;
            case R.id.tv_unlogin_info:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_SUB_LOGIN, PageIdConstants.PAGE_MY);
                IntentUtils.startLoginActivity(getActivity(), true);
                break;
            case R.id.mine_member_container:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_IFENG_VIP, PageIdConstants.PAGE_MY);
                if (User.isVip()) {
                    IntentUtils.startMemberCenterActivity(getActivity());
                } else {
                    IntentUtils.startOpenMemberActivity(getActivity());
                }
                break;
            case R.id.txt_user_sign_in:
                PageActionTracker.clickBtn(ActionIdConstants.MY_CLICK_SIGN_IN, PageIdConstants.PAGE_MY);
                if (mHasSign) {
                    ToastUtils.getInstance().showShortToast(R.string.user_center_has_sign_in_toast);
                } else {
                    UserPointManager.addRewardsForSignIn(IfengApplication.getAppContext(), UserPointManager.PointType.addBySignIn, new UserPointManager.PointPostCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            requestPointCount();
                        }

                        @Override
                        public void onFail(String result) {

                        }
                    });
                }
                break;
            case R.id.txt_user_coin_cnt:
                PageActionTracker.clickBtn(ActionIdConstants.MY_CLICK_POINT, PageIdConstants.PAGE_MY);
                IntentUtils.startPointTaskActivity(getActivity());
                break;
            case R.id.iv_edit_nick_name:
            case R.id.rl_user_name:
                mUserNameText.requestFocus();
                InputMethodManager inManager = (InputMethodManager) mUserNameText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inManager.showSoftInput(mUserNameText, 0);
                break;
            default:
                break;
        }
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING) {
                    mHasLogin = true;
                    mySubscribeWeMedia();//如果用户登录则获取订阅内容
                    requestPointCount();
                    if (mUserNameText != null) {
                        mUserNameText.setText(user.getUserName());
                    }
                    ToastUtils.getInstance().showShortToast("登录成功");
                    tv_login_info.setVisibility(View.GONE);

                    setUserHeadIcon(user.getUserIcon());
                    mSignAndPoint.setVisibility(View.VISIBLE);

                } else if (loginState == IntentKey.UNLOGIN) {
                    mHasLogin = false;
                    tv_login_info.setVisibility(View.VISIBLE);
                    mUserHeadImg.setImageResource(R.drawable.icon_unlogin_header);
                    mUserNameText.setText(R.string.mine_username);
                    subscribeList.clear();
                    mSignAndPoint.setVisibility(View.GONE);
                }
                showLayout();
            }
        }
    };

    private void setUserHeadIcon(String iconUrl) {
        if (TextUtils.isEmpty(iconUrl)) {
            mUserHeadImg.setImageResource(R.drawable.icon_unlogin_header);
            return;
        }
        VolleyHelper.getImageLoader().get(iconUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    Bitmap bitmap = BitmapUtils.makeRoundCorner(response.getBitmap());
                    mUserHeadImg.setImageBitmap(bitmap);
                } else {
                    mUserHeadImg.setImageResource(R.drawable.icon_login_default_header);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                mUserHeadImg.setImageResource(R.drawable.icon_login_default_header);
                logger.debug("error={}", error);
            }
        });
    }

    private void checkIsNickName() {

        if (User.isLogin()) {
            if (user != null && (StringUtils.isMobileNumberAll(user.getUserName()) || StringUtils.isEmail(user.getUserName()))) {
                requestCheckNickName(user.getIfengUserName(), User.getIfengToken(), User.getRealNameStatus());
            }
        } else {
            mHasLogin = false;
            tv_login_info.setVisibility(View.VISIBLE);
            mUserHeadImg.setImageResource(R.drawable.icon_unlogin_header);
            mUserNameText.setText(R.string.mine_username);

            //Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);
            //loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.UNLOGIN);
            //getActivity().sendBroadcast(loginBroad);
        }
    }

    private void requestCheckNickName(final String useName, final String token, final String realNameStatus) {
        LoginDao.requestIfengUser(token, PhoneConfig.userKey, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("mine", "" + response.toString());

                        int code = getStateCode(response);
                        if (code == 1) {
                            User user;
                            String name;
                            String nickname = getDataContent(response, "nickname");
                            String nicknameStatus = getDataContent(response, "nicknameStatus");
                            String imageUrl = getDataContent(response, "image");
                            String imageStatus = getDataContent(response, "imageStatus");
                            String username = getDataContent(response, "username");
                            String guid = getDataContent(response, "guid");
                            int vipStatus;
                            String vipExp = "";
                            String sign = "";
                            JSONObject object = JSONObject.parseObject(response);
                            vipStatus = object.getIntValue("VIPStatus");
                            vipExp = object.getString("VIPEXP");
                            sign = object.getString("sign");

                            if (!StringUtils.isBlank(nickname) && !nickname.equals("null")) {
                                name = nickname;
                            } else if (!StringUtils.isBlank(username) && !username.equalsIgnoreCase("null")) {
                                name = username;
                            } else {
                                name = "ifengvideo" + guid;
                            }
                            if ("0".equals(nicknameStatus)) {
                                String tmpName;
                                if (TextUtils.isEmpty(User.getUserName())) {
                                    tmpName = nickname;
                                } else {
                                    tmpName = User.getUserName();
                                }
                                if (checkNickname(tmpName)) {
                                    requestNickNameUrl(tmpName, false);
                                } else {
                                    Log.d("mine", "昵称不正确");
                                    mUserNameText.requestFocus();
                                }
                            }
                            if ("0".equals(imageStatus)) {
                                if (TextUtils.isEmpty(User.getUserIcon())) {
                                    requestUploadHeaderFileUrl(imageUrl);
                                }
                            }


                            if (!TextUtils.isEmpty(imageUrl)) {
                                imageUrl = getEscapeString(imageUrl);
                            }
                            user = new User(name, imageUrl, token, getActivity(), guid, null, useName, realNameStatus, vipStatus, vipExp, getDataContent(response, "timestamp"), sign);
                            user.storeUserInfo();

                            Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);
                            loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.LOGINING);
                            getActivity().sendBroadcast(loginBroad);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            logger.error("requestCheckNickName error ! {}", error.toString(), error);
                        }
                    }
                });
    }

    private int getStateCode(String jsonString) {
        int code = 2; // 不能用默认0，因为0,1，-1，都为状态码。
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            code = jsonObject.getInteger("code");
        } catch (Exception e) {
            logger.error("getStateCode error ! {}", e.toString(), e);
        }
        return code;
    }

    /**
     * 获得登录返回结果的详细信息
     */
    private String getDataContent(String jsonString, String content) {
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            JSONObject jsonObject2 = jsonObject.getJSONObject("data");
            return jsonObject2.getString(content);
        } catch (Exception e) {
            logger.error("getDataContent error ! {}", e.toString(), e);
            return null;
        }
    }

    /**
     * Ifeng登陆查询用户信息的头像图片时做的相应的解码工作
     */
    private String getEscapeString(String escape) {
        try {
            return URLDecoderUtils.decodeInUTF8(escape);
        } catch (UnsupportedEncodingException e) {
            logger.error("getEscapeString error ! {}", e.toString(), e);
            return escape;
        }
    }

    private void updateMarkVisibility() {
        if (mMarkViewGroup == null) {
            return;
        }
        if (PhoneConfig.isGooglePlay() || PhoneConfig.isGionee()) {
            mMarkViewGroup.setVisibility(View.GONE);
            return;
        }
        if (verifyMarketIntent() != null) {
            mMarkViewGroup.setVisibility(View.VISIBLE);
            mark_under_line.setVisibility(View.VISIBLE);
        } else {
            mMarkViewGroup.setVisibility(View.GONE);
            mark_under_line.setVisibility(View.GONE);
        }
    }

    private Intent verifyMarketIntent() {
        if (getApp() == null) {
            return null;
        }
        Uri uri = Uri.parse("market://details?id=" + getApp().getPackageName());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        PackageManager packageManager = this.getApp().getPackageManager();
        if (intent.resolveActivity(packageManager) == null) {
            return null;
        }
        return intent;
    }

    /**
     * 更新页面数据
     */
    @Override
    public void refreshData() {
        initData();
        checkIsNickName();
    }

    /**
     * 展示布局判断条件
     */
    private void showLayout() {
        boolean isSubOver3 = subscribeList.size() >= 3;
        boolean isShowRecommendView = recommendList.size() > 0;
        showLayout(mHasLogin, isSubOver3, isShowRecommendView);
    }

    /**
     * 展示订阅内容信息
     *
     * @param isLogin        是否登录
     * @param isOver3        数量是否超过3个
     * @param isShowGameView 是否展示游戏布局
     */
    private void showLayout(boolean isLogin, boolean isOver3, boolean isShowGameView) {
        mShowSubscribeUnLogin.setVisibility(isLogin && isOver3 ? View.VISIBLE : View.GONE);
        gridView.setVisibility(isLogin && isOver3 ? View.VISIBLE : View.GONE);
        view_subscribe_underline.setVisibility(isLogin && !isOver3 ? View.GONE : View.VISIBLE);
        tv_unlogin_info.setVisibility(isLogin ? View.GONE : View.VISIBLE);

        if (PhoneConfig.isGooglePlay()) {
            listView_recommend.setVisibility(View.GONE);
            rl_recommend_separate.setVisibility(View.GONE);
        } else {
            listView_recommend.setVisibility(isShowGameView ? View.VISIBLE : View.GONE);
            rl_recommend_separate.setVisibility(isShowGameView ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * 获取我的推荐数据
     */
    private void getWebPush() {
        CommonDao.sendRequest(DataInterface.WE_RECOMMEND_INFO, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            showLayout();
                            return;
                        }

                        JSONArray jsonArray = null;
                        try {
                            jsonArray = JSONArray.parseArray(response.toString());
                            if (jsonArray == null || jsonArray.size() <= 0) {
                                showLayout();
                                return;
                            }
                            List<WebPushData.WebPushEntity> entityList = new ArrayList<WebPushData.WebPushEntity>();

                            for (Object aJsonArray : jsonArray) {
                                WebPushData.WebPushEntity entity = new WebPushData.WebPushEntity();
                                JSONObject jsonObject = (JSONObject) aJsonArray;
                                entity.setTitle(jsonObject.getString("title"));
                                entity.setUrl(jsonObject.getString("url"));
                                entity.setImage(jsonObject.getString("image"));
                                entity.setIsNeedParameters(jsonObject.getString("isNeedParameters"));
                                entity.setIsOpenInApp(jsonObject.getString("isOpenInApp"));
                                entityList.add(entity);
                            }

                            if (ListUtils.isEmpty(entityList)) {
                                showLayout();
                            } else {
                                recommendList = entityList;
                                recommendListAdapter.setList(recommendList);
                                showLayout();
                            }
                        } catch (Exception e) {
                            showLayout();
                            logger.error("getWebPush error ! {} ", e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showLayout();
                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);
    }

    /**
     * 获取我的订阅数据
     */
    private void mySubscribeWeMedia() {
        WeMediaDao.getUserSubscribeWeMediaList(
                new User(getActivity()).getUid(),
                "18", "",
                System.currentTimeMillis() + "",
                SubscribeList.class,
                new Response.Listener<SubscribeList>() {
                    @Override
                    public void onResponse(SubscribeList response) {
                        subscribeList.clear();
                        if (response != null && !ListUtils.isEmpty(response.getWeMediaList())) {
                            subscribeList.addAll(response.getWeMediaList());
                        }
                        showLayout();
                        subscribeAdapter.setList(subscribeList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        subscribeList.clear();
                        showLayout();
                    }
                }, false);
    }

    private void requestMobileOrderStatus() {
        MobileDao.requestMobielOrderStatus(PhoneConfig.userKey,
                SharePreUtils.getInstance().getMobilePcid(),
                MobileUserModel.MobileOrder.class,
                new Response.Listener<MobileUserModel.MobileOrder>() {
                    @Override
                    public void onResponse(MobileUserModel.MobileOrder response) {
                        if (response == null) {
                            logger.debug("mobileOrder is null");
                            return;
                        }
//                        Log.d("mobileOrder :", response.toString());
                        int resultStatus = response.getResult();
                        SharePreUtils.getInstance().setMobileOrderStatus(resultStatus);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("mobileOrderError is " + error.getMessage());
                    }
                });
    }

    private void requestMobileFreeVideoEnterUrl() {
        MobileDao.requestMobileFreeVideoEnterUrl(PhoneConfig.userKey, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                logger.debug("mobileEnterUrl:{}", response == null ? "is null" : response.toString());
                if (response == null) {
                    return;
                }
                com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(response.toString());
                String url = jsonObject.getString("resultUrl");
                SharePreUtils.getInstance().setMobileEnterUrl(url);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("mobileEnterUrl: {}", error.getMessage());
            }
        });
    }

    private void requestPointCount() {
        if (!User.isLogin()) {
            return;
        }

        String url = String.format(DataInterface.GET_POINT_TASK_POINT_COUNT, new User(getActivity()).getUid(), PhoneConfig.mos, PhoneConfig.userKey, "", User.getIfengToken());
        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || !isAdded()) {
                            return;
                        }
                        JSONObject jsonObject = JSON.parseObject(response.toString());
                        int code = jsonObject.getIntValue("code");
                        if (200 == code) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            int count = data.getIntValue("credit_num");
                            JSONObject signInfo = data.getJSONObject("signin_info");
                            mHasSign = signInfo.getBoolean("signin_today");
                            int signDays = signInfo.getIntValue("signin_days");
                            mPoint.setText(String.format(getString(R.string.user_center_point_count), count));
                            if (mHasSign) {
                                mSign.setText(String.format(getString(R.string.user_center_sign_days), signDays));
                                if (null != mListener) {
                                    mListener.onClickPoint();
                                }
                            } else {
                                mSign.setText(getString(R.string.mine_sign_text));
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }


    private void requestMsgCount() {
        if (!User.isLogin()) {
            return;
        }

        String timer = "";
        if (SharePreUtils.getInstance().getMsgTimer() != null && 0l != SharePreUtils.getInstance().getMsgTimer()) {
            timer = String.valueOf(SharePreUtils.getInstance().getMsgTimer());
        }

        CommentDao.userVideoMsg(User.getUid(), timer, CommentsModel.class,
                new Response.Listener<CommentsModel>() {
                    @Override
                    public void onResponse(CommentsModel response) {
                        if (response != null && !ListUtils.isEmpty(response.getComments())) {
                            List<CommentsModel.Comment> list = handleData(response.getComments());
                            if (!ListUtils.isEmpty(list)) {
                                tv_msg.setText(list.size() + "");
                                tv_red.setVisibility(View.VISIBLE);
                                mActivity.getMyTabView().showRedPoint();
                                msgStatus = true;
                                logger.debug("Msg:{}", response.toString());
                            }
                        } else {
                            tv_msg.setText("0");
                            msgStatus = false;
                            mActivity.getMyTabView().hideRedPoint();
                            tv_red.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("MsgError:{}", error.getMessage());
                    }
                });
    }

    private List<CommentsModel.Comment> handleData(List<CommentsModel.Comment> list) {
        List<CommentsModel.Comment> tempList = new ArrayList<>();
        for (CommentsModel.Comment comment : list) {
            if (comment == null
                    || TextUtils.isEmpty(comment.getDoc_url())
                    || comment.getDoc_url().contains("http")
                    || comment.getDoc_url().contains("sub_")
                    || comment.getDoc_url().contains("ifengnews_")) {
                continue;
            }
            tempList.add(comment);
        }
        return tempList;
    }

    public interface onClickPointListener {
        void onClickPoint();
    }

    public void setOnLineText(int signDays) {
        int readDays = signDays + 1;
        mSign.setText(String.format(getString(R.string.user_center_sign_days), readDays));
        mHasLogin = true;
    }

    public void setEditTextStatus() {
        initUserEditText();

    }


    private static final String IMAGE_FILE_NAME = "temp_head_image.jpg";

    private Uri imageuri;

    private void createDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.shareDialogTheme);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setContentView(R.layout.header_image_dialog_layout);

        Button alarmBtn = (Button) window.findViewById(R.id.alarm_btn);
        Button captureBtn = (Button) window.findViewById(R.id.capture_btn);
        TextView cancelTV = (TextView) window.findViewById(R.id.cancel_tv);

        File outputfile = new File(mActivity.getExternalCacheDir(), "output.jpg");
        try {
            if (outputfile.exists()) {
                outputfile.delete();
            }
            outputfile.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(mActivity,
                    "com.ifeng.newvideo.fileprovider", //可以是任意字符串
                    outputfile);
        } else {
            imageuri = Uri.fromFile(outputfile);
        }


        alarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
                startActivityForResult(intent, RESULT_CODE_ALBUM);
                dialog.dismiss();
            }
        });

        captureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, RESULT_CODE_CAPTURE);
                dialog.dismiss();
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_CODE_ALBUM:
                if (resultCode == RESULT_OK && null != data) {
                    startActivityForResult(PictureUtils.getInstance().CutForPhoto(data.getData()), RESULT_CODE_CROP_BY_ALBUM);
                }
                break;
            case RESULT_CODE_CAPTURE:
                if (resultCode == RESULT_OK) {
                    String path = getActivity().getExternalCacheDir().getPath();
                    String name = "output.jpg";
                    startActivityForResult(PictureUtils.getInstance().CutForCamera(getActivity(), path, name), RESULT_CODE_CROP_BY_CAPTURE);
                }
                break;
            case RESULT_CODE_CROP_BY_CAPTURE:
            case RESULT_CODE_CROP_BY_ALBUM:
                if (resultCode == RESULT_OK) {
                    setPicToView(data);
                }
                break;
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        try {
            super.startActivityForResult(intent, requestCode);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void setPicToView(Intent picdata) {
        Bitmap photo = null;
        Uri dataUri = null;
        ContentResolver resolver = getActivity().getContentResolver();
        if (null == picdata || picdata.getData() == null) {
            try {
                //用文件的绝对路径替代data.getExtra()
                photo = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(PictureUtils.getInstance().mCutUri));
                dataUri = Uri.parse(MediaStore.Images.Media.insertImage(resolver, photo, null, null));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            dataUri = picdata.getData();
        }

        if (dataUri != null || !EmptyUtils.isEmpty(dataUri)) {
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    File imgUri = new File(GetImagePath.getPath(getActivity(), dataUri));
                    dataUri = FileProvider.getUriForFile(getActivity(), "com.ifeng.newvideo.fileprovider", imgUri);
                    photo = PictureUtils.getInstance().decodeUriAsBitmap(getActivity(), dataUri);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            } else {
                photo = PictureUtils.getInstance().decodeUriAsBitmap(getActivity(), dataUri);
            }
        }

        if (null != photo) {
            final Bitmap finalPhoto = photo;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mUserHeadImg.setImageBitmap(BitmapUtils.makeRoundCorner(finalPhoto));
                    mHeaderImageBg.setVisibility(View.VISIBLE);
                    mHeaderImageBg.setImageBitmap(BitmapUtils.gsBlurFilter(finalPhoto));
                    saveImageToGallery(getActivity(), finalPhoto);
                    finalPhoto.recycle();
                }
            }, 300);
        }

        PictureUtils.getInstance().deleteTempFile();


    }

    public void saveImageToGallery(Context context, Bitmap bmp) {
        lastImagePath = PictureUtils.getInstance().saveImageToGallery(context, bmp);
        if (!TextUtils.isEmpty(lastImagePath)) {
            fileId = UploadUtil.getInstance().shaHexForTransmission(lastImagePath) + "_1";
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String result;
                    result = UploadUtil.getInstance().uploadFile(new File(lastImagePath), DataInterface.FILE_UPLOAD_URL);
                    uploadResult[0] = result;
                    mHandler.sendEmptyMessage(RESULT_FILE_UPLOAD);
                }
            }).start();
        }

    }


    private void requestFileUploadPath() {
        String url = String.format(DataInterface.HEADER_FILE_INFO_URL, fileId, "1");

        CommonDao.sendRequest(url, null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        try {
                            org.json.JSONObject jsonObject = new org.json.JSONObject(response.toString());
                            if (jsonObject.getString("success").equals("true")) {
                                filePath = jsonObject.getString("fileUrl");
                                Log.d("file", "---" + filePath);
                                requestUploadHeaderFileUrl(filePath);
                            } else {
                                filePath = "";
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);

    }

    private void requestUploadHeaderFileUrl(String filePath) {
        String url = String.format(DataInterface.UPLOAD_HEADER_IMAGE_URL, User.getIfengToken(), filePath);
        Log.d("file", "uploadFileUrl:" + url);

        CommonDao.sendRequest(url, null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        Log.d("file", "--" + response.toString());

                        try {
                            org.json.JSONObject jsonObject = new org.json.JSONObject(response.toString());
                            if (jsonObject.getString("code").equals("1")) {
                                Log.d("file", "---" + jsonObject.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);
    }

    private boolean checkNickname(String content) {
        if (!TextUtils.isEmpty(content)) {
            try {
                Log.d("mine", "" + content.getBytes("GBK").length);
                if (content.getBytes("GBK").length > 24) {
                    ToastUtils.getInstance().showShortToast("字数超出上限了哦！");
                    return false;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (!checkNickName(content)) {
            return false;
        }
        return true;
    }


    private void requestNickNameUrl(final String content, final boolean secondCheck) {
        if (!User.isLogin()) {
            mUserNameText.setText(preUserName);
            return;
        }
        if (TextUtils.isEmpty(content)) {
            ToastUtils.getInstance().showShortToast("昵称不可以为空");
            mUserNameText.setText(preUserName);
            return;
        }

        if (!TextUtils.isEmpty(content)) {
            try {
                Log.d("mine", "" + content.getBytes("GBK").length);
                if (content.getBytes("GBK").length > 24) {
                    ToastUtils.getInstance().showShortToast("字数超出上限了哦！");
                    return;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (!checkNickName(content)) {
            ToastUtils.getInstance().showShortToast("名字格式不对哦，请重新输入");
            return;
        }
        if (preUserName.equals(content) && !secondCheck) {
            ToastUtils.getInstance().showShortToast("昵称未修改");
            initUserEditText();
            return;
        }

        String encodedUserName = content;
        try {
            encodedUserName = URLEncoderUtils.encodeInUTF8(content);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = String.format(DataInterface.UPLOAD_NICK_NAME, User.getIfengToken(), encodedUserName);
        Log.d("mine", url + "");

        CommonDao.sendRequest(url,
                null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {

                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }

                        Log.d("mine", "--" + response.toString());

                        String nickName;
                        try {
                            org.json.JSONObject jsonObject = new org.json.JSONObject(response.toString());
                            if (jsonObject.getString("code").equals("1")) {
                                preUserName = mUserNameText.getText().toString();
                                initUserEditText();
                                ToastUtils.getInstance().showShortToast("修改成功");
                                UserPointManager.addRewards(UserPointManager.PointType.addByChangeNickName);

                            } else {
                                String msgCode = jsonObject.getString("msgcode");
                                switch (msgCode) {
                                    case "5002":
                                        ToastUtils.getInstance().showShortToast("该用户名已被占用");
                                        userNameRequesFocus();
                                        break;
                                    case "5003":
                                        ToastUtils.getInstance().showShortToast(jsonObject.getString("message"));
                                        userNameRequesFocus();
                                        break;
                                    case "5004":
                                        ToastUtils.getInstance().showShortToast(jsonObject.getString("message"));
                                        userNameRequesFocus();
                                        break;
                                    default:
                                        break;
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    private void userNameRequesFocus() {
        mUserNameText.setText(User.getUserName());
        mUserNameText.requestFocus();
        rl_user_name.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        mUserNameText.setCursorVisible(true);
        mUserNameText.setSelection(User.getUserName().length());
        InputMethodManager inManager = (InputMethodManager) mUserNameText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inManager.showSoftInput(mUserNameText, 0);
    }

    private void initUserEditText() {
        rl_user_name.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        mUserNameText.clearFocus();
        mUserNameText.setCursorVisible(false);
    }

    private boolean checkNickName(String content) {
        String nickNameRegex = "^[a-z0-9\\-_\\x{4e00}-\\x{9fa5}]{2,24}$";
        String pattern = "^[a-z0-9\\-_\\x{4e00}-\\x{9fa5}]{2,24}$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(content);
        return m.matches();
    }

    public String subStr(String str, int subSLength) throws UnsupportedEncodingException {
        if (str == null)
            return "";
        else {
            int tempSubLength = subSLength;//截取字节数
            String subStr = str.substring(0, str.length() < subSLength ? str.length() : subSLength);//截取的子串
            int subStrByetsL = subStr.getBytes("GBK").length;//截取子串的字节长度
            //int subStrByetsL = subStr.getBytes().length;//截取子串的字节长度
            // 说明截取的字符串中包含有汉字
            while (subStrByetsL > tempSubLength) {
                int subSLengthTemp = --subSLength;
                subStr = str.substring(0, subSLengthTemp > str.length() ? str.length() : subSLengthTemp);
                subStrByetsL = subStr.getBytes("GBK").length;
                //subStrByetsL = subStr.getBytes().length;
            }
            return subStr;
        }
    }


    public boolean isChinese(char c) {

        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);

        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS

                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS

                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A

                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION

                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION

                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {

            return true;

        }

        return false;
    }


}
