package com.ifeng.newvideo.ui.subscribe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.newvideo.widget.ViewPagerColumn;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import com.ifeng.video.dao.db.model.subscribe.WeMediaTypeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * 全部自媒体Activity
 * Created by Administrator on 2016/7/21.
 */
public class AllWeMediaActivity extends BaseFragmentActivity implements RequestData {
    private static final Logger logger = LoggerFactory.getLogger(AllWeMediaActivity.class);

    private UIStatusLayout uiStatusLayout;
    private PagerSlidingTabStrip tabStrip;
    private ViewPagerColumn viewPager;
    private ViewPagerAdapter adapter;
    private List<WeMediaTypeInfo.WeMediaInfoEntity> list = new ArrayList<>();//自媒体分类数据
    private final Map<Integer, Fragment> fragmentList = new WeakHashMap<>();
    String echid = "";
    /**
     * 凤凰卫视自媒体所在position
     */
    private int mIfengTVPosition = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_media_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        echid = getIntent().getStringExtra(IntentKey.E_CHID);
        initView();
        getWeMediaType();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int position = tabStrip.getCurrentPosition();
        if (fragmentList.get(position) != null) {
            BaseFragment currentFragment = (BaseFragment) fragmentList.get(position);
            currentFragment.readPageTime(position);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void requestData() {
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        getWeMediaType();

    }

    private void initView() {
        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.WEMEDIA_ALL);
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(getResources().getString(R.string.we_media_all));
        uiStatusLayout = (UIStatusLayout) findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tab_strip);
        viewPager = (ViewPagerColumn) findViewById(R.id.viewPager_fragment);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabStrip.setViewPager(viewPager);
        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                enableExitWithSlip(i == 0);
                if (list != null && list.size() > i && list.get(i) != null) {
                    String actionId = String.format(ActionIdConstants.CLICK_WEMEDIA_CID, list.get(i).getWeMediaCid());
                    PageActionTracker.clickBtn(actionId, PageIdConstants.WEMEDIA_ALL);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
    }

    /**
     * 获取自媒体分类
     */
    private void getWeMediaType() {
        WeMediaDao.getWeMediaType(System.currentTimeMillis() + "", WeMediaTypeInfo.class,
                new Response.Listener<WeMediaTypeInfo>() {
                    @Override
                    public void onResponse(WeMediaTypeInfo response) {
                        if (response != null && !ListUtils.isEmpty(response.getWeMediaInfo())) {
                            uiStatusLayout.setStatus(UIStatusLayout.ALL_GONE);
                            tabStrip.setVisibility(View.VISIBLE);
                            viewPager.setVisibility(View.VISIBLE);
                            list = response.getWeMediaInfo();
                            adapter.setList(list);
                            if (getIntent().getBooleanExtra(IntentKey.LOCATE_TO_TV, false)) {
                                viewPager.setCurrentItem(mIfengTVPosition);
                            }else {
                                //默认报一次选中事件
                                String actionId = String.format(ActionIdConstants.CLICK_WEMEDIA_CID, list.get(0).getWeMediaCid());
                                PageActionTracker.clickBtn(actionId, PageIdConstants.WEMEDIA_ALL);
                            }
                        } else {
                            uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error instanceof NetworkError) {
                            uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
                        } else {
                            uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                        }
                    }
                },
                false);
    }

    /**
     * ViewPager适配器，装载的是Fragment
     * Created by Administrator on 2016/7/21.
     */
    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<WeMediaTypeInfo.WeMediaInfoEntity> list = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Fragment getItem(int position) {
            String weMediaCid = list.get(position).getWeMediaCid();
            WeMediaTypeFragment fragment = WeMediaTypeFragment.newInstance(weMediaCid, echid);
            fragmentList.put(position, fragment);
            return fragment;
        }

        public void setList(List<WeMediaTypeInfo.WeMediaInfoEntity> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (WeMediaInfoList.WE_MEDIA_CID_IFENG.equals(list.get(position).getWeMediaCid())) {
                mIfengTVPosition = position;
            }
            return list.get(position).getName();
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            tabStrip.notifyDataSetChanged();
        }
    }
}
