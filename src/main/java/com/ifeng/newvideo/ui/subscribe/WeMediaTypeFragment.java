package com.ifeng.newvideo.ui.subscribe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.subscribe.adapter.WeMediaListAdapter;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshGridView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import com.ifeng.video.dao.db.model.subscribe.WeMediaList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 自媒体分类Fragment
 * Created by Administrator on 2016/7/21.
 */
public class WeMediaTypeFragment extends BaseFragment implements View.OnClickListener, WeMediaListAdapter.ClickListener {
    private static final Logger logger = LoggerFactory.getLogger(WeMediaTypeFragment.class);

    private View loading_layout;//加载层
    private View load_fail_layout;//加载失败层
    private View no_net_layer;//无网层
    private PullToRefreshGridView gridView;

    private WeMediaListAdapter adapter;
    private List<WeMediaList.WeMediaListEntity> handleList = new ArrayList<>();//最后经过过滤，去重之后的List
    private WeMediaList.WeMediaListEntity entity;

    private String weMediaCid;//自媒体分类id
    private String echid;
    private String positionId = "";
    private int currentPage = 1;//当前页码

    private boolean isClickSubscribeBtn = false;//是否点击了订阅按钮

    public static WeMediaTypeFragment newInstance(String weMediaCid, String echid) {
        WeMediaTypeFragment fragment = new WeMediaTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IntentKey.CHANNELID, weMediaCid);
        bundle.putString(IntentKey.E_CHID, echid);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            weMediaCid = bundle.getString(IntentKey.CHANNELID);
            echid = bundle.getString(IntentKey.E_CHID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media_type_layout, container, false);
        initView(view);
        initAdapter();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerLoginBroadcast();
        showStatusLayout(LOADING);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isClickSubscribeBtn) {
            return;
        }
        refreshData();
    }

    /**
     * 刷新数据
     */
    private void refreshData() {
        currentPage = 1;
        positionId = "";
        getWeMediaList(weMediaCid);
    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(mLoginReceiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.load_fail_layout:
            case R.id.no_net_layer:
                showStatusLayout(LOADING);
                refreshData();
                break;
        }
    }

    @Override
    public void onSubscribeClickListener(WeMediaList.WeMediaListEntity entity) {
        if (!NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        this.entity = entity;
        if (User.isLogin()) {
            int state = entity.getFollowed();
            state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            subscribe(entity, getUid(), state);
            PageActionTracker.clickWeMeidaSub(state == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.WEMEDIA_ALL);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(getActivity());
        }
    }

    @Override
    public void onItemClickListener(WeMediaList.WeMediaListEntity entity) {
        IntentUtils.startWeMediaHomePageActivity(getActivity(), entity.getWeMediaID() + "", echid);
    }

    /**
     * 注册登录广播
     */
    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        getActivity().registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING && entity != null) {
                    if (isClickSubscribeBtn) {//如果点击了订阅按钮则处理，否则不处理
                        subscribe(entity, getUid(), SubscribeRelationModel.SUBSCRIBE);
                    }
                    isClickSubscribeBtn = false;
                } else {
                    isClickSubscribeBtn = false;
                    refreshData();
                }
            }
        }
    };

    private void initView(View view) {
        loading_layout = view.findViewById(R.id.loading_layout);
        load_fail_layout = view.findViewById(R.id.load_fail_layout);
        load_fail_layout.setOnClickListener(this);
        no_net_layer = view.findViewById(R.id.no_net_layer);
        no_net_layer.setOnClickListener(this);
        gridView = (PullToRefreshGridView) view.findViewById(R.id.gridView);
        gridView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        gridView.getRefreshableView().setNumColumns(3);//展示3列
        gridView.setShowIndicator(false);

        gridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                getWeMediaList(weMediaCid);
            }
        });
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        adapter = new WeMediaListAdapter();
        adapter.setClickListener(this);
        gridView.getRefreshableView().setAdapter(adapter);
    }

    /**
     * 获取自媒体
     */
    private void getWeMediaList(String weMediaCid) {
        WeMediaDao.getWeMediaTypeList(
                weMediaCid,
                getUid(),
                currentPage + "",
                DataInterface.PAGESIZE_18,
                positionId,
                System.currentTimeMillis() + "",
                WeMediaList.class,
                new Response.Listener<WeMediaList>() {
                    @Override
                    public void onResponse(WeMediaList response) {
                        gridView.onRefreshComplete();
                        handleResponseEvent(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        gridView.onRefreshComplete();
                        handleErrorEvent(error);
                    }
                }, false);

        if (currentPage != 1) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.WEMEDIA_ALL);
        }
    }

    /**
     * 处理响应事件
     */
    private void handleResponseEvent(WeMediaList response) {
        if (response != null && !ListUtils.isEmpty(response.getWeMediaList())) {
            if (currentPage == 1) {//如果当前是第一页，清除之前的数据
                handleList.clear();
            }
            if (handleData(response.getWeMediaList()).size() > 0) {
                handleList.addAll(handleData(response.getWeMediaList()));//数据处理
            }
            showStatusLayout(NORMAL);
            adapter.setList(handleList);
            currentPage++;
        } else {
            if (currentPage == 1) {
                showStatusLayout(ERROR);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 处理错误响应
     */
    private void handleErrorEvent(VolleyError error) {
        if (currentPage == 1) {
            if (error != null && error instanceof NetworkError) {
                showStatusLayout(NO_NET);
            } else {
                showStatusLayout(ERROR);
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    @Override
    protected void requestNet() {
    }

    /**
     * 对服务端返回数据进行过滤，去重处理
     *
     * @param list 服务端返回的数据
     */
    private List<WeMediaList.WeMediaListEntity> handleData(List<WeMediaList.WeMediaListEntity> list) {
        List<WeMediaList.WeMediaListEntity> weMediaList = new ArrayList<>();
        for (WeMediaList.WeMediaListEntity entity : list) {//数据过滤、去重
            if (TextUtils.isEmpty(entity.getWeMediaID()) || TextUtils.isEmpty(entity.getName()) || handleList.contains(entity)) {
                continue;
            }
            weMediaList.add(entity);
        }
        if (list.size() > 0) {
            positionId = list.get(list.size() - 1).getWeMediaID();
        }
        return weMediaList;
    }

    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final WeMediaList.WeMediaListEntity entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(entity.getWeMediaID(), uid, type, System.currentTimeMillis() + "", SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        //refreshData();//订阅成功之后不重新请求数据
                        int followNo = 0;
                        if (TextUtils.isDigitsOnly(entity.getFollowNo())) {
                            followNo = Integer.parseInt(entity.getFollowNo());
                        }
                        boolean isSubscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        entity.setFollowed(type);
                        entity.setFollowNo(String.valueOf(isSubscribe ? (++followNo) : (--followNo)));
                        adapter.notifyDataSetChanged();
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, isSubscribe, entity.getName());
                        ToastUtils.getInstance().showShortToast(isSubscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    private void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(getActivity(), action, title, "");
    }

    /**
     * 获取当前用户id
     */
    private String getUid() {
        String uid = new User(IfengApplication.getInstance()).getUid();
        return TextUtils.isEmpty(uid) ? "" : uid;
    }

    private static final int LOADING = 0;//加载中
    private static final int NORMAL = 1;//正常
    private static final int ERROR = 2;//数据错误
    private static final int NO_NET = 3;//无网

    /**
     * 显示状态布局
     */
    private void showStatusLayout(int status) {
        switch (status) {
            case LOADING:
                loading_layout.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                load_fail_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NORMAL:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
                load_fail_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case ERROR:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
                load_fail_layout.setVisibility(View.VISIBLE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NO_NET:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
                load_fail_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.VISIBLE);
                break;
        }
    }
}
