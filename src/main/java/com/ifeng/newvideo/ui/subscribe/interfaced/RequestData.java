package com.ifeng.newvideo.ui.subscribe.interfaced;

/**
 * 该接口用于处理无网、数据加载失败重新请求数据
 * Created by Administrator on 2016/8/19.
 */
public interface RequestData {
    void requestData();
}
