package com.ifeng.newvideo.ui.subscribe;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.adapter.WeMediaHomePageListAdapter;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.newvideo.videoplayer.activity.widget.WeMediaHeadView;
import com.ifeng.newvideo.videoplayer.activity.widget.WeMediaHotNewView;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BitmapUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 自媒体主页
 * Created by Administrator on 2016/7/21.
 */
public class WeMediaHomePageActivity extends BaseFragmentActivity implements View.OnClickListener, RequestData, PullToRefreshBase.OnRefreshListener, AbsListView.OnScrollListener {
    private static final Logger logger = LoggerFactory.getLogger(WeMediaHomePageActivity.class);

    private RelativeLayout rl_back;//返回
    private ImageView iv_back;
    private UIStatusLayout uiStatusLayout;
    private TextView tv_subscribe;//订阅
    //    private ImageView iv_subscibe;
    private RelativeLayout rl_subscribe;
    private NetworkImageView img_banner;//背景图

    private WeMediaHeadView weMediaHeadView;
    private WeMediaHotNewView newHotView;

    private RelativeLayout rl_top_bar;
    private LinearLayout ll_listview_top;
    private View line;
    private RelativeLayout ll_new;//最新、最热布局
    private RelativeLayout ll_hot;//最新、最热布局
    private TextView tv_new;//最新
    private TextView tv_hot;//最热

    private PullToRefreshListView listView;//视频列表
    private LinearLayout ll_no_video;//未发布视频布局

    private WeMediaHomePageListAdapter adapter;
    private List<WeMediaInfoList.InfoListEntity.BodyListEntity> newList = new ArrayList<>();//最新List
    private List<WeMediaInfoList.InfoListEntity.BodyListEntity> hotList = new ArrayList<>();//最热List
    private WeMediaInfoList.InfoListEntity.WeMediaEntity weMediaEntity;

    private String weMediaId = "";
    private String type = WeMediaDao.WE_MEDIA_TYPE_NEW;//默认请求最新的数据
    private boolean isPullUpHot = false;//是否是上拉加载请求最热数据
    private boolean isPullUpNew = false;//是否是上拉加载请求最新数据
    private boolean isClickSubscribeBtn = false;//是否点击了订阅图标
    private int currentNewPage = 1;//当前最新页码
    private int currentHotPage = 1;//当前最热页码
    String echid;
    private int mHeight = 0;
    private int totalHeight = 0;

    private RelativeLayout backView;
    private NetworkImageView icon_header;
    private TextView tv_media_title;
    private LinearLayout ll_red_view;
    private View redView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wemedia_home_page_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        weMediaId = getIntent().getExtras().getString(IntentKey.WE_MEIDA_ID);
        echid = getIntent().getExtras().getString(IntentKey.E_CHID);
        registerLoginBroadcast();
        initView();
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        rl_back.setVisibility(View.GONE);
        getWeMediaInfo(weMediaId, type);

        ViewTreeObserver viewTreeObserver = weMediaHeadView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                weMediaHeadView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                totalHeight = weMediaHeadView.getHeight();
                mHeight = weMediaHeadView.getHeight() - rl_top_bar.getHeight();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mLoginReceiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icon_back:
            case R.id.rl_back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.WEMEDIA_HOME);
                finish();
                break;
            case R.id.tv_subscribe:
            case R.id.rl_subscribe:
                handleSubscribe();
                break;
            case R.id.tv_new:
            case R.id.ll_new:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA_NEW, PageIdConstants.WEMEDIA_HOME);
                type = WeMediaDao.WE_MEDIA_TYPE_NEW;
                isPullUpNew = false;
                currentNewPage = 1;
                newHotView.setType(WeMediaDao.WE_MEDIA_TYPE_NEW);
                getWeMediaInfo(weMediaId, type);
                tv_hot.setTextColor(getResources().getColor(R.color.black));
                tv_new.setTextColor(getResources().getColor(R.color.subscribe_text_color));
                startLineMoveLeft();
                break;
            case R.id.tv_hot:
            case R.id.ll_hot:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA_HOT, PageIdConstants.WEMEDIA_HOME);
                type = WeMediaDao.WE_MEDIA_TYPE_HOT;
                isPullUpHot = false;
                currentHotPage = 1;
                newHotView.setType(WeMediaDao.WE_MEDIA_TYPE_HOT);
                getWeMediaInfo(weMediaId, type);
                tv_hot.setTextColor(getResources().getColor(R.color.subscribe_text_color));
                tv_new.setTextColor(getResources().getColor(R.color.black));
                startLineMoveRight();
                break;
        }
    }

    @Override
    public void requestData() {
        currentNewPage = 1;
        currentHotPage = 1;
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        rl_back.setVisibility(View.GONE);
        getWeMediaInfo(weMediaId, type);
    }

    /**
     * 注册登录广播
     */
    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING && isClickSubscribeBtn && weMediaEntity != null) {
                    subscribe(weMediaEntity, getUserId(), SubscribeRelationModel.SUBSCRIBE);
                    isClickSubscribeBtn = false;
                } else {
                    isClickSubscribeBtn = false;
                }
            }
        }
    };

    private void initView() {
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);

        findViewById(R.id.icon_back).setOnClickListener(this);
        backView = (RelativeLayout) findViewById(R.id.backview);
        img_banner = (NetworkImageView) findViewById(R.id.img_banner);
        icon_header = (NetworkImageView) findViewById(R.id.icon_header);
        tv_media_title = (TextView) findViewById(R.id.tv_media_title);
        icon_header.setVisibility(View.GONE);
        tv_media_title.setVisibility(View.GONE);
        ll_red_view = (LinearLayout) findViewById(R.id.ll_red_view);
        redView = findViewById(R.id.ll_red_line);
        ll_red_view.setVisibility(View.GONE);
//        redView.setVisibility(View.GONE);

        iv_back = (ImageView) findViewById(R.id.icon_back);
        rl_top_bar = (RelativeLayout) findViewById(R.id.rl_top_bar);
        rl_top_bar.setOnClickListener(this);
        uiStatusLayout = (UIStatusLayout) findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        tv_subscribe = (TextView) findViewById(R.id.tv_subscribe);
        rl_subscribe = (RelativeLayout) findViewById(R.id.rl_subscribe);
        tv_subscribe.setOnClickListener(this);
        rl_subscribe.setOnClickListener(this);

        ll_listview_top = (LinearLayout) findViewById(R.id.ll_listview_top);
        ll_listview_top.setOnClickListener(this);
        ll_listview_top.setVisibility(View.GONE);
        line = findViewById(R.id.view_separate);
        line.setVisibility(View.GONE);
        ll_new = (RelativeLayout) findViewById(R.id.ll_new);
        tv_new = (TextView) findViewById(R.id.tv_new);
        tv_new.setOnClickListener(this);
        ll_new.setOnClickListener(this);
        //默认报一次点击事件
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA_NEW, PageIdConstants.WEMEDIA_HOME);
        tv_hot = (TextView) findViewById(R.id.tv_hot);
        ll_hot = (RelativeLayout) findViewById(R.id.ll_hot);
        tv_hot.setOnClickListener(this);
        ll_hot.setOnClickListener(this);
        ll_no_video = (LinearLayout) findViewById(R.id.ll_no_video);

        weMediaHeadView = new WeMediaHeadView(this);
        newHotView = new WeMediaHotNewView(this);
        newHotView.setMainActivity(this);
        newHotView.setWeMediaId(weMediaId);

        listView = (PullToRefreshListView) findViewById(R.id.listView);
        listView.setShowIndicator(false);
        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        listView.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_NEVER);

        listView.getRefreshableView().addHeaderView(weMediaHeadView);
        listView.getRefreshableView().addHeaderView(newHotView);

        listView.setOnRefreshListener(this);

        listView.setOnScrollListener(this);
        adapter = new WeMediaHomePageListAdapter();
        adapter.setEchid(echid);
        listView.setAdapter(adapter);
    }


    /**
     * 处理订阅逻辑
     */
    private void handleSubscribe() {
        if (!NetUtils.isNetAvailable(WeMediaHomePageActivity.this)) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        if (User.isLogin() && weMediaEntity != null) {
            int state = weMediaEntity.getFollowed();
            state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            subscribe(weMediaEntity, getUserId(), state);
            PageActionTracker.clickWeMeidaSub(state == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.WEMEDIA_HOME);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(WeMediaHomePageActivity.this);
        }
    }

    /**
     * 展示布局
     */
    private void showListView(boolean isShow) {
        ll_no_video.setVisibility(isShow ? View.GONE : View.VISIBLE);
        ll_new.setVisibility(isShow ? View.VISIBLE : View.GONE);
        listView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 设置订阅按钮样式
     *
     * @param isSubscribe 是否订阅标识
     */
    private void showSubscribeStyle(boolean isSubscribe) {
        tv_subscribe.setText(isSubscribe ? R.string.subscribed : R.string.subscribe);
        tv_subscribe.setBackgroundResource(isSubscribe ? R.drawable.btn_subscribe_shape_selected : R.drawable.btn_subscribe_shape);
    }

    public void getWeMediaInfoView(String weMediaId, String selectedType, boolean isForm) {
        if (isForm) {
            isPullUpNew = false;
            currentNewPage = 1;
        } else {
            isPullUpHot = false;
            currentHotPage = 1;
        }
        setType(selectedType);
        getWeMediaInfo(weMediaId, type);
    }

    public void setType(String clickType) {
        type = clickType;
        if (type.equals(WeMediaDao.WE_MEDIA_TYPE_NEW)) {
            tv_hot.setTextColor(getResources().getColor(R.color.black));
            tv_new.setTextColor(getResources().getColor(R.color.subscribe_text_color));
            startLineMoveLeft();
        } else {
            tv_hot.setTextColor(getResources().getColor(R.color.subscribe_text_color));
            tv_new.setTextColor(getResources().getColor(R.color.black));
            startLineMoveRight();
        }
    }


    /**
     * 获取自媒体信息及最新、最热数据
     *
     * @param type 请求数据类型，最新、最热
     */
    private void getWeMediaInfo(String weMediaId, final String type) {
        int currentPage = type.equals(WeMediaDao.WE_MEDIA_TYPE_NEW) ? currentNewPage : currentHotPage;
        WeMediaDao.getWeMediaList(weMediaId,
                getUserId(),
                type,
                currentPage + "",
                DataInterface.PAGESIZE_20,
                System.currentTimeMillis() + "",
                WeMediaInfoList.class,
                new Response.Listener<WeMediaInfoList>() {
                    @Override
                    public void onResponse(WeMediaInfoList response) {
                        listView.onRefreshComplete();
                        if (response != null && !ListUtils.isEmpty(response.getInfoList())) {
                            uiStatusLayout.setStatus(UIStatusLayout.ALL_GONE);//全部隐藏掉该布局中的内容
                            rl_back.setVisibility(View.GONE);
                            iv_back.setVisibility(View.VISIBLE);
                            rl_subscribe.setVisibility(View.VISIBLE);
                            handleWeMediaInfo(response);
                            if (type.equals(WeMediaDao.WE_MEDIA_TYPE_NEW)) {
                                handleNewResponse(response);
                            } else if (type.equals(WeMediaDao.WE_MEDIA_TYPE_HOT)) {
                                handleHotResponse(response);
                            }
                        } else {
                            if (currentNewPage == 1 || currentHotPage == 1) {
                                uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                                rl_back.setVisibility(View.VISIBLE);
                                iv_back.setVisibility(View.GONE);
                                rl_subscribe.setVisibility(View.GONE);
                            } else {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listView.onRefreshComplete();
                        handleErrorEvent(error);
                    }
                }, false);

        if (currentPage != 1) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.WEMEDIA_HOME);
        }
    }

    /**
     * 处理自媒体信息数据
     */
    private void handleWeMediaInfo(WeMediaInfoList response) {
        if (response.getInfoList().size() > 0 && response.getInfoList().get(0) != null) {//有自媒体信息数据
            weMediaEntity = response.getInfoList().get(0).getWeMedia();//获取自媒体信息，取infoList中第一条数据
            weMediaHeadView.initWeMediaData(weMediaEntity);
            setBannerBg(weMediaEntity.getHeadPic(), img_banner);//设置Banner背景
            setAvatar(icon_header, weMediaEntity.getHeadPic());
            tv_media_title.setText(weMediaEntity.getName());
            showSubscribeStyle(weMediaEntity.getFollowed() == SubscribeRelationModel.SUBSCRIBE);//是否订阅
        }
    }

    private void setAvatar(final NetworkImageView imageView, String url) {
        VolleyHelper.getImageLoader().get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    imageView.setImageBitmap(BitmapUtils.makeRoundCorner(response.getBitmap()));
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                imageView.setImageResource(R.drawable.icon_login_default_header);
            }
        });
    }


    /**
     * 处理最新响应事件
     */
    private void handleNewResponse(WeMediaInfoList response) {
        if (!isPullUpNew) {
            newList.clear();
        }
        List<WeMediaInfoList.InfoListEntity.BodyListEntity> handleList = handleData(response, NEW);
        if (handleList.size() > 0) {
            newList.addAll(handleList);
        } else {
            if (currentNewPage > 1) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
        showListView(newList.size() > 0);
        adapter.setList(newList);
        currentNewPage++;

    }

    /**
     * 处理最热响应事件
     */
    private void handleHotResponse(WeMediaInfoList response) {
        if (!isPullUpHot) {
            hotList.clear();
        }
        if (handleData(response, HOT).size() > 0) {
            hotList.addAll(handleData(response, HOT));
        } else {
            if (currentHotPage > 1) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
        showListView(hotList.size() > 0);
        adapter.setList(hotList);
        currentHotPage++;
    }

    /**
     * 处理Error事件
     */
    private void handleErrorEvent(VolleyError error) {
        if (currentNewPage == 1) {
            if (error != null && error instanceof NetworkError) {
                uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
                rl_back.setVisibility(View.VISIBLE);
            } else {
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                rl_back.setVisibility(View.VISIBLE);
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    private static final int NEW = 0;
    private static final int HOT = 1;

    /**
     * 处理服务端返回的数据
     */
    private List<WeMediaInfoList.InfoListEntity.BodyListEntity> handleData(WeMediaInfoList response, int type) {
        List<WeMediaInfoList.InfoListEntity.BodyListEntity> list = new ArrayList<>();

        if (response.getInfoList().size() > 0) {
            for (WeMediaInfoList.InfoListEntity.BodyListEntity entity : response.getInfoList().get(0).getBodyList()) {
                switch (type) {//视频列表并没有去重，因为没复写equals()方法,没有可比较的规则
                    case NEW:
                        if (newList.contains(entity)) {
                            continue;
                        }
                        break;
                    case HOT:
                        if (hotList.contains(entity)) {
                            continue;
                        }
                        break;
                }
                list.add(entity);
            }
        }
        return list;
    }


    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final WeMediaInfoList.InfoListEntity.WeMediaEntity entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(entity.getWeMediaID(), uid, type, System.currentTimeMillis() + "", SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        if (subscribe) {
                            weMediaHeadView.mFollowNo++;
                        } else {
                            weMediaHeadView.mFollowNo--;
                        }
                        weMediaEntity.setFollowed(type);//重新设置订阅关系
                        weMediaHeadView.setFollowNo();
                        showSubscribeStyle(subscribe);
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, subscribe, entity.getName());
                        ToastUtils.getInstance().showShortToast(subscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    public void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(WeMediaHomePageActivity.this, action, title, "");
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    private int mCurrentfirstVisibleItem = 0;
    private SparseArray recordSp = new SparseArray(0);

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        int scrollHeight = getScrollHeight(absListView, firstVisibleItem);
        if (scrollHeight == 0) {
            backView.getLayoutParams().height = DisplayUtils.convertDipToPixel(174);
            backView.requestLayout();
        }
        logger.debug("wemedia:{}", scrollHeight);

        if (scrollHeight > 0 && scrollHeight <= mHeight) {
            backView.setVisibility(View.GONE);
            icon_header.setVisibility(View.GONE);
            tv_media_title.setVisibility(View.GONE);
            ll_listview_top.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
            ll_red_view.setVisibility(View.GONE);
            redView.setVisibility(View.GONE);
        } else if (scrollHeight > mHeight) {
            backView.setVisibility(View.VISIBLE);
            backView.getLayoutParams().height = DisplayUtils.convertDipToPixel(44);
            backView.bringToFront();
            backView.requestLayout();
            icon_header.setVisibility(View.VISIBLE);
            tv_media_title.setVisibility(View.VISIBLE);
            rl_top_bar.bringToFront();
            ll_listview_top.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
            ll_red_view.setVisibility(View.VISIBLE);
            redView.setVisibility(View.VISIBLE);
        }

        if (totalItemCount > visibleItemCount) {
            listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        } else {
            listView.setMode(PullToRefreshBase.Mode.DISABLED);
        }
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        if (type.equals(WeMediaDao.WE_MEDIA_TYPE_NEW)) {
            isPullUpNew = true;
        } else {
            isPullUpHot = true;
        }
        getWeMediaInfo(weMediaId, type);
    }

    /**
     * 设置Banner背景,并实现毛玻璃效果
     *
     * @param url       背景图url
     * @param imageView
     */
    private void setBannerBg(String url, final ImageView imageView) {
        VolleyHelper.getImageLoader().get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    Bitmap bitmap = BitmapUtils.gsBlurFilter(response.getBitmap());
                    imageView.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                imageView.setImageResource(R.drawable.icon_login_default_header);
            }
        });
    }

    private void startLineMoveRight() {
        redView.clearAnimation();
        ValueAnimator mAnimator = null;

        mAnimator = ValueAnimator.ofFloat(0, DisplayUtils.convertDipToPixel(80.0f));
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (redView != null) {
                    redView.bringToFront();
                    redView.setTranslationX((Float) valueAnimator.getAnimatedValue());
                    redView.invalidate();
                }
            }
        });
        mAnimator.setDuration(100);
        mAnimator.setRepeatCount(0);
        mAnimator.start();
    }

    private void startLineMoveLeft() {
        redView.clearAnimation();
        ValueAnimator mAnimator = null;

        mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(80.0f), 0);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (redView != null) {
                    redView.bringToFront();
                    redView.setTranslationX((Float) valueAnimator.getAnimatedValue());
                    redView.invalidate();
                }
            }
        });
        mAnimator.setDuration(100);
        mAnimator.setRepeatCount(0);
        mAnimator.start();
    }

}
