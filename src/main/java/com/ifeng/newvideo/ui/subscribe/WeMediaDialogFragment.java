package com.ifeng.newvideo.ui.subscribe;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.bean.OnInnerItemClickListener;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.adapter.WeMediaRefreshAdapter;
import com.ifeng.newvideo.videoplayer.bean.ColumnTime;
import com.ifeng.newvideo.videoplayer.bean.RelativeDramaVideoInfo;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInfoItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.dao.VideoDao;
import com.ifeng.newvideo.widget.CustomRecyclerView;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.DataInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class WeMediaDialogFragment extends DialogFragment implements WeMediaRefreshAdapter.WeMediaProgramContent {

    private static final Logger logger = LoggerFactory.getLogger(WeMediaDialogFragment.class);
    public static final String CHANNEL_ID = "guid";
    public static final String WEMEDIA_ID = "wemediaId";
    public String mguid, weMediaId;

    private CustomRecyclerView yearRecyclerView;
    private CustomRecyclerView monthRecyclerView;
    private List<ColumnTime.Timer> timers = new ArrayList<>();
    private List<String> yearList;
    private List<String> monthList;
    private List<VideoItem> mList = new ArrayList<>();
    private YearRecyclerAdapter yearAdapter;
    private MonthRecyclerAdapter monthAdapter;

    //    private MyPullToRefreshListView pullToRefreshListView;
    private UIStatusLayout uiStatusLayout;
    private PullToRefreshListView listView;
    private WeMediaRefreshAdapter mAdapter;

    private int currentPage = 1;
    private int currentPlay = -1;
    private String currentYear, currentMonth;
    private List<Boolean> yearIsClick = new ArrayList<>();
    private List<Boolean> monthIsClick = new ArrayList<>();


    public static WeMediaDialogFragment newInstance(String guid, String weMediaId) {
        WeMediaDialogFragment fragment = new WeMediaDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CHANNEL_ID, guid);
        bundle.putString(WEMEDIA_ID, weMediaId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        yearList = new ArrayList<>();
        monthList = new ArrayList<>();

        if (getArguments() != null) {
            mguid = getArguments().getString(CHANNEL_ID);
            weMediaId = getArguments().getString(WEMEDIA_ID);
        }
        loadColumnTimeList(weMediaId);
        loadRelativeDramaVideo(mguid, "", "", ChannelConstants.DEFAULT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_drama_video_layout, container, false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(-1, -2);
        initView(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null) {
            return;
        }
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = getShowHeight();
        lp.windowAnimations = R.style.anim_tv_program_dialog;
        window.setAttributes(lp);
    }

    private void initView(View view) {
        yearRecyclerView = (CustomRecyclerView) view.findViewById(R.id.recycler_year);
        monthRecyclerView = (CustomRecyclerView) view.findViewById(R.id.recycler_month);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        yearRecyclerView.setLayoutManager(linearLayoutManager);
        yearAdapter = new YearRecyclerAdapter();
        yearRecyclerView.setAdapter(yearAdapter);
        yearAdapter.notifyDataSetChanged();

        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        linearManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        monthRecyclerView.setLayoutManager(linearManager);
        monthAdapter = new MonthRecyclerAdapter();
        monthRecyclerView.setAdapter(monthAdapter);
        monthAdapter.notifyDataSetChanged();

        initUiLayout(view);

        View icon_close = view.findViewById(R.id.icon_close);
        icon_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_EPG_CLOSE, PageIdConstants.PAGE_LIVE);
                dismiss();
            }
        });
    }

    private void initUiLayout(View root) {
        uiStatusLayout = (UIStatusLayout) root.findViewById(R.id.ui_status_layout_msg);
        uiStatusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uiStatusLayout.setStatus(UIStatusLayout.LOADING);
                String currentMonth = getRealMonth();
                loadRelativeDramaVideo("", currentYear, currentMonth, ChannelConstants.DEFAULT);
            }
        });
        listView = uiStatusLayout.getListView();

        listView.setMode(PullToRefreshBase.Mode.BOTH);
        listView.setShowIndicator(false);
        listView.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_NEVER);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                String currentMonth = getRealMonth();
                loadRelativeDramaVideo("", currentYear, currentMonth, ChannelConstants.DOWN);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                String currentMonth = getRealMonth();

                loadRelativeDramaVideo("", currentYear, currentMonth, ChannelConstants.UP);
            }
        });

        listView.getRefreshableView().setFooterDividersEnabled(true);
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        mAdapter = new WeMediaRefreshAdapter(getActivity(), mList, mguid);
        mAdapter.setProgramContent(this);
        listView.getRefreshableView().setAdapter(mAdapter);
    }

    /**
     * 获取展示高度
     */
    private int getShowHeight() {
        int screenHeight = DisplayUtils.getWindowHeight();//屏幕高度
        int statusBarHeight = DisplayUtils.getStatusBarHeight();//状态栏高度
        int videoHeight = DisplayUtils.getWindowWidth() * 9 / 16;//播放器和标题总高度,该值可能会根据需求作调整

//        int titleBarHeight = DisplayUtils.convertDipToPixel(40);//播放器和标题总高度,该值可能会根据需求作调整
        return screenHeight - statusBarHeight - videoHeight;
    }

    private String getRealMonth() {
        String realMonth;
        if (null != currentMonth && currentMonth.contains("月")) {
            int start = currentMonth.indexOf("月");
            realMonth = currentMonth.substring(0, start);
        } else {
            realMonth = "all";
        }
        return realMonth;
    }


    private class YearRecyclerAdapter extends RecyclerView.Adapter<YearRecyclerAdapter.YearRecyclerHolder> {


        @Override
        public YearRecyclerHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
            View containerView = LayoutInflater.from(getActivity()).inflate(R.layout.item_year_recycler, viewGroup, false);
            final YearRecyclerHolder yearRecyclerHolder = new YearRecyclerHolder(containerView);
            containerView.setOnClickListener(new OnInnerItemClickListener() {
                @Override
                public void onItemInnerClick(View view, int position) {
                    currentYear = yearList.get(position);
                    uiStatusLayout.setStatus(UIStatusLayout.LOADING);
                    updateMonthList(timers.get(position));
                    for (int i = 0; i < yearIsClick.size(); i++) {
                        yearIsClick.set(i, false);
                    }
                    yearIsClick.set(position, true);

                    notifyDataSetChanged();
                    currentPage = 1;
                    loadRelativeDramaVideo("", currentYear, "", ChannelConstants.DEFAULT);
                }
            });

            return yearRecyclerHolder;
        }

        @Override
        public void onBindViewHolder(YearRecyclerHolder yearRecyclerHolder, int position) {
            String year = yearList.get(position);
            if (year != null) {
                yearRecyclerHolder.tv_year.setText(year);
            }

            if (yearIsClick.get(position)) {
                yearRecyclerHolder.tv_year.setTextColor(Color.parseColor("#FF0000"));
            } else {
                yearRecyclerHolder.tv_year.setTextColor(Color.parseColor("#000000"));
            }

            yearRecyclerHolder.itemView.setTag(R.id.tag_key_click, position);
        }

        @Override
        public int getItemCount() {
            if (!ListUtils.isEmpty(yearList)) {
                return yearList.size();
            }
            return 0;
        }

        class YearRecyclerHolder extends RecyclerView.ViewHolder {
            TextView tv_year;

            public YearRecyclerHolder(View itemView) {
                super(itemView);
                tv_year = (TextView) itemView.findViewById(R.id.tv_year);
            }
        }
    }

    private class MonthRecyclerAdapter extends RecyclerView.Adapter<MonthRecyclerAdapter.MonthRecyclerHolder> {


        @Override
        public MonthRecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View containerView = LayoutInflater.from(getActivity()).inflate(R.layout.item_month_recycler, viewGroup, false);
            MonthRecyclerHolder monthRecyclerHolder = new MonthRecyclerHolder(containerView);

            containerView.setOnClickListener(new OnInnerItemClickListener() {
                @Override
                public void onItemInnerClick(View view, int position) {
                    currentMonth = monthList.get(position);
                    String realMonth = "";
                    if (currentMonth.contains("月")) {
                        int start = currentMonth.indexOf("月");
                        realMonth = currentMonth.substring(0, start);
                    } else {
                        realMonth = "all";
                    }
                    for (int i = 0; i < monthIsClick.size(); i++) {
                        monthIsClick.set(i, false);
                    }
                    monthIsClick.set(position, true);

                    notifyDataSetChanged();
                    currentPage = 1;
                    uiStatusLayout.setStatus(UIStatusLayout.LOADING);
                    loadRelativeDramaVideo("", currentYear, realMonth, ChannelConstants.DEFAULT);
                }
            });
            return monthRecyclerHolder;
        }

        @Override
        public void onBindViewHolder(MonthRecyclerHolder monthRecyclerHolder, int position) {
            String month = monthList.get(position);
            if (month != null) {
                monthRecyclerHolder.tv_month.setText(month);
            }

            if (monthIsClick.get(position)) {
                monthRecyclerHolder.tv_month.setTextColor(Color.parseColor("#FF0000"));
            } else {
                monthRecyclerHolder.tv_month.setTextColor(Color.parseColor("#000000"));
            }
            monthRecyclerHolder.itemView.setTag(R.id.tag_key_click, position);
        }

        @Override
        public int getItemCount() {
            if (!ListUtils.isEmpty(monthList)) {
                return monthList.size();
            }
            return 0;
        }

        class MonthRecyclerHolder extends RecyclerView.ViewHolder {
            TextView tv_month;

            public MonthRecyclerHolder(View itemView) {
                super(itemView);
                tv_month = (TextView) itemView.findViewById(R.id.tv_month);
            }
        }
    }

    private void loadRelativeDramaVideo(final String guid, String year, String month, final String opation) {
        VideoDao.getRelativeDramaVideoById(guid,
                weMediaId,
                year,
                month,
                DataInterface.PAGESIZE_20,
                opation,
                new Response.Listener<RelativeDramaVideoInfo>() {
                    @Override
                    public void onResponse(RelativeDramaVideoInfo response) {
                        listView.onRefreshComplete();
                        if (opation.equalsIgnoreCase(ChannelConstants.DEFAULT)) {
                            updateRelativeDramaVideoInfo(guid, response);
                        } else {
                            updateMoreVideoInfo(guid, response);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleErrorEvent(error);
                        uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                        logger.debug("loadRelativeDramaError:{}", error.getMessage());
                    }
                });
    }

    private void loadColumnTimeList(String weMediaId) {
        VideoDao.getColumnTimeList(weMediaId,
                new Response.Listener<ColumnTime>() {
                    @Override
                    public void onResponse(ColumnTime response) {
                        if (response != null && !ListUtils.isEmpty(response.getTimeList())) {
                            logger.debug("timeList", response.getTimeList().toString());
                            updateYearMonthList(response.getTimeList());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    }

    private void updateRelativeDramaVideoInfo(String guid, RelativeDramaVideoInfo response) {
        if (!ListUtils.isEmpty(response.columnVideoInfo)) {
            currentPage++;
            mList.clear();
            for (RelativeVideoInfoItem videoItem : response.columnVideoInfo) {
                VideoItem item = videoItem.videoInfo;
                if (item == null
                        || TextUtils.isEmpty(item.guid)
                        || TextUtils.isEmpty(item.name)
                        || ListUtils.isEmpty(item.videoFiles)) {
                    continue;
                }
                mList.add(item);
            }

            updateList();
            mAdapter.notifyDataSetChanged();
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
        } else {
            if (currentPage == 1) {
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    private void updateMoreVideoInfo(String guid, RelativeDramaVideoInfo response) {
        if (response != null && !ListUtils.isEmpty(response.columnVideoInfo)) {
            int listSize = mList.size();
            currentPage++;
            for (RelativeVideoInfoItem videoItem : response.columnVideoInfo) {
                VideoItem item = videoItem.videoInfo;
                if (item == null
                        || TextUtils.isEmpty(item.guid)
                        || TextUtils.isEmpty(item.name)
                        || ListUtils.isEmpty(item.videoFiles)
                        || mList.contains(item)) {
                    continue;
                }
                mList.add(item);
            }
            if (mList.size() > listSize) {
                updateList();
                mAdapter.notifyDataSetChanged();
                uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            } else {
                currentPage--;
                uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }

        } else {
            if (currentPage == 1) {
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    private void updateList() {
        for (int i = 0; i < mList.size(); i++) {
            if (mguid.equalsIgnoreCase(mList.get(i).guid)) {
                currentPlay = i;
                break;
            } else {
                currentPlay = -1;
            }
        }
        if (currentPlay != -1) {
            mAdapter.setSelectItem(currentPlay);
            listView.getRefreshableView().setSelection(currentPlay);
        } else {
            mAdapter.setSelectItem(currentPlay);

        }
    }

    private void updateCurrentYearMonth(String guid) {
        String guidYear = null;
        String guidMonth = null;
        if (guid != null && !"".equals(guid)) {
            for (int i = 0; i < mList.size(); i++) {
                if (guid.equalsIgnoreCase(mList.get(i).guid)) {
                    currentPlay = i;
                    guidYear = mList.get(i).columnYear;
                    guidMonth = mList.get(i).columnMonth;
                    break;
                } else {
                    guidYear = mList.get(0).columnYear;
                    guidMonth = mList.get(0).columnMonth;
                    currentPlay = -1;
                }
            }
//            initYearList(guidYear);
//            initMonthList(guidMonth);
            mAdapter.setSelectItem(currentPlay);
            listView.getRefreshableView().setSelection(currentPlay);
        } else {
            currentPlay = -1;
            mAdapter.setSelectItem(currentPlay);
        }
    }

    private void updateYearMonthList(List<ColumnTime.Timer> timer) {
        for (ColumnTime.Timer time : timer) {
            updateYearList(time);
            timers.add(time);
        }
        setYearIsClick();
        currentYear = yearList.get(0);
        updateMonthList(timer.get(0));
        yearAdapter.notifyDataSetChanged();
    }

    private void updateYearList(ColumnTime.Timer time) {
        if (time != null) {
            String year = time.getYear();
            if (!yearList.contains(year)) {
                yearList.add(year);
            }
        }
    }

    private void setYearIsClick() {
        yearIsClick.clear();
        for (int i = 0; i < yearList.size(); i++) {
            yearIsClick.add(false);
        }
        yearIsClick.set(0, true);
    }

    private void initYearList(String year) {
        int currentYearPosition = 0;
        if (year.equals(currentYear)) return;

        if (year != null && !"".equals(year)) {
            for (int i = 0; i < yearList.size(); i++) {
                if (year.equals(yearList.get(i))) {
                    yearIsClick.set(i, true);
                    currentYearPosition = i;
                    currentYear = yearList.get(i);
                    yearIsClick.set(0, false);
                    break;
                }
            }
        } else {
            yearIsClick.set(0, true);
        }
        if (!ListUtils.isEmpty(timers)) {
            updateMonthList(timers.size() > 0 ? timers.get(currentYearPosition) : timers.get(0));
        }
        yearAdapter.notifyDataSetChanged();
        yearRecyclerView.smoothScrollToPosition(currentYearPosition);
    }

    private void updateMonthList(ColumnTime.Timer time) {
        if (time != null) {
            monthList.clear();
            String[] month = time.getMonth().split(",");
            int length = month.length;
            monthList.add("全部");
            for (int i = 0; i < length; i++) {
                String months = month[i] + "月";
                if (!monthList.contains(months)) {
                    monthList.add(months);
                }
            }
        }
        setMonthIsClick();
        currentMonth = monthList.get(0);
        monthAdapter.notifyDataSetChanged();
    }

    private void setMonthIsClick() {
        monthIsClick.clear();
        for (int i = 0; i < monthList.size(); i++) {
            monthIsClick.add(false);
        }
        monthIsClick.set(0, true);
    }

    private void initMonthList(String month) {
        int currentMonthPosition = 0;
        if (month != null && !"".equals(month)) {
            for (int i = 0; i < monthList.size(); i++) {
                String monthSB = month + "月";
                if (monthSB.equals(monthList.get(i))) {
                    monthIsClick.set(i, true);
                    currentMonth = monthList.get(i);
                    currentMonthPosition = i;
                    monthIsClick.set(0, false);
                    break;
                }
            }
        } else {
            monthIsClick.set(0, true);
            currentMonth = monthList.get(0);
        }
        monthAdapter.notifyDataSetChanged();
        monthRecyclerView.smoothScrollToPosition(currentMonthPosition);

    }

    private void handleErrorEvent(VolleyError error) {
        if (currentPage == 1) {
            if (error != null && error instanceof NetworkError) {
                uiStatusLayout.setStatus(UIStatusLayout.NO_NET);//无网
            } else {
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);//错误
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    private OnClickProgramContent mClickProgramContent;

    public interface OnClickProgramContent {
        void transferGuid(String videoId);
    }

    public void setOnClickProgramContent(OnClickProgramContent listener) {
        this.mClickProgramContent = listener;
    }

    @Override
    public void onClickProgramContent(String guid) {
        if (mClickProgramContent != null) {
            mClickProgramContent.transferGuid(guid);
        }
    }

}
