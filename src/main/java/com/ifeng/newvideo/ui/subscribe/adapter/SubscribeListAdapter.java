package com.ifeng.newvideo.ui.subscribe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.subscribe.SubscribeList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的订阅列表适配器
 * Created by Administrator on 2016/7/21.
 */
public class SubscribeListAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SubscribeListAdapter.class);

    public static final int SIZE_THREE = 3;//展示三个
    public static final int SIZE_MAX = Integer.MAX_VALUE;//不限制展示数量
    private int size = SIZE_MAX;

    private List<SubscribeList.WeMediaListEntity> list = new ArrayList<>();

    private boolean isShowSubscribeBtn = true;//是否展示订阅按钮
    private boolean isShowSubscribeNum = false;//是否展示订阅数量

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public int getCount() {
        return list.size() >= size ? size : list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        ViewHolder holder;
        OnClick onClick;
        if (view == null) {
            holder = new ViewHolder();
            onClick = new OnClick();
            view = LayoutInflater.from(context).inflate(R.layout.adapter_media_grid_item_layout, null);
            holder.initView(view);
            view.setTag(holder);
            view.setTag(R.id.view_parent, onClick);
        } else {
            holder = (ViewHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }

        if (!isShowSubscribeBtn) {
            holder.tv_subscribe.setVisibility(View.GONE);
        }

        onClick.setPosition(i);
        holder.itemView.setOnClickListener(onClick);
        holder.tv_subscribe.setOnClickListener(onClick);
        holder.img_media_cover.setDefaultImageResId(R.drawable.icon_default_header);
        holder.img_media_cover.setImageUrl(list.get(i).getHeadPic(), VolleyHelper.getImageLoader());
        holder.img_media_cover.setDefaultImageResId(R.drawable.icon_login_default_header);
        holder.img_media_cover.setErrorImageResId(R.drawable.icon_login_default_header);
        holder.tv_media_name.setText(list.get(i).getName());
        holder.tv_subscribe_num.setVisibility(isShowSubscribeNum ? View.VISIBLE : View.GONE);
        holder.tv_subscribe_num.setText(StringUtils.changeNumberMoreThen10000(list.get(i).getFollowNo()) + "人订阅");
        if (list.get(i).getFollowed() == 1) {//已订阅
            holder.tv_subscribe.setText(R.string.subscribed);
            holder.tv_subscribe.setTextColor(context.getResources().getColor(R.color.home_divider_color_bottom));
            holder.tv_subscribe.setBackgroundResource(R.drawable.btn_subscribe_shape_gray_border);
        } else {//未订阅  添加
            holder.tv_subscribe.setText(R.string.subscribe);
            holder.tv_subscribe.setTextColor(context.getResources().getColor(R.color.add_subscribe_text_color));
            holder.tv_subscribe.setBackgroundResource(R.drawable.btn_subscribe_shape_border);
        }
        return view;
    }

    public void setList(List<SubscribeList.WeMediaListEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setShowSubscribeBtn(boolean showSubscribeBtn) {
        isShowSubscribeBtn = showSubscribeBtn;
    }

    public void setShowSubscribeNum(boolean showSubscribeNum) {
        isShowSubscribeNum = showSubscribeNum;
    }

    static class ViewHolder {
        View itemView;
        NetworkImageView img_media_cover;//自媒体封面
        TextView tv_media_name;//自媒体名称
        TextView tv_subscribe_num;//订阅数量
        TextView tv_subscribe;//订阅按钮

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            img_media_cover = (NetworkImageView) view.findViewById(R.id.img_media_cover);
            tv_media_name = (TextView) view.findViewById(R.id.tv_media_name);
            tv_subscribe_num = (TextView) view.findViewById(R.id.tv_subscribe_num);
            tv_subscribe = (TextView) view.findViewById(R.id.tv_subscribe);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (clickListener == null) {
                return;
            }
            switch (view.getId()) {
                case R.id.view_parent:
                    clickListener.onItemClickListener(list.get(position));
                    break;
                case R.id.tv_subscribe:
                    clickListener.onSubscribeClickListener(list.get(position));
                    break;
            }
        }
    }

    private ClickListener clickListener;

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClickListener(SubscribeList.WeMediaListEntity entity);

        void onSubscribeClickListener(SubscribeList.WeMediaListEntity entity);
    }
}
