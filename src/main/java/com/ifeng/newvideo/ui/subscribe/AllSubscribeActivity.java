package com.ifeng.newvideo.ui.subscribe;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.adapter.SubscribeContentListAdapter;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 全部订阅Activity
 * Created by Administrator on 2016/7/21.
 */
public class AllSubscribeActivity extends BaseFragmentActivity implements RequestData {
    private static final Logger logger = LoggerFactory.getLogger(AllSubscribeActivity.class);

    private UIStatusLayout uiStatusLayout;
    private PullToRefreshListView listView;

    private int currentPage = 1;//当前页码
    private SubscribeContentListAdapter adapter;

    private List<WeMediaInfoList.InfoListEntity.WeMediaEntity> weMediaList = new ArrayList<>();//存放服务端返回的自媒体，用于去重、过滤
    private List<WeMediaInfoList.InfoListEntity> infoList = new ArrayList<>();//视频

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_subscribe_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        intiView();
        getSubscribeContent();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void requestData() {
        currentPage = 1;
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);//Loading
        getSubscribeContent();
    }

    /**
     * 初始化View
     */
    private void intiView() {
        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.SUB_ALL);
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(getResources().getString(R.string.subscribe_all));
        uiStatusLayout = (UIStatusLayout) findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        listView = uiStatusLayout.getListView();

        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        listView.setShowIndicator(false);
        listView.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_NEVER);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.SUB_ALL);
                getSubscribeContent();
            }
        });
        adapter = new SubscribeContentListAdapter();
        adapter.setShowBottomLine(true);
        listView.getRefreshableView().setFooterDividersEnabled(true);
        listView.getRefreshableView().setAdapter(adapter);
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);//Loading
    }

    /**
     * 获取订阅内容
     */
    private void getSubscribeContent() {
        WeMediaDao.getWeMediaList(
                "",
                getUserId(),
                WeMediaDao.WE_MEDIA_TYPE_SUBSCRIBE,
                currentPage + "",
                DataInterface.PAGESIZE_20,
                System.currentTimeMillis() + "",
                WeMediaInfoList.class,
                new Response.Listener<WeMediaInfoList>() {
                    @Override
                    public void onResponse(WeMediaInfoList response) {
                        listView.onRefreshComplete();
                        handleResponseEvent(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listView.onRefreshComplete();
                        handleErrorEvent(error);
                    }
                }, false);
    }

    /**
     * 处理响应事件
     */
    private void handleResponseEvent(WeMediaInfoList response) {
        if (response != null && !ListUtils.isEmpty(response.getInfoList())) {
            currentPage++;
            List<WeMediaInfoList.InfoListEntity> list = handleData(response);
            if (list.size() > 0) {
                infoList.addAll(list);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);//正常
            adapter.setList(infoList);
        } else {
            if (currentPage == 1) {//如果是第一页无数据，
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 处理错误事件
     */
    private void handleErrorEvent(VolleyError error) {
        if (currentPage == 1) {
            if (error != null && error instanceof NetworkError) {
                uiStatusLayout.setStatus(UIStatusLayout.NO_NET);//无网
            } else {
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);//错误
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 数据处理
     */
    private List<WeMediaInfoList.InfoListEntity> handleData(WeMediaInfoList response) {
        List<WeMediaInfoList.InfoListEntity> tempList = new ArrayList<>();

        for (WeMediaInfoList.InfoListEntity infoListEntity : response.getInfoList()) {

            boolean b = TextUtils.isEmpty(infoListEntity.getWeMedia().getWeMediaID())
                    || TextUtils.isEmpty(infoListEntity.getWeMedia().getName())
                    || weMediaList.contains(infoListEntity.getWeMedia());//容错、去重处理
            if (b) {
                continue;
            }
            weMediaList.add(infoListEntity.getWeMedia());
            if (infoListEntity.getBodyList().size() == 0) {
                continue;
            }
            tempList.add(infoListEntity);
        }
        return tempList;
    }
}
