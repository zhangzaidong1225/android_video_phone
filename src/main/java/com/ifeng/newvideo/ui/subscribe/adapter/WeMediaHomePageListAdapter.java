package com.ifeng.newvideo.ui.subscribe.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 自媒体主页视频列表适配器
 * Created by Administrator on 2016/9/5.
 */
public class WeMediaHomePageListAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(WeMediaHomePageListAdapter.class);

    private List<WeMediaInfoList.InfoListEntity.BodyListEntity> list = new ArrayList<>();

    String echid = "";

    public void setEchid(String echid) {
        this.echid = echid;
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void setList(List<WeMediaInfoList.InfoListEntity.BodyListEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderVideo videoHolder = null;
        OnClick onClick = null;
        if (view == null) {
            videoHolder = new ViewHolderVideo();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_listiview_mix_text_wemedia, viewGroup, false);
            videoHolder.initView(view);
            view.setTag(R.id.view_parent, onClick);
            view.setTag(videoHolder);
        } else {
            videoHolder = (ViewHolderVideo) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        videoHolder.itemView.setOnClickListener(onClick);
        WeMediaInfoList.InfoListEntity.BodyListEntity bodyListEntity = list.get(i);

        videoHolder.tvVideoTime.setText(StringUtils.changeDuration(bodyListEntity.getMemberItem().getDuration()));
        videoHolder.tvVideoTitle.setText(bodyListEntity.getTitle());

        String playTime = bodyListEntity.getMemberItem().getPlayTime();
        boolean isShowPlayTime = !TextUtils.isEmpty(playTime);
        videoHolder.tvVideoWatchTime.setVisibility(isShowPlayTime ? View.VISIBLE : View.GONE);
        videoHolder.tvVideoWatchTime.setText(StringUtils.changePlayTimes(playTime));

        String programNo = bodyListEntity.getMemberItem().getProgramNo();
        if (!TextUtils.isEmpty(programNo) && programNo.length() >= 8) { // 20170817 共8位
            videoHolder.author.setVisibility(View.VISIBLE);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(programNo.substring(0, 4)).append("-");
            stringBuilder.append(programNo.substring(4, 6)).append("-");
            stringBuilder.append(programNo.substring(6, 8));
            videoHolder.author.setText(stringBuilder.toString());
        } else {
            videoHolder.author.setVisibility(View.GONE);
        }


        boolean isShowTag = !TextUtils.isEmpty(bodyListEntity.getTag());//是否显示Tag标签
        videoHolder.tvVideoType.setVisibility(isShowTag ? View.VISIBLE : View.GONE);
        videoHolder.tvVideoType.setText(TagUtils.getTagTextForList(bodyListEntity.getTag(), bodyListEntity.getMemberType(), ""));
        videoHolder.tvVideoType.setTextColor(TagUtils.getTagTextColor(bodyListEntity.getMemberType()));
        videoHolder.tvVideoType.setBackgroundResource(TagUtils.getTagBackground(bodyListEntity.getMemberType()));

        if (bodyListEntity.getImagelist().size() > 0) {
            String img = bodyListEntity.getImagelist().get(0).getImage();
            videoHolder.videoCover.setImageUrl(img, VolleyHelper.getImageLoader());
        }
        videoHolder.videoCover.setDefaultImageResId(R.drawable.bg_default_small);
        videoHolder.videoCover.setErrorImageResId(R.drawable.bg_default_small);

        return view;
    }

    static class ViewHolderVideo {
        View itemView;
        TextView tvVideoTitle;//视频标题
        TextView tvVideoType;//类型：逻辑思维
        TextView tvVideoWatchTime;//观看次数
        NetworkImageView videoCover;//视频封面
        NetworkImageView avatar;//自媒体头像
        ImageView avatarMask;//自媒体头像圆形遮罩
        TextView tvVideoTime;//视频时长
        TextView author;//自媒体名称
        View divider;//底部一条线

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvVideoTitle = (TextView) view.findViewById(R.id.tv_left_title);
            tvVideoType = (TextView) view.findViewById(R.id.tv_category_label);
            tvVideoWatchTime = (TextView) view.findViewById(R.id.tv_play_times);
            videoCover = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
            tvVideoTime = (TextView) view.findViewById(R.id.tv_duration);
            author = (TextView) view.findViewById(R.id.tv_author);
            avatar = (NetworkImageView) view.findViewById(R.id.niv_left_emoji);
            avatarMask = (ImageView) view.findViewById(R.id.niv_left_avatar_mask);
            divider = view.findViewById(R.id.divider);
//            author.setVisibility(View.GONE);
            avatar.setVisibility(View.GONE);
            avatarMask.setVisibility(View.GONE);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            WeMediaInfoList.InfoListEntity.BodyListEntity bodyListEntity = list.get(position);
            String guid = bodyListEntity.getMemberItem().getGuid();
            if (!TextUtils.isEmpty(guid)) {
                if (CheckIfengType.isVRVideo(bodyListEntity.getMemberType())) {
                    List<String> videoList = new ArrayList<>();
                    videoList.add(guid);
                    IntentUtils.startVRVideoActivity(view.getContext(), 0, "", "", 0, videoList, true);
                } else {
                    IntentUtils.toVodDetailActivity(view.getContext(), guid, echid, false, false, 0, "");
                }
            }
        }
    }
}

