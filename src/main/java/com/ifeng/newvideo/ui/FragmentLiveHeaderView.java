package com.ifeng.newvideo.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.live.adapter.TVLiveAdapter;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 直播ListView的HeaderView
 * Created by Administrator on 2016/8/14.
 */
public class FragmentLiveHeaderView extends RelativeLayout {
    private static final Logger logger = LoggerFactory.getLogger(FragmentSubscribe.class);

    public static final int LOADING = 0;
    public static final int ERROR = 1;
    public static final int NORMAL = 2;
    private Context context;
    private View loading;
    private View error;//电视直播加载失败界面
    private GridView gridView;//电视直播GridView

    private TVLiveAdapter tvLiveAdapter;//电视直播适配器

    public FragmentLiveHeaderView(Context context) {
        this(context, null);
    }

    public FragmentLiveHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FragmentLiveHeaderView(final Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.fragment_live_header_layout, this, true);
        loading = findViewById(R.id.loading_layout);
        error = findViewById(R.id.tv_live_load_fail);
        error.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (requestHeaderViewData != null) {
                    requestHeaderViewData.requestHeaderViewData();
                }
            }
        });
        findViewById(R.id.rl_all_tv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtils.startTVLiveListActivity(context);
            }
        });
        gridView = (GridView) findViewById(R.id.gridView);
        initAdapter();
    }

    private void initAdapter() {
        tvLiveAdapter = new TVLiveAdapter(context);
        gridView.setAdapter(tvLiveAdapter);
    }

    /**
     * 更新布局
     *
     * @param tvLiveInfoList 电视台数据
     */
    public void updateHeaderView(List<TVLiveInfo> tvLiveInfoList) {
        tvLiveAdapter.setData(tvLiveInfoList);
    }


    public void setStatus(int status) {
        switch (status) {
            case LOADING:
                loading.setVisibility(VISIBLE);
                error.setVisibility(GONE);
                gridView.setVisibility(GONE);
                break;
            case ERROR:
                loading.setVisibility(GONE);
                error.setVisibility(VISIBLE);
                gridView.setVisibility(GONE);
                break;
            case NORMAL:
                loading.setVisibility(GONE);
                error.setVisibility(GONE);
                gridView.setVisibility(VISIBLE);
                break;
        }
    }


    private RequestHeaderViewData requestHeaderViewData;

    public interface RequestHeaderViewData {
        void requestHeaderViewData();
    }

    public void setRequestHeaderViewData(RequestHeaderViewData requestHeaderViewData) {
        this.requestHeaderViewData = requestHeaderViewData;
    }
}
