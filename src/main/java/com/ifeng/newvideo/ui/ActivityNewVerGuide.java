package com.ifeng.newvideo.ui;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @describe 用户指导页
 * Created by antboyqi on 14-8-6.
 */
public class ActivityNewVerGuide extends BaseFragmentActivity {

    private static final Logger logger = LoggerFactory.getLogger(ActivityNewVerGuide.class);
    private Drawable[] drawables;
    private ViewPager viewPager;
    private final PagerAdapter pagerAdapter = new PagerAdapter() {
        @Override
        public int getCount() {
            return drawables.length;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(ActivityNewVerGuide.this);
            TypedArray images = getResources().obtainTypedArray(R.array.user_guide_img_list);
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
            opt.inPurgeable = true;
            opt.inInputShareable = true;
            Bitmap bitmap = BitmapFactory.decodeStream(getResources().openRawResource(images.getResourceId(position, 0)), null, opt);
            imageView.setImageBitmap(bitmap);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            images.recycle();
            RelativeLayout relativeLayout = new RelativeLayout(ActivityNewVerGuide.this);
            relativeLayout.setBackgroundColor(ActivityNewVerGuide.this.getResources().getColor(R.color.white));
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            lp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            relativeLayout.addView(imageView, lp);
            container.addView(relativeLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return relativeLayout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (object instanceof ImageView) {
                container.removeView((ImageView) object);
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    };

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return gestureDetector.onTouchEvent(ev) || super.dispatchTouchEvent(ev);
    }

    private final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (viewPager.getCurrentItem() == pagerAdapter.getCount() - 1) jumpToNextActivity();
            return super.onSingleTapUp(e);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            final int FLING_MIN_DISTANCE = 100;// X或者y轴上移动的距离(像素)
            final int FLING_MIN_VELOCITY = 200;// x或者y轴上的移动速度(像素/秒)
            if ((e1.getX() - e2.getX()) > FLING_MIN_DISTANCE && Math.abs(velocityX) > FLING_MIN_VELOCITY) {
                // 左滑动
                if (viewPager.getCurrentItem() == pagerAdapter.getCount() - 1) {
                    jumpToNextActivity();
                    return true;
                }
            }
            return false;
        }
    });

    private void jumpToNextActivity() {
        String action = getIntent().getAction();
        if (IntentUtils.ACTION_SPLASHGUIDE2MAINTAB.equals(action)) {
            Intent intent=new Intent(this, ActivityMainTab.class);
            intent.setAction(action);
            startActivity(intent);
            finish();
        } else if (IntentUtils.ACTION_SPLASHGUIDESELF.equals(action)) {
            finish();
        } else {
            logger.warn("please set action with {} or {} in ActivityNewVerGuide class", IntentUtils.ACTION_SPLASHGUIDE2MAINTAB, IntentUtils.ACTION_SPLASHGUIDESELF);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPager = new ViewPager(this);
        viewPager.setBackgroundColor(R.color.white);
        setContentView(viewPager);
        TypedArray images = getResources().obtainTypedArray(R.array.user_guide_img_list);
        drawables = new Drawable[images.length()];
        images.recycle();
        viewPager.setAdapter(pagerAdapter);
    }
}