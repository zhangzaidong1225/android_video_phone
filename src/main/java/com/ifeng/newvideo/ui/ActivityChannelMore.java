package com.ifeng.newvideo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.*;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.bean.OnInnerItemClickListener;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.Channel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guolc on 2016/8/2.
 */
public class ActivityChannelMore extends BaseFragmentActivity implements View.OnClickListener, PullToRefreshBase.OnLastItemVisibleListener {

    private MyPullToRefreshListView mPullToRefreshListView;
    private ListView mListView;
    private BaseAdapter mAdapter;
    private List<Channel.ChannelInfoBean> mDataList = new ArrayList<>();
    private ArrayList<Channel.ChannelInfoBean> mLv3 = new ArrayList<>();
    private TextView mTextView;
    private ImageView mImageView;
    private View loadView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_channel_more);
        enableExitWithSlip(true);
        initHadSubscribedData();
        initView();
        initAdapter();
        fetchChannelInfoData(Status.FIRST);
    }

    private void initHadSubscribedData() {
        ArrayList<Channel.ChannelInfoBean> list3 = getIntent().getParcelableArrayListExtra(ChannelDao.CHANNEL_KEY);
        if (!ListUtils.isEmpty(list3)) {
            mLv3.addAll(list3);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initView() {
        findViewById(R.id.iv_back_activity_channel_more).setOnClickListener(this);
        loadView = findViewById(R.id.rl_request_again_tv);
        loadView.setOnClickListener(this);
        mPullToRefreshListView = (MyPullToRefreshListView) findViewById(R.id.lv_channel_more);
        mListView = mPullToRefreshListView.getRefreshableView();
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.DISABLED);
        mPullToRefreshListView.hideFootView();
        mPullToRefreshListView.setOnLastItemVisibleListener(this);
        mTextView = (TextView) findViewById(R.id.tv_status_hint_ifengtv);
        mImageView = (ImageView) findViewById(R.id.iv_status_ifeng_tv);
        mImageView.setImageResource(R.drawable.icon_common_loading);
    }

    private void fetchChannelInfoData(final Status status) {

        String itemId = null;
        if (status == Status.LOAD_MORE) {
            itemId = ListUtils.isEmpty(mDataList) ? "" : mDataList.get(mDataList.size() - 1).getItemId();
        } else {
            itemId = "";
        }
        ChannelDao.requestChannel(ChannelDao.CHANNEL_LV_1 + "", itemId, Channel.class, new Response.Listener<Channel>() {
            @Override
            public void onResponse(Channel response) {
                if (response == null || ListUtils.isEmpty(response.getChannelInfo())) {
                    if (status == Status.LOAD_MORE) {
                        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                        mPullToRefreshListView.hideFootView();
                    } else {
                        mPullToRefreshListView.setVisibility(View.GONE);
                        mTextView.setText(getString(R.string.toast_no_more));
                        mImageView.setImageResource(R.drawable.icon_common_loading);
                    }
                    return;
                }
                List<Channel.ChannelInfoBean> list = response.getChannelInfo();
                list = filterInvalidChannel(list, mDataList);
                if (!ListUtils.isEmpty(list)) {
                    loadView.setVisibility(View.GONE);
                    mPullToRefreshListView.hideFootView();
                    mPullToRefreshListView.setVisibility(View.VISIBLE);
                    for (Channel.ChannelInfoBean bean : list) {
                        bean.setSrcChannelLevel(bean.getChannelLevel());
                    }
                    mDataList.addAll(list);
                    mAdapter.notifyDataSetChanged();
                } else {
                    if (status == Status.FIRST) {
                        mPullToRefreshListView.setVisibility(View.GONE);
                        loadView.setVisibility(View.VISIBLE);
                        mImageView.setImageResource(R.drawable.commen_load_data_err);
                        mTextView.setText(R.string.common_load_data_error);
                    } else {
                        mPullToRefreshListView.hideFootView();
                        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (status == Status.FIRST) {
                    mPullToRefreshListView.setVisibility(View.GONE);
                    loadView.setVisibility(View.VISIBLE);
                    if (error != null && error instanceof NetworkError) {
                        mImageView.setImageResource(R.drawable.commen_net_err);
                        mTextView.setText(R.string.common_net_useless_try_again);
                    } else {
                        mImageView.setImageResource(R.drawable.commen_load_data_err);
                        mTextView.setText(R.string.common_load_data_error);
                    }
                } else {
                    mPullToRefreshListView.hideFootView();
                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                }
            }
        }, false, null);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back_activity_channel_more) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.HOME_CHANNEL_MORE);
            result();
            finish();
        } else if (id == R.id.rl_request_again_tv) {
            mTextView.setText(R.string.common_onloading);
            mImageView.setImageResource(R.drawable.icon_common_loading);
            fetchChannelInfoData(Status.FIRST);
        }
    }

    private void result() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(ChannelDao.CHANNEL_KEY, mLv3);
        setResult(ChannelDao.REQUEST_CODE_MORE_CHANNEL, intent);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            result();
        }
        return super.onKeyUp(keyCode, event);
    }

    private List<Channel.ChannelInfoBean> filterInvalidChannel(List<Channel.ChannelInfoBean> list, List<Channel.ChannelInfoBean> exist) {

        if (!ListUtils.isEmpty(list)) {
            List<Channel.ChannelInfoBean> add = new ArrayList<>();
            for (Channel.ChannelInfoBean bean : list) {
                if (bean.getChannelLevel() <= 3 && bean.getChannelLevel() >= 1 &&
                        !TextUtils.isEmpty(bean.getChannelName()) && !TextUtils.isEmpty(bean.getChannelId())
                        && !TextUtils.isEmpty(bean.getShowType()) && !add.contains(bean)) {
                    if (!ListUtils.isEmpty(exist) && exist.contains(bean)) {
                        continue;
                    }
                    add.add(bean);
                }
            }
            return add;
        }
        return null;
    }

    private void initAdapter() {
        mAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                if (mDataList != null) {
                    return mDataList.size();
                }
                return 0;
            }

            @Override
            public Object getItem(int position) {

                return mDataList.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(ActivityChannelMore.this).inflate(R.layout.item_channel_more, parent, false);
                }
                Channel.ChannelInfoBean bean = mDataList.get(position);
                if (bean != null) {
                    Holder holder = getHolder(convertView);
                    holder.head.setImageUrl(bean.getPic(), VolleyHelper.getImageLoader());
                    holder.head.setDefaultImageResId(R.drawable.avatar_default);
                    holder.head.setErrorImageResId(R.drawable.avatar_default);
                    holder.mName.setText(bean.getChannelName());
                    holder.add_cancel.setTag(R.id.tag_key_click, position);
                    if (mLv3.contains(bean)) {
                        holder.add_cancel.setImageResource(R.drawable.channel_cancel);
                    } else {
                        holder.add_cancel.setImageResource(R.drawable.channel_add);
                    }
                }
                return convertView;
            }

            private Holder getHolder(View view) {
                Holder holder = null;
                if (view.getTag() == null) {
                    holder = new Holder();
                    holder.mName = (TextView) view.findViewById(R.id.tv_channel_name_activity_more);
                    holder.head = (NetworkImageView) view.findViewById(R.id.niv_activity_channel_more);
                    holder.add_cancel = (ImageView) view.findViewById(R.id.iv_add_cancel_activity_channel_more);
                    holder.add_cancel.setOnClickListener(new OnInnerItemClickListener() {
                        @Override
                        public void onItemInnerClick(View view, int positon) {
                            boolean action;
                            ImageView imageView = (ImageView) view;
                            Channel.ChannelInfoBean bean = mDataList.get(positon);
                            if (mLv3.contains(bean)) {
                                imageView.setImageResource(R.drawable.channel_add);
                                mLv3.remove(bean);
                                action = false;
                                ToastUtils.getInstance().showShortToast(R.string.remove_from_homepage);
                            } else {
                                imageView.setImageResource(R.drawable.channel_cancel);
                                bean.setSrcChannelLevel(ChannelDao.CHANNEL_LV_1);
                                bean.setChannelLevel(ChannelDao.CHANNEL_LV_3);
                                if (mLv3.size() == 0) {
                                    bean.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_3);
                                } else {
                                    Channel.ChannelInfoBean b = mLv3.get(mLv3.size() - 1);
                                    bean.setSortId((b.getSortId() + 1));
                                }
                                mLv3.add(bean);
                                action = true;
                                ToastUtils.getInstance().showShortToast(R.string.add_to_homepage);
                            }
                            sendChannelStatistics(bean.getChannelId(), action, bean.getChannelName());
                        }
                    });
                    view.setTag(holder);
                } else {
                    holder = (Holder) view.getTag();
                }
                return holder;
            }
        };
        mPullToRefreshListView.setAdapter(mAdapter);
    }

    @Override
    public void onLastItemVisible() {
        int visibleCount = mListView.getLastVisiblePosition() - mListView.getFirstVisiblePosition();
        if (mDataList.size() > visibleCount) {
            mPullToRefreshListView.showFootView();
            fetchChannelInfoData(Status.LOAD_MORE);
        }
    }


    private class Holder {
        public ImageView add_cancel;
        public NetworkImageView head;
        public TextView mName;
    }

    /**
     * 发送频道订阅、取消订阅统计
     */
    private void sendChannelStatistics(String id, boolean action, String title) {
        List<ColumnRecord> columnRecords = new ArrayList<>();
        ColumnRecord columnRecord = new ColumnRecord(id, ColumnRecord.TYPE_CH, action, title);
        columnRecords.add(columnRecord);
        CustomerStatistics.sendChannelSubScribe(columnRecords);
        SendSmartStatisticUtils.sendChannelOperatorStatistics(ActivityChannelMore.this, action, title);
    }
}
