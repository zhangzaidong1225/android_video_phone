package com.ifeng.newvideo.ui.maintab;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.bean.CustomScrollView;
import com.ifeng.newvideo.login.HandleDataService;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.ActivityChannelMore;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.FragmentHomePage;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.maintab.adapter.ChannelDragAdapter;
import com.ifeng.newvideo.ui.maintab.adapter.ChannelDragView;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.Channel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页频道管理fragment
 * Created by wangqing1 on 2014/10/11.
 */
public class ChannelManagerFragment extends BaseFragment implements View.OnClickListener {

    public static final String CHANNEL_MANAGER_TAG = "channel_manage_tag";
    private ChannelDragView mGridViewLv3;   //mine
    private ChannelDragView mGridViewLv2;   //recommend
    private ChannelDragAdapter mAdapterLv3;
    private ChannelDragAdapter mAdapterLv2;
    private ImageView mImageView;
    private TextView mDragTextView;
    private TextView mTextView;
    private boolean isEdit;
    private View mView2ShowMore;
    private ActivityMainTab mActivity;
    private ViewStub mViewStub;
    private ArrayList<Channel.ChannelInfoBean> mChannels_Lv3 = new ArrayList<>();//刚进入页面原始三级频道
    private ArrayList<Channel.ChannelInfoBean> mChannels_Lv3_new = new ArrayList<>();//编辑三级频道
    private ArrayList<Channel.ChannelInfoBean> mChannels_Lv2 = new ArrayList<>();//原始二级频道
    private ArrayList<Channel.ChannelInfoBean> mChannels_Lv2_new = new ArrayList<>();//编辑二级频道

    private List<ColumnRecord> columnRecords = new ArrayList<>();//统计集合

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActivityMainTab) activity;
        mActivity.setChannelManageFragmentShow(true);
    }

    @Override
    protected void requestNet() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        ArrayList<Channel.ChannelInfoBean> list_3 = bundle.getParcelableArrayList(FragmentHomePage.KEY_LV3);
        ArrayList<Channel.ChannelInfoBean> list_2 = bundle.getParcelableArrayList(FragmentHomePage.KEY_LV2_VIRTUAL);
        if (!ListUtils.isEmpty(list_2)) {
            mChannels_Lv2.addAll(list_2);
            mChannels_Lv2_new.addAll(mChannels_Lv2);
        }
        if (!ListUtils.isEmpty(list_3)) {
            mChannels_Lv3.addAll(list_3);
            mChannels_Lv3_new.addAll(mChannels_Lv3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_channel_mgr, container, false);
        initView(root);
        initAdapter();
        initListener();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (ListUtils.isEmpty(mChannels_Lv2_new)) {
            showStubViewContent(View.VISIBLE);
        }
    }

    private long enterTime;

    @Override
    public void onResume() {
        super.onResume();
        getFocus();
        enterTime = DateUtils.getCurrentTime();
    }

    @Override
    public void onPause() {
        super.onPause();
        pageInvisible();
    }

    private void pageInvisible() {
        if (enterTime > 0) {
            PageActionTracker.endPageChMag(enterTime);
            enterTime = 0;
        }
    }

    private void getFocus() {
        if (getView() != null) {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                        close();
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    private void showStubViewContent(int visible) {
        if (visible != View.VISIBLE && mView2ShowMore == null) {
            return;
        }
        if (mView2ShowMore == null) {
            mView2ShowMore = mViewStub.inflate();
            mView2ShowMore.findViewById(R.id.tv_more_fragment_channel_manager).setOnClickListener(this);
        }
        mView2ShowMore.setVisibility(visible);
    }

    private void initView(View root) {
        mGridViewLv3 = (ChannelDragView) root.findViewById(R.id.cdv_my_channel);
        mGridViewLv2 = (ChannelDragView) root.findViewById(R.id.cdv_recommend_channel);
        mGridViewLv3.setTag(ChannelDragView.TOPGRID);
        mGridViewLv2.setTag(ChannelDragView.BOTTOMGRID);
        mGridViewLv3.setSelector(new ColorDrawable(Color.TRANSPARENT));
        mGridViewLv2.setSelector(new ColorDrawable(Color.TRANSPARENT));
        mDragTextView = (TextView) root.findViewById(R.id.tv_my_channel_drag_text);
        mImageView = (ImageView) root.findViewById(R.id.iv_close_channel_manager);
        mTextView = (TextView) root.findViewById(R.id.tv_edit_channel);
        mViewStub = (ViewStub) root.findViewById(R.id.vs_fragment_channel_more);
        ((CustomScrollView) mGridViewLv2.getParent().getParent()).setmChannelDragView_bottop(mGridViewLv2);
        ((CustomScrollView) mGridViewLv3.getParent().getParent()).setmChannelDragView_top(mGridViewLv3);
    }

    private void initAdapter() {
        mAdapterLv3 = new ChannelDragAdapter(getActivity().getApplicationContext(), ChannelDragView.TOPGRID, false) {
            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
            }
        };
        mAdapterLv2 = new ChannelDragAdapter(getActivity().getApplicationContext(), ChannelDragView.BOTTOMGRID, true) {
            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();

            }
        };
        mAdapterLv2.setData(mChannels_Lv2_new);
        mAdapterLv3.setData(mChannels_Lv3_new);
        mGridViewLv3.setAdapter(mAdapterLv3);
        mGridViewLv2.setAdapter(mAdapterLv2);
    }

    private void initListener() {
        mImageView.setOnClickListener(this);
        mTextView.setOnClickListener(this);
        mGridViewLv3.setOnDragListener(new ChannelDragView.OnDragListener() {

            @Override
            public void onDragging(int from, int to) {

                switchSortId(mChannels_Lv3_new, from, to);
                Channel.ChannelInfoBean bean1 = mChannels_Lv3_new.remove(from);
                mChannels_Lv3_new.add(to, bean1);
                mAdapterLv3.setHidePosition(to);
                mAdapterLv3.notifyDataSetChanged();
            }

            @Override
            public void onDragToBeyondRect(int position) {
                if (position < mChannels_Lv3_new.size()) {
                    if (mChannels_Lv2_new.size() == 0) {
                        mGridViewLv2.setVisibility(View.VISIBLE);
                        showStubViewContent(View.GONE);
                    }
                    sort(mChannels_Lv3_new, mChannels_Lv2_new, ChannelDao.CHANNEL_LV_2, position, true);
                    mAdapterLv3.setHidePosition(-1);
                    mAdapterLv3.notifyDataSetChanged();
                    mAdapterLv2.notifyDataSetChanged();
                }
            }

            @Override
            public void onDragInRect() {
                mAdapterLv3.setHidePosition(-1);
                mAdapterLv3.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(int position) {
                PageActionTracker.clickBtn(ActionIdConstants.DRAG_CH_MY_ITEM, PageIdConstants.HOME_CHANNEL_EDIT);
                isEdit = true;
                mAdapterLv3.setEdit(true);
                mDragTextView.setText(R.string.channel_mgr_drag_sort);
                mTextView.setText(getString(R.string.finish));
                mAdapterLv3.hidePosition = position;
                mAdapterLv3.notifyDataSetChanged();
            }
        });
        mGridViewLv2.setOnDragListener(new ChannelDragView.OnDragListener() {

            @Override
            public void onDragging(int from, int to) {
                if (from < mChannels_Lv2_new.size() && to < mChannels_Lv2_new.size()) {//多了"更多"项
                    switchSortId(mChannels_Lv2_new, from, to);
                    Channel.ChannelInfoBean bean1 = mChannels_Lv2_new.remove(from);
                    mChannels_Lv2_new.add(to, bean1);
                    mAdapterLv2.setHidePosition(to);
                    mAdapterLv2.notifyDataSetChanged();
                } else if (from == mChannels_Lv2_new.size()) {
                    mAdapterLv2.setHidePosition(from);
                    mAdapterLv2.notifyDataSetChanged();
                }
            }

            @Override
            public void onDragToBeyondRect(int position) {
                if (position < mChannels_Lv2_new.size()) {
                    sort(mChannels_Lv2_new, mChannels_Lv3_new, ChannelDao.CHANNEL_LV_3, position, false);
                    mAdapterLv2.setHidePosition(-1);
                    mAdapterLv2.notifyDataSetChanged();
                    mAdapterLv3.notifyDataSetChanged();
                    if (mChannels_Lv2_new.size() == 0) {
                        mGridViewLv2.setVisibility(View.GONE);
                        showStubViewContent(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onDragInRect() {
                mAdapterLv2.setHidePosition(-1);
                mAdapterLv2.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(int position) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_REC_ITEM, PageIdConstants.HOME_CHANNEL_EDIT);
            }
        });
        mGridViewLv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == mChannels_Lv2_new.size()) {
                    toActivityChannelMore();
                    return;
                }
                sort(mChannels_Lv2_new, mChannels_Lv3_new, ChannelDao.CHANNEL_LV_3, position, false);
                mAdapterLv2.notifyDataSetChanged();
                mAdapterLv3.notifyDataSetChanged();
                if (mChannels_Lv2_new.size() == 0) {
                    mGridViewLv2.setVisibility(View.GONE);
                    showStubViewContent(View.VISIBLE);
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_REC_ITEM, PageIdConstants.HOME_CHANNEL_EDIT);
            }
        });
        mGridViewLv3.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (isEdit) {
                    if (position == 0) {
                        return;
                    }
                    if (mChannels_Lv2_new.size() == 0) {
                        mGridViewLv2.setVisibility(View.VISIBLE);
                        showStubViewContent(View.GONE);
                    }
                    sort(mChannels_Lv3_new, mChannels_Lv2_new, ChannelDao.CHANNEL_LV_2, position, true);
                    mAdapterLv2.notifyDataSetChanged();
                    mAdapterLv3.notifyDataSetChanged();
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_MY_ITEM, true, PageIdConstants.HOME_CHANNEL_EDIT);
                } else {
                    close();
                    Channel.ChannelInfoBean s = (Channel.ChannelInfoBean) parent.getItemAtPosition(position);
                    mActivity.switchCurrentChannelFragment(s.getChannelId());
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_MY_ITEM, false, PageIdConstants.HOME_CHANNEL_EDIT);
                }

            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGridViewLv3.removeDragImage();
        mGridViewLv2.removeDragImage();
        mActivity.setChannelManageFragmentShow(false);
        updateDB();
    }


    private void updateDB() {
        final boolean b = !mChannels_Lv3_new.equals(mChannels_Lv3);
        final boolean c = !mChannels_Lv2_new.equals(mChannels_Lv2);
        if (b || c) {//如有变化，更新数据库
            SharePreUtils.getInstance().setHasEdited(true);
            Bundle bundle = new Bundle();
            if (b) {
                bundle.putParcelableArrayList(HandleDataService.KEY_CHANNEL_3, mChannels_Lv3_new);
            }
            if (c) {
                bundle.putParcelableArrayList(HandleDataService.KEY_CHANNEL_2, mChannels_Lv2_new);
            }
            updateChannelDataInHomePage();
            IntentUtils.startTaskForChannels(mActivity.getApplicationContext(), bundle);
        }
    }

    public void close() {
        try {
            updateChannelDataInHomePage();
            FragmentTransaction tr1 = getFragmentManager().beginTransaction();
            tr1.setCustomAnimations(R.anim.common_slide_left_in, R.anim.common_slide_right_out);
            tr1.remove(this);
            tr1.commit();
            //发送频道订阅、退订统计
            mActivity.refreshWellChosenDataIfNecessary();
            initRecordData();
            CustomerStatistics.sendChannelSubScribe(columnRecords);
            sendSmartStatistics();
        } catch (Exception e) {
            logger.error("close error ! {}", e);
        }
    }

    /**
     * 发送频道编辑智能统计
     */
    private void sendSmartStatistics() {
        for (ColumnRecord record : columnRecords) {
            SendSmartStatisticUtils.sendChannelOperatorStatistics(getActivity(), record.isSub(), record.getTitle());
        }
    }

    private void sort(ArrayList<Channel.ChannelInfoBean> delete,
                      ArrayList<Channel.ChannelInfoBean> add, int newLevel, int position, boolean addFirst) {
        Channel.ChannelInfoBean s = delete.remove(position);
        if (addFirst) {
            if (add.size() == 0) {
                s.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_2);
                add.add(s);
            } else {
                Channel.ChannelInfoBean s2 = add.get(0);
                s.setSortId(s2.getSortId() - 1);
                add.add(0, s);
            }
        } else {
            if (add.size() == 0) {
                s.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_3);
                add.add(s);
            } else {
                Channel.ChannelInfoBean s1 = add.get(add.size() - 1);
                s.setSortId(s1.getSortId() + 1);
                add.add(s);
            }
        }
        s.setChannelLevel(newLevel);

    }

    private void switchSortId(ArrayList<Channel.ChannelInfoBean> list, int from, int to) {
        if (to > from) {
            for (int j = from; j <= to; j++) {
                Channel.ChannelInfoBean bean = list.get(j);
                if (j == from) {
                    bean.setSortId(list.get(to).getSortId());
                } else {
                    bean.setSortId(bean.getSortId() - 1);
                }
            }
        } else if (from > to) {
            for (int i = from; i >= to; i--) {
                Channel.ChannelInfoBean bean = list.get(i);
                if (i == from) {
                    bean.setSortId(list.get(to).getSortId());
                } else {
                    bean.setSortId(bean.getSortId() + 1);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close_channel_manager:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_CLOSE, PageIdConstants.HOME_CHANNEL_EDIT);
                close();
                break;
            case R.id.tv_edit_channel:
                if (getString(R.string.edit).equals(mTextView.getText())) {
                    mTextView.setText(getString(R.string.finish));
                    mDragTextView.setText(R.string.channel_mgr_drag_sort);
                    isEdit = true;
                } else {
                    mTextView.setText(getString(R.string.edit));
                    mDragTextView.setText(R.string.channel_mgr_click_to_channel);
                    isEdit = false;
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_EDIT, isEdit, PageIdConstants.HOME_CHANNEL_EDIT);
                mAdapterLv3.setEdit(isEdit);
                mAdapterLv3.notifyDataSetChanged();
                break;
            case R.id.tv_more_fragment_channel_manager:
                toActivityChannelMore();
                break;

            default:
                break;
        }

    }

    private void deleteItemLevel_1() {
        if (!ListUtils.isEmpty(mChannels_Lv2_new)) {
            List<Channel.ChannelInfoBean> delete = new ArrayList<>();
            for (Channel.ChannelInfoBean bean : mChannels_Lv2_new) {
                if (bean.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_1) {
                    delete.add(bean);
                }
            }
            if (!ListUtils.isEmpty(delete)) {
                mChannels_Lv2_new.removeAll(delete);
                mAdapterLv2.notifyDataSetChanged();
                updateChannelDataInHomePage();
                try {
                    ChannelDao.deleteChannelList(mActivity.getApplicationContext(), delete);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateChannelDataInHomePage() {
        mActivity.getFragmentHomePage().updateChannelData(mChannels_Lv3_new, mChannels_Lv2_new);
    }


    private void toActivityChannelMore() {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_CH_MORE, PageIdConstants.HOME_CHANNEL_EDIT);
        Intent intent = new Intent(mActivity, ActivityChannelMore.class);
        if (!ListUtils.isEmpty(mChannels_Lv3_new)) {
            intent.putParcelableArrayListExtra(ChannelDao.CHANNEL_KEY, mChannels_Lv3_new);
        }
        startActivityForResult(intent, 100);
        deleteItemLevel_1();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ChannelDao.REQUEST_CODE_MORE_CHANNEL == resultCode) {
            ArrayList<Channel.ChannelInfoBean> list_3 = data.getParcelableArrayListExtra(ChannelDao.CHANNEL_KEY);
            if (!ListUtils.isEmpty(list_3)) {
                ArrayList<Channel.ChannelInfoBean> old_lv3 = new ArrayList<>();
                old_lv3.addAll(mChannels_Lv3_new);

                mChannels_Lv3_new.clear();
                mChannels_Lv3_new.addAll(list_3);
                updateChannelDataInHomePage();
                mAdapterLv3.notifyDataSetChanged();
                syncDB(list_3, old_lv3);
            }
        }
    }

    private void syncDB(final ArrayList<Channel.ChannelInfoBean> list_3, final ArrayList<Channel.ChannelInfoBean> old_lv3) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<Channel.ChannelInfoBean> delete = new ArrayList<>();
                ArrayList<Channel.ChannelInfoBean> add = new ArrayList<>();
                for (Channel.ChannelInfoBean bean : list_3) {
                    boolean contain = false;
                    String channel_id = bean.getChannelId();
                    for (Channel.ChannelInfoBean bean_in : old_lv3) {
                        if (!TextUtils.isEmpty(channel_id) && channel_id.equals(bean_in.getChannelId())) {
                            updateChannelInfoBeanContent(bean_in, bean);
                            contain = true;
                            break;
                        }
                    }
                    if (!contain) {
                        add.add(bean);
                    }
                }
                for (Channel.ChannelInfoBean bean : old_lv3) {
                    if (bean.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_1 && !list_3.contains(bean)) {
                        delete.add(bean);
                    }
                }
                if (!ListUtils.isEmpty(delete)) {
                    try {
                        ChannelDao.deleteChannelList(getActivity().getApplicationContext(), delete);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (!ListUtils.isEmpty(add)) {
                    try {
                        ChannelDao.createOrUpdate(mActivity.getApplicationContext(), add);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    private void updateChannelInfoBeanContent(Channel.ChannelInfoBean src, Channel.ChannelInfoBean latest) {
        src.setPid(latest.getPid());
        src.setPic(latest.getPic());
        src.setShowType(latest.getShowType());
        src.setChannelName(latest.getChannelName());
        src.setStatisticChannelId(latest.getStatisticChannelId());
        src.setTag(latest.getTag());
        src.setItemId(latest.getItemId());
    }

    /**
     * 获取List差值
     *
     * @param preList 最初List
     * @param endList 结束List
     * @return 差值
     */
    private List<Channel.ChannelInfoBean> getDifferent(List<Channel.ChannelInfoBean> preList, List<Channel.ChannelInfoBean> endList) {
        List<Channel.ChannelInfoBean> diff = new ArrayList<>();
        Map<Channel.ChannelInfoBean, Integer> map = new HashMap<>(endList.size());
        for (Channel.ChannelInfoBean bean : endList) {
            map.put(bean, 1);
        }
        for (Channel.ChannelInfoBean bean : preList) {
            if (map.get(bean) != null) {
                map.put(bean, 2);//
                continue;
            }
            diff.add(bean);
        }
        for (Map.Entry<Channel.ChannelInfoBean, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                diff.add(entry.getKey());
            }
        }
        return diff;
    }

    /**
     * 初始化统计数据
     */
    private void initRecordData() {
        //List<Channel.ChannelInfoBean> mChannels_Lv2_diff = getDifferent(mChannels_Lv2, mChannels_Lv2_new);
        List<Channel.ChannelInfoBean> mChannels_Lv3_diff = getDifferent(mChannels_Lv3, mChannels_Lv3_new);
        if (mChannels_Lv3.size() < mChannels_Lv3_new.size()) {//三级频道中：如果初始的小于最后的，证明有2级频道升级到3级频道
            for (Channel.ChannelInfoBean bean : mChannels_Lv3_diff) {
                columnRecords.add(new ColumnRecord(bean.getChannelId(), ColumnRecord.TYPE_CH, true, bean.getChannelName()));
            }
        } else {//三级频道中：如果初始的大于最后的，证明有3级频道降低到2级频道
            for (Channel.ChannelInfoBean bean : mChannels_Lv3_diff) {
                columnRecords.add(new ColumnRecord(bean.getChannelId(), ColumnRecord.TYPE_CH, false, bean.getChannelName()));
            }
        }
    }
}
