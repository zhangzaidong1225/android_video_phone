package com.ifeng.newvideo.ui.maintab;

import java.io.Serializable;

public class LiveData implements Serializable {

    private String channelId;//电视台Id
    private String echid;
    private String chid;

    //精选传入分享数据，需记录最初的videoItem
    private String shareTitle;
    private String shareImg;

    private boolean playVideo;//是否要播放视频

    private boolean isFromPush;//是否来自推送

    public LiveData() {
        this("", "", "", "", "", false, false);
    }

    public LiveData(String channelId, String echid, String chid, String shareTitle, String shareImg, boolean playVideo, boolean isFromPush) {
        this.channelId = channelId;
        this.echid = echid;
        this.chid = chid;
        this.shareTitle = shareTitle;
        this.shareImg = shareImg;
        this.playVideo = playVideo;
        this.isFromPush = isFromPush;
    }

    public void resetLiveData() {
        this.channelId = "";
        this.echid = "";
        this.chid = "";
        this.shareTitle = "";
        this.shareImg = "";
        playVideo = false;
        isFromPush = false;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getEchid() {
        return echid;
    }

    public String getChid() {
        return chid;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public String getShareImg() {
        return shareImg;
    }

    public boolean isPlayVideo() {
        return playVideo;
    }

    public boolean isFromPush() {
        return isFromPush;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public void setFromPush(boolean fromPush) {
        isFromPush = fromPush;
    }

    @Override
    public String toString() {
        return "LiveData{" +
                "channelId='" + channelId + '\'' +
                ", echid='" + echid + '\'' +
                ", chid='" + chid + '\'' +
                ", shareTitle='" + shareTitle + '\'' +
                ", shareImg='" + shareImg + '\'' +
                ", playVideo='" + playVideo + '\'' +
                ", isFromPush='" + isFromPush + '\'' +
                '}';
    }

}
