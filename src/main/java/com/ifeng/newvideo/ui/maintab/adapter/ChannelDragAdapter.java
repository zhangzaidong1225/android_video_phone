package com.ifeng.newvideo.ui.maintab.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.fontsOverride.CalligraphyUtils;
import com.ifeng.newvideo.utils.Util4act;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.ChannelId;
import com.ifeng.video.dao.db.constants.CheckChannelId;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.Channel;

import java.util.ArrayList;
import java.util.List;

public class ChannelDragAdapter extends BaseAdapter {
    private List<Channel.ChannelInfoBean> arr = new ArrayList<>();//本adpater的数据
    private final Context context;
    public int hidePosition = -1;//记录需要隐藏的item位置
    private final LayoutInflater inflater;

    public void setHidePosition(int hidePosition) {
        this.hidePosition = hidePosition;
    }

    private boolean edit;
    private final int tag;
    private boolean showMore;


    public ChannelDragAdapter(Context context, int tag, boolean showMore) {
        this.showMore = showMore;
        this.context = context;
        this.tag = tag;
        inflater = LayoutInflater.from(context);
    }

    public void setEdit(boolean b) {
        edit = b;
    }

    public void setData(List<Channel.ChannelInfoBean> arr) {
        this.arr = arr;
    }

    @Override
    public int getCount() {
        if (arr != null && arr.size() != 0) {
            if (showMore) {
                return arr.size() + 1;
            }
            return arr.size();
        }
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        if (showMore) {
            return 2;
        }
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arr.size()) {
            return 1;
        }
        return super.getItemViewType(position);
    }

    @Override
    public Object getItem(int position) {
        return arr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == 1) {
            View view = inflater.inflate(R.layout.channel_edit_lay, parent, false);
            TextView textView = (TextView) view.findViewById(R.id.channel_name_tv);
            textView.setBackgroundResource(R.drawable.red_bg_textview);
            textView.setTextColor(Color.parseColor("#f54343"));
            textView.setText(textView.getResources().getString(R.string.more));
            CalligraphyUtils.applyFontToTextView(context, textView, context.getString(R.string.font_for_xiaomi));
            return view;
        }
        View container = inflater.inflate(R.layout.channel_edit_lay, parent, false);
        TextView textView = (TextView) container.findViewById(R.id.channel_name_tv);
        TextView textViewTag = (TextView) container.findViewById(R.id.channel_name_tv_tag);
        CalligraphyUtils.applyFontToTextView(context, textView, context.getString(R.string.font_for_xiaomi));
        CalligraphyUtils.applyFontToTextView(context, textViewTag, context.getString(R.string.font_for_xiaomi));
        View cancelBookImageView = container.findViewById(R.id.channel_cancel_book_img);

        Channel.ChannelInfoBean info = arr.get(position);
        if (position == hidePosition) {
            container.setVisibility(View.INVISIBLE);
        }
        // 精选不可编辑，不同背景
        boolean isFixedItem = "1".equalsIgnoreCase(info.getItemId()) && tag == ChannelDragView.TOPGRID;
        if (isFixedItem) {
            textView.setBackgroundResource(R.drawable.text_channel_bg_special);
        } else {
            textView.setBackgroundResource(R.drawable.text_channel_normal_bg);
        }
        if (edit) {
            if (isFixedItem) { // itemId为1不显示 x
                cancelBookImageView.setVisibility(View.GONE);
            } else {
                cancelBookImageView.setVisibility(View.VISIBLE);
            }
        } else {
            cancelBookImageView.setVisibility(View.GONE);
        }
        String channelName = getChannelName(info);
        textView.setText(channelName);

        if (StringUtils.calculateLength(channelName) > 4) {
            textView.setTextSize(13);
        }

        if (info.getChannelLevel() == ChannelDao.CHANNEL_LV_2 && !TextUtils.isEmpty(info.getTag())) {
            textViewTag.setText(info.getTag());
            textViewTag.setVisibility(View.VISIBLE);
        } else {
            textViewTag.setVisibility(View.GONE);
        }
        return container;
    }

    private String getChannelName(Channel.ChannelInfoBean info) {
        if (CheckChannelId.isMainPager(info.getChannelId())
                || Util4act.Constants.SHOUYE_ORIGIN.equals(info.getChannelName())) {
            return Util4act.Constants.SHOUYE_RECOMMEND;
        } else if (info.getChannelId().equals(ChannelId.SUBCHANNELID_DOCUMENTARY) || info.getChannelName().contains("纪录片")) {
            return "纪录片";
        } else if (ChannelId.SUBCHANNELID_TOPIC_LIANBO.equals(info.getChannelId())
                || Util4act.Constants.LiBOTAI_RECOMMEND.equals(info.getChannelName())) {
            return Util4act.Constants.LiBOTAI_RECOMMEND;
        } else {
            return info.getChannelName();
        }
    }

}
