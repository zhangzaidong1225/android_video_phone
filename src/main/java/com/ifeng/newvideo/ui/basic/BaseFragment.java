package com.ifeng.newvideo.ui.basic;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.PageRefreshConstants;
import com.ifeng.newvideo.statistics.domains.PageInfoRecord;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.umeng.analytics.MobclickAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFragment extends Fragment {
    public static final Logger logger = LoggerFactory.getLogger(BaseFragment.class);
    private TextView mTextView;
    private ImageView mImageView;
    private View mStatusView;
    private View[] contentViews = new View[1];
    private long startTime;
    private long endTime;
    protected Status currentStatus;
    public List<PageInfoRecord> mFocusList = new ArrayList<>(20);

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initV(View.GONE);
        initDa(view);
        initListen();
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected View initV(int visible) {
        mStatusView = View.inflate(getActivity(), R.layout.viewstub_ifeng_tv, null);
        mImageView = (ImageView) mStatusView.findViewById(R.id.iv_status_ifeng_tv);
        mTextView = (TextView) mStatusView.findViewById(R.id.tv_status_hint_ifengtv);
        mStatusView.setVisibility(visible);
        return mStatusView;
    }

    private void initDa(View view) {
        if (view == null) {
            return;
        }
        if (view instanceof FrameLayout) {
            FrameLayout frameLayout = (FrameLayout) view;
            frameLayout.setBackgroundResource(R.color.common_white);
            contentViews = (View[]) Array.newInstance(View.class, frameLayout.getChildCount());
            for (int index = 0; index < frameLayout.getChildCount(); index++) {
                contentViews[index] = frameLayout.getChildAt(index);
            }
            ((FrameLayout) view).addView(mStatusView);
        } else {
            ViewGroup viewGroup = (ViewGroup) view.getParent();
            for (int index = 0; index < viewGroup.getChildCount(); index++) {
                if (view == viewGroup.getChildAt(index)) {
                    viewGroup.removeViewAt(index);
                    FrameLayout frameLayout = new FrameLayout(getActivity());
                    frameLayout.setBackgroundResource(R.color.common_white);
                    frameLayout.addView(view);
                    contentViews[0] = view;
                    frameLayout.addView(mStatusView);
                    viewGroup.addView(frameLayout, index);
                    break;
                }
            }
        }

    }

    protected void initListen() {
        mStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentStatus == Status.DATA_ERROR || currentStatus == Status.REQUEST_NET_FAIL) {
                    mTextView.setText(R.string.common_onloading);
                    requestNet();
                }
            }
        });
    }

    //用于改变网络背景状态
    protected void updateViewStatus(Status status) {
        if (currentStatus != status) {
            currentStatus = status;
            logger.debug("in updateViewStatus {}", status.toString());
            switch (status) {
                case DATA_ERROR://数据url为空、或者json解析出错
                    mStatusView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(R.drawable.commen_load_data_err);
                    mTextView.setText(R.string.common_load_data_error);
                    for (View view : contentViews) {
                        view.setVisibility(View.GONE);
                    }
                    break;
                case REQUEST_NET_FAIL://网络问题加载失败
                    mStatusView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(R.drawable.commen_net_err);
                    mTextView.setText(R.string.common_net_useless_try_again);
                    for (View view : contentViews) {
                        view.setVisibility(View.GONE);
                    }
                    break;
                case LOADING://loading
                    mStatusView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(R.drawable.icon_common_loading);
                    for (View view : contentViews) {
                        view.setVisibility(View.GONE);
                    }
                    break;
                case REQUEST_NET_SUCCESS://show data
                    mStatusView.setVisibility(View.GONE);
                    for (View view : contentViews) {
                        view.setVisibility(View.VISIBLE);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void recordPageTime(int pageNum) {
        if (getActivity() == null) return;

        startTime = System.currentTimeMillis();
        logger.debug("---> in recordPageTime  pageNum is {} ,startTime is {} ", pageNum, startTime);
        SharePreUtils.getInstance().setPageData(PageRefreshConstants.CATEGORY_HOME, pageNum, startTime);
    }

    public void readPageTime(int pageNum) {
        endTime = System.currentTimeMillis();
        startTime = SharePreUtils.getInstance().getPageData(PageRefreshConstants.CATEGORY_HOME, pageNum);
        logger.debug("---> in readPageTime pageNum is {} ,startTime is {}, endTime is {}, endTime-startTime is {}", pageNum, startTime, endTime, endTime - startTime);
        if ((endTime - startTime) > PageRefreshConstants.REFRESH_DATA_INTERVAL) {
            requestNet();
        }
    }

    //用于实现重新请求网络接口
    protected abstract void requestNet();


}