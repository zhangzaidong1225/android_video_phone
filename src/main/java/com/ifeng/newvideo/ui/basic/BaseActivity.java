package com.ifeng.newvideo.ui.basic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.ifeng.newvideo.fontsOverride.CalligraphyContextWrapper;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.q.Qt;
import com.umeng.analytics.MobclickAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Activity基类，用于友盟统计，使代码精简
 * Created by Pengcheng on 2015/1/26 026.
 */
public class BaseActivity extends FragmentActivity {
    private static final Logger logger = LoggerFactory.getLogger(BaseActivity.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(getClass().getName());
        MobclickAgent.onResume(this);
        initQuestMobile();
    }

    /**
     * 用于初始化QuestMobile
     */
    private void initQuestMobile() {
        try {
            Qt.setDeviceUniqID(PhoneConfig.userKey);
            Qt.start(getApplicationContext(), PhoneConfig.publishid);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(getClass().getName());
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    /**
     * 重写onActivityResult方法，使二个或多个fragment嵌套使用时能收到onActivityResult回调
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        FragmentManager fm = getSupportFragmentManager();
        int index = requestCode >> 16;
        if (index != 0) {
            index--;
            if (fm.getFragments() == null || index < 0 || index >= fm.getFragments().size()) {
                logger.debug("Activity result fragment index out of range: 0x" + Integer.toHexString(requestCode));
                return;
            }
            Fragment frag = fm.getFragments().get(index);
            if (frag == null) {
                logger.debug("Activity result no fragment exists for index: 0x" + Integer.toHexString(requestCode));
            } else {
                handleResult(frag, requestCode, resultCode, data);
            }
        }
    }

    /**
     * 递归调用，对所有子Fragment生效
     */
    private void handleResult(Fragment frag, int requestCode, int resultCode, Intent data) {
        frag.onActivityResult(requestCode & 0xffff, resultCode, data);
        List<Fragment> frags = frag.getChildFragmentManager().getFragments();
        if (frags == null) {
            return;
        }
        for (Fragment f : frags) {
            if (f != null)
                handleResult(f, requestCode, resultCode, data);
        }
    }
}