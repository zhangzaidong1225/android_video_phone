package com.ifeng.newvideo.ui.basic;

/**
 * Created by guolc on 2016/8/13.
 */
public enum Status {
//用于更新fragment加载状态
    REQUEST_NET_SUCCESS,
    REQUEST_NET_FAIL,
    DATA_ERROR,
    LOADING,
    FIRST,//用于网络请求的第一次请求
    REFRESH,//用于网络请求的刷新请求
    LOAD_MORE//用于网络请求的加载更多请求

}
