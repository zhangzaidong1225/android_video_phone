package com.ifeng.newvideo.ui.basic;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.google.gson.Gson;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.ui.live.weblive.ActivityH5Live;
import com.ifeng.newvideo.ui.live.weblive.ActivityLiveColumn;
import com.ifeng.newvideo.ui.live.weblive.JsBridge;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.widget.BaseWebViewClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class BaseLiveActivity extends BaseFragmentActivity implements JsBridge.JSDispatchListener {

    private static final Logger logger = LoggerFactory.getLogger(BaseLiveActivity.class);

    protected static final String H5_LIVE_URl = "http://izhibo.ifeng.com/live.html?liveid=%s";

    //用户登陆回调requestCode
    public static final int USER_LOGIN_REQUESTCODE = 101;
    public static final int OPEN_INPUT_LAYOUT = 0x01016;
    public static final int OPEN_SHARE_POP = 0x01017;
    public static final int HIDE_TITLE_BAR = 0x01018;

    protected WebView mWebview;
    protected JsBridge mJsBridge;
    protected ProgressBar mProgressBar;
    protected String pageUrl;
    protected View noNetView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long startTime = System.currentTimeMillis() / 1000;
        SharePreUtils.getInstance().setLivePageStartTime(startTime);
    }

    protected void initWebView(WebView mWebview) {
        WebSettings settings = mWebview.getSettings();
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setSupportZoom(true);
        settings.setTextZoom(100);
        settings.setBuiltInZoomControls(true);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setBlockNetworkImage(false);

        // 支持localStorage
        settings.setAllowFileAccess(true);
        settings.setDomStorageEnabled(true);
        settings.setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        settings.setAppCacheEnabled(true);

//        mJsBridge = new JsBridge(this, mWebview);
//        mJsBridge.setDispatchListener(this);
//        mWebview.addJavascriptInterface(mJsBridge, "grounds");

        setToken(pageUrl);
        mWebview.loadUrl(pageUrl);

        mWebview.setWebViewClient(new LiveWebViewClient());
        mWebview.setWebChromeClient(new LiveWebChromeClient());
    }

    public void initJsBridge(WebView mWebview) {
//        mJsBridge = new JsBridge(this, mWebview);
        mJsBridge.setDispatchListener(this);
        mWebview.addJavascriptInterface(mJsBridge, "grounds");
    }

    protected void setToken(String pageUrl) {
        String mToken = "";
        if (User.isLogin()) {
            mToken = User.getIfengToken();
        }
        if (!TextUtils.isEmpty(mToken)) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setCookie(pageUrl, "sid=" + mToken + "; Domain=.ifeng.com" + "; path=/");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.flush();
            } else {
                CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this);
                cookieSyncManager.startSync();
                cookieSyncManager.sync();
            }
        }
    }

    private class LiveWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int progress) {

            if (progress < 100 && mProgressBar.getVisibility() == ProgressBar.GONE) {
                mProgressBar.setVisibility(ProgressBar.VISIBLE);
            }
            mProgressBar.setProgress(progress);

            if (progress >= 90) {
                mProgressBar.setVisibility(ProgressBar.GONE);
            }
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            if (callback != null) {
                callback.invoke(origin, true, false);
            }
            super.onGeolocationPermissionsShowPrompt(origin, callback);
        }
    }

    private class LiveWebViewClient extends BaseWebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            noNetView.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            view.stopLoading();
            view.clearView();
            noNetView.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        mJsBridge.onWebViewResume();
//        long startTime = System.currentTimeMillis() / 1000;
//        SharePreUtils.getInstance().setLivePageStartTime(startTime);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mJsBridge.onWebViewPause();
//        sendPageInfo();
    }

    private void sendPageInfo() {
        String pageJson = SharePreUtils.getInstance().getLivePageJson();
        if (!TextUtils.isEmpty(pageJson)) {
            PageLiveRecord pageLiveRecord = new Gson().fromJson(pageJson, PageLiveRecord.class);

            long currentTime = System.currentTimeMillis() / 1000;
            long startTime = SharePreUtils.getInstance().getLivePageStartTime();
            long durTime = currentTime - startTime;

            CommonStatictisListUtils.getInstance().sendPageLive(new PageLiveRecord(pageLiveRecord.getId(), pageLiveRecord.ref, pageLiveRecord.type, durTime + ""));
            SharePreUtils.getInstance().setLivePageStartTime(System.currentTimeMillis() / 1000);
            SharePreUtils.getInstance().setLivePageJson("");
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mJsBridge.onWebViewDestroy();
        if (mWebview != null) {
            mWebview.removeAllViews();
            mWebview.destroy();
            mWebview = null;
        }

        SharePreUtils.getInstance().setLiveCurrentStream(0);
    }

    @Override
    public void dispatch(String type, String url, String category, String errurl, String documentid) {
        Log.d("dispatch", "type:" + type + "url:" + url + "category:" + category + "errurl:" + errurl + "documentid:" + documentid);
        runOnUiThread(new ClickRunnable(this, type, url, errurl, documentid, category));
    }

    @Override
    public void dispatch(Map<String, String> paramMaps) {
        Log.d("dispatch", "paramMaps");
        runOnUiThread(new ClickRunnable(paramMaps));
    }

    public class ClickRunnable implements Runnable {

        Context context;
        String type;
        String url;
        String errUrl;
        String documentId;
        String category;

        String pageRef;
        String pageTag;

        public ClickRunnable(Context context, String type, String url, String errUrl, String documentId, String category) {
            this.context = context;
            this.type = type;
            this.url = url;
            this.errUrl = errUrl == null ? "http:\\www.ifeng.com" : errUrl;
            this.documentId = documentId;
            this.category = category;
        }

        public ClickRunnable(Map<String, String> maps) {

            this.url = maps.get(JsBridge.PARAM_URL);

            this.type = maps.get(JsBridge.PARAM_TYPE);

            this.pageRef = maps.get(JsBridge.PARAM_REF);

            this.pageTag = maps.get(JsBridge.PARAM_TAG);
        }

        @Override
        public void run() {
            boolean isWebType = "web".equals(type);
            if (isWebType) {
                Log.d("dispatch", "web1");
                Intent intent = new Intent();
                intent.setClass(context, ActivityLiveColumn.class);
                mJsBridge.savePageData(true);
                intent.putExtra("url", url);
                startActivity(intent);
            } else {
                if (TextUtils.isEmpty(url)) {
                    return;
                }
                String realUrl = String.format(H5_LIVE_URl, url);
                Log.d("dispatch", "text_live:" + realUrl);
                mJsBridge.savePageData(true);
                Intent intent = new Intent();
                intent.setClass(context, ActivityH5Live.class);
                intent.putExtra("url", realUrl);
                startActivity(intent);
            }
        }

    }
}
