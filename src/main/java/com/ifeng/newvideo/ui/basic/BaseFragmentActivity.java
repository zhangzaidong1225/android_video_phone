package com.ifeng.newvideo.ui.basic;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.appstore.StoreDownLoadService;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.dialogUI.DialogUtil;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.receiver.ScreenOnReceiver;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.StatisticsInEndService;
import com.ifeng.newvideo.statistics.domains.PageInfoRecord;
import com.ifeng.newvideo.ui.ActivitySplash;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.ScreenShotListenManager;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.wxapi.WXPayBroadcastReceiver;
import com.ifeng.newvideo.wxapi.WXPayEntryActivity;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuelong on 2014/8/26.
 */
public class BaseFragmentActivity extends BaseActivity implements ScreenOnReceiver.OnScreenOffListener {

    private static final Logger logger = LoggerFactory.getLogger(BaseFragmentActivity.class);
    public AudioManager mAudioManager;
    private int animFlag = -1;
    private boolean isFirstOnStart = true;
    protected static final int ANIM_FLAG_BOTTOM_TOP = 0;
    protected static final int ANIM_FLAG_LEFT_RIGHT = 1;
    private boolean withToast = false;
    private long startTime;
    private static StatisticsInEndService iService = null;

    private static final int DOUBLE_INTERVAL = 2500;
    public static final int SNAP_VELOCITY = 1200;  //返回最小的滑动速率
    private boolean isRestart;
    private ScreenOnReceiver mScreenReceiver;//锁屏、开屏、解锁广播接收器
    private HomeKeyReceiver homeReceiver;//Home键广播接收器
    private boolean isSlide = false;
    public SharePreUtils mSharePreUtils;
    private GestureDetector mGestureDetector;
    public boolean hasHomeClick = false;
    public boolean isLandScape = false;
    private boolean isIntercept = false;
    private Thread mThread = null;
    private boolean isFromPush;
    private ScreenShotListenManager screenShotManager;
    private static boolean isActivitySwitchPause = false;
    public List<PageInfoRecord> mFocusList = new ArrayList<>(20);


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mScreenReceiver = new ScreenOnReceiver();
        mScreenReceiver.setOnScreenOffListener(this);
        mSharePreUtils = SharePreUtils.getInstance();
        registerScreenReceiver();
        // 因为新闻统计in，需要在启动页面的配置接口获取开关，所以单独处理了
        if (!(this instanceof ActivitySplash)) {
            bindStatisticInEndService();
        }
        IfengApplication.getInstance().setActivityState(this, true);
        IfengApplication.getInstance().addTopActivity(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            isFromPush = getIntent().getExtras().getBoolean(IntentKey.IS_FROM_PUSH, false);
        }
        screenShotManager = ScreenShotListenManager.newInstance(getApplication());
        screenShotManager.setListener(
                new ScreenShotListenManager.OnScreenShotListener() {
                    public void onShot(String imagePath) {
                        PageActionTracker.clickSnapshot(isLandScape(), BaseFragmentActivity.this);
                    }
                }
        );
        initWXBroadcastReceiver(this);


    }

    @Override
    public void onScreenOffCallBack() {
        onScreenOff();
    }

    /**
     * 锁屏：子类需要时复写该方法
     */
    protected void onScreenOff() {
    }

    private void registerScreenReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        this.registerReceiver(mScreenReceiver, filter);
    }

    private void unregisterScreenReceiver() {
        this.unregisterReceiver(mScreenReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        logger.debug("onStart");
        if (isFirstOnStart) {
            if (animFlag == ANIM_FLAG_BOTTOM_TOP) {
                overridePendingTransition(R.anim.common_push_bottom_in, R.anim.common_no_animation);
            } else if (animFlag == ANIM_FLAG_LEFT_RIGHT) {
                overridePendingTransition(R.anim.common_slide_left_in, R.anim.common_slide_left_out);
            }
            isFirstOnStart = false;
        }
        registerHomeReceiver();
    }

    private void registerHomeReceiver() {
        if (homeReceiver == null) {
            homeReceiver = new HomeKeyReceiver(this);
        }
        registerReceiver(homeReceiver, new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        logger.debug("onRestart");
        isRestart = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(homeReceiver);
        isRestart = false;
        CustomerStatistics.isAppVisible = false;
        logger.debug("onStop");
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        mThread = new MThread(this);
        mThread.start();
    }

    static class MThread extends Thread {
        WeakReference<BaseFragmentActivity> weakReference;

        MThread(BaseFragmentActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void run() {
            BaseFragmentActivity activity = weakReference.get();
            if (activity == null) {
                return;
            }
            try {
                Thread.sleep(500);
                IfengApplication.getInstance().setActivityState(activity, false);
                IfengApplication.getInstance().removeActivityState(activity);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 设置启动离开动画
     *
     * @param flag ANIM_FLAG_BOTTOM_TOP 从下到上开始 从上到下离开
     *             ANIM_FLAG_LEFT_RIGHT 从左到右开始 从右到左离开
     */
    protected void setAnimFlag(int flag) {
        animFlag = flag;
    }

    @Override
    public void finish() {
        super.finish();
        if (animFlag == ANIM_FLAG_BOTTOM_TOP) {
            overridePendingTransition(0, R.anim.common_below_out);
        } else if (animFlag == ANIM_FLAG_LEFT_RIGHT) {
            overridePendingTransition(R.anim.common_slide_right_in, R.anim.common_slide_right_out);
        }
    }

    boolean isFinished = false;
    boolean isDramaScroll = false;

    public void setIsDramaScroll(boolean dramaScroll) {
        this.isDramaScroll = dramaScroll;
    }

    public void rightSlipFinish() {
        if (!isFinished && !isDramaScroll) {
            PageActionTracker.rightSlipFinish(this);
            finish();
            isFinished = true;
            isDramaScroll = true;

        }
    }

    public void toPortrait() {
        if (isLandScape()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isLandScape()) {
                toPortrait();
            } else {
                if (withToast) {
                    exitWithToastHint();
                } else {
                    return super.onKeyUp(keyCode, event);
                }
            }
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * 双击back键退出应用
     */
    protected void exitWithToastHint() {
        if (startTime <= 0) {
            if (!isNoticeAppNewsInstall()) {
                startTime = System.currentTimeMillis();
                ToastUtils.getInstance().showShortToast(R.string.common_toast_text_quit_app);
            }
        } else {
            long endTime = System.currentTimeMillis();
            if (endTime - startTime < DOUBLE_INTERVAL) {
                exit();
            } else {
                startTime = endTime;
                ToastUtils.getInstance().showShortToast(R.string.common_toast_text_quit_app);
            }
        }
    }

    protected void exit() {
        //退出前设为移动网络不可播
        IfengApplication.mobileNetCanPlay = false;
        //离开应用暂停缓存
        CacheManager.stopCaching(this);
        IfengApplication.getInstance().setShowContinueCacheVideoDialog(true);
        if (null != dialog) {
            dialog.dismiss();
        }
        SharePreUtils.getInstance().setVideoHeadInfo("");
        finish();
    }

    private Dialog dialog;

    private boolean isNoticeAppNewsInstall() {
        if (PhoneConfig.isGooglePlay()) {
            return false;
        }

        //获取开关和delay时间值
        int appDaysLater = SharePreUtils.getInstance().getInstallNewsAppDelay();
        if (this instanceof ActivitySplash || appDaysLater < 0) {
            return false;
        }
        //没有安装新闻客户端
        if (!PackageUtils.checkAppExist(this, PackageUtils.NEWS_PACKAGE_NAME)) {

            long curTime = System.currentTimeMillis();
            long lastCancelTime = SharePreUtils.getInstance().getCancelInstallTime();

            //1、首次安装、或升级后首次打开的用户，当天不提示，接口值 X 天后提示
            if (SharePreUtils.getInstance().isFirstOpenApp()) {
                saveCancelTime();
                return false;
            }
            //2、取消累计>=2次的用户，不再提示
            if (SharePreUtils.getInstance().getCancelNewsAppTimes() >= 2) {
                return false;
            }
            //3、用户没取消过或用户取消days天后
            if (lastCancelTime <= 0L || curTime - lastCancelTime > appDaysLater * 24 * 60 * 60 * 1000) {
                dialog = DialogUtil.showDownloadAppNewsDialog(this,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String downLoadUrl = getString(R.string.back_dialog_downLoadUrl);
                                download(downLoadUrl);
                                PageActionTracker.clickBtn(ActionIdConstants.CLICK_APP_NEWS_INSTALL,
                                        ActionIdConstants.ACT_YES, PageIdConstants.PAGE_HOME);
                                exit();
                            }
                        }

                );
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        saveCancelTime();
                        //用户取消次数
                        int cancelTimes = SharePreUtils.getInstance().getCancelNewsAppTimes();
                        SharePreUtils.getInstance().setCancelNewsAppTimes(cancelTimes + 1);
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_APP_NEWS_INSTALL,
                                ActionIdConstants.ACT_NO, PageIdConstants.PAGE_HOME);
                        exit();
                    }
                });
                dialog.show();

                return true;
            }

        }

        return false;
    }

    private void saveCancelTime() {
        SharePreUtils.getInstance().setCancelInstallTime(System.currentTimeMillis());
    }

    private void download(String url) {
        try {
            logger.debug("BaseFragmentActivity------> onDownloadStart url is {}", url);
            Intent intent = new Intent(this, StoreDownLoadService.class);
            intent.putExtra(IntentKey.STORE_APP_URL, url);
            String apkName = "凤凰新闻客户端.apk";
            intent.putExtra(IntentKey.STORE_APP_NAME, apkName);
            this.startService(intent);
        } catch (Exception e) {
            logger.error("BaseFragmentActivity------> onDownloadStart Exception ! ", e);
        }
    }

    private static class HomeKeyReceiver extends BroadcastReceiver {
        WeakReference<BaseFragmentActivity> weakReference;

        HomeKeyReceiver(BaseFragmentActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            BaseFragmentActivity activity = weakReference.get();
            if (activity == null) {
                return;
            }
            activity.hasHomeClick = true;
            activity.isLandScape = activity.isLandScape();
            isActivitySwitchPause = true;
            logger.debug("hasHomeClick");
        }
    }

    /**
     * 返回当前activity是否为横屏
     */
    public boolean isLandScape() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    protected String getActivityName() {
        return this.getClass().getName();
    }

    protected IfengApplication getApp() {
        return IfengApplication.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mThread != null) {
            mThread.interrupt();
        }
        IfengApplication.getInstance().setActivityState(this, true);
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        hasHomeClick = false;
        CustomerStatistics.isAppVisible = true;
        logger.debug("onResume");
        PageActionTracker.enterPage();
        if (isFromPush) {
            PageActionTracker.lastPage = PageIdConstants.REF_PUSH;
        }
        screenShotManager.startListen();
/*        if (isActivitySwitchPause) {
            String number = SharePreUtils.getInstance().getMobileFlowRemain();
            String totalNum = SharePreUtils.getInstance().getMobileFlowTotal();
            MobileFlowUtils.getInstance(this).mobileFlowAlert(number, totalNum);
            isActivitySwitchPause = false;
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        logger.debug("onPause");
        screenShotManager.stopListen();
        PageActionTracker.endPage(isLandScape(), this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PageActionTracker.configChangePage(isLandScape(), this);
    }

    /**
     * 界面需要双击退出，设为true,默认FALSE
     */
    protected void setExitWithToast(boolean withToast) {
        this.withToast = withToast;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logger.debug("onDestroy");
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        IfengApplication.getInstance().removeTopActivity(this);
        // 因为新闻统计in，需要在启动页面的配置接口获取开关，所以单独处理了
        if (!(this instanceof ActivitySplash)) {
            unbindStatisticsInEndService();
        }
        unregisterScreenReceiver();
        if (mThread != null) {
            mThread.interrupt();
        }
        IfengApplication.getInstance().removeActivityState(this);
        if (null != mWXBroadcastReceiver) {
            unregisterReceiver(mWXBroadcastReceiver);
        }
    }

    public void unbindStatisticsInEndService() {
        unbindService(sInEndConnection);
    }

    public void bindStatisticInEndService() {
        Intent intent = new Intent(this, StatisticsInEndService.class);
        bindService(intent, sInEndConnection, Context.BIND_AUTO_CREATE);
    }

    private static final ServiceConnection sInEndConnection = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName name) {
            logger.debug("Activity ->Disconnected the LocalService");
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            //获取连接的服务对象
            iService = (StatisticsInEndService) service;
            logger.debug("Activity ->Connected the Service");
        }
    };

    /**
     * @param hasSlide 页面只要有各个方向的滑动设为true，固定不动为false
     */
    protected void enableExitWithSlip(boolean hasSlide) {
        this.isSlide = hasSlide;
        mGestureDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {

            float tempX;

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                if (Math.abs(distanceX) <= Math.abs(distanceY)) {
                    tempX = e2.getX();
                } else if (e2.getX() - tempX > DisplayUtils.getWindowWidth() * 2 / 5) {//小于45右划，累计横向滑动距离屏幕2/5也返回
                    if (isDramaScroll) {
                        return false;
                    }
                    rightSlipFinish();
                    isIntercept = true;
                    return true;
                }
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (!isLandScape()) {
                    if (e1 != null && e2 != null) {
                        if (e1.getX() - e2.getX() < 0 && velocityX > SNAP_VELOCITY && Math.abs(velocityY) < SNAP_VELOCITY && velocityX > Math.abs(velocityY)) {
                            if (isDramaScroll) {
                                return false;
                            }
                            rightSlipFinish();
                            isIntercept = true;
                            return true;
                        }
                    }
                }
                return false;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                tempX = e.getX();
                return e.getPressure() < 1.5;
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mGestureDetector != null && mGestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    private GestureDetector getGestureDetector() {
        if (mGestureDetector == null) {
            enableExitWithSlip(isSlide);
        }
        return mGestureDetector;
    }

    protected boolean isExit(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getGestureDetector() != null && (isSlide || isExit(ev))) {
            getGestureDetector().onTouchEvent(ev);
        }
        if (isIntercept) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 获取当前用户id
     */
    protected String getUserId() {
        String uid = new User(this).getUid();
        return TextUtils.isEmpty(uid) ? "" : uid;
    }

    public WXPayBroadcastReceiver mWXBroadcastReceiver;

    public void initWXBroadcastReceiver(Activity activity) {
        mWXBroadcastReceiver = new WXPayBroadcastReceiver();
        IntentFilter filter = new IntentFilter(WXPayEntryActivity.ACTION_VIDEO_WX_PAY);
        activity.registerReceiver(mWXBroadcastReceiver, filter);
    }

    private SparseArray recordSp = new SparseArray(0);
    private int mCurrentfirstVisibleItem = 0;

    /**
     * 获得滑动高度
     */
    public int getScrollHeight(AbsListView view, int firstVisibleItem) {
        mCurrentfirstVisibleItem = firstVisibleItem;
        View firstView = view.getChildAt(0);
        if (null != firstView) {
            ItemRecod itemRecord = (ItemRecod) recordSp.get(firstVisibleItem);
            if (null == itemRecord) {
                itemRecord = new ItemRecod();
            }
            itemRecord.height = firstView.getHeight();
            itemRecord.top = firstView.getTop();
            recordSp.append(firstVisibleItem, itemRecord);
        }
        int height = getScrollY();
        return height;
    }


    private int getScrollY() {
        int height = 0;
        for (int i = 0; i < mCurrentfirstVisibleItem; i++) {
            ItemRecod itemRecod = (ItemRecod) recordSp.get(i);
            if (itemRecod != null) {
                height += itemRecod.height;
            }
        }
        ItemRecod itemRecod = (ItemRecod) recordSp.get(mCurrentfirstVisibleItem);
        if (null == itemRecod) {
            itemRecod = new ItemRecod();
        }
        return height - itemRecod.top;
    }


    class ItemRecod {
        int height = 0;
        int top = 0;
    }

}
