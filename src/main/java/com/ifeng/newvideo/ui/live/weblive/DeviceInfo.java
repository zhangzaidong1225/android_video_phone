package com.ifeng.newvideo.ui.live.weblive;


import android.webkit.JavascriptInterface;

import java.io.Serializable;

public class DeviceInfo implements Serializable {

    public String gv;
    public String av;
    public String uid;
    public String deviceId;
    public String proid;
    public String os;
    public String screen;
    public String df;
    public String vt;
    public String publishid;

    public String pushType;

    public String manufacturer;

    @JavascriptInterface
    public String getGv() {
        return gv;
    }

    public void setGv(String gv) {
        this.gv = gv;
    }

    @JavascriptInterface
    public String getAv() {
        return av;
    }

    public void setAv(String av) {
        this.av = av;
    }

    @JavascriptInterface
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @JavascriptInterface
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JavascriptInterface
    public String getProid() {
        return proid;
    }

    public void setProid(String proid) {
        this.proid = proid;
    }

    @JavascriptInterface
    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    @JavascriptInterface
    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    @JavascriptInterface
    public String getDf() {
        return df;
    }

    public void setDf(String df) {
        this.df = df;
    }

    @JavascriptInterface
    public String getVt() {
        return vt;
    }

    public void setVt(String vt) {
        this.vt = vt;
    }

    @JavascriptInterface
    public String getPublishid() {
        return publishid;
    }

    public void setPublishid(String publishid) {
        this.publishid = publishid;
    }

    @JavascriptInterface
    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    @JavascriptInterface
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "gv='" + gv + '\'' +
                ", av='" + av + '\'' +
                ", uid='" + uid + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", proid='" + proid + '\'' +
                ", os='" + os + '\'' +
                ", screen='" + screen + '\'' +
                ", df='" + df + '\'' +
                ", vt='" + vt + '\'' +
                ", publishid='" + publishid + '\'' +
                ", pushType='" + pushType + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                '}';
    }
}
