package com.ifeng.newvideo.ui.live;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.adapter.TVLiveListAdapter;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.LiveDao;
import com.ifeng.video.dao.db.model.live.CurrentTVLiveListInfo;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.dao.db.model.live.TVLiveListInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 电视台直播列表页
 * Created by Administrator on 2016/7/19.
 */
public class TVLiveListActivity extends BaseFragmentActivity implements RequestData {
    private static final Logger logger = LoggerFactory.getLogger(TVLiveListActivity.class);

    private UIStatusLayout uiStatusLayout;
    private TVLiveListAdapter adapter;
    private List<CurrentTVLiveListInfo.CurrentLiveInfoEntity> scheduleList = new ArrayList<>();//播放节目列表
    private List<TVLiveListInfo.LiveInfoEntity> tvList = new ArrayList<>();//电视台列表(不包含播放节目信息)

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_live_list_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initView();
        getTVLiveList();//获取电视台列表
        getAllTVLiveList();//获取播放节目信息
    }


    @Override
    public void requestData() {
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        getTVLiveList();//获取电视台列表
        getAllTVLiveList();//获取播放节目信息
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void initView() {
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(getResources().getString(R.string.live_tv));
        uiStatusLayout = (UIStatusLayout) findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        PullToRefreshListView listView = uiStatusLayout.getListView();
        listView.getRefreshableView().setBackgroundColor(Color.parseColor("#eeeeee"));
        listView.setMode(PullToRefreshBase.Mode.DISABLED);
        adapter = new TVLiveListAdapter(this);
        listView.setAdapter(adapter);
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
    }

    /**
     * 获取电视台直播节目信息
     */
    private void getAllTVLiveList() {
        LiveDao.getTVLiveInfo(CurrentTVLiveListInfo.class,
                new Response.Listener<CurrentTVLiveListInfo>() {
                    @Override
                    public void onResponse(CurrentTVLiveListInfo response) {
                        scheduleList.clear();
                        if (response != null && !ListUtils.isEmpty(response.getCurrentLiveInfo())) {
                            for (CurrentTVLiveListInfo.CurrentLiveInfoEntity model : response.getCurrentLiveInfo()) {
                                if (TextUtils.isEmpty(model.getChannelId())) {
                                    continue;
                                }
                                scheduleList.add(model);
                            }
                        }
                        isGetScheduleData = true;//无论数据是否获取成功，都发送成功消息
                        handler.sendEmptyMessage(MESSAGE);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isGetScheduleData = true;//无论数据是否获取成功，都发送成功消息
                        handler.sendEmptyMessage(MESSAGE);
                        logger.error("error=={}", error.toString());
                    }
                }, false);

    }

    /**
     * 获取电视台直播列表
     */
    private void getTVLiveList() {
        LiveDao.getTVLiveAddress(TVLiveListInfo.class,
                new Response.Listener<TVLiveListInfo>() {
                    @Override
                    public void onResponse(TVLiveListInfo response) {
                        tvList.clear();
                        if (response != null && !ListUtils.isEmpty(response.getLiveInfo())) {
                            for (TVLiveListInfo.LiveInfoEntity entity : response.getLiveInfo()) {
                                if (TextUtils.isEmpty(entity.getChannelId())) {
                                    continue;
                                }
                                tvList.add(entity);
                            }
                            isGetTVListData = true;
                            handler.sendEmptyMessage(MESSAGE);
                        } else {
                            uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error instanceof NetworkError) {
                            uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
                        } else {
                            uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                        }
                    }
                }, false);
    }

    /**
     * 重新组合数据
     */
    private List<TVLiveInfo> changeTVList() {
        List<TVLiveInfo> tvLiveInfoList = new ArrayList<>();
        for (TVLiveListInfo.LiveInfoEntity liveInfoEntity : tvList) {
            TVLiveInfo tvLiveInfo = new TVLiveInfo();
            tvLiveInfo.setTitle(liveInfoEntity.getTitle());
            tvLiveInfo.setcName(liveInfoEntity.getCName());
            tvLiveInfo.setVideo(liveInfoEntity.getVideo());
            tvLiveInfo.setVideoInReview(liveInfoEntity.getVideoInReview());
            tvLiveInfo.setVideoH(liveInfoEntity.getVideoH());
            tvLiveInfo.setVideoM(liveInfoEntity.getVideoM());
            tvLiveInfo.setVideoL(liveInfoEntity.getVideoL());
            tvLiveInfo.setBigIconURL(liveInfoEntity.getBigIconURL());
            tvLiveInfo.setSmallIconURL(liveInfoEntity.getSmallIconURL());
            tvLiveInfo.setDescription(liveInfoEntity.getDescription());
            tvLiveInfo.setmUrl(liveInfoEntity.getMUrl());
            tvLiveInfo.setImg490_490(liveInfoEntity.getImg490_490());
            tvLiveInfo.setChannelId(liveInfoEntity.getChannelId());

            String channelId = liveInfoEntity.getChannelId().toUpperCase();
            for (CurrentTVLiveListInfo.CurrentLiveInfoEntity currentLiveInfoEntity : scheduleList) {
                if (channelId.equals(currentLiveInfoEntity.getChannelId().toUpperCase())) {//channelId相等
                    List<TVLiveInfo.Schedule> schedules = new ArrayList<>();
                    List<CurrentTVLiveListInfo.CurrentLiveInfoEntity.ScheduleEntity> scheduleEntities = currentLiveInfoEntity.getSchedule();
                    for (CurrentTVLiveListInfo.CurrentLiveInfoEntity.ScheduleEntity scheduleEntity : scheduleEntities) {
                        TVLiveInfo.Schedule schedule = tvLiveInfo.new Schedule();
                        schedule.setStartTime(scheduleEntity.getStartTime());
                        schedule.setEndTime(scheduleEntity.getEndTime());
                        schedule.setProgramTitle(scheduleEntity.getProgramTitle());
                        schedules.add(schedule);
                    }
                    tvLiveInfo.setSchedule(schedules);
                }
            }
            tvLiveInfoList.add(tvLiveInfo);
        }
        return tvLiveInfoList;
    }

    private boolean isGetTVListData = false;//是否获取到电视台直播列表数据
    private boolean isGetScheduleData = false;//是否获取到直播节目数据

    private static final int MESSAGE = 100;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE:
                    if (isGetTVListData && isGetScheduleData) {
                        if (changeTVList().size() > 0) {
                            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
                            adapter.setData(changeTVList());
                        } else {
                            uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                        }
                    }
                    break;
            }
        }
    };
}
