package com.ifeng.newvideo.ui.live.vr;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.statistics.domains.LiveRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.live.liveplayer.VRLiveLandMediaController;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.model.LiveRoomModel;
import com.ifeng.video.player.ChoosePlayerUtils;
import com.ifeng.video.player.PlayState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VR直播底页
 * Created by Administrator on 2016/5/30.
 */
public class VRLiveActivity extends VRBaseActivity {
    private static final Logger logger = LoggerFactory.getLogger(VRLiveActivity.class);

    @Override
    protected void onResume() {
        super.onResume();
        if (mobileNetShowing) {//如果显示运营商网络界面，直接返回bug #10026
            return;
        }
        insertCustomerStatistics(PlayState.STATE_PREPARING);//初始化统计:保证下次上报不被拦截
        if (!isCurrPauseState || OneKeyShare.isLandWebQQ) {//如果离开时不是暂停状态，播放
            OneKeyShare.isLandWebQQ = false;
            if (mVideoController != null && mVideoView != null) {
                mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
                mVideoView.start();
            }
        }
    }

    @Override
    protected void initData(Intent intent) {
        guid = intent.getStringExtra(IntentKey.VR_LIVE_ID);
        vrTitle = intent.getStringExtra(IntentKey.VR_LIVE_PROGRAM_TITLE);
        vrLiveUrl = intent.getStringExtra(IntentKey.VR_LIVE_URL);
        vrImageUrl = intent.getStringExtra(IntentKey.VR_LIVE_IMAGE_URL);
        vrEchid = intent.getStringExtra(IntentKey.VR_LIVE_ECHID);
        vrChid = intent.getStringExtra(IntentKey.VR_LIVE_CHID);
        vrShareUrl = intent.getStringExtra(IntentKey.VR_LIVE_SHARE_URL);
        if (iconShare != null) {
            iconShare.setVisibility(TextUtils.isEmpty(vrShareUrl) ? View.GONE : View.VISIBLE);//分享地址为空，不展示分享按钮
        }
        weMediaId = intent.getStringExtra(IntentKey.VR_LIVE_WE_MEDIA_ID);
        weMediaName = intent.getStringExtra(IntentKey.VR_LIVE_WE_MEDIA_NAME);
    }


    @Override
    protected void initVideoMediaController() {
        mVideoController = new VRLiveLandMediaController(this);
        mVideoController.setOnHiddenListener(this);
        mVideoController.setOnShownListener(this);
        mVideoController.setControllerToVideoPlayerListener(controllerListener);
        mVideoView.setMediaController(mVideoController);
        if (supportGyro != null) {//初始化VRLibrary获取是否支持陀螺仪信息回调到Controller中设置控件状态
            supportGyro.supportGyro(isSupportGyro);
        }
    }

    @Override
    protected void getPlayData() {
        if (!isSupportVRLive) {//如果手机不支持VR播放,直接返回
            return;
        }
        final LiveRoomModel liveRoomModel = getLiveRoomModel(guid);
        if (liveRoomModel != null) {
            currentModel = liveRoomModel;
        }
        if (!NetUtils.isNetAvailable(this)) {//没有网络情况下显示无网界面
            updateNoNetLayer(true);
        } else {
            updateLoadingLayer(true);//有网情况下显示loading界面
            if (liveRoomModel != null) {
                prepareToPlay();
            }
        }
    }

    @Override
    protected void onVideoPlayComplete() {
        SendSmartStatisticUtils.sendPlayOperatorStatistics(VRLiveActivity.this, UserOperatorConst.TYPE_VRLIVE, getDuration(), weMediaName, vrTitle);
        if (!NetUtils.isNetAvailable(this) && vrLiveUrl.endsWith(".m3u8")) {
            mHandler.sendEmptyMessage(MESSAGE_ERROR);//因为直播流断网之后把缓冲的播完会直接报COMPLETE状态，而不是ERROR状态，所以让状态再转为ERROR状态
            updateNoNetLayer(true);//如果没有网络并且是直播流显示ERROR界面
        } else if (NetUtils.isMobile(this)) {
            mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
            updateMobileNetLayer(true);//如果是运营商网络显示运营商提示界面
        } else {
            playPauseImg.setImageResource(R.drawable.btn_play);//播放完成，图标改为播放状态
        }
    }

    @Override
    protected void initVRecord() {
        if (currentModel != null) {
            mVRecord = new LiveRecord(currentModel.getTitle(), vrTitle, "", VodRecord.P_TYPE_LV,
                    currentModel.getName(), (LiveRecord) mVRecord, vrEchid, ChoosePlayerUtils.useIJKPlayer(VRLiveActivity.this), weMediaId, weMediaName, "", "");
        }
    }

    @Override
    protected boolean isToastChangeStream() {
        return false;
    }

    @Override
    protected int getVideoType() {
        return TYPE_LIVE;
    }
}
