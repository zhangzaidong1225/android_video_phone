package com.ifeng.newvideo.ui.live.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.live.FragmentLiveProgramList;
import com.ifeng.video.dao.db.model.LiveChannelModels;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Vincent on 2014/11/18.
 */
public class LiveProgramAdapter extends BaseAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LiveProgramAdapter.class);
    private final Context mContext;
    private List<LiveChannelModels.LiveChannelModel> data;
    private LiveInfoModel infoModel;
    private final int day;
    private final FragmentLiveProgramList.NotifyPlayingProgram notifyPlayingProgram;
    private boolean initFocus; //初始化聚焦

    public LiveProgramAdapter(Context mContext, int day, FragmentLiveProgramList.NotifyPlayingProgram notifyPlayingProgram) {
        this.mContext = mContext;
        this.day = day;
        this.notifyPlayingProgram = notifyPlayingProgram;
    }

    public void setData(List<LiveChannelModels.LiveChannelModel> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    public LiveChannelModels.LiveChannelModel getModel(int position) {
        return data.get(position);
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProgramHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.live_program_item, null);
            holder = new ProgramHolder();
            holder.initView(convertView);
            convertView.setTag(holder);
        }
        LiveChannelModels.LiveChannelModel model = data.get(position);
        holder = (ProgramHolder) convertView.getTag();
        holder.setModel(model, position);
        holder.initData();
        holder.initProgramListState();
        holder.setListener();
        return convertView;
    }

    public class ProgramHolder implements View.OnClickListener {
        TextView tvProgram, tvLiving, tvReview;
        LiveChannelModels.LiveChannelModel model;
        int position;

        public void initView(View convertView) {
            tvProgram = (TextView) convertView.findViewById(R.id.tv_program);
            tvLiving = (TextView) convertView.findViewById(R.id.tv_living);
            tvReview = (TextView) convertView.findViewById(R.id.tv_review);
        }

        public void initData() {
            if (model != null) {
                String time = model.getStartTime();
                time = time.substring(time.indexOf(" "), time.length() - 3);
                setMessge(tvProgram, time + " " + model.getProgramTitle());
                infoModel.setStartTime(model.getStartTime());
                infoModel.setProgramTitle(model.getProgramTitle());
            }
        }

        private void initProgramListState() {
            switch (model.getState()) {
                case LiveChannelModels.LiveChannelModel.LIVE:
                    tvProgram.setTextColor(Color.rgb(18, 18, 18));
                    tvLiving.setVisibility(View.VISIBLE);
                    tvReview.setVisibility(View.GONE);
                    tvLiving.setSelected(false);
                    break;

                case LiveChannelModels.LiveChannelModel.BOOK:
                    tvProgram.setTextColor(Color.rgb(18, 18, 18));
                    tvLiving.setVisibility(View.GONE);
                    tvReview.setVisibility(View.GONE);
                    break;

                case LiveChannelModels.LiveChannelModel.OVER:
                    tvProgram.setTextColor(Color.rgb(166, 166, 166));
                    tvLiving.setVisibility(View.GONE);
                    tvReview.setVisibility(View.GONE);
                    break;

                case LiveChannelModels.LiveChannelModel.LOOKBACK:
                    tvProgram.setTextColor(Color.rgb(166, 166, 166));
                    tvLiving.setVisibility(View.GONE);
                    tvReview.setVisibility(View.VISIBLE);
                    tvReview.setSelected(false);
                    tvReview.setText(tvReview.getResources().getString(R.string.review));
                    break;

                case LiveChannelModels.LiveChannelModel.BOOKING:
                    tvProgram.setTextColor(Color.rgb(18, 18, 18));
                    tvLiving.setVisibility(View.GONE);
                    tvReview.setVisibility(View.GONE);
                    break;

                case LiveChannelModels.LiveChannelModel.LIVING:
                    tvProgram.setTextColor(Color.rgb(245, 67, 67));
                    tvLiving.setVisibility(View.VISIBLE);
                    tvReview.setVisibility(View.GONE);
                    tvLiving.setSelected(true);
                    //首次聚焦的时候 我们需要通过回调来通知上层进行播放。
                    if (!initFocus) {
                        initFocus = true;
                        notifyPlayingProgram.playingProgam(model, day, position);
                    }
                    break;

                case LiveChannelModels.LiveChannelModel.LOOKBACKING:
                    tvProgram.setTextColor(Color.rgb(245, 67, 67));
                    tvLiving.setVisibility(View.GONE);
                    tvReview.setVisibility(View.VISIBLE);
                    tvReview.setSelected(true);
                    tvReview.setText(tvReview.getResources().getString(R.string.reviewing));
                    break;
            }
        }

        private void setListener() {
            tvLiving.setOnClickListener(this);
            tvReview.setOnClickListener(this);
        }

        void setMessge(TextView tv, String str) {
            if (tv != null && !TextUtils.isEmpty(str)) {
                tv.setText(str);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_living:
                    if (model.getState() == LiveChannelModels.LiveChannelModel.LIVE) {
                        model.setState(LiveChannelModels.LiveChannelModel.LIVING);
                        tvLiving.setSelected(true);
                        //切回直播来 TODO  加上播放器逻辑
                        tvProgram.setTextColor(Color.rgb(245, 67, 67));
                        notifyPlayingProgram.playingProgam(model, day, position);
                    }
                    break;
                case R.id.tv_review:
                    if (model.getState() == LiveChannelModels.LiveChannelModel.LOOKBACK) {
                        model.setState(LiveChannelModels.LiveChannelModel.LOOKBACKING);
                        tvReview.setSelected(true);
                        tvReview.setText(tvReview.getResources().getString(R.string.reviewing));
                        tvProgram.setTextColor(Color.rgb(245, 67, 67));
                        notifyPlayingProgram.playingProgam(model, day, position);
                    }
                    break;
            }
        }

        void setModel(LiveChannelModels.LiveChannelModel model, int position) {
            this.model = model;
            this.position = position;
        }
    }

    /**
     * 此方法主要用于点击直播或回看时及时刷新状态用
     *
     * @param position
     */
    public void setPlayingChange(int position) {
        if (data == null)
            return;

        for (int i = 0; i < data.size(); i++) {
            if (position == i)
                continue;

            LiveChannelModels.LiveChannelModel model = data.get(i);
            int state = model.getState();

            if (state == LiveChannelModels.LiveChannelModel.LIVING) {
                model.setState(LiveChannelModels.LiveChannelModel.LIVE);
                continue;
            }

            if (state == LiveChannelModels.LiveChannelModel.LOOKBACKING) {
                model.setState(LiveChannelModels.LiveChannelModel.LOOKBACK);
            }
        }
        notifyDataSetChanged();
    }

    public void setModel(LiveInfoModel model) {
        this.infoModel = model;
    }

}
