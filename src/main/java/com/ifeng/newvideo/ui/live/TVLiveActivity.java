package com.ifeng.newvideo.ui.live;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.adapter.TVLivePagerAdapter;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.widget.skin.TitleView;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.video.core.utils.*;
import com.ifeng.video.dao.db.dao.LiveDao;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.dao.db.model.live.TVLiveListInfo;
import com.ifeng.video.dao.db.model.live.TVLiveProgramInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 电视台直播页
 * Created by Administrator on 2016/7/28.
 */
public class TVLiveActivity extends BaseFragmentActivity implements TVLiveProgramFragment.IScheduleInfo,
        TitleView.OnVideoAudioChangeListener, TitleView.OnShareProgramLClickListener, NotifyShareCallback, RequestData {
    private static final Logger logger = LoggerFactory.getLogger(TVLiveActivity.class);
    public static final int DAY_COUNT = 3;//一共3天

    /**
     * 视频播放相关的
     */

    private NormalVideoHelper mPlayerHelper;
    private VideoSkin mVideoSkin;
    private UIPlayContext mPlayContext;
    private View bottomLayout;

    /**
     * TabStrip
     */
    private PagerSlidingTabStrip slidingTabStrip;
    private ViewPager viewPager;
    private UIStatusLayout uiStatusLayout;
    private TVLivePagerAdapter pagerAdapter;
    private OneKeyShare mOneKeyShare;

    private String channelId;//推送过来的channelId
    private String echid;
    private String chid;
    private String weMediaId;
    private TVLiveInfo tvLiveInfo;
    private String channelName;//频道名称

    private List<TVLiveListInfo.LiveInfoEntity> tvList = new ArrayList<>();//电视台列表

    //精选传入分享数据，需记录最初的videoItem
    private String shareTitle = null;
    private String shareImg = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_live);
        enableExitWithSlip(false);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        mOneKeyShare = new OneKeyShare(this);
        initView();
        initSkin();
        initIntent(getIntent());
    }

    protected boolean isExit(MotionEvent ev) {
        if (isLandScape()) {
            return false;
        }
        int playerHeight = DisplayUtils.getWindowWidth() * 9 / 16
                + DisplayUtils.getStatusBarHeight() + DisplayUtils.convertDipToPixel(10);
        if (ev != null && ev.getY() > playerHeight) {
            return viewPager.getCurrentItem() == 0;
        }
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPlayerHelper != null) {
            mPlayerHelper.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPlayerHelper != null) {
            mPlayerHelper.onPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPlayerHelper != null) {
            mPlayerHelper.onDestroy();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mPlayerHelper != null) {
            mPlayerHelper.onConfigureChange(newConfig);
        }
    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {

    }

    /**
     * 获取当前直播节目信息
     *
     * @param entity 当前直播节目
     */
    @Override
    public void getScheduleInfo(TVLiveProgramInfo.ScheduleEntity entity) {
        List<TVLiveInfo.Schedule> scheduleList = new ArrayList<>();

        TVLiveInfo.Schedule schedule = tvLiveInfo.new Schedule();
        schedule.setProgramTitle(entity.getProgramTitle());

        scheduleList.add(schedule);
        tvLiveInfo.setSchedule(scheduleList);

        mPlayContext.tvLiveInfo = tvLiveInfo;
    }

    @Override
    public void onShareClick() {
        showSharePop();
    }

    @Override
    public void onLiveProgramClick() {

    }

    @Override
    public void onVideoAudioChange() {

    }

    @Override
    public void requestData() {
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        getTVLiveList();
    }

    @Override
    protected void onScreenOff() {
        if (mPlayerHelper != null) {
            mPlayerHelper.sendScreenOffStatistics();
        }
    }

    /**
     * 初始化View
     */
    private void initView() {
        uiStatusLayout = (UIStatusLayout) findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        bottomLayout = findViewById(R.id.bottom_layout);
        slidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.live_activity_slidingtab);
        slidingTabStrip.setShouldExpand(true);
        slidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                enableExitWithSlip(i == 0);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        viewPager = (ViewPager) findViewById(R.id.live_activity_pager);

    }

    /**
     * 初始化皮肤
     */
    private void initSkin() {
        mVideoSkin = (VideoSkin) findViewById(R.id.video_skin);
        mPlayContext = new UIPlayContext();
        mPlayContext.isFromPush = getIntent() != null && getIntent().getBooleanExtra(IntentKey.IS_FROM_PUSH, false);
        mPlayContext.skinType = VideoSkin.SKIN_TYPE_TV;
        mPlayerHelper = new NormalVideoHelper();
        mPlayerHelper.init(mVideoSkin, mPlayContext);
        mVideoSkin.setNoNetWorkListener(new VideoSkin.OnNetWorkChangeListener() {
            @Override
            public void onNoNetWorkClick() {
                if (tvLiveInfo == null) {
                    getTVLiveList();
                } else {
                    playVideo();
                }
            }

            @Override
            public void onMobileClick() {
                if (tvLiveInfo == null) {
                    getTVLiveList();
                } else {
                    playVideo();
                }
            }
        });
        mVideoSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener() {
            @Override
            public void onLoadFailedListener() {
                if (tvLiveInfo == null) {
                    getTVLiveList();
                } else {
                    playVideo();
                }
            }
        });

        if (null != mVideoSkin.getTitleView()) {
            mVideoSkin.getTitleView().setOnShareProgramLClickListener(this);
            mVideoSkin.getTitleView().setOnVideoAudioChangeListener(this);
        }
    }

    /**
     * 初始化Intent
     */
    private void initIntent(Intent intent) {
        if (intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            channelId = bundle.getString(IntentKey.TV_LIVE_CHANNEL_ID);
            echid = bundle.getString(IntentKey.TV_LIVE_ECHID);
            chid = bundle.getString(IntentKey.TV_LIVE_CHID);
            shareTitle = getIntent().getExtras().getString(IntentKey.HOME_VIDEO_TITLE);
            shareImg = getIntent().getExtras().getString(IntentKey.HOME_VIDEO_IMG);
        }
        if (!NetUtils.isNetAvailable(TVLiveActivity.this)) {
            uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
            return;
        }
        if (TextUtils.isEmpty(channelId)) {
            showNoResourceDialog();
            return;
        }
        mPlayContext.channelId = echid;
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        getTVLiveList();
    }

    /**
     * 播放
     */
    private void playVideo() {
        if (NetUtils.isMobile(this) && IfengApplication.mobileNetCanPlay) {
            if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                ToastUtils.getInstance().showShortToast(R.string.play_moblie_net_hint);
            }
        }

        mPlayContext.title = channelName;
        mPlayContext.tvLiveInfo = tvLiveInfo;
        String url = StreamUtils.getMediaUrlForTV(tvLiveInfo);//加防盗链
        mPlayerHelper.openVideo(url);
        mPlayContext.streamType = StreamUtils.getLiveStreamType();
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        pagerAdapter = new TVLivePagerAdapter(TVLiveActivity.this, getSupportFragmentManager(), tvLiveInfo.getChannelId());
        viewPager.setOffscreenPageLimit(DAY_COUNT);
        viewPager.setAdapter(pagerAdapter);
        slidingTabStrip.setViewPager(viewPager);
        pagerAdapter.notifyDataSetChanged();
        slidingTabStrip.notifyDataSetChanged();
        viewPager.setCurrentItem(0);
    }

    private Dialog noResourceDialog;

    /**
     * 弹出没有资源提示窗
     */
    private void showNoResourceDialog() {
        noResourceDialog = AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.video_play_url_miss), getString(R.string.common_i_know), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (noResourceDialog != null && noResourceDialog.isShowing()) {
                    noResourceDialog.dismiss();
                }
                finish();
                Intent intent = new Intent(TVLiveActivity.this, ActivityMainTab.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    /**
     * 分享
     */
    private void showSharePop() {
        if (tvLiveInfo != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            String name = channelName;
            String image = tvLiveInfo.getBigIconURL();
            //精选首页进来，分享标题和图片为推荐位标题和图片
            if (!TextUtils.isEmpty(shareTitle) && !TextUtils.isEmpty(shareImg)) {
                name = shareTitle;
                image = shareImg;
            }
            mOneKeyShare.shareLive(tvLiveInfo.getmUrl(), mVideoSkin, name, this, tvLiveInfo.getcName(), image, false);
        }
    }

    /**
     * 获取电视台直播列表
     */
    private void getTVLiveList() {
        LiveDao.getTVLiveAddress(TVLiveListInfo.class,
                new Response.Listener<TVLiveListInfo>() {
                    @Override
                    public void onResponse(TVLiveListInfo response) {
                        if (response == null || ListUtils.isEmpty(response.getLiveInfo())) {
                            showNoResourceDialog();
                            return;
                        }
                        uiStatusLayout.setStatus(UIStatusLayout.ALL_GONE);
                        bottomLayout.setVisibility(View.VISIBLE);
                        tvList = response.getLiveInfo();
                        for (TVLiveListInfo.LiveInfoEntity entity : tvList) {
                            if (TextUtils.isEmpty(entity.getChannelId())) {
                                continue;
                            }
                            // 拿传进来的channelId去接口中匹配channelId或者title
                            boolean isChannelIdExist = entity.getChannelId().toLowerCase().equalsIgnoreCase(channelId)
                                    || entity.getTitle().toLowerCase().equalsIgnoreCase(channelId);
                            if (isChannelIdExist) {
                                // 拿数据去播放
                                initTVLiveData(entity);
                                return;
                            }
                        }

                        // 如果没有找到channelId
                        showNoResourceDialog();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error instanceof NetworkError) {
                            uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
                        } else {
                            uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                            bottomLayout.setVisibility(View.GONE);
                        }
                        logger.error("error={}", error);
                    }
                }, false);
    }

    /**
     * 初始化节目信息
     */
    private void initTVLiveData(TVLiveListInfo.LiveInfoEntity liveInfoEntity) {
        tvLiveInfo = new TVLiveInfo();
        tvLiveInfo.setChannelId(liveInfoEntity.getChannelId());
        tvLiveInfo.setmUrl(liveInfoEntity.getMUrl());
        tvLiveInfo.setTitle(liveInfoEntity.getTitle());
        tvLiveInfo.setcName(liveInfoEntity.getCName());
        tvLiveInfo.setVideo(liveInfoEntity.getVideo());
        tvLiveInfo.setVideoInReview(liveInfoEntity.getVideoInReview());
        tvLiveInfo.setVideoH(liveInfoEntity.getVideoH());
        tvLiveInfo.setVideoM(liveInfoEntity.getVideoM());
        tvLiveInfo.setVideoL(liveInfoEntity.getVideoL());
        tvLiveInfo.setBigIconURL(liveInfoEntity.getBigIconURL());
        tvLiveInfo.setSmallIconURL(liveInfoEntity.getSmallIconURL());
        tvLiveInfo.setDescription(liveInfoEntity.getDescription());
        tvLiveInfo.setImg490_490(liveInfoEntity.getImg490_490());

        channelName = tvLiveInfo.getcName();

        playVideo();

        mOneKeyShare.initShareStatisticsData(tvLiveInfo.getChannelId(), echid, "", "", PageIdConstants.PLAY_LIVE_V);
        mOneKeyShare.initSmartShareData(UserOperatorConst.TYPE_LIVE, channelName, tvLiveInfo.getTitle());

        initAdapter();
    }


}
