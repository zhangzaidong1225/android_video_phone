package com.ifeng.newvideo.ui.live.vr;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.asha.vrlib.MDVRLibrary;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.LiveRecord;
import com.ifeng.newvideo.statistics.domains.VideoRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.liveplayer.VRLiveLandMediaController;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.dao.SurveyDao;
import com.ifeng.newvideo.videoplayer.listener.ConnectivityReceiver;
import com.ifeng.newvideo.videoplayer.player.PlayUrlAuthUtils;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.LiveRoomModel;
import com.ifeng.video.player.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.Date;

import static com.ifeng.newvideo.videoplayer.widget.skin.TitleView.DATE_FORMAT;

/**
 * VR播放页基类
 * Created by Administrator on 2016/11/15.
 */
public abstract class VRBaseActivity extends BaseFragmentActivity implements NotifyShareCallback, View.OnClickListener,
        ConnectivityReceiver.ConnectivityChangeListener, IfengMediaController.OnShownListener, IfengMediaController.OnHiddenListener, StateListener {
    private static final Logger logger = LoggerFactory.getLogger(VRBaseActivity.class);

    protected static final int TYPE_VIDEO = 1;//VR点播
    protected static final int TYPE_LIVE = 2;//VR直播

    protected IfengVRVideoView mVideoView;//videoView
    private ImageView backImg;//返回键

    protected ImageView playPauseImg;//播放、暂停图标

    private TextView errorRetryBottomTv;//加载失败啦文字

    private RelativeLayout stateParentLayout;//所有状态层的父布局
    private View mobileNetLayer;//运营商网络
    private View errorLayer;//错误
    private View noNetLayer;//无网
    private View errorRetryLayer;
    private RelativeLayout bufferingLayer;//正在载入布局
    private ViewGroup loadingLayer;//加载
    private TextView loadingTitle;//loading界面的title
    protected View mSelectStreamLayout;//选择码流布局

    private View topTitleLayout;//顶部标题布局：返回键，标题，分享图标
    private View batteryAndTimeLayout;
    private TextView timeView;
    private TextView tvTopTitle;//顶部标题
    protected ImageView iconShare;//标题分享

    public MDVRLibrary mVRLibrary;
    private Surface mSurface;

    protected VideoRecord mVRecord;
    protected OneKeyShare onekeyShare;

    protected IfengMediaController mVideoController;
    private ConnectivityReceiver connectivityReceiver;

    protected boolean isShouldShowControllerView = true;//是否显示控制栏，第一次播放:正常播放和点击通知栏进入播放
    protected boolean mIsNewRegisteredReceiver = true;
    protected boolean isActive = true;
    protected boolean mobileNetShowing = false;//显示手机网络提示界面
    protected boolean isSupportGyro = true;//是否支持陀螺仪功能
    protected boolean isCurrPauseState = false;// 记录离开页面的时候是否本身就为暂停状态
    protected boolean isSupportVRLive = Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN;//4.1系统以下不支持VR直播

    protected String guid;
    protected String vrTitle = "";//vr标题
    protected String vrLiveUrl;//VR地址
    protected String vrImageUrl;
    protected String vrEchid = "";
    protected String vrChid = "";
    protected String weMediaId;
    protected String weMediaName = "";
    protected String vrShareUrl = "";

    protected LiveRoomModel currentModel;

    private final static int DELAY_TIME = 2 * 1000;
    private final static int DELAY_STOP_TIME = 10 * 1000;
    private final static int TIMEOUT_MAX = 30 * 1000;
    protected final static int MESSAGE_ERROR = 1;
    private final static int MESSAGE_KARTUN = 2;

    protected long seekToPosition = 0;//播放位置

    protected boolean isChangeStream = false;//是否切了换清晰度，该标记用于V统计

    protected int currentPositionInVideoList;//当前视频在视频列表中的索引(VR点播)

    /**
     * 初始化数据
     */
    protected abstract void initData(Intent intent);

    /**
     * 获取将要播放数据
     */
    protected abstract void getPlayData();

    /**
     * 初始化MediaController
     */
    protected abstract void initVideoMediaController();

    /**
     * 获取视频类型
     * <p/>
     * return:TYPE_VIDEO、TYPE_LIVE
     */
    protected abstract int getVideoType();

    /**
     * 视频播放完成
     */
    protected abstract void onVideoPlayComplete();

    /**
     * 初始化V统计
     */
    protected abstract void initVRecord();

    /**
     * 是否提示切换清晰度
     */
    protected abstract boolean isToastChangeStream();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vr_live);
        onekeyShare = new OneKeyShare(this);
        onekeyShare.notifyShareCallback = this;
        stopAudioPlayer();
        initData(getIntent());
        initView();//1
        initVRLibrary();//2
        initVideoMediaController();//3
        registerMobileAlertReceiver();
        getPlayData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVRLibrary.onResume(this);
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVRLibrary.onPause(this);
        if (mVideoView != null) {
            if (mVideoView.isInPlaybackState()) {
                seekToPosition = mVideoView.getCurrentPosition();
            }
            isCurrPauseState = !mVideoView.isPlaying();
            if (isCurrPauseState) {//是暂停状态
                if (mVideoController != null && mVideoView.isInPlaybackState()) {
                    seekToPosition = mVideoView.getCurrentPosition();
                    mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                    mVideoView.pause();
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mVideoView != null && mVideoView.isInPlaybackState() && mVideoController != null) {
            mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
            mVideoView.pause();
        }
        insertCustomerStatistics(PlayState.STATE_IDLE);//发送统计
        isActive = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        unRegistMobileAlertReceiver();
        mVRLibrary.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        updateLoadingLayer(true);
        isShouldShowControllerView = true;//点击推送进来展示控制栏
        mVideoView.stopPlayback();
        initData(intent);
        getPlayData();
    }

    protected void initView() {
        DisplayUtils.setDisplayStatusBar(VRBaseActivity.this, true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        ViewGroup parentLayout = (ViewGroup) findViewById(R.id.vr_live_playlayout);
        mVideoView = (IfengVRVideoView) findViewById(R.id.player_VR_videoview);
        mVideoView.setIsFullScreen(true);//设置即将播放页面横屏全屏展示
        mVideoView.setVideoLayout(IfengVideoView.VIDEO_LAYOUT_FULL);

        //隐藏back键
        backImg = (ImageView) parentLayout.findViewById(R.id.media_controller_back);
        backImg.setVisibility(View.GONE);
        backImg.setOnClickListener(this);
        // 顶部控制栏
        topTitleLayout = findViewById(R.id.video_top_title_layout);
        tvTopTitle = (TextView) topTitleLayout.findViewById(R.id.video_landscape_title);
        // 电量 时间
        batteryAndTimeLayout = findViewById(R.id.battery_time_layout);
        timeView = (TextView) findViewById(R.id.system_time);

        View mTitleLayBackBtnClick = topTitleLayout.findViewById(R.id.video_detail_landscape_top_back_btn_click);
        mTitleLayBackBtnClick.setOnClickListener(this);
        mTitleLayBackBtnClick.setVisibility(View.VISIBLE);
        iconShare = (ImageView) topTitleLayout.findViewById(R.id.icon_share);
        iconShare.setOnClickListener(this);
        iconShare.setVisibility(TextUtils.isEmpty(vrShareUrl) ? View.GONE : View.VISIBLE);//分享地址为空，不展示分享按钮
        //播放、暂停
        playPauseImg = (ImageView) findViewById(R.id.btn_play_pause);
        playPauseImg.setOnClickListener(this);
        playPauseImg.setVisibility(View.GONE);

        stateParentLayout = (RelativeLayout) parentLayout.findViewById(R.id.video_loading_and_retry_lay);//loading层父布局
        //loading层
        loadingLayer = (ViewGroup) parentLayout.findViewById(R.id.video_loading_layout);
        loadingTitle = ((TextView) loadingLayer.findViewById(R.id.progress_text_info));//loading层中的标题
        //buffering正在载入层
        bufferingLayer = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.video_buffer_progress, null);
        //使用运营商网络布局
        mobileNetLayer = LayoutInflater.from(this).inflate(R.layout.common_video_mobile_net_layer, null);
        mobileNetLayer.findViewById(R.id.video_mobile_continue).setOnClickListener(this);
        //有网加载失败布局
        errorLayer = LayoutInflater.from(this).inflate(R.layout.common_video_error_retry_layer, null);
        errorLayer.findViewById(R.id.video_error_retry_img).setOnClickListener(this);
        errorRetryLayer = errorLayer.findViewById(R.id.video_error_retry_layout);
        errorRetryBottomTv = (TextView) errorLayer.findViewById(R.id.video_error_retry_bottom_tv);
        //没网加载失败布局
        noNetLayer = LayoutInflater.from(this).inflate(R.layout.common_video_error_pause_layer, null);
        noNetLayer.findViewById(R.id.pauseToResume).setOnClickListener(this);
        //选择码流布局
        mSelectStreamLayout = findViewById(R.id.video_land_right_stream);
        mSelectStreamLayout.setOnClickListener(this);
        mSelectStreamLayout.setVisibility(View.GONE);
        //手机系统不支持VR播放提示布局
        ViewStub v = (ViewStub) parentLayout.findViewById(R.id.view_stub);
        if (!isSupportVRLive) {//不支持VR播放,显示不支持布局
            setControllerShowHide(false);//点击界面时控制栏不显示
            v.inflate();
            topTitleLayout.setVisibility(View.VISIBLE);
            tvTopTitle.setVisibility(View.GONE);
            mTitleLayBackBtnClick.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 关闭音频播放
     */
    private void stopAudioPlayer() {
        if (ActivityMainTab.mAudioService != null) {
            ActivityMainTab.mAudioService.stopAudioService(VideoSkin.SKIN_TYPE_SPECIAL);
        }
    }

    /**
     * 初始化分享统计数据
     */
    private void initShareData() {
        onekeyShare.initShareStatisticsData(guid, vrEchid, vrChid, weMediaId, PageIdConstants.PLAY_VR);
        onekeyShare.initSmartShareData(getVideoType() == TYPE_LIVE ? UserOperatorConst.TYPE_VRLIVE : UserOperatorConst.TYPE_VIDEO, weMediaName, vrTitle);
    }

    /**
     * 锁屏广播回调
     */
    @Override
    protected void onScreenOff() {
        if (mVideoView.isInPlaybackState()) {
            SendSmartStatisticUtils.sendPlayOperatorStatistics(VRBaseActivity.this,
                    getVideoType() == TYPE_LIVE ? UserOperatorConst.TYPE_VRLIVE : UserOperatorConst.TYPE_VIDEO,
                    getDuration(), weMediaName, vrTitle);
        }
    }

    private void initVRLibrary() {
        mVRLibrary = MDVRLibrary.with(this)
                .displayMode(MDVRLibrary.DISPLAY_MODE_NORMAL)
                .interactiveMode(MDVRLibrary.INTERACTIVE_MODE_MOTION)
                .video(new MDVRLibrary.IOnSurfaceReadyCallback() {
                    @Override
                    public void onSurfaceReady(Surface surface) {
                        if (surface != null) {
                            mSurface = surface;
                        }
                        setSurface(surface);
                    }
                })
                .ifNotSupport(new MDVRLibrary.INotSupportCallback() {
                    @Override
                    public void onNotSupport(int mode) {
                        isSupportGyro = false;
                    }
                })
                .pinchEnabled(true)//是否支持缩放功能
                .isSupportVR(isSupportVRLive)
                .gesture(new MDVRLibrary.IGestureListener() {
                    @Override
                    public void onClick(MotionEvent e) {
                        if (mSelectStreamLayout.getVisibility() == View.VISIBLE) {
                            mSelectStreamLayout.setVisibility(View.GONE);
                        }
                    }
                })
                .build(R.id.player_VR_videoview);
        if (!isSupportGyro) {//如果不支持陀螺仪则转换为手势模式
            mVRLibrary.switchInteractiveMode(VRBaseActivity.this);
        }
    }

    protected void prepareToPlay() {
        if (mVideoView != null && mVideoView.isInPlaybackState()) {
            mVideoView.stopPlayback();
        }

        if (isActive) {
            if (NetUtils.isMobile(this)) {//是手机网络
                if (IfengApplication.mobileNetCanPlay) {
                    if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                        ToastUtils.getInstance().showLongToast(R.string.play_moblie_net_hint); //提示运营商网络
                    }
                } else {
                    updateMobileNetLayer(true);
                    return;
                }
            }
        }
        loadingLayer.findViewById(R.id.video_loading_imageview).setVisibility(View.GONE);//凤凰视频图片
        videoPlayerInit();
    }

    private void videoPlayerInit() {
        if (currentModel == null) {
            updateErrorLayer(true);
            return;
        }
        String url = currentModel.getVideo();
        mVideoView.setStateListener(this);
        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                mVideoView.seekStopTouchLitener(new IfengMediaController.SeekStopTouchLitener() {
                    @Override
                    public void seekStopTouch() {
                        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PROGRESS_DRAG, null, PageIdConstants.PLAY_VR);
                    }
                });
            }
        });
        UserFeed.initIfengAddress(IfengApplication.getAppContext(), url);
        //如果视频地址地址不存在
        if (TextUtils.isEmpty(url)) {
            if (NetUtils.isNetAvailable(this)) {
                updateErrorLayer(true);
            } else {
                updateNoNetLayer(true);
            }
            hideController();
            return;
        }
        logger.debug("videoPlayerInit() lastPosition={}", seekToPosition);
        if (seekToPosition > 0) {
            mVideoView.seekTo(seekToPosition);
            seekToPosition = 0;
        }
        if (this instanceof VRVideoActivity) {
            SurveyDao.accumulatorPlayCount(guid);
        }

        IMediaPlayer.MediaSource source = new IMediaPlayer.MediaSource();
        source.id = PlayType.FEATRUE;
        source.playUrl = PlayUrlAuthUtils.getVideoAuthUrl(url, guid);
        IMediaPlayer.MediaSource[] mediaSources = new IMediaPlayer.MediaSource[1];
        mediaSources[0] = source;
        mVideoView.setVideoPath(mediaSources);
        if (playPauseImg != null) {
            playPauseImg.setImageResource(R.drawable.btn_pause);
        }
    }

    /**
     * 获取互动直播对象
     */
    protected LiveRoomModel getLiveRoomModel(String guid) {
        if (TextUtils.isEmpty(vrLiveUrl)) {
            return null;
        }
        return new LiveRoomModel(vrTitle, guid, vrLiveUrl, vrImageUrl);
    }

    /**
     * 获取当前播放位置
     */
    protected int getPlayerPosition() {
        int position = mVideoView.getCurrentPosition();
        if (position != 0) {
            return position;
        } else {
            return (int) mVideoView.getPrePosition();
        }
    }

    /**
     * 用于卡顿Toast提示
     */
    private int bufferCount;//卡顿次数
    private long millions;

    /**
     * 播放器状态回调方法
     */
    @Override
    public void onStateChange(PlayState state) {
        logger.debug("onStateChange()   state={} ", state.toString());
        currState = state;
        insertCustomerStatistics(state);
        switch (state) {
            case STATE_PREPARING:
                bufferCount = 0;
                updateLoadingLayer(true);
                setSurface(mSurface);
                break;
            case STATE_PAUSED:
                if (getVideoType() == TYPE_VIDEO) {
                    seekToPosition = getPlayerPosition();//记录当前播放位置
                }
                saveHistory(false);
                break;
            case STATE_PREPARED:
                break;
            case STATE_IDLE:
                SendSmartStatisticUtils.sendPlayOperatorStatistics(VRBaseActivity.this,
                        getVideoType() == TYPE_LIVE ? UserOperatorConst.TYPE_VRLIVE : UserOperatorConst.TYPE_VIDEO,
                        getDuration(), weMediaName, vrTitle);
                mHandler.removeMessages(MESSAGE_ERROR);
                break;
            case STATE_PLAYING:
                mVideoView.requestLayout();
                updateLoadingLayer(false);
                if (isShouldShowControllerView) {
                    showController();
                    isShouldShowControllerView = false;
                }
                break;
            case STATE_BUFFERING_START:
                millions = System.currentTimeMillis();
                updateBufferLayer(true);
                mHandler.removeMessages(MESSAGE_ERROR);
                mHandler.sendEmptyMessageDelayed(MESSAGE_ERROR, DELAY_STOP_TIME);
                break;
            case STATE_BUFFERING_END:
                long BUFFER_MILLIONS_DISTANCE = 3 * DateUtils.SECOND_IN_MILLIS;
                int BUFFER_TIMES = 3;
                long currentMillions = System.currentTimeMillis();
                if (currentMillions - millions > BUFFER_MILLIONS_DISTANCE) {
                    bufferCount++;
                }
                if (bufferCount == BUFFER_TIMES) {
                    if (isToastChangeStream()) {
                        ToastUtils.getInstance().showShortToast("当前网络缓慢，建议切换至低清晰度观看");
                    }
                    bufferCount = 0;
                }
                mHandler.removeMessages(MESSAGE_ERROR);
                updateBufferLayer(false);
                break;
            case STATE_PLAYBACK_COMPLETED:
                onVideoPlayComplete();
                break;
            case STATE_ERROR:
                if (getVideoType() == TYPE_VIDEO) {
                    seekToPosition = getPlayerPosition();//记录当前播放位置
                }
                SendSmartStatisticUtils.sendPlayOperatorStatistics(VRBaseActivity.this, getVideoType() == TYPE_LIVE ? UserOperatorConst.TYPE_VRLIVE : UserOperatorConst.TYPE_VIDEO,
                        getDuration(), weMediaName, vrTitle);
                handleErrorState();
                break;
        }
    }

    /**
     * 保存看过记录
     */
    protected void saveHistory(boolean isComplete) {
    }

    /**
     * 获取播放时长
     */
    protected String getDuration() {
        String duration = "";
        if (mVideoView.mMediaPlayer != null) {
            long durationMillions = mVideoView.mMediaPlayer.getCurrentPosition();
            if (durationMillions == 0L) {
                return "";
            }
            float durationSeconds = durationMillions * 1.0F / 1000F;
            duration = String.format("%.2f", durationSeconds);
        }
        return duration;
    }

    /**
     * 网络连接状态回调
     */
    @Override
    public void onConnectivityChange(String action) {
        if (mIsNewRegisteredReceiver) {//解决无网进入，连网之后重新播放
            mIsNewRegisteredReceiver = false;
            return;
        }
        if (NetUtils.isNetAvailable(this)) {//有网
            if (NetUtils.isMobile(this)) {//是手机网络
                if (!isInPlaybackState()) {
                    updateMobileNetLayer(true);
                }
            } else {//wifi网络
                updateMobileNetLayer(false);
                if (isInPlaybackState()) {
                    mVideoView.start();//播放
                } else {
                    if (isSupportVRLive) {
                        prepareToPlay();//重新播放
                    }
                }
            }
        } else {//无网
            if (errorRetryLayerIsShowing) {//如果展示的是加载失败页面，改为无网页面
                updateErrorLayer(false);
                updateNoNetLayer(true);
            }
            if (mobileNetShowing) {
                updateNoNetLayer(true);
                mobileNetShowing = false;
            }
        }
    }

    private void setSurface(Surface surface) {
        if (mVideoView.mMediaPlayer != null && surface != null) {
            mVideoView.mMediaPlayer.setSurface(surface);
        }
    }

    private boolean isInPlaybackState() {
        return (mVideoView != null && mVideoView.isInPlaybackState());
    }

    /**
     * 隐藏顶部标题栏和控制栏
     */
    private void hideTopAndControllerView() {
        topTitleLayout.setVisibility(View.GONE);
        mVideoController.hide();
        playPauseImg.setVisibility(View.GONE);
        mSelectStreamLayout.setVisibility(View.GONE);
    }

    /**
     * 控制栏按键事件回调
     */
    protected final ControllerToVideoPlayerListener controllerListener = new ControllerToVideoPlayerListener() {
        @Override
        public void onFullScreenClick() {
        }

        @Override
        public void onSubscribeClick() {

        }

        @Override
        public void onDlnaClick() {
        }

        @Override
        public void onStreamItemClick(int definition) {

        }

        @Override
        public void onSelectClick() {
            onSelectStreamClick();
            PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DEF, null, PageIdConstants.PLAY_VR);
        }

        @Override
        public void onLockClick() {

        }

        @Override
        public void onSwitchAVMode() {
            changeDisplayMode();
        }

        @Override
        public void onGyroClick() {
            changeInteractiveMode();

        }

        @Override
        public void onSwitchProgram(boolean nextOrPre) {

        }
    };

    /**
     * 点击码流文字
     */
    protected void onSelectStreamClick() {
    }

    /**
     * 更改交互模式
     */
    private void changeInteractiveMode() {
        if (!isSupportGyro) {//不支持陀螺仪，手动状态下直接返回
            return;
        }
        mVRLibrary.switchInteractiveMode(VRBaseActivity.this);
        switch (mVRLibrary.getInteractiveMode()) {
            case MDVRLibrary.INTERACTIVE_MODE_MOTION:
                ((VRLiveLandMediaController) mVideoController).setMotionImageSelected(true);
                supportGyro.isGyroMode(true);
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_VR_MOVE, true, PageIdConstants.PLAY_VR);
                break;
            case MDVRLibrary.INTERACTIVE_MODE_TOUCH:
                ((VRLiveLandMediaController) mVideoController).setMotionImageSelected(false);
                supportGyro.isGyroMode(false);
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_VR_MOVE, false, PageIdConstants.PLAY_VR);
                break;
        }
    }

    /**
     * 切换展示模式：双眼、正常
     */
    private void changeDisplayMode() {
        mVRLibrary.switchDisplayMode(VRBaseActivity.this);
        switch (mVRLibrary.getDisplayMode()) {
            case MDVRLibrary.DISPLAY_MODE_NORMAL://正常模式
                ((VRLiveLandMediaController) mVideoController).setGlassesImageSelected(false);
                supportGyro.isGlassesMode(false);
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_VR_GLASS, false, PageIdConstants.PLAY_VR);

                break;
            case MDVRLibrary.DISPLAY_MODE_GLASS://眼镜模式
                ((VRLiveLandMediaController) mVideoController).setGlassesImageSelected(true);
                supportGyro.isGlassesMode(true);
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_VR_GLASS, true, PageIdConstants.PLAY_VR);
                if (mVRLibrary.getInteractiveMode() == MDVRLibrary.INTERACTIVE_MODE_TOUCH) {//开启眼镜模式，如果是touch模式则切换到陀螺仪模式
                    mVRLibrary.switchInteractiveMode(VRBaseActivity.this);
                    ((VRLiveLandMediaController) mVideoController).setMotionImageSelected(true);//更改图标选中状态
                    supportGyro.isGyroMode(true);
                    ToastUtils.getInstance().showShortToast(R.string.start_glass_with_gyro);
                }
                break;
        }
    }

    /**
     * 重置播放模式，如果是眼镜模式改为正常模式
     */
    protected void resetDisplayMode() {
        if (mVRLibrary.getDisplayMode() == MDVRLibrary.DISPLAY_MODE_GLASS) {
            ((VRLiveLandMediaController) mVideoController).setGlassesImageSelected(false);
            supportGyro.isGlassesMode(false);
            mVRLibrary.switchDisplayMode(VRBaseActivity.this);
        }
    }

    private void continueMobilePlay() {
        IfengApplication.mobileNetCanPlay = true;
        updateMobileNetLayer(false);
        if (NetUtils.isNetAvailable(this)) {
            if (mVideoView.isPauseState()) {
                mVideoView.start();
            } else {
                if (isSupportVRLive) {
                    prepareToPlay();
                }
            }
        } else {
            updateNoNetLayer(true);
        }
    }

    @Override
    public void onHidden() {
        topTitleLayout.setVisibility(View.GONE);
        playPauseImg.setVisibility(View.GONE);
    }

    @Override
    public void onShown() {
        tvTopTitle.setText(vrTitle);
        timeView.setText(DATE_FORMAT.format(new Date()));
        topTitleLayout.setVisibility(View.VISIBLE);
        playPauseImg.setVisibility(View.VISIBLE);
    }

    private PlayState currState;

    @Override
    public PlayState getCurrState() {
        return currState;
    }

    private void showController() {
        if (mVideoController != null) {
            mVideoController.show();
        }
    }

    public void hideController() {
        if (mVideoController != null) {
            mVideoController.hide();
        }
    }

    /**
     * 加载页面
     */
    protected void updateLoadingLayer(boolean show) {
        setControllerShowHide(!show);
        if (show) {
            hideTopAndControllerView();
            mobileNetShowing = false;
            stateParentLayout.removeAllViews();
            if (loadingLayer.getVisibility() != View.VISIBLE) {
                loadingLayer.setVisibility(View.VISIBLE);
            }
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (loadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.VISIBLE);
            stateParentLayout.addView(loadingLayer, loadingLp);
            if (currentModel != null) {
                loadingTitle.setText(vrTitle);
            }
            loadingTitle.setVisibility(View.VISIBLE);
            stateParentLayout.setVisibility(View.VISIBLE);
            mHandler.removeMessages(MESSAGE_ERROR);
            mHandler.sendEmptyMessageDelayed(MESSAGE_ERROR, TIMEOUT_MAX);
        } else {
            mHandler.removeMessages(MESSAGE_ERROR);
            loadingLayer.setVisibility(View.GONE);
            stateParentLayout.removeAllViews();
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            stateParentLayout.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 缓冲界面
     */
    private void updateBufferLayer(boolean show) {
        stateParentLayout.setVisibility(View.VISIBLE);
        if (show) {
            if (loadingLayer.getVisibility() != View.VISIBLE) {
                stateParentLayout.removeView(bufferingLayer);
                bufferingLayer.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);//居中
                bufferingLayer.setGravity(Gravity.CENTER_HORIZONTAL);
                stateParentLayout.addView(bufferingLayer, loadingLp);
            }
        } else {
            stateParentLayout.removeView(bufferingLayer);
            bufferingLayer.setVisibility(View.GONE);
        }
    }

    /**
     * 有网加载失败页面是否正在显示
     */
    private boolean errorRetryLayerIsShowing = false;

    /**
     * 在有网情况下展示的error界面
     *
     * @param show
     */
    protected void updateErrorLayer(boolean show) {
        errorRetryLayerIsShowing = show;
        setControllerShowHide(!show);
        if (show) {
            hideTopAndControllerView();
            mobileNetShowing = false;
            stateParentLayout.removeAllViews();
            if (loadingLayer.getVisibility() != View.VISIBLE) {
                loadingLayer.setVisibility(View.VISIBLE);
            }
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (loadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.INVISIBLE);
            loadingTitle.setVisibility(View.GONE);
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            stateParentLayout.addView(errorLayer, errorLayerLp);
            errorRetryLayer.setVisibility(View.VISIBLE);
            errorRetryBottomTv.setVisibility(View.VISIBLE);
            stateParentLayout.setVisibility(View.VISIBLE);
        } else {
            loadingLayer.setVisibility(View.GONE);
            stateParentLayout.removeAllViews();
            stateParentLayout.setVisibility(View.GONE);
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            errorRetryLayer.setVisibility(View.GONE);
            errorRetryBottomTv.setVisibility(View.GONE);
        }
    }

    /**
     * 在无网情况下展示的error界面
     *
     * @param show
     */
    protected void updateNoNetLayer(boolean show) {
        setControllerShowHide(!show);
        if (show) {
            hideTopAndControllerView();
            mobileNetShowing = false;
            stateParentLayout.removeAllViews();
            if (loadingLayer.getVisibility() != View.VISIBLE) {
                loadingLayer.setVisibility(View.VISIBLE);
            }
            loadingLayer.findViewById(R.id.video_loading_imageview).setVisibility(View.GONE);
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (loadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.INVISIBLE);
            stateParentLayout.addView(loadingLayer, loadingLp);
            loadingTitle.setVisibility(View.GONE);

            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            stateParentLayout.addView(noNetLayer, errorLayerLp);
            noNetLayer.findViewById(R.id.video_error_pause_lay).setVisibility(View.VISIBLE);
            stateParentLayout.setVisibility(View.VISIBLE);
        } else {
            stateParentLayout.removeAllViews();
            stateParentLayout.setVisibility(View.GONE);
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            errorLayer.findViewById(R.id.video_error_retry_bottom_tv).setVisibility(View.GONE);
        }
    }

    /**
     * 在运营商网络下提示页面
     *
     * @param show
     */
    protected void updateMobileNetLayer(boolean show) {
        mobileNetShowing = show;
        if (mVideoView != null) {//bug #10026：多次后台唤醒播放页停留在运营商界面，但是概率出现有时声音在播
            mVideoView.isNeedTodPlay(!show);
        }
        setControllerShowHide(!show);
        if (show) {
            hideTopAndControllerView();
            stateParentLayout.removeAllViews();
            if (loadingLayer.getVisibility() != View.VISIBLE) {
                loadingLayer.setVisibility(View.VISIBLE);
            }
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (loadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.INVISIBLE);
            stateParentLayout.addView(loadingLayer, loadingLp);
            loadingTitle.setVisibility(View.GONE);

            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_video_loading_all_bg));
            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            stateParentLayout.addView(mobileNetLayer, errorLayerLp);
            mobileNetLayer.findViewById(R.id.video_mobile_net_layout).setVisibility(View.VISIBLE);
            stateParentLayout.setVisibility(View.VISIBLE);
        } else {
            stateParentLayout.removeAllViews();
            stateParentLayout.setVisibility(View.GONE);
            stateParentLayout.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            mobileNetLayer.findViewById(R.id.video_mobile_net_layout).setVisibility(View.GONE);
        }
    }

    /**
     * 点击界面时控制栏是否显示
     *
     * @param isShow true：显示
     */
    private void setControllerShowHide(boolean isShow) {
        backImg.setVisibility(isShow ? View.GONE : View.VISIBLE);
        if (mVideoView != null) {
            mVideoView.setControllerVisibily(isShow);
        }
    }

    private void unRegistMobileAlertReceiver() {
        if (connectivityReceiver != null) unregisterReceiver(connectivityReceiver);
    }

    private void registerMobileAlertReceiver() {
        if (connectivityReceiver == null) {
            connectivityReceiver = new ConnectivityReceiver(this);
        }
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private boolean isKeyDown = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isKeyDown = true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!isActive || !isKeyDown) {
            return true;
        }
        isKeyDown = false;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mVideoController != null) {
            if (mVideoView.isInPlaybackState() && !mobileNetShowing && mVRLibrary.handleTouchEvent(ev)) return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            if (mVideoController != null && mVideoController.isShowing()) {
                mVideoController.continueShow();
            }
        } else {
            if (mVideoController != null) {
                mVideoController.rmMsgFadeOut();
            }
        }
    }

    /**
     * 重试
     */
    private void handleRetryBtn() {
        if (!NetUtils.isNetAvailable(this)) {
            updateNoNetLayer(true);
            return;
        }
        if (currentModel == null) {
            getPlayData();
        } else {
            prepareToPlay();
        }
    }

    /**
     * ERROR界面处理
     **/
    private void handleErrorState() {
        if (NetUtils.isNetAvailable(this)) {
            updateErrorLayer(true);
        } else {
            updateNoNetLayer(true);
        }
    }

    /**
     * 上报统计
     */
    protected void insertCustomerStatistics(PlayState playState) {
        if (getIntent().getStringExtra(IntentKey.VR_LIVE_ECHID) != null) {
            vrEchid = getIntent().getStringExtra(IntentKey.VR_LIVE_ECHID);
        }
        vrEchid = !TextUtils.isEmpty(vrEchid) ? vrEchid : "";
        switch (playState) {
            case STATE_PREPARING:
                initVRecord();
                break;
            case STATE_PREPARED:
                break;
            case STATE_PAUSED:
                if (mVRecord != null) {
                    mVRecord.stopPlayTime();
                }
                break;
            case STATE_PLAYING:
                if (mVRecord != null) {
                    mVRecord.setSuccessPlayFirstFrame(true);
                    if (mVRecord instanceof LiveRecord) {//直播
                        ((LiveRecord) mVRecord).stopPrepareTime();
                        mVRecord.startPlayTime();
                    } else if (mVRecord instanceof VodRecord) {//点播
                        ((VodRecord) mVRecord).stopPrepareTime();
                        mVRecord.startPlayTime();
                    }
                }
                break;
            case STATE_BUFFERING_START:
                if (mVRecord != null) {
                    mHandler.removeMessages(MESSAGE_KARTUN);
                    mHandler.sendEmptyMessageDelayed(MESSAGE_KARTUN, DELAY_TIME);
                    mVRecord.stopPlayTime();
                }
                break;
            case STATE_BUFFERING_END:
                if (mVRecord != null) {
                    mHandler.removeMessages(MESSAGE_KARTUN);
                    mVRecord.startPlayTime();
                }
                break;
            case STATE_PLAYBACK_COMPLETED:
                if (mVRecord != null) {
                    mVRecord.stopPlayTime();
                    if (mVRecord instanceof LiveRecord) {//直播
                        ((LiveRecord) mVRecord).setLiveTitle(vrTitle);
                        ((LiveRecord) mVRecord).setAudio(false);
                        CustomerStatistics.sendLiveRecord(((LiveRecord) mVRecord));
                    } else if (mVRecord instanceof VodRecord) {
                        if (!isChangeStream) {
                            CustomerStatistics.sendVODRecord((VodRecord) mVRecord);
                        }
                        isChangeStream = false;
                    }
                }
                break;
            case STATE_IDLE:
                if (mVRecord != null) {
                    mVRecord.stopPlayTime();
                    mHandler.removeMessages(MESSAGE_KARTUN);
                    if (mVRecord instanceof LiveRecord) {//直播
                        ((LiveRecord) mVRecord).setAudio(false);
                        ((LiveRecord) mVRecord).setLiveTitle(vrTitle);
                        ((LiveRecord) mVRecord).stopPrepareTime();
                        CustomerStatistics.sendLiveRecord(((LiveRecord) mVRecord));
                    } else if (mVRecord instanceof VodRecord) {
                        ((VodRecord) mVRecord).stopPrepareTime();
                        ((VodRecord) mVRecord).setAudio(false);
                        if (!isChangeStream) {
                            CustomerStatistics.sendVODRecord((VodRecord) mVRecord);
                        }
                        isChangeStream = false;
                    }
                }
                break;
            case STATE_ERROR:
                if (mVRecord != null) {
                    if (mVRecord instanceof LiveRecord) {//直播
                        ((LiveRecord) mVRecord).setAudio(false);
                        ((LiveRecord) mVRecord).setLiveTitle(vrTitle);
                        if (mVRecord.isSuccessPlayFirstFrame()) {
                            ((LiveRecord) mVRecord).setErr(true);
                        }
                        mVRecord.stopPlayTime();
                        ((LiveRecord) mVRecord).stopPrepareTime();
                        CustomerStatistics.sendLiveRecord(((LiveRecord) mVRecord));
                    } else if (mVRecord instanceof VodRecord) {
                        ((VodRecord) mVRecord).stopPrepareTime();
                        mVRecord.stopPlayTime();
                        ((VodRecord) mVRecord).setAudio(false);
                        if (!isChangeStream) {
                            CustomerStatistics.sendVODRecord((VodRecord) mVRecord);
                        }
                        isChangeStream = false;
                    }
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();
        switch (vId) {
            case R.id.video_detail_landscape_top_back_btn_click://顶部布局返回键
            case R.id.media_controller_back://各种页面顶部返回键
                onBackPressed();
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_BACK, null, PageIdConstants.PLAY_VR);
                break;
            case R.id.pauseToResume://没有网络：点击重试
            case R.id.video_error_retry_img://有网加载失败：点击重试，重新获取数据
                handleRetryBtn();
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_RETRY, null, PageIdConstants.PLAY_VR);
                break;
            case R.id.video_mobile_continue://运营商网络继续播放
                continueMobilePlay();
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_CONTINUE, null, PageIdConstants.PLAY_VR);
                break;
            case R.id.btn_play_pause:
                playPauseLogic();
                break;
            case R.id.icon_share:
                share();
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_SHARE, null, PageIdConstants.PLAY_VR);
                break;
            case R.id.video_land_right_stream:
                break;
        }
    }

    protected boolean isPlayComplete = false;//该条视频是否播放完成

    public static final String CURRENT_POSITION_IN_VIDEO_LIST = "currentPositionInVideoList";

    @Override
    public void onBackPressed() {
        saveHistory(isPlayComplete);//退出时保存下播放记录
        Intent intent = getIntent();
        intent.putExtra(CURRENT_POSITION_IN_VIDEO_LIST, currentPositionInVideoList);
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * 社会化分享
     */
    private void share() {
        initShareData();
        OneKeyShareContainer.oneKeyShare = onekeyShare;
        if (getVideoType() == TYPE_LIVE) {
            onekeyShare.shareVRLive(vrShareUrl, vrTitle, vrImageUrl, null);
        } else {
            onekeyShare.shareVRVideo(vrShareUrl, vrTitle, vrImageUrl, null);
        }
        onekeyShare.getADMorePopWindow().hideCopyBrowserRefreshView();
        onekeyShare.getADMorePopWindow().shareIconClickable(!TextUtils.isEmpty(vrShareUrl));//如果分享地址为空的话，分享图标点击无响应
        onekeyShare.showOrDismissADPopupWindow(iconShare);
    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageVrPlay("", "");
        } else {
            PageActionTracker.enterPage();
        }
    }


    /**
     * 播放暂停逻辑
     */
    private void playPauseLogic() {
        if (mVideoController.isShowing()) {
            mVideoController.show();
        }
        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, !mVideoView.isPlaying(), PageIdConstants.PLAY_VR);
        if (mVideoView.isPlaying()) {
            mVideoView.pause();
            playPauseImg.setImageResource(R.drawable.btn_play);
        } else {
            mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
            mVideoView.start();
            playPauseImg.setImageResource(R.drawable.btn_pause);
        }
    }

    protected ISupportGyro supportGyro;

    /**
     * 该接口用来判断设备是否具有陀螺仪功能
     */
    public interface ISupportGyro {
        /**
         * 是否具有陀螺仪功能
         */
        void supportGyro(boolean isSupport);

        /**
         * 判断交互模式是Gyro还是Touch模式
         */
        void isGyroMode(boolean isGyroMode);

        /**
         * 是否是双眼模式
         */
        void isGlassesMode(boolean isGlassesMode);
    }

    public void setSupportGyro(ISupportGyro supportGyro) {
        this.supportGyro = supportGyro;
    }

    protected Handler mHandler = new WeakHandler(VRBaseActivity.this);

    static class WeakHandler extends Handler {

        WeakReference<VRBaseActivity> weakReference;

        public WeakHandler(VRBaseActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            VRBaseActivity activity = weakReference.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case MESSAGE_ERROR:
                    if (activity.mVideoView != null) {
                        activity.mVideoView.stopPlayback();
                        activity.mVideoView.notifyStateChange(PlayState.STATE_ERROR);
                    }
                    break;
                case MESSAGE_KARTUN:
                    if (activity.mVRecord != null) {
                        activity.mVRecord.statisticBN();
                    }
                    break;
            }
        }
    }
}
