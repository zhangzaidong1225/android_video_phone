package com.ifeng.newvideo.ui.live.weblive;


import android.webkit.JavascriptInterface;

import java.io.Serializable;

public class UserInfo implements Serializable {


    private String username;
    private String nickname;
    private String userImage;
    private String uid;
    private String guid;
    private String token;
    private String os;
    private String deviceId;
    private String userPhoneNum;

    @JavascriptInterface
    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    @JavascriptInterface
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @JavascriptInterface
    public String getUserPhoneNum() {
        return userPhoneNum;
    }

    public void setUserPhoneNum(String userPhoneNum) {
        this.userPhoneNum = userPhoneNum;
    }

    @JavascriptInterface
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JavascriptInterface
    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    @JavascriptInterface
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @JavascriptInterface
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JavascriptInterface
    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    @JavascriptInterface
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "username='" + username + '\'' +
                ", nickname='" + nickname + '\'' +
                ", userImage='" + userImage + '\'' +
                ", uid='" + uid + '\'' +
                ", token='" + token + '\'' +
                ", os='" + os + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", userPhoneNum='" + userPhoneNum + '\'' +
                '}';
    }
}
