package com.ifeng.newvideo.ui.live.listener;


import android.view.View;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.ui.live.ActivityLive;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.constants.MediaConstants;

/**
 * 直播页面点击事件监听
 * Created by Vincent on 2014/11/25.
 */
public class LivePlayClickListener implements View.OnClickListener {

    public ActivityLive activityLivePlayer;

    public LivePlayClickListener(ActivityLive activityLive) {
        this.activityLivePlayer = activityLive;
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();
        if (activityLivePlayer == null) {
            return;
        }
        switch (vId) {
            case R.id.video_land_left_clock:
                activityLivePlayer.clockScreen();
                break;
            case R.id.video_land_right_stream_auto:
                activityLivePlayer.onRightStreamItemClick(MediaConstants.STREAM_AUTO);
                break;
            case R.id.video_land_right_stream_original:
                activityLivePlayer.onRightStreamItemClick(MediaConstants.STREAM_ORIGINAL);
                break;
            case R.id.video_land_right_stream_supper:
                activityLivePlayer.onRightStreamItemClick(MediaConstants.STREAM_SUPPER);
                break;
            case R.id.video_land_right_stream_high:
                activityLivePlayer.onRightStreamItemClick(MediaConstants.STREAM_HIGH);
                break;
            case R.id.video_land_right_stream_mid:
                activityLivePlayer.onRightStreamItemClick(MediaConstants.STREAM_MID);
                break;
            case R.id.video_land_right_stream_low:
                activityLivePlayer.onRightStreamItemClick(MediaConstants.STREAM_LOW);
                break;
            case R.id.share_iv_sina:
                Platform sinaPlatform = ShareSDK.getPlatform(SinaWeibo.NAME);
                shareToPlatform(sinaPlatform, true);
                break;
            case R.id.share_iv_wechat:
                Platform wechatPlatform = ShareSDK.getPlatform(Wechat.NAME);
                shareToPlatform(wechatPlatform, false);
                break;
            case R.id.share_iv_qq:
                Platform qqPlatform = ShareSDK.getPlatform(QQ.NAME);
                shareToPlatform(qqPlatform, false);
                break;
            case R.id.share_iv_wechat_moment:
                Platform wechatMomentPlatform = ShareSDK.getPlatform(WechatMoments.NAME);
                shareToPlatform(wechatMomentPlatform, false);
                break;
            case R.id.share_iv_qzone:
                Platform qzonePlatform = ShareSDK.getPlatform(QZone.NAME);
                shareToPlatform(qzonePlatform, false);
                break;
            case R.id.share_iv_alipay:
                Platform alipayPlatform = ShareSDK.getPlatform(Alipay.NAME);
                shareToPlatform(alipayPlatform, false);
                break;


            case R.id.video_gesture_guide://手势指导层
                activityLivePlayer.updateGestureGuideLayer(false);
                break;

            case R.id.video_detail_landscape_top_back_btn_click://顶部返回键
                handleTitleBack();
                break;
            case R.id.media_controller_back://竖屏返回按钮
                handleBottomBack();
                break;
            case R.id.btn_volume://播放器静音按钮
                handleBtnVolume();
                break;
            case R.id.pauseToResume://播放器播放按钮
            case R.id.video_error_retry_img://播放器刷新按钮
                handleRetryBtn(vId);
                break;
//            case R.id.video_av_switch_img://播放器切换按钮
//                handleAvSwitchBtn();
//                break;
//            case R.id.bottom_share_iv://竖屏分享按钮
            case R.id.bottom_share_rl://竖屏分享按钮
                handleBottomShareBtn(v);
                break;

            case R.id.media_controller_audio:
                handleAvSwitchBtn();
                break;
            case R.id.video_mobile_continue:
                activityLivePlayer.continueMobilePlay();
                break;
        }
    }


    private void handleTitleBack() {
        activityLivePlayer.isClocked = false;
        activityLivePlayer.switchOrientation();
//                CustomerStatistics.OnclickBtn(StatisticsConstants.BtnName.B_BACK, "", getPageName());
        activityLivePlayer.hideController();
    }

    private void handleBottomBack() {
        if (activityLivePlayer.isLandScape()) {
            activityLivePlayer.switchOrientation();
            return;
        }
        activityLivePlayer.mVideoView.stopPlayback();
        activityLivePlayer.finish();
    }

    private void handleBtnVolume() {
        activityLivePlayer.silentBtnClick();
    }

    private void handleAvSwitchBtn() {
        if (!NetUtils.isNetAvailable(activityLivePlayer)) {
            //ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            if (activityLivePlayer.mVideoView != null) {
                activityLivePlayer.mVideoView.stopPlayback();
            }
            activityLivePlayer.updateErrorPauseLayer(true);
        }
    }

    private void handleHorizontalShareBtn(View v) {
        activityLivePlayer.oneShareHorizontal(v);
    }

    private void handleBottomShareBtn(View v) {
        if (!NetUtils.isNetAvailable(activityLivePlayer)) {
            return;
        }
        if (!activityLivePlayer.canClickBottomBtn) {
            return;
        }
        activityLivePlayer.oneShare(v);
    }

    private void handleRetryBtn(int vId) {
        if (!NetUtils.isNetAvailable(activityLivePlayer)) {
            //ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (vId == R.id.video_error_retry_img) {
            if (activityLivePlayer.mCurrentStream == MediaConstants.STREAM_AUTO) {
                //当加载失败重新加载时，为跳过预留码流，超清、原画
                activityLivePlayer.mCurrentStream -= 3;
            } else {
                activityLivePlayer.mCurrentStream--;
            }
            if (activityLivePlayer.mCurrentStream == ActivityLive.INVALID_URL) {
                activityLivePlayer.mCurrentStream = MediaConstants.STREAM_HIGH;
            }
        }
        activityLivePlayer.refreshFragmentData();
    }

    private void shareToPlatform(Platform platform, boolean isShowEditPage) {
        showController();
        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare.shareToPlatform(platform, isShowEditPage);
            if (!isShowEditPage && !(platform.getName()).equals("QQ")) {
                OneKeyShareContainer.oneKeyShare = null;
            }
        }
    }

    private void showController() {
        if (activityLivePlayer.mVideoController != null) {
            activityLivePlayer.mVideoController.show();
        }
    }
}
