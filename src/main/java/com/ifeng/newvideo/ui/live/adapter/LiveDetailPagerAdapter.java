package com.ifeng.newvideo.ui.live.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.ifeng.newvideo.ui.live.FragmentLiveProgramList;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.video.core.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;


/**
 * 直播播放基页下方viewPager Adapter
 * import java.util.List;
 * Created by Vincent on 2014/11/18.
 */
public class LiveDetailPagerAdapter extends FragmentStatePagerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LiveDetailPagerAdapter.class);

    private List<FragmentLiveProgramList> data;

    private FragmentLiveProgramList todayFragment;

    public LiveDetailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setData(List<FragmentLiveProgramList> data) {
        this.data = data;

    }

    @Override
    public Fragment getItem(int position) {
        return data.get(position);
    }

    public Fragment getTodayItem(){
        return todayFragment;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        FragmentLiveProgramList fragment = data.get(position);
        if (fragment.getDay() != 0 && fragment.getDay() != 1) {
            Date date = DateUtils.getDateFormat(LiveUtils.getCurrentTime());
            return DateUtils.getWeek(date, fragment.getDay());
        }
        todayFragment = fragment;
        return "今天";
    }


}
