package com.ifeng.newvideo.ui.live.listener;

import android.annotation.SuppressLint;
import android.graphics.PointF;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.live.ActivityLive;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.player.IfengVideoView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 直播页面onTouch监听
 * Created by Vincent on 2014/11/25.
 */
public class LiveDetailPlayerTouchListener {

    private static final Logger logger = LoggerFactory.getLogger(LiveDetailPlayerTouchListener.class);

    private final ActivityLive mActivity;
    private PointF startPoint;
    private float startDistance;
    /**
     * 用来区分是单指双指 *
     */
    private int mode;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    /**
     * 用来区分是音量，亮度还是进度 **
     */
    private int drag_type;
    private static final int VOLUME = 3;
    private static final int BRIGHTNESS = 4;
    private static final int PROGRESS = 5;
    private boolean twoFinger;
    private long msec = -1;

    public LiveDetailPlayerTouchListener(ActivityLive activity) {
        this.mActivity = activity;
        GestureDetector.OnGestureListener onGestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                scaleVideoView();
                return super.onDoubleTap(e);
            }
        };
        gestureDetector = new GestureDetector(activity, onGestureListener);
    }

    /**
     * 滑动改变声音大小
     */
    private void onVolumeSlide(float percent) {
        if (mActivity.mVolume == -1) {
            mActivity.mVolume = mActivity.mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (mActivity.mVolume < 0) {
                mActivity.mVolume = 0;
                mActivity.mOperationBg.setImageResource(R.drawable.common_gesture_volume_mute);
            } else {
                mActivity.mOperationBg.setImageResource(R.drawable.common_gesture_volume_nomal);
            }
        }
        mActivity.mProgress_graph_fl.setVisibility(View.VISIBLE);
        mActivity.mProgress_text_ll.setVisibility(View.GONE);
        mActivity.mGestureRootView.setVisibility(View.VISIBLE);

        int tempVolume = (int) (percent * mActivity.mMaxVolume) + mActivity.mVolume;
        if (tempVolume > mActivity.mMaxVolume) {
            tempVolume = mActivity.mMaxVolume;
        } else if (tempVolume < 0) {
            tempVolume = 0;
        }
        if (tempVolume > 0) {
            mActivity.mOperationBg.setImageResource(R.drawable.common_gesture_volume_nomal);
        } else {
            mActivity.mOperationBg.setImageResource(R.drawable.common_gesture_volume_mute);
        }
        // 变更声音
        mActivity.mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, tempVolume, 0);
        mActivity.mCurrentVolume = tempVolume;
        mActivity.updateVolume(tempVolume);
        // 变更进度条
        ViewGroup.LayoutParams lp = mActivity.mOperationPercent.getLayoutParams();
        lp.width = mActivity.findViewById(R.id.operation_full).getLayoutParams().width * tempVolume / mActivity.mMaxVolume;
        mActivity.mOperationPercent.setLayoutParams(lp);
    }

    /**
     * 滑动改变亮度
     */
    private void onBrightnessSlide(float percent) {
        if (mActivity.mBrightness < 0) {
            mActivity.mBrightness = mActivity.getWindow().getAttributes().screenBrightness;
            if (mActivity.mBrightness <= 0.00f) mActivity.mBrightness = 0.50f;
            if (mActivity.mBrightness < 0.01f) mActivity.mBrightness = 0.00f;
        }
        int brightnessInt = 0;
        try {
            brightnessInt = Settings.System.getInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            logger.error("Settings.SettingNotFoundException {}", e);
        }
        mActivity.mBrightness = ((float) brightnessInt) / 255.0f;
        if (mActivity.mBrightness > 1.0f) {
            mActivity.mBrightness = 1.0f;
        } else if (mActivity.mBrightness < 0.01f) {
            mActivity.mBrightness = 0.00f;
        }
        mActivity.mOperationBg.setImageResource(R.drawable.common_gesture_brightness);
        mActivity.mProgress_graph_fl.setVisibility(View.VISIBLE);
        mActivity.mProgress_text_ll.setVisibility(View.GONE);
        mActivity.mGestureRootView.setVisibility(View.VISIBLE);
        WindowManager.LayoutParams lpa = mActivity.getWindow().getAttributes();
        lpa.screenBrightness = mActivity.mBrightness + (percent - mActivity.proBrightPercent);
        if (lpa.screenBrightness > 1.0f)
            lpa.screenBrightness = 1.0f;
        else if (lpa.screenBrightness < 0.01f) lpa.screenBrightness = 0.00f;
        mActivity.getWindow().setAttributes(lpa);
        ViewGroup.LayoutParams lp = mActivity.mOperationPercent.getLayoutParams();
        lp.width = (int) (mActivity.findViewById(R.id.operation_full).getLayoutParams().width * lpa.screenBrightness);
        mActivity.mOperationPercent.setLayoutParams(lp);
        float screenBrightness = lpa.screenBrightness;
        int brightness = (int) (255 * screenBrightness);
        if (brightness < 0) {
            brightness = 0;
        } else if (brightness > 255) {
            brightness = 255;
        }
        Settings.System.putInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);
        mActivity.proBrightPercent = percent;
    }

    private final GestureDetector gestureDetector;

    public boolean onTouchEvent(MotionEvent event) {

        boolean isTouchList = event.getX() >= (DisplayUtils.getWindowWidth() - DisplayUtils.convertDipToPixel(230));
        if (mActivity.isSelectedCheck()) {
            if (isTouchList) {
                return false;
            }
        }
        boolean isControllerShow = mActivity.mVideoController != null && mActivity.mVideoController.isShowing();
        if (isControllerShow && mActivity.isLandScape()
                && event.getX() >= (DisplayUtils.getWindowWidth() - DisplayUtils.convertDipToPixel(50))) {
            return false;
        }
        int windowWidth = DisplayUtils.getWindowHeight();
        int windowHeight = DisplayUtils.getWindowWidth();
        if (!mActivity.isLandScape()) {
            windowWidth = DisplayUtils.getWindowWidth();
            windowHeight = windowWidth * 9 / 16;
            if (event.getY() > windowHeight) {
                endGesture();
                return false;
            }
        }

        gestureDetector.onTouchEvent(event);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mode = DRAG;
                twoFinger = false;
                drag_type = 0;
                startPoint = new PointF(event.getX(), event.getY());
                msec = -1;
                endGesture();
                mActivity.proBrightPercent = 0;
                if (startPoint.x < windowHeight / 3.0) {
                    int brightnessInt = 0;
                    try {
                        brightnessInt = Settings.System.getInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
                    } catch (Settings.SettingNotFoundException e) {
                        logger.error("Settings.SettingNotFoundException {}", e);
                    }
                    mActivity.mBrightness = ((float) brightnessInt) / 255.0f;
                }
                break;

            case MotionEvent.ACTION_MOVE:
                switch (mode) {
                    case DRAG:
                        float dx = event.getX() - startPoint.x;
                        float dy = event.getY() - startPoint.y;
                        if (drag_type == 0) { // 只有未确定滑动类型时，才执行下面判断类型逻辑
                            if (mActivity.isLandScape()) {
                                if (startPoint.x > windowHeight / 2.0 && Math.abs(dy) > 10 && Math.abs(dy) >= Math.abs(dx)) {// 屏幕右半边滑动
                                    drag_type = VOLUME;
                                } else if (startPoint.x < windowHeight / 2.0 && Math.abs(dy) > 10 && Math.abs(dy) >= Math.abs(dx)) {// 屏幕左变边滑动
                                    drag_type = BRIGHTNESS;
                                } else if (Math.abs(dx) > Math.abs(dy) && Math.abs(dx) > 15) { // 横向滑动的距离，全屏的比竖屏的大点
                                    drag_type = PROGRESS;
                                }
                            } else {
                                if (startPoint.x > windowWidth / 2.0 && Math.abs(dy) > 10 && Math.abs(dy) >= Math.abs(dx)) {// 屏幕右半边滑动
                                    drag_type = VOLUME;
                                } else if (startPoint.x < windowWidth / 2.0 && Math.abs(dy) > 10 && Math.abs(dy) >= Math.abs(dx)) {// 屏幕左变边滑动
                                    drag_type = BRIGHTNESS;
                                } else if (Math.abs(dx) > Math.abs(dy) && Math.abs(dx) > 15) { // 横向滑动的距离
                                    drag_type = PROGRESS;
                                }
                            }
                        }
                        if (drag_type == VOLUME) {
                            float percent = mActivity.isLandScape() ? -dy * 1.1f / windowWidth : -dy * 1.1f / windowHeight;
                            onVolumeSlide(percent); // 屏幕高度代表100%
                            break;
                        } else if (drag_type == BRIGHTNESS) {
                            float percent = mActivity.isLandScape() ? -dy * 1.1f / windowWidth : -dy * 1.1f / windowHeight;
                            onBrightnessSlide(percent);
                            break;
                        } else if (drag_type == PROGRESS) {
//                            float percent = mActivity.isLandScape() ? dx *1.1f  / windowWidth : dx *1.1f  / windowHeight;
//                            msec = onVideoFastProgress(percent);
                            break;
                        }
                        break;

                    case ZOOM:
                        if (twoFinger) {
                            float endDistance = distance(event);
                            if (endDistance - startDistance > 15) {
                                if (mActivity.isLandScape()) {
                                    mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_ZOOM;
                                } else {
                                    mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_ZOOM;
                                }
                                if (mActivity.mVideoView != null) {
                                    mActivity.mVideoView.setVideoLayout(mActivity.mLayout);
                                }
                            } else if (endDistance - startDistance < -15) {
                                if (mActivity.isLandScape()) {
                                    mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_FULL;
                                } else {
                                    mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_PORTRAIT;
                                }
                                if (mActivity.mVideoView != null) {
                                    mActivity.mVideoView.setVideoLayout(mActivity.mLayout);
                                }
                            }
                        }
                        break;
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:// 已经有一个手指按住屏幕，再有一个手指按下屏幕就会触发该事件
                mode = ZOOM;
                startDistance = distance(event);
                if (startDistance > 10) {// 一根手指最小的距离，防止一根手指出现老茧错认为两根
                    twoFinger = true;
                }
                endGesture();
                break;
            case MotionEvent.ACTION_POINTER_UP:// 有一个手指离开屏幕，但还有手指在屏幕就会触发该事件
            case MotionEvent.ACTION_UP:// 最后一个手指离开屏幕，就会触发该事件
                mode = 0;
                twoFinger = false;
                if (msec >= 0) {
                    mActivity.mVideoView.seekTo((int) msec);
                }
                msec = -1;
                endGesture();
                break;
        }
        return false;
    }

    private void scaleVideoView() {
        //TODO touch事件的大小缩放
            /*
             * if (mCurVideoLayout == IfengVideoView.VIDEO_LAYOUT_ZOOM) mCurVideoLayout = IfengVideoView.VIDEO_LAYOUT_ZOOM; else mCurVideoLayout++;
			 */
        if (mActivity.isLandScape()) {
            if (mActivity.mLayout == IfengVideoView.VIDEO_LAYOUT_ZOOM) {
                mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_FULL;
            } else {
                mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_ZOOM;
            }
        } else {
            if (mActivity.mLayout == IfengVideoView.VIDEO_LAYOUT_PORTRAIT) {
                mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_ZOOM;
            } else {
                mActivity.mLayout = IfengVideoView.VIDEO_LAYOUT_PORTRAIT;
            }
        }
        if (mActivity.mVideoView != null) {
            mActivity.mVideoView.setVideoLayout(mActivity.mLayout);
        }
    }

    /**
     * 手势结束
     */
    private void endGesture() {
        mActivity.mVolume = -1;
        mActivity.mBrightness = -1f;
        mActivity.mVideoProgress = -1;
        // 隐藏
        gestureHandler.removeMessages(MESSAGE_GESTURE);
        gestureHandler.sendEmptyMessageDelayed(MESSAGE_GESTURE, 500);
    }

    private static final int MESSAGE_GESTURE = 911;
    /**
     * 定时隐藏
     */
    @SuppressLint("HandlerLeak")
    private final Handler gestureHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mActivity.mGestureRootView.setVisibility(View.GONE);
        }
    };

    /**
     * 计算两点之间的距离
     */
    private float distance(MotionEvent event) {
        if (event != null && event.getPointerCount() > 1) {
            float dx = event.getX(1) - event.getX(0);
            float dy = event.getY(1) - event.getY(0);
            return (float) Math.sqrt(dx * dx + dy * dy);
        } else {
            return 0;
        }
    }
}