package com.ifeng.newvideo.ui.live.listener;

import com.ifeng.video.dao.db.model.LiveInfoModel;

/**
 * 直播推荐 点击回调
 * Created by Vincent on 2014/11/28.
 */
public interface NotifyProgramChange {
    void notifyChange(LiveInfoModel model);
}
