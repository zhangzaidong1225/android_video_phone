package com.ifeng.newvideo.ui.live.listener;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.LiveRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.ui.live.ActivityLive;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import com.ifeng.video.player.ChoosePlayerUtils;
import com.ifeng.video.player.PlayState;
import com.ifeng.video.player.StateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Vincent on 2014/11/25.
 */
public class LivePlayStateChangeListener implements StateListener {

    private static final Logger logger = LoggerFactory.getLogger(LivePlayStateChangeListener.class);
    private static final int IFENG_TYPE = 1;
    private final ActivityLive activityVideoPlayer;
    private final static int DELAY_TIME = 2000;
    private final static int DELAY_STOP_TIME = 10000;
    private final static int BUFFER_CANSHOW_TIME = 2 * 1000;
    private boolean isSingleToast = true;
    private PlayState currState;
    private LiveRecord mVRecord;
    private boolean canShowBuffer = false;
    private final static int BUFFER_CANSHOUW_MSG = 10002;

    public LivePlayStateChangeListener(ActivityLive activityVideoPlayer) {
        this.activityVideoPlayer = activityVideoPlayer;
    }

    public final Handler mTimeoutHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            handleErroState();
        }
    };

    private final Handler mDelayHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (activityVideoPlayer.mVideoView != null) {
//                activityLivePlayer.getPlayerPosition();
                activityVideoPlayer.mVideoView.notifyStateChange(PlayState.STATE_ERROR);
                activityVideoPlayer.mVideoView.stopPlayback();
            }
        }
    };

    private final Handler mDelayStatisticHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mVRecord != null) {
                mVRecord.statisticBN();
            }
        }
    };

    @Override
    public PlayState getCurrState() {
        return currState;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onStateChange(PlayState state) {
        if (activityVideoPlayer == null) {
            logger.debug("onStateChange activityLivePlayer is null!!!");
            return;
        }
        logger.debug("onStateChange ######## {}", state);
        currState = state;
        //TODO 统计预留
        insertCustomerStatistics(state);
        switch (state) {
            case STATE_PREPARING:
                logger.debug("the live preparing ------->");
                canShowBuffer = false;
                mDelayHandler.removeMessages(0);
                mDelayHandler.sendEmptyMessageDelayed(0, DELAY_STOP_TIME);
                activityVideoPlayer.updateLoadingLayer(true);
                break;

            case STATE_PREPARED:
                break;

            case STATE_IDLE:
                mDelayHandler.removeMessages(0);
                mDelayStatisticHandler.removeMessages(0);
//                activityLivePlayer.updateBufferLayer(false);
                break;

            case STATE_PLAYING:
                mDelayHandler.removeMessages(0);
                handlePlaying();
                break;

            case STATE_PAUSED:

                break;

            case STATE_PLAYBACK_COMPLETED:
                activityVideoPlayer.isCompleted = true;
                activityVideoPlayer.playPosWhenSwitchAV = 0;
                if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
                    activityVideoPlayer.updateErrorPauseLayer(true);
                }
                break;
            case STATE_BUFFERING_START:
                handleBufferStart();
                break;

            case STATE_BUFFERING_END:
                handleBufferEnd();
                break;

            case STATE_ERROR:
                // 自动切换播放地址
                handleErroState();
                break;

            //TODO 此处预留留给直播回看用
            case STATE_PLAY_NEXT:
                activityVideoPlayer.playPosWhenSwitchAV = 0;
//                activityLivePlayer.autoSwitchNextVideo();
                break;

            case STATE_PLAY_PRE:
                activityVideoPlayer.playPosWhenSwitchAV = 0;
//                activityLivePlayer.autoSwitchPreVideo();
                break;
            default:
                break;
        }
    }

    private void handlePlaying() {
        sendShowBUfferMsg();
        if (activityVideoPlayer.mSharePreUtils.getLivePlayGestureState()) {
            activityVideoPlayer.updateGestureGuideLayer(true);
        }
        activityVideoPlayer.mVideoView.setControllerVisibily(true);
        activityVideoPlayer.isCompleted = false;
        activityVideoPlayer.isErroring = false;
        activityVideoPlayer.mVideoView.requestLayout();
        activityVideoPlayer.updateLoadingLayer(false);
        activityVideoPlayer.isClickStreamSelect = false;
        if (activityVideoPlayer.isShouldShow) {
            activityVideoPlayer.showController();
            activityVideoPlayer.isShouldShow = false;
        }
    }

    private void handleBufferEnd() {
        activityVideoPlayer.updateBufferLayer(false);
        mDelayStatisticHandler.removeMessages(0);
        mDelayHandler.removeMessages(0);
    }

    private void handleBufferStart() {
        if (!canShowBuffer) {
            return;
        }
        activityVideoPlayer.showStreamSwitchToast();
        activityVideoPlayer.updateBufferLayer(true);
        mDelayHandler.removeMessages(0);
        mDelayHandler.sendEmptyMessageDelayed(0, DELAY_STOP_TIME);
        mDelayStatisticHandler.removeMessages(0);
        mDelayStatisticHandler.sendEmptyMessageDelayed(0, DELAY_TIME);
    }

    private final Handler mBufferHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            canShowBuffer = true;
        }
    };

    private void sendShowBUfferMsg() {
        removeBufferHandMsg(BUFFER_CANSHOUW_MSG);
        Message msg = Message.obtain();
        msg.what = BUFFER_CANSHOUW_MSG;
        mBufferHandler.sendMessageDelayed(msg, BUFFER_CANSHOW_TIME);
    }

    private void removeBufferHandMsg(int msg) {
        mBufferHandler.removeMessages(msg);
    }

    private void handleErroState() {
        activityVideoPlayer.isErroring = true;
//        activityLivePlayer.currentPlayStream--;
//        if (activityLivePlayer.currentPlayStream == activityLivePlayer.INVALID_URL) {
//            activityLivePlayer.currentPlayStream = MediaConstants.STREAM_HIGH;
//            ToastUtils.showShortToast(activityLivePlayer,
//                    R.string.video_play_url_error);
//        } else {
//            activityLivePlayer.prepareToPlay();
//            return;
//        }
        // end
        activityVideoPlayer.isPlayerInit = false;
        if (NetUtils.isNetAvailable(activityVideoPlayer)) {
            activityVideoPlayer.updateErrorRetryLayer(true);
        } else {
            activityVideoPlayer.updateErrorPauseLayer(true);
//            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
    }

    public void insertCustomerStatistics(PlayState playState) {
        logger.debug("insertCustomerStatistics{}", playState.toString());
        switch (playState) {
            case STATE_PREPARING:
                initVRecord();
                break;
            case STATE_PREPARED:
                break;
            case STATE_PAUSED:
                if (mVRecord != null) {
                    mVRecord.stopPlayTime();
                }
                break;
            case STATE_PLAYING:
                if (mVRecord != null) {
                    mVRecord.setSuccessPlayFirstFrame(true);
                    mVRecord.startPlayTime();
                    mVRecord.stopPrepareTime();
                }
                break;
            case STATE_BUFFERING_START:
                if (mVRecord != null) {
                    mVRecord.stopPlayTime();
                }
                break;
            case STATE_BUFFERING_END:
                if (mVRecord != null) {
                    mVRecord.startPlayTime();
                }
                break;
            case STATE_PLAYBACK_COMPLETED:
                if (mVRecord != null) {
                    mVRecord.setLiveTitle(activityVideoPlayer.programTitle);
                    mVRecord.setAudio(false);
                    mVRecord.stopPlayTime();
                    CustomerStatistics.sendLiveRecord(mVRecord);
                    logger.debug("STATE_PLAYBACK_COMPLETED title======{}", activityVideoPlayer.programTitle);
                }
                break;
            case STATE_IDLE:
                if (mVRecord != null && !activityVideoPlayer.isClickStreamSelect) {
                    mVRecord.setAudio(false);
                    logger.debug("STATE_IDLE title======{}", activityVideoPlayer.programTitle);
                    mVRecord.setLiveTitle(activityVideoPlayer.programTitle);
                    mVRecord.stopPlayTime();
                    mVRecord.stopPrepareTime();
                    CustomerStatistics.sendLiveRecord(mVRecord);
                }
                break;
            case STATE_ERROR:
                if (mVRecord != null) {
                    mVRecord.setAudio(false);
                    if (mVRecord.isSuccessPlayFirstFrame()) {
                        mVRecord.setErr(true);
                    }
                    logger.debug("STATE_ERROR title======{}", activityVideoPlayer.programTitle);
                    mVRecord.setLiveTitle(activityVideoPlayer.programTitle);
                    mVRecord.stopPlayTime();
                    mVRecord.stopPrepareTime();
                    CustomerStatistics.sendLiveRecord(mVRecord);
                }
                break;
        }
    }

    private void initVRecord() {
        String ptype = VodRecord.P_TYPE_LV;
        String echId = null;
        String vId = null;
        if (activityVideoPlayer.getCurrProgram() != null) {
            echId = activityVideoPlayer.echid;
            echId = !TextUtils.isEmpty(echId) ? echId : "";
            LiveInfoModel model = activityVideoPlayer.getCurrProgram();
            if (model == null) {
                return;
            }
            String cName = model.getcName();
            String title = model.getTitle();
            if (model.getIsiFeng() == IFENG_TYPE) {
                vId = TextUtils.isEmpty(title) ? cName : title;
            } else {
                vId = TextUtils.isEmpty(cName) ? model.getNodeId() : cName;
            }
            mVRecord = new LiveRecord(vId, model.getProgramTitle(), "", ptype, cName,
                    mVRecord, echId, ChoosePlayerUtils.useIJKPlayer(activityVideoPlayer),"" , "", "", "");
        }
    }
}
