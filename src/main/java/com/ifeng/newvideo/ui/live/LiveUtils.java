package com.ifeng.newvideo.ui.live;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.video.core.net.RequestJson;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.model.LaunchAppModel;
import com.ifeng.video.dao.db.model.LiveChannelModels;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import com.ifeng.video.dao.db.model.live.TVLiveProgramInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 直播相关的工具类
 * Created by Pengcheng on 2015/11/27 027.
 */
public class LiveUtils {

    private static final Logger logger = LoggerFactory.getLogger(LiveUtils.class);

    /**
     * 直播推荐页去掉过期
     *
     * @param models
     * @param serverDateTime
     */
    public static List<LiveInfoModel> getLiveRecommendList(List<LiveInfoModel> models, Date serverDateTime) {
        if (models == null)
            return null;

        Date startDate, endDate;
        List<LiveInfoModel> data = new ArrayList<LiveInfoModel>();
        for (LiveInfoModel model : models) {
            try {
                startDate = DateUtils.hhmmssFormat_One.parse(model.getStartTime());
                endDate = DateUtils.hhmmssFormat_One.parse(model.getEndTime());
            } catch (ParseException e) {
                try {
                    startDate = DateUtils.hhmmssFormat.parse(model.getStartTime());
                    endDate = DateUtils.hhmmssFormat.parse(model.getEndTime());
                } catch (ParseException e1) {
                    continue;
                }
            }

            if (serverDateTime.before(startDate)) {
                model.setState(LiveInfoModel.BOOK);
            } else if (serverDateTime.after(endDate)) {
                continue;
            } else {
                model.setState(LiveInfoModel.LIVE);
                //playing
            }

            if (serverDateTime.after(endDate))
                continue;

            data.add(model);
        }
        return data;
    }

    /**
     * 拿到 最近的时间
     *
     * @param models
     * @param serverDateTime
     * @return
     */
    public static Date getRecentTime(List<LiveInfoModel> models, Date serverDateTime) {
        List<Date> dateList = getValidDateList(models, serverDateTime);
        Date tempDate = null;
        final int size = dateList.size();
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (dateList.get(j).before(dateList.get(i))) {
                    tempDate = dateList.get(i);
                    dateList.set(i, dateList.get(j));
                    dateList.set(j, tempDate);
                }
            }
        }
        return dateList.get(0);
    }

    /**
     * 直播时间过滤，过滤过期时间
     *
     * @param models
     * @param serverDateTime
     * @return
     */
    private static List<Date> getValidDateList(List<LiveInfoModel> models, Date serverDateTime) {
        List<Date> dateList = new ArrayList<Date>();
        Date startDate, endDate;
        for (LiveInfoModel model : models) {
            try {
                startDate = DateUtils.hhmmssFormat_One.parse(model.getStartTime());
                endDate = DateUtils.hhmmssFormat_One.parse(model.getEndTime());
            } catch (ParseException e) {
                try {
                    startDate = DateUtils.hhmmssFormat.parse(model.getStartTime());
                    endDate = DateUtils.hhmmssFormat.parse(model.getEndTime());
                } catch (ParseException e1) {
                    continue;
                }
            }

            if (serverDateTime.before(startDate)) {
                dateList.add(startDate);
            }
            if (serverDateTime.before(endDate)) {
                dateList.add(endDate);
            }
        }
        return dateList;
    }

    /**
     * 此处主要用于处理直播状态，并取得聚焦position
     *
     * @param liveInfoSchedule
     * @param serverDateTime
     * @return
     */
    public static int handleLiveSchedule(LiveChannelModels liveInfoSchedule, Date serverDateTime) {
        int position = 0;
        boolean isLookIngBack = false; //此处用于判断是否有在回看状态的，有则，直播中应该为Live,没有为Living
        if (liveInfoSchedule != null) {
            List<LiveChannelModels.LiveChannelModel> list = liveInfoSchedule.getSchedule();
            if (list != null) {
                Date startDate = null, endDate;
                for (int i = 0; i < list.size(); i++) {
                    LiveChannelModels.LiveChannelModel schedule = list.get(i);
                    try {
                        startDate = DateUtils.hhmmssFormat_One.parse(schedule.getStartTime());
                        endDate = DateUtils.hhmmssFormat_One.parse(schedule.getEndTime());
                    } catch (ParseException e) {
                        try {
                            startDate = DateUtils.hhmmssFormat.parse(schedule.getStartTime());
                            endDate = DateUtils.hhmmssFormat.parse(schedule.getEndTime());
                        } catch (ParseException e1) {
                            if (serverDateTime.before(startDate)) {
                                schedule.setState(LiveChannelModels.LiveChannelModel.BOOK);
                                //schedulePlay
                            }
                            continue;// schedule 无效
                        }

                    }
                    if (serverDateTime.before(startDate)) {
                        schedule.setState(LiveChannelModels.LiveChannelModel.BOOK);
                        //schedulePlay
                    } else if (serverDateTime.after(endDate)) {

                        //是否移动
                        if (schedule.getContId() != null) {
                            if (schedule.getState() == LiveChannelModels.LiveChannelModel.LOOKBACKING) {
                                isLookIngBack = true;
                                schedule.setState(LiveChannelModels.LiveChannelModel.LOOKBACKING);
                            } else {
                                schedule.setState(LiveChannelModels.LiveChannelModel.LOOKBACK);
                            }
                            //Ifeng的
                        } else {
                            schedule.setState(LiveChannelModels.LiveChannelModel.OVER);
                        }
                        //scheduleOver
                    } else {
                        schedule.setState(isLookIngBack ? LiveChannelModels.LiveChannelModel.LIVE : LiveChannelModels.LiveChannelModel.LIVING);
                        position = i; //
                        //playing
                    }
                }
            }
        }
        return position;
    }

    /**
     * 处理电视台直播列表各个节目状态：播放结束、直播中、预告,并返回当前直播的position
     */
    public static int handleTVLiveState(TVLiveProgramInfo programInfo, Date serverDateTime) {
        int position = 0;
        if (programInfo != null) {
            List<TVLiveProgramInfo.ScheduleEntity> list = programInfo.getSchedule();
            if (list != null) {
                Date startDate, endDate;
                for (int i = 0; i < list.size(); i++) {
                    TVLiveProgramInfo.ScheduleEntity schedule = list.get(i);
                    try {
                        startDate = DateUtils.hhmmssFormat_One.parse(schedule.getStartTime());
                        endDate = DateUtils.hhmmssFormat_One.parse(schedule.getEndTime());
                    } catch (ParseException e) {
                        try {
                            startDate = DateUtils.hhmmssFormat.parse(schedule.getStartTime());
                            endDate = DateUtils.hhmmssFormat.parse(schedule.getEndTime());
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                            continue;// schedule 无效
                        }
                    }
                    if (serverDateTime.before(startDate)) {//预告
                        schedule.setState(TVLiveProgramInfo.ScheduleEntity.NOTICE);
                    } else if (serverDateTime.after(endDate)) {//scheduleOver
                        schedule.setState(TVLiveProgramInfo.ScheduleEntity.END);
                    } else {//playing
                        schedule.setState(TVLiveProgramInfo.ScheduleEntity.LIVING);
                        position = i;
                    }
                }
            }
        }
        return position;
    }

    /**
     * 由于要与IOS保持一致，同时又不改变现有代码逻辑，所以在生成PushMessage对象的时候，对ID做下转换
     */
    public static String transLiveId(String liveId) {
        if (liveId == null)
            return null;
        if (liveId.equals("11"))
            return "chinese";
        if (liveId.equals("12"))
            return "info";
        if (liveId.equals("13"))
            return "hongkong";
        if (liveId.equals("zixun"))
            return "info";
        if (liveId.equals("zhongwen"))
            return "chinese";
        return liveId;
    }

    /**
     * 处理清晰度选择
     */
    public static String getSelectedStream(int currentStream) {
        switch (currentStream) {
            case MediaConstants.STREAM_AUTO:
                return "自动";
            case MediaConstants.STREAM_ORIGINAL:
                return "原画";
            case MediaConstants.STREAM_SUPPER:
                return "超清";
            case MediaConstants.STREAM_HIGH:
                return "高清";
            case MediaConstants.STREAM_MID:
                return "标清";
            case MediaConstants.STREAM_LOW:
                return "流畅";
        }
        return "";
    }

    /**
     * 获取当前时间，此时间是通过与服务器校验得出的，最准
     */
    public static long getCurrentTime() {
        return System.currentTimeMillis() + getTimeDiff();
    }

    /**
     * 获取本地和服务器时间差
     */
    private static long getTimeDiff() {
        return getLiveTimeDiffIsSuccess() ? (long) IfengApplication.getInstance().getAttribute(IntentKey.LIVE_SERVICE_TIME) : 0;
    }

    /**
     * 设置直播时间差获取是否成功
     */
    public static void setLiveTimeDiffIsSuccess(boolean isSuccess) {
        IfengApplication.getInstance().setAttribute(IntentKey.LIVE_GET_SERVICETIME, isSuccess);
    }

    private static boolean getLiveTimeDiffIsSuccess() {
        if (IfengApplication.getInstance().getAttribute(IntentKey.LIVE_GET_SERVICETIME) == null) {
            getServiceTime();
            return false;
        }
        return ((boolean) IfengApplication.getInstance().getAttribute(IntentKey.LIVE_GET_SERVICETIME));
    }

    /**
     * 获取服务器时间
     */
    private static void getServiceTime() {
        RequestJson<LaunchAppModel> myReq = new RequestJson<LaunchAppModel>(Request.Method.GET,
                DataInterface.getLaunchAppUrl(),
                LaunchAppModel.class,
                new Response.Listener<LaunchAppModel>() {
                    @Override
                    public void onResponse(LaunchAppModel response) {
                        try {
                            Long serTime = Long.valueOf(response.getSystemTime());
                            Long timeDiff = serTime - System.currentTimeMillis();
                            IfengApplication.getInstance().setAttribute(IntentKey.LIVE_SERVICE_TIME, timeDiff);
                            setLiveTimeDiffIsSuccess(true);
                        } catch (Exception e) {
                            setLiveTimeDiffIsSuccess(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setLiveTimeDiffIsSuccess(false);
                    }
                }
        );
        VolleyHelper.getRequestQueue().add(myReq);
    }
}
