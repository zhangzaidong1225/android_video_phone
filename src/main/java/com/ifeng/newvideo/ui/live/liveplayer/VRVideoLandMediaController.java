package com.ifeng.newvideo.ui.live.liveplayer;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VR点播全屏controller
 * <p/>
 * Created by Vincent on 2014/11/24.
 */
public class VRVideoLandMediaController extends VRLiveLandMediaController {
    private static final Logger logger = LoggerFactory.getLogger(VRVideoLandMediaController.class);

    private String streamType;

    public VRVideoLandMediaController(Context context) {
        super(context);
        streamType = StreamUtils.getStreamType();
    }

    @Override
    protected View makeControllerView() {
        return ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.live_vr_land_mediacontroller, this);
    }

    private TextView stream;

    @Override
    protected void initControllerView(View v) {
        super.initControllerView(v);
        v.findViewById(R.id.mediacontroller_seekbar_ll).setVisibility(VISIBLE);
        mProgressBar = (SeekBar) v.findViewById(R.id.mediacontroller_seekbar);
        if (mProgressBar != null && mProgressBar instanceof SeekBar) {
            SeekBar seeker = (SeekBar) mProgressBar;
            Drawable thumb = mContext.getResources().getDrawable(R.drawable.controller_seek_bar_thumb);
            thumb.setBounds(0, 0, thumb.getIntrinsicWidth(), thumb.getIntrinsicHeight());
            seeker.setThumb(thumb);
            seeker.setThumbOffset(thumb.getIntrinsicWidth() / 2);
        }

        stream = (TextView) v.findViewById(R.id.selectedStream);
        stream.setVisibility(VISIBLE);
        stream.setText(streamType);
        stream.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getControllerToVideoPlayerListener() != null) {
                    getControllerToVideoPlayerListener().onSelectClick();
                }
            }
        });
    }

    public void updateStreamText(final String streamType) {
        this.streamType = streamType;
        stream.setVisibility(VISIBLE);
        stream.post(new Runnable() {
            @Override
            public void run() {
                stream.setText(streamType);
            }
        });
    }
}
