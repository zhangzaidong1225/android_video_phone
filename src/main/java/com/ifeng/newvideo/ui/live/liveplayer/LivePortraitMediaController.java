package com.ifeng.newvideo.ui.live.liveplayer;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.player.IfengMediaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Vincent on 2014/11/20.
 */
public class LivePortraitMediaController extends IfengMediaController {

    private static final Logger logger = LoggerFactory.getLogger(LivePortraitMediaController.class);
    private TextView tvChannel;
    private boolean isComeFrom;

    public LivePortraitMediaController(Context context) {
        super(context);
        DisplayMetrics disp = mContext.getResources().getDisplayMetrics();
        mWindowWidth = disp.widthPixels;
        mWindowHeight = mWindowWidth * 9 / 16;
    }

    @Override
    protected View makeControllerView() {
        return ((LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.live_mediacontroller_portrait, this);
    }

    @Override
    protected void initControllerView(View v) {
        super.initControllerView(v);
        mFullScreenButton = (RelativeLayout) v.findViewById(R.id.mediacontroller_fullScreen);
        tvChannel = (TextView) v.findViewById(R.id.live_tv_channel);
        if (mFullScreenButton != null) {
            mFullScreenButton.setOnClickListener(mScaleVideoListener);
        }

        mFileName = (TextView) v.findViewById(com.ifeng.video.widget.R.id.mediacontroller_file_name);
        if (tvChannel != null)
            tvChannel.setText(mTitle);
        if (isComeFrom)
            setTextViewComeFromStyle();
    }


    private final OnClickListener mScaleVideoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onFullScreenClick();
            }
        }
    };

    /**
     * 将TextView调整为 来源的样式。
     */
    private void setTextViewComeFromStyle() {
        if (tvChannel != null)
            tvChannel.setTextAppearance(mContext, R.style.live_come_from);
    }

    public void setLiveComeFrom(boolean flag) {
        this.isComeFrom = flag;
    }

    public void setFileName(String name) {
        mTitle = name;
        if (tvChannel != null)
            tvChannel.setText(mTitle);
    }

    @Override
    protected void setWindow() {
        int[] location = new int[2];
        mAnchor.getLocationOnScreen(location);
        mWindow.setAnimationStyle(mAnimStyle);
        mWindow.setWidth(mWindowWidth);
        mWindow.setHeight(mWindowHeight / 4);
        mWindow.showAsDropDown(mAnchor, 0, -mWindowHeight / 4);
    }

    @Override
    public void updatePausePlayView() {
        if (mPlayer == null || mRoot == null || mPauseOrPlayButton == null) {
            return;
        }
        if (mPlayer.isPlaying()) {
            mPauseOrPlayButton.setImageResource(R.drawable.video_pause_btn_selector);
        } else {
            mPauseOrPlayButton.setImageResource(R.drawable.video_play_btn_selector);
        }
    }

}
