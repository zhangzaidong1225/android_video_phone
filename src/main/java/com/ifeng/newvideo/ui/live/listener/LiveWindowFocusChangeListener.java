package com.ifeng.newvideo.ui.live.listener;

/**
 * 主要用于获取屏幕焦掉 从activity中传到Fragment直播中。。
 * Created by Vincent on 2014/11/25.
 */
public interface LiveWindowFocusChangeListener {

    void LiveWindowFocusChanged(boolean hasFocus);
}
