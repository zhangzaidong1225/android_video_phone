package com.ifeng.newvideo.ui.live.liveplayer;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.player.IfengMediaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 直播全屏controller
 * <p/>
 * Created by Vincent on 2014/11/24.
 */
public class LiveLandMediaController extends IfengMediaController {
    private static final Logger logger = LoggerFactory.getLogger(LiveLandMediaController.class);
    private TextView tvComeFrom;
    private boolean isLandscape = true;//是否是横屏

    public LiveLandMediaController(Context context) {
        super(context);
        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        DisplayMetrics disp = mContext.getResources().getDisplayMetrics();
        mWindowWidth = disp.widthPixels;
        mWindowHeight = disp.heightPixels;
        if (!isLandscape) {//如果是竖屏，把宽高互换，转为横屏，为了解决锁屏中点击通知栏进入界面时控制栏显示位置错误（左移）、控制栏不显示（偏移量计算错误，移出屏幕）的问题。bug##9910
            int temp = mWindowWidth;
            mWindowWidth = mWindowHeight;
            mWindowHeight = temp;
        }
        logger.debug("mWindowWidth={}  mWindowHeight={}", mWindowWidth, mWindowHeight);
    }

    @Override
    protected View makeControllerView() {
        return ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.live_land_mediacontroller, this);
    }

    private final OnClickListener mSelectVideoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onSelectClick();
            }
        }
    };

    @Override
    protected void initControllerView(View v) {
        super.initControllerView(v);
        mFullScreenButton = (RelativeLayout) v.findViewById(R.id.mediacontroller_fullScreen);
        if (mFullScreenButton != null) {
            mFullScreenButton.setOnClickListener(mScaleVideoListener);
        }

//        tvComeFrom = (TextView) v.findViewById(R.id.tv_comefrom);
//        if (tvComeFrom != null)
//            tvComeFrom.setText(mTitle);

        mSelectStream = (TextView) v.findViewById(R.id.mediacontroller_selected);
        if (mSelectStream != null) {
            mSelectStream.setOnClickListener(mSelectVideoListener);
        }
    }

    public void setFileName(String name) {
        mTitle = name;
        if (tvComeFrom != null)
            tvComeFrom.setText(mTitle);
    }

    private final OnClickListener mScaleVideoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onFullScreenClick();
            }
        }
    };

    @Override
    protected void setWindow() {
        int[] anchorPos = new int[2];
        mAnchor.getRootView().getLocationOnScreen(anchorPos);
        setLayoutParams(new LayoutParams(mAnchor.getRootView().getWidth(), mAnchor.getRootView().getHeight()));
        Rect anchorRect = new Rect(anchorPos[0], anchorPos[1], anchorPos[0] + mAnchor.getRootView().getWidth(), anchorPos[1] + mAnchor.getRootView().getHeight());
        setLayoutParams(new LayoutParams(mAnchor.getRootView().getWidth(), mAnchor.getRootView().getHeight()));
        mRoot.getLayoutParams().height = mAnchor.getRootView().getHeight() / 4;
        mRoot.requestLayout();
        mWindow.setWidth(mWindowWidth);
        mWindow.setHeight(mWindowHeight / 4);
        logger.debug("setWindow====={}", anchorRect.toString());
        mWindow.showAtLocation(mAnchor.getRootView(), Gravity.NO_GRAVITY, 0, (mWindowHeight * 3 / 4));
    }

    public void updatePausePlayView() {
        if (mPlayer == null || mRoot == null || mPauseOrPlayButton == null) {
            return;
        }
        if (mPlayer.isPlaying()) {
            mPauseOrPlayButton.setImageResource(R.drawable.video_land_pause_btn_selector);
        } else {
            mPauseOrPlayButton.setImageResource(R.drawable.video_land_play_selector);
        }
    }

    public void updateSelectStreamView(int currentStream) {
        if (mSelectStream == null) {
            return;
        }
        switch (currentStream) {
            case MediaConstants.STREAM_AUTO:
                mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_auto));
                break;

            case MediaConstants.STREAM_HIGH:
                mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_hight));
                break;
            case MediaConstants.STREAM_MID:
                mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_mid));
                break;
            case MediaConstants.STREAM_LOW:
                mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_low));
                break;
            default:
                break;
        }
    }
}
