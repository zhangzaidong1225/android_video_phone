package com.ifeng.newvideo.ui.live;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.live.adapter.TVLiveProgramAdapter;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.LiveDao;
import com.ifeng.video.dao.db.model.live.TVLiveProgramInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 电视台节目列表Fragment
 * Created by Administrator on 2016/8/3.
 */
public class TVLiveProgramFragment extends FragmentBase implements TVLiveProgramAdapter.GetLivingProgram{
    private static final Logger logger = LoggerFactory.getLogger(TVLiveProgramFragment.class);
    public static final String CHANNEL_ID = "channelId";
    public static final String DAY = "day";

    private int day;
    private String channelId;

    private View loading;
    private View error;
    private View noNet;
    private ListView listView;
    private TVLiveProgramAdapter adapter;
    private List<TVLiveProgramInfo.ScheduleEntity> list = new ArrayList<>();

    public TVLiveProgramFragment() {
        super();
    }

    public static TVLiveProgramFragment newInstance(String channelId, int day) {
        TVLiveProgramFragment fragment = new TVLiveProgramFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CHANNEL_ID, channelId);
        bundle.putInt(DAY, day);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            channelId = getArguments().getString(CHANNEL_ID);
            day = getArguments().getInt(DAY);
        }
        logger.debug("channelId={},day={}", channelId, day);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_program_list, container, false);
        initView(view);
        getProgramList(channelId, day);
        return view;
    }

    @Override
    public void getLivingProgramInfo(TVLiveProgramInfo.ScheduleEntity entity) {
        if (iScheduleInfo != null) {
            iScheduleInfo.getScheduleInfo(entity);
        }
    }

    @Override
    public void onClick(View v) {
        setStatus(UIStatusLayout.LOADING);
        if (getArguments() != null) {
            getProgramList(channelId, day);
        }
    }

    private void initView(View root) {
        loading = root.findViewById(R.id.loading_layout);
        error = root.findViewById(R.id.load_fail_layout);
        error.setOnClickListener(this);
        noNet = root.findViewById(R.id.no_net_layer);
        noNet.setOnClickListener(this);
        listView = (ListView) root.findViewById(R.id.listView);

        adapter = new TVLiveProgramAdapter(getActivity());
        adapter.setLivingProgramInfo(this);
        listView.setAdapter(adapter);
        setStatus(UIStatusLayout.LOADING);
    }

    /**
     * 获取节目列表数据
     */
    private void getProgramList(String channelId, final int day) {
        Log.d("live", "getProgramList:" + channelId + "--" + day);
        LiveDao.getTVLiveProgramInfo(channelId, day, TVLiveProgramInfo.class,
                new Response.Listener<TVLiveProgramInfo>() {
                    @Override
                    public void onResponse(TVLiveProgramInfo response) {
                        if (response == null || ListUtils.isEmpty(response.getSchedule())) {
                            setStatus(UIStatusLayout.ERROR);
                        } else {
                            setStatus(UIStatusLayout.NORMAL);
                            int focusPosition = initProgramState(response);
                            list = response.getSchedule();
                            adapter.setList(list);
                            listView.setSelection(focusPosition);//聚焦到指定位置
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error instanceof NetworkError) {
                            setStatus(UIStatusLayout.NO_NET);
                        } else {
                            setStatus(UIStatusLayout.ERROR);
                        }
                    }
                }, false);
    }

    /**
     * 初始化节目状态：播放结束、直播中、预告,并且返回当前正在播放的节目的位置
     */
    private int initProgramState(TVLiveProgramInfo models) {
        //Date date = DateUtils.getDateFormat(TimeUtils.getRealTime(getActivity(), System.currentTimeMillis()));
        Date date = DateUtils.getDateFormat(System.currentTimeMillis());
        return LiveUtils.handleTVLiveState(models, date);
    }


    public IScheduleInfo iScheduleInfo;

    /**
     * 获取正在播放的节目信息
     */
    public interface IScheduleInfo {
        void getScheduleInfo(TVLiveProgramInfo.ScheduleEntity entity);
    }

    public void setIScheduleInfo(IScheduleInfo iScheduleInfo) {
        this.iScheduleInfo = iScheduleInfo;
    }


    public static final int LOADING = 1;//加载
    public static final int NORMAL = 2;//正常
    public static final int ERROR = 3;//错误(接口请求失败，数据解析错误)
    public static final int NO_NET = 4;//无网

    /**
     * 根据状态显示相应的布局
     *
     * @param status 状态
     */
    public void setStatus(int status) {
        switch (status) {
            case LOADING:
                loading.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                break;
            case NORMAL:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                break;
            case ERROR:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                noNet.setVisibility(View.GONE);
                break;
            case NO_NET:
                loading.setVisibility(View.GONE);
                listView.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.VISIBLE);
                break;
        }
    }

}
