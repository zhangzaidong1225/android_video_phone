package com.ifeng.newvideo.ui.live;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.ifeng.IfengProxyUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.dialogUI.DialogUtilsFor3G;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.adapter.LiveDetailPagerAdapter;
import com.ifeng.newvideo.ui.live.listener.LiveDetailPlayerTouchListener;
import com.ifeng.newvideo.ui.live.listener.LivePlayClickListener;
import com.ifeng.newvideo.ui.live.listener.LivePlayStateChangeListener;
import com.ifeng.newvideo.ui.live.liveplayer.LiveLandMediaController;
import com.ifeng.newvideo.ui.live.liveplayer.LivePortraitMediaController;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.adapter.BottomLayoutInit;
import com.ifeng.newvideo.videoplayer.listener.ActivityLiveDialogClickListener;
import com.ifeng.newvideo.videoplayer.listener.ConnectivityReceiver;
import com.ifeng.newvideo.videoplayer.player.PlayUrlAuthUtils;
import com.ifeng.newvideo.videoplayer.widget.BottomLayout;
import com.ifeng.newvideo.videoplayer.widget.IfengPortraitMediaController;
import com.ifeng.newvideo.videoplayer.widget.SeekBarVer;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.GravityUtils;
import com.ifeng.video.core.utils.JsonUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.dao.LiveNetWorkDao;
import com.ifeng.video.dao.db.model.LiveChannelModels;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import com.ifeng.video.player.ControllerToVideoPlayerListener;
import com.ifeng.video.player.IMediaPlayer;
import com.ifeng.video.player.IfengMediaController;
import com.ifeng.video.player.IfengVideoView;
import com.ifeng.video.player.PlayState;
import com.ifeng.video.player.PlayType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 直播的播放基页
 * Created by Vincent on 2014/11/18.R
 */
public class ActivityLive extends BaseFragmentActivity implements IfengVideoView.InterceptOpenVideo,
        SensorEventListener, ConnectivityReceiver.ConnectivityChangeListener,
        IfengPortraitMediaController.OnShownListener, IfengPortraitMediaController.OnHiddenListener,
        BottomLayout.IsShowBottomLayout, FragmentLiveProgramList.NotifyPlayingProgram, FragmentLiveProgramList.NotifyTitle,
        NotifyShareCallback {

    private static final Logger logger = LoggerFactory.getLogger(ActivityLive.class);

    private PagerSlidingTabStrip slidingTabStrip;
    private ViewPager viewPager;
    private LiveInfoModel model;
    public String echid = null;
    public static final int INVALID_URL = -1;
    private static final int MAX_CHANGE_TIMES = 4;//直播码流数（自动、高清、标清、流畅）
    public boolean isPlayerInit = false;
    public boolean isErroring = false;
    public IfengVideoView mVideoView;
    private LinearLayout mStreamSwitchToast;
    public IfengMediaController mVideoController;
    private SensorManager sensorManager;
    public int mLayout = IfengVideoView.VIDEO_LAYOUT_PORTRAIT;
    public int mCurrentStream = MediaConstants.STREAM_AUTO; //默认码流为高清
    public AudioManager mAudioManager;
    public boolean isCompleted = false;
    public int playPosWhenSwitchAV = 0;
    public boolean isClickStreamSelect = false;
    private View mVideoStreamSelectLayer;
    private RelativeLayout loadingAndRetryLay;
    private ViewGroup videoLoadingLayer;
    private ImageView mTopAudioSwitcher;
    private ImageView mTopPortraitBack;
    private Sensor sensor;
    public boolean isClocked = false;
    private boolean isFirstVolumeChange = true;

    private View mTopTitleLayer;
    private ImageView mTitleLayBackBtn;

    private RelativeLayout gestureGuideLayP;
    private RelativeLayout gestureGuideLayL, gestureGuideLay;
    public ImageView mOperationBg, mOperationPercent;
    public View mProgress_graph_fl, mProgress_text_ll;
    private View mRightVolumeLayer;
    private View mRightListLayer;
    private SeekBarVer seekbar_volume;

    private final static int TIMEOUT_MAX = 30000;
    private final int STREAM_SWITCH_TOAST_DELAY_TIME = 3000;
    private Button btn_volume;

    public boolean isShouldShow = true;
    public View mGestureRootView;
    private LiveDetailPlayerTouchListener mTouchListener;
    private View mVideoErrorRetryLayer;
    private boolean sensor_portrait;
    private ViewGroup mVideoViewParent;
    private View videoErrorPauseLayer;
    private View videoMobileNetLayer;
    private LivePlayClickListener livePlayClickListener;
    private LivePlayStateChangeListener playStateChangeListener;
    private final static int CONTROLLER_SHOW_TIME = 3600 * 1000;
    private final List<FragmentLiveProgramList> mProgramFragmentList = new ArrayList<FragmentLiveProgramList>();
    private boolean isFirstRegistFinishReceiver = false;
    //通过点击转成横屏，重力感应器需要做判断
    private boolean hasClickToLandScape;
    //通过点击转成竖屏，重力感应器需要做判断
    private boolean hasClickToPortrait;


    //左侧锁屏
    private ImageView mLeftClock;

    //二级页面标记
    public static final int RIGHTLAYER_MORE = 3001;//更多
    public static final int RIGHTLAYER_SHARE = 3002;//分享
    private static final int RIGHTLAYER_STREAM = 3003;//码流切换

    //播放器全屏右侧二级页面
    private RelativeLayout mRightMoreLayer;
    private LinearLayout mRightShareLayer;
    private RelativeLayout mRightDownLoad;
    private RelativeLayout mRightCollect;
    private RelativeLayout mRightBook;
    private ImageView ivQQ;
    private ImageView ivSina;
    private ImageView ivWechat;
    private ImageView ivWechatMoment;
    private ImageView ivQzone;
    private ImageView ivAlipay;

    private RelativeLayout mRightStreamLayer;
    private RelativeLayout mRightStreamAuto;
    private RelativeLayout mRightStreamOrig;
    private RelativeLayout mRightStreamSupper;
    private RelativeLayout mRightStreamHigh;
    private RelativeLayout mRightStreamMid;
    private RelativeLayout mRightStreamLow;
    private ImageView mRightStreamAutoLine;
    private ImageView mRightStreamOrigLine;
    private ImageView mRightStreamSuperLine;
    private ImageView mRightStreamHighLine;
    private ImageView mRightStreamMidLine;

    private final static int IFENG_TYPE = 1;
    private final static int OTHER_TYPE = 0;
    private EditPage mEditPage;
    public String programTitle = "";
    private TextView tvLoadingProgram;
    /**
     * 最大声音
     */
    public int mMaxVolume;
    private boolean isCurrentUnicomData = false;
    /**
     * 当前声音
     */
    public int mVolume = -1;
    /**
     * 用来记录横竖屏切换之前保存的currentVolume值，防止点击静音键无效果
     */
    public int mCurrentVolume = 0;
    private boolean isActive = true;
    private boolean mIsMobileNet = false;
    /**
     * 当前播放进度
     */
    public long mVideoProgress = 0;
    private boolean mIsNewRegisterReceiver = true;
    private ConnectivityReceiver connectivityReceiver;
    //亮度
    public float mBrightness = -1f;
    public float proBrightPercent = 0;
    // 视频播放过程中缓冲图层
    private RelativeLayout bufferingLay;
    private OneKeyShare mOneKeyShare;
    private View errorRetryBottomTv;
    private TextView errorLayerTitle;
    private View errorRetryLayer = null;
    private List<LiveInfoModel> models;
    private String channelIdOrTitle;     //凤凰自有台的 title 或者 ChannelId 。用来定位具体台。
    private final ActivityLiveDialogClickListener mDialogClickListener = new ActivityLiveDialogClickListener(this);
    //private RelativeLayout mTitleLayBackBtnClick;
    public boolean canClickBottomBtn = false;
    public boolean mobileNetShowing;
    private boolean noNetworkShowing;
    private LiveDetailPagerAdapter pagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
        initData();
        initView();
        initIntent(getIntent());
        initBottomLayout();
        volumeBusiness();
        registerMobileAlertReceiver();
        registerNotifyActivityLiveUiReceiver();
    }

    private DialogUtilsFor3G m3GDialogUtils;

    public LiveInfoModel getCurrProgram() {
        return model;
    }

    private void unregisterMobileAlertReceiver() {
        if (connectivityReceiver != null) unregisterReceiver(connectivityReceiver);
    }

    private void registerMobileAlertReceiver() {
        mIsMobileNet = NetUtils.isMobile(this);

        if (connectivityReceiver == null) {
            connectivityReceiver = new ConnectivityReceiver(this);
        }
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterMobileAlertReceiver();
        unRegisterNotifyActivityLiveUiReceiver();
        VolleyHelper.getRequestQueue().cancelAll(LiveNetWorkDao.Tag);
    }

    private void updateClockBtn(boolean selected) {
        if (isLandScape()) {
            mLeftClock.setVisibility(View.VISIBLE);
        } else {
            mLeftClock.setVisibility(View.GONE);
        }
        mLeftClock.setSelected(selected);
        mSharePreUtils.setVodCurrentStream(mCurrentStream);
    }

    public void clockScreen() {
        if (isDialogShow()) {
            return;
        }
        if (isClocked) {
            updateClockBtn(false);
            isClocked = false;
        } else {
            updateClockBtn(true);
            isClocked = true;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void initView() {
        slidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.live_activity_slidingtab);
        slidingTabStrip.setShouldExpand(true);

        viewPager = (ViewPager) findViewById(R.id.live_activity_pager);
        LinearLayout bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);

        mVideoView = (IfengVideoView) findViewById(R.id.player_videoview);
        mVideoViewParent = (ViewGroup) findViewById(R.id.live_player);
        mVideoView.setVideoLayout(mLayout);

        gestureGuideLay = (RelativeLayout) findViewById(R.id.video_gesture_guide);
        if (gestureGuideLay != null) {
            gestureGuideLay.setOnClickListener(livePlayClickListener);
        }
        gestureGuideLayL = (RelativeLayout) findViewById(R.id.video_gesture_guide_land);
        gestureGuideLayP = (RelativeLayout) findViewById(R.id.video_gesture_guide_portrait);
        mGestureRootView = findViewById(R.id.video_gesture_handle_lay);

        loadingAndRetryLay = (RelativeLayout) findViewById(R.id.video_loading_and_retry_lay);


        videoLoadingLayer = (ViewGroup) findViewById(R.id.video_loading_layout);
        errorLayerTitle = ((TextView) videoLoadingLayer.findViewById(R.id.progress_text_info));
        loadingAndRetryLay.removeAllViews();
        RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
        videoLoadingLayer.setVisibility(View.VISIBLE);
        loadingAndRetryLay.addView(videoLoadingLayer, loadingLp);
        tvLoadingProgram = ((TextView) videoLoadingLayer.findViewById(R.id.progress_text_info));

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mVideoErrorRetryLayer = inflater.inflate(R.layout.common_video_error_retry_layer, null);
        videoErrorPauseLayer = inflater.inflate(R.layout.common_video_error_pause_layer, null);
        videoMobileNetLayer = inflater.inflate(R.layout.common_video_mobile_net_layer, null);
        mVideoStreamSelectLayer = inflater.inflate(R.layout.common_video_streamchange_layer, null);

        mVideoViewParent.findViewById(R.id.gesture_hint_progress_img_p).setVisibility(View.GONE);
        mVideoViewParent.findViewById(R.id.gesture_hint_progress_img).setVisibility(View.GONE);
        mVideoViewParent.findViewById(R.id.gesture_hint_progress_tv).setVisibility(View.GONE);
        mVideoViewParent.findViewById(R.id.gesture_hint_progress_tv_p).setVisibility(View.GONE);


        mVideoErrorRetryLayer.findViewById(R.id.video_error_retry_img).setOnClickListener(livePlayClickListener);

        errorRetryLayer = mVideoErrorRetryLayer.findViewById(R.id.video_error_retry_layout);
        errorRetryBottomTv = mVideoErrorRetryLayer.findViewById(R.id.video_error_retry_bottom_tv);
        videoErrorPauseLayer.findViewById(R.id.pauseToResume).setOnClickListener(livePlayClickListener);
        videoMobileNetLayer.findViewById(R.id.video_mobile_continue).setOnClickListener(livePlayClickListener);
        ImageView centerPausePlayImg = new ImageView(this);
        RelativeLayout.LayoutParams centerPauseRlp = new RelativeLayout.LayoutParams(DisplayUtils.convertDipToPixel(72),
                DisplayUtils.convertDipToPixel(73));
        centerPauseRlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        centerPausePlayImg.setScaleType(ImageView.ScaleType.FIT_XY);
        centerPausePlayImg.setImageDrawable(getResources().getDrawable(R.drawable.common_resume_button_selector));
        centerPausePlayImg.setLayoutParams(centerPauseRlp);
        bufferingLay = (RelativeLayout) inflater.inflate(R.layout.video_buffer_progress, null);

        // 手势相关
        mOperationBg = (ImageView) findViewById(R.id.operation_bg);
        mOperationPercent = (ImageView) findViewById(R.id.operation_percent);
        mProgress_graph_fl = findViewById(R.id.progress_graph_fl);
        mProgress_text_ll = findViewById(R.id.progress_text_ll);

        // 顶部控制栏
        mTopTitleLayer = findViewById(R.id.video_top_title_layout);


        mTitleLayBackBtn = (ImageView) mTopTitleLayer.findViewById(R.id.video_detail_landscape_top_back_btn_click);
        mTitleLayBackBtn.setOnClickListener(livePlayClickListener);
        mTitleLayBackBtn.setVisibility(View.VISIBLE);
        //mTitleLayBackBtnClick.setVisibility(View.VISIBLE);

        //右侧二级页面

        mRightShareLayer = (LinearLayout) findViewById(R.id.video_land_right_share);


        //清晰度頁面
        mRightStreamLayer = (RelativeLayout) findViewById(R.id.video_land_right_stream);
        mRightStreamAuto = (RelativeLayout) findViewById(R.id.video_land_right_stream_auto);
        mRightStreamAuto.setVisibility(View.VISIBLE);
        mRightStreamAutoLine = (ImageView) findViewById(R.id.land_right_line_auto);
        mRightStreamOrig = (RelativeLayout) findViewById(R.id.video_land_right_stream_original);
        mRightStreamOrigLine = (ImageView) findViewById(R.id.land_right_line_original);
        mRightStreamSupper = (RelativeLayout) findViewById(R.id.video_land_right_stream_supper);
        mRightStreamSuperLine = (ImageView) findViewById(R.id.land_right_line_supper);
        mRightStreamHigh = (RelativeLayout) findViewById(R.id.video_land_right_stream_high);
        mRightStreamHighLine = (ImageView) findViewById(R.id.land_right_line_high);
        mRightStreamMid = (RelativeLayout) findViewById(R.id.video_land_right_stream_mid);
        mRightStreamMidLine = (ImageView) findViewById(R.id.land_right_line_mid);
        mRightStreamLow = (RelativeLayout) findViewById(R.id.video_land_right_stream_low);
        mRightStreamAuto.setOnClickListener(livePlayClickListener);
        mRightStreamOrig.setOnClickListener(livePlayClickListener);
        mRightStreamSupper.setOnClickListener(livePlayClickListener);
        mRightStreamHigh.setOnClickListener(livePlayClickListener);
        mRightStreamMid.setOnClickListener(livePlayClickListener);
        mRightStreamLow.setOnClickListener(livePlayClickListener);
        //分享页面
        ivQQ = (ImageView) findViewById(R.id.share_iv_qq);
        ivSina = (ImageView) findViewById(R.id.share_iv_sina);
        ivWechat = (ImageView) findViewById(R.id.share_iv_wechat);
        ivWechatMoment = (ImageView) findViewById(R.id.share_iv_wechat_moment);
        ivQzone = (ImageView) findViewById(R.id.share_iv_qzone);
        ivAlipay = (ImageView) findViewById(R.id.share_iv_alipay);
        ivQQ.setOnClickListener(livePlayClickListener);
        ivSina.setOnClickListener(livePlayClickListener);
        ivWechat.setOnClickListener(livePlayClickListener);
        ivWechatMoment.setOnClickListener(livePlayClickListener);
        ivQzone.setOnClickListener(livePlayClickListener);
        ivAlipay.setOnClickListener(livePlayClickListener);
        mLeftClock = (ImageView) findViewById(R.id.video_land_left_clock);
        mLeftClock.setOnClickListener(livePlayClickListener);
        mVideoView.setmInterceptOpenVideo(this);

        //卡顿提示view
        mStreamSwitchToast = (LinearLayout) findViewById(R.id.stream_switch_toast);
        mRightVolumeLayer = this.findViewById(R.id.volume_layer);
        seekbar_volume = (SeekBarVer) this.findViewById(R.id.seekBar_volume);
        seekbar_volume.setOnSeekBarChangeListener(new SeekBarVer.OnSeekBarChangeListenerVer() {
            @Override
            public void onProgressChanged(SeekBarVer VerticalSeekBar, int curVolume, boolean fromUser) {
                mCurrentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                if (!isFirstVolumeChange) {
                    if (curVolume > mMaxVolume) {
                        updateVolume(mMaxVolume);
                    } else if (curVolume <= 0) {
                        updateVolume(0);
                    } else {
                        updateVolume(curVolume);
                    }
                }
                isFirstVolumeChange = false;
            }

            @Override
            public void onStartTrackingTouch(SeekBarVer VerticalSeekBar) {
                mVideoController.show(CONTROLLER_SHOW_TIME);
            }

            @Override
            public void onStopTrackingTouch(SeekBarVer VerticalSeekBar) {
                mVideoController.show();
                mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            }
        });

        btn_volume = (Button) this.findViewById(R.id.btn_volume);
        btn_volume.setOnClickListener(livePlayClickListener);
        mRightListLayer = this.findViewById(R.id.right_layer_listView);
        mTopAudioSwitcher = (ImageView) this.findViewById(R.id.media_controller_audio);
        mTopAudioSwitcher.setOnClickListener(livePlayClickListener);
        mTopPortraitBack = (ImageView) this.findViewById(R.id.media_controller_back);
        mTopPortraitBack.setOnClickListener(livePlayClickListener);
        //mTopAudioSwitcher.setVisibility(View.VISIBLE);
    }

    private boolean isMobileNetOpen() {
        boolean isMobile = NetUtils.isMobile(this);
//        boolean isMobileOpen = mSharePreUtils.getNetState();
        boolean isMobileOpen = true;
        return !isMobile || isMobileOpen;
    }

    @Override
    public void onConnectivityChange(String action) {
        // 由播放器报error处理
        if (!PackageUtils.isActivityOnStackTop(this, getActivityName())) {
            return;
        }

        if (mIsNewRegisterReceiver) {
            mIsNewRegisterReceiver = false;
            return;
        }

        //展示无网界面
        if (!NetUtils.isNetAvailable(this)) {
            handleNoNet();
            return;
        }

        if (NetUtils.isMobile(this) && show3GDialogForNetChange()) {

        } else if (NetUtils.isNetAvailable(this)) {
            if (isCurrentUnicomData) {
                refreshDataOnlyVideo();
            } else {
                if (isInPlaybackState()) {
                    //切回wifi之后重新续播
                    if (mobileNetShowing && mVideoView.isPauseState()) {
                        updateMobileNetLayer(false);
                        mVideoView.start();
                    } else {
                        updateMobileNetLayer(false);
                        prepareToPlay();
                    }
                } else {
                    updateMobileNetLayer(false);
                    getVideoPosition();
                    logger.debug("playPosWhenSwitchAVWifi{}", "" + playPosWhenSwitchAV);
                    //prepareToPlay();
                    refreshFragmentData();
                }
            }
        }
    }


    /**
     * 如果没有播放展示界面（loading），如果正在播放则继续播放
     */
    private void handleNoNet() {
        if (mVideoView != null && !mVideoView.isInPlaybackState()) {
            logger.debug("the no net stop video service");
            updateErrorPauseLayer(true);
        }
    }

    private final Handler streamHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            hideStreamSwitchToast();
        }
    };

    public void showStreamSwitchToast() {
        if (!isLandScape() || mVideoView.isSeeking || mCurrentStream == MediaConstants.STREAM_LOW || mCurrentStream == MediaConstants.STREAM_AUTO) {
            return;
        }
        streamHandler.removeMessages(0);
        mStreamSwitchToast.setVisibility(View.VISIBLE);
        streamHandler.sendEmptyMessageDelayed(0, STREAM_SWITCH_TOAST_DELAY_TIME);
    }

    private void hideStreamSwitchToast() {
        mStreamSwitchToast.setVisibility(View.GONE);
    }

    private boolean showDialogFor3G() {
        logger.debug("Business  showDialogFor3G============{}");
        if (isActive) {
            if (NetUtils.isMobile(this) && !isMobileNetOpen()) {
                logger.debug("Business  isMobileNetOpen============{}");
                updateErrorPauseLayer(true);
                m3GDialogUtils.showNoMobileOpenDialog(ActivityLive.this);
                return true;
            } else if (NetUtils.isMobile(this) && isMobileNetOpen()) {
                logger.debug("Business  show3GNetAlertDialog============{}");
                if (IfengApplication.mobileNetCanPlay) {
                    //提示运营商网络
                    if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                        ToastUtils.getInstance().showLongToast(R.string.play_moblie_net_hint);
                        return false;
                    }
                } else {
                    updateMobileNetLayer(true);
                    return true;
                }
            } else if (NetUtils.isNetAvailable(this)) {
                logger.debug("Business  isNetAvailable============{}");
                if (isCurrentUnicomData) {
                    refreshDataOnlyVideo();
                    return true;
                }
            }
        }
        return false;
    }

    private boolean show3GDialogForRefresh() {
        if (m3GDialogUtils.mCurMobileWapDialog != null && isIfeng()) {
            if (NetUtils.isMobile(this) && !NetUtils.isMobileWap(this)) {
                m3GDialogUtils.mCurMobileWapDialog.cancel();
            }
        }
        if (NetUtils.isMobile(this) && !isMobileNetOpen()) {
            updateErrorPauseLayer(true);
            m3GDialogUtils.showNoMobileOpenDialog(ActivityLive.this);
            return true;
        }
        return false;
    }

    private boolean show3GDialogForNetChange() {
        logger.debug("show3GDialogForNetChange");
        if (isActive) {
            if (isMobileNetOpen()) {
                if (!IfengApplication.mobileNetCanPlay) {
                    getVideoPosition();
                    mVideoView.pause();
                    updateMobileNetLayer(true);
//                        m3GDialogUtils.show3GNetAlertDialog(this);
                    return true;
                } else {
                    if (isInPlaybackState()) {
                        if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                            ToastUtils.getInstance().showLongToast(R.string.play_moblie_net_hint);
                        }
                        return true;
                    }
                    getVideoPosition();
                    prepareToPlay();
                    return true;
                }
            } else {
                updateErrorPauseLayer(true);
                mVideoView.stopPlayback();
                m3GDialogUtils.showNoMobileOpenDialog(ActivityLive.this);
                return true;
            }
        }
        return false;
    }


    public void continueMobilePlay() {
        IfengApplication.mobileNetCanPlay = true;
        updateMobileNetLayer(false);
        if (mVideoView != null) {
            if (mVideoView.isPauseState()) {
                mVideoView.start();
            } else {
                prepareToPlay();
            }
        }
    }

    /**
     * 联通免流刷新数据逻辑
     * 因为需要重新取凤凰的免流量播放地址，因此需要从频道详情页拉取凤凰的联通凤凰播放地址
     */
    private void refreshDataOnlyVideo() {
        logger.error("走刷新逻辑啦~~~~~");
        if (model != null) {
            getVideoPosition();
            // title和channelId都可以用来匹配ifeng直播台   优先级： ChannelId > Title
            if (TextUtils.isEmpty(model.getTitle())) {
                channelIdOrTitle = model.getChannelId();
            } else {
                channelIdOrTitle = model.getTitle();
            }
            unGetLiveInfoModel();
        }
    }

    private void initData() {
        mLayout = IfengVideoView.VIDEO_LAYOUT_FULL;

        m3GDialogUtils = new DialogUtilsFor3G();
        m3GDialogUtils.setDialogCallBack(mDialogClickListener);

        livePlayClickListener = new LivePlayClickListener(this);
        playStateChangeListener = new LivePlayStateChangeListener(this);
        mTouchListener = new LiveDetailPlayerTouchListener(this);
        mVideoController = new LivePortraitMediaController(this);

        mOneKeyShare = new OneKeyShare(this);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mCurrentStream = mSharePreUtils.getLiveCurrentStream();
    }

    private boolean hasShowShareWindow;

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        hasShowShareWindow = isShow;
    }

    private static class GetLiveIfengChannelErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
        }
    }

    public class LiveAudioFinishReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isFinishing() && !isFirstRegistFinishReceiver) {
                finish();
            }
            isFirstRegistFinishReceiver = false;
        }
    }

    private void initIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        boolean isPush = intent.getBooleanExtra(IntentKey.LIVE_BY_PUSH_INDEX, false);
        if (intent.getExtras() != null) {
            echid = intent.getExtras().getString(IntentKey.E_CHID);
        }
        echid = !TextUtils.isEmpty(echid) ? echid : "";
        if (isPush) {
            channelIdOrTitle = intent.getStringExtra(IntentKey.LIVE_IFENG_URL);
            getLiveInfoModel();
        } else {
            model = (LiveInfoModel) intent.getSerializableExtra(IntentKey.LIVE_INFO_MODEL);
            if (model == null) {
                showNoResourceDialog();
                return;
            }
            if (model.getVideoH() != null) {
                isCurrentUnicomData = model.getVideo().contains("cuff.");
            }
            logger.debug("预约拉起{}", model.toString());
            setAdapter();
        }

    }

    private void setAdapter() {
        initFragmentManager(getSupportFragmentManager());
        pagerAdapter = new LiveDetailPagerAdapter(getSupportFragmentManager());
        mProgramFragmentList.clear();
        mProgramFragmentList.addAll(getProgramFragment(model));
        logger.debug("mProgramFragmentList {}", mProgramFragmentList.toString());

        pagerAdapter.setData(mProgramFragmentList);
        viewPager.setOffscreenPageLimit(mProgramFragmentList.size());
        viewPager.setAdapter(pagerAdapter);
        slidingTabStrip.setViewPager(viewPager);
        pagerAdapter.notifyDataSetChanged();
        slidingTabStrip.notifyDataSetChanged();

        viewPager.setCurrentItem(0);

        if (!NetUtils.isNetAvailable(this)) {
            updateErrorPauseLayer(true);
            return;
        }

        //凤凰的直接播放
        if (model.getIsiFeng() == IFENG_TYPE) {
            prepareToPlay();
        }
    }


    /**
     * 当需要改变ViewPager中的Fragment时，需要先清除以前的
     */
    private void initFragmentManager(FragmentManager fm) {
        logger.debug("初始化该DetailVideoActivity中的FragmentManager");
        FragmentTransaction ft = fm.beginTransaction();
        for (int i = 0; i < 3; i++) {
            String name = makeFragmentName(viewPager.getId(), i);
            logger.debug("name=={}", name);
            Fragment fragment = fm.findFragmentByTag(name);
            if (fragment != null) {
                ft.detach(fragment);
                ft.remove(fragment);
            }
        }
        ft.commitAllowingStateLoss();
    }

    /**
     * 获取viewPage里面的Fragment类名
     *
     * @param viewId
     * @param id
     * @return
     */
    private String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    public boolean isIfeng() {
        return model != null && model.getIsiFeng() == IFENG_TYPE;
    }

    private void unGetLiveInfoModel() {
        logger.error("unGetLiveInfoModel");
        isCurrentUnicomData = LiveNetWorkDao.getLiveIfengChannelData(
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString().trim())) {
                            showNoResourceDialog();
                            return;
                        }
                        JSONObject object = JSON.parseObject(response.toString());
                        if (object == null || object.getJSONArray("liveInfo") == null) {
                            showNoResourceDialog();
                            return;
                        }
                        models = JSON.parseArray(object.getJSONArray("liveInfo").toString(), LiveInfoModel.class);
                        for (LiveInfoModel model1 : models) {
                            if (model1.getTitle().equalsIgnoreCase(channelIdOrTitle) || model1.getChannelId().equalsIgnoreCase(channelIdOrTitle)) {
                                model = model1;
                                model.setIsiFeng(IFENG_TYPE);
                                break;
                            }
                        }
                        if (model == null) {
                            showNoResourceDialog();
                            return;
                        }
                        prepareToPlay();
                    }
                },
                new GetLiveIfengChannelErrorListener());
    }


    private void getLiveInfoModel() {
        if (!NetUtils.isNetAvailable(this)) {
            updateErrorPauseLayer(true);
            videoErrorPauseLayer.findViewById(R.id.pauseToResume).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getLiveInfoModel();
                }
            });
            return;
        }

        isCurrentUnicomData = LiveNetWorkDao.getLiveIfengChannelData(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (response == null || TextUtils.isEmpty(response.toString().trim())) {
                    showNoResourceDialog();
                    return;
                }
                JSONObject object = JSON.parseObject(response.toString());
                if (object == null || object.getJSONArray("liveInfo") == null) {
                    showNoResourceDialog();
                    return;
                }
                models = JSON.parseArray(object.getJSONArray("liveInfo").toString(), LiveInfoModel.class);
                for (LiveInfoModel model1 : models) {
                    if (model1.getTitle().equalsIgnoreCase(channelIdOrTitle) || model1.getChannelId().equalsIgnoreCase(channelIdOrTitle)) {
                        model = model1;
                        model.setIsiFeng(IFENG_TYPE);
                        break;
                    }
                }
                if (model == null) {
                    showNoResourceDialog();
                    return;
                }
                setAdapter();
                //TODO NO_NET
            }
        }, new GetLiveIfengChannelErrorListener());

    }


    /**
     * 获取下方Fragment 列表
     *
     * @param model
     * @return
     */
    private List<FragmentLiveProgramList> getProgramFragment(LiveInfoModel model) {
        List<FragmentLiveProgramList> data = new ArrayList<FragmentLiveProgramList>();
        //凤凰
        if (model.getChannelId() != null) {
            data.add(new FragmentLiveProgramList(1, "?channelId=" + model.getChannelId(), IFENG_TYPE, this, this, this));
            data.add(new FragmentLiveProgramList(2, "?channelId=" + model.getChannelId(), IFENG_TYPE, this, this, this));
            data.add(new FragmentLiveProgramList(3, "?channelId=" + model.getChannelId(), IFENG_TYPE, this, this, this));
            //移动
        } else if (model.getNodeId() != null) {
            data.add(new FragmentLiveProgramList(-2, "?nodeId=" + model.getNodeId(), OTHER_TYPE, this, this, this));
            data.add(new FragmentLiveProgramList(-1, "?nodeId=" + model.getNodeId(), OTHER_TYPE, this, this, this));
            data.add(new FragmentLiveProgramList(0, "?nodeId=" + model.getNodeId(), OTHER_TYPE, this, this, this));
        }
        return data;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (isClocked && isLandScape()) {
            return;
        }
        if (mVideoView == null) {
            return;
        }
//        if (!mVideoView.isInPlaybackState() && ((audioService != null && !audioService.isInPlaybackState()) || audioService == null)) {
//            return;
//        }
        if (!GravityUtils.isSystemGravityOpened(mVideoView.getContext())) {
            if (!isLandScape() && !hasClickToLandScape) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            return;
        }

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        //接收到重力变为竖着的时候做的一些操作
        if (Math.abs(x) <= 3.0f && y >= 7.0f && Math.abs(z) <= 5) {
            hasClickToPortrait = false;
            if (!hasClickToLandScape) {
                if (!isLandScape()) {// 当前是竖屏了不用再发了
                    return;
                }
                if (!sensor_portrait) {
                    if (Build.VERSION.SDK_INT >= 9) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }
                    sensor_portrait = true;
                }
            }
        }
        //接收到重力变为横着的时候做的一些操作
        if (Math.abs(x) >= 7.0f && Math.abs(y) <= 4f && Math.abs(z) <= 6.0) {
            hasClickToLandScape = false;
            sensor_portrait = false;
            if (isLandScape()) {// 当前是横屏了不用再发了
                return;
            }
            if (!hasClickToPortrait) {
                if (Build.VERSION.SDK_INT >= 9) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    private boolean isInPlaybackState() {
        return (mVideoView != null && mVideoView.isInPlaybackState()) || model == null;
    }

    private void getVideoPosition() {
        if (mVideoView != null && mVideoView.getPrePosition() != 0) {
            playPosWhenSwitchAV = (int) mVideoView.getPrePosition();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerSensorListener();
    }

    private void registerSensorListener() {
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            if (mVideoController != null && mVideoController.isShowing()) {
                mVideoController.continueShow();
            }
        } else {
            if (mVideoController != null) {
                mVideoController.rmMsgFadeOut();
            }
        }
    }

    public void updateVolume(int volume) {
        if (mAudioManager != null && seekbar_volume != null && btn_volume != null) {
            setCurrentVolume(volume);
            seekbar_volume.setProgress(volume);
            setVolBtnDrawable(volume);
        }
    }

    private void setCurrentVolume(int currentVolume) {
        mCurrentVolume = currentVolume;
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
    }

    private void setVolBtnDrawable(int volume) {
        if (volume <= 0) {
            btn_volume.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_videoplay_button_no_volume_selector));
        } else {
            btn_volume.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_video_play_btn_volume_selector));
        }
    }

    public void switchOrientation() {
        Configuration config = this.getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            toLand();
            hasClickToLandScape = true;
        } else {
            toPortrait();
            hasClickToPortrait = true;
        }
    }

    private void toLand() {
        if (Build.VERSION.SDK_INT >= 9) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    public void toPortrait() {
        if (isLandScape()) {
            if (Build.VERSION.SDK_INT >= 9) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    }

    public void hideController() {
        if (mVideoController != null) {
            mVideoController.hide();
        }
    }

    public void updateGestureGuideLayer(boolean show) {
        if (show) {
            gestureGuideLay.setVisibility(View.VISIBLE);
            gestureGuideLayL.setVisibility(isLandScape() ? View.VISIBLE : View.GONE);
            gestureGuideLayP.setVisibility(isLandScape() ? View.GONE : View.VISIBLE);
        } else {
            mSharePreUtils.setLivePlayGestureState(false);
            gestureGuideLay.setVisibility(View.GONE);
            gestureGuideLayL.setVisibility(View.GONE);
            gestureGuideLayP.setVisibility(View.GONE);
        }
    }

    private boolean isSilent = false;

    public void silentBtnClick() {
        if (mAudioManager != null) {
            isSilent = !isSilent;
            updateSilentVolumeBtn(mCurrentVolume);
        }
    }

    private void updateSilentVolumeBtn(int volume) {
        if (mAudioManager != null && seekbar_volume != null && btn_volume != null) {
            if (isSilent) {
                int mute = 0;
                mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                seekbar_volume.setProgress(mute);
                setVolBtnDrawable(mute);
            } else {
                setCurrentVolume(volume);
                seekbar_volume.setProgress(volume);
                setVolBtnDrawable(volume);
            }
        }
    }

    public void prepareToPlay() {
        logger.debug("prepareToPlay------start---------");
        if (mVideoView != null && mVideoView.isInPlaybackState()) {
            mVideoView.stopPlayback();
        }
        if (NetUtils.isNetAvailable(this)) {
            videoLoadingLayer.findViewById(R.id.video_loading_imageview).setVisibility(View.VISIBLE);
        }
        if (showDialogFor3G() || !NetUtils.isNetAvailable(this)) {
            logger.debug("prepareToPlay------start---------3G Dialog");
            return;
        }

        if (model == null) {
            return;
        }
        updateLoadingLayer(true);
        videoPlayerInit();
        isPlayerInit = true;
        logger.debug("prepareToPlay------end---------");
    }


    private boolean isDialogShow() {
        if (m3GDialogUtils == null) {
            return false;
        }
        if (m3GDialogUtils.businessVideoDialog != null && m3GDialogUtils.businessVideoDialog.isShowing()) {
            return true;
        }
        if (m3GDialogUtils.m3GNetAlertDialog != null && m3GDialogUtils.m3GNetAlertDialog.isShowing()) {
            return true;
        }
        if (m3GDialogUtils.mNoMobileOpenDialog != null && m3GDialogUtils.mNoMobileOpenDialog.isShowing()) {
            return true;
        }
        return m3GDialogUtils.mCurMobileWapDialog != null && m3GDialogUtils.mCurMobileWapDialog.isShowing();

    }

    @Override
    public boolean isInterceptOpenVideo() {
        return isDialogShow();
    }

    public void updateLoadingLayer(boolean show) {
        if (show) {
            mobileNetShowing = false;
            noNetworkShowing = false;
            hideGestureView();
            setControllerShowHide(false);
            loadingAndRetryLay.removeAllViews();
            if (isClickStreamSelect) {
                loadingAndRetryLay.setBackgroundColor(getResources().getColor(R.color.transparent_stream));
                RelativeLayout.LayoutParams streamLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);
                streamLp.addRule(RelativeLayout.CENTER_IN_PARENT);
                videoLoadingLayer.setVisibility(View.GONE);
                mVideoStreamSelectLayer.setVisibility(View.VISIBLE);
                loadingAndRetryLay.addView(mVideoStreamSelectLayer, streamLp);
                ((TextView) mVideoStreamSelectLayer.findViewById(R.id.stream_selected)).setText(LiveUtils.getSelectedStream(mCurrentStream));
            } else {
                if (videoLoadingLayer.getVisibility() != View.VISIBLE) {
                    videoLoadingLayer.setVisibility(View.VISIBLE);
                }
                loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
                RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
                (videoLoadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.VISIBLE);
                loadingAndRetryLay.addView(videoLoadingLayer, loadingLp);
                LiveInfoModel program = model;
                if (program != null) {
                    tvLoadingProgram.setText(programTitle);
                }

                tvLoadingProgram.setVisibility(View.VISIBLE);
                playStateChangeListener.mTimeoutHandler.removeMessages(0);
                playStateChangeListener.mTimeoutHandler.sendEmptyMessageDelayed(0, TIMEOUT_MAX);
            }
            loadingAndRetryLay.setVisibility(View.VISIBLE);
        } else {
            setControllerShowHide(true);
            playStateChangeListener.mTimeoutHandler.removeMessages(0);
            videoLoadingLayer.setVisibility(View.GONE);
            loadingAndRetryLay.removeAllViews();
            loadingAndRetryLay.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            loadingAndRetryLay.setVisibility(View.INVISIBLE);
        }

    }

    /**
     * 隐藏手势标示
     */
    private void hideGestureView() {
        if (mGestureRootView != null) {
            mGestureRootView.setVisibility(View.GONE);
        }
    }

    /**
     * 控制控制栏是否显示
     *
     * @param isShow
     */
    private void setControllerShowHide(boolean isShow) {
        canClickBottomBtn = isShow;
        if (isLandScape()) {
            if (!isShow) {
                mTopPortraitBack.setVisibility(View.VISIBLE);
            } else {
                mTopPortraitBack.setVisibility(View.GONE);
            }
        }

        if (mVideoView != null) {
            mVideoView.setControllerVisibily(isShow);
        }
    }

    private void updateVideoMediaController() {
        if (isLandScape()) {
            mVideoController = new LiveLandMediaController(this);
        } else {
            mVideoController = new LivePortraitMediaController(this);
            ((LivePortraitMediaController) mVideoController).setLiveComeFrom(true);
        }
        mVideoController.setFileName(model.getIsiFeng() == IFENG_TYPE ? "" : getResources().getString(R.string.live_controller_source_name));
        mVideoController.setOnHiddenListener(this);
        mVideoController.setOnShownListener(this);
        mVideoController.setControllerToVideoPlayerListener(controllerListener);
        mVideoView.setMediaController(mVideoController);
    }

    private void videoPlayerInit() {
        updateVideoMediaController();
        mVideoView.setStateListener(playStateChangeListener);
//        mVideoView.seekTo(playPosWhenSwitchAV);
        String url = getCorrectUrl();
        UserFeed.initIfengAddress(IfengApplication.getAppContext(), url);
        logger.debug("videoPlayerInitOnline  url===={}", url);
        if (TextUtils.isEmpty(url)) {
            if (NetUtils.isNetAvailable(this)) {
                updateErrorRetryLayer(true);
                showNoResourceDialog();
            } else {
                updateErrorPauseLayer(true);
                //ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            }
            hideController();
            return;
        }
        IMediaPlayer.MediaSource source = new IMediaPlayer.MediaSource();
        source.id = PlayType.FEATRUE;
        source.playUrl = url;
        IMediaPlayer.MediaSource[] mediaSources = new IMediaPlayer.MediaSource[1];
        mediaSources[0] = source;
        mVideoView.setVideoPath(mediaSources);
    }

    public void refreshFragmentData() {
        for (int i = 0; i < mProgramFragmentList.size(); i++) {
            FragmentLiveProgramList programList = mProgramFragmentList.get(i);
            if (programList != null) {
                programList.getData();
            }
        }
    }

    public String getCorrectUrl() {
        if (model == null) {
            return null;
        }
        String url = null;
        if (mCurrentStream == INVALID_URL) {
            mCurrentStream = MediaConstants.STREAM_HIGH;
        }
        int times = 0;
        while (StringUtils.isBlank(url)) {
            switch (mCurrentStream) {
                case MediaConstants.STREAM_AUTO:
                    mCurrentStream = MediaConstants.STREAM_AUTO;
                    url = model.getVideo();
                    break;
                case MediaConstants.STREAM_HIGH:
                    mCurrentStream = MediaConstants.STREAM_HIGH;
                    url = model.getVideoH();
                    break;
                case MediaConstants.STREAM_MID:
                    mCurrentStream = MediaConstants.STREAM_MID;
                    url = model.getVideoM();
                    break;
                case MediaConstants.STREAM_LOW:
                    mCurrentStream = MediaConstants.STREAM_LOW;
                    url = model.getVideoL();
                    break;
                default:
                    break;
            }
            //将所有码流查找一遍后退出
            if (times == MAX_CHANGE_TIMES) {
                break;
            }
            if (StringUtils.isBlank(url)) {
                times++;
                mCurrentStream--;
                if (mCurrentStream == INVALID_URL) {
                    mCurrentStream = MediaConstants.STREAM_HIGH;
                }
            }
        }

        if (isIfeng()) {
            url = IfengProxyUtils.getProxyUrl(url);
            url = PlayUrlAuthUtils.getLiveAuthUrl(url);
        }
        logger.debug("live playUrl = {}", url);
        return url;
    }

    public void oneShareHorizontal(View v) {
        if (model != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            mOneKeyShare.shareLive(model.getShareUrl(), v, programTitle, this, model.getcName(), model.getBkgURL(), true);
        }
    }

    /**
     * 没有加载出资源时候弹的dialog
     */
    private Dialog dialog;


    private void showNoResourceDialog() {
        AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.video_play_url_miss), getString(R.string.common_i_know), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                finish();
                Intent intent = new Intent(ActivityLive.this, ActivityMainTab.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    public void oneShare(View v) {
        if (model != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            mOneKeyShare.shareLive(model.getShareUrl(), v, programTitle, this, model.getcName(), model.getBkgURL(), false);
        }
    }

    @Override
    public void onShown() {
        if (isLandScape()) {
            hideLandRight();
            DisplayUtils.setDisplayStatusBar(this, false);

            mTopTitleLayer.setVisibility(View.VISIBLE);
            updateRightVolume(true);
            updateVolumeByKeyEvent();
        } else {

            this.updateRightVolume(false);
            mTopTitleLayer.setVisibility(View.GONE);
        }
        updateClockBtn(isClocked);
        updateSelectView();
        if (mVideoController != null && mVideoController.mSelectStream != null) {
            mVideoController.mSelectStream.setVisibility(View.VISIBLE);
            mVideoController.mSelectStream.setTextColor(getResources().getColor(R.drawable.common_pop_spinner_text_selector));
            mVideoController.mSelectStream.setClickable(true);
        }
        updateLandTitleView();

    }

    private void updateRightVolume(boolean show) {
        if (mRightVolumeLayer != null) {
            mRightVolumeLayer.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onHidden() {
        updateRightVolume(false);

        mOneKeyShare.popDismiss();
        if (mLeftClock.getVisibility() == View.VISIBLE) {
            mLeftClock.setVisibility(View.GONE);
        }
        if (mRightListLayer.getVisibility() == View.GONE) {
            mTopTitleLayer.setVisibility(View.GONE);
            if (isLandScape()) {
                DisplayUtils.setDisplayStatusBar(this, true);
            }
        }
    }

    /**
     * 在有网情况下展示的error界面
     *
     * @param show
     */
    public void updateErrorRetryLayer(boolean show) {
        if (show) {
            mobileNetShowing = false;
            noNetworkShowing = false;
            hideGestureView();
            setControllerShowHide(false);
            loadingAndRetryLay.removeAllViews();
            if (videoLoadingLayer.getVisibility() != View.VISIBLE) {
                videoLoadingLayer.setVisibility(View.VISIBLE);
            }
            loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (videoLoadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.INVISIBLE);
            errorLayerTitle.setVisibility(View.GONE);
            loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            loadingAndRetryLay.addView(mVideoErrorRetryLayer, errorLayerLp);
            errorRetryLayer.setVisibility(View.VISIBLE);
            TextView tv = (TextView) errorRetryBottomTv;

            View retry = mVideoErrorRetryLayer.findViewById(R.id.video_error_retry_img);
            tv.setVisibility(View.VISIBLE);
            if (model.getIsiFeng() == IFENG_TYPE) {
                tv.setText(getResources().getString(R.string.error_retry_bottom_audio));

            } else {
                tv.setText(getResources().getString(R.string.error_retry_bottom));

                ((RelativeLayout.LayoutParams) retry.getLayoutParams()).addRule(RelativeLayout.CENTER_HORIZONTAL);
            }
            loadingAndRetryLay.setVisibility(View.VISIBLE);
        } else {
            setControllerShowHide(true);
            videoLoadingLayer.setVisibility(View.GONE);
            loadingAndRetryLay.removeAllViews();
            loadingAndRetryLay.setVisibility(View.GONE);
            loadingAndRetryLay.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            errorRetryLayer.setVisibility(View.GONE);
            errorRetryBottomTv.setVisibility(View.GONE);
        }

    }

    /**
     * 在无网情况下展示的error界面
     *
     * @param show
     */
    public void updateErrorPauseLayer(boolean show) {
        noNetworkShowing = show;
        if (show) {
            mobileNetShowing = false;
            hideController();
            hideLandRight();
            hideGestureView();
            setControllerShowHide(false);
            loadingAndRetryLay.removeAllViews();
            if (videoLoadingLayer.getVisibility() != View.VISIBLE) {
                videoLoadingLayer.setVisibility(View.VISIBLE);
            }
            videoLoadingLayer.findViewById(R.id.video_loading_imageview).setVisibility(View.INVISIBLE);
            loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (videoLoadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.INVISIBLE);
            loadingAndRetryLay.addView(videoLoadingLayer, loadingLp);
            errorLayerTitle.setVisibility(View.GONE);

            loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            loadingAndRetryLay.addView(videoErrorPauseLayer, errorLayerLp);
            videoErrorPauseLayer.findViewById(R.id.video_error_pause_lay).setVisibility(View.VISIBLE);
            loadingAndRetryLay.setVisibility(View.VISIBLE);
        } else {
            setControllerShowHide(true);
            loadingAndRetryLay.removeAllViews();
            loadingAndRetryLay.setVisibility(View.GONE);
            loadingAndRetryLay.setBackgroundColor(getResources().getColor(R.color.common_transparent));

            mVideoErrorRetryLayer.findViewById(R.id.video_error_retry_bottom_tv).setVisibility(View.GONE);
        }
    }

    /**
     * 在运营商网络下展示提示
     *
     * @param show
     */
    public void updateMobileNetLayer(boolean show) {
        mobileNetShowing = show;
        if (mVideoView != null) {//视频多次后台唤醒后播放页为运营商界面但有声音在播 bug #10026。为了解决该问题在SurfaceView生命周期内增加判断条件，判断是否执行播放器状态方法
            mVideoView.isNeedTodPlay(!show);
        }
        if (show) {
            noNetworkShowing = false;
            hideLandRight();
            hideGestureView();
            setControllerShowHide(false);
            loadingAndRetryLay.removeAllViews();
            if (videoLoadingLayer.getVisibility() != View.VISIBLE) {
                videoLoadingLayer.setVisibility(View.VISIBLE);
            }
            loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            (videoLoadingLayer.findViewById(R.id.player_progress_layout)).setVisibility(View.INVISIBLE);
            loadingAndRetryLay.addView(videoLoadingLayer, loadingLp);
            errorLayerTitle.setVisibility(View.GONE);

            loadingAndRetryLay.setBackgroundColor(R.color.common_video_loading_all_bg);
            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);
            loadingAndRetryLay.addView(videoMobileNetLayer, errorLayerLp);
            //videoMobileNetLayer.findViewById(R.id.video_mobile_net_lay).setVisibility(View.VISIBLE);
            loadingAndRetryLay.setVisibility(View.VISIBLE);
        } else {
            setControllerShowHide(true);
            loadingAndRetryLay.removeAllViews();
            loadingAndRetryLay.setVisibility(View.GONE);
            loadingAndRetryLay.setBackgroundColor(getResources().getColor(R.color.common_transparent));
            //videoMobileNetLayer.findViewById(R.id.video_mobile_net_lay).setVisibility(View.GONE);
        }
    }


    public void showController() {
        if (mVideoController != null) {
            mVideoController.show();
        }
    }

    private void continueShowController() {
        if (mVideoController != null) {
            mVideoController.continueShow();
        }
        updateClockBtn(isClocked);
    }

    public void updateBufferLayer(boolean show) {
        loadingAndRetryLay.setVisibility(View.VISIBLE);
        for (int i = 0; i < loadingAndRetryLay.getChildCount(); i++) {
            View view = loadingAndRetryLay.getChildAt(i);

            view.setVisibility(View.GONE);

        }
        if (show) {
            if (videoLoadingLayer.getVisibility() != View.VISIBLE) {
                loadingAndRetryLay.removeView(bufferingLay);
                bufferingLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);
                loadingAndRetryLay.addView(bufferingLay, loadingLp);
            }
        } else {
            loadingAndRetryLay.removeView(bufferingLay);
            bufferingLay.setVisibility(View.GONE);
        }

    }


    private final ControllerToVideoPlayerListener controllerListener = new ControllerToVideoPlayerListener() {

        @Override
        public void onGyroClick() {

        }

        @Override
        public void onFullScreenClick() {
            switchOrientation();
        }

        @Override
        public void onSubscribeClick() {
        }

        @Override
        public void onDlnaClick() {
        }

        @Override
        public void onStreamItemClick(int definition) {
            mCurrentStream = definition;
            mSharePreUtils.setLiveCurrentStream(mCurrentStream);
//            playPosWhenSwitchAV = mVideoView.getCurrentPosition();
            isClickStreamSelect = true;
            prepareToPlay();
        }

        @Override
        public void onSelectClick() {
            showLandRight(RIGHTLAYER_STREAM);
        }

        @Override
        public void onLockClick() {

        }

        @Override
        public void onSwitchAVMode() {
        }

        @Override
        public void onSwitchProgram(boolean nextOrPre) {
            if (!NetUtils.isNetAvailable(ActivityLive.this)) {
                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                mVideoView.stopPlayback();
                hideController();
                updateErrorPauseLayer(true);
                return;
            }
            playPosWhenSwitchAV = 0;
        }
    };

    private void updateSelectView() {
        if (mVideoController != null && mVideoController instanceof LiveLandMediaController) {
            mVideoController.updateSelectStreamBtn(true);
            mVideoController.updateSelectStreamView(mCurrentStream);
        }
    }

    private void initControllerShowState() {
        isShouldShow = mVideoController != null && mVideoController.isShowing();
    }

    private void showLandscapeLayer() {
        if (mRightListLayer != null && mRightListLayer.getVisibility() == View.VISIBLE) {
            if (isLandScape()) {
                mRightListLayer.setVisibility(View.VISIBLE);
                mVideoView.setControllerVisibily(false);
            } else {
                mRightListLayer.setVisibility(View.GONE);
                mVideoView.setControllerVisibily(true);
            }
        }
        if (mTopTitleLayer.getVisibility() == View.VISIBLE) {
            mTopTitleLayer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initControllerShowState();
        if (mGestureRootView != null) {
            mGestureRootView.setVisibility(View.GONE);
        }
        mOneKeyShare.setIsLandScape(isLandScape());
        updateGestureGuideLayer(mSharePreUtils.getLivePlayGestureState());
        if (isLandScape()) {
            //根据无网页面是否展示来判断是否应该隐藏，如果无网页面展示了就显示后退键，否则相反
            int visibility = videoErrorPauseLayer.findViewById(R.id.video_error_pause_lay).getVisibility();
            if (!mobileNetShowing && ((mVideoView != null && mVideoView.isInPlaybackState() && visibility != View.VISIBLE))) {
                mTopPortraitBack.setVisibility(View.GONE);
            }

            DisplayUtils.setDisplayStatusBar(ActivityLive.this, true);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            mTitleLayBackBtn.setVisibility(View.VISIBLE);
            //mTitleLayBackBtnClick.setVisibility(View.VISIBLE);
            if (mVideoView != null) {
                ViewGroup.LayoutParams plp = mVideoViewParent.getLayoutParams();
                plp.height = ViewGroup.LayoutParams.MATCH_PARENT;
                plp.width = ViewGroup.LayoutParams.MATCH_PARENT;
                mVideoViewParent.requestLayout();
                mLayout = IfengVideoView.VIDEO_LAYOUT_FULL;
                mVideoView.setVideoLayout(IfengVideoView.VIDEO_LAYOUT_FULL);
                updateVideoMediaController();
            }
            if (BottomLayoutInit.bottomLayout != null) {
                BottomLayoutInit.bottomLayout.hideBottomLayout();
            }
        } else { // Orientation portrait.
            hideLandRight();
            hideStreamSwitchToast();
            mTopPortraitBack.setVisibility(View.VISIBLE);
            DisplayUtils.setDisplayStatusBar(this, false);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            mVideoViewParent.requestLayout();
            mTitleLayBackBtn.setVisibility(View.INVISIBLE);
            //mTitleLayBackBtnClick.setVisibility(View.INVISIBLE);
            if (mVideoView != null) {
                mLayout = IfengVideoView.VIDEO_LAYOUT_PORTRAIT;
                mVideoView.setVideoLayout(IfengVideoView.VIDEO_LAYOUT_PORTRAIT);
                updateVideoMediaController();
            }
            if (BottomLayoutInit.bottomLayout != null) {
                BottomLayoutInit.bottomLayout.showBottomLayout();
            }
        }
        showLandscapeLayer();
//        hideController();
        if (isShouldShow) {
            continueShowController();
        }
    }

    private void updateVolumeByKeyEvent() {
        int tempVolume = getCurrentVolume();
        updateVolume(tempVolume);
    }

    private int getCurrentVolume() {
        return mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    private void updateLandTitleView() {
        if (isLandScape()) {
            DisplayUtils.setDisplayStatusBar(this, false);
            ((RelativeLayout.LayoutParams) mTopTitleLayer.getLayoutParams()).topMargin = DisplayUtils.getStatusBarHeight();
        } else {
            ((RelativeLayout.LayoutParams) mTopTitleLayer.getLayoutParams()).topMargin = 0;
        }
        TextView titleTv = (TextView) mTopTitleLayer.findViewById(R.id.video_landscape_title);
        titleTv.setText(programTitle != null ? programTitle : model.getTitle());
    }

    private void volumeBusiness() {
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mCurrentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        setVolBtnDrawable(mCurrentVolume);
        if (seekbar_volume != null) {
            SeekBarVer seeker = seekbar_volume;
            seeker.setEnabled(true);
            seekbar_volume.setMax(mMaxVolume);
            seekbar_volume.setProgress(mCurrentVolume);
        }
    }

    private GestureDetector popBottomViewDetector;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isLandScape()) {
            if (mVideoController != null) {
                if (mVideoView.isInPlaybackState() && !mobileNetShowing && !noNetworkShowing && mTouchListener.onTouchEvent(ev)) {
                    return true;
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        //竖屏事件处理
        else {
            if (mVideoController != null) {
                if (mVideoView.isInPlaybackState() && !noNetworkShowing && mTouchListener.onTouchEvent(ev)) {
                    return true;
                }
            }
            //底部栏事件处理
            if (popBottomViewDetector != null) {
                if (popBottomViewDetector.onTouchEvent(ev)) {
                    return true;
                }
            }
            return super.dispatchTouchEvent(ev);
        }
    }

    /**
     * 初始化底部的操作栏
     */
    private void initBottomLayout() {
        popBottomViewDetector = BottomLayoutInit.create(this, (ViewGroup) this.findViewById(R.id.video_bottom), livePlayClickListener);
        if (BottomLayoutInit.bottomLayout != null) {
            BottomLayoutInit.bottomLayout.setVisibility(View.VISIBLE);
            BottomLayoutInit.bottomLayout.bottom_collect_rl.setVisibility(View.GONE);
            BottomLayoutInit.bottomLayout.bottom_dlna.setVisibility(View.GONE);
            BottomLayoutInit.bottomLayout.bottom_subscribe.setVisibility(View.GONE);
            BottomLayoutInit.bottomLayout.bottom_play_audio_rl.setVisibility(View.GONE);
            BottomLayoutInit.bottomLayout.setIsShowBottomLayout(this);
            BottomLayoutInit.bottomLayout.showBottomLayout();
        }
    }

    @Override
    public boolean isShowBottomLayout() {
        return true;
    }

    /**
     * 此处 其实 主要用于移动台的
     *
     * @param liveChannelModel
     * @param day
     * @param position
     */
    @Override
    public void playingProgam(final LiveChannelModels.LiveChannelModel liveChannelModel, int day, int position) {
        for (int i = 0; i < 3; i++) {
            if (i == getFragmentNum(day)) {
                mProgramFragmentList.get(i).setPlayingChange(position);
                continue;
            }
            mProgramFragmentList.get(i).setPlayingChange(-1);
        }

        //非凤凰处理逻辑，因为只有非凤凰的才有切换节目的功能
        if (model.getIsiFeng() != IFENG_TYPE) {

            model.setContId(liveChannelModel.getContId());
            mVideoView.stopPlayback();
            model.setProgramTitle(liveChannelModel.getProgramTitle());
            programTitle = liveChannelModel.getProgramTitle();
            updateLoadingLayer(true);
        }
    }

    private int getFragmentNum(int day) {
        if (model.getIsiFeng() == IFENG_TYPE) {
            return day - 1;
        }
        return day + 2;
    }

    private long firstTme = 0;

    private boolean isDoubleClick() {
        if (!PhoneConfig.ua_for_video.contains("huawei")) {
            return false;
        }
        if (firstTme <= 0) {
            firstTme = System.currentTimeMillis();
            return false;
        } else {
            long endTime = System.currentTimeMillis();
            int DOUBLE_CLICK_MAX_TIME = 1500;
            if (endTime - firstTme < DOUBLE_CLICK_MAX_TIME) {
                return true;
            } else {
                firstTme = endTime;
                return false;
            }
        }
    }

    private boolean isKeyDown = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isKeyDown = true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (isDoubleClick() || !isActive || !isKeyDown) {
                    return true;
                }
                isKeyDown = false;
                isClocked = false;
                if (isLandScape()) {
                    hasClickToPortrait = true;
                    toPortrait();
                    return true;
                }
                mVideoView.stopPlayback();
                finish();
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        volumeHandler.sendEmptyMessage(0);
                    }
                }.start();
                return super.onKeyUp(keyCode, event);
            default:
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    private final Handler volumeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            updateVolumeByKeyEvent();
        }
    };
    private boolean isCurrPauseState = false;// 记录离开页面的时候是否本身就为暂停状态

    public LiveInfoModel getLiveModel() {
        return model;
    }

    private int getPlayerPosition() {
        int position = mVideoView.getCurrentPosition();
        if (position != 0) {
            return position;
        } else {
            return (int) mVideoView.getPrePosition();
        }
    }

    @Override
    public void notifyTitle(String title) {
        programTitle = title;
        if (tvLoadingProgram != null) {
            tvLoadingProgram.setText(programTitle);
        }
        super.onPause();
        if (mVideoView != null) {
            if (mVideoView.isInPlaybackState()) {
                playPosWhenSwitchAV = getPlayerPosition();
            }
            //isCurrPauseState = !mVideoView.isPlaying();
            if (isCurrPauseState) {
                if (mVideoController != null && mVideoView.isInPlaybackState()) {
                    playPosWhenSwitchAV = getPlayerPosition();
                    mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                    mVideoView.pause();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideLandRight();
        if (mVideoView != null) {
            if (mVideoView.isInPlaybackState()) {
                playPosWhenSwitchAV = getPlayerPosition();
            }
            isCurrPauseState = !mVideoView.isPlaying();
            if (isCurrPauseState) {
                if (mVideoController != null && mVideoView.isInPlaybackState()) {
                    playPosWhenSwitchAV = getPlayerPosition();
                    mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                    mVideoView.pause();
                }
            } else {
                if (isLandScape()) {
                    mVideoView.pause();
                } else if (!hasShowShareWindow) {
                    mVideoView.pause();
                }
            }
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        hasShowShareWindow = false;
        isActive = true;
        if (!isLandScape()) {
            DisplayUtils.setDisplayStatusBar(this, false);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        if (mobileNetShowing) {//如果显示运营商网络界面，直接返回bug #10026
            return;
        }
        if (IfengApplication.isShareVertialActivityFinish) {
            IfengApplication.isShareVertialActivityFinish = false;
        } else {
            playStateChangeListener.insertCustomerStatistics(PlayState.STATE_PREPARING);
        }
        if (!isCurrPauseState) {
            OneKeyShare.isLandWebQQ = false;
            if (mVideoController != null && mVideoView != null) {
                mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
                mVideoView.start();
            }
        }
        if (IfengApplication.isShareVertialActivityFinish) {
            IfengApplication.isShareVertialActivityFinish = false;
        } else {
            playStateChangeListener.insertCustomerStatistics(PlayState.STATE_PREPARING);
        }
        if (!isCurrPauseState) {
            OneKeyShare.isLandWebQQ = false;
            if (mVideoController != null && mVideoView != null) {
                mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
                mVideoView.start();
            }
        }
        if (show3GDialogForRefresh()) {
        }
    }

    private boolean isShareShow = false;

    @Override
    public void notifyShare(EditPage page, boolean show) {
        if (show) {
            mEditPage = page;
            isShareShow = true;
            if (mVideoController != null && mVideoView != null && mVideoView.isInPlaybackState()) {
                if (!mVideoView.isPauseState()) {
                    mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                    mVideoView.pause();
                }
            }
        } else {
            isShareShow = false;
            if (hasHomeClick) {
                mVideoView.seekTo(playPosWhenSwitchAV);
            }
        }
    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {
        if (isWebQQ) {
            if (mVideoController != null && mVideoView != null && mVideoView.isInPlaybackState()) {
                if (!mVideoView.isPauseState()) {
                    if (!isLandScape()) {
                        mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                        mVideoView.pause();
                    }
                }
            }
        }
    }

    @Override
    protected void onStop() {
        isActive = false;
        if (mVideoController != null && mVideoView != null && mVideoView.isInPlaybackState()) {
            mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
            mVideoView.pause();
        }
        if (mVideoView.isInPlaybackState()) {
            playStateChangeListener.insertCustomerStatistics(PlayState.STATE_IDLE);
        }
        super.onStop();
    }

    protected boolean isExit(MotionEvent ev) {
        if (isLandScape()) {
            return false;
        }
        int playerHeight = DisplayUtils.getWindowWidth() * 9 / 16
                + DisplayUtils.getStatusBarHeight() + DisplayUtils.convertDipToPixel(10);
        if (ev != null && ev.getY() > playerHeight) {
            return viewPager.getCurrentItem() == 0;
        }
        return false;
    }

    /**
     * 展示二级界面
     */
    public void showLandRight(int layerType) {
        hideController();
        switch (layerType) {
            case RIGHTLAYER_MORE:
                if (mRightMoreLayer != null && mRightMoreLayer.getVisibility() == View.GONE) {
                    showAnimation(mRightMoreLayer);
                    updateRightMore();
                }
                break;
            case RIGHTLAYER_SHARE:
                if (mRightShareLayer != null && mRightShareLayer.getVisibility() == View.GONE) {
                    showAnimation(mRightShareLayer);
                }
                break;
            case RIGHTLAYER_STREAM:
                if (mRightStreamLayer != null && mRightStreamLayer.getVisibility() == View.GONE) {
                    showAnimation(mRightStreamLayer);
                    updateRightStream();
                }
                break;
        }
    }

    private void updateRightStream() {
        updateSelectView();
        if (mRightStreamOrig == null || mRightStreamSupper == null || mRightStreamHigh == null || mRightStreamMid == null || mRightStreamLow == null) {
            return;
        }
        switch (mCurrentStream) {
            case MediaConstants.STREAM_AUTO:
                //mVideoController.mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_auto));
                mRightStreamAuto.setSelected(true);
                mRightStreamOrig.setSelected(false);
                mRightStreamSupper.setSelected(false);
                mRightStreamHigh.setSelected(false);
                mRightStreamMid.setSelected(false);
                mRightStreamLow.setSelected(false);
                break;
            case MediaConstants.STREAM_ORIGINAL:
                //mVideoController.mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_original));
                mRightStreamAuto.setSelected(false);
                mRightStreamOrig.setSelected(true);
                mRightStreamSupper.setSelected(false);
                mRightStreamHigh.setSelected(false);
                mRightStreamMid.setSelected(false);
                mRightStreamLow.setSelected(false);
                break;
            case MediaConstants.STREAM_SUPPER:
                //mVideoController.mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_supper));
                mRightStreamAuto.setSelected(false);
                mRightStreamOrig.setSelected(false);
                mRightStreamSupper.setSelected(true);
                mRightStreamHigh.setSelected(false);
                mRightStreamMid.setSelected(false);
                mRightStreamLow.setSelected(false);
                break;
            case MediaConstants.STREAM_HIGH:
                //mVideoController.mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_hight));
                mRightStreamAuto.setSelected(false);
                mRightStreamOrig.setSelected(false);
                mRightStreamSupper.setSelected(false);
                mRightStreamHigh.setSelected(true);
                mRightStreamMid.setSelected(false);
                mRightStreamLow.setSelected(false);
                break;
            case MediaConstants.STREAM_MID:
                //mVideoController.mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_mid));
                mRightStreamAuto.setSelected(false);
                mRightStreamOrig.setSelected(false);
                mRightStreamSupper.setSelected(false);
                mRightStreamMid.setSelected(true);
                mRightStreamLow.setSelected(false);
                mRightStreamHigh.setSelected(false);
                break;
            case MediaConstants.STREAM_LOW:
                //mVideoController.mSelectStream.setText(this.getResources().getString(com.ifeng.video.widget.R.string.android_video_player_video_low));
                mRightStreamAuto.setSelected(false);
                mRightStreamOrig.setSelected(false);
                mRightStreamSupper.setSelected(false);
                mRightStreamHigh.setSelected(false);
                mRightStreamMid.setSelected(false);
                mRightStreamLow.setSelected(true);
                break;
            default:
                break;
        }
        ImageView lastTopLine = null;
        if (model != null) {
            if (!JsonUtils.checkDataInJSONObject(model.getVideo())) {
                mRightStreamAuto.setVisibility(View.GONE);
            } else {
                mRightStreamAuto.setVisibility(View.VISIBLE);
                lastTopLine = mRightStreamAutoLine;
            }
//            if (!JsonUtils.checkDataInJSONObject(model.media_url_original)) {
            mRightStreamOrig.setVisibility(View.GONE);
//            } else {
//                mRightStreamOrig.setVisibility(View.VISIBLE);
//                lastTopLine = mRightStreamOrigLine;
//            }
//            if (!JsonUtils.checkDataInJSONObject(program.media_url_supper)) {
            mRightStreamSupper.setVisibility(View.GONE);
//            } else {
//                mRightStreamSupper.setVisibility(View.VISIBLE);
//                lastTopLine = mRightStreamSuperLine;
//            }
            if (!JsonUtils.checkDataInJSONObject(model.getVideoH())) {
                mRightStreamHigh.setVisibility(View.GONE);
            } else {
                mRightStreamHigh.setVisibility(View.VISIBLE);
                lastTopLine = mRightStreamHighLine;
            }
            if (!JsonUtils.checkDataInJSONObject(model.getVideoM())) {
                mRightStreamMid.setVisibility(View.GONE);
            } else {
                mRightStreamMid.setVisibility(View.VISIBLE);
                lastTopLine = mRightStreamMidLine;
            }
            if (!JsonUtils.checkDataInJSONObject(model.getVideoL())) {
                mRightStreamLow.setVisibility(View.GONE);
                if (lastTopLine != null) {
                    lastTopLine.setVisibility(View.GONE);
                }
            } else {
                mRightStreamLow.setVisibility(View.VISIBLE);
            }
        }
    }

    private void updateRightMore() {

    }

    public void onRightStreamItemClick(int definition) {
        if (definition == mCurrentStream) {
            return;
        }
        hideLandRight();
        if (!NetUtils.isNetAvailable(ActivityLive.this)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (isDialogShow()) {
            return;
        }
        mCurrentStream = definition;
        mSharePreUtils.setLiveCurrentStream(mCurrentStream);
        getVideoPosition();
        isClickStreamSelect = true;
        prepareToPlay();
        updateRightStream();
    }

    /**
     * 隐藏二级界面
     */
    private void hideLandRight() {
        if (mRightMoreLayer != null && mRightMoreLayer.getVisibility() == View.VISIBLE) {
            hideAnimation(mRightMoreLayer);
        }
        if (mRightShareLayer != null && mRightShareLayer.getVisibility() == View.VISIBLE) {
            hideAnimation(mRightShareLayer);
        }
        if (mRightStreamLayer != null && mRightStreamLayer.getVisibility() == View.VISIBLE) {
            hideAnimation(mRightStreamLayer);
        }
    }

    /**
     * 隐藏二级页面动画
     */
    private void hideAnimation(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_slide_right_out));
        view.setVisibility(View.GONE);
    }

    /**
     * 显示二级页面动画
     */
    private void showAnimation(View view) {
        view.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_slide_left_in));
    }

    public boolean isSelectedCheck() {
        return (mRightMoreLayer != null && mRightMoreLayer.getVisibility() == View.VISIBLE)
                || (mRightShareLayer != null && mRightShareLayer.getVisibility() == View.VISIBLE)
                || (mRightStreamLayer != null && mRightStreamLayer.getVisibility() == View.VISIBLE);
    }

    /**
     * 播放音频的服务用广播来通知UI改变
     */
    public void registerNotifyActivityLiveUiReceiver() {
        if (notifyUiReceiver == null) {
            notifyUiReceiver = new NotifyBroadcastReceiver();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.ACTION_AUDIO_PLAYING);
        filter.addAction(IntentKey.ACTION_INIT_CONTROLL_AUDIO_SERVICE);
        filter.addAction(IntentKey.ACTION_OVERFLOW_AUDIO_SERVICE);
        filter.addAction(IntentKey.ACTION_CHANGE_3GNET_SERVICE);
        LocalBroadcastManager.getInstance(this).registerReceiver(notifyUiReceiver, filter);
    }

    private void unRegisterNotifyActivityLiveUiReceiver() {
        if (notifyUiReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(notifyUiReceiver);
            notifyUiReceiver = null;
        }
    }

    /**
     * 消息接受类
     */
    private NotifyBroadcastReceiver notifyUiReceiver;

    /**
     * 类描述： 用于消息推送广播的接收 创建人： 揭耀祖 创建时间： 2013-10-28
     */
    private class NotifyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }
            if (IntentKey.ACTION_AUDIO_PLAYING.equals(intent.getAction())) {
                mVideoView.setControllerVisibily(true);
            } else if (IntentKey.ACTION_CHANGE_3GNET_SERVICE.equals(intent.getAction())) {
                updateMobileNetLayer(true);
            }
        }
    }

}
