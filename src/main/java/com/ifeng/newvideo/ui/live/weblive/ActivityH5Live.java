package com.ifeng.newvideo.ui.live.weblive;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import cn.sharesdk.framework.Platform;
import com.google.gson.Gson;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.dialogUI.LiveCommentUtil;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.ui.basic.BaseLiveActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.NetUtils;

import java.util.Map;

public class ActivityH5Live extends BaseLiveActivity implements View.OnClickListener, NotifyShareCallback, JsBridge.JSDispatchListener {


    private LiveCommentUtil mCommentEditFragment;
    private OneKeyShare mOneKeyShare;
    private boolean isKeyDown = false;
    private static String pageInfo;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d("jsbridge", "H5Live");
            switch (msg.what) {
                case OPEN_INPUT_LAYOUT:
                    showEditCommentWindow("");
                    break;
                case OPEN_SHARE_POP:
                    Bundle bundle = msg.getData();
                    String shareUrl = bundle.getString("shareUrl");
                    String title = bundle.getString("title");
                    String desc = bundle.getString("desc");
                    String thumbnail = bundle.getString("thunbnail");
                    String documentId = bundle.getString("documentid");
                    showSharePop(shareUrl, title, desc, mWebview, thumbnail, documentId);
                    break;
                    default:
                        break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_activity_layout);
        mWebview = (WebView) findViewById(R.id.web_page_live);
        mProgressBar = (ProgressBar) findViewById(R.id.live_progress);
        mJsBridge = new JsBridge(this, mWebview);
        mJsBridge.setOutHandler(handler);
        mJsBridge.setDispatchListener(this);
        pageUrl = getIntent().getStringExtra("url");
        mWebview.addJavascriptInterface(mJsBridge, "grounds");

        mOneKeyShare = new OneKeyShare(this);
        mOneKeyShare.setShareType(Platform.SHARE_WEBPAGE);
        mOneKeyShare.notifyShareCallback = this;

        noNetView = findViewById(R.id.net_check_live);
        noNetView.setVisibility(View.GONE);
        noNetView.setOnClickListener(this);
        initWebView(mWebview);

        mWebview.loadUrl(pageUrl);

//        mOneKeyShare.initShareStatisticsData(adId, echid, chid, "", PageIdConstants.H5);
//        mOneKeyShare.initSmartShareData(UserOperatorConst.TYPE_WEB, "", webTitle);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (mWebview != null) {
            mWebview.loadUrl(pageUrl);
        } else {
            initWebView(mWebview);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mOneKeyShare != null && mOneKeyShare.getPop() != null && mOneKeyShare.getPop().isShowing()) {
                mOneKeyShare.getPop().dismiss();
                return true;
            }

            if (isKeyDown) {
                if (!ScreenUtils.isLand()) {
                    finish();
                } else {
                    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
            isKeyDown = false;
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        isKeyDown = true;
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void dispatch(String type, String url, String category, String errurl, String documentid) {
        runOnUiThread(new ClickRunnable(this, type, url, errurl, documentid, category));
    }

    @Override
    public void dispatch(Map<String, String> paramMaps) {
        runOnUiThread(new BaseLiveActivity.ClickRunnable(paramMaps));

    }

    @Override
    protected void onPause() {
        super.onPause();
        sendPageInfo();
    }

    private void sendPageInfo() {
        pageInfo = SharePreUtils.getInstance().getLivePageJson();
        if (!TextUtils.isEmpty(pageInfo)) {
            PageLiveRecord pageLiveRecord = new Gson().fromJson(pageInfo, PageLiveRecord.class);

            long currentTime = System.currentTimeMillis() / 1000;
            long startTime = SharePreUtils.getInstance().getLivePageStartTime();
            long durTime = currentTime - startTime;

            CommonStatictisListUtils.getInstance().sendPageLive(new PageLiveRecord(pageLiveRecord.getId(), pageLiveRecord.ref, pageLiveRecord.type, durTime + ""));
            SharePreUtils.getInstance().setLivePageStartTime(System.currentTimeMillis() / 1000);
            SharePreUtils.getInstance().setLivePageJson("");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOneKeyShare = null;
        pageInfo = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (mCommentEditFragment != null && !mCommentEditFragment.isHidden()) {
                mCommentEditFragment.dismissAllowingStateLoss();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USER_LOGIN_REQUESTCODE) {
            if (resultCode == RESULT_OK) {
                if (mWebview != null) {
                    setToken(pageUrl);
                    mWebview.loadUrl(pageUrl);
                } else {
                    initWebView(mWebview);
                }
            }
        }
    }

    /**
     * 用于显示评论对话框
     *
     * @param comments 评论
     */
    public void showEditCommentWindow(String comments) {
        if (mCommentEditFragment == null) {
            mCommentEditFragment = new LiveCommentUtil();
        }

        mCommentEditFragment.setJSBridge(mJsBridge);
        mCommentEditFragment.setShowInputMethod(true);
        mCommentEditFragment.setCommends(comments);
        if (!mCommentEditFragment.isAdded() && !this.isFinishing()) {
            mCommentEditFragment.show(getSupportFragmentManager(), "dialog");

        }
    }

    private void showSharePop(String shareUrl, String title, String desc, View clickView, String thumbnail, String documentid) {
        if (!TextUtils.isEmpty(shareUrl)) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            mOneKeyShare.shareH5Live(shareUrl,
                    clickView,
                    title,
                    this,
                    desc,
                    thumbnail,
                    false);

        }
    }


    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {

    }

    public void clearHistory() {
        if (mWebview != null && mWebview.canGoBack()) {
            mWebview.clearHistory();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.net_check_live) {
            refresh();
        }
    }

    public void refresh() {
        if (mWebview == null || pageUrl == null || !NetUtils.isNetAvailable(this)) {
            return;
        }
        mWebview.loadUrl(pageUrl);
    }

    public class ClickRunnable implements Runnable {

        Context context;
        String type;
        String url;
        String errUrl;
        String documentId;
        String category;

        String pageRef;
        String pageTag;

        public ClickRunnable(Context context, String type, String url, String errUrl, String documentId, String category) {
            this.context = context;
            this.type = type;
            this.url = url;
            this.errUrl = errUrl == null ? "http:\\www.ifeng.com" : errUrl;
            this.documentId = documentId;
            this.category = category;
        }

        public ClickRunnable(Map<String, String> maps) {

            this.url = maps.get(JsBridge.PARAM_URL);

            this.type = maps.get(JsBridge.PARAM_TYPE);

            this.pageRef = maps.get(JsBridge.PARAM_REF);

            this.pageTag = maps.get(JsBridge.PARAM_TAG);
        }

        @Override
        public void run() {
            if (TextUtils.isEmpty(url)) {
                return;
            }

            boolean isWeb = "web".equals(type);
            if (!isWeb) {
                url = String.format(H5_LIVE_URl, url);
            }
            sendPageInfo();
            mJsBridge.savePageData(true);
            mWebview.loadUrl(url);

        }

    }

}
