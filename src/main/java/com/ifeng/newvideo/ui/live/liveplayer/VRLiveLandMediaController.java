package com.ifeng.newvideo.ui.live.liveplayer;

import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.live.vr.VRBaseActivity;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.player.IfengMediaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VR直播全屏controller
 * <p/>
 * Created by Vincent on 2014/11/24.
 */
public class VRLiveLandMediaController extends IfengMediaController {
    private static final Logger logger = LoggerFactory.getLogger(VRLiveLandMediaController.class);

    private ImageView mMotionImg;//陀螺仪图标
    private ImageView mGlassesImg;//眼镜图标

    private boolean isSupportGyro = true; //是否支持陀螺仪，默认支持
    private boolean isGyroMode = true;//支持陀螺仪下：是否是陀螺仪状态,true:陀螺仪状态;false：touch状态
    private boolean isGlassesMode = false;//是否是眼镜模式，默认不是

    private boolean isLandscape = true;//是否是横屏
    private int resId = R.drawable.video_vr_gyro_selector;//默认支持陀螺仪的图标id

    public VRLiveLandMediaController(Context context) {
        super(context);
        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        DisplayMetrics disp = mContext.getResources().getDisplayMetrics();
        mWindowWidth = disp.widthPixels;
        mWindowHeight = disp.heightPixels;
        if (!isLandscape) {//如果是竖屏，把宽高互换，转为横屏，为了解决锁屏中点击通知栏进入界面时控制栏显示位置错误（左移）、控制栏不显示（偏移量计算错误，移出屏幕）的问题。bug##9910
            int temp = mWindowWidth;
            mWindowWidth = mWindowHeight;
            mWindowHeight = temp;
        }
        if (context instanceof VRBaseActivity) {
            ((VRBaseActivity) context).setSupportGyro(supportGyro); //设置回调接口
        }
        logger.debug("mWindowWidth={}  mWindowHeight={}", mWindowWidth, mWindowHeight);
    }

    VRBaseActivity.ISupportGyro supportGyro = new VRBaseActivity.ISupportGyro() {

        //是否支持陀螺仪回调
        @Override
        public void supportGyro(boolean isSupport) {
            isSupportGyro = isSupport;
            if (!isSupport) {
                resId = R.drawable.video_vr_gyroscope_btn_no;//没有陀螺仪功能图标id
            }
            logger.debug("是否支持陀螺仪--{}", isSupport);
        }

        @Override
        public void isGyroMode(boolean mode) {
            isGyroMode = mode;
            logger.debug("是否是陀螺仪状态--{}", mode);
        }

        @Override
        public void isGlassesMode(boolean mode) {
            isGlassesMode = mode;
            logger.debug("是否是眼镜模式--{}", mode);
        }
    };

    @Override
    protected View makeControllerView() {
        return ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.live_vr_land_mediacontroller, this);
    }

    private final OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            show();//显示控制栏，重新计时
            switch (v.getId()) {
                case R.id.icon_vr_gyro:
                    if (isGlassesMode) {
                        ToastUtils.getInstance().showShortToast(R.string.start_glass_unclose_gyro);
                        return;
                    }
                    if (getControllerToVideoPlayerListener() != null) {
                        getControllerToVideoPlayerListener().onGyroClick();
                    }
                    break;
                case R.id.icon_vr_glasses:
                    if (getControllerToVideoPlayerListener() != null) {
                        getControllerToVideoPlayerListener().onSwitchAVMode();//todo 先使用这个方法，也可以在该接口中添加新的方法
                    }
                    break;
            }
        }
    };

    @Override
    protected void initControllerView(View v) {
        super.initControllerView(v);
        logger.debug("initControllerView()");
        mMotionImg = (ImageView) v.findViewById(R.id.icon_vr_gyro);
        mMotionImg.setImageResource(resId);
        mMotionImg.setSelected(isGyroMode);
        mMotionImg.setEnabled(isSupportGyro);
        mMotionImg.setOnClickListener(onClickListener);

        mGlassesImg = (ImageView) v.findViewById(R.id.icon_vr_glasses);
        mGlassesImg.setSelected(isGlassesMode);
        mGlassesImg.setOnClickListener(onClickListener);
    }

    /**
     * 设置交互方式图标是否选中
     */
    public void setMotionImageSelected(boolean selected) {
        if (mMotionImg != null) {
            mMotionImg.setSelected(selected);
        }
    }

    /**
     * 设置双眼图标是否选中
     */
    public void setGlassesImageSelected(boolean selected) {
        if (mGlassesImg != null) {
            mGlassesImg.setSelected(selected);
        }
    }

    @Override
    protected void setWindow() {
        int[] anchorPos = new int[2];
        mAnchor.getRootView().getLocationOnScreen(anchorPos);//获取控件在屏幕上位置
        setLayoutParams(new LayoutParams(mAnchor.getRootView().getWidth(), mAnchor.getRootView().getHeight()));
        mRoot.getLayoutParams().height = mAnchor.getRootView().getHeight() / 8;
        mRoot.requestLayout();
        mWindow.setWidth(mWindowWidth);
        mWindow.setHeight(mWindowHeight / 8);
        mWindow.showAtLocation(mAnchor.getRootView(), Gravity.BOTTOM, 0, 0);
    }
}
