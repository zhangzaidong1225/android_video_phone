package com.ifeng.newvideo.ui.live.vr;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.live.liveplayer.VRVideoLandMediaController;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.dao.VideoDao;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.model.HistoryModel;
import com.ifeng.video.dao.db.model.LiveRoomModel;
import com.ifeng.video.player.ChoosePlayerUtils;
import com.ifeng.video.player.PlayState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * VR点播底页
 * Created by Administrator on 2016/11/15.
 */
public class VRVideoActivity extends VRBaseActivity implements View.OnClickListener {
    private static final Logger logger = LoggerFactory.getLogger(VRVideoActivity.class);

    private List<String> videoList = new ArrayList<>();//视频列表

    private TextView mAutoTextView;
    private TextView mSuperTextView;
    private TextView mHighTextView;
    private TextView mStandardTextView;
    private TextView mLowTextView;
    private List<String> mStreams;
    private List<View> mStreamView;

    private boolean isFromHistory;//是否从看过跳转而来
    private String simId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStreams = new ArrayList<>();
        mStreamView = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mobileNetShowing) {//如果显示运营商网络界面，直接返回bug #10026
            return;
        }
        if (mVideoView != null && seekToPosition > 0) {
            mVideoView.seekTo(seekToPosition);
        }
        insertCustomerStatistics(PlayState.STATE_PREPARING);//初始化统计:保证下次上报不被拦截
        if (!isCurrPauseState || OneKeyShare.isLandWebQQ) {//如果离开时不是暂停状态，播放
            OneKeyShare.isLandWebQQ = false;
            if (mVideoController != null && mVideoView != null) {
                mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
                mVideoView.start();
                seekToPosition = 0;
            }
        }
    }

    @Override
    protected void initView() {
        super.initView();
        mAutoTextView = (TextView) findViewById(R.id.video_stream_auto);
        mSuperTextView = (TextView) findViewById(R.id.video_stream_supper);
        mHighTextView = (TextView) findViewById(R.id.video_stream_high);
        mStandardTextView = (TextView) findViewById(R.id.video_stream_standard);
        mLowTextView = (TextView) findViewById(R.id.video_stream_low);

        mAutoTextView.setOnClickListener(this);
        mSuperTextView.setOnClickListener(this);
        mHighTextView.setOnClickListener(this);
        mStandardTextView.setOnClickListener(this);
        mLowTextView.setOnClickListener(this);
    }

    @Override
    protected void initVideoMediaController() {
        mVideoController = new VRVideoLandMediaController(this);
        mVideoController.setOnHiddenListener(this);
        mVideoController.setOnShownListener(this);
        mVideoController.setControllerToVideoPlayerListener(controllerListener);
        mVideoView.setMediaController(mVideoController);
        if (supportGyro != null) {//初始化VRLibrary获取是否支持陀螺仪信息回调到Controller中设置控件状态
            supportGyro.supportGyro(isSupportGyro);
        }
    }

    @Override
    protected void onSelectStreamClick() {
        showAnimation(mSelectStreamLayout);
    }

    @Override
    protected void initData(Intent intent) {
        currentPositionInVideoList = intent.getIntExtra(IntentKey.VR_CURRENT_POSITION_IN_VIDEO_LIST, 0);
        vrEchid = intent.getStringExtra(IntentKey.VR_LIVE_ECHID);
        vrChid = intent.getStringExtra(IntentKey.VR_LIVE_CHID);
        seekToPosition = intent.getLongExtra(IntentKey.VR_SEEKTO_POSITION, 0);
        videoList = intent.getStringArrayListExtra(IntentKey.VR_VIDEO_LIST);
        isFromHistory = intent.getBooleanExtra(IntentKey.VR_VIDEO_IS_FROM_HISTORY, false);
    }

    private VideoItem videoItem;

    @Override
    protected void getPlayData() {
        if (!isSupportVRLive) {//如果手机不支持VR播放,直接返回
            return;
        }
        VideoDao.getVideoInformationById(videoList.get(currentPositionInVideoList), "",
                new Response.Listener<VideoItem>() {
                    @Override
                    public void onResponse(VideoItem response) {
                        videoItem = response;
                        if (!ListUtils.isEmpty(response.videoFiles)) {
                            videoFiles.clear();
                            videoFiles.addAll(response.videoFiles);
                        }
                        initParentData(response);
                        final LiveRoomModel liveRoomModel = getLiveRoomModel(guid);
                        if (liveRoomModel == null) {
                            updateErrorLayer(true);
                            return;
                        }
                        currentModel = liveRoomModel;
                        updateLoadingLayer(true);
                        if (!ListUtils.isEmpty(response.videoFiles)) {
                            setStreams(StreamUtils.getVRAllStream(response.videoFiles));
                        }
                        prepareToPlay();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error instanceof NetworkError) {
                            updateNoNetLayer(true);
                        } else {
                            updateErrorLayer(true);
                        }
                    }
                });
    }

    private List<FileType> videoFiles = new ArrayList<>();

    /**
     * 初始化父类中数据
     */
    private void initParentData(VideoItem response) {
        vrShareUrl = response.mUrl;
        vrTitle = response.title;
        vrImageUrl = response.image;
        guid = response.guid;
        vrChid = response.searchPath;
        weMediaId = response.weMedia == null ? "" : response.weMedia.id;
        weMediaName = response.weMedia == null ? "" : response.weMedia.name;
        simId = response.simId;
        vrLiveUrl = StreamUtils.getVRStreamUrl(videoItem.videoFiles);//获取播放地址
        ((VRVideoLandMediaController) mVideoController).updateStreamText(StreamUtils.getVRStreamType(videoFiles));
        iconShare.setVisibility(TextUtils.isEmpty(vrShareUrl) ? View.GONE : View.VISIBLE);
    }

    @Override
    protected int getVideoType() {
        return TYPE_VIDEO;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.video_stream_auto:
            case R.id.video_stream_supper:
            case R.id.video_stream_high:
            case R.id.video_stream_standard:
            case R.id.video_stream_low:
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DEF_ITEM, null, PageIdConstants.PLAY_VR);
                String stream = ((TextView) v).getText().toString();
                hideAnimation(mSelectStreamLayout);
                if (stream.equals(StreamUtils.getVRStreamType(videoFiles))) {//如果当前码流与选择的码流相同，不处理
                    return;
                }
                saveStreamType(v);
                resetStatus();
                changeStream();
                break;
        }
    }

    /**
     * 更改码流
     */
    private void changeStream() {
        if (isSupportVRLive) {
            isChangeStream = true;
            seekToPosition = getPlayerPosition();
            if (currentModel != null && !ListUtils.isEmpty(videoItem.videoFiles)) {
                String vrStreamUrl = StreamUtils.getVRStreamUrl(videoItem.videoFiles);
                currentModel.setVideo(vrStreamUrl);//设播放地址
                logger.debug("mediaUrl={}", vrStreamUrl);
            }
            prepareToPlay();
        }
    }

    /**
     * 保存码流选择的码流类型
     */
    private void saveStreamType(View view) {
        if (view != null && view instanceof TextView) {
            String stream = ((TextView) view).getText().toString();
            int streamValue = StreamUtils.getStreamValue(stream.trim());
            SharePreUtils.getInstance().setVodCurrentStream(streamValue);
            SharePreUtils.getInstance().setHasUserSelectedVodStream(true);
            ToastUtils.getInstance().showShortToast(stream + "切换中，请稍后...");
            ((VRVideoLandMediaController) mVideoController).updateStreamText(stream);
            logger.debug("stream:{}, streamValue:{}", stream, streamValue);
        }
    }

    /**
     * 设置码流类型
     */
    private void setStreams(List<String> streams) {
        this.mStreams.clear();
        mStreamView.clear();
        this.mStreams.addAll(streams);
        addStreamView();
        resetStatus();
    }

    private int mCurrentStream;

    /**
     * 重置选中状态
     */
    private void resetStatus() {
        int len = mStreams.size();
        mCurrentStream = 0;
        for (int i = 0; i < len; i++) {
            if (mStreams.get(i).equals(StreamUtils.getVRStreamType(videoFiles))) {
                mCurrentStream = i;
                break;
            }
        }
        for (View view : mStreamView) {
            view.setSelected(false);
        }
        if (!ListUtils.isEmpty(mStreamView)) {
            mStreamView.get(mCurrentStream).setSelected(true);
        }
    }

    /**
     * 添加码流类型
     */
    private void addStreamView() {
        if (mStreams.contains(this.getString(R.string.common_video_auto))) {
            mStreamView.add(mAutoTextView);
        }
        if (mStreams.contains(this.getString(R.string.common_video_supper))) {
            mStreamView.add(mSuperTextView);
        }
        if (mStreams.contains(this.getString(R.string.common_video_high))) {
            mStreamView.add(mHighTextView);
        }
        if (mStreams.contains(this.getString(R.string.common_video_mid))) {
            mStreamView.add(mStandardTextView);
        }
        if (mStreams.contains(this.getString(R.string.common_video_low))) {
            mStreamView.add(mLowTextView);
        }
        hideAllStreamView();
        for (View view : mStreamView) {
            view.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 隐藏全部码流View
     */
    private void hideAllStreamView() {
        mAutoTextView.setVisibility(View.GONE);
        mSuperTextView.setVisibility(View.GONE);
        mHighTextView.setVisibility(View.GONE);
        mStandardTextView.setVisibility(View.GONE);
        mLowTextView.setVisibility(View.GONE);
    }

    /**
     * 隐藏动画
     */
    private void hideAnimation(View view) {
        if (view != null && view.getVisibility() == View.VISIBLE) {
            view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_slide_right_out));
            view.setVisibility(View.GONE);
        }
    }

    /**
     * 显示动画
     */
    private void showAnimation(View view) {
        hideController();
        if (view != null && view.getVisibility() == View.GONE) {
            view.setVisibility(View.VISIBLE);
            view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_slide_left_in));
        }
    }

    /**
     * 保存看过记录
     *
     * @param isComplete 是否播放完成
     */
    @Override
    protected void saveHistory(boolean isComplete) {
        if (currentModel != null) {
            if (mVideoView == null || mVideoView.getCurrentPosition() < 1000) {//播放时长小于1秒不保存观看记录
                return;
            }
            HistoryModel model = new HistoryModel();
            model.setProgramGuid(guid);
            model.setColumnName("");
            model.setName(vrTitle);
            model.setType(HistoryModel.TYPE_VR_VOD);
            model.setResource(HistoryModel.RESOURCE_VIDEO);
            model.setImgUrl(vrImageUrl);
            model.setBookMark(isComplete ? (mVideoView.getDuration() * 1000) : mVideoView.getCurrentPosition());
            model.setVideoDuration(mVideoView.getDuration());
            model.setTopicId("");
            model.setTopicMemberType("");
            model.setSimId(simId);
            HistoryDAO.getInstance(this).saveHistoryData(model);
            logger.debug("saveHistory() bookMark={}", mVideoView.getCurrentPosition());
        }
    }

    @Override
    protected void onVideoPlayComplete() {
        currentModel = null;//播放完成，置为空
        seekToPosition = 0;
        saveHistory(true);
        resetDisplayMode();
        SendSmartStatisticUtils.sendPlayOperatorStatistics(VRVideoActivity.this, UserOperatorConst.TYPE_VIDEO, getDuration(), weMediaName, vrTitle);
        if (isFromHistory) {//如果从看过页面过来，播放完成finish
            isPlayComplete = true;
            onBackPressed();
            return;
        }
        currentPositionInVideoList++;
        if (currentPositionInVideoList >= videoList.size()) {//列表播完了，重新开始播
            currentPositionInVideoList = 0;
        }
        getPlayData();
    }

    @Override
    protected void initVRecord() {
        if (videoItem != null) {
            String tag = "";
            String echid = vrEchid;
            String chid = videoItem.searchPath;
            String weMediaId = videoItem.weMedia == null ? "" : videoItem.weMedia.id;
            String weMediaName = videoItem.weMedia == null ? "" : videoItem.weMedia.name;
            String vlen = String.valueOf(videoItem.duration);
            String op = "no";
            String sp = videoItem.cpName;
            mVRecord = new VodRecord(videoItem.guid, videoItem.title, chid, echid, weMediaId, weMediaName,
                    vlen, VodRecord.P_TYPE_RV, op, sp, (VodRecord) mVRecord, tag, ChoosePlayerUtils.useIJKPlayer(VRVideoActivity.this), videoItem.simId, "");
        }
    }

    @Override
    protected boolean isToastChangeStream() {
        return mStreamView != null
                && mStreamView.size() > 1//有大于一个清晰度
                && mCurrentStream < mStreamView.size() - 1;//有比当前清晰度更低的清晰度
    }
}
