package com.ifeng.newvideo.ui.live;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.alibaba.fastjson.JSON;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.live.adapter.LiveProgramAdapter;
import com.ifeng.newvideo.utils.LoadDataTask;
import com.ifeng.newvideo.utils.TimeUtils;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.LiveNetWorkDao;
import com.ifeng.video.dao.db.model.LiveChannelModels;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * 直播播放基頁的fragment
 * Created by Vincent on 2014/11/18.
 */
public class FragmentLiveProgramList extends FragmentBase {
    private static final Logger logger = LoggerFactory.getLogger(FragmentLiveProgramList.class);

    private int day;
    private String params;
    private int isIfeng;

    //private View loading, noNet;
    //private ListView lv;
    private LiveProgramAdapter adapter;
    private NotifyPlayingProgram notifyPlayingProgram;
    private LiveChannelModels channelModels;
    private LiveInfoModel infoModel;
    private NotifyTitle notifyTitle;
    //private TextView tvHintError;
    private static final int USELESS_DAY = -10;
    private final Handler handler = new Handler();

    public FragmentLiveProgramList() {
        super();
    }

    public FragmentLiveProgramList(int day, String params, int isIfeng, NotifyPlayingProgram notifyPlayingProgram, NotifyTitle notifyTitle, Context context) {
        this.day = day;
        this.params = params;
        this.isIfeng = isIfeng;
        this.notifyPlayingProgram = notifyPlayingProgram;
        this.notifyTitle = notifyTitle;
        adapter = new LiveProgramAdapter(context, getDay(), notifyPlayingProgram);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_program_list, null);
        //loading = root.findViewById(R.id.live_loading);
        //noNet = root.findViewById(R.id.live_no_net);
        //tvHintError = (TextView) noNet.findViewById(R.id.tv_nonet);
        //lv = (ListView) root.findViewById(R.id.live_lv_program);

        //noNet.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        getData();
        //    }
        //});
        getData();
        return root;
    }

    public void getData() {
        //boolean isPlayCompleted = ((TVLiveActivity) getActivity()).mVideoView.mCurrentState == PlayState.STATE_PLAYBACK_COMPLETED;
        //if (!((TVLiveActivity) getActivity()).mVideoView.isInPlaybackState() && ((TVLiveActivity) getActivity()).getCorrectUrl() != null || isPlayCompleted) {
        //    ((TVLiveActivity) getActivity()).prepareToPlay();
        //}
        //如果有数据了就不要获取列表数据了
        if (adapter != null && adapter.getCount() > 0) {
            return;
        }
        loading();
        LiveNetWorkDao.getLiveEpgProgramData(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                logger.debug("response=={}", response);
                if (response == null) {
                    dataError();
                    return;
                }
                show();
                try {
                    final LiveChannelModels models = JSON.parseObject(response.toString(), LiveChannelModels.class);
                    //接口 会出现list是空的坑爹情况，加入容错
                    if (models == null || ListUtils.isEmpty(models.getSchedule())) {
                        dataError();
                        return;
                    }
                    final int focus = initProgramState(models);
                    new LoadDataTask(new LoadDataTask.LoadingData() {
                        @Override
                        public void loadDbAchieve() {
                            if (getActivity() == null) {
                                return;
                            }
                            setChannel2UpperCase(models);
                        }

                        @Override
                        public void result() {
                            if (getActivity() == null || channelModels == null) {
                                return;
                            }
                            if (adapter == null) {
                                adapter = new LiveProgramAdapter(getActivity(), getDay(), notifyPlayingProgram);
                            }
                            adapter.setData(programFilter(channelModels.getSchedule()));
                            //lv.setAdapter(adapter);
                            infoModel = ((ActivityLive) getActivity()).getLiveModel();
                            adapter.setModel(infoModel);
                            //lv.setSelection(focus);
                            show();
                            if (isToday(day) && (focus + 1) < channelModels.getSchedule().size()) {
                                if (notifyTitle != null) {
                                    notifyTitle.notifyTitle(channelModels.getSchedule().get(focus).getProgramTitle());
                                }
                            }
                        }
                    }).executed();

                } catch (Exception e) {
                    logger.error("getData error ! {}", e.toString());
                    dataError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error instanceof NetworkError) {
                    noNet();
                } else {
                    dataError();
                }
            }
        }, params, day + "");
    }

    /**
     * 转换为大写
     */
    private void setChannel2UpperCase(LiveChannelModels models) {
        if (models.getChannelId() != null) {
            models.setChannelId(models.getChannelId().toUpperCase());
        }
    }


    private int initProgramState(LiveChannelModels models) {
        Date date = DateUtils.getDateFormat(TimeUtils.getRealTime(System.currentTimeMillis()));
        return LiveUtils.handleLiveSchedule(models, date);
    }

    private void noNet() {
        //loading.setVisibility(View.GONE);
        //noNet.setVisibility(View.VISIBLE);
        //tvHintError.setText(R.string.common_net_useless_try_again);
        //lv.setVisibility(View.GONE);
    }

    private void dataError() {
        //loading.setVisibility(View.GONE);
        //noNet.setVisibility(View.VISIBLE);
        //tvHintError.setText(R.string.common_load_data_error);
        //lv.setVisibility(View.GONE);
    }

    private void loading() {
        //loading.setVisibility(View.VISIBLE);
        //noNet.setVisibility(View.GONE);
        //lv.setVisibility(View.GONE);
    }

    private void show() {
        //loading.setVisibility(View.GONE);
        //noNet.setVisibility(View.GONE);
        //lv.setVisibility(View.VISIBLE);
    }

    public int getDay() {
        return day;
    }

    /**
     * 此接口主要用于通知上层页面当前播放为第几条，及时做状态切换
     */
    public interface NotifyPlayingProgram {
        void playingProgam(LiveChannelModels.LiveChannelModel model, int day, int position);
    }

    public void setPlayingChange(int position) {
        adapter.setPlayingChange(position);
    }

    private boolean isToday(int day) {
        return day == 0 || day == 1;
    }

    public interface NotifyTitle {
        void notifyTitle(String title);
    }

    /**
     * 节目单数据过滤 ，去除()
     *
     * @return
     */
    private List<LiveChannelModels.LiveChannelModel> programFilter(List<LiveChannelModels.LiveChannelModel> data) {
        for (LiveChannelModels.LiveChannelModel model : data) {
            if (model == null || model.getProgramTitle() == null) {
                continue;
            }
            String programTitle = model.getProgramTitle();
            if (programTitle.contains("(")) {
                programTitle = programTitle.substring(0, programTitle.indexOf("("));
                model.setProgramTitle(programTitle);
            }
        }
        return data;
    }
}
