package com.ifeng.newvideo.ui.live;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.constants.PageRefreshConstants;
import com.ifeng.newvideo.ui.live.adapter.LiveChannelGirdAdapter;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.JsonUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.LiveNetWorkDao;
import com.ifeng.video.dao.db.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 直播地方台
 * Created by Vincent on 2014/11/10.
 */
public class FragmentLiveChannel extends FragmentLiveChannelBasic {

    private static final Logger logger = LoggerFactory.getLogger(FragmentLiveChannel.class);
    private Context context;
    private LiveCurrentInfoModels mLiveCurrentInfoModels;
    private List<LiveInfoModel> channelInfos;
    private LiveChannelGirdAdapter channelAdapter;
    private int pageNum;
    private long startTime;
    private final BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetUtils.isNetAvailable(getActivity()) && isFirstRegisterNetworkChangeReceiver) {//从无网到有网的时候,如果没有数据将重新请求
                getData();
            }
            isFirstRegisterNetworkChangeReceiver = true;
        }
    };

    public FragmentLiveChannel() {
    }

    public FragmentLiveChannel(LiveChannelCategory channelCategory, Context context, int pageNum) {
        super(channelCategory);
        this.context = context;
        this.pageNum = pageNum;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        channelAdapter = new LiveChannelGirdAdapter(getActivity());
        if (channelCategory != null) {
            channelAdapter.setEchid(channelCategory.getChannelLid());
        }
        registerNet();
    }

    private void registerNet() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(connectionReceiver, intentFilter);
    }

    /**
     * 获取台标，相关播放信息
     */
    @Override
    public void getData() {
        if (pullToRefreshListView == null || channelCategory == null) {
            return;
        }
        logger.debug("---> in getData pageNum is {}", pageNum);
        LiveNetWorkDao.getLiveChannelInfoData(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                try {

                    LiveChannelInfoModel liveChannelInfoModel = (LiveChannelInfoModel) response;
                    channelInfos = liveChannelInfoModel.getLiveInfo();
                    getCurrentLiveInfo();
                    IfengApplication app = FragmentLiveChannel.this.getApp();
                    for (LiveInfoModel model : channelInfos) {
                        if (model != null && model.getChannelId() != null) {
                            app.setShareUrl(model.getChannelId(), model.getShareUrl());
                        }
                    }
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String cacheString = CommonDao.getCache(DataInterface.getLiveChannelInfo(channelCategory.getChannelLid()));
                if (cacheString != null) {
                    try {
                        LiveChannelInfoModel liveChannelInfoModel = JSON.parseObject(cacheString, LiveChannelInfoModel.class);
                        channelInfos = liveChannelInfoModel.getLiveInfo();
                        getCurrentLiveInfo();
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                    return;
                }
                if (error instanceof NetworkError || !NetUtils.isNetAvailable(getActivity())) {
                    noNet();
                } else {
                    dataError();
                }
//                noNetOrError(channelAdapter.getCount() == 0);
            }
        }, channelCategory.getChannelLid());

    }

    private void noNetOrError(boolean flag) {
        if (flag)
            noNet();
        else
            pullToRefreshListView.onRefreshComplete();
    }

    /**
     * 获取播放列表，卫视直播节目信息（当前在播信息）
     */
    private void getCurrentLiveInfo() {
        LiveNetWorkDao.getLiveCurrentProgram(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                recordPageTime(pageNum);
                show();
                refreshUI(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String cacheString = CommonDao.getCache(DataInterface.getLiveCurrentProgram(channelCategory.getChannelLid(), channelCategory.getIsiFeng() + ""));
                if (!TextUtils.isEmpty(cacheString)) {
                    try {
                        refreshUI(JsonUtils.parseObject(cacheString, LiveCurrentInfoModels.class));
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                    return;
                }

                if (error instanceof NetworkError || (getActivity() != null && !NetUtils.isNetAvailable(getActivity()))) {
                    noNetOrError(channelAdapter.getCount() == 0);
                } else {
                    dataError();
                }
            }
        }, channelCategory.getChannelLid(), channelCategory.getIsiFeng() + "");

    }

    private void refreshUI(Object response) {
        try {
            pullToRefreshListView.onRefreshComplete();
            if (response == null)
                return;
            LiveCurrentInfoModels liveCurrentInfoModels = (LiveCurrentInfoModels) response;
            List<LiveChannelModels> models = liveCurrentInfoModels.getCurrentLiveInfo();
            List<LiveChannelModels> filterModels = programFilter(models);
            if (liveCurrentInfoModels.equals(mLiveCurrentInfoModels)) {
                logger.debug("in childChannel liveCurrentInfoModels.equals(mLiveCurrentInfoModels)");
                return;
            }
            mLiveCurrentInfoModels = liveCurrentInfoModels;
            setChannel2UpperCase();
            channelAdapter.setData(transLateLiveInfo(filterModels));
            pullToRefreshListView.setAdapter(channelAdapter);
            pullToRefreshListView.getRefreshableView().setSelection(0);
        } catch (Exception error) {
            logger.error(error.toString());
        }
    }

    private List<LiveInfoModel> transLateLiveInfo(List<LiveChannelModels> data) {
        for (LiveChannelModels models : data) {
            for (LiveInfoModel liveInfoModel : channelInfos) {
                liveInfoModel.setIsiFeng(channelCategory.getIsiFeng());
                if (models.getChannelName() != null && models.getChannelName().equalsIgnoreCase(liveInfoModel.getcName())
                        && models.getSchedule() != null && !models.getSchedule().isEmpty()) {
                    //此处与IOS他们保持一致，
//                    Util4act.getNeedLiveSchedule(models, DateUtils.getDateFormat(LiveTimeUtil.getCurrentTime()), channelInfos.get(i));
                    if (models.getSchedule().size() == 3 || models.getSchedule().size() > 3) {
                        liveInfoModel.setCurrentProgram(models.getSchedule().get(1));
                        liveInfoModel.setNextProgram(models.getSchedule().get(2));
                    } else if (models.getSchedule().size() == 2) {
                        liveInfoModel.setCurrentProgram(models.getSchedule().get(0));
                        liveInfoModel.setNextProgram(models.getSchedule().get(1));
                    } else {
                        liveInfoModel.setCurrentProgram(models.getSchedule().get(0));
                    }
                    break;
                }
            }
        }
        return channelInfos;
    }

    private List<LiveChannelModels> programFilter(List<LiveChannelModels> data) {
        for (LiveChannelModels models : data) {
            for (LiveChannelModels.LiveChannelModel model : models.getSchedule()) {
                if (model.getProgramTitle() == null)
                    continue;

                String programTitle = model.getProgramTitle();
                if (programTitle.contains("(")) {
                    programTitle = programTitle.substring(0, programTitle.indexOf("("));
                    model.setProgramTitle(programTitle);
                }
            }
        }

        return data;
    }

    /**
     * 转换为大写
     */
    private void setChannel2UpperCase() {
        for (LiveInfoModel infoModel : channelInfos) {
            if (infoModel.getChannelId() != null) {
                infoModel.setChannelId(infoModel.getChannelId().toUpperCase());
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (connectionReceiver != null) {
            getActivity().unregisterReceiver(connectionReceiver);
        }
    }

    private void recordPageTime(int pageNum) {
        if (getActivity() == null) return;
        startTime = System.currentTimeMillis();
        logger.debug("---> in recordPageTime  pageNum is {} ,startTime is {} ", pageNum, startTime);
        SharePreUtils.getInstance().setPageData(PageRefreshConstants.CATEGORY_DOC, pageNum, startTime);
    }

    public void readPageTime(int pageNum) {
        long endTime = System.currentTimeMillis();
        startTime = SharePreUtils.getInstance().getPageData(PageRefreshConstants.CATEGORY_DOC, pageNum);
        logger.debug("---> in readPageTime pageNum is {} ,startTime is {}, endTime is {}, endTime-startTime is {}", pageNum, startTime, endTime, endTime - startTime);
        if ((endTime - startTime) > PageRefreshConstants.REFRESH_DATA_INTERVAL) {
            getData();
        }
    }
}
