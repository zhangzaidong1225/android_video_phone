package com.ifeng.newvideo.ui.live.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.dao.db.model.live.TVLiveProgramInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 电视台节目列表适配器
 * Created by Administrator on 2016/8/3.
 */
public class TVLiveProgramAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<TVLiveProgramInfo.ScheduleEntity> list = new ArrayList<>();

    public TVLiveProgramAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.adapter_tv_live_program_item_layout, viewGroup, false);
            holder.initView(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.program_start_time.setText(subTime(list.get(i).getStartTime()));
        holder.program_title.setText(subTitle(list.get(i).getProgramTitle()));

        if (list.get(i).getState() == TVLiveProgramInfo.ScheduleEntity.END) {//已结束
            holder.program_start_time.setTextColor(context.getResources().getColor(R.color.tv_end_text_color));
            holder.program_title.setTextColor(context.getResources().getColor(R.color.tv_end_text_color));
            holder.program_state.setVisibility(View.INVISIBLE);
        } else if (list.get(i).getState() == TVLiveProgramInfo.ScheduleEntity.LIVING) {//直播中
            holder.program_start_time.setTextColor(context.getResources().getColor(R.color.tv_living_text_color));
            holder.program_title.setTextColor(context.getResources().getColor(R.color.tv_living_text_color));
            holder.program_state.setVisibility(View.VISIBLE);
            if (livingProgramInfo != null) {
                livingProgramInfo.getLivingProgramInfo(list.get(i));
            }
        } else { //预告
            holder.program_start_time.setTextColor(context.getResources().getColor(R.color.tv_notice_text_color));
            holder.program_title.setTextColor(context.getResources().getColor(R.color.tv_notice_text_color));
            holder.program_state.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    /**
     * 截取时间，格式：HH:mm
     */
    private String subTime(String time) {
        if (time.contains(" "))
            return time.substring(time.indexOf(" "), time.length() - 3);
        else
            return time;
    }

    /**
     * 格式化标题，去除()
     */
    private String subTitle(String title) {
        if (title.contains("（")) {
            return title.substring(0, title.indexOf("（"));
        } else if (title.contains("(")) {
            return title.substring(0, title.indexOf("("));
        } else {
            return title;
        }
    }

    public void setList(List<TVLiveProgramInfo.ScheduleEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView program_start_time;
        TextView program_title;
        TextView program_state;

        private void initView(View view) {
            program_start_time = (TextView) view.findViewById(R.id.program_start_time);
            program_title = (TextView) view.findViewById(R.id.program_title);
            program_state = (TextView) view.findViewById(R.id.program_state);
        }
    }

    private GetLivingProgram livingProgramInfo;

    /**
     * 获取正在直播的节目信息
     */
    public interface GetLivingProgram {
        void getLivingProgramInfo(TVLiveProgramInfo.ScheduleEntity entity);
    }

    public void setLivingProgramInfo(GetLivingProgram livingProgramInfo) {
        this.livingProgramInfo = livingProgramInfo;
    }
}
