package com.ifeng.newvideo.ui.live.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 直播各个台listView 的Adapter
 * Created by Vincent on 2014/11/18.
 */
public class LiveChannelGirdAdapter extends BaseAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LiveChannelGirdAdapter.class);
    private static final int TYPE_ITEM_LEFT = 0;
    private static final int TYPE_ITEM_RIGHT = 1;
    private static final int TYPE_ITEM_TOTAL = 2;
    private List<LiveInfoModel> data;
    private static Context context;
    private static LinearLayout.LayoutParams contianerParams;
    private String echid;

    public LiveChannelGirdAdapter(Context context) {
        LiveChannelGirdAdapter.context = context;
    }

    public void setData(List<LiveInfoModel> data) {
        this.data = data;
    }

    public void setEchid(String echid) {
        this.echid = echid;
    }

    @Override
    public int getCount() {
        if (data != null)
            return data.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LiveChannelViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            switch (type) {
                case TYPE_ITEM_LEFT:
                    convertView = LayoutInflater.from(context).inflate(R.layout.live_channel_left_item, null);
                    holder = new LiveChannelViewHolder();
                    break;
                case TYPE_ITEM_RIGHT:
                    convertView = LayoutInflater.from(context).inflate(R.layout.live_channel_right_item, null);
                    holder = new LiveChannelViewHolder();
                    break;
                default:
                    break;

            }

            holder.initView(convertView);
            convertView.setTag(holder);
        }
        holder = (LiveChannelViewHolder) convertView.getTag();
        if (position == 0 || position == 1) {
            holder.grayBar.setVisibility(View.VISIBLE);
        } else {
            holder.grayBar.setVisibility(View.GONE);
        }
        holder.bindData(data.get(position));
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        if ((position + 1) % 2 == 1) {
            return TYPE_ITEM_LEFT;
        } else {
            return TYPE_ITEM_RIGHT;
        }
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_ITEM_TOTAL;
    }

    public class LiveChannelViewHolder {
        LinearLayout contianer;
        NetworkImageView imgBg;
        TextView tvTitle, tvProgram, tvNextProgram;
        View itemView, grayBar;

        public void initView(View view) {
            contianer = (LinearLayout) view.findViewById(R.id.live_channel);
            imgBg = (NetworkImageView) view.findViewById(R.id.live_channel_img);
            tvTitle = (TextView) view.findViewById(R.id.live_channel_title);
            tvProgram = (TextView) view.findViewById(R.id.live_grid_program);
            tvNextProgram = (TextView) view.findViewById(R.id.live_grid_program_info);
            grayBar = view.findViewById(R.id.live_item_gray_bar);
            itemView = view;
            if (contianerParams == null) {
                int width = (DisplayUtils.getWindowWidth() - DisplayUtils.convertDipToPixel(28)) / 2;
                int height = (int) (width * 0.75);
                contianerParams = new LinearLayout.LayoutParams(width, height);
            }
            contianer.setLayoutParams(contianerParams);
        }


        private void bindData(final LiveInfoModel model) {
            if (model != null) {
                setMessge(tvTitle, model.getcName());
                if (model.getCurrentProgram() != null) {
                    setMessge(tvProgram, model.getCurrentProgram().getProgramTitle());
                } else {
                    setMessge(tvProgram, "暂无节目");
                }
                if (model.getNextProgram() != null) {
                    String startTime = model.getNextProgram().getStartTime();
                    startTime = startTime.substring(startTime.indexOf(" "), startTime.length() - 3);
                    setMessge(tvNextProgram, startTime + " " + model.getNextProgram().getProgramTitle());
                } else {
                    setMessge(tvNextProgram, "暂无节目");
                }
                load140Image(model, imgBg);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        model.setIsiFeng(model.getChannelId() != null ? 1 : 0);
                        IntentUtils.toLiveDetailActivity(context, model, echid);
                    }
                });
            }
        }

        private void load140Image(LiveInfoModel model, NetworkImageView itemImage) {
            itemImage.setImageUrl(model.getBkgURL(), VolleyHelper.getImageLoader());
            itemImage.setDefaultImageResId(R.drawable.live_default_bg);
            itemImage.setErrorImageResId(R.drawable.live_default_bg);
        }

        void setMessge(TextView tv, String str) {
            if (tv != null && !TextUtils.isEmpty(str)) {
                tv.setText(str);
            }
        }

    }

}
