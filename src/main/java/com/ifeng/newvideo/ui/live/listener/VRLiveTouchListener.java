package com.ifeng.newvideo.ui.live.listener;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.ifeng.newvideo.ui.live.vr.VRLiveActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VR直播页面onTouch监听
 * Created by Vincent on 2014/12/9.
 */
public class VRLiveTouchListener {

    private static final Logger logger = LoggerFactory.getLogger(VRLiveTouchListener.class);

    private final VRLiveActivity mActivity;

    public VRLiveTouchListener(VRLiveActivity activity) {
        this.mActivity = activity;
        GestureDetector.OnGestureListener onGestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                scaleVideoView();
                return super.onDoubleTap(e);
            }
        };
        gestureDetector = new GestureDetector(activity, onGestureListener);
    }

    private final GestureDetector gestureDetector;


    public boolean onTouchEvent(MotionEvent event) {
        return mActivity.mVRLibrary.handleTouchEvent(event);

    }

    private void scaleVideoView() {
        //TODO touch事件的大小缩放

    }
}
