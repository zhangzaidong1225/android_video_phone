package com.ifeng.newvideo.ui.live.weblive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.maintab.LiveData;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerLive;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.widget.BaseWebViewClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class FragmentWebLive extends FragmentBase implements JsBridge.JSDispatchListener {

    private static final Logger logger = LoggerFactory.getLogger(FragmentWebLive.class);

    private View mContentView;
    private WebView mWebView;
    private ProgressBar mProgressBar;
    private View noNetView;
    private ImageView iv_ifeng_tv;
    private JsBridge mJsBridge;
    private LiveData liveData;//直播页需要的数据，包括channelId,echid,
    private long startTime;
    private long endTime;
    private long durTime;
    private boolean isHidden = false;

    private static final String H5_LIVE_HOMEPAGE = "http://izhibo.ifeng.com/home.html#live_jx";
    private static final String H5_LIVE_URl = "http://izhibo.ifeng.com/live.html?liveid=%s";


    private Handler mHanlder = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    noNetView.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    noNetView.setVisibility(View.GONE);
                    mWebView.reload();
                    break;
            }
        }
    };
    private ActivityMainTab mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ActivityMainTab) {
            mActivity = (ActivityMainTab) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.web_live_layout, container, false);
        initView();

        initWebSetting();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        mJsBridge = new JsBridge(getActivity(), mWebView);
        mJsBridge.setDispatchListener(this);
        mWebView.addJavascriptInterface(mJsBridge, "grounds");
        mWebView.setWebViewClient(new LiveWebViewClient());
        mWebView.setWebChromeClient(new LiveWebChromeClient());

        mWebView.clearCache(true);
        mWebView.clearHistory();

        setToken(H5_LIVE_HOMEPAGE);

        startTime = System.currentTimeMillis() / 1000;
        SharePreUtils.getInstance().setLivePageStartTime(startTime);

        mWebView.loadUrl(H5_LIVE_HOMEPAGE);

        return mContentView;
    }


    private void initView() {
        mWebView = (WebView) mContentView.findViewById(R.id.web_page_live);
        mProgressBar = (ProgressBar) mContentView.findViewById(R.id.web_page_progress_live);
        iv_ifeng_tv = (ImageView) mContentView.findViewById(R.id.iv_ifeng_tv);
        iv_ifeng_tv.setVisibility(View.GONE);
        iv_ifeng_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.LIVE_TV, PageIdConstants.PAGE_LIVE);
                Intent intent = new Intent(getActivity(), ActivityVideoPlayerLive.class);
                intent.putExtra("liveData", liveData);
                startActivity(intent);
            }
        });
        noNetView = mContentView.findViewById(R.id.net_check_live);
        noNetView.setVisibility(View.GONE);
        noNetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHanlder.sendEmptyMessage(1);
            }
        });
    }

    private void setToken(String pageUrl) {
        String mToken = "";
        if (User.isLogin()) {
            mToken = new User(getActivity()).getIfengToken();
        }

        if (!TextUtils.isEmpty(mToken)) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setCookie(pageUrl, "sid=" + mToken + "; Domain=.ifeng.com" + "; path=/");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.flush();
            } else {
                CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(getActivity());
                cookieSyncManager.startSync();
                cookieSyncManager.sync();
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        liveData = mActivity.getLiveData();//取直播页数据
    }

    private void initWebSetting() {
        if (mWebView == null || getActivity() == null) return;
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(true);
        //适应屏幕，内容将自动缩放
        webSettings.setTextZoom(100);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setAppCachePath(getActivity().getApplicationContext().getCacheDir().getAbsolutePath());
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);

        webSettings.setDatabaseEnabled(true);
        // 启用地理定位
        webSettings.setGeolocationEnabled(true);
        try {
            String dir = getActivity().getApplicationContext()
                    .getDir("database", Context.MODE_PRIVATE).getPath();
            // 设置定位的数据库路径
            mWebView.getSettings().setGeolocationDatabasePath(dir);
        } catch (Exception e) {
        }

        webSettings.setDomStorageEnabled(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(
                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
    }

    @Override
    public void dispatch(String type, String url, String category, String errurl, String documentid) {
        logger.debug("jsbpage_dispatch:{}", " type:" + type + " url:" + url + " category:" + category
                + " errurl:" + errurl + " documentid:" + documentid);
        String realUrl = null;
        Intent intent = null;
        boolean isWebType = "web".equals(type);
        if (isWebType) {
            realUrl = url;
            boolean isColumns = realUrl.contains("columns");
            if (isColumns) {
                intent = new Intent(getActivity(), ActivityLiveAllColumn.class);
            } else {
                intent = new Intent(getActivity(), ActivityLiveColumn.class);
            }
            mJsBridge.savePageData(true);
            intent.putExtra("url", realUrl);
            getActivity().startActivity(intent);
        } else {
            realUrl = String.format(H5_LIVE_URl, url);
            mJsBridge.savePageData(true);
            intent = new Intent(getActivity(), ActivityH5Live.class);
            intent.putExtra("url", realUrl);
            startActivity(intent);
        }


    }

    @Override
    public void dispatch(Map<String, String> paramMaps) {
        Log.d("dispatch", "paramMaps");
    }

    private class LiveWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int progress) {

            if (progress < 100 && mProgressBar.getVisibility() == ProgressBar.GONE) {
                mProgressBar.setVisibility(ProgressBar.VISIBLE);
            }
            mProgressBar.setProgress(progress);

            if (progress >= 80) {
                mProgressBar.setVisibility(ProgressBar.GONE);
                iv_ifeng_tv.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            if (callback != null) {
                callback.invoke(origin, true, false);
            }
            super.onGeolocationPermissionsShowPrompt(origin, callback);
        }
    }

    private class LiveWebViewClient extends BaseWebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            view.stopLoading();
            view.clearView();
            mHanlder.sendEmptyMessage(0);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
        startTime = System.currentTimeMillis() / 1000;
        SharePreUtils.getInstance().setLivePageStartTime(startTime);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isHidden = hidden;
        if (hidden) {
            endTime = System.currentTimeMillis() / 1000;
            durTime = endTime - startTime;
            sendPageStartTime();
        } else {
            startTime = System.currentTimeMillis() / 1000;
            SharePreUtils.getInstance().setLivePageStartTime(startTime);
        }
    }

    //2017-08-16+16:24:07#page#id=live_jx$title=$type=live$ref=live$tag=$dur=10.58201694488525
    private void sendPageStartTime() {
        String pageId = mJsBridge.getPageStat();
        if (TextUtils.isEmpty(pageId)) {
            pageId = "live_jx";
        }
        PageLiveRecord pageLiveRecord = new PageLiveRecord(pageId, "live", "live", durTime + "");
        CommonStatictisListUtils.getInstance().sendPageLive(pageLiveRecord);
        SharePreUtils.getInstance().setLivePageStartTime(0);

    }

    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
        logger.debug("liveHidden:{}", "onPause");
        if (!isHidden) {
            endTime = System.currentTimeMillis() / 1000;
            durTime = endTime - startTime;
            logger.debug("liveHidden:{}", "onPause" + durTime);
            sendPageStartTime();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHanlder.removeCallbacksAndMessages(null);
        if (mWebView != null) {
            mWebView.removeAllViews();
            mWebView.destroy();
            mWebView = null;
        }
    }


}
