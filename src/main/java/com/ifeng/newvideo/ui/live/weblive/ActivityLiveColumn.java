package com.ifeng.newvideo.ui.live.weblive;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.google.gson.Gson;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.ui.basic.BaseLiveActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.SharePreUtils;

/**
 * http://izhibo.ifeng.com/column.html?id=155&refer=live_jx&webkit=1&isFull=0&mode=daycategory:errurl:documentid:
 */
public class ActivityLiveColumn extends BaseLiveActivity {

    private static String pageInfo = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_activity_layout);
        mWebview = (WebView) findViewById(R.id.web_page_live);
        mProgressBar = (ProgressBar) findViewById(R.id.live_progress);
        mJsBridge = new JsBridge(this, mWebview);
        mJsBridge.setDispatchListener(this);
        mWebview.addJavascriptInterface(mJsBridge, "grounds");
        pageUrl = getIntent().getStringExtra("url");
        initWebView(mWebview);

        noNetView = this.findViewById(R.id.net_check_live);
        noNetView.setVisibility(View.GONE);
        noNetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebview.reload();
            }
        });

        mWebview.loadUrl(pageUrl);

    }

    @Override
    protected void onResume() {
        super.onResume();
        long startTime = System.currentTimeMillis() / 1000;
        SharePreUtils.getInstance().setLivePageStartTime(startTime);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sendPageInfo();
    }

    private void sendPageInfo() {
        if (TextUtils.isEmpty(pageInfo)) {
            pageInfo = SharePreUtils.getInstance().getLivePageJson();
        }
        if (!TextUtils.isEmpty(pageInfo)) {
            PageLiveRecord pageLiveRecord = new Gson().fromJson(pageInfo, PageLiveRecord.class);

            long currentTime = System.currentTimeMillis() / 1000;
            long startTime = SharePreUtils.getInstance().getLivePageStartTime();
            long durTime = currentTime - startTime;

            CommonStatictisListUtils.getInstance().sendPageLive(new PageLiveRecord(pageLiveRecord.getId(), pageLiveRecord.ref, pageLiveRecord.type, durTime + ""));
            SharePreUtils.getInstance().setLivePageStartTime(System.currentTimeMillis() / 1000);
            SharePreUtils.getInstance().setLivePageJson("");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pageInfo = null;
    }

}
