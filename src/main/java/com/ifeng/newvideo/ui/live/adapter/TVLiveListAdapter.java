package com.ifeng.newvideo.ui.live.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 电视直播列表适配器
 * Created by Administrator on 2016/7/19.
 */
public class TVLiveListAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(TVLiveListAdapter.class);

    private LayoutInflater layoutInflater;
    private List<TVLiveInfo> list = new ArrayList<>();
    private Context context;

    public TVLiveListAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        OnClick onClick = null;
        if (view == null) {
            holder = new ViewHolder();
            onClick = new OnClick();
            view = layoutInflater.inflate(R.layout.adapter_tv_live_list_item_layout, viewGroup, false);
            holder.initView(view);
            view.setTag(holder);
            view.setTag(R.id.view_parent, onClick);
        } else {
            holder = (ViewHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }

        onClick.setPosition(i);
        onClick.setTextView(holder.tvNoProgram);
        holder.itemView.setOnClickListener(onClick);
        holder.tvIcon.setImageUrl(list.get(i).getImg490_490(), VolleyHelper.getImageLoader());
        holder.tvIcon.setDefaultImageResId(R.drawable.icon_default_tv_live);
        holder.tvIcon.setErrorImageResId(R.drawable.icon_default_tv_live);
        holder.tvName.setText(list.get(i).getcName());
        List<TVLiveInfo.Schedule> schedules = list.get(i).getSchedule();//节目数据
        if (null != schedules && schedules.size() == 3) {//上一个节目，播放中，下一个节目
            handleThreeProgram(holder, schedules);
        } else if (null != schedules && schedules.size() == 2) {
            handleTwoProgram(holder, schedules);
        } else if (null != schedules && schedules.size() == 1) {
            handleOneProgram(holder, schedules);
        } else {
            handleNoProgram(holder);
        }
        return view;
    }

    public void setData(List<TVLiveInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    /**
     * 处理返回三档节目
     */
    private void handleThreeProgram(ViewHolder holder, List<TVLiveInfo.Schedule> schedules) {
        TVLiveInfo.Schedule twoSchedule = schedules.get(1);//第二档节目     //不考虑第一档节目
        TVLiveInfo.Schedule threeSchedule = schedules.get(2);//第三档节目
        if (isProgramInToday(twoSchedule.getStartTime(), twoSchedule.getEndTime())
                && isProgramInToday(threeSchedule.getStartTime(), threeSchedule.getEndTime())) {//后两档节目都在今天
            if (programIsLiving(twoSchedule.getStartTime(), twoSchedule.getEndTime())) {//第二档节目在直播中
                setShowLayout(LIVING_AND_NEXT, holder);
                holder.tvLivingTitle.setText(subProgramTitle(twoSchedule.getProgramTitle()));//正在播放节目
                holder.tvSoonLiveTitle.setText(subProgramTitle(threeSchedule.getProgramTitle()));//下一个节目
                holder.tvSoonLive.setText(subTime(threeSchedule.getStartTime()));//下一个节目开始时间
            } else {//第二档节目不在直播中
                if (programIsLiving(threeSchedule.getStartTime(), threeSchedule.getEndTime())) {//第三档节目在直播中
                    setShowLayout(LIVING_NO_NEXT, holder);
                    holder.tvLivingTitle.setText(subProgramTitle(threeSchedule.getProgramTitle()));
                } else {//都不在直播中
                    setShowLayout(NO_PROGRAM, holder);
                }
            }
        } else if (isProgramInToday(twoSchedule.getStartTime(), twoSchedule.getEndTime())
                && !isProgramInToday(threeSchedule.getStartTime(), threeSchedule.getEndTime())) {//第二档节目在今天,第三档节目不在今天
            if (programIsLiving(twoSchedule.getStartTime(), twoSchedule.getEndTime())) {//第二档直播中
                setShowLayout(LIVING_NO_NEXT, holder);
                holder.tvLivingTitle.setText(subProgramTitle(twoSchedule.getProgramTitle()));
            } else {//第二档不在直播中
                setShowLayout(NO_PROGRAM, holder);
            }
        } else if (!isProgramInToday(twoSchedule.getStartTime(), twoSchedule.getEndTime())
                && isProgramInToday(threeSchedule.getStartTime(), threeSchedule.getEndTime())) {//第二档节目不在今天,第三档节目在今天
            if (programIsLiving(threeSchedule.getStartTime(), threeSchedule.getEndTime())) {//第三档直播中
                setShowLayout(LIVING_NO_NEXT, holder);
                holder.tvLivingTitle.setText(subProgramTitle(threeSchedule.getProgramTitle()));
            } else {//第三档不在直播中
                setShowLayout(NO_PROGRAM, holder);
            }
        } else {//第二档、第三档都不在今天
            setShowLayout(NO_PROGRAM, holder);
        }
    }

    /**
     * 处理返回二档节目
     */
    private void handleTwoProgram(ViewHolder holder, List<TVLiveInfo.Schedule> schedules) {
        TVLiveInfo.Schedule oneSchedule = schedules.get(0);
        TVLiveInfo.Schedule twoSchedule = schedules.get(1);
        if (isProgramInToday(oneSchedule.getStartTime(), oneSchedule.getEndTime())
                && isProgramInToday(twoSchedule.getStartTime(), twoSchedule.getEndTime())) {//两档节目都在今天
            if (programIsLiving(oneSchedule.getStartTime(), oneSchedule.getEndTime())) {//第一个在直播中
                setShowLayout(LIVING_AND_NEXT, holder);
                holder.tvLivingTitle.setText(subProgramTitle(oneSchedule.getProgramTitle()));//正在播放节目
                holder.tvSoonLiveTitle.setText(subProgramTitle(twoSchedule.getProgramTitle()));//下一个节目
                holder.tvSoonLive.setText(subTime(twoSchedule.getStartTime()));//下一个节目开始时间
            } else {//第一个不在直播中
                if (programIsLiving(twoSchedule.getStartTime(), twoSchedule.getEndTime())) {//第二个在直播中
                    setShowLayout(LIVING_NO_NEXT, holder);
                    holder.tvLivingTitle.setText(subProgramTitle(twoSchedule.getProgramTitle()));
                } else {//都不在直播中
                    setShowLayout(NO_PROGRAM, holder);
                }
            }
        } else if (isProgramInToday(oneSchedule.getStartTime(), oneSchedule.getEndTime())
                && !isProgramInToday(twoSchedule.getStartTime(), twoSchedule.getEndTime())) {//一档在今天,一档在明天
            if (programIsLiving(oneSchedule.getStartTime(), oneSchedule.getEndTime())) {//今天的在直播中
                setShowLayout(LIVING_NO_NEXT, holder);
                holder.tvLivingTitle.setText(subProgramTitle(oneSchedule.getProgramTitle()));
            } else {//今天的不在直播中
                setShowLayout(NO_PROGRAM, holder);
            }
        }
    }

    /**
     * 处理返回一档节目
     */

    private void handleOneProgram(ViewHolder holder, List<TVLiveInfo.Schedule> schedules) {
        TVLiveInfo.Schedule schedule = schedules.get(0);
        if (isProgramInToday(schedule.getStartTime(), schedule.getEndTime()) //该档节目在今天
                && programIsLiving(schedule.getStartTime(), schedule.getEndTime())) {//并且有正在播放的节目
            setShowLayout(LIVING_NO_NEXT, holder);
            holder.tvLivingTitle.setText(subProgramTitle(schedule.getProgramTitle()));
        } else {//该档节目不在今天
            setShowLayout(NO_PROGRAM, holder);
        }
    }

    /**
     * 处理没有返回节目
     */
    private void handleNoProgram(ViewHolder holder) {
        setShowLayout(NO_PROGRAM, holder);
    }

    /**
     * 去除()
     */
    private String subProgramTitle(String programTitle) {
        if (programTitle.contains("(")) {
            return programTitle.substring(0, programTitle.indexOf("("));
        } else {
            return programTitle;
        }
    }

    /**
     * 规则化时间 00:00
     */
    private String subTime(String startTime) {
        return startTime.substring(startTime.indexOf(" ") + 1, startTime.length() - 3);
    }

    /**
     * 判断节目是否在今天
     */
    private boolean isProgramInToday(String startDate, String endDate) {
        Date currentDate = getDateFormat(System.currentTimeMillis());//当前日期
        Date start;//节目开始日期
        Date end;
        try {
            start = DateUtils.yymmdd.parse(startDate);
            end = DateUtils.yymmdd.parse(endDate);
            return !start.after(currentDate) && !end.before(currentDate);//节目开始日期不在今天日期后面,不在今天日期前面
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 判断节目是否在直播中
     *
     * @param startTime 节目开始时间
     * @param endTime   节目结束时间
     */
    private boolean programIsLiving(String startTime, String endTime) {
        Date currentTime = DateUtils.getDateFormat(System.currentTimeMillis());//当前时间
        Date start;
        Date end;
        try {
            start = DateUtils.hhmmssFormat_One.parse(startTime);
            end = DateUtils.hhmmssFormat_One.parse(endTime);
            return !start.after(end)
                    && start.before(currentTime)
                    && end.after(currentTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取yyyy-MM-dd格式的日期 24小时制
     *
     * @return
     */
    public static Date getDateFormat(long time) {
        Date currentTime = new Date(time);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);

        try {
            return formatter.parse(dateString);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return currentTime;
    }

    static class ViewHolder {
        View itemView;
        NetworkImageView tvIcon;//电视台图标
        TitleTextView tvName;//电视台名称
        TextView tvLiving;//直播中
        TextView tvLivingTitle;//直播中节目
        TextView tvSoonLive;//即将播放的时间
        TextView tvSoonLiveTitle;//即将播放的节目
        TextView tvNoProgram;//暂无节目
        RelativeLayout rlLiveState;//正在播放、即将播放布局

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvIcon = (NetworkImageView) view.findViewById(R.id.tv_icon);
            tvName = (TitleTextView) view.findViewById(R.id.tv_name);
            tvLiving = (TextView) view.findViewById(R.id.tv_living);
            tvLivingTitle = (TextView) view.findViewById(R.id.tv_living_title);
            tvSoonLive = (TextView) view.findViewById(R.id.tv_soon_live);
            tvSoonLiveTitle = (TextView) view.findViewById(R.id.tv_soon_live_title);
            tvNoProgram = (TextView) view.findViewById(R.id.tv_no_program);
            rlLiveState = (RelativeLayout) view.findViewById(R.id.rl_live_state);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;
        private TextView textView;

        public void setPosition(int position) {
            this.position = position;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }

        @Override
        public void onClick(View view) {
            IntentUtils.startActivityTVLive(context, list.get(position).getChannelId(), "", "", "", "", false);
        }
    }

    private static final int LIVING_AND_NEXT = 0;//直播中、下一个
    private static final int LIVING_NO_NEXT = 1;//直播中
    private static final int NO_PROGRAM = 2;//暂无节目

    private void setShowLayout(int status, ViewHolder holder) {
        switch (status) {
            case LIVING_AND_NEXT:
                holder.tvLiving.setVisibility(View.VISIBLE);
                holder.tvLivingTitle.setVisibility(View.VISIBLE);
                holder.tvSoonLiveTitle.setVisibility(View.VISIBLE);
                holder.tvSoonLive.setVisibility(View.VISIBLE);
                holder.tvNoProgram.setVisibility(View.GONE);
                break;
            case LIVING_NO_NEXT:
                holder.tvLiving.setVisibility(View.VISIBLE);
                holder.tvLivingTitle.setVisibility(View.VISIBLE);
                holder.tvSoonLiveTitle.setVisibility(View.GONE);
                holder.tvSoonLive.setVisibility(View.GONE);
                holder.tvNoProgram.setVisibility(View.GONE);
                break;
            case NO_PROGRAM:
                holder.tvLiving.setVisibility(View.GONE);
                holder.tvLivingTitle.setVisibility(View.GONE);
                holder.tvSoonLive.setVisibility(View.GONE);
                holder.tvSoonLiveTitle.setVisibility(View.GONE);
                holder.tvNoProgram.setVisibility(View.VISIBLE);
            default:
                break;
        }
    }
}
