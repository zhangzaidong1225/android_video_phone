package com.ifeng.newvideo.ui.live.weblive;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import cn.sharesdk.framework.Platform;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.login.activity.LoginMainActivity;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.push.AlarmReceiver;
import com.ifeng.newvideo.push.PushReceiver;
import com.ifeng.newvideo.statistics.domains.ActionConvertRecord;
import com.ifeng.newvideo.statistics.domains.ActionLiveRecord;
import com.ifeng.newvideo.statistics.domains.LiveExposeRecord;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.basic.BaseLiveActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.NetUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsBridge implements Handler.Callback {

    private static final Logger logger = LoggerFactory.getLogger(JsBridge.class);

    // 跳转页ID或url.
    public static final String PARAM_URL = "url";

    // 跳转类型.
    public static final String PARAM_TYPE = "type";

    //当前所在web页ID，跳转成功后的page统计以它作为ref参数.
    public static final String PARAM_REF = "ref";

    // 跳转标签，统计使用.
    public static final String PARAM_TAG = "tag";
    private final String TAG = "JsBridge";


    private Activity mContext;
    private WebView mWebView;
    private HashMap<String, String> mCallBacks;
    private Handler mainHandler;
    /**
     * 横向
     */
    private int HORIZONTAL_FLAG = 1;
    /**
     * 纵向
     */
    private int VERTICAL_FLAG = 0;

    private int screenState = -1;
    private OneKeyShare mOnekeyShare;

    public JsBridge(Activity mContext, WebView mWebView) {
        this.mContext = mContext;
        this.mWebView = mWebView;
        this.mainHandler = new Handler(this);
        mCallBacks = new HashMap<String, String>();
        mOnekeyShare = new OneKeyShare(mContext);
        mOnekeyShare.setShareType(Platform.SHARE_WEBPAGE);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case JSCommend.DEVICE_INFO_CODE:
                String deviceInfoCallback = (String) msg.obj;
                setDeviceInfo(deviceInfoCallback);
                break;
            case JSCommend.MAXIMIZATION_CODE:
                hiddenTitleBar();
                break;
            case JSCommend.EXIT_MAXIMIZATION_CODE:
                //TODO
                break;
            case JSCommend.NETWORK_CHANGE_CODE:
                setNetWorkType();
                break;
            case JSCommend.EXIT_CODE:
                //TODO
                break;
            case JSCommend.ACTIVE_CODE:
                //TODO
                break;
            case JSCommend.DESTORY_CODE:
                //TODO
                break;
            case JSCommend.ROTATE_CODE:
                int screenFlag = msg.arg1;
                switchScreenState(screenFlag);
                break;
            case JSCommend.USER_LOGIN_CODE:
                performUserLogin();
                break;
            case JSCommend.OPEN_SHARE_CODE:
                //TODO
                break;
            case JSCommend.DISPATH_CODE:
                //TODO
                break;
            case JSCommend.CLOSE_CODE:
                mContext.finish();
                break;
            case JSCommend.USER_BIND_CODE:
                //performUserBind();
                break;
            case JSCommend.OPEN_VIP:
                IntentUtils.startOpenMemberActivity(mContext);
                break;
            default:
                break;
        }
        return false;
    }

    public interface JSCommend {
        /**
         * 获取设备信息
         */
        int DEVICE_INFO_CODE = 101;
        /**
         * 最大化WebView
         */
        int MAXIMIZATION_CODE = 102;
        /**
         * 退出最大化webview
         */
        int EXIT_MAXIMIZATION_CODE = 103;
        /**
         * 网络状态变化
         */
        int NETWORK_CHANGE_CODE = 104;
        /**
         * onPause
         */
        int EXIT_CODE = 105;
        /**
         * onResume
         */
        int ACTIVE_CODE = 106;
        /**
         * onDestory
         */
        int DESTORY_CODE = 107;
        /**
         * 屏幕旋转
         */
        int ROTATE_CODE = 108;
        /**
         * 调起登录
         */
        int USER_LOGIN_CODE = 109;
        /**
         * 执行分享
         */
        int OPEN_SHARE_CODE = 110;
        /**
         * 调起native界面
         */
        int DISPATH_CODE = 111;
        /**
         * 退出界面
         */
        int CLOSE_CODE = 112;

        /**
         * 调起绑定
         */
        int USER_BIND_CODE = 113;
        /**
         * 无图模式
         */
        int NO_PICTURE_CODE = 114;
        /**
         * 无图模式
         */
        int DAY_OR_NIGHT_CODE = 115;
        /**
         * 执行统计
         */
        int PERFORM_STATIST_CODE = 116;
        /**
         * v统计
         */
        int V_STAT_CODE = 117;

        /**
         * 网络监听注册名称
         */
        String NETWORK_CHANGE = "networkchange";

        /**
         * H5请求推送开关时，通过该方法回调。
         */
        String PUSH_STATE_CALLBACK = "pushStateCallBack";
        /**
         * H5 调用推送代码
         */
        String INPUT = "input";

        /**
         * onPause
         */
        String EXIT = "exit";

        /**
         * onResume
         */
        String ACTIVE = "active";

        /**
         * onDestory
         */
        String DESTORY = "destory";

        String LOGIN = "login";

        /**
         * 开通会员
         */
        int OPEN_VIP = 118;
    }


    private JSDispatchListener dispatchListener;

    public interface JSDispatchListener {
        public void dispatch(String type, String url, String category, String errurl, String documentid);


        public void dispatch(Map<String, String> paramMaps);
    }

    public void setDispatchListener(JSDispatchListener dispatchListener) {
        this.dispatchListener = dispatchListener;
    }

    @JavascriptInterface
    public void dispatch(String type, String url, String category, String errurl, String documentid) {
        if (dispatchListener != null) {
            dispatchListener.dispatch(type, url, category, errurl, documentid);
        }
    }

    @JavascriptInterface
    public void dispatch(String type, String url) {
        dispatch(type, url, "", "", "");
    }

    @JavascriptInterface
    public DeviceInfo getDeviceInfo() {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setAv(PhoneConfig.softversion);
        deviceInfo.setDf("androidphone");
        deviceInfo.setGv(PhoneConfig.softversion);
        deviceInfo.setProid("videoapp");
        deviceInfo.setPublishid(PhoneConfig.publishid);
        deviceInfo.setScreen(ScreenUtils.getScreenSize(mContext));
        deviceInfo.setUid(new User(mContext).getUid());
        deviceInfo.setDeviceId(new User(mContext).getUid());
        deviceInfo.setOs(PhoneConfig.mos);
        deviceInfo.setVt("5");

        return deviceInfo;
    }

    private void setDeviceInfo(String callback) {
        String info = getDeviceJsonInfo();
        performJSCallBack(callback, info);
    }

    private String getDeviceJsonInfo() {
        JSONObject json = new JSONObject();
        try {
            json.put("av", PhoneConfig.softversion);
            json.put("df", "androidphone");
            json.put("gv", PhoneConfig.softversion);
            json.put("proid", "videoapp");
            json.put("publishid", PhoneConfig.publishid);
            json.put("screen", ScreenUtils.getScreenSize(mContext));
            json.put("uid", new User(mContext).getUid());
            json.put("deviceid", PhoneConfig.UID);
            json.put("os", PhoneConfig.mos);
            json.put("vt", "5");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private void performUserLogin() {
        Log.d(TAG, "performUserLogin");
        Intent intent = new Intent(mContext, LoginMainActivity.class);
        mContext.startActivityForResult(intent, LoginMainActivity.USER_LOGIN_REQUESTCODE);
        mContext.overridePendingTransition(R.anim.common_slide_left_in, R.anim.common_slide_left_out);
    }

    public void switchScreenState(int rotate) {
        //TODO
//        if (rotate == screenState) {
//            return;
//        }
//
//        screenState = rotate;
        if (rotate == HORIZONTAL_FLAG) {
            if (!ScreenUtils.isLand()) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else if (rotate == VERTICAL_FLAG) {
            if (ScreenUtils.isLand()) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }

    }


    @JavascriptInterface
    public boolean userIsLogin() {
        return User.isLogin();
    }

    @JavascriptInterface
    public void login(String callback) {
        mCallBacks.put(JSCommend.LOGIN, callback);
        mainHandler.sendEmptyMessage(JSCommend.USER_LOGIN_CODE);
    }

    @JavascriptInterface
    public void userLogin() {
        mainHandler.sendEmptyMessage(JSCommend.USER_LOGIN_CODE);
    }

    /**
     * 后端所传递的参数可能有以下几种:
     * <p>
     * key = 'url',
     */
    @JavascriptInterface
    public void goTo(String paramMapStr) {
        if (TextUtils.isEmpty(paramMapStr)) {
            return;
        }
        try {
            Map<String, String> maps = new Gson().fromJson(paramMapStr, HashMap.class);
            if (dispatchListener != null && maps != null && !maps.isEmpty()) {
                dispatchListener.dispatch(maps);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    @JavascriptInterface
    public void on(String commendName, String callback) {
        if (TextUtils.isEmpty(commendName)) {
            return;
        }

        Message msg = mainHandler.obtainMessage();
        if (commendName.equals(JSCommend.ACTIVE)) {
            mCallBacks.put(JSCommend.ACTIVE, callback);
        }
        if (commendName.equals(JSCommend.EXIT)) {
            mCallBacks.put(JSCommend.EXIT, callback);
        }
        if (commendName.equals(JSCommend.DESTORY)) {
            mCallBacks.put(JSCommend.DESTORY, callback);
        }
        if (commendName.equals(JSCommend.NETWORK_CHANGE)) {
            msg.what = JSCommend.NETWORK_CHANGE_CODE;
            mCallBacks.put(JSCommend.NETWORK_CHANGE, callback);
            msg.obj = callback;
            mainHandler.sendMessage(msg);
        }
    }

    @JavascriptInterface
    public void maximization() {
        setTitleBarHidden();
    }

    @JavascriptInterface
    public void setTitleBarHidden() {
        mainHandler.sendEmptyMessage(JSCommend.MAXIMIZATION_CODE);
    }

    public void hiddenTitleBar() {
        Log.d(TAG, "hiddenTitleBar");
        if (mContext instanceof ADActivity) {
            Message msg = outHandler.obtainMessage();
            msg.what = BaseLiveActivity.HIDE_TITLE_BAR;
            outHandler.sendMessage(msg);
        }
    }

    @JavascriptInterface
    public void exitMaximization() {
        Log.d(TAG, "exitMaximization");
    }

    @JavascriptInterface
    public void close() {
        if (ScreenUtils.isLand()) {
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            mainHandler.sendEmptyMessage(JSCommend.CLOSE_CODE);
        }
    }

    @JavascriptInterface
    public void clearHistory(boolean isClear) {
        if (!isClear) {
            return;
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mContext != null && mContext instanceof ActivityH5Live) {

                    ((ActivityH5Live) mContext).clearHistory();
                }
            }
        });
    }

    @JavascriptInterface
    public String slip(String callback) {
        return "out";
    }

    @JavascriptInterface
    public void vStat(String data, int end) {
        if (end == 1) {
            Message msg = mainHandler.obtainMessage();
            msg.what = JSCommend.V_STAT_CODE;
            msg.obj = data;
            mainHandler.sendMessage(msg);
        }
//        this.vData = data;
    }


    @JavascriptInterface
    public void deviceInfo(String callback) {
        Message msg = mainHandler.obtainMessage();
        msg.what = JSCommend.DEVICE_INFO_CODE;
        msg.obj = callback;
        mainHandler.sendMessage(msg);
    }

    @JavascriptInterface
    public void rotate(int rotate) {
        Message msg = mainHandler.obtainMessage();
        msg.what = JSCommend.ROTATE_CODE;
        msg.arg1 = rotate;
        mainHandler.sendMessage(msg);
    }

    public void onWebViewDestroy() {
        String callback = mCallBacks.get(JSCommend.DESTORY);
        performJSCallBack(callback);
    }

    public void onWebViewResume() {
        String callback = mCallBacks.get(JSCommend.ACTIVE);
        performJSCallBack(callback);
    }

    public void onWebViewPause() {
        String callback = mCallBacks.get(JSCommend.EXIT);
        performJSCallBack(callback);
    }

    @JavascriptInterface
    public UserInfo getUserInfo() {
//        Log.d(TAG, "getUserInfo:");

        UserInfo userInfo = new UserInfo();
        if (User.isLogin()) {
            userInfo.setNickname(new User(mContext).getIfengUserName());
            userInfo.setUserImage(new User(mContext).getUserIcon());
            userInfo.setUid(new User(mContext).getUid());
            userInfo.setGuid(new User(mContext).getUid());
            userInfo.setToken(new User(mContext).getIfengToken());
        }
//        Log.d(TAG, "getUserInfo:" + userInfo.toString());
        return userInfo;
    }

    @JavascriptInterface
    public void openShare(final String shareUrl, final String title, final String desc, final String thumbnail, final String documentid) {
//        Log.d(TAG, "openShare:" + "shareUrl:" + shareUrl + ",title:" + title + ",desc:" + desc + ",thunbnail:" + thumbnail + ",documentid:" + documentid);

        Message msg = outHandler.obtainMessage();
        msg.what = BaseLiveActivity.OPEN_SHARE_POP;
        Bundle bundle = new Bundle();
        bundle.putString("shareUrl", shareUrl);
        bundle.putString("title", title);
        bundle.putString("desc", desc);
        bundle.putString("thumbnail", thumbnail);
        bundle.putString("documentid", documentid);
        msg.setData(bundle);
        outHandler.sendMessage(msg);
    }

    private static boolean savePageInfo = false;
    private static String pageId;

    public String getPageStat() {
        return pageId;
    }

    public void savePageData(boolean data) {
        savePageInfo = data;
    }

    /**
     * 传json键值对，page统计必须传id。其他参数根据统计文档添加，例如
     * {'id':'reg_1','refType':'login','**':'***'},
     * 键为统计的参数名，值为参数对应的值。
     * pageStat{"ref":"live_jx","id":"live_td","type":"live"}
     */
    @JavascriptInterface
    public void pageStat(String pageJsonStr) {
        logger.debug("jsbPage:{}", pageJsonStr);

        if (TextUtils.isEmpty(pageJsonStr)) {
            return;
        }

        PageLiveRecord pageLiveRecord = null;
        pageLiveRecord = new Gson().fromJson(pageJsonStr, PageLiveRecord.class);

        if (savePageInfo) {
            SharePreUtils.getInstance().setLivePageJson(pageJsonStr);
            savePageInfo = false;
            return;
        }
        sendPageTime(pageJsonStr, pageLiveRecord);
//        CommonStatictisListUtils.getInstance().sendPageLive(new PageLiveRecord(pageLiveRecord.getId(), pageLiveRecord.ref, pageLiveRecord.type));
//        sendExpose();

    }

    private void sendPageTime(String pageJsonStr, PageLiveRecord pageLiveRecord) {

        if (pageLiveRecord != null
                && !TextUtils.isEmpty(pageLiveRecord.getId())
                && !TextUtils.isEmpty(pageLiveRecord.ref)
                && !TextUtils.isEmpty(pageLiveRecord.type)) {
            savePageInfo = false;
            pageId = pageLiveRecord.getId();
            long currentTime = System.currentTimeMillis() / 1000;
            long startTime = SharePreUtils.getInstance().getLivePageStartTime();
            long durTime = currentTime - startTime;

            CommonStatictisListUtils.getInstance().sendPageLive(new PageLiveRecord(pageLiveRecord.getId(), pageLiveRecord.ref, pageLiveRecord.type, durTime + ""));
            SharePreUtils.getInstance().setLivePageStartTime(System.currentTimeMillis() / 1000);
            sendExpose();
        }
    }


    @JavascriptInterface
    public String getShowMode() {
        return "day";
    }

    /**
     * actionStat{"type":"upscreen","id":"live_jx","psnum":2}
     * 2017-08-10+11:17:16#action#id=lorder$act=$page=piclive_108470$add=
     */
    @JavascriptInterface
    public void actionStat(String actionJsonStr) {
//        logger.debug("jsbAction:{}", actionJsonStr);
        if (TextUtils.isEmpty(actionJsonStr)) return;
        ActionConvertRecord record = null;
        record = new Gson().fromJson(actionJsonStr, ActionConvertRecord.class);

        ActionLiveRecord actionLiveRecord = new ActionLiveRecord(record.getType(), record.getId(), record.getPsnum());
        CommonStatictisListUtils.getInstance().sendActionLive(actionLiveRecord);
        sendExpose();

    }

    //TODO
    private List<LiveExposeRecord> liveExposeRecords = new ArrayList<>(20);

    /**
     * 曝光
     * expose{"reftype":"editor","id":"piclive_107759","rnum":17,"ch":"live_jx","src":"谈资视频"}
     * expose{"reftype":"editor","id":"piclive_107761","rnum":"1_2","ch":"live_jx","src":"法制晚报法律大讲堂"}
     *
     * @param jsonString
     */
    @JavascriptInterface
    public void expose(String jsonString) {
//        logger.debug("jsbExpose:{}", jsonString);

        LiveExposeRecord.PInfo pInfo = null;
        pInfo = new Gson().fromJson(jsonString, LiveExposeRecord.PInfo.class);
        LiveExposeRecord liveExposeRecord = new LiveExposeRecord(pInfo);
        if (!liveExposeRecords.contains(liveExposeRecord)) {
            liveExposeRecords.add(liveExposeRecord);
            CommonStatictisListUtils.liveLocalExposeRecords.add(liveExposeRecord);
        }

        if (liveExposeRecords.size() > 10) {
            sendExpose();
        }
    }

    private void sendExpose() {
        CommonStatictisListUtils.getInstance().sendLivePageInfo(liveExposeRecords);
        liveExposeRecords.clear();
    }

    @JavascriptInterface
    public void stat(String session, boolean immediately) {
        Message msg = mainHandler.obtainMessage();
        msg.what = JSCommend.PERFORM_STATIST_CODE;
        if (immediately) {
            msg.arg1 = 1;
        } else {
            msg.arg1 = 0;
        }
        msg.obj = session;
        mainHandler.sendMessage(msg);
    }


    @Deprecated
    @JavascriptInterface
    public boolean setAlarmClock(String documentId, long dataTime, String title) {

        if (!TextUtils.isEmpty(documentId)) {
            Bundle b = new Bundle();
            b.putCharSequence("title", title);
            b.putString("type", "slv");
            b.putString("aid", documentId);
            Intent messageIntent = new Intent(mContext, AlarmReceiver.class);
            messageIntent.setPackage("com.ifeng.newvideo");
            messageIntent.setAction(PushReceiver.PUSH_LIVE_URL);
            messageIntent.putExtra(AlarmReceiver.PUSH_EXTRA_BUNDLE, b);
            int requestCode = 0;
            try {
                requestCode = Integer.parseInt(documentId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, messageIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager manager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            manager.set(AlarmManager.RTC_WAKEUP, dataTime, pendingIntent);
            return true;
        }
        return false;
    }

    @JavascriptInterface
    public boolean setAlarmClock(String documentId, long dataTime, String title, String type) {
//        long time = 1501138060000L;
        if (TextUtils.isEmpty(documentId)) {
            return false;
        }

        if ("text_live".equals(type)) {
            Bundle b = new Bundle();
            b.putCharSequence("title", title);
            b.putString("type", "text_live");
            b.putString("aid", documentId);
            b.putLong("datetime", dataTime);
            Intent messageIntent = new Intent(mContext, AlarmReceiver.class);
            messageIntent.setPackage(IfengApplication.PACKAGE_NAME);
            messageIntent.setAction(AlarmReceiver.SPORTS_LIVE_MESSAGE_ACTION);
            messageIntent.putExtra(AlarmReceiver.PUSH_EXTRA_BUNDLE, b);
            int requestCode = 0;
            try {
                requestCode = Integer.parseInt(documentId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, messageIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager manager = (AlarmManager) mContext
                    .getSystemService(Context.ALARM_SERVICE);
            manager.set(AlarmManager.RTC_WAKEUP, dataTime, pendingIntent);
            saveDocumentId(documentId);

            return true;
        }

        if ("h5_live".equals(type)) {
            Bundle b = new Bundle();
            b.putCharSequence("title", title);
            b.putString("type", "web");
            b.putString("aid", IntentUtils.H5_LIVE_URL + documentId + "&isFull=1");
            Intent messageIntent = new Intent(AlarmReceiver.SPORTS_LIVE_MESSAGE_ACTION);
            messageIntent.setPackage(IfengApplication.PACKAGE_NAME);
            messageIntent.putExtra(AlarmReceiver.PUSH_EXTRA_BUNDLE, b);
            int requestCode = 0;
            try {
                requestCode = Integer.parseInt(documentId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, messageIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager manager = (AlarmManager) mContext
                    .getSystemService(Context.ALARM_SERVICE);
            manager.set(AlarmManager.RTC_WAKEUP, dataTime, pendingIntent);
            saveDocumentId(documentId);
            return true;
        }

        Bundle b = new Bundle();
        b.putCharSequence("title", title);
        b.putString("type", type);
        b.putString("aid", documentId);
        Intent messageIntent = new Intent(AlarmReceiver.SPORTS_LIVE_MESSAGE_ACTION);
        messageIntent.setPackage(IfengApplication.PACKAGE_NAME);
        messageIntent.putExtra(AlarmReceiver.PUSH_EXTRA_BUNDLE, b);
        int requestCode = 0;
        try {
            requestCode = Integer.parseInt(documentId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, messageIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager) mContext
                .getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP, dataTime, pendingIntent);

        return true;
    }

    private void saveDocumentId(String documentId) {
        StringBuilder sb = new StringBuilder();
        String preId = SharePreUtils.getInstance().getH5DocumentId();
        if (preId != null) {
            sb.append(preId);
        }
        sb.append(documentId).append(",");
        SharePreUtils.getInstance().setH5DocumentId(sb.toString());
    }

    private void removeDocumentId(String documentId) {
        String preId = SharePreUtils.getInstance().getH5DocumentId();
        String[] array = preId.split(",");
        StringBuilder sb = new StringBuilder();
        for (String id : array) {
            if (!id.equals(documentId)) {
                sb.append(id).append(",");
            }
        }
        SharePreUtils.getInstance().setH5DocumentId(sb.toString());
    }

    @JavascriptInterface
    public boolean cancelAlarmClock(String documentId) {

        if (!TextUtils.isEmpty(documentId)) {
            Bundle b = new Bundle();
            b.putString("aid", documentId);
            Intent messageIntent = new Intent(mContext, AlarmReceiver.class);
            messageIntent.setPackage(IfengApplication.PACKAGE_NAME);
            messageIntent.putExtra(AlarmReceiver.PUSH_EXTRA_BUNDLE, b);
            int requestCode = 0;
            try {
                requestCode = Integer.parseInt(documentId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, messageIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager manager = (AlarmManager) mContext
                    .getSystemService(Context.ALARM_SERVICE);
            manager.cancel(pendingIntent);
            removeDocumentId(documentId);

            return true;
        }

        return false;
    }

    private Handler outHandler;

    public void setOutHandler(Handler handler) {
        this.outHandler = handler;
    }

    @JavascriptInterface
    public void input(String params, String callback) {
        Log.d("jsbridge", "input");
        mCallBacks.put(JSCommend.INPUT, callback);
        Message msg = outHandler.obtainMessage();
        msg.what = BaseLiveActivity.OPEN_INPUT_LAYOUT;
        msg.obj = params;
        outHandler.sendMessage(msg);
    }


    @JavascriptInterface
    public void isTiming(final String ids, final String callBack) {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                performQueryTimingFromId(ids, callBack);
            }
        });
    }

    public void performQueryTimingFromId(String ids, String callBack) {

        if (TextUtils.isEmpty(ids) || TextUtils.isEmpty(callBack)) {
            return;
        }
        String[] idArray = ids.split(",");

        StringBuilder sb = new StringBuilder();
        for (String id : idArray) {
            sb.append(isTiming(id)).append(",");
        }
        sb.deleteCharAt(sb.length() - 1);

        performJSCallBack(callBack, sb.toString());
    }

    /**
     * 获取是否设置过定时器
     *
     * @return
     */
    @JavascriptInterface
    public String isTiming(String keys) {
        StringBuffer sb = new StringBuffer();
        if (!TextUtils.isEmpty(keys)) {
            String[] ids = keys.split(",");
            if (ids != null && ids.length > 0) {
                int length = ids.length;
                for (int i = 0; i < length; i++) {
                    if (i == length - 1) {
                        sb.append(isReaded(ids[i]) ? 1 : 0);
                    } else {
                        sb.append((isReaded(ids[i]) ? 1 : 0) + ",");
                    }
                }
            }
        }
        return sb.toString();
    }

    public boolean isReaded(String key) {
        String preId = SharePreUtils.getInstance().getH5DocumentId();
        if (preId == null) return false;
        if (preId.contains(key)) {
            return true;
        }
        return false;
    }


    public void setNetWorkType() {
        String netType = NetUtils.getNetType(mContext);
        String netWorkCallBack = mCallBacks.get(JSCommend.NETWORK_CHANGE);
        performJSCallBack(netWorkCallBack, netType);
    }

    private void performJSCallBack(String callBack, Object... args) {
        if (TextUtils.isEmpty(callBack) || mContext == null || mContext.isFinishing()) {
            //如果当前页面用户已经退出了，就别在回掉了，应该会出现NPE.
            return;
        }
        StringBuilder sb = new StringBuilder();
        String params = "";
        if (args != null && args.length != 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof String) {
                    sb.append("'" + args[i] + "'").append(",");
                } else {
                    sb.append(args[i]).append(",");
                }
            }
            params = sb.toString().substring(0, sb.length() - 1);
            sb.delete(0, sb.length());
        }
        sb.append("javascript:").append(callBack + "(").append(params).append(")");
//        logger.debug("sb:{}", sb.toString());
        mWebView.loadUrl(sb.toString());
    }

    public void performInputCallBack(String text) {
        String callback = mCallBacks.get(JSCommend.INPUT);
        if (TextUtils.isEmpty(callback)) {
            return;
        }
        performJSCallBack(callback, text);
    }

    @JavascriptInterface
    public void openMemberCenter() {
        mainHandler.sendEmptyMessage(JSCommend.OPEN_VIP);
    }

    @JavascriptInterface
    public boolean userIsVip() {
        return User.isVip();
    }

    /**
     * H5 判断是不是端内加载
     *
     * @return
     */
    @JavascriptInterface
    public boolean isLoadInApp() {
        return true;
    }


}
