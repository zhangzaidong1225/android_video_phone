package com.ifeng.newvideo.ui.live.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 电视直播GridView适配器
 * Created by Administrator on 2016/7/19.
 */
public class TVLiveAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(TVLiveAdapter.class);

    private LayoutInflater layoutInflater;
    private List<TVLiveInfo> list = new ArrayList<>();
    private Context context;

    public TVLiveAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public int getCount() {
        return list.size() >= 4 ? 4 : list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        OnClick onClick = null;
        if (view == null) {
            holder = new ViewHolder();
            onClick = new OnClick();
            view = layoutInflater.inflate(R.layout.adapter_tv_live_item_layout, null);
            holder.initView(view);
            view.setTag(R.id.view_parent, onClick);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }

        onClick.setPosition(i);
        holder.itemView.setOnClickListener(onClick);
        holder.tvName.setText(list.get(i).getcName());
        holder.tvIcon.setImageUrl(list.get(i).getImg490_490(), VolleyHelper.getImageLoader());
        holder.tvIcon.setDefaultImageResId(R.drawable.icon_default_tv_live);
        holder.tvIcon.setErrorImageResId(R.drawable.icon_default_tv_live);
        return view;
    }

    public void setData(List<TVLiveInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        View itemView;//ItemView
        NetworkImageView tvIcon;//电视台图标
        TitleTextView tvName;//电视台名称

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvIcon = (NetworkImageView) view.findViewById(R.id.tv_icon);
            tvName = (TitleTextView) view.findViewById(R.id.tv_name);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            IntentUtils.startActivityTVLive(context, list.get(position).getChannelId(), "", "", "", "", false);
        }
    }
}
