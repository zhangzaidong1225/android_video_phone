package com.ifeng.newvideo.ui.live.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.dao.db.model.live.LiveListInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 直播列表ListView适配器
 * Created by Administrator on 2016/7/19.
 */
public class LiveListAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(LiveListAdapter.class);

    private List<LiveListInfo.BodyEntity> list = new ArrayList<>();

    public LiveListAdapter() {
        isError = false;
        isLoading = false;
        isNoNet = false;
    }

    @Override
    public Object getItem(int i) {
        if (isLoading || isError || isNoNet || list.isEmpty()) {
            return null;
        }
        return list.get(i);
    }

    @Override
    public int getCount() {
        if (isLoading || isError || isNoNet) {
            return 1;
        }
        return list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (isLoading) {
            return getLoadingView(viewGroup);
        } else if (isError) {
            return getErrorView(viewGroup);
        } else if (isNoNet) {
            return getNoNetView(viewGroup);
        }

        ViewHolder holder = null;
        OnClick onClick = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            holder = new ViewHolder();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_live_list_item_layout, null);
            holder.initView(view);
            view.setTag(holder);
            view.setTag(R.id.view_parent, onClick);
        } else {
            holder = (ViewHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        holder.itemView.setOnClickListener(onClick);
        LiveListInfo.BodyEntity bodyEntity = list.get(i);
        if (bodyEntity.getStatus().equals("0")) {
            holder.tv_state.setVisibility(View.VISIBLE);
            holder.tv_state.setBackgroundResource(R.drawable.tag_tv_complete);
            holder.tv_state.setText(R.string.live_foreshow);
            holder.tv_state.setTextColor(Color.parseColor("#262626"));
        } else if (bodyEntity.getStatus().equals("2")) {
            holder.tv_state.setVisibility(View.VISIBLE);
            holder.tv_state.setBackgroundResource(R.drawable.tag_tv_complete);
            holder.tv_state.setText(R.string.live_complete);
            holder.tv_state.setTextColor(Color.parseColor("#999999"));
        } else if (bodyEntity.getStatus().equals("1")) {
            holder.tv_state.setVisibility(View.VISIBLE);
            holder.tv_state.setBackgroundResource(R.drawable.tag_tv_living);
            holder.tv_state.setText(R.string.live_living);
            holder.tv_state.setTextColor(viewGroup.getContext().getResources().getColor(R.color.white));
        } else {//如果没有状态值则隐藏掉该标签
            holder.tv_state.setVisibility(View.GONE);
        }
        holder.img_banner.setImageUrl(bodyEntity.getImage(), VolleyHelper.getImageLoader());
        holder.img_banner.setDefaultImageResId(R.drawable.tab_live_item_bg);
        holder.img_banner.setErrorImageResId(R.drawable.tab_live_item_bg);
        String time = bodyEntity.getChannelDate();
        holder.tv_time.setText(subTime(time));//格式化时间
        holder.tv_title.setText(bodyEntity.getTitle());

        return view;
    }

    class ViewHolder {
        View itemView;
        TextView tv_title;//标题
        NetworkImageView img_banner;//图片
        TextView tv_vr;//VR图标
        TextView tv_state;//直播状态
        TextView tv_time;//时间
        View line;//竖线

        public void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            img_banner = (NetworkImageView) view.findViewById(R.id.img_banner);
            tv_vr = (TextView) view.findViewById(R.id.tv_vr);
            tv_state = (TextView) view.findViewById(R.id.tv_state);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            line = view.findViewById(R.id.line);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            String imageUrl = list.get(position).getImage();
            String title = list.get(position).getTitle();
            String url = list.get(position).getUrl();
            //跳转到H5直播页面
            IntentUtils.startADActivity(view.getContext(), list.get(position).getChannelId(), url,
                    null, ADActivity.FUNCTION_VALUE_H5_LIVE, title, null, imageUrl, "", "", null, null);
        }
    }

    public void setData(List<LiveListInfo.BodyEntity> list) {
        isError = false;
        isLoading = false;
        isNoNet = false;
        this.list = list;
        notifyDataSetChanged();
    }

    /**
     * 规则化时间 MM-dd hh:mm
     *
     * @param time yy-MM-dd hh:mm:ss     2016-08-18 14:00:00
     */
    private String subTime(String time) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        try {
            boolean isToday = isToday(time);//是否是今天
            if (time.contains(" ")) {//如果是：yy-MM-dd hh:mm:ss 格式
                String temp[] = time.split(" ");//根据"中间空格"分割字符串
                if (isToday) {
                    temp[0] = "今天";
                } else {
                    temp[0] = temp[0].substring(5, temp[0].length());//截取格式为：MM-dd
                }
                temp[1] = temp[1].substring(0, 5);//截取格式为：hh:mm
                return temp[0] + " " + temp[1];//重新组合字符串
            } else {////如果不是：yy-MM-dd hh:mm:ss 格式
                if (isToday) {
                    return "今天" + " " + time.substring(11, time.length() - 3);//格式为：今天 hh:mm
                } else {
                    return time.substring(5, time.length() - 3);//截取格式为：MM-ddhh:mm,中间无空格
                }
            }
        } catch (Exception e) {
            logger.error("subTime error ! ", e);
        }
        return "";
    }

    /**
     * 是否在今天
     */
    public boolean isToday(String date) {
        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentDate);
        try {
            currentDate = formatter.parse(dateString);//当前日期
            Date startDate = DateUtils.yymmdd.parse(date);
            return !startDate.after(currentDate) && !startDate.before(currentDate);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return false;
    }

    private volatile boolean isError;
    private volatile boolean isLoading;
    private volatile boolean isNoNet;

    public synchronized void showLoadingView() {
        isLoading = true;
        isError = false;
        isNoNet = false;
        notifyDataSetChanged();
    }

    public synchronized void showErrorView() {
        isLoading = false;
        isError = true;
        isNoNet = false;
        notifyDataSetChanged();
    }

    public synchronized void showNoNetView() {
        isLoading = false;
        isError = false;
        isNoNet = true;
        notifyDataSetChanged();
    }

    private View getLoadingView(ViewGroup viewGroup) {
        return inflaterView(viewGroup, R.layout.live_list_loading_layout);
    }

    private View getErrorView(ViewGroup viewGroup) {
        View view = inflaterView(viewGroup, R.layout.live_list_error_layout);
        ImageView icon = (ImageView) view.findViewById(R.id.common_load_icon);
        TextView textView = (TextView) view.findViewById(R.id.empty_tv);
        icon.setImageResource(R.drawable.icon_common_load_fail);
        textView.setText(R.string.common_load_data_error);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onLoadFailedClick();
                }
            }
        });
        return view;
    }

    private View getNoNetView(ViewGroup viewGroup) {
        View view = inflaterView(viewGroup, R.layout.live_list_error_layout);
        ImageView icon = (ImageView) view.findViewById(R.id.common_load_icon);
        TextView textView = (TextView) view.findViewById(R.id.empty_tv);
        icon.setImageResource(R.drawable.icon_common_no_net);
        textView.setText(R.string.common_net_useless_try_again);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onLoadFailedClick();
                }
            }
        });
        return view;
    }

    private View inflaterView(ViewGroup root, @LayoutRes int layoutId) {
        View view = LayoutInflater.from(root.getContext())
                .inflate(layoutId, root, false);
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        }
        return view;
    }

    private OnLoadFailedClick mListener;

    public void setOnLoadFailClick(OnLoadFailedClick listener) {
        this.mListener = listener;
    }

    public interface OnLoadFailedClick {
        void onLoadFailedClick();
    }
}
