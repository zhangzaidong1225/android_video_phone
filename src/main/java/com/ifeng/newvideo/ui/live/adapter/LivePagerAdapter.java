package com.ifeng.newvideo.ui.live.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.ifeng.newvideo.ui.live.FragmentLiveChannel;
import com.ifeng.newvideo.ui.live.FragmentLiveChannelBasic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 直播的FragmentViewpager
 * Created by Vincent on 2014/11/10.
 */
public class LivePagerAdapter extends FragmentStatePagerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LivePagerAdapter.class);

    private List<FragmentLiveChannelBasic> data;


    public LivePagerAdapter(FragmentManager fm) {
        super(fm);
    }


    public void setData(List<FragmentLiveChannelBasic> data) {
        this.data = data;
    }

    public void addItem(FragmentLiveChannel fragment) {
        if (data == null) {
            data = new ArrayList<FragmentLiveChannelBasic>();
        }
        if (!data.contains(fragment)) {
            data.add(fragment);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position).getCategoryName();
    }

    @Override
    public Fragment getItem(int arg0) {
        if (data == null) {
            throw new NullPointerException("columnFragment is still null, fill it will your fragments");
        }
        return data.get(arg0);
    }

    @Override
    public int getCount() {
        if (data == null) {
            throw new NullPointerException("mFrags is still null, fill it will your fragments");
        }
        return data.size();
    }

}
