package com.ifeng.newvideo.ui.live.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.ifeng.newvideo.ui.live.TVLiveActivity;
import com.ifeng.newvideo.ui.live.TVLiveProgramFragment;
import com.ifeng.video.core.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * 电视台直播底页ViewPager的适配器
 * Created by Administrator on 2016/8/3.
 */
public class TVLivePagerAdapter extends FragmentStatePagerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(TVLivePagerAdapter.class);

    private Context context;
    private String channelId;

    public TVLivePagerAdapter(Context context, FragmentManager fm, String channelId) {
        super(fm);
        this.context = context;
        this.channelId = channelId;
    }

    @Override
    public int getCount() {
        return TVLiveActivity.DAY_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        TVLiveProgramFragment fragment = TVLiveProgramFragment.newInstance(channelId, position + 1);
        if (context instanceof TVLiveActivity)
            fragment.setIScheduleInfo(((TVLiveActivity) context));
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "今天";
        } else {
            //Date date = DateUtils.getDateFormat(LiveUtils.getCurrentTime());
            Date date = DateUtils.getDateFormat(System.currentTimeMillis());
            return DateUtils.getWeek(date, position + 1);
        }
    }
}
