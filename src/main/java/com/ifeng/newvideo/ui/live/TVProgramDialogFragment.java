package com.ifeng.newvideo.ui.live;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.*;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.live.adapter.TVLivePagerAdapter;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 电视台节目DialogFragment
 * Created by Administrator on 2016/11/21.
 */
public class TVProgramDialogFragment extends DialogFragment {
    private static final Logger logger = LoggerFactory.getLogger(TVProgramDialogFragment.class);

    public static final String CHANNEL_ID = "channelId";

    private String channelId;

    private PagerSlidingTabStrip slidingTabStrip;
    private ViewPager viewPager;

    public static TVProgramDialogFragment newInstance(String channelId) {
        TVProgramDialogFragment fragment = new TVProgramDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CHANNEL_ID, channelId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            channelId = getArguments().getString(CHANNEL_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_tv_program_layout, container, false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(-1, -2);
        initView(view);
        initAdapter();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null) {
            return;
        }
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = getShowHeight();
        lp.windowAnimations = R.style.anim_tv_program_dialog;
        window.setAttributes(lp);
    }

    private void initView(View view) {
        slidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.pager_tab_container);
        slidingTabStrip.setShouldExpand(true);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        View icon_close = view.findViewById(R.id.icon_close);
        icon_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_EPG_CLOSE, PageIdConstants.PAGE_LIVE);
                dismiss();
            }
        });
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        TVLivePagerAdapter pagerAdapter = new TVLivePagerAdapter(getActivity(), getChildFragmentManager(), channelId);
        viewPager.setOffscreenPageLimit(3);//三天
        viewPager.setAdapter(pagerAdapter);
        slidingTabStrip.setViewPager(viewPager);
        pagerAdapter.notifyDataSetChanged();
        slidingTabStrip.notifyDataSetChanged();
        viewPager.setCurrentItem(0);
    }

    /**
     * 获取展示高度
     */
    private int getShowHeight() {
        int screenHeight = DisplayUtils.getWindowHeight();//屏幕高度
        int statusBarHeight = DisplayUtils.getStatusBarHeight();//状态栏高度
        int videoHeight = DisplayUtils.getWindowWidth() * 9 / 16;//播放器和标题总高度,该值可能会根据需求作调整

        int titleBarHeight = DisplayUtils.convertDipToPixel(40);//播放器和标题总高度,该值可能会根据需求作调整
        return screenHeight - statusBarHeight - titleBarHeight - videoHeight;
    }
}

