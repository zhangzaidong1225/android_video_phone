package com.ifeng.newvideo.ui.live.listener;

import android.view.MotionEvent;

/**
 * 由MainTabActivity捕获TouchEvent分发给LiveActivity处理相应的手势事件
 * Created by Vincent on 2014/11/25.
 */

public class LiveMotionEvent {

    private final MotionEvent ev;

    public LiveMotionEvent(MotionEvent _ev) {
        this.ev = _ev;
    }

    public MotionEvent getMotionEvent() {
        return ev;
    }
}