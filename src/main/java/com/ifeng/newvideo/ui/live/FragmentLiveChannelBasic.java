package com.ifeng.newvideo.ui.live;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshGridView;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.LiveChannelCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 直播页面基类
 * Created by Vincent on 2014/11/11.
 */
public abstract class FragmentLiveChannelBasic extends FragmentBase implements PullToRefreshBase.OnRefreshListener2<GridView> {

    private static final Logger logger = LoggerFactory.getLogger(FragmentLiveChannelBasic.class);

    PullToRefreshGridView pullToRefreshListView;
    LiveChannelCategory channelCategory;
    private View noNet;
    private View loadIng;
    View livePlayer;
    private TextView tvErrorHint;

    FragmentLiveChannelBasic(LiveChannelCategory channelCategory) {
        this.channelCategory = channelCategory;
    }

    public FragmentLiveChannelBasic() {
        super();
    }

    public String getCategoryName() {
        return channelCategory.getChannelName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.live_channel_basic, container, false);
        livePlayer = root.findViewById(R.id.live_player);
        pullToRefreshListView = (PullToRefreshGridView) root.findViewById(R.id.live_channel_gridView);
        noNet = root.findViewById(R.id.live_channel_nonet);
        tvErrorHint = (TextView) root.findViewById(R.id.tv_error_hint);
        loadIng = root.findViewById(R.id.live_channel_loading);
        pullToRefreshListView.getRefreshableView().setNumColumns(2);
        pullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
//        pullToRefreshListView.setEmptyView(loadIng);
        pullToRefreshListView.setShowIndicator(false);
        pullToRefreshListView.setOnRefreshListener(this);
        initView();
        loading();
        getData();
        noNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading();
                getData();
            }
        });
        return root;
    }

    protected abstract void getData();

    void initView() {
    }

    protected void loading() {
        noNet.setVisibility(View.INVISIBLE);
        loadIng.setVisibility(View.VISIBLE);
        pullToRefreshListView.setVisibility(View.INVISIBLE);
    }

    void noNet() {
        noNet.setVisibility(View.VISIBLE);
        tvErrorHint.setText(R.string.common_net_useless_try_again);
        loadIng.setVisibility(View.INVISIBLE);
        pullToRefreshListView.setVisibility(View.INVISIBLE);
    }

    void dataError() {
        noNet.setVisibility(View.VISIBLE);
        tvErrorHint.setText(R.string.common_load_data_error);
        loadIng.setVisibility(View.INVISIBLE);
        pullToRefreshListView.setVisibility(View.INVISIBLE);
    }

    void show() {
        noNet.setVisibility(View.INVISIBLE);
        loadIng.setVisibility(View.INVISIBLE);
        pullToRefreshListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
        if (getActivity() != null && !NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(getResources().getString(R.string.common_net_useless));
            pullToRefreshListView.onRefreshComplete();
            return;
        }
        getData();
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {

    }
}
