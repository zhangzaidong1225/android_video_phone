package com.ifeng.newvideo.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.dialogUI.DanmakuCommentFragment;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.ListAdapter4BigPictureChannel;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.LastDocUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.ViewUtils;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInfoItem;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInformation;
import com.ifeng.newvideo.videoplayer.bean.VideoDanmuItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.dao.VideoDao;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.BigPicAdView;
import com.ifeng.newvideo.videoplayer.widget.skin.DanmuEditView;
import com.ifeng.newvideo.videoplayer.widget.skin.FloatVideoViewSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayAdButton;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayButton;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.DefaultControllerView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ifeng.newvideo.ui.ActivityMainTab.getPiPMode;
import static com.ifeng.newvideo.ui.adapter.ListAdapter4BigPictureChannel.STATUS_PAUSE;
import static com.ifeng.newvideo.ui.adapter.ListAdapter4BigPictureChannel.STATUS_PLAYING;


public class UniversalChannelFragment extends ChannelBaseFragment implements AbsListView.OnScrollListener, IPlayController.OnPlayCompleteListener,
        VideoSkin.OnNetWorkChangeListener, VideoSkin.OnLoadFailedListener, VideoSkin.onClickVideoSkin, DefaultControllerView.OnShowOrHideDanmuView,
        PlayAdButton.onPlayAndPreplayClickListener, PlayButton.OnPlayOrPauseListener, BigPicAdView.OnClickAdView, DanmuEditView.OnClickEditButton, UIObserver {

    private ViewGroup mLandScapeContainer;
    private VideoSkin mVideoSkin;
    private NormalVideoHelper mVideoHelper;
    private UIPlayContext mUIPlayerContext;
    private FrameLayout mVideoViewWarpper;
    private ActivityMainTab mActivity;
    private boolean isVisible;
    private Resources mResource;
    private int mHeadViewCount;
    private String requireTime = "";
    private boolean needRecover;
    private ListAdapter4BigPictureChannel picAdapter;
    private boolean hasRegisterReceiver;
    private boolean continuous_model;
    private boolean continuous_model_no_more;
    private int oldDataLenght = -1;
    private boolean mHidden;
    private int mUpTimes;
    private String preGuid = "";
    private DanmakuCommentFragment mDanmakuEditFragment;
    private IPlayController mPlayController;

    private FloatVideoPlayController.OnListViewScrollToPiPVideoView mOnListViewScrollToPiPVideoView = new FloatVideoPlayController.OnListViewScrollToPiPVideoView() {
        @Override
        public void onPlayToVisibleView(String guid, FloatVideoViewSkin skin) {
            mActivity.mCurrentPiPVideoGuid = guid;
            if (isVisible) {
                List<ChannelBean.HomePageBean> list = picAdapter.getDataList();
                for (int i = 0; i < list.size(); i++) {
                    if (EmptyUtils.isNotEmpty(list.get(i).getMemberItem())
                            && EmptyUtils.isNotEmpty(list.get(i).getMemberItem().getGuid())
                            && list.get(i).getMemberItem().getGuid().equals(guid)) {
                        i++;
                        if (i >= mListViewVisibleItem.get(0) && i <= (mListViewVisibleItem.get(0) + mListViewVisibleItem.get(1) - 1)) {
                            skin.removeViews();
                            scrollToPiPPositionAndPlay(500, true, i);
                        }
                        break;
                    }
                }
            }
        }
    };

    private void scrollToPiPPositionAndPlay(final int mScrollDuration, final boolean isPortrait, final int index) {
        logger.debug("scollToNextPositionAndPlay:");
        continuous_model = true;
        final HomePageBeanBase base = picAdapter.getDataList().get(index - 1);
        if (base == null) {
            return;
        }
        final ListView listView = mPullToRefreshListView.getRefreshableView();
        View current = getViewByPosition(listView, index - 1);
        if (current != null) {
            int top = 0;
            if (current.getTop() < 0) {
                top = current.getTop();
            } else {
                current = getViewByPosition(listView, index - 2);
                top = current.getTop() + current.getHeight();
            }

            listView.smoothScrollBy(top, 500);
        }
        if (CheckIfengType.isAD(base.getMemberType()) || AdTools.videoType.equals(base.getMemberItem().adConditions.showType)) {
            picAdapter.increaseClickToPlayPosition();
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int i = index;
                    scrollToPiPPositionAndPlay(mScrollDuration, isPortrait, i++);
                }
            }, mScrollDuration);
        } else {
            final int next_index = index - 1;
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    picAdapter.autoPlayNext(base, next_index, getViewByPosition(listView, next_index), isPortrait);
                }
            }, mScrollDuration);
        }
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (!getPiPMode()) {
            switch (status) {
                case STATE_PREPARING://准备开始播放
                    break;
                case STATE_IDLE:
                    isPlayToHalf = false;
                    PlayerPlayingListener.removeCallbacksAndMessages(null);
                    break;
                case STATE_BUFFERING_START://缓冲开始
                    isPlayToHalf = false;
                    break;
                case STATE_PLAYING:
                case PIP_STATE_PLAYING:
                    PlayerPlayingListener.sendEmptyMessage(PLAYER_PLAYING_MSG_FOR_REL_VIDEO);
                    break;
            }
        }
    }

    private boolean isPlayToHalf = false;
    private int PLAYER_PLAYING_MSG_FOR_REL_VIDEO = 10001;
    private Handler PlayerPlayingListener = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == PLAYER_PLAYING_MSG_FOR_REL_VIDEO) {
                if (mPlayController.getCurrentPosition() >= mPlayController.getDuration() / 2 && !isPlayToHalf) {
                    loadRelativeVideo(mUIPlayerContext.videoItem.guid);
                    isPlayToHalf = true;
                } else {
                    sendEmptyMessageDelayed(PLAYER_PLAYING_MSG_FOR_REL_VIDEO, 1000);
                }
            }
        }
    };

    private void loadRelativeVideo(String guid) {
        VideoDao.getRelativeVideoByIdForLianbo(guid, new Response.Listener<RelativeVideoInformation>() {
                    @Override
                    public void onResponse(RelativeVideoInformation response) {
                        updateRelativeVideoInfo(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
    }

    private void updateRelativeVideoInfo(RelativeVideoInformation response) {
        List<VideoItem> list = new ArrayList<>();
        VideoItem relItem = new VideoItem();
        if (!ListUtils.isEmpty(response.guidRelativeVideoInfo)) {
            for (RelativeVideoInfoItem videoItem : response.guidRelativeVideoInfo) {
                VideoItem item = videoItem.videoInfo;
                if (item == null ||
                        TextUtils.isEmpty(item.guid) ||
                        TextUtils.isEmpty(item.name) ||
                        ListUtils.isEmpty(item.videoFiles)) {
                    continue;
                }
                relItem = item;
                break;
            }
            for (ChannelBean.HomePageBean bean : picAdapter.getDataList()) {
                if ("video".equals(bean.getMemberType())) {
                    if (bean.getMemberItem().getGuid().equals(relItem.guid)) {
                        return;
                    }
                }
            }
            if (EmptyUtils.isNotEmpty(relItem) &&
                    !TextUtils.isEmpty(relItem.guid) &&
                    !TextUtils.isEmpty(relItem.name) &&
                    !ListUtils.isEmpty(relItem.videoFiles)) {
                ChannelBean.HomePageBean beanBase = videoItem2HomePageBeanBase(relItem);
                if (EmptyUtils.isNotEmpty(beanBase)) {
                    int pos = 0;
                    for (int i = 0; i < picAdapter.getDataList().size(); i++) {
                        if (mUIPlayerContext.videoItem.guid.equals(picAdapter.getDataList().get(i).getMemberItem().getGuid())) {
                            pos = i;
                            pos++;
                            break;
                        }
                    }
                    picAdapter.getDataList().add(pos, beanBase);
                    picAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private ChannelBean.HomePageBean videoItem2HomePageBeanBase(VideoItem item) {
        ChannelBean.HomePageBean bean = new ChannelBean.HomePageBean();
        ChannelBean.MemberItemBean memberItem = new ChannelBean.MemberItemBean();
        ChannelBean.WeMediaBean weMedia = new ChannelBean.WeMediaBean();
        if (item.weMedia != null) {
            weMedia.setId(item.weMedia.id);
            weMedia.setName(item.weMedia.name);
            weMedia.setHeadPic(item.weMedia.headPic);
            weMedia.setDesc(item.weMedia.desc);
        }
        bean.setWeMedia(weMedia);
        memberItem.setGuid(item.guid);
        memberItem.setSimId(item.simId);
        memberItem.setCpName(item.cpName);
        bean.setTitle(item.title);
        //bean.setInfoId(item.itemId);
        bean.setMemberType("video");
        memberItem.setSearchPath(item.searchPath);
        List<HomePageBeanBase.ImageBean> imageList = new ArrayList<>();
        String image = item.image;
        if (!TextUtils.isEmpty(image)) {
            HomePageBeanBase.ImageBean img = new HomePageBeanBase.ImageBean();
            img.setImage(image);
            imageList.add(img);
        }
        bean.setImageList(imageList);
        List<ChannelBean.VideoFilesBean> videoFilesBeanList = new ArrayList<>();
        for (FileType video : item.videoFiles) {
            ChannelBean.VideoFilesBean videoFilesBean = new ChannelBean.VideoFilesBean();
            videoFilesBean.setFilesize(video.filesize);
            videoFilesBean.setMediaUrl(video.mediaUrl);
            videoFilesBean.setSpliteTime(video.spliteTime);
            videoFilesBean.setUseType(video.useType);
            videoFilesBeanList.add(videoFilesBean);
        }
        memberItem.setVideoFiles(videoFilesBeanList);
        memberItem.setDuration(item.duration);
        memberItem.setmUrl(item.mUrl);
        memberItem.setName(item.title);
        bean.setMemberItem(memberItem);
        return bean;
    }


    private enum Auto {
        autoNoRetry,//自动重试
        manually,//手动加载行为
        needAutoRetry;
    }//需要在失败时重试一次


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActivityMainTab) activity;
        mResource = mActivity.getResources();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVideoViewWarpper = new FrameLayout(mActivity);
        initVideoSkin();
    }

    private void initVideoSkin() {
        mUIPlayerContext = new UIPlayContext();
        mUIPlayerContext.skinType = VideoSkin.SKIN_TYPE_PIC;
        mUIPlayerContext.channelId = mChannel_id;
        mVideoSkin = new VideoSkin(mActivity);
        mVideoSkin.setId(R.id.video_skin);
        mVideoSkin.setVideoMargin(false);
        mVideoSkin.setOnLoadFailedListener(this);
        mVideoSkin.setNoNetWorkListener(this);
        mVideoSkin.setClipChildren(false);
        mVideoSkin.setClipToPadding(false);
        mVideoSkin.setOnClickVideoSkin(this);
        mVideoViewWarpper.addView(mVideoSkin);
        mVideoHelper = new NormalVideoHelper();
        mVideoHelper.init(mVideoSkin, mUIPlayerContext);
        mVideoHelper.setOnPlayCompleteListener(this);
        setVideoSkinListener();
        mPlayController = mVideoHelper.getPlayController();
        mPlayController.addUIObserver(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        super.onItemClick(parent, view, position, id);
        // picAdapter.recoverUI();
    }

    public String getChannelId() {
        return mChannel_id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        isVisible = true;
        mHidden = false;
        View rootView = inflater.inflate(R.layout.universal_channel_frg, container, false);
        initView(rootView);
        mFocusList = new ArrayList<>(20);
        return rootView;
    }

    private void setVideoSkinListener() {
        if (null != mVideoSkin.getPlayAdView()) {
            mVideoSkin.getPlayAdView().setOnPlayAndPrePlayClickListener(this);
        }
        if (null != mVideoSkin.getPlayView()) {
            mVideoSkin.getPlayView().setPlayOrPauseListener(this);
        }
        if (null != mVideoSkin.getBigPicAdView()) {
            mVideoSkin.getBigPicAdView().setOnClickAdView(this);
        }
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getBaseControlView().setShowListener(this);
            initDanmuViewStatus();
        }
        if (null != mVideoSkin.getDanmuEditView()) {
            mVideoSkin.getDanmuEditView().setEditListener(this);
        }
    }

    private void initDanmuViewStatus() {
        if (!IfengApplication.danmaSwitchStatus) {
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuEditView() && null != mVideoSkin.getBaseControlView()) {
                mVideoSkin.getDanmuView().hideView();
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
                mVideoSkin.getDanmuView().setShowEditView(false);
                mVideoSkin.getDanmuView().initDanmukuView();
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }
        } else {
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuEditView() && null != mVideoSkin.getBaseControlView()) {
                mVideoSkin.getDanmuView().showView();
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
                mVideoSkin.getDanmuView().initDanmukuView();
                mVideoSkin.getBaseControlView().setShowControlView(true);
            }
        }

    }


    private void initView(View rootView) {
        mPullToRefreshListView = (MyPullToRefreshListView) rootView.findViewById(R.id.page_listView);
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mPullToRefreshListView.setShowIndicator(false);
        mLandScapeContainer = (FrameLayout) rootView.findViewById(R.id.rl_landscape_container);
        mLandScapeContainer.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ListAdapter4BigPictureChannel(7, mActivity, this, mChannel_id);
        picAdapter = (ListAdapter4BigPictureChannel) mAdapter;
        picAdapter.setVideoHelper(mVideoHelper);
        picAdapter.setVideoSkinWrapper(mVideoViewWarpper);
        picAdapter.setUIPlayContext(mUIPlayerContext);
        picAdapter.setDanmuView(mVideoSkin.getDanmuView());
        mPullToRefreshListView.setAdapter(mAdapter);
        mHeadViewCount = mPullToRefreshListView.getRefreshableView().getHeaderViewsCount();
        initListener();
    }

    private void initListener() {
        mPullToRefreshListView.setOnRefreshListener(this);
        mPullToRefreshListView.setOnScrollListener(this);
        mPullToRefreshListView.setOnLastItemVisibleListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestNet();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVideoHelper != null) {
            mVideoHelper.onDestroy();
            if (null != mVideoSkin.getDanmuView()) {
                mVideoSkin.getDanmuView().onDestory();
            }
        }

        CommonStatictisListUtils.getInstance().sendStatictisList(mFocusList);
        mFocusList.clear();
    }

    @Override
    protected void requestNet() {
        updateViewStatus(Status.LOADING);
        mUpTimes = 0;
        requestNet(requireTime, DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, Auto.manually, "");
    }

    private void requestNet(final String position_id, final String count, final Status status, final int action, final String operation, final Auto auto, String upTimes) {
        logger.debug("requestNet");
        final boolean net = NetUtils.isNetAvailable(IfengApplication.getInstance());
        if (!net) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
        if (isLoading) {
            mPullToRefreshListView.onRefreshComplete();
            mPullToRefreshListView.hideFootView();
            return;
        }
        isLoading = true;
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String nw = NetUtils.getNetType(getActivity());
        ChannelDao.requestChannelData(mChannel_id, mChannel_Type, count, position_id, 0, ChannelBean.class,
                true, null, action, SharePreUtils.getInstance().getInreview(), operation, new User(IfengApplication.getAppContext()).getUid(),
                PhoneConfig.userKey, LastDocUtils.getLastDoc(), upTimes,
                province, city, nw, PhoneConfig.publishid,
                new Response.Listener<ChannelBean>() {
                    @Override
                    public void onResponse(ChannelBean response) {
                        isLoading = false;
                        if (response != null) {
                            List<ChannelBean.HomePageBean> body = response.getBodyList();
                            if (Status.FIRST == status) {
                                requireTime = response.getSystemTime();
                                //包含空曝光的list
                                List<ChannelBean.HomePageBean> list = filterList(body, null, status, false);
                                if (ListUtils.isEmpty(list)) {
                                    updateViewStatus(Status.DATA_ERROR);
                                } else {
                                    updateViewStatus(Status.REQUEST_NET_SUCCESS);
                                    mAdapter.setData(list);
                                    mAdapter.notifyDataSetChanged();
                                }

                            } else if (Status.REFRESH == status) {
                                requireTime = response.getSystemTime();
                                //包含空曝光的list
                                List<ChannelBean.HomePageBean> list = filterList(body, null, status, false);
                                if (net && !ListUtils.isEmpty(list)) {
                                    recoverUI();
                                    mAdapter.setData(list);
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    mAdapter.notifyDataSetChanged();
                                }
                                mPullToRefreshListView.onRefreshComplete();
                            } else {
                                mPullToRefreshListView.hideFootView();
                                List<ChannelBean.HomePageBean> list = filterList(body, mAdapter == null ? null : mAdapter.getDataList(), status, false);
                                if (ListUtils.isEmpty(list)) {
                                    onLoadMoreFail(auto, position_id, count, status, action, operation, net);
                                    return;
                                }
                                logger.debug("load more list size:{}", list.size());
                                mAdapter.addData(list, false);
                                mAdapter.notifyDataSetChanged();

                            }
                        } else {
                            if (Status.LOAD_MORE == status) {
                                mPullToRefreshListView.hideFootView();
                                onLoadMoreFail(auto, position_id, count, status, action, operation, net);
                            } else if (Status.FIRST == status) {
                                updateViewStatus(Status.DATA_ERROR);
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("volley onErrorResponse ! {}", error);
                        isLoading = false;
                        if (status == Status.REFRESH) {
                            mPullToRefreshListView.onRefreshComplete();
                            if (mAdapter != null && !ListUtils.isEmpty(mAdapter.getDataList())) {
                                mAdapter.notifyDataSetChanged();
                            }
                        } else if (status == Status.LOAD_MORE) {
                            mPullToRefreshListView.hideFootView();
                            onLoadMoreFail(auto, position_id, count, status, action, operation, net);
                        } else {
                            handleRequestError(status, error);
                        }
                    }
                });

        PageActionTracker.pullCh(status == Status.LOAD_MORE, mChannel_id);
    }

    private void onLoadMoreFail(Auto auto, String position_id, String count, Status status, int action, String operation, boolean net) {
        if (auto == Auto.needAutoRetry) {
            requestNet(position_id, count, status, action, operation, Auto.autoNoRetry, "");
        } else if (auto == Auto.autoNoRetry) {
            continuous_model_no_more = true;
        } else if (net && auto == Auto.manually) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
        }
    }


    private void setFullscreen(boolean on) {
        Window win = getActivity().getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!isVisible || mHidden) {
            logger.debug(mChannel_id + "--onConfigurationChanged isVisible:{}--mHidden:{}", isVisible, mHidden);
            return;
        }

        mVideoHelper.onConfigureChange(newConfig);
        //用流的标记模拟不发送V的标记
        mUIPlayerContext.isBigReportVideo = true;
        initAdViewPosition();

        if (Configuration.ORIENTATION_LANDSCAPE == mResource.getConfiguration().orientation) {
            doOrientationLandscape();
            PageActionTracker.endPageHomeCh(mChannel_id);
        } else if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            //切竖屏的时候，报横屏播放器page的统计
            PageActionTracker.endPageVideoPlay(true, "", "");
            doOrientationPortrait();
            if (null != mDanmakuEditFragment && !mDanmakuEditFragment.isHidden()) {
                mDanmakuEditFragment.dismissAllowingStateLoss();
            }
        }
        //用流的标记模拟不发送V的标记
        mUIPlayerContext.isBigReportVideo = false;

    }

    private void initAdViewPosition() {
        if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
            ValueAnimator mAnimator = null;
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(30.0f), DisplayUtils.convertDipToPixel(0));
            mAnimator.setDuration(100);
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null && mVideoSkin.getBigPicAdView().getTranslationY() != 0) {
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });
            mAnimator.setRepeatCount(0);
            mAnimator.start();
        }
    }


    private void setViewPagerScanScroll(boolean scroll) {
        mActivity.getFragmentHomePage().getViewPager().setScanScroll(scroll);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
            hasRegisterReceiver = true;
            mVideoHelper.onResume();
            danmuResume();
        }
        needRecover = false;
    }

    @Override
    public void onPause() {
        if (mVideoHelper != null && hasRegisterReceiver) {
            hasRegisterReceiver = false;
            mVideoHelper.onPause();
        }
        if (isNormalDanmaShow()) {
            mVideoSkin.getDanmuView().onPause();
        }

        if (mActivity.isLandScape()) {
            needRecover = true;
        } else {
            recoverUI();
        }
        super.onPause();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        logger.debug(mChannel_id + "--onHiddenChanged:{}", hidden);
        mHidden = hidden;
        if (hidden) {
            if (mVideoHelper != null && hasRegisterReceiver) {
                hasRegisterReceiver = false;
                mVideoHelper.onPause();

            }
            if (isNormalDanmaShow()) {
                mVideoSkin.getDanmuView().onPause();
            }

            recoverUI();
        } else {
            if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
                hasRegisterReceiver = true;
                mVideoHelper.onResume();
            }

            danmuResume();
        }
    }

    private void danmuResume() {
        if (null != mVideoSkin && null != mVideoSkin.getDanmuView()) {
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getBaseControlView().setShowControlView(true);
            } else {
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }
            mVideoSkin.getDanmuView().onResume();
        }
    }

    public void setUserVisibleHint(boolean isVisibleToUser) {
        isVisible = isVisibleToUser;
        SharePreUtils.getInstance().getVisibleMap().put(mChannel_id, isVisible);
        if (isVisibleToUser) {
            if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
                hasRegisterReceiver = true;
                mVideoHelper.onResume();
            }

            danmuResume();

            if (EmptyUtils.isNotEmpty(mActivity) && getPiPMode()) {
                List<ChannelBean.HomePageBean> list = picAdapter.getDataList();
                for (int i = 0; i < list.size(); i++) {
                    if (EmptyUtils.isNotEmpty(list.get(i).getMemberItem())
                            && EmptyUtils.isNotEmpty(list.get(i).getMemberItem().getGuid())
                            && list.get(i).getMemberItem().getGuid().equals(mActivity.mCurrentPiPVideoGuid)) {
                        i++;
                        if (i >= mListViewVisibleItem.get(0) && i <= (mListViewVisibleItem.get(0) + mListViewVisibleItem.get(1) - 1)) {
                            scrollToCurrentPiPPositionAndPlay(500, true, i - 1);
                        }
                        break;
                    }
                }
            }

        } else {
//            if (mVideoHelper != null && hasRegisterReceiver) {
//                hasRegisterReceiver = false;
//                mVideoHelper.onPause();
//            }

//            if (isNormalDanmaShow()) {
//                mVideoSkin.getDanmuView().onPause();
//            }

            recoverUI();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private boolean isNormalDanmaShow() {
        return null != mVideoSkin && null != mVideoSkin.getDanmuView();
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        super.onRefresh(refreshView);
        logger.debug("onRefresh");
        requestNet(requireTime, DataInterface.PAGESIZE_20, Status.REFRESH, ChannelDao.REFRESH, ChannelConstants.DOWN, Auto.manually, String.valueOf(++mUpTimes));
    }

    public void onLastItemVisible() {
        logger.debug("onLastItemVisible");
        if (continuous_model) {
            return;
        }
        super.onLastItemVisible();
        if (mAdapter != null) {
            HomePageBeanBase homePageBean = mAdapter.getLastItem();
            if (homePageBean != null && !TextUtils.isEmpty(homePageBean.getItemId())) {
                requestNet(homePageBean.getItemId(), DataInterface.PAGESIZE_20, Status.LOAD_MORE, ChannelDao.LOAD_MORE, ChannelConstants.UP, Auto.manually, "");
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        logger.debug("onScrollStateChanged:" + scrollState);
        if (AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL == scrollState) {
            continuous_model = false;
            continuous_model_no_more = false;
        }

    }

    //    private boolean isRecovered = false;
    private List<Integer> mListViewVisibleItem = Arrays.asList(0, 1);

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        mListViewVisibleItem.set(0, firstVisibleItem);
        mListViewVisibleItem.set(1, visibleItemCount);
        if (picAdapter.getClickToPlayPositon() > -1 &&
                (firstVisibleItem + visibleItemCount - mHeadViewCount == picAdapter.getClickToPlayPositon()
                        || firstVisibleItem + visibleItemCount == picAdapter.getClickToPlayPositon()
                        || picAdapter.getClickToPlayPositon() + 1 == firstVisibleItem - mHeadViewCount
                        || picAdapter.getClickToPlayPositon() + 2 == firstVisibleItem - mHeadViewCount)) {


            continuous_model = false;
            continuous_model_no_more = false;
//            if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
//                if (mUIPlayerContext.status == IPlayer.PlayerState.STATE_PLAYING) {
//                    isRecovered = true;
//                }
//                if (mUIPlayerContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
//                    isRecovered = false;
//                }
//            }
            if (picAdapter.playStatus == STATUS_PLAYING) {
                recoverUI(SharePreUtils.getInstance().getPiPModeState());
            } else {
                recoverUI();
            }
        }

        if (EmptyUtils.isNotEmpty(mActivity) && getPiPMode() && isVisible) {
            List<ChannelBean.HomePageBean> list = picAdapter.getDataList();
            for (int i = 0; i < list.size(); i++) {
                if (EmptyUtils.isNotEmpty(list.get(i).getMemberItem())
                        && EmptyUtils.isNotEmpty(list.get(i).getMemberItem().getGuid())
                        && list.get(i).getMemberItem().getGuid().equals(mActivity.mCurrentPiPVideoGuid)) {
                    ListView listView = mPullToRefreshListView.getRefreshableView();
                    int headerCount = listView.getHeaderViewsCount();
                    if ((i + headerCount) >= firstVisibleItem && (i + headerCount) <= (firstVisibleItem + visibleItemCount - 1)) {
                        scrollToCurrentPiPPositionAndPlay(1500, true, i);
                    }
                    break;
                }
            }
        }
    }


    private void scrollToCurrentPiPPositionAndPlay(final int mScrollDuration, final boolean isPortrait, final int index) {
        logger.debug("scrollToCurrentPiPPositionAndPlay:");
        continuous_model = true;
        final HomePageBeanBase base = picAdapter.getDataList().get(index);
        if (base == null) {
            return;
        }
        final ListView listView = mPullToRefreshListView.getRefreshableView();
//        final View current = getViewByPosition(listView, index);
//        if (current != null) {
//            final int top = current.getTop() + current.getHeight();
//            listView.smoothScrollBy(top, mScrollDuration);
//        }
        if (CheckIfengType.isAD(base.getMemberType()) || AdTools.videoType.equals(base.getMemberItem().adConditions.showType)) {
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int i = index;
                    scrollToCurrentPiPPositionAndPlay(mScrollDuration, isPortrait, i++);
                }
            }, mScrollDuration);
        } else {
            final int next_index = index;
            mVideoHelper.setBookMark(mActivity != null ? mActivity.getPiPPlayerLastPosition() : 0);
            mVideoHelper.setPlayPathFromPip(mActivity.getPipCurrentPath());
            picAdapter.autoPlayNext(base, next_index, getViewByPosition(listView, next_index), isPortrait);
            mActivity.removePiPViews();
        }
    }

    public void recoverUI() {
        if (picAdapter != null) {
            if (mUIPlayerContext.videoType != null) {
                if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                    initAdViewPosition();
                }
            }
            picAdapter.recoverUI();
        }
    }

    public void recoverUI(boolean showPiP) {
        if (picAdapter != null) {
            if (mUIPlayerContext.videoType != null) {
                if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                    initAdViewPosition();
                }
            }
//            picAdapter.recoverUI();
            picAdapter.recoverUI(showPiP, mOnListViewScrollToPiPVideoView);
        }
    }

    private void doOrientationPortrait() {
        mPullToRefreshListView.setVisibility(View.VISIBLE);
        mLandScapeContainer.setVisibility(View.GONE);
        mActivity.setTabVisible(View.VISIBLE);
        setFullscreen(false);
        if (needRecover) {
            needRecover = false;
            recoverUI();
        } else {
            picAdapter.back2PortraitAndContinuePlay();
        }
        setViewPagerScanScroll(true);

    }

    private void doOrientationLandscape() {
        if (mVideoViewWarpper.getParent() != null) {
            ViewGroup parent = (ViewGroup) mVideoViewWarpper.getParent();
            parent.removeView(mVideoViewWarpper);
        }
        mLandScapeContainer.addView(mVideoViewWarpper);
        mPullToRefreshListView.setVisibility(View.GONE);
        mActivity.setTabVisible(View.GONE);
        mActivity.hideOnLineView();
        mLandScapeContainer.setVisibility(View.VISIBLE);
        setFullscreen(true);
        setViewPagerScanScroll(false);
        if (isNormalDanmaShow()) {
            if (!isDanmuSendStatus) {
                mVideoSkin.getDanmuEditView().hideView();
            } else {
                mVideoSkin.getDanmuEditView().showView();
            }
        }
        if (picAdapter.isPlaying) {
            picAdapter.continuePlay();
        }
    }

    @Override
    public void onPlayComplete() {
        logger.debug("onPlayComplete");
        if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
            if (mActivity.isLandScape()) {
                needRecover = true;
                mActivity.toPortrait();
            }
            mVideoSkin.showControllerView();
            if (picAdapter.adVideoRecord != null) {
                picAdapter.adVideoRecord.setCompleteNum();
                picAdapter.adVideoRecord.stopPlayTime();
            }
            return;
        }

        picAdapter.setCurrentPositionNeedReopen();
        final int reopen_position = picAdapter.getNeedReopenPositon();
        if (continuousModelLoadMore(reopen_position)) {
            HomePageBeanBase homePageBean = mAdapter.getLastItem();
            if (homePageBean != null && !TextUtils.isEmpty(homePageBean.getItemId())) {
                //TODO:ChannelConstants.UP 是否改成 ChannelConstants.AUTO
                requestNet(homePageBean.getItemId(), DataInterface.PAGESIZE_20, Status.LOAD_MORE, ChannelDao.LOAD_MORE, ChannelConstants.UP, Auto.needAutoRetry, "");
            }
        }
        if (mActivity.isLandScape()) {
            if (picAdapter.hasNext(reopen_position)) {
                //show the last portrait container's childe view to make sure itemView display rigth height
                //FIXME:联播状态下全屏模式时,触发预加载时,getViewByPosition返回异常的情况,未再复现
                ViewUtils.showChildrenView(picAdapter.getPortraitPlayingContainer());
                scollToNextPositionAndPlay(500, false);

            } else {
                logger.debug("has no next and rec over to portrait");
                continuous_model = false;
                continuous_model_no_more = false;
                needRecover = true;
                mActivity.toPortrait();
            }

            destoryAndCreatDanmakuView();

        } else {
            preGuid = "";
            recoverUI();
            if (picAdapter.hasNext(reopen_position)) {
                scollToNextPositionAndPlay(2000, true);
            } else {
                continuous_model = false;
                continuous_model_no_more = false;
            }
        }
    }

    private void destoryAndCreatDanmakuView() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().clearDanmaku();
            mVideoSkin.getDanmuView().onDestory();

            mVideoSkin.getDanmuView().showView();
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getDanmuView().showDanmukuView();
            } else {
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
            }
        }
    }

    @Override
    public void onNoNetWorkClick() {
        pauseAndHideDanmaview();
        picAdapter.continuePlay();
    }

    private void pauseAndHideDanmaview() {
        if (null != mVideoSkin.getDanmuView()) {
            if (null == mVideoSkin.getDanmuView().getDanmakuView()) {
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
            }
            mVideoSkin.getDanmuView().getDanmakuView().pause();
            mVideoSkin.getDanmuView().getDanmakuView().hide();
        }
    }

    @Override
    public void onMobileClick() {
        pauseAndHideDanmaview();
        picAdapter.continuePlay();
    }

    @Override
    public void onLoadFailedListener() {
        pauseAndHideDanmaview();
        picAdapter.continuePlay();
    }

    private void scollToNextPositionAndPlay(final int mScrollDuration, final boolean isPortrait) {
        logger.debug("scollToNextPositionAndPlay:");
        continuous_model = true;
        final HomePageBeanBase base = picAdapter.getNextBeanWhenPlayComplete();
        if (base == null) {
            return;
        }
        final ListView listView = mPullToRefreshListView.getRefreshableView();
        final int index = picAdapter.getClickToPlayPositon();
        final View current = getViewByPosition(listView, index);
        if (current != null) {
            final int top = current.getTop() + current.getHeight();
            listView.smoothScrollBy(top, mScrollDuration);
        }
        if (CheckIfengType.isAD(base.getMemberType()) || AdTools.videoType.equals(base.getMemberItem().adConditions.showType)) {
            picAdapter.increaseClickToPlayPosition();
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scollToNextPositionAndPlay(mScrollDuration, isPortrait);
                }
            }, mScrollDuration);
        } else {
            final int next_index = index + 1;
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    picAdapter.autoPlayNext(base, next_index, getViewByPosition(listView, next_index), isPortrait);
                }
            }, mScrollDuration);
        }
    }

    private View getViewByPosition(ListView listView, int adapterPosition) {
        final int headerCount = listView.getHeaderViewsCount();
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = listView.getLastVisiblePosition();
        int position = adapterPosition + headerCount;
        logger.debug("firstListItemPosition:" + firstListItemPosition + "" +
                "\n lastListItemPosition:" + lastListItemPosition + "" + "\n adapterPosition:" + adapterPosition +
                "\n position:" + position);
        if (position < firstListItemPosition || position > lastListItemPosition) {
            logger.debug("越界将导致播放异常");
//            return listView.getAdapter().getView(position-headerCount, null, listView);
            return null;
        } else {
            final int childIndex = position - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    private boolean continuousModelLoadMore(int positon) {
        if (continuous_model_no_more) {
            return false;
        }
        final List list = picAdapter.getDataList();
        if (list == null || list.isEmpty()) {
            return true;
        }
        if (oldDataLenght == -1 || oldDataLenght <= positon) {
            oldDataLenght = list.size();
        }
        return oldDataLenght == list.size() && (list.size() <= 5 || positon >= (list.size() - 5));
    }

    @Override
    public void showControllerView() {
        ValueAnimator mAnimator = null;
        mVideoSkin.getBigPicAdView().clearAnimation();

        if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(0), -DisplayUtils.convertDipToPixel(30.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });
            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        } else {
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(0), -DisplayUtils.convertDipToPixel(40.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().bringToFront();
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });

            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();
        }
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mVideoSkin.bringChildToFront(mVideoSkin.getBigPicAdView());
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
    }

    @Override
    public void hiddenControllerView() {
        ValueAnimator mAnimator = null;
        mVideoSkin.getBigPicAdView().clearAnimation();

        if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            mAnimator = ValueAnimator.ofFloat(-DisplayUtils.convertDipToPixel(30.0f), DisplayUtils.convertDipToPixel(0));
            mAnimator.setDuration(200);
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null && mVideoSkin.getBigPicAdView().getTranslationY() != 0) {
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        } else {
            mAnimator = ValueAnimator.ofFloat(-DisplayUtils.convertDipToPixel(40.0f), DisplayUtils.convertDipToPixel(0.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    //将子view置顶
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().bringToFront();
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });

            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        }

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mVideoSkin.bringChildToFront(mVideoSkin.getBigPicAdView());

            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
    }

    @Override
    public void onPlayClick() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }

    @Override
    public void prePlayClick() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.setPrePlayNum();
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }


    @Override
    public void onPausePlayButton() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.setPauseNum();
            picAdapter.adVideoRecord.stopPlayTime();
            initAdViewPosition();
        }
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }
        picAdapter.playStatus = STATUS_PAUSE;
    }

    @Override
    public void onPlayButton() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }
        picAdapter.playStatus = STATUS_PLAYING;
    }

    private boolean isDanmuSendStatus;

    public void setDanmuSendStatus(boolean isSend) {
        this.isDanmuSendStatus = isSend;
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().setShowEditView(isSend);
        }
    }

    //针对不同guid，pauseTime = 0;
    public void setDanmaData(String guid, List<VideoDanmuItem> danmaData) {
        if (null == mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
        }

        // 待用
        if (!TextUtils.isEmpty(preGuid) && preGuid.equalsIgnoreCase(guid)) {
            mVideoSkin.getDanmuView().currentVideoPosition((int) (mVideoHelper.getLastPosition() / 1000));
        } else {
            preGuid = guid;
            mVideoSkin.getDanmuView().currentVideoPosition(0);
        }

        mVideoSkin.getDanmuView().addDataSource(danmaData);

    }

    @Override
    public void showDanmuView(boolean isShow) {
        if (isShow) {
            mVideoSkin.getDanmuView().showView();
            if (isDanmuSendStatus) {
                mVideoSkin.getDanmuEditView().showView();
            }
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            IfengApplication.danmaSwitchStatus = true;
            mVideoSkin.getDanmuView().showDanmukuView();
            if (mVideoSkin.getDanmuView().getDanmakuView().isPaused()) {
                mVideoSkin.getDanmuView().getDanmakuView().resume();
            }
            if (mUIPlayerContext.status != IPlayer.PlayerState.STATE_PAUSED) {
                mVideoSkin.getDanmuView().resumeDanmaku();
            } else {
                mVideoSkin.getDanmuView().getDanmakuView().pause();
            }
        } else {
            mVideoSkin.getDanmuView().hideView();
            mVideoSkin.getDanmuView().hideDanmakuView();
            mVideoSkin.getDanmuEditView().hideView();
            IfengApplication.danmaSwitchStatus = false;
        }
    }

    @Override
    public void onClickEditButton() {
        if (ScreenUtils.isLand()) {
            if (mVideoHelper != null) {
                mVideoHelper.onPause();
            }
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
                mVideoSkin.getDanmuView().getDanmakuView().pause();
            }
            showEditDanmaWindow("", "");
        } else {
            //播放底页
            picAdapter.toVideoActivityForDanmu();
        }

    }

    public void showEditDanmaWindow(String comments, String comment_id) {
        if (mDanmakuEditFragment == null) {
            mDanmakuEditFragment = new DanmakuCommentFragment();
        }

        mDanmakuEditFragment.setShowInputMethod(false);
        mDanmakuEditFragment.setCommends(comments, comment_id);
        mDanmakuEditFragment.setVideoItem(mUIPlayerContext.videoItem);
        mDanmakuEditFragment.setNormalVideoPlayer(mVideoHelper);
        mDanmakuEditFragment.setFragment(this);

        if (!mDanmakuEditFragment.isAdded()) {
            mDanmakuEditFragment.show(getActivity().getSupportFragmentManager(), "danmuDialog");

        }

    }

    public void updateLocalDanmu(String comment) {

        if (null != mVideoHelper) {
            mVideoHelper.onResume();
        }

        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().sendTextMessage(comment);
        }

    }


    @Override
    public void onClickStopUrl() {
        recoverUI();
    }

    @Override
    public void onClickAdToKnow() {
        recoverUI();
    }
}
