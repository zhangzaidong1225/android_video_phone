package com.ifeng.newvideo.ui;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TabHost;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.ifeng.core.fragment.IfengNewsFragment;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.badge.BadgeUtils;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.constants.PageRefreshConstants;
import com.ifeng.newvideo.keepAlive.KeepAliveJobService;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.push.PushReceiver;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.StatisticsConstants;
import com.ifeng.newvideo.statistics.domains.NewsSDKInRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.listener.InterfaceLiveHandle;
import com.ifeng.newvideo.ui.live.listener.LiveMotionEvent;
import com.ifeng.newvideo.ui.live.listener.LiveWindowFocusChangeListener;
import com.ifeng.newvideo.ui.live.weblive.ActivityH5Live;
import com.ifeng.newvideo.ui.live.weblive.FragmentWebLive;
import com.ifeng.newvideo.ui.maintab.ChannelManagerFragment;
import com.ifeng.newvideo.ui.maintab.LiveData;
import com.ifeng.newvideo.utils.ActivityUtils;
import com.ifeng.newvideo.utils.CheckStealUtils;
import com.ifeng.newvideo.utils.IPushUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.NotificationCheckUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.utils.TabIconUpdateUtils;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.utils.Util4act;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerLive;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.FloatVideoViewSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.widget.FragmentTabHost;
import com.ifeng.newvideo.widget.RedPointImageView;
import com.ifeng.newvideo.widget.ViewPagerColumn;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.BuildUtils;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.SharePreConstants;
import com.ifeng.video.dao.db.dao.ADInfoDAO;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.dao.ClientInfoDAO;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.model.ADInfoModel;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.ClientInfo;
import com.ifeng.video.dao.db.model.TabIconConfigModel;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.player.ChoosePlayerUtils;
import com.video.videosdk.MediaPlayerSDKIfeng;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 主界面
 * Created by antboyqi on 14-8-6.
 */
public class ActivityMainTab extends BaseFragmentActivity implements FragmentMine.onClickPointListener, VideoSkin.OnNetWorkChangeListener, VideoSkin.OnLoadFailedListener {

    private static final Logger logger = LoggerFactory.getLogger(ActivityMainTab.class);

    public static final String TAB_HOME = "首页";
    public static final String TAB_LIVE = "直播";
    public static final String TAB_SUB = "订阅";
    public static final String TAB_MY = "我的";
    public static final String TAB_NEWS = "新闻";

    public static String currentFragmentTag = "";//用于标记当前点击展示的fragment
    public static boolean isActivityMainTabShowing = false;//标记主activity的可见状态,用于控制轮播图广告曝光

    public static final int POS_HOME = 0;
    public static final int POS_NEWS = POS_HOME + 1;
    public static final int POS_LIVE = POS_NEWS + 1;
    public static final int POS_SUB = POS_LIVE + 1;
    public static final int POS_MY = POS_SUB + 1;

    @Override
    public void onNoNetWorkClick() {
        mPlayController.playSource(getPipCurrentPath());
    }

    @Override
    public void onMobileClick() {
        mPlayController.playSource(getPipCurrentPath());
    }

    @Override
    public void onLoadFailedListener() {
        mPlayController.playSource(getPipCurrentPath());
    }

    public String getPipCurrentPath() {
        return StreamUtils.getMediaUrlForPic(mUiPlayContext.videoFilesBeanList);
    }

    @IntDef({POS_HOME, POS_NEWS, POS_LIVE, POS_SUB, POS_MY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TabIndex {
    }

    //每天一次的数据上报,失败后重试次数
    public int retryTime = 1;

    private final List<ADInfoModel> adInfoModels = new ArrayList<ADInfoModel>();

    /**
     * 一像素边,底部导航和上面view间的一条线
     */
    private View mSplitLineView;

    // 百度lbs定位
    private LocationClient mLocationClient;
    private BDLocationListener mLocationListener;
    private int mLocateTryTime;
    private boolean hasReceivedLocation = false;

    //直播touch事件回调
    private InterfaceLiveHandle interfaceLiveHandle;
    // Audio Float View
    private View mGifView;
    private View mSignView;
    private FragmentTabHost mTabHost;
    private FragmentLive liveFrag;
    private boolean isShowGifView = false;//是否显示GifView
    private boolean isShowOnLineView = true;
    public static boolean mIsRunning; // 判断当前Activity是否启动Ø
    private LiveWindowFocusChangeListener liveWindowFocusChangeListener;

    public FragmentHomePage getFragmentHomePage() {
        return mFragmentHomePage;
    }

    private FragmentHomePage mFragmentHomePage;

    private ADInfoDAO adDao;

    private static boolean isChannelManageFragmentShow;

    public static AudioService mAudioService;
    private FloatViewReceiver mFloatViewReceiver;

    // 配置接口中的底部导航图片url
    private TabIconConfigModel mLocalTabIconModel;

    private String homeNormal = "";
    private String homeSelected = "";
    private String liveNormal = "";
    private String liveSelected = "";
    private String subNormal = "";
    private String subSelected = "";
    private String myNormal = "";
    private String mySelected = "";
    private String newsNormal = "";
    private String newsSelected = "";
    private String lineColor = "";
    private String backgroundColor = "";

    private RedPointImageView mainTabView;
    private RedPointImageView liveTabView;
    private RedPointImageView subTabView;
    private RedPointImageView myTabView;
    private RedPointImageView newsTabView;

    private Context mContext;

    private FrameLayout mFloatVideoView;
    private FloatVideoViewSkin mSkin;
    private UIPlayContext mUiPlayContext;
    private FloatVideoPlayController mPlayController;
    private static boolean isPiPMode = false;
    public String mCurrentPiPVideoGuid;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_maintab);
        mContext = this;
        // showIfengTVGuide(getApp());
        showHomeGuideView();
        setExitWithToast(true);
        initTabs();

        initIfengPlayerIfNeeded();

        initAudioFloatGifView();
        initOnLineView();
        initFloatVideoView();
        mIsRunning = true;
        initIntent(getIntent());
        registerFloatViewReceiver();
        registerHomeKeyReceiver(this);
        registerLoginBroadcast();
        getLocation();

        TabIconUpdateUtils.updateTabBarIconConfigs();
        UserFeed.getIfengAddress(this);
        getCoverStoryAdData();
        CheckStealUtils.checkStealUrl();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            KeepAliveJobService.startJobScheduler();
        }
//        MobileFlowUtils.getInstance(this).checkMobileFlow();
        openNotificationSwitch();

    }

    private void initIfengPlayerIfNeeded() {
        if (!ChoosePlayerUtils.useIJKPlayer(this)) {
            MediaPlayerSDKIfeng.init(IfengApplication.getInstance(), "http://v.ifeng.com/appData/video/player_config.js",
                    PhoneConfig.publishid, PhoneConfig.UID, PhoneConfig.softversion, "videoapp", "IFeng_Video", "");
        }
    }

    private boolean isFromPush(Intent intent) {
        return intent != null && intent.getBooleanExtra(IntentKey.IS_FROM_PUSH, false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        logger.debug("onNewIntent");
        initIntent(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityMainTabShowing = false;
        removePiPViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityMainTabShowing = true;
        if (!PhoneConfig.isGooglePlay()) {
            VideoAdConfigDao.getAdconfig();
        }
        if (toOpenNotification) {
            if (NotificationCheckUtils.notificationIsOpen(this) && !SharePreUtils.getInstance().getPushMessageState()) {
                SharePreUtils.getInstance().setPushMessageState(true);
            }
        }
        uploadClientInfoAd();
        BadgeUtils.clearBadge(this);
        requestPointCount();
    }

    private void getCoverStoryAdData() {
        if (PhoneConfig.isGooglePlay()) {
            return;
        }
        adDao = ADInfoDAO.getInstance(this);
        ADInfoDAO.getCoverStoryADInfo(
                new Response.Listener() {
                    @Override
                    public void onResponse(Object result) {
                        if (result != null) {
                            List<ADInfoModel> data = ADInfoModel.parsesCoverStoryADJson(result.toString());
                            if (!ListUtils.isEmpty(data)) {
                                adInfoModels.addAll(data);
                            }
                        }
                        getCoverStoryData();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        logger.error("failed to get getCoverStoryAdData ! {}", volleyError);
                        getCoverStoryData();
                    }
                },
                Util4act.buildUrlParams4CoverStoryAD());
    }

    private void getCoverStoryData() {
        ADInfoDAO.getCoverStoryInfo(
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response != null) {
                            List<ADInfoModel> data = ADInfoModel.parsesCoverStoryJson(response.toString());
                            if (!ListUtils.isEmpty(data)) {
                                adInfoModels.addAll(data);
                            }
                        }
                        adDao.addDbCacheWithDelte(adInfoModels);
                        loadSplashADImage(adInfoModels);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("failed to get getCoverStoryData , clear ad table !! {}", error);
                        //本次没取到广告，直接清表，
                        adDao.addDbCacheWithDelte(adInfoModels);
                        loadSplashADImage(adInfoModels);
                    }
                });
    }

    /**
     * 上报客户端设备信息
     */
    private void uploadClientInfoAd() {
        if (!NetUtils.isNetAvailable(ActivityMainTab.this)) {
            return;
        }
        long uploadTime = mSharePreUtils.getUploadDate();
        long serverTime = mSharePreUtils.getServerTime();

        if (!DateUtils.isBeforeDate(uploadTime, serverTime)) {
            logger.debug("---->uploadClientInfoAd today has uploaded ! so return !");
            return;
        }

        boolean isValidUploadUrl = VideoAdConfigDao.adConfig == null
                || VideoAdConfigDao.adConfig.getInfoAD() == null
                || TextUtils.isEmpty(VideoAdConfigDao.adConfig.getInfoAD().getAdUpdateUrl());
        if (isValidUploadUrl) {
            logger.info("uploadClientInfoAd adUpdateUrl is empty! so return !");
            return;
        }

        uploadingInfoAd(getClientInfoRequestParams());
    }

    private ClientInfo getClientInfoRequestParams() {
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setUid(PhoneConfig.UID);
        clientInfo.setBrand(PhoneConfig.BRAND);
        clientInfo.setAndroid_id("");
        clientInfo.setModel(Build.MODEL);
        clientInfo.setDevice(PhoneConfig.ua_for_video);
        clientInfo.setManufacturer(PhoneConfig.MANUFACTURER);
        clientInfo.setOs_version(PhoneConfig.mos);
        clientInfo.setOs_type(ClientInfoDAO.OS_TYPE_ANDROID);
        clientInfo.setScreen_density(PhoneConfig.screenDensityDpi);
        clientInfo.setScreen_width(PhoneConfig.screenWidth);
        clientInfo.setScreen_height(PhoneConfig.screenHeight);
        clientInfo.setCarries(PhoneConfig.getSimCode(this));
        clientInfo.setImei(PhoneConfig.IMEI);
        String mac = "";
        try {
            mac = StringUtils.SHA1(PhoneConfig.getDeviceMac()).hashCode() + "";
        } catch (Exception e) {
        }
        clientInfo.setMac(mac);
        clientInfo.setLanguage(PhoneConfig.getPhoneLanguage());
        clientInfo.setSign(StringUtils.md5s(clientInfo.getUid() + ClientInfoDAO.APP_NAME));
        return clientInfo;
    }

    private void uploadingInfoAd(final ClientInfo clientInfo) {
        try {
            String clientInfoString = JSON.toJSONString(clientInfo);
            logger.debug("----> ClientInfo is {}", clientInfoString);

            ClientInfoDAO.uploadDeviceInfo(VideoAdConfigDao.adConfig.getInfoAD().getAdUpdateUrl(),
                    clientInfoString,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            logger.debug("----> ClientInfo uploadDeviceInfo success");
                            SharePreUtils.getInstance().setUploadDate(SharePreUtils.getInstance().getServerTime());
                            VolleyHelper.getRequestQueue().cancelAll(ClientInfoDAO.TAG_CLIENT_INFO);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            logger.error("----> ClientInfo uploadDeviceInfo error  {} ", error);
                            VolleyHelper.getRequestQueue().cancelAll(ClientInfoDAO.TAG_CLIENT_INFO);
                            if (retryTime > 0) {
                                retryTime--;
                                uploadingInfoAd(clientInfo);
                            }
                        }
                    });
        } catch (Exception e) {
            logger.error("----> ClientInfo uploadDeviceInfo Exception  {} ", e);
        }
    }

    /**
     * 预加载splash页广告图片
     *
     * @param splashADInfoModels
     */
    private void loadSplashADImage(List<ADInfoModel> splashADInfoModels) {
        if (ListUtils.isEmpty(splashADInfoModels)) {
            return;
        }
        for (ADInfoModel adInfoModel : splashADInfoModels) {
            String imageUrl = adInfoModel.getImage();
            if (StringUtils.isBlank(imageUrl)) {
                continue;
            }
            VolleyHelper.getAdImageLoader().get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    logger.debug("Load splash AD image success!  bitmap = {}", response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error("Load splash AD image fail! {}", error.getMessage());
                }
            });
        }
    }

    /**
     * 主要用于处理几种不同的启动方式
     */
    private void initIntent(Intent intent) {
        // 推送
        if (sendPushMsg(intent)) {
            CustomerStatistics.inType = StatisticsConstants.APPSTART_TYPE_FROM_PUSH;
            return;
        }
        //闹钟
        if (isAlarmIntent(intent)) {
            return;
        }

        // H5拉起
        if (initH5Intent(intent)) {
            return;
        }

        //直播预约拉起
        if (dealByLiveNotify(intent)) {
            return;
        }
        //进直播页
        if (jump2LiveChannel(intent)) {
            return;
        }
        // 启动页广告跳转
        if (isLaunchFromSplashAD(intent)) {
            toADActivity(intent);
        }
    }

    private void toADActivity(Intent intent) {
        ADInfoModel adInfoModel = (ADInfoModel) intent.getSerializableExtra(IntentKey.AD_MODEL_FROM_SPLASH_ACTIVITY);
        if (adInfoModel == null) {
            return;
        }
        // 广告部的异步点击地址SDK上报
        if (!TextUtils.isEmpty(adInfoModel.getAsyncClick())) {
            ArrayList<String> asyncClickList = new ArrayList<String>();
            for (String asyncClick : Arrays.asList(adInfoModel.getAsyncClick().split(";"))) {
                asyncClickList.add(asyncClick);
            }
            AdvertExposureDao.sendAdvertClickReq(adInfoModel.getADId(), asyncClickList);
        }

        IntentUtils.startADActivity(this, adInfoModel.getADId(), adInfoModel.getClickUrl(),
                adInfoModel.getClickUrl(), ADActivity.FUNCTION_VALUE_AD, adInfoModel.getTitle(), null, adInfoModel.getImage(), "", "", null, null);
    }

    private boolean isAlarmIntent(Intent intent) {
        if (intent == null) {
            return false;
        }

        if (!TextUtils.isEmpty(intent.getStringExtra("url"))) {

            String title = intent.getStringExtra("title");
            String url = intent.getStringExtra("url");
            String aid = intent.getStringExtra("aid");
            Intent liveIntent = new Intent(this, ActivityH5Live.class);
            liveIntent.putExtra("url", url);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(liveIntent);
//            IntentUtils.startADActivity(this, aid, url,
//                    url, null, title, "", "", "", null, null);
            return true;
        }
        return false;
    }


    private boolean jump2LiveChannel(Intent intent) {
        logger.debug("jump2LiveChannel:{}", intent);
        if (intent == null) {
            return false;
        }
//        if (mTabHost.getCurrentTab() == POS_LIVE) {
//            return false;
//        }
        boolean isFromAudioService = intent.getBooleanExtra(AudioService.EXTRA_FROM, false);
        if (isFromAudioService) {
            TVLiveInfo tvLiveInfo = PlayQueue.getInstance().getTVLiveInfo();
            //直播推送出现在这
            intent = new Intent(this, ActivityVideoPlayerLive.class);
            liveData = new LiveData(tvLiveInfo.getChannelId(), "", "", tvLiveInfo.getTitle(), "", false, false);
            intent.putExtra("liveData", liveData);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            setCurrentTabToLive(tvLiveInfo == null ? "" : tvLiveInfo.getChannelId(), "", "", "", "", false);
//            if (mFragmentHomePage != null) {
//                mFragmentHomePage.closeChannelMangerFragment();
//            }
            return true;
        }
        return false;
    }

    /**
     * 由推送到达
     */
    private boolean sendPushMsg(Intent intent) {
        if (intent == null) {
            return false;
        }
        if (isFromPush(intent)) {
            String memberType = intent.getStringExtra(PushReceiver.PUSH_TYPE);
            String pushId = intent.getStringExtra(PushReceiver.PUSH_ID);
            boolean isDownload = intent.getBooleanExtra(PushReceiver.KEY_PUSH_DOWNLOAD, false);
            logger.debug("sendPushMsg isPush = true, memberType={}, pushId={},isDownload={}", memberType, pushId, isDownload);
            // 专题
            if (CheckIfengType.isTopicType(memberType)) {
                IntentUtils.toTopicFromPush(this, memberType, pushId);
            }
            // 单条
            else if (CheckIfengType.isVideo(memberType)) {
                if (pushId.length() > 20) {
                    if (isDownload) {
                        IntentUtils.startCacheAllActivityFromPush(this, pushId, null, null, memberType);
                    } else {
                        IntentUtils.toVodVideoActivityFromPush(this, pushId, null, null);
                    }
                } else {
                    if (isDownload) {
                        IntentUtils.startCacheAllActivityFromPush(this, null, pushId, null, memberType);
                    } else {
                        IntentUtils.toVodVideoActivityFromPush(this, null, pushId, null);
                    }
                }
            }
            // 直播
            else if (CheckIfengType.isLiveType(memberType)) {
                //推送TV
                intent = new Intent(this, ActivityVideoPlayerLive.class);
                liveData = new LiveData(pushId, "", "", "", "", true, false);
                intent.putExtra("liveData", liveData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

//                setCurrentTabToLive(pushId, "", "", "", "", true, true);//来自推送

                //if (CheckIfengType.isClassicsLiveType(memberType)) {
                //    IntentUtils.toLiveDetailActivityWithIndex(this, pushId, "");
                //} else {
                //    IntentUtils.toLiveDetailActivityWithIndexCMCC(this, pushId, intent.getStringExtra(PushReceiver.PUSH_CONTID), "", intent.getStringExtra(PushReceiver.PUSH_IMAGE));
                //}

            }
            // 栏目
            else if (CheckIfengType.isColumn(memberType)) {
                if (isDownload) {
                    IntentUtils.startCacheAllActivityFromPush(this, pushId, null, intent.getStringExtra(PushReceiver.PUSH_COLUMN_ID), memberType);
                }
            }
            // web
            else if (CheckIfengType.isWEB(memberType)) {
                IntentUtils.startADActivity(this, null, intent.getStringExtra(PushReceiver.PUSH_WEB_URL),
                        intent.getStringExtra(PushReceiver.PUSH_WEB_URL), null, intent.getStringExtra(PushReceiver.PUSH_TITLE),
                        null, intent.getStringExtra(PushReceiver.PUSH_IMAGE), "", "", null, null);
            }
            // awaken 唤醒老用户
            else if (CheckIfengType.isAwaken(memberType)) {
                logger.info("-------sendPushMsg----awaken-------------");
            } else if (CheckIfengType.isText(memberType)) {
                IntentUtils.startIfengNewsFromPush(this, pushId, intent.getStringExtra(PushReceiver.PUSH_CONTENT));
            } else if (CheckIfengType.isVRLive(memberType)) {
                //VR LIVE
                String vrGroupTitle = intent.getStringExtra(PushReceiver.PUSH_TITLE);
                String vrId = intent.getStringExtra(PushReceiver.PUSH_ID);
                String vrLiveUrl = intent.getStringExtra(PushReceiver.PUSH_WEB_URL);
                String imageUrl = intent.getStringExtra(PushReceiver.PUSH_IMAGE);
                String shareUrl = intent.getStringExtra(PushReceiver.PUSH_VR_SHARE_URL);
                String echID = "";
                IntentUtils.startVRLiveActivity(this, vrId, vrLiveUrl, vrGroupTitle, echID, "", "", "", imageUrl, shareUrl);
            } else if (CheckIfengType.isVRVideo(memberType)) {//VR VIDEO
                List<String> videoList = new ArrayList<>();
                videoList.add(pushId);
                IntentUtils.startVRVideoActivity(this, 0, "", "", 0, videoList, true);
            }
            // 其他，不支持的类型
            else {
                logger.info("-------sendPushMsg----doActionPushTypeNoSupported-------------");
            }
            return true;
        }
        return false;
    }

    // 启动页广告跳转
    private boolean isLaunchFromSplashAD(Intent intent) {
        return intent != null && intent.getBooleanExtra(IntentKey.IS_FROM_SPLASH_ACTIVITY, false);
    }

    private void initTabs() {
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mSplitLineView = findViewById(R.id.split);

        mTabHost.setup(this, getSupportFragmentManager(), R.id.main_container);
        if (BuildUtils.hasHoneycomb()) {
            mTabHost.getTabWidget().setDividerDrawable(R.color.transparent);
        }
        // 创建底部导航图标，并设置图片、分割线、背景色
        buildTabViews();

        //首页
        TabHost.TabSpec tabSpecHome = mTabHost.newTabSpec(TAB_HOME);
        mainTabView.setTag(POS_HOME);
        mainTabView.setContentDescription(getString(R.string.auto_test_content_des_home_page));
        tabSpecHome.setIndicator(mainTabView);
        mTabHost.setCurrentTab(POS_HOME);
        PageActionTracker.clickHomeTab(0);
        mTabHost.addTab(tabSpecHome, FragmentHomePage.class, null);

        //新闻
        TabHost.TabSpec tabSpecNews = mTabHost.newTabSpec(TAB_NEWS);
        mainTabView.setTag(POS_NEWS);
        newsTabView.setContentDescription(getString(R.string.auto_test_content_des_news));
        tabSpecNews.setIndicator(newsTabView);
        mTabHost.setCurrentTab(POS_NEWS);
        mTabHost.addTab(tabSpecNews, IfengNewsFragment.class, null);

        //直播
        TabHost.TabSpec tabSpecLive = mTabHost.newTabSpec(TAB_LIVE);
        mainTabView.setTag(POS_LIVE);
        liveTabView.setContentDescription(getString(R.string.auto_test_content_des_live));
        tabSpecLive.setIndicator(liveTabView);
        mTabHost.setCurrentTab(POS_LIVE);
//        mTabHost.addTab(tabSpecLive, FragmentLive.class, null);
        mTabHost.addTab(tabSpecLive, FragmentWebLive.class, null);

        //订阅
        TabHost.TabSpec tabSpecColumn = mTabHost.newTabSpec(TAB_SUB);
        mainTabView.setTag(POS_SUB);
        subTabView.setContentDescription(getString(R.string.auto_test_content_des_sub));
        tabSpecColumn.setIndicator(subTabView);
        mTabHost.setCurrentTab(POS_SUB);
        mTabHost.addTab(tabSpecColumn, FragmentSubscribe.class, null);

        //我的
        TabHost.TabSpec tabSpecMy = mTabHost.newTabSpec(TAB_MY);
        mainTabView.setTag(POS_MY);
        myTabView.setContentDescription(getString(R.string.auto_test_content_des_my));
        tabSpecMy.setIndicator(myTabView);
        mTabHost.setCurrentTab(POS_MY);
        mTabHost.addTab(tabSpecMy, FragmentMine.class, null);

        setTabWidgetClickListener();

        setTabChangedListener();

        setCurrentTab(POS_HOME);
    }

    private void buildTabViews() {
        initTabIconUrlsAndColors();

        mainTabView = setTabItemView(mainTabView, R.drawable.selector_navigate_main, homeSelected);
        liveTabView = setTabItemView(liveTabView, R.drawable.selector_navigate_live, liveNormal);
        subTabView = setTabItemView(subTabView, R.drawable.selector_navigate_sub, subNormal);
        myTabView = setTabItemView(myTabView, R.drawable.selector_navigate_me, myNormal);
        newsTabView = setTabItemView(newsTabView, R.drawable.selector_navigate_news, newsNormal);

        if (!TextUtils.isEmpty(lineColor) && !TextUtils.isEmpty(backgroundColor)) {
            mSplitLineView.setBackgroundColor(Color.parseColor(lineColor));
            mTabHost.setBackgroundColor(Color.parseColor(backgroundColor));
        }
    }

    private void initTabIconUrlsAndColors() {
        try {
            mLocalTabIconModel = TabIconUpdateUtils.getLocalData();
            if (mLocalTabIconModel == null || mLocalTabIconModel.getTarbar() == null
                    || mLocalTabIconModel.getTarbar().getIconArray() == null) {
                return;
            }
            TabIconConfigModel.TarbarBean tarbar = mLocalTabIconModel.getTarbar();
            TabIconConfigModel.TarbarBean.IconArrayBean iconArray = tarbar.getIconArray();

            homeNormal = iconArray.getHomeIcon().getNormal3x();
            homeSelected = iconArray.getHomeIcon().getSelected3x();
            liveNormal = iconArray.getLiveIcon().getNormal3x();
            liveSelected = iconArray.getLiveIcon().getSelected3x();
            subNormal = iconArray.getSubIcon().getNormal3x();
            subSelected = iconArray.getSubIcon().getSelected3x();
            myNormal = iconArray.getMyIcon().getNormal3x();
            mySelected = iconArray.getMyIcon().getSelected3x();
            newsNormal = iconArray.getNewsIcon().getNormal3x();
            newsSelected = iconArray.getNewsIcon().getSelected3x();

            lineColor = tarbar.getLineColor();
            backgroundColor = tarbar.getBackgroundColor();

        } catch (Exception e) {
            logger.error("initTabIconUrlsAndColors error ! {}", e);
        }
    }

    private RedPointImageView setTabItemView(RedPointImageView tabImageView, final int drawRes, String imgUrl) {
        if (tabImageView == null) {
            tabImageView = new RedPointImageView(this, false);
        }
        if (TextUtils.isEmpty(imgUrl)) {
            tabImageView.setImageResource(drawRes);
        } else {
            tabImageView.setDefaultImageResId(drawRes);
            tabImageView.setErrorImageResId(drawRes);
            tabImageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        }
        return tabImageView;
    }

    private void initAudioFloatGifView() {
        mGifView = findViewById(R.id.home_gif);
        mGifView.setVisibility(View.GONE);
        mGifView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_AUDIO_ENTRY, PageActionTracker.getPageName(isLandScape(), ActivityMainTab.this));
                if (mAudioService != null) {
                    mAudioService.backToActivity();
                    ActivityMainTab.this.overridePendingTransition(R.anim.common_slide_left_in, R.anim.common_slide_left_out);
                }
            }
        });
    }

    private ScaleAnimation animation;

    private void initFloatVideoView() {
        mFloatVideoView = (FrameLayout) findViewById(R.id.home_video_view);
        mFloatVideoView.setVisibility(View.GONE);
        mSkin = new FloatVideoViewSkin(this);
        mFloatVideoView.addView(mSkin);
        mPlayController = new FloatVideoPlayController();
        mSkin.setNoNetWorkListener(this);
        mSkin.setOnLoadFailedListener(this);
        mSkin.buildSkin(null);
        animation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f);
        animation.setDuration(300);
        animation.setRepeatCount(0);
        animation.setFillAfter(true);
    }

    public void addVideoView2FloatView(UIPlayContext uiPlayContext, List<ChannelBean.HomePageBean> list, FloatVideoPlayController.OnListViewScrollToPiPVideoView listener, long lastpos) {
        mUiPlayContext = uiPlayContext;
        mPlayController.init(mSkin, mUiPlayContext);
        mPlayController.setOnListViewScrollToPiPVideoView(listener);
        mPlayController.setData(list, lastpos);
        mFloatVideoView.setVisibility(View.VISIBLE);
        mFloatVideoView.setAnimation(animation);
        animation.startNow();
        setPiPMode(true);
    }

    public static void setPiPMode(boolean value) {
        isPiPMode = value;
    }

    public static boolean getPiPMode() {
        return isPiPMode;
    }

    public void removePiPViews() {
        if (isPiPMode) {
            if (EmptyUtils.isNotEmpty(mSkin)) {
                mSkin.removeViews();
            }
        }
    }

    public long getPiPPlayerLastPosition() {
        return mPlayController.getCurrentPosition();
    }

    private void initOnLineView() {
        mSignView = findViewById(R.id.home_sign_view);
        mSignView.setVisibility(View.GONE);
        mSignView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_ONLINE_POINT, PageIdConstants.PAGE_HOME);
                if (User.isLogin()) {

                    UserPointManager.addRewardsForSignIn(IfengApplication.getAppContext(), UserPointManager.PointType.addBySignIn, new UserPointManager.PointPostCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            mHasSign = true;
                            mSignView.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFail(String result) {

                        }
                    });
                } else {
                    IntentUtils.startLoginActivity(mContext);
                }
            }
        });
    }

    private void setTabChangedListener() {
        mTabHost.setOnTabChangedListener(new MTabChangeListener(this));
    }

    static class MTabChangeListener implements TabHost.OnTabChangeListener {
        WeakReference<ActivityMainTab> weakReference;

        MTabChangeListener(ActivityMainTab activityMainTab) {
            weakReference = new WeakReference<>(activityMainTab);
        }

        @Override
        public void onTabChanged(String tabId) {
            ActivityMainTab activityMainTab = weakReference.get();
            if (activityMainTab == null) {
                return;
            }

            changeTabViewStatus(tabId, activityMainTab);

            if (mAudioService != null && activityMainTab.isShowGifView) {
                if (TAB_LIVE.equals(tabId)) {//直播页切音频不显示悬浮窗
                    activityMainTab.mGifView.setVisibility(View.GONE);
                    activityMainTab.mSignView.setVisibility(View.GONE);
                } else {
                    activityMainTab.mGifView.setVisibility(View.VISIBLE);
                    activityMainTab.mSignView.setVisibility(View.GONE);
                }
            } else {
                activityMainTab.mGifView.setVisibility(View.GONE);
                activityMainTab.controlOnLineView();
            }

            if (TAB_MY.equals(tabId)) {
                List<Fragment> fragments = activityMainTab.getSupportFragmentManager().getFragments();
                if (!ListUtils.isEmpty(fragments)) {
                    for (Fragment fragment : fragments) {
                        if (fragment != null && fragment instanceof FragmentMine) {
                            ((FragmentMine) fragment).refreshData();
                            break;
                        }
                    }
                }
            }
        }

        /**
         * 改变普通、按下状态
         */
        private void changeTabViewStatus(String tabId, ActivityMainTab activityMainTab) {
            if (activityMainTab.mLocalTabIconModel == null) {
                return;
            }

            if (TAB_HOME.equals(tabId)) {
                activityMainTab.setTabItemView(activityMainTab.mainTabView, R.drawable.selector_navigate_main, activityMainTab.homeSelected);
                activityMainTab.setTabItemView(activityMainTab.liveTabView, R.drawable.selector_navigate_live, activityMainTab.liveNormal);
                activityMainTab.setTabItemView(activityMainTab.subTabView, R.drawable.selector_navigate_sub, activityMainTab.subNormal);
                activityMainTab.setTabItemView(activityMainTab.myTabView, R.drawable.selector_navigate_me, activityMainTab.myNormal);
                activityMainTab.setTabItemView(activityMainTab.newsTabView, R.drawable.selector_navigate_news, activityMainTab.newsNormal);
            } else if (TAB_LIVE.equals(tabId)) {
                activityMainTab.setTabItemView(activityMainTab.mainTabView, R.drawable.selector_navigate_main, activityMainTab.homeNormal);
                activityMainTab.setTabItemView(activityMainTab.liveTabView, R.drawable.selector_navigate_live, activityMainTab.liveSelected);
                activityMainTab.setTabItemView(activityMainTab.subTabView, R.drawable.selector_navigate_sub, activityMainTab.subNormal);
                activityMainTab.setTabItemView(activityMainTab.myTabView, R.drawable.selector_navigate_me, activityMainTab.myNormal);
                activityMainTab.setTabItemView(activityMainTab.newsTabView, R.drawable.selector_navigate_news, activityMainTab.newsNormal);
            } else if (TAB_SUB.equals(tabId)) {
                activityMainTab.setTabItemView(activityMainTab.mainTabView, R.drawable.selector_navigate_main, activityMainTab.homeNormal);
                activityMainTab.setTabItemView(activityMainTab.liveTabView, R.drawable.selector_navigate_live, activityMainTab.liveNormal);
                activityMainTab.setTabItemView(activityMainTab.subTabView, R.drawable.selector_navigate_sub, activityMainTab.subSelected);
                activityMainTab.setTabItemView(activityMainTab.myTabView, R.drawable.selector_navigate_me, activityMainTab.myNormal);
                activityMainTab.setTabItemView(activityMainTab.newsTabView, R.drawable.selector_navigate_news, activityMainTab.newsNormal);
            } else if (TAB_MY.equals(tabId)) {
                activityMainTab.setTabItemView(activityMainTab.mainTabView, R.drawable.selector_navigate_main, activityMainTab.homeNormal);
                activityMainTab.setTabItemView(activityMainTab.liveTabView, R.drawable.selector_navigate_live, activityMainTab.liveNormal);
                activityMainTab.setTabItemView(activityMainTab.subTabView, R.drawable.selector_navigate_sub, activityMainTab.subNormal);
                activityMainTab.setTabItemView(activityMainTab.myTabView, R.drawable.selector_navigate_me, activityMainTab.mySelected);
                activityMainTab.setTabItemView(activityMainTab.newsTabView, R.drawable.selector_navigate_news, activityMainTab.newsNormal);
            } else if (TAB_NEWS.equals(tabId)) {
                activityMainTab.setTabItemView(activityMainTab.mainTabView, R.drawable.selector_navigate_main, activityMainTab.homeNormal);
                activityMainTab.setTabItemView(activityMainTab.liveTabView, R.drawable.selector_navigate_live, activityMainTab.liveNormal);
                activityMainTab.setTabItemView(activityMainTab.subTabView, R.drawable.selector_navigate_sub, activityMainTab.subNormal);
                activityMainTab.setTabItemView(activityMainTab.myTabView, R.drawable.selector_navigate_me, activityMainTab.myNormal);
                activityMainTab.setTabItemView(activityMainTab.newsTabView, R.drawable.selector_navigate_news, activityMainTab.newsSelected);
            }
        }
    }

    /**
     * 刷新精选数据
     */
    public void refreshWellChosenDataIfNecessary() {
        long timestamp = IfengApplication.getInstance().getWellChosenTimestamp();
        if (timestamp != 0 && System.currentTimeMillis() - timestamp > PageRefreshConstants.HOMEPAGE_REFRESH_DATA_INTERVAL) {
            FragmentHomePage homePage = getFragmentHomePage();
            if (homePage != null && homePage.getViewPager() != null) {
                ViewPagerColumn pager = homePage.getViewPager();
                FragmentHomePage.ViewPagerAdapterMain adapter = (FragmentHomePage.ViewPagerAdapterMain) pager.getAdapter();
                if (adapter != null) {
                    adapter.refreshWellChosenData();
                }
            }
        }
    }

    private void setTabWidgetClickListener() {
        mTabHost.getTabWidget().getChildAt(POS_HOME).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGifView(POS_HOME);
                int currentTab = mTabHost.getCurrentTab();
                if (currentTab != POS_HOME) {
                    mTabHost.setCurrentTab(POS_HOME);
                    refreshWellChosenDataIfNecessary();
                } else {
                    FragmentHomePage homePage = getFragmentHomePage();
                    if (homePage != null && homePage.getViewPager() != null) {
                        ViewPagerColumn pager = homePage.getViewPager();
                        FragmentHomePage.ViewPagerAdapterMain adapter = (FragmentHomePage.ViewPagerAdapterMain) pager.getAdapter();
                        if (adapter != null) {
                            adapter.refreshCurrentFragmentData(true);
                        }
                    }
                }

                PageActionTracker.clickHomeTab(POS_HOME);
            }
        });

        mTabHost.getTabWidget().getChildAt(POS_NEWS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickTabAndStatistics(POS_NEWS);
            }
        });

        mTabHost.getTabWidget().getChildAt(POS_LIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickTabAndStatistics(POS_LIVE);
            }
        });

        mTabHost.getTabWidget().getChildAt(POS_SUB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickTabAndStatistics(POS_SUB);
            }
        });

        mTabHost.getTabWidget().getChildAt(POS_MY).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickTabAndStatistics(POS_MY);
            }
        });
    }

    private void clickTabAndStatistics(int targetTabPosition) {
        showGifView(targetTabPosition);
        int currentTab = mTabHost.getCurrentTab();
        if (currentTab != targetTabPosition) {
            if (currentTab == POS_HOME) {
                IfengApplication.getInstance().setWellChosenTimestamp(System.currentTimeMillis());
            }
            mTabHost.setCurrentTab(targetTabPosition);
            // 发新闻SDK in统计，时机：点的新闻 && 当前非新闻 && 30s内
            if (targetTabPosition == POS_NEWS) {
                sendNewsInStatistics();
            }
        }
        // 发action  click统计
        PageActionTracker.clickHomeTab(targetTabPosition);
    }

    private long mLastClickNewsTabTime = 0L;

    private void sendNewsInStatistics() {
        if (SystemClock.uptimeMillis() - mLastClickNewsTabTime > 30 * android.text.format.DateUtils.SECOND_IN_MILLIS) {
            CustomerStatistics.sendNewsSDKInRecode(new NewsSDKInRecord());
        }
        mLastClickNewsTabTime = SystemClock.uptimeMillis();
    }

    /**
     * 显示GifView
     *
     * @param currentTab 当前Tab页
     */
    private void showGifView(int currentTab) {
        if (mAudioService != null && isShowGifView) {
            mGifView.setVisibility(currentTab == POS_LIVE ? View.GONE : View.VISIBLE);
        }
        if (!User.isLogin()) {
            return;
        }
        if (!mHasSign && currentTab == POS_HOME && isShowOnLineView && !mGifView.isShown()) {
            mSignView.setVisibility(View.VISIBLE);
        } else {
            mSignView.setVisibility(View.GONE);
        }

    }

    public void hideOnLineView() {
        mSignView.setVisibility(View.GONE);
    }

    public void showOnLineView() {
        mSignView.setVisibility(View.VISIBLE);
    }

    public void controlOnLineView() {
        int currentTab = mTabHost.getCurrentTab();
        if (!mHasSign && currentTab == POS_HOME && isShowOnLineView && !mGifView.isShown()) {
            mSignView.setVisibility(View.VISIBLE);
        } else {
            mSignView.setVisibility(View.GONE);
        }
    }

    /**
     * 设置当前Tab页为直播页
     *
     * @param playVideo 是否播视频
     *                  默认不是来自推送
     */
    public void setCurrentTabToLive(String channelId, String echid, String chid, String shareTitle, String shareImg, boolean playVideo) {
        setCurrentTabToLive(channelId, echid, chid, shareTitle, shareImg, playVideo, false);
    }

    /**
     * 设置当前Tab页为直播页
     *
     * @param playVideo  是否播视频
     * @param isFromPush 是否来自推送
     */
    public void setCurrentTabToLive(String channelId, String echid, String chid, String shareTitle, String shareImg, boolean playVideo, boolean isFromPush) {
        liveData = new LiveData(channelId, echid, chid, shareTitle, shareImg, playVideo, isFromPush);
        if (mTabHost != null && mTabHost.getTabWidget().getChildAt(POS_LIVE) != null) {
            mTabHost.getTabWidget().getChildAt(POS_LIVE).performClick();
        }
    }

    private boolean dealByLiveNotify(Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return false;
        }
        return false;
    }

    /**
     * H5拉起
     * 1、跳转类型为video （包括带video的文章）则跳转标识为视频GUID
     * 格式： ifengVideoPlayer://跳转类型/视频GUID/跳转渠道
     * 例子：ifengVideoPlayer://video/64e5352f-4eee-4a79-b449-7c3ee184bf86/wx
     * <p/>
     * 2、跳转类型为live
     * 格式： ifengVideoPlayer://跳转类型/视频GUID/跳转渠道
     * 例子：ifengVideoPlayer://live/30e8fa4a-9646-4371-ab99-6e0a035c9032/sf
     * <p/>
     * 3、跳转类型为vrlive
     * 格式： ifengVideoPlayer://跳转类型/视频GUID/跳转渠道?vrURL=http://XXXX.XXX.XXX
     * 例子：ifengVideoPlayer://vrlive/30e8fa4a-9646-4371-ab99-6e0a035c9032/h5?vrURL=http://XXXX.XXX.XXX
     * <p/>
     * 跳转类型：vrlive
     * 视频guid： 直播为channelId的值
     * 视频H5页拉起客户端跳转渠道：h5
     * 参数 : vrURL vr直播的地址
     */
    private boolean initH5Intent(Intent intent) {
        if (intent == null || intent.getData() == null) {
            logger.debug("客户端拉起：非H5拉起，intent或data为空::{}", intent);
            return false;
        }
        Uri data = intent.getData();

        if (TextUtils.isEmpty(data.getHost())) {
            return false;
        }
        logger.debug("客户端拉起：H5拉起 Uri : {} , data.getHost is {}", data, data.getHost());

        statisticH5(data);

        List<String> segments = data.getPathSegments();
        if (segments != null && !segments.isEmpty()) {
            startActivityByH5(data);
            return true;
        }

        logger.debug("客户端 H5拉起：拉起失败");
        return false;
    }

    private void statisticH5(Uri data) {
        String appStartType = "";
        String last = data.getLastPathSegment();
        logger.debug("客户端H5 拉起：getLastPathSegment  = " + last);
        String ifengNewsApp = "ifengnewsapp";
        String ifengNews = "ifengnews";
        String h5 = StatisticsConstants.APPSTART_TYPE_FROM_H5;
        String sf = StatisticsConstants.APPSTART_TYPE_FROM_SF;
        if (!TextUtils.isEmpty(last)) {
            if (ifengNewsApp.equalsIgnoreCase(last.trim()) || ifengNews.equalsIgnoreCase(last.trim())) {
                appStartType = StatisticsConstants.APPSTART_TYPE_FROM_NEWS;
            } else if (h5.equalsIgnoreCase(last.trim())) {
                appStartType = h5;
            } else if (sf.equalsIgnoreCase(last.trim())) {
                appStartType = sf;
            } else {
                appStartType = last.trim();
            }
        }
        logger.debug("客户端H5拉起：appStartType：{}", appStartType);
        CustomerStatistics.inType = StatisticsConstants.APPSTART_TYPE_FROM_OUTSIDE;
        CustomerStatistics.openId = appStartType;
    }

    /**
     * 检查视频信息是否是合法的,h5拉起(Type:video/column/live/cmcclive/liveroom/cmpptopic/lianbo/focus/adin)
     */
    private void startActivityByH5(Uri data) {
        if (!NetUtils.isNetAvailable(this) || ActivityMainTab.this.isFinishing()) {
            return;
        }
        String memberType = data.getHost();
        logger.debug("客户端H5拉起：memberType = {}", memberType);

        List<String> segments = data.getPathSegments();
        final String id = segments.get(0);

        logger.debug("客户端H5拉起：succeedOnResult id = {} ", id);
        if (TextUtils.isEmpty(memberType) || TextUtils.isEmpty(id)) {
            return;
        }
        //跳转到点播播放页
        if (CheckIfengType.isVideo(memberType)) {
            IntentUtils.toVodDetailActivity(ActivityMainTab.this, id, "", false, false, 0, "");
        }
        //跳转到直播播放页 留空
        else if (CheckIfengType.isLiveType(memberType)) {
            setCurrentTabToLive(id, "", "", "", "", true);
            //IntentUtils.startActivityTVLive(ActivityMainTab.this, id, "", "", "", "", false);
        }
        // VR直播
        else if (CheckIfengType.isVRLive(memberType)) {
            String vrURL = data.getQueryParameter("vrURL");
            IntentUtils.startVRLiveActivity(this, id, vrURL, null, "", "", "", "", "", "");
        }
        //VR点播
        else if (CheckIfengType.isVRVideo(memberType)) {
            List<String> videoList = new ArrayList<>();
            videoList.add(id);
            IntentUtils.startVRVideoActivity(this, 0, "", "", 0, videoList, true);
        }
        //跳转到专题播放页
        else if (CheckIfengType.isTopicType(memberType)) {
            // 数据类型转换special-->cmpptopic，用作接口请求参数type，如
            // http://vcis.ifeng.com/api/detailTopic?platformType=androidPhone&itemId=186&itemType=focus&adapterNo=7.2.2&protocol=1.0.0
            if (IfengType.TYPE_TOPIC_SPECIAL.equalsIgnoreCase(memberType)) {
                memberType = IfengType.TYPE_TOPIC_CMPPTOPIC; //注意要小写，详见接口2.7.2 itemType规定值
            }
            String topicId = segments.get(0);
            String guid = segments.get(1);
            IntentUtils.toTopicDetailActivity(ActivityMainTab.this, guid, topicId, "", memberType, false, 0, "");
        }
        //跳转到自媒体主页
        else if (CheckIfengType.isWeMedia(memberType)) {
            IntentUtils.startWeMediaHomePageActivity(ActivityMainTab.this, id, "");
        }
        // 开通会员页
        else if ("VIPPay".equalsIgnoreCase(memberType)) {
            if (User.isVip()) {
                IntentUtils.startMemberCenterActivity(this);
            } else {
                IntentUtils.startOpenMemberActivity(this);
            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        /**频道订阅back退出*/
        if (keyCode == KeyEvent.KEYCODE_BACK && mTabHost != null && mTabHost.getCurrentTabTag().equals(TAB_HOME) && isChannelManageFragmentShow) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment f = manager.findFragmentByTag(ChannelManagerFragment.CHANNEL_MANAGER_TAG);
            if (f != null) {
                manager.beginTransaction().setCustomAnimations(R.anim.common_slide_left_in, R.anim.common_slide_right_out).remove(f).commit();
            }
            isChannelManageFragmentShow = false;
            refreshWellChosenDataIfNecessary();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void exit() {
        if (mAudioService != null) {
            mAudioService.stopAudioService(VideoSkin.SKIN_TYPE_EXIT);
        }
        super.exit();
    }

//    public boolean isLiveFragmentShowing() {
//        return mTabHost.getCurrentTabTag().equals(TAB_LIVE);
//    }
//
//    public FragmentLive getLiveFrag() {
//        if (liveFrag == null) {
//            liveFrag = (FragmentLive) getSupportFragmentManager().findFragmentByTag(TAB_LIVE);
//        }
//        return liveFrag;
//    }

    public void setFragmentHomePage(FragmentHomePage homePage) {
        this.mFragmentHomePage = homePage;
    }


    @Override
    protected void onDestroy() {
        mIsRunning = false;
        UserFeed.cacheIfengAddress(this);
        mLocationListener = null;
        mTabHost.setOnTabChangedListener(null);
        unregisterHomeKeyReceiver();
        unregisterFloatViewReceiver();
        unregisterLoginReceiver();
        PageActionTracker.lastPage = "";
        super.onDestroy();
    }

    private static class GetVideoByGuidErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            logger.debug("getSingleVideoByGuid：failedOnError:{}", error);
        }
    }

    private static class GetLiveIfengChannelErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            logger.error("getLiveIfengChannelData：failedOnError:{}", error);
        }
    }

    /**
     * 百度定位获取地理位置，主要用于定向推送
     */
    private void getLocation() {
        // 解决33578bug，web推送不进行定位
        Intent intent = getIntent();
        if (intent == null || !isFromPush(intent) || !CheckIfengType.isWEB(intent.getStringExtra(PushReceiver.PUSH_TYPE))) {
            mLocationClient = new LocationClient(getApplicationContext());
            mLocationListener = new LocationListener(this);
            mLocationClient.registerLocationListener(mLocationListener);
            setLocationOption();// 获取地理位置
            startLocate();

            //获取相关手机信息用于安全登录统计
            //TODO 安全统计 这版不做，暂时预留出来。
            //        PhoneModel phone = Util4act.getPhoneInfo(ActivityMainTab.this);
            //        Util4act.setPhoneInfo(ActivityMainTab.this, phone);
        }
    }

    private void setLocationOption() {
        try {
            LocationClientOption option = new LocationClientOption();
            // 1、高精度模式定位策略：这种定位模式下，会同时使用网络定位和GPS定位，优先返回最高精度的定位结果；
            // 2、低功耗模式定位策略：该定位模式下，不会使用GPS，只会使用网络定位（Wi-Fi和基站定位）；
            // 3、仅用设备模式定位策略：这种定位模式下，不需要连接网络，只使用GPS进行定位，这种模式下不支持室内环境的定位。
            // 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
            // option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);// 设置定位模式
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
            option.setScanSpan(1000); // 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
            option.setIsNeedAddress(true);// 返回的定位结果包含地址信息
            option.disableCache(false);//禁止启用缓存定位
            option.setOpenGps(false); //关闭开启GPS操作
            option.setIgnoreKillProcess(false);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死

            mLocationClient.setLocOption(option);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void startLocate() {
        if (mLocationClient.isStarted()) {
            mLocationClient.requestLocation();
        } else {
            mLocationClient.start();
        }
    }

    private void stopLocateAndSetPushTag() {
        IPushUtils.setIPushTag(false);
        SendSmartStatisticUtils.sendSmartLaunchStatistics();
        try {
            mLocationClient.stop();
            mLocationClient.unRegisterLocationListener(mLocationListener);
            killBaiduProcess();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 杀掉百度SDK进程，避免不必要的内存占用
     */
    private void killBaiduProcess() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = activityManager.getRunningAppProcesses();

        for (ActivityManager.RunningAppProcessInfo processInfo : processes) {
            if (processInfo.processName != null && processInfo.processName.equals(IfengApplication.PACKAGE_NAME + ":bdMap")) {
                Process.killProcess(processInfo.pid);
            }
        }
    }

    private static class LocationListener implements BDLocationListener {
        WeakReference<ActivityMainTab> weakReference;

        LocationListener(ActivityMainTab activityMainTab) {
            weakReference = new WeakReference<>(activityMainTab);
        }

        @Override
        public void onReceiveLocation(BDLocation location) {
            ActivityMainTab activityMainTab = weakReference.get();
            if (activityMainTab == null) {
                return;
            }

            // 返回码  http://lbsyun.baidu.com/index.php?title=android-locsdk/guide/getloc
            activityMainTab.hasReceivedLocation = location.getLocType() == BDLocation.TypeGpsLocation ||
                    location.getLocType() == BDLocation.TypeNetWorkLocation ||
                    location.getLocType() == BDLocation.TypeOffLineLocation ||
                    location.getLocType() == BDLocation.TypeCacheLocation;
            logger.debug("hasReceivedLocation {} , locType={}, location:{}", activityMainTab.hasReceivedLocation, location.getLocType(), location);
            if (activityMainTab.hasReceivedLocation) {
                logger.debug("receivedLocation !!!!!!!!!!!!!!");
                activityMainTab.hasReceivedLocation = true;
                activityMainTab.mSharePreUtils.setLocation(location);
                activityMainTab.stopLocateAndSetPushTag();
            } else {
                activityMainTab.mLocateTryTime++;
                if (activityMainTab.mLocateTryTime <= 3) {
                    logger.info("will request again for {} time ", activityMainTab.mLocateTryTime);
                    activityMainTab.startLocate();
                } else {
                    activityMainTab.stopLocateAndSetPushTag();
                }
            }
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (mTabHost.getCurrentTab() == POS_LIVE
//                && getLiveFrag() != null
                && liveWindowFocusChangeListener != null) {
            //            if (DisplayUtils.isDeviceInLandscape(this) && liveWindowFocusChangeListener != null) {
            liveWindowFocusChangeListener.LiveWindowFocusChanged(hasFocus);
            //            }
        }
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean isLiveRecommendFrag = mTabHost.getCurrentTab() == POS_LIVE; //&& getLiveFrag() != null;
        if (isLiveRecommendFrag) {
            if (DisplayUtils.isDeviceInLandscape() && interfaceLiveHandle != null) {
                interfaceLiveHandle.frgPlayTouchEvent(new LiveMotionEvent(ev));
            }
        }
//        boolean isPicFragment = mTabHost.getCurrentTab() == POS_HOME && getHomeMainFrag() != null && getHomeMainFrag().isPicshow();
//        logger.debug("the isPicFragment ---- >{}", isPicFragment);
//        if (isPicFragment) {
//            if (DisplayUtils.isDeviceInLandscape(this)) {
//                ((ChildHomeSubChannelPictureFragment) getHomeMainFrag().getVisibleFrg()).dispatchMyTouchEvent(new LiveMotionEvent(ev));
//            }
//        }
        return super.dispatchTouchEvent(ev);
    }

    public void setTouchListener(InterfaceLiveHandle inTouch) {
        this.interfaceLiveHandle = inTouch;
    }

    public void setLiveWindowFocusChangeListener(LiveWindowFocusChangeListener liveWindowFocusChangeListener) {
        this.liveWindowFocusChangeListener = liveWindowFocusChangeListener;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager.isActive()) {
                inputMethodManager.hideSoftInputFromWindow(ActivityMainTab.this.getCurrentFocus().getWindowToken(), 0);
                if (null != fragmentMine) {
                    fragmentMine.setEditTextStatus();
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * 提供给Fragment使用
     *
     * @return 是否按下了home键
     */
    public boolean getHasHomeClicked() {
        return hasHomeClick;
    }

    public boolean currentTabIsLive() {
        return mTabHost.getCurrentTab() == POS_LIVE;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // 拦截从左往右滑动时，走基类方法退出
        return true;
    }

    public int getCurrentTab() {
        return mTabHost.getCurrentTab();
    }

    public int getTabHostHeight() {
        return mTabHost.getHeight();
    }

    /**
     * 监听HOME键
     */
    public static class HomeWatcherReceiver extends BroadcastReceiver {
        private static final String SYSTEM_DIALOG_REASON_KEY = "reason";
        private static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
        private static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
        private static final String SYSTEM_DIALOG_REASON_LOCK = "lock";
        private static final String SYSTEM_DIALOG_REASON_ASSIST = "assist";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                // android.intent.action.CLOSE_SYSTEM_DIALOGS
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);

                if (SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) {
                    // 短按Home键
                    CustomerStatistics.inType = StatisticsConstants.APPSTART_TYPE_FROM_DIRECT;
                } else if (SYSTEM_DIALOG_REASON_RECENT_APPS.equals(reason)) {
                    // 长按Home键 或者 activity切换键
                } else if (SYSTEM_DIALOG_REASON_LOCK.equals(reason)) {
                    // 锁屏
                } else if (SYSTEM_DIALOG_REASON_ASSIST.equals(reason)) {
                    // samsung 长按Home键
                }
            }
        }
    }

    private void registerHomeKeyReceiver(Context context) {
        if (context == null) {
            return;
        }
        if (mHomeKeyReceiver == null) {
            mHomeKeyReceiver = new HomeWatcherReceiver();
        }
        context.registerReceiver(mHomeKeyReceiver, new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
    }

    private static HomeWatcherReceiver mHomeKeyReceiver = null;

    private void unregisterHomeKeyReceiver() {
        if (mHomeKeyReceiver != null) {
            unregisterReceiver(mHomeKeyReceiver);
        }
    }

    private void unregisterLoginReceiver() {
        if (null != mLoginReceiver) {
            unregisterReceiver(mLoginReceiver);
        }
    }

    public void setChannelManageFragmentShow(boolean channelManageFragmentShow) {
        isChannelManageFragmentShow = channelManageFragmentShow;
        if (mFragmentHomePage != null) {
            mFragmentHomePage.setChannelManageFragmentShow(channelManageFragmentShow);
        }
    }

    public static boolean isChannelManageFragmentShow() {
        return isChannelManageFragmentShow;
    }

    public void switchCurrentChannelFragment(String channelId) {
        if (mFragmentHomePage != null) {
            mFragmentHomePage.switchCurrentChannelFragment(channelId);
        }
    }

    private void showIfengTVGuide(final Context context) {
        logger.debug("show ifeng tv guid");
        try {
            final SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
            final String curVersionName = PackageUtils.getAppVersion(context);
            String preVersionName = preferences.getString("versionName_for_ifeng_tv_guide_pic", null);
            if (TextUtils.isEmpty(curVersionName) || TextUtils.isEmpty(preVersionName) || !curVersionName.equals(preVersionName)) {
                ViewStub viewStub = (ViewStub) findViewById(R.id.vs_ifeng_guid);
                final View guid_view = viewStub.inflate();
                guid_view.setVisibility(View.VISIBLE);
                guid_view.findViewById(R.id.iv_ifeng_guid_pic).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        guid_view.setVisibility(View.GONE);
                        if (!TextUtils.isEmpty(curVersionName)) {
                            preferences.edit().putString("versionName_for_ifeng_tv_guide_pic", curVersionName).apply();
                        }
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_TV_GUIDE, PageIdConstants.PAGE_HOME);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showHomeGuideView() {
        logger.debug(" showHomeGuideView");
        try {
            final SharedPreferences preferences = IfengApplication.getInstance()
                    .getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
            final String curVersionName = PackageUtils.getAppVersion(IfengApplication.getInstance());
            String preVersionName = preferences.getString("versionName_for_home_guide", null);
            boolean isNewOrUpdate = TextUtils.isEmpty(curVersionName) || TextUtils.isEmpty(preVersionName)
                    || !curVersionName.equals(preVersionName);
            // isNewOrUpdate = true;
            if (isNewOrUpdate) {
                ViewStub viewStub = (ViewStub) findViewById(R.id.view_stub_home_guide_view);
                final View guid_view = viewStub.inflate();
                guid_view.setVisibility(View.VISIBLE);
                guid_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        guid_view.setVisibility(View.GONE);
                        if (!TextUtils.isEmpty(curVersionName)) {
                            preferences.edit().putString("versionName_for_home_guide", curVersionName).apply();
                        }
                        // PageActionTracker.clickBtn(ActionIdConstants.CLICK_TV_GUIDE, PageIdConstants.PAGE_HOME);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTabVisible(int visible) {
        if (mTabHost != null) {
            mTabHost.setVisibility(visible);
        }
        if (mSplitLineView != null) {
            mSplitLineView.setVisibility(visible);
        }
        if (mFragmentHomePage != null) {
            mFragmentHomePage.setTabsVisible(visible);
        }
    }

    private LiveData liveData;

    public LiveData getLiveData() {
        if (liveData == null) {
            liveData = new LiveData();
        }
        return liveData;
    }


    private static class FloatViewReceiver extends BroadcastReceiver {
        WeakReference<ActivityMainTab> weakReference;

        FloatViewReceiver(ActivityMainTab a) {
            weakReference = new WeakReference<>(a);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            ActivityMainTab activityMainTab = weakReference.get();
            if (activityMainTab == null) {
                return;
            }

            if (activityMainTab.mGifView != null && intent != null) {
                String action = intent.getAction();
                switch (action) {
                    case AudioService.ACTION_ONCREATE:
                    case AudioService.ACTION_ONSTART:
                        activityMainTab.isShowGifView = true;
                        if (activityMainTab.mTabHost.getCurrentTab() == POS_LIVE) {//直播页切音频不显示悬浮窗
                            activityMainTab.mGifView.setVisibility(View.GONE);
                            activityMainTab.mSignView.setVisibility(View.GONE);
                        } else {
                            activityMainTab.mGifView.setVisibility(View.VISIBLE);
                            activityMainTab.mSignView.setVisibility(View.GONE);
                        }
                        break;
                    case AudioService.ACTION_ONDESTROY:
                    case AudioService.ACTION_ONPAUSE:
                    case AudioService.ACTION_ONERROR:
                        activityMainTab.isShowGifView = false;
                        activityMainTab.mGifView.setVisibility(View.GONE);
                        activityMainTab.controlOnLineView();
                        break;
                }
            }
        }

    }

    private void registerFloatViewReceiver() {
        if (mFloatViewReceiver == null) {
            mFloatViewReceiver = new FloatViewReceiver(this);
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AudioService.ACTION_ONCREATE);
        intentFilter.addAction(AudioService.ACTION_ONDESTROY);
        intentFilter.addAction(AudioService.ACTION_ONPAUSE);
        intentFilter.addAction(AudioService.ACTION_ONSTART);
        intentFilter.addAction(AudioService.ACTION_ONERROR);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mFloatViewReceiver, intentFilter);
    }

    private void unregisterFloatViewReceiver() {
        if (mFloatViewReceiver != null) {
            mFloatViewReceiver = new FloatViewReceiver(this);
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mFloatViewReceiver);
            mFloatViewReceiver = null;
        }
    }

    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        registerReceiver(mLoginReceiver, filter);
    }

    public void setCurrentTab(@TabIndex int tab_index) {
        if (mTabHost != null) {
            mTabHost.setCurrentTab(tab_index);
        }
    }

    private FragmentLive mFragmentLive;

    public void setFragmentLive(FragmentLive mFragmentLive) {
        this.mFragmentLive = mFragmentLive;
    }

    public void switchFragmentLiveController() {
        if (mFragmentLive != null) {
            mFragmentLive.switchAudioVideoController(true);
        }
    }

    public RedPointImageView getMyTabView() {
        return myTabView;
    }

    private boolean mHasSign;

    private void requestPointCount() {
        if (!User.isLogin()) {
            return;
        }

        String url = String.format(DataInterface.GET_POINT_TASK_POINT_COUNT, new User(this).getUid(), PhoneConfig.mos, PhoneConfig.userKey, "", User.getIfengToken());
        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        JSONObject jsonObject = JSON.parseObject(response.toString());
                        int code = jsonObject.getIntValue("code");
                        if (200 == code) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            int count = data.getIntValue("credit_num");
                            JSONObject signInfo = data.getJSONObject("signin_info");
                            mHasSign = signInfo.getBoolean("signin_today");
                            signDays = signInfo.getIntValue("signin_days");
                            if (mHasSign) {
                                mSignView.setVisibility(View.GONE);
                            } else {
                                int currentTab = mTabHost.getCurrentTab();
                                if (currentTab == POS_HOME && isShowOnLineView && !mGifView.isShown()) {
                                    mSignView.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }


    @Override
    public void onClickPoint() {
        if (null != mSignView) {
            mSignView.setVisibility(View.GONE);
            isShowOnLineView = false;
        }
    }

    private FragmentMine fragmentMine;
    private int signDays;

    public void setFragmentMine(FragmentMine fragment) {
        this.fragmentMine = fragment;
    }

    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING) {
                    //TODO 做限制
                    int currentTab = mTabHost.getCurrentTab();
                    if (currentTab == POS_HOME && !ActivityUtils.isApplicationBroughtToBackground(mContext)) {
                        UserPointManager.addRewardsForSignIn(IfengApplication.getAppContext(), UserPointManager.PointType.addBySignIn, new UserPointManager.PointPostCallback<String>() {
                            @Override
                            public void onSuccess(String result) {
                                mHasSign = true;
                                mSignView.setVisibility(View.GONE);
                            }

                            @Override
                            public void onFail(String result) {

                            }
                        });
                    }

                } else if (loginState == IntentKey.UNLOGIN) {
                    controlOnLineView();
                }
            }
        }
    };

    private void openNotificationSwitch() {
        if (!SharePreUtils.getInstance().isClosedNotification()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//API19+
                try {
                    if (HistoryDAO.getInstance(IfengApplication.getInstance()).getVideoHistoryDataCount() >= 10 && !NotificationCheckUtils.notificationIsOpen(this)) {
                        showOpenNotificationDialog();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            } else {
                try {
                    if (HistoryDAO.getInstance(IfengApplication.getInstance()).getVideoHistoryDataCount() >= 10 && SharePreUtils.getInstance().getPushMessageState()) {
                        showOpenNotificationDialog();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            }
        }
    }

    private Dialog showOpenNotificationDialog;
    private boolean toOpenNotification;

    private void showOpenNotificationDialog() {

        if (null != showOpenNotificationDialog && showOpenNotificationDialog.isShowing()) {
            showOpenNotificationDialog.dismiss();
        }

        showOpenNotificationDialog = AlertUtils.getInstance().showConfirmBtnDialog(this, getString(R.string.custom_dialog_title), getString(R.string.custom_dialog_content), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharePreUtils.getInstance().setClosedNotification(true);
                showOpenNotificationDialog.dismiss();

            }
        }, getString(R.string.custom_dialog_confirm_btn), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.openAndroidAppSet(mContext);
                toOpenNotification = true;
                showOpenNotificationDialog.dismiss();
            }
        });
    }


}
