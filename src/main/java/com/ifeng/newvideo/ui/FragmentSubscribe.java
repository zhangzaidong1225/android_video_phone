package com.ifeng.newvideo.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.adapter.SubscribeRecommendListAdapter;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.exception.ResponseEmptyException;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.RecommendBaseBean;
import com.ifeng.video.dao.db.model.subscribe.RecommendVideoItem;
import com.ifeng.video.dao.db.model.subscribe.RecommendWeMediaItem;
import com.ifeng.video.dao.db.model.subscribe.SubscribeList;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * 订阅Fragment
 * Created by guolc on 2016/7/19.
 */
public class FragmentSubscribe extends FragmentBase implements RequestData, SubscribeRecommendListAdapter.OnLoadFailedClick,
        SubscribeRecommendListAdapter.ClickListener, FragmentSubscribeHeaderView.RequestHeaderViewData {
    private static final Logger logger = LoggerFactory.getLogger(FragmentSubscribe.class);

    private UIStatusLayout uiStatusLayout;
    private FragmentSubscribeHeaderView headerView;
    private PullToRefreshListView listView;//精品推荐ListView
    private SubscribeRecommendListAdapter recommendAdapter;//精品推荐适配器

    private List<WeMediaInfoList.InfoListEntity> subscribeList = new Vector<>();//订阅内容数据
    private List<SubscribeList.WeMediaListEntity> ifengTVList = new ArrayList<>();//凤凰卫视推荐自媒体
    private List<RecommendBaseBean> recommendList = new ArrayList<>();//转化后的精品推荐数据
    private List<WeMediaInfoList.InfoListEntity.WeMediaEntity> weMediaList = new ArrayList<>();//保存服务端自媒体信息，用于去重比较

    private RecommendWeMediaItem clickSubscribeWeMedia;//点击订阅推荐Item中的自媒体
    private boolean isClickSubscribeBtn = false;//是否点击了订阅订阅推荐Item中订阅按钮

    private boolean isGetRecommendDataFail = false;//获取推荐内容是否失败
    private boolean isGetContentDataFail = false;//获取订阅内容是否失败
    private boolean isGetIfengTVDataFail = false;//获取凤凰推荐自媒体是否失败

    private boolean isPullRequestRecommendData = false;//是否上拉加载更多推荐数据

    private int currentPage = 1;//当前页码

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribe, container, false);
        initView(view);
        initHeaderView();
        initAdapter();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerLoginBroadcast();
        loading();
    }

    /**
     * loading
     */
    private void loading() {
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);//第一次进入整理Loading
        headerView.setContentStatus(FragmentSubscribeHeaderView.LOADING);
        headerView.setIfengTVStatus(FragmentSubscribeHeaderView.LOADING);
        headerView.setIfengRecommendErrorStatus(FragmentSubscribeHeaderView.LOADING);
        recommendAdapter.showLoadingView();
    }

    @Override
    public void onResume() {
        super.onResume();
        getSubscribeContentLogic();
        if (!isClickSubscribeBtn && !headerView.isClickSubscribeBtn()) {//如果没有点击订阅推荐和凤凰卫视推荐中订阅按钮,请求订阅推荐
            getIfengTVRecommend();
            refreshRecommendData();
        }
        if (!isHidden()) {
            PageActionTracker.enterPage();
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            refreshRecommendData();
            getSubscribeContentLogic();
            getIfengTVRecommend();
            PageActionTracker.enterPage();
        } else {
            PageActionTracker.endPageSub();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isHidden()) {
            PageActionTracker.endPageSub();
        }
    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(mLoginReceiver);
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 点击订阅按钮回调方法
     */
    @Override
    public void onSubscribeClickListener(RecommendWeMediaItem entity) {
        if (!NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        clickSubscribeWeMedia = entity;
        if (User.isLogin()) {
            int state = entity.getFollowed();
            state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            subscribe(entity, getUid(), state);
            PageActionTracker.clickWeMeidaSub(state == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.PAGE_SUB);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(getActivity());
        }
    }

    /**
     * ListView自媒体Item点击事件
     */
    @Override
    public void onWeMediaItemClickListener(RecommendWeMediaItem entity) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_REC_WEMEDIA, PageIdConstants.PAGE_SUB);
        IntentUtils.startWeMediaHomePageActivity(getActivity(), entity.getWeMediaID(), "");
    }

    /**
     * ListView视频Item点击事件
     */
    @Override
    public void onVideoItemClickListener(RecommendVideoItem entity) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_REC_ITEM, PageIdConstants.PAGE_SUB);
        String guid = entity.getMemberItem().getGuid();
        if (CheckIfengType.isVRVideo(entity.getMemberType())) {
            List<String> videoList = new ArrayList<>();
            videoList.add(guid);
            IntentUtils.startVRVideoActivity(getActivity(), 0, "", "", 0, videoList, true);
        } else {
            IntentUtils.toVodDetailActivity(getActivity(), guid, "", false, false, 0, "");
        }
    }

    /**
     * 整体加载失败、无网，重新拉取数据
     */
    @Override
    public void requestData() {
        loading();
        refreshRecommendData();
        getSubscribeContentLogic();
        getIfengTVRecommend();
    }

    /**
     * HeaderView订阅内容加载失败，拉取数据
     */
    @Override
    public void requestContentData() {
        requestDataLogic();
    }

    /**
     * HeaderView凤凰推荐自媒体加载失败，拉取数据
     */
    @Override
    public void requestIfengTVData() {
        requestDataLogic();
    }

    /**
     * ListView中数据加载失败，拉取数据
     */
    @Override
    public void onLoadFailedClick() {
        requestDataLogic();
    }

    /**
     * 请求数据逻辑
     */
    private void requestDataLogic() {
        if (ListUtils.isEmpty(subscribeList) && User.isLogin() && !isShowNoSubscribeView) {
            headerView.setContentStatus(FragmentSubscribeHeaderView.LOADING);
            getSubscribeContent();//获取订阅内容
        }
        if (ListUtils.isEmpty(ifengTVList)) {
            headerView.setIfengTVStatus(FragmentSubscribeHeaderView.LOADING);
            getIfengTVRecommend();//获取凤凰推荐自媒体
        }
        if (ListUtils.isEmpty(recommendList)) {
            recommendAdapter.showLoadingView();
            refreshRecommendData();
        }
        if (!NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
        }
    }

    /**
     * 获取订阅内容逻辑
     */
    private void getSubscribeContentLogic() {
        if (!User.isLogin()) {
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            headerView.setContentStatus(FragmentSubscribeHeaderView.NO_LOGIN);
        } else {
            getSubscribeContent();
        }
    }

    /**
     * 注册登录广播
     */
    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        getActivity().registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING) {
                    headerView.setContentStatus(FragmentSubscribeHeaderView.LOADING);
                    getSubscribeContent();//获取订阅内容
                    if (!isClickSubscribeBtn && !headerView.isClickSubscribeBtn()) {
                        getIfengTVRecommend();
                        refreshRecommendData();
                    }

                    if (isClickSubscribeBtn && clickSubscribeWeMedia != null) {//如果点击了订阅按钮则处理订阅逻辑，否则不处理
                        subscribe(clickSubscribeWeMedia, getUid(), SubscribeRelationModel.SUBSCRIBE);
                    }
                    isClickSubscribeBtn = false;
                } else {
                    isClickSubscribeBtn = false;
                    subscribeList.clear();//用户未登录或退出登录，清除订阅内容数据
                    headerView.setContentStatus(FragmentSubscribeHeaderView.NO_LOGIN);
                    getIfengTVRecommend();
                    refreshRecommendData();
                }
            }
        }
    };

    /**
     * 刷新订阅推荐数据
     */
    private void refreshRecommendData() {
        isPullRequestRecommendData = false;
        currentPage = 1;
        getSubscribeRecommend();
    }

    private void initView(View view) {
        View searchLayout = view.findViewById(R.id.search);
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_HOME, PageIdConstants.PAGE_SUB);
                IntentUtils.toSearchActivity(getActivity());
            }
        });
        uiStatusLayout = (UIStatusLayout) view.findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        listView = uiStatusLayout.getListView();
        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);//上拉加载
        listView.setShowIndicator(false);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                isPullRequestRecommendData = true;
                getSubscribeRecommend();
            }
        });
    }

    /**
     * 初始化HeaderView
     */
    private void initHeaderView() {
        headerView = new FragmentSubscribeHeaderView(getActivity());
        headerView.setRequestHeaderViewData(this);
        headerView.setOnSubscribeSuccess(new FragmentSubscribeHeaderView.OnSubscribeSuccess() {
            @Override
            public void onSubscribeSuccess() {
                getSubscribeContentLogic();
                getIfengTVRecommend();
                refreshRecommendData();
            }
        });
        listView.getRefreshableView().addHeaderView(headerView, null, false);
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        recommendAdapter = new SubscribeRecommendListAdapter();
        recommendAdapter.setClickListener(this);
        recommendAdapter.setOnLoadFailClick(this);
        listView.getRefreshableView().setAdapter(recommendAdapter);
    }

    /**
     * 获取订阅内容
     */
    private void getSubscribeContent() {
        WeMediaDao.getWeMediaList("",
                getUid(),
                WeMediaDao.WE_MEDIA_TYPE_SUBSCRIBE,
                "1",//只取第一页数据
                DataInterface.PAGESIZE_20,
                System.currentTimeMillis() + "",
                WeMediaInfoList.class,
                new Response.Listener<WeMediaInfoList>() {
                    @Override
                    public void onResponse(WeMediaInfoList response) {
                        handleContentResponse(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("getSubscribeContent()  error={}", error);
                        handleVolleyError(error, SUBSCRIBE_EMPTY);
                    }
                },
                true);
    }

    private boolean isShowNoSubscribeView = false;//订阅内容处是否展示没有订阅内容View

    /**
     * 处理订阅内容响应事件
     */
    private void handleContentResponse(WeMediaInfoList response) {
        isGetContentDataFail = false;
        uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
        if (response != null && !ListUtils.isEmpty(response.getInfoList())) {
            subscribeList.clear();//清除以前数据
            for (WeMediaInfoList.InfoListEntity infoListEntity : response.getInfoList()) {
                boolean illegal = TextUtils.isEmpty(infoListEntity.getWeMedia().getWeMediaID())//自媒体id为空
                        || TextUtils.isEmpty(infoListEntity.getWeMedia().getName())//自媒体名称为空
                        || infoListEntity.getBodyList().size() == 0;//没有发布视频
                if (illegal) {
                    continue;
                }
                subscribeList.add(infoListEntity);
            }
            if (subscribeList.size() == 0) {
                isShowNoSubscribeView = true;
                headerView.setContentStatus(FragmentSubscribeHeaderView.NO_SUBSCRIBE);
            } else {
                isShowNoSubscribeView = false;
                boolean isOver3 = subscribeList.size() > 3;
                headerView.setIsDataCountOver3(isOver3);
                headerView.setContentStatus(FragmentSubscribeHeaderView.NORMAL);
                headerView.updateContentView(isOver3 ? subscribeList.subList(0, 3) : subscribeList);//只取前三个数据,因为最多只展示三条数据
            }
        } else {
            isShowNoSubscribeView = true;
            headerView.setContentStatus(FragmentSubscribeHeaderView.NO_SUBSCRIBE);
        }
    }

    /**
     * 获取凤凰推荐自媒体
     */
    private void getIfengTVRecommend() {
        WeMediaDao.getWeMediaRecommendList(
                getUid(),
                "ifengWemediaRec",
                "", "",
                System.currentTimeMillis() + "",
                SubscribeList.class,
                new Response.Listener<SubscribeList>() {
                    @Override
                    public void onResponse(SubscribeList response) {
                        if (response == null || ListUtils.isEmpty(response.getWeMediaList())) {
                            if (ListUtils.isEmpty(ifengTVList)) {
                                sendDataEmptyMsg(IFENGTV_EMPTY);
                            }
                        } else {
                            isGetIfengTVDataFail = false;
                            uiStatusLayout.setVisibility(View.VISIBLE);
                            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
                            headerView.setIfengTVStatus(FragmentSubscribeHeaderView.NORMAL);
                            ifengTVList.clear();
                            ifengTVList = handleIfengTVData(response.getWeMediaList());
                            headerView.updateIfengView(ifengTVList);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("getIfengTVRecommend()  error={}", error);
                        handleVolleyError(error, IFENGTV_EMPTY);
                    }
                },
                true);
    }

    /**
     * 获取推荐内容
     */
    private void getSubscribeRecommend() {
        WeMediaDao.getWeMediaList(
                "",
                getUid(),
                WeMediaDao.WE_MEDIA_TYPE_RECOMMEND,
                currentPage + "",
                DataInterface.PAGESIZE_20,
                System.currentTimeMillis() + "",
                WeMediaInfoList.class,
                new Response.Listener<WeMediaInfoList>() {
                    @Override
                    public void onResponse(WeMediaInfoList response) {
                        listView.onRefreshComplete();
                        handleResponse(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("getSubscribeRecommend()  error={}", error);
                        listView.onRefreshComplete();
                        if (currentPage == 1) {
                            handleVolleyError(error, RECOMMEND_EMPTY);
                        } else {
                            if (error != null && error instanceof NetworkError) {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
                            } else {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                            }
                        }
                    }
                },
                true);
    }

    /**
     * 处理订阅推荐响应事件
     */
    private void handleResponse(WeMediaInfoList response) {
        if (response != null && !ListUtils.isEmpty(response.getInfoList())) {
            uiStatusLayout.setVisibility(View.VISIBLE);
            recommendAdapter.showLoadingView();
            headerView.setIfengRecommendErrorStatus(FragmentSubscribeHeaderView.NORMAL);
            uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.PULL_FROM_END);
            isGetRecommendDataFail = false;
            if (currentPage == 1) {
                recommendList.clear();
                weMediaList.clear();
            }
            List<WeMediaInfoList.InfoListEntity> handleList = handleRecommendData(response);
            if (handleList.size() > 0) {
                recommendList.addAll(transformModel(handleList));
            } else {
                if (currentPage > 1 && isPullRequestRecommendData) {//不是第一页并且是上拉加载Toast
                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                }
            }
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            if (!User.isLogin()) {
                headerView.setContentStatus(FragmentSubscribeHeaderView.NO_LOGIN);
            }
            recommendAdapter.setList(recommendList);
            currentPage++;
        } else {
            if (currentPage == 1) {
                if (ListUtils.isEmpty(recommendList)) {
                    sendDataEmptyMsg(RECOMMEND_EMPTY);
                }
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 处理请求数据错误
     */
    private void handleVolleyError(VolleyError error, int type) {
        if (error != null && error instanceof ResponseEmptyException) { // 当结果为"{}"时，返回ResponseEmptyException，所以单独处理该Error
            isShowNoSubscribeView = true;
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            headerView.setContentStatus(FragmentSubscribeHeaderView.NO_SUBSCRIBE);
        } else if (error != null && error instanceof NetworkError) {//无网错误
            handleNetworkError(type);
        } else {//其他错误
            handleVolleyOtherError(type);
        }
    }

    /**
     * 处理无网错误
     */
    private void handleNetworkError(int type) {
        isShowNoSubscribeView = false;
        if (ListUtils.isEmpty(subscribeList) && ListUtils.isEmpty(ifengTVList) && ListUtils.isEmpty(recommendList)) {
            uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
        } else {
            handleVolleyOtherError(type);//无网错误，如果相应模块数据为空，展示加载失败布局
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);//订阅页无网：从其他页面回来需有无网toast提示
        }
    }

    /**
     * 处理Volley其他错误:ResponseEmptyException、NetworkError之外的错误
     */
    private void handleVolleyOtherError(int type) {
        switch (type) {
            case SUBSCRIBE_EMPTY:
                isShowNoSubscribeView = false;
                if (ListUtils.isEmpty(subscribeList)) {
                    sendDataEmptyMsg(type);
                }
                break;
            case IFENGTV_EMPTY:
                if (ListUtils.isEmpty(ifengTVList)) {
                    sendDataEmptyMsg(type);
                }
                break;
            case RECOMMEND_EMPTY:
                if (ListUtils.isEmpty(recommendList)) {
                    sendDataEmptyMsg(type);
                } else {
                    if (isPullRequestRecommendData) {
                        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                    }
                }
                break;
        }
    }

    private static final int SUBSCRIBE_EMPTY = 100;//订阅内容为空
    private static final int IFENGTV_EMPTY = 101;//凤凰卫视为空
    private static final int RECOMMEND_EMPTY = 102;//订阅推荐为空

    /**
     * 发送数据为空的消息
     */
    private void sendDataEmptyMsg(int which) {
        switch (which) {
            case SUBSCRIBE_EMPTY:
                isGetContentDataFail = true;
                if (User.isLogin() && !isShowNoSubscribeView) {
                    headerView.setContentStatus(FragmentSubscribeHeaderView.ERROR);
                }
                break;
            case IFENGTV_EMPTY:
                isGetIfengTVDataFail = true;
                headerView.setIfengTVStatus(FragmentSubscribeHeaderView.ERROR);
                break;
            case RECOMMEND_EMPTY:
                isGetRecommendDataFail = true;
                recommendAdapter.showEmptyView();
                headerView.setIfengRecommendErrorStatus(FragmentSubscribeHeaderView.ERROR);
                uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.DISABLED);
                headerView.showMineSubscribe();
                uiStatusLayout.setStatus(UIStatusLayout.ALL_GONE);
                break;
        }
        handler.sendMessage(handler.obtainMessage());
    }

    /**
     * 对凤凰卫视自媒体数据进行过滤，去重处理
     *
     * @param list 服务端返回的数据
     */
    private List<SubscribeList.WeMediaListEntity> handleIfengTVData(List<SubscribeList.WeMediaListEntity> list) {
        List<SubscribeList.WeMediaListEntity> weMediaList = new ArrayList<>();
        for (SubscribeList.WeMediaListEntity entity : list) {//数据过滤、去重
            if (TextUtils.isEmpty(entity.getWeMediaID()) || TextUtils.isEmpty(entity.getName())) {
                continue;
            }
            weMediaList.add(entity);
        }
        return weMediaList;
    }

    /**
     * 对订阅推荐数据：容错，去重
     *
     * @param response 服务端返回的数据
     * @return 容错，去重之后的数据
     */
    private List<WeMediaInfoList.InfoListEntity> handleRecommendData(WeMediaInfoList response) {
        List<WeMediaInfoList.InfoListEntity> list = new ArrayList<>();

        for (WeMediaInfoList.InfoListEntity infoListEntity : response.getInfoList()) {
            //自媒体容错、去重处理
            if (TextUtils.isEmpty(infoListEntity.getWeMedia().getWeMediaID())
                    || TextUtils.isEmpty(infoListEntity.getWeMedia().getName())
                    || weMediaList.contains(infoListEntity.getWeMedia())) {
                continue;
            }

            list.add(infoListEntity);
            weMediaList.add(infoListEntity.getWeMedia());
        }
        return list;
    }

    /**
     * 数据转化，把服务端返回的数据（容错、去重之后），转换为自己需要的数据
     *
     * @param handleList 服务端返回的数据处理完之后的数据
     * @return 适配器中使用的格式
     */
    private ArrayList<RecommendBaseBean> transformModel(List<WeMediaInfoList.InfoListEntity> handleList) {
        ArrayList<RecommendBaseBean> trueList = new ArrayList<>();//存放转化之后的数据

        for (WeMediaInfoList.InfoListEntity infoListEntity : handleList) {
            //自媒体实体类
            RecommendWeMediaItem weMediaItem = new RecommendWeMediaItem(SubscribeRecommendListAdapter.TYPE_WEMEDIA_INFO);
            weMediaItem.setWeMediaID(infoListEntity.getWeMedia().getWeMediaID());
            weMediaItem.setName(infoListEntity.getWeMedia().getName());
            weMediaItem.setHeadPic(infoListEntity.getWeMedia().getHeadPic());
            weMediaItem.setBanner(infoListEntity.getWeMedia().getBanner());
            weMediaItem.setDesc(infoListEntity.getWeMedia().getDesc());
            weMediaItem.setFollowNo(infoListEntity.getWeMedia().getFollowNo());
            weMediaItem.setFollowed(infoListEntity.getWeMedia().getFollowed());
            weMediaItem.setTotalNum(infoListEntity.getWeMedia().getTotalNum());
            weMediaItem.setTotalPage(infoListEntity.getWeMedia().getTotalPage());
            trueList.add(weMediaItem);//添加自媒体

            List<WeMediaInfoList.InfoListEntity.BodyListEntity> bodyList = infoListEntity.getBodyList();
            int size = bodyList.size() >= 2 ? 2 : bodyList.size();//因为只展示两条视频数据，所以这里获取两条数据
            for (int i = 0; i < size; i++) {
                //视频实体类
                RecommendVideoItem videoItem = new RecommendVideoItem(SubscribeRecommendListAdapter.TYPE_WEMEDIA_VIDEO);
                WeMediaInfoList.InfoListEntity.BodyListEntity entity = bodyList.get(i);
                videoItem.setTitle(entity.getTitle());
                videoItem.setAbstractDesc(entity.getAbstractDesc());
                videoItem.setShowType(entity.getShowType());
                videoItem.setMemberType(entity.getMemberType());
                videoItem.setItemId(entity.getItemId());
                videoItem.setTag(entity.getTag());
                //ImageList实体
                List<RecommendVideoItem.ImageList> imageList = new ArrayList<>();
                for (WeMediaInfoList.InfoListEntity.BodyListEntity.ImageListEntity entity1 : entity.getImagelist()) {
                    RecommendVideoItem.ImageList image = videoItem.new ImageList();
                    image.setImage(entity1.getImage());
                    imageList.add(image);
                }
                videoItem.setImageList(imageList);
                //MemberItem实体类
                RecommendVideoItem.MemberItem memberItem = videoItem.new MemberItem();
                WeMediaInfoList.InfoListEntity.BodyListEntity.MemberItemEntity memberItemEntity = entity.getMemberItem();
                memberItem.setDuration(memberItemEntity.getDuration());
                memberItem.setGuid(memberItemEntity.getGuid());
                memberItem.setSearchPath(memberItemEntity.getSearchPath());
                memberItem.setShareTimes(memberItemEntity.getShareTimes());
                memberItem.setCpName(memberItemEntity.getCpName());
                memberItem.setCreateDate(memberItemEntity.getCreateDate());
                memberItem.setUpdateDate(memberItemEntity.getUpdateDate());
                memberItem.setPlayTime(memberItemEntity.getPlayTime());
                memberItem.setCommentNo(memberItemEntity.getCommentNo());
                memberItem.setName(memberItemEntity.getName());
                memberItem.setmUrl(memberItemEntity.getmUrl());
                memberItem.setPcUrl(memberItemEntity.getPcUrl());
                //VideoFiles实体类
                List<RecommendVideoItem.MemberItem.VideoFiles> videoFileList = new ArrayList<>();
                List<WeMediaInfoList.InfoListEntity.BodyListEntity.MemberItemEntity.VideoFilesEntity> videoFiles = memberItemEntity.getVideoFiles();
                for (WeMediaInfoList.InfoListEntity.BodyListEntity.MemberItemEntity.VideoFilesEntity videoFilesEntity : videoFiles) {
                    RecommendVideoItem.MemberItem.VideoFiles videoFile = memberItem.new VideoFiles();
                    videoFile.setUseType(videoFilesEntity.getUseType());
                    videoFile.setMediaUrl(videoFilesEntity.getMediaUrl());
                    videoFile.setFilesize(videoFilesEntity.getFilesize() + "");
                    videoFile.setSpliteTime(videoFilesEntity.getSpliteTime());
                    videoFile.setSpliteNo(videoFilesEntity.getSpliteNo());
                    videoFile.setIsSplite(videoFilesEntity.getIsSplite());
                    videoFileList.add(videoFile);
                }
                memberItem.setVideoFiles(videoFileList);
                videoItem.setMemberItem(memberItem);
                trueList.add(videoItem);
            }
        }
        return trueList;
    }

    /**
     * 获取当前用户id
     */
    private String getUid() {
        return TextUtils.isEmpty(new User(getActivity()).getUid()) ? "" : new User(getActivity()).getUid();
    }

    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final RecommendWeMediaItem entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(
                entity.getWeMediaID(),
                uid,
                type,
                System.currentTimeMillis() + "",
                SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        getSubscribeContent();//订阅成功重新获取订阅内容
                        getIfengTVRecommend();//订阅成功重新获取凤凰卫视推荐
                        refreshRecommendData();//订阅成功重新获取推荐内容
                        int followNo = 0;
                        if (TextUtils.isDigitsOnly(entity.getFollowNo())) {
                            followNo = Integer.parseInt(entity.getFollowNo());
                        }
                        boolean isSubscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        entity.setFollowed(type);
                        entity.setFollowNo(String.valueOf(isSubscribe ? (++followNo) : (--followNo)));
                        recommendAdapter.notifyDataSetChanged();
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, isSubscribe, entity.getName());
                        ToastUtils.getInstance().showShortToast(isSubscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    private void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(getActivity(), action, title, "");
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    private Handler handler = new WeakHandler(this);

    private static class WeakHandler extends Handler {
        WeakReference<FragmentSubscribe> weakReference;

        public WeakHandler(FragmentSubscribe fragment) {
            weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            FragmentSubscribe fragment = weakReference.get();
            if (fragment == null) {
                return;
            }
            boolean contentEmpty = (fragment.isGetContentDataFail && !fragment.isShowNoSubscribeView) && User.isLogin();//订阅内容为空、未展示无订阅数据View、用户登录
            boolean isAllError = contentEmpty && fragment.isGetIfengTVDataFail && fragment.isGetRecommendDataFail;
            if (isAllError) {
                fragment.uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                fragment.uiStatusLayout.setVisibility(View.GONE);
            }
        }
    }
}
