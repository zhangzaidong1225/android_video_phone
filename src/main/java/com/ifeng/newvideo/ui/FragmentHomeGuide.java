package com.ifeng.newvideo.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.DisplayUtils;

/**
 * 首页手势引导图
 */
public class FragmentHomeGuide extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_guide, null);

        if (getActivity() != null) {
            view.setLayoutParams(new ViewGroup.LayoutParams(DisplayUtils.getWindowWidth(), DisplayUtils.getWindowHeight()));
        }

        view.findViewById(R.id.main_guide_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() == null) {
                    return;
                }
                SharePreUtils.getInstance().setHomeGuideGestureState(false);

                FragmentManager fragmentManager = getFragmentManager();
                if (fragmentManager != null) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    if (fragmentTransaction != null) {
                        fragmentTransaction.remove(FragmentHomeGuide.this);
                        fragmentTransaction.commit();
                    }
                }
            }
        });

        return view;
    }
}