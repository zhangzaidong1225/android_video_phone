package com.ifeng.newvideo.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.ui.maintab.ChannelManagerFragment;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.utils.Util4act;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.newvideo.widget.RedPointImageView;
import com.ifeng.newvideo.widget.ViewPagerColumn;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.ChannelId;
import com.ifeng.video.dao.db.constants.CheckChannelId;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.Channel;
import com.ifeng.video.dao.db.model.LaunchAppModel;
import com.ifeng.video.upgrade.IntentConfig;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guolc on 2016/8/1.
 */
public class FragmentHomePage extends BaseFragment implements View.OnClickListener {


    public static final String KEY_LV3 = "level3";
    public static final String KEY_LV2_VIRTUAL = "level3_virtual";//三级频道在有缓存且缓存频道被编辑过,则新加lv3频道在二级显示
    private ActivityMainTab mActivity;
    private ViewPagerAdapterMain mPagerAdapter;

    private String channel_status;
    public static final String CHANNEL_OFFLINE = "0";
    public static final String CHANNEL_ONLINE = "1";

    private String updateTimeOnline;
    private ViewPagerColumn mViewPager;
    private PagerSlidingTabStrip mChannelTabsLayout;
    private RedPointImageView mChannelMgrView;
    private ImageView mSearchIv;
    private BroadcastReceiver mUpgradeReceiver;
    private View mTabsContainerView;
    private boolean hasEdited;
    private boolean isNeedUpdate = true;
    private String updateTimeLocal;//初始化数据时从sp中取上次启动接口中的 channelUpdate
    private ArrayList<Channel.ChannelInfoBean> mChannels_Lv3 = new ArrayList<>();
    private ArrayList<Channel.ChannelInfoBean> mChannels_Lv2 = new ArrayList<>();
    private ArrayList<Channel.ChannelInfoBean> db_channels = new ArrayList<>();
    private static String mCurChannelId = "";
    private String param_3_2 = "3,2";
    private boolean showRedPoint;
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                mViewPager.setCurrentItem(msg.arg1, true);
                setCurChannelId();
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActivityMainTab) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setFragmentHomePage(this);
        mPagerAdapter = new ViewPagerAdapterMain(getChildFragmentManager());
        if (!PhoneConfig.isGooglePlay()) {
            IntentConfig.startCheckUpgrade(IfengApplication.getInstance());
        }
        mUpgradeReceiver = IntentConfig.registerUIforUpgrade(mActivity);
        hasEdited = SharePreUtils.getInstance().hasEdited();

        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentConfig.CLICK_UPGRADE_LATER);
        filter.addAction(IntentConfig.CLICK_UPGRADE_NOW);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(upgradeReceiver, filter);
    }

    private BroadcastReceiver upgradeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentConfig.CLICK_UPGRADE_LATER)) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_UPGRADE, false, PageIdConstants.PAGE_HOME);
            } else if (intent.getAction().equals(IntentConfig.CLICK_UPGRADE_NOW)) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_UPGRADE, true, PageIdConstants.PAGE_HOME);
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_home_or_doc, container, false);
        mViewPager = (ViewPagerColumn) view.findViewById(R.id.viewPager_mainFragment);
        mChannelTabsLayout = (PagerSlidingTabStrip) view.findViewById(R.id.top_slide_tab_main);
        mTabsContainerView = view.findViewById(R.id.pager_tab_container);
        mChannelMgrView = (RedPointImageView) view.findViewById(R.id.mainpage_expand_iv);
        mSearchIv = (ImageView) view.findViewById(R.id.iv_search);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewPager.setAdapter(mPagerAdapter);
        mChannelTabsLayout.setViewPager(mViewPager);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initListener();
        initData();
        UserPointManager.addRewards(UserPointManager.PointType.addByOpenApp);
    }

    private void initListener() {
        mChannelMgrView.setOnClickListener(this);
        mSearchIv.setOnClickListener(this);
        mChannelTabsLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                if (mPagerAdapter == null) return;

                if (!mCurChannelId.equals(mPagerAdapter.getChannelIdByPosition(position))) {
                    PageActionTracker.endPageHomeCh(mCurChannelId);
                }
                mCurChannelId = mPagerAdapter.getChannelIdByPosition(position);
/*                if (!mCurChannelId.equals("20")) {
                    mActivity.hideOnLineView();
                } else {
                    mActivity.showOnLineView();
                }*/
                PageActionTracker.enterPage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (mPagerAdapter != null) {
            mPagerAdapter.notifyChildFragmentOnHiddenChange(hidden);
        }

        // hidden是true 即将不可见(需上报)，false 即将可见
        if (hidden) {
            PageActionTracker.endPageHomeCh(mCurChannelId);
        } else {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isHidden()) {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isHidden()) {
            PageActionTracker.endPageHomeCh(mCurChannelId);
        }
    }

    //打开频道编辑页，发生统计时间。关闭重新计时
    public void setChannelManageFragmentShow(boolean channelManageFragmentShow) {
        if (channelManageFragmentShow) {
            PageActionTracker.endPageHomeCh(mCurChannelId);
        } else {
            PageActionTracker.enterPage();
        }

    }

    private boolean initRedPoint() {
        return SharePreUtils.getInstance().getRedPointShow();
    }

    /**
     * 判断启动接口中channelUpdate字段是否一样，用于更新频道信息
     */
    private boolean isNeedUpdateChannel() {
        boolean isChannelUpdate = true;
        LaunchAppModel model = (LaunchAppModel) IfengApplication.getInstance().getAttribute(LaunchAppModel.LAUNCH_APP_MODEL);
        if (model != null) {
            updateTimeOnline = model.getChannelUpdate();
            channel_status = model.getChannelStatus();
            updateTimeLocal = SharePreUtils.getInstance().getChannelUpdate();
            if (!TextUtils.isEmpty(updateTimeOnline) && updateTimeOnline.equals(updateTimeLocal)) {
                isChannelUpdate = false;
            }
        }
        return isChannelUpdate || !hasDataInDB() || PhoneConfig.isUpdateUser();
    }

    private void initData() {
        updateViewStatus(Status.LOADING);
        showRedPoint = initRedPoint();
        isNeedUpdate = isNeedUpdateChannel();
        logger.debug("isNeedUpdate = " + isNeedUpdate + "， hasEdited = " + hasEdited + "， channel_status = " + channel_status);

        // FIXME 好别扭的逻辑
        if (!isNeedUpdate || hasEdited) {
            getChannelsFromDB();
        }
        if (isNeedUpdate) {
            requestChannelInfoData();
        }
    }

    private void requestChannelInfoData() {
        logger.debug("requestChannelInfoData");
        ChannelDao.requestChannel(param_3_2, "", Channel.class, new Response.Listener<Channel>() {
            @Override
            public void onResponse(Channel response) {
                if (response == null) {
                    if (ListUtils.isEmpty(mChannels_Lv3)) {
                        updateViewStatus(Status.DATA_ERROR);
                    }
                    return;
                }
                List<Channel.ChannelInfoBean> list = response.getChannelInfo();
                list = filterInvalidChannel(list);
                if (!ListUtils.isEmpty(list)) {
                    handleNetChannelData(list);
                    saveChannelUpdate();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.debug("volley onErrorResponse");
                if (error != null && error instanceof NetworkError) {
                    updateViewStatus(Status.REQUEST_NET_FAIL);
                } else {
                    updateViewStatus(Status.DATA_ERROR);
                }

            }
        }, false, null);

    }

    private void saveChannelUpdate() {
        if (TextUtils.isEmpty(updateTimeOnline)) {
            LaunchAppModel model = (LaunchAppModel) IfengApplication.getInstance().getAttribute(LaunchAppModel.LAUNCH_APP_MODEL);
            if (model != null) {
                updateTimeOnline = model.getChannelUpdate();
            }
        }
        logger.debug("setChannelUpdate {}", updateTimeOnline);
        SharePreUtils.getInstance().setChannelUpdate(updateTimeOnline);
    }

    public ViewPagerColumn getViewPager() {
        return mViewPager;
    }

    private void handleNetChannelData(List<Channel.ChannelInfoBean> netChannels) {
        // 用户没有编辑过频道，
        logger.debug("handleNetChannelData  isNeedUpdate = {},   hasEdited = {}", isNeedUpdate, hasEdited);
        if (isNeedUpdate && !hasEdited) {
            logger.debug("有更新&&未编辑过");
            initChannelNetData(netChannels);
            updateViewStatus(Status.REQUEST_NET_SUCCESS);
            return;
        }

        if (CHANNEL_OFFLINE.equals(channel_status)) {
            handleOffline(netChannels);
        } else if (CHANNEL_ONLINE.equals(channel_status)) {
            handleOnline(netChannels);
        }
        updateViewStatus(Status.REQUEST_NET_SUCCESS);
    }

    private void handleOffline(List<Channel.ChannelInfoBean> netChannels) {
        logger.debug("handleOffline");
        if (!ListUtils.isEmpty(db_channels)) {
            List<Channel.ChannelInfoBean> delete = new ArrayList<>();
            for (Channel.ChannelInfoBean src : db_channels) {
                //已订阅的一级频道不删除
                if (src.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_1) {
                    logger.debug("level 1 : " + src.getChannelName());
                    continue;
                }
                int index = netChannels.indexOf(src);
                if (index > -1) {
                    updateChannelInfoBeanContent(src, netChannels.get(index));
                    logger.debug("更新 : {}", src.getChannelName());
                } else {
                    delete.add(src);
                    logger.debug("下线 : {}", src.getChannelName());
                }
            }
            if (!ListUtils.isEmpty(delete)) {
                mChannels_Lv3.removeAll(delete);

                mPagerAdapter.notifyDataSetChanged();
                setCurChannelId();
                mChannels_Lv2.removeAll(delete);
                try {
                    ChannelDao.deleteChannelList(IfengApplication.getInstance(), delete);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleOnline(List<Channel.ChannelInfoBean> netChannels) {
        logger.debug("handleOnline");
        for (Channel.ChannelInfoBean online : netChannels) {
            List<Channel.ChannelInfoBean> add = new ArrayList<>();
            int index = db_channels.indexOf(online);
            boolean isInDB = index > -1;
            if (isInDB) {
                updateChannelInfoBeanContent(db_channels.get(index), online);
            } else {
                add.add(online);
                logger.debug("add : " + online.getChannelName());
            }
            if (!ListUtils.isEmpty(add)) {
                addChannel2Appropriate(add);
            }
        }
    }

    private void updateChannelInfoBeanContent(Channel.ChannelInfoBean src, Channel.ChannelInfoBean latest) {
        src.setPid(latest.getPid());
        src.setPic(latest.getPic());
        src.setShowType(latest.getShowType());
        src.setChannelName(latest.getChannelName());
        src.setStatisticChannelId(latest.getStatisticChannelId());
        src.setTag(latest.getTag());
        src.setItemId(latest.getItemId());
    }

    /**
     * 已编辑过，2、3级都不改变本地顺序，新上线的2、3级频道要放在2级队首
     * <p>
     * 未编辑过，先都加到列表里，然后按接口itemId排序
     */
    private void addChannel2Appropriate(List<Channel.ChannelInfoBean> add) {
        List<Channel.ChannelInfoBean> addToLv3 = new ArrayList<>();
        List<Channel.ChannelInfoBean> addToLv2 = new ArrayList<>();
        for (Channel.ChannelInfoBean channel : add) {
            channel.setSrcChannelLevel(channel.getChannelLevel());
            // 3级
            if (channel.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_3) {
                if (hasEdited) { //3级编辑过的，插到2级队首
                    channel.setChannelLevel(ChannelDao.CHANNEL_LV_2);
                    int size = mChannels_Lv2.size();
                    if (size > 0) {
                        channel.setSortId(mChannels_Lv2.get(0).getSortId() - 1);
                    } else {
                        channel.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_2);
                    }
                    addToLv2.add(channel);
                    logger.debug("上线，已编辑过，3级频道加到2级队首 : {}, {}", channel.getChannelName(), channel.getSortId());
                } else { //3级未编辑过的，插到3级队尾
                    channel.setChannelLevel(ChannelDao.CHANNEL_LV_3);
                    int size = mChannels_Lv3.size();
                    if (size > 0) {
                        channel.setSortId(mChannels_Lv3.get(size - 1).getSortId() + 1);
                    } else {
                        channel.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_3);
                    }
                    addToLv3.add(channel);
                    logger.debug("上线，未编辑过，3级频道加到3级队尾 : {} , {}", channel.getChannelName(), channel.getSortId());
                }
            }
            // 2级
            else if (channel.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_2) {
                channel.setChannelLevel(ChannelDao.CHANNEL_LV_2);
                int size = mChannels_Lv2.size();
                if (size > 0) {
                    channel.setSortId(mChannels_Lv2.get(0).getSortId() - 1);
                } else {
                    channel.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_2);
                }
                addToLv2.add(channel);
                logger.debug("上线，2级频道加到队首  {}，{}", channel.getChannelName(), channel.getSortId());
            }

            mChannels_Lv3.addAll(addToLv3);
            mPagerAdapter.notifyDataSetChanged();
            setCurChannelId();
            mChannels_Lv2.addAll(0, addToLv2);
            try {
                ChannelDao.createOrUpdate(IfengApplication.getInstance(), add);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void initChannelNetData(List<Channel.ChannelInfoBean> list) {//所有频道数据重新初始化并重新入库
        List<Channel.ChannelInfoBean> s3 = addAndSort(list, ChannelDao.CHANNEL_LV_3);
        if (!ListUtils.isEmpty(s3)) {
            mChannels_Lv3.addAll(s3);
            mPagerAdapter.notifyDataSetChanged();
            setCurChannelId();
        }
        if (!mChannels_Lv3.isEmpty()) {
            mTabsContainerView.setVisibility(View.VISIBLE);
            mChannelMgrView.setShowRedPoint(showRedPoint);
        }

        List<Channel.ChannelInfoBean> s2 = addAndSort(list, ChannelDao.CHANNEL_LV_2);
        if (!ListUtils.isEmpty(s2)) {
            mChannels_Lv2.addAll(s2);
        }
        try {
            ChannelDao.createOrUpdateAndDeleteAllOld(IfengApplication.getInstance(), list);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private List<Channel.ChannelInfoBean> addAndSort(List<Channel.ChannelInfoBean> toAdd, int level) {
        if (!ListUtils.isEmpty(toAdd)) {
            List<Channel.ChannelInfoBean> list = new ArrayList<>();
            for (int i = 0; i < toAdd.size(); i++) {
                Channel.ChannelInfoBean bean = toAdd.get(i);
                bean.setSrcChannelLevel(bean.getChannelLevel());
                if (level == ChannelDao.CHANNEL_LV_3 && bean.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_3) {
                    bean.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_3 + i);
                    list.add(bean);
                } else if (level == ChannelDao.CHANNEL_LV_2 && bean.getSrcChannelLevel() == ChannelDao.CHANNEL_LV_2) {
                    bean.setSortId(ChannelDao.CHANNEL_SORD_ID_BENCHMARK_2 + i);
                    list.add(bean);
                }
            }
            return list;
        }
        return null;
    }

    private void getChannelsFromDB() {
        logger.debug("getChannelsFromDB");
        try {
            List<Channel.ChannelInfoBean> list = ChannelDao.getAllChannels(IfengApplication.getInstance());
            if (list != null && !list.isEmpty()) {
                db_channels.addAll(list);
                if (!isNeedUpdate) {
                    updateViewStatus(Status.REQUEST_NET_SUCCESS);
                }
                List<Channel.ChannelInfoBean> temp = new ArrayList<>();
                for (Channel.ChannelInfoBean channel : list) {
                    if (channel == null) {
                        continue;
                    }
                    if (PhoneConfig.isGooglePlay()) {
                        // 谷歌渠道去掉美女和时尚
                        if (!TextUtils.isEmpty(channel.getChannelName())
                                && (channel.getChannelName().contains("美女") || channel.getChannelName().contains("时尚"))) {
                            continue;
                        }
                    }

                    if (channel.getChannelLevel() == ChannelDao.CHANNEL_LV_3) {
                        temp.add(channel);
                    } else if (channel.getChannelLevel() == ChannelDao.CHANNEL_LV_2) {
                        mChannels_Lv2.add(channel);
                    }
                }
                if (!ListUtils.isEmpty(temp)) {
                    mChannels_Lv3.addAll(temp);
                    mPagerAdapter.notifyDataSetChanged();
                    setCurChannelId();
                }
                if (!mChannels_Lv3.isEmpty()) {
                    mTabsContainerView.setVisibility(View.VISIBLE);
                    mChannelMgrView.setShowRedPoint(showRedPoint);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            requestNet();
        }
        logger.debug("getChannelsFromDB  " + "mChannels_Lv3 = " + mChannels_Lv3.size() + ", mChannels_Lv2 = " + mChannels_Lv2.size());
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.error("homepage" + "--onDestroy");
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        CommonStatictisListUtils.getInstance().sendDestoryList();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
        logger.error("homepage" + "--onDetach");
        unregisterUpgradeReceiver();
    }

    /**
     * unregister升级receiver
     */
    private void unregisterUpgradeReceiver() {
        if (mUpgradeReceiver != null) {
            LocalBroadcastManager.getInstance(IfengApplication.getAppContext()).unregisterReceiver(mUpgradeReceiver);
            mUpgradeReceiver = null;
        }
    }

    public void setTabsVisible(int visible) {
        mTabsContainerView.setVisibility(visible);
    }

    @Override
    protected void requestNet() {
        logger.debug("requestNet");
        updateViewStatus(Status.LOADING);
        requestChannelInfoData();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mainpage_expand_iv) {

            if (mPagerAdapter != null) {
                mPagerAdapter.stopPlayIfNecessary();
            }
            toChannelMangerFragment();

            mChannelMgrView.hideRedPoint();
            if (showRedPoint) {
                SharePreUtils.getInstance().setRedPointShow(false);
            }
            PageActionTracker.clickChAdd();
        }

        if (v.getId() == R.id.iv_search) {
            String page = String.format(PageIdConstants.HOME_CHANNEL, mCurChannelId);
            Log.d("home", page);
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_HOME, page);
            IntentUtils.toSearchActivity(mActivity);
        }

    }

    private ChannelManagerFragment channelManagerFragment;

    private void toChannelMangerFragment() {
        channelManagerFragment = new ChannelManagerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(FragmentHomePage.KEY_LV3, mChannels_Lv3);
        bundle.putParcelableArrayList(FragmentHomePage.KEY_LV2_VIRTUAL, mChannels_Lv2);
        channelManagerFragment.setArguments(bundle);
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.common_slide_left_in, R.anim.common_slide_right_out);
        transaction.add(R.id.rl_maintab, channelManagerFragment, ChannelManagerFragment.CHANNEL_MANAGER_TAG);
        transaction.commitAllowingStateLoss();
        IfengApplication.getInstance().setWellChosenTimestamp(System.currentTimeMillis());
    }

    /**
     * 关闭频道管理页
     */
    public void closeChannelMangerFragment() {
        if (channelManagerFragment != null) {
            FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.common_slide_left_in, R.anim.common_slide_right_out);
            transaction.remove(channelManagerFragment).commit();
        }
    }

    public String setCurChannelId() {
        if (mViewPager == null) return null;
        int curPos = mViewPager.getCurrentItem();
        if (mChannels_Lv3 != null && mChannels_Lv3.size() > curPos && mChannels_Lv3.get(curPos) != null) {
            mCurChannelId = mChannels_Lv3.get(curPos).getChannelId();
        }
        return mCurChannelId;
    }

    public static String getCurChannelId() {
        return mCurChannelId;
    }

    public class ViewPagerAdapterMain extends FragmentStatePagerAdapter {

        private FragmentManager fragmentManager;

        public ViewPagerAdapterMain(FragmentManager fragmentManager) {
            super(fragmentManager);
            this.fragmentManager = fragmentManager;
        }

        public void notifyChildFragmentOnHiddenChange(boolean hidden) {
            if (fragmentManager == null) {
                return;
            }
            List<Fragment> fragments = fragmentManager.getFragments();
            if (!ListUtils.isEmpty(fragments)) {
                for (Fragment f : fragments) {
                    if (f instanceof ChannelBaseFragment ||
                            f instanceof LianboChannelFragment ||
                            f instanceof FmChannelFragment) {
                        f.onHiddenChanged(hidden);
                    }
                }
            }
        }

        public void stopPlayIfNecessary() {
            if (fragmentManager == null) {
                return;
            }
            List<Fragment> fragments = fragmentManager.getFragments();
            if (!ListUtils.isEmpty(fragments)) {
                for (Fragment f : fragments) {
                    if (f instanceof UniversalChannelFragment) {
                        UniversalChannelFragment uni = (UniversalChannelFragment) f;
                        uni.recoverUI();
                    }
                }
            }
        }

        public void refreshWellChosenData() {
            if (fragmentManager == null) {
                return;
            }
            List<Fragment> fragments = fragmentManager.getFragments();
            if (!ListUtils.isEmpty(fragments)) {
                for (Fragment f : fragments) {
                    if (f instanceof WellChosenFragment) {
                        WellChosenFragment fragment = (WellChosenFragment) f;
                        fragment.requestNetIfNeed();
                    }
                }
            }
        }

        public void refreshCurrentFragmentData(boolean isWellChosenRefetchDataIf) {
            if (fragmentManager == null) {
                return;
            }
            String channelId = getChannelIdByPosition(mViewPager.getCurrentItem());
            if (channelId == null) {
                return;
            }
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments == null) {
                return;
            }
            for (Fragment f : fragments) {
                if (f instanceof ChannelBaseFragment) {
                    ChannelBaseFragment base = (ChannelBaseFragment) f;
                    if (channelId.equals(base.getChannel_id())) {
                        if (isWellChosenRefetchDataIf && base instanceof WellChosenFragment) {
                            ((WellChosenFragment) base).requestNet();
                        } else {
                            base.onRefresh(null);
                        }
                        break;
                    }
                } else if (f instanceof LianboChannelFragment) {
                    ((LianboChannelFragment) f).onRefresh(null);
                    break;
                } else if (f instanceof FmChannelFragment) {
                    ((FmChannelFragment) f).requestNet(null);
                }
            }
        }

        @Override
        public Fragment getItem(int position) {
            Channel.ChannelInfoBean bean = mChannels_Lv3.get(position);
            String showType = bean.getShowType();

            String channelId = bean.getChannelId();
            Fragment fragment;
            Bundle jumpBundle = new Bundle();
            jumpBundle.putString(IntentKey.CHANNELID, channelId);
            if (ChannelConstants.isMix(showType)) {
                fragment = new WellChosenFragment();
            } else if (ChannelConstants.isColumn(showType)) {
                fragment = new IfengTVFragment();
            } else if (ChannelConstants.isFm(showType)) {
                fragment = new FmChannelFragment();
            } else if (ChannelConstants.isTopic(showType)) {
                fragment = new LianboChannelFragment();
                jumpBundle.putString(IntentKey.CHANNEL_TYPE, showType);
            } else if (ChannelConstants.isVR(showType)) {
                fragment = new VRChannelFragment();
            } else if (ChannelConstants.isRecommend(showType)) {
                fragment = new RecommendFragment();
            } else {
                fragment = new UniversalChannelFragment();
            }
            fragment.setArguments(jumpBundle);
            return fragment;
        }

        @Override
        public int getCount() {
            if (mChannels_Lv3 != null) {
                return mChannels_Lv3.size();
            }
            return 0;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (mChannels_Lv3 == null || position >= mChannels_Lv3.size()) {
                return "";
            }
            Channel.ChannelInfoBean info = mChannels_Lv3.get(position);
            // 新闻推荐-->精选
            if (CheckChannelId.isMainPager(info.getChannelId())
                    || Util4act.Constants.SHOUYE_ORIGIN.equals(info.getChannelName())) {
                return Util4act.Constants.SHOUYE_RECOMMEND;
            }
            // 联播专题-->联播台
            if (ChannelId.SUBCHANNELID_TOPIC_LIANBO.equals(info.getChannelId())
                    || Util4act.Constants.LiBOTAI_RECOMMEND.equals(info.getChannelName())) {
                return Util4act.Constants.LiBOTAI_RECOMMEND;
            }
            return info.getChannelName();
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            if (mChannelTabsLayout != null) {
                mChannelTabsLayout.notifyDataSetChanged();
            }
        }

        public String getChannelIdByPosition(int position) {
            if (!ListUtils.isEmpty(mChannels_Lv3) && position < mChannels_Lv3.size() && mChannels_Lv3.get(position) != null) {
                return mChannels_Lv3.get(position).getChannelId();
            }
            return "";
        }
    }

    public void switchCurrentChannelFragment(String channelId) {
        int itemPosition = getItemPositionByChannelId(channelId);
        if (itemPosition != -1) {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                mViewPager.setCurrentItem(itemPosition, true);
                setCurChannelId();
            } else {
                Message message = mHandler.obtainMessage();
                message.what = 1;
                message.arg1 = itemPosition;
                mHandler.sendMessage(message);
            }
        }
    }

    private int getItemPositionByChannelId(String channel_id) {
        if (TextUtils.isEmpty(channel_id)) {
            return -1;
        }
        for (int i = 0; i < mChannels_Lv3.size(); i++) {
            Channel.ChannelInfoBean info = mChannels_Lv3.get(i);
            if (channel_id.equals(info.getChannelId())) {
                return i;
            }
        }
        return -1;
    }

    public void updateChannelData(ArrayList<Channel.ChannelInfoBean> channels_Lv3, ArrayList<Channel.ChannelInfoBean> channels_Lv2) {
        if (mPagerAdapter != null && !mChannels_Lv3.equals(channels_Lv3)) {
            this.mChannels_Lv3.clear();
            this.mChannels_Lv3.addAll(channels_Lv3);
            mPagerAdapter.notifyDataSetChanged();
            setCurChannelId();
        }
        if (channels_Lv2 != null && !mChannels_Lv2.equals(channels_Lv2)) {
            mChannels_Lv2.clear();
            mChannels_Lv2.addAll(channels_Lv2);
        }
    }

    private List<Channel.ChannelInfoBean> filterInvalidChannel(List<Channel.ChannelInfoBean> list) {
        if (!ListUtils.isEmpty(list)) {
            List<Channel.ChannelInfoBean> add = new ArrayList<>();
            for (Channel.ChannelInfoBean bean : list) {
                // 谷歌渠道去掉美女和时尚
                if (PhoneConfig.isGooglePlay()) {
                    boolean isBeautyOrFashionChannel = bean != null && !TextUtils.isEmpty(bean.getChannelName())
                            && (bean.getChannelName().contains("美女") || bean.getChannelName().contains("时尚"));
                    if (isBeautyOrFashionChannel) {
                        continue;
                    }
                }

                boolean isValidChannel = bean != null && bean.getChannelLevel() <= 3 && bean.getChannelLevel() >= 1 &&
                        !TextUtils.isEmpty(bean.getChannelName()) && !TextUtils.isEmpty(bean.getChannelId())
                        && !TextUtils.isEmpty(bean.getShowType()) && !add.contains(bean);
                if (isValidChannel) {
                    //4.1系统以下不支持VR直播
                    if (ChannelConstants.isVR(bean.getShowType())) {
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                            add.add(bean);
                        }
                    } else {
                        add.add(bean);
                    }
                }
            }
            return add;
        }
        return null;
    }

    private boolean hasDataInDB() {
        try {
            return ChannelDao.hasData(IfengApplication.getInstance());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
