package com.ifeng.newvideo.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.PageRefreshConstants;
import com.ifeng.newvideo.dialogUI.DanmakuCommentFragment;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.HeaderViewPagerAdapter;
import com.ifeng.newvideo.ui.adapter.WellChosenFragmentAdapter;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.LastDocUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.bean.VideoDanmuItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.BigPicAdView;
import com.ifeng.newvideo.videoplayer.widget.skin.DanmuEditView;
import com.ifeng.newvideo.videoplayer.widget.skin.FloatVideoViewSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayAdButton;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayButton;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.DefaultControllerView;
import com.ifeng.newvideo.widget.HeadFlowView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.ifeng.newvideo.ui.adapter.ListAdapter4BigPictureChannel.STATUS_PAUSE;
import static com.ifeng.newvideo.ui.adapter.ListAdapter4BigPictureChannel.STATUS_PLAYING;


public class WellChosenFragment extends ChannelBaseFragment implements AbsListView.OnScrollListener, IPlayController.OnPlayCompleteListener,
        VideoSkin.OnNetWorkChangeListener, VideoSkin.OnLoadFailedListener, VideoSkin.onClickVideoSkin, DefaultControllerView.OnShowOrHideDanmuView,
        PlayAdButton.onPlayAndPreplayClickListener, PlayButton.OnPlayOrPauseListener, BigPicAdView.OnClickAdView, DanmuEditView.OnClickEditButton {

    /**
     * 离开此页面判读是否刷新的阈值
     */
    private String requireTime = "";

    private int mUpTimes = 0;
    //    private View mSearchView;
    private View mHeadView;
    private HeadFlowView mHeadFlowView;
    private static final String TAG = WellChosenFragment.class.getName();

    private ViewGroup mLandScapeContainer;
    private VideoSkin mVideoSkin;
    private NormalVideoHelper mVideoHelper;
    private UIPlayContext mUIPlayerContext;
    private FrameLayout mVideoViewWrapper;
    private ActivityMainTab mActivity;
    private Resources mResource;
    private int mHeadViewCount;
    private boolean needRecover;
    private boolean hasRegisterReceiver;
    private boolean isVisible;
    private boolean mHidden;
    private WellChosenFragmentAdapter picAdapter;
    private String mCurrentPiPVideoGuid;


    private Comparator<ChannelBean.HomePageBean> comparator = new Comparator<ChannelBean.HomePageBean>() {
        @Override
        public int compare(ChannelBean.HomePageBean lhs, ChannelBean.HomePageBean rhs) {
            if (lhs != null && rhs != null) {
                String update_1 = lhs.getUpdateDate();
                String update_2 = rhs.getUpdateDate();
                if (!TextUtils.isEmpty(update_1) && !TextUtils.isEmpty(update_2)) {
                    return -update_1.compareTo(update_2);
                }
            }
            return 0;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActivityMainTab) activity;
        mResource = mActivity.getResources();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requireTime=SharePreUtils.getInstance(mActivity).getChannelDataLastTime(mChannel_id);
        mVideoViewWrapper = new FrameLayout(mActivity);
        initVideoSkin();
    }

    private void initVideoSkin() {
        mUIPlayerContext = new UIPlayContext();
        mUIPlayerContext.skinType = VideoSkin.SKIN_TYPE_PIC;
        mUIPlayerContext.channelId = mChannel_id;
        mVideoSkin = new VideoSkin(mActivity);
        mVideoSkin.setId(R.id.video_skin);
        mVideoSkin.setVideoMargin(true);
        mVideoSkin.setOnLoadFailedListener(this);
        mVideoSkin.setNoNetWorkListener(this);
        mVideoSkin.setOnClickVideoSkin(this);
        mVideoSkin.setClipChildren(false);
        mVideoSkin.setClipToPadding(false);
        mVideoViewWrapper.addView(mVideoSkin);
        mVideoHelper = new NormalVideoHelper();
        mVideoHelper.init(mVideoSkin, mUIPlayerContext);
        mVideoHelper.setOnPlayCompleteListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_well_chosen, container, false);
//        mSearchView = inflater.inflate(R.layout.well_chosen_search_layout, null);
        mHeadView = inflater.inflate(R.layout.well_chosen_header_banner, null);
        mHeadView.setVisibility(View.VISIBLE);
        mHeadFlowView = (HeadFlowView) mHeadView.findViewById(R.id.well_chosen_headFlowView);
        mFocusList = new ArrayList<>(20);
        initView(contentView);
        initAdapterAndListener();
        isVisible = true;
        mHidden = false;
        setVideoSkinListener();
        return contentView;
    }

    private void setVideoSkinListener() {
        if (null != mVideoSkin.getPlayAdView()) {
            mVideoSkin.getPlayAdView().setOnPlayAndPrePlayClickListener(this);
        }
        if (null != mVideoSkin.getPlayView()) {
            mVideoSkin.getPlayView().setPlayOrPauseListener(this);
        }
        if (null != mVideoSkin.getBigPicAdView()) {
            mVideoSkin.getBigPicAdView().setOnClickAdView(this);
        }
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getBaseControlView().setShowListener(this);
            initDanmuViewStatus();
        }
        if (null != mVideoSkin.getDanmuEditView()) {
            mVideoSkin.getDanmuEditView().setEditListener(this);
        }

    }


    private void initDanmuViewStatus() {
        if (!IfengApplication.danmaSwitchStatus) {
            Log.d("danmu", "close:" + mChannel_id);
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuEditView() && null != mVideoSkin.getBaseControlView()) {
                mVideoSkin.getDanmuView().hideView();
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
                mVideoSkin.getDanmuView().setShowEditView(false);
                mVideoSkin.getDanmuView().initDanmukuView();
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }
        } else {
            Log.d("danmu", "open:" + mChannel_id);
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuEditView() && null != mVideoSkin.getBaseControlView()) {
                mVideoSkin.getDanmuView().showView();
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
                mVideoSkin.getDanmuView().initDanmukuView();
                mVideoSkin.getBaseControlView().setShowControlView(true);
            }
        }

    }

    private void initView(View contentView) {
        mPullToRefreshListView = (MyPullToRefreshListView) contentView.findViewById(R.id.page_listView);
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mPullToRefreshListView.setShowIndicator(false);
        mPullToRefreshListView.getRefreshableView().addHeaderView(mHeadView);
        mHeadViewCount = mPullToRefreshListView.getRefreshableView().getHeaderViewsCount();

        mLandScapeContainer = (FrameLayout) contentView.findViewById(R.id.rl_landscape_container);
        mLandScapeContainer.setVisibility(View.GONE);
    }

    private void initAdapterAndListener() {
        mHeaderViewPagerAdapter = new HeaderViewPagerAdapter(this, mActivity, mChannel_id);
        mHeadFlowView.setViewPagerAdapter(mHeaderViewPagerAdapter);

        mAdapter = new WellChosenFragmentAdapter(13, mActivity, this, mChannel_id);
        picAdapter = (WellChosenFragmentAdapter) mAdapter;
        picAdapter.setVideoHelper(mVideoHelper);
        picAdapter.setVideoSkinWrapper(mVideoViewWrapper);
        picAdapter.setUIPlayContext(mUIPlayerContext);
        picAdapter.setDanmuView(mVideoSkin.getDanmuView());
        mPullToRefreshListView.setAdapter(mAdapter);
        mPullToRefreshListView.getRefreshableView().setOnItemClickListener(this);
        mPullToRefreshListView.setOnRefreshListener(this);
        mPullToRefreshListView.setOnLastItemVisibleListener(this);
        mPullToRefreshListView.setOnScrollListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestNet();
    }

    @Override
    protected void requestNet() {
        mPullToRefreshListView.getRefreshableView().setSelection(0);
        if (mAdapter == null || ListUtils.isEmpty(mAdapter.getDataList())) {
            updateViewStatus(Status.LOADING);
        }
        mUpTimes = 0;
        requestNet("", DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
    }

    private void requestNet(String position_id_or_requre_time, final String count,
                            final Status status, int action, String operation, String upTimes) {
        final boolean netAvailable = NetUtils.isNetAvailable(IfengApplication.getInstance());
        if (!netAvailable) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
        if (isLoading) {
            mPullToRefreshListView.onRefreshComplete();
            mPullToRefreshListView.hideFootView();
            return;
        }
        isLoading = true;
        VolleyHelper.getRequestQueue().cancelAll(TAG);
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String nw = NetUtils.getNetType(getActivity());
        ChannelDao.requestChannelData(mChannel_id, mChannel_Type, count, position_id_or_requre_time, 0,
                ChannelBean.class, true, TAG, action, SharePreUtils.getInstance().getInreview(), operation,
                new User(IfengApplication.getAppContext()).getUid(), PhoneConfig.userKey, LastDocUtils.getLastDoc(), upTimes,
                province, city, nw, PhoneConfig.publishid,
                new Response.Listener<ChannelBean>() {
                    @Override
                    public void onResponse(ChannelBean response) {
                        isLoading = false;
                        if (response != null && !ListUtils.isEmpty(response.getBodyList())) {
                            if (Status.FIRST == status) {
                                handleFirstRequest(response, status);
                            } else if (Status.REFRESH == status) {
                                recoverUI();
                                handleRefreshRequest(response, status);
                            } else {
                                handleMoreRequest(response, status);
                            }
                            updateViewStatus(Status.REQUEST_NET_SUCCESS);
                            mActivity.controlOnLineView();
                        } else {
                            logger.debug("volley response null");
                            onDataEmpty(status, netAvailable);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("volley onErrorResponse ! {}", error);
                        handleRequestError(status, error);
                        isLoading = false;
                        if (Status.REFRESH == status) {
                            mPullToRefreshListView.getRefreshableView().setSelection(0);
                        } else if (Status.LOAD_MORE == status) {
                            if (netAvailable) {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                            }
                        }
                        mActivity.hideOnLineView();
                    }
                });

        PageActionTracker.pullCh(status == Status.LOAD_MORE, mChannel_id);
    }

    private void handleFirstRequest(ChannelBean response, Status status) {
        requireTime = response.getSystemTime();
        logger.debug("first requireTime:{}", requireTime);
        List<ChannelBean.HomePageBean> bodyList = filterList(response.getBodyList(), null, status, false);
        if (!ListUtils.isEmpty(bodyList)) {
            mAdapter.setData(bodyList);
        }

        List<ChannelBean.HomePageBean> headList = filterList(response.getHeader(), null, status, true);
        if (!ListUtils.isEmpty(headList)) {
            mHeaderViewPagerAdapter.setData(headList);
            mHeaderViewPagerAdapter.notifyDataSetChanged();
        }
        freshStatus();
    }

    private void handleMoreRequest(ChannelBean response, Status status) {
        List<ChannelBean.HomePageBean> bodyList = filterList(response.getBodyList(), mAdapter.getDataList(), status, false);
        if (ListUtils.isEmpty(bodyList)) {
            if (NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
            mPullToRefreshListView.hideFootView();
        } else {
            mAdapter.addData(bodyList, false);
            freshStatus();
        }
    }

    private void handleRefreshRequest(ChannelBean response, Status status) {
        mPullToRefreshListView.onRefreshComplete();
        requireTime = response.getSystemTime();
        logger.debug("refresh requireTime:{}", requireTime);
        if (NetUtils.isNetAvailable(IfengApplication.getInstance())) {
            List<ChannelBean.HomePageBean> bodyList = filterList(response.getBodyList(), mAdapter.getDataList(), status, false);
            if (!ListUtils.isEmpty(bodyList)) {
                Collections.sort(bodyList, comparator);
                mAdapter.addData(bodyList, true);
            }

            List<ChannelBean.HomePageBean> headList = filterList(response.getHeader(), mHeaderViewPagerAdapter.getData(), status, false);
            if (!ListUtils.isEmpty(headList)) {
                mHeaderViewPagerAdapter.setData(headList);
                mHeaderViewPagerAdapter.notifyDataSetChanged();
            }

            freshStatus();
        }
    }

    private void onDataEmpty(Status status, boolean net) {
        if (Status.FIRST == status) {
            updateViewStatus(Status.DATA_ERROR);
        } else if (Status.LOAD_MORE == status) {
            mPullToRefreshListView.hideFootView();
            if (net) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        } else if (net && status == Status.REFRESH) {
            freshStatus();
        }
    }


    private void freshStatus() {
        mAdapter.notifyDataSetChanged();
        mPullToRefreshListView.hideFootView();
        mPullToRefreshListView.onRefreshComplete();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        logger.debug(mChannel_id + "--onHiddenChanged:{}", hidden);
        mHidden = hidden;
        if (hidden) {
            if (mVideoHelper != null && hasRegisterReceiver) {
                hasRegisterReceiver = false;
                mVideoHelper.onPause();
            }
            if (isWellDanmaShow()) {
                mVideoSkin.getDanmuView().onPause();
            }

            recoverUI();
        } else {
            if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
                hasRegisterReceiver = true;
                mVideoHelper.onResume();
            }
            danmuResume();

        }
    }

    private void danmuResume() {
        if (null != mVideoSkin && null != mVideoSkin.getDanmuView()) {
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getBaseControlView().setShowControlView(true);
            } else {
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }
            mVideoSkin.getDanmuView().onResume();
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!isVisible || mHidden) {
            logger.debug(mChannel_id + "--onConfigurationChanged isVisible:{}--mHidden:{}", isVisible, mHidden);
            return;
        }

        mVideoHelper.onConfigureChange(newConfig);
        //用流的标记模拟不发送V的标记
        mUIPlayerContext.isBigReportVideo = true;
        initAdViewPosition();

        if (Configuration.ORIENTATION_LANDSCAPE == mResource.getConfiguration().orientation) {
            doOrientationLandscape();
            PageActionTracker.endPageHomeCh(mChannel_id);
        } else if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            //切竖屏的时候，报横屏播放器page的统计
            PageActionTracker.endPageVideoPlay(true, "", "");
            doOrientationPortrait();
            if (null != mDanmakuEditFragment && !mDanmakuEditFragment.isHidden()) {
                mDanmakuEditFragment.dismissAllowingStateLoss();
            }
        }
        //用流的标记模拟不发送V的标记
        mUIPlayerContext.isBigReportVideo = false;
    }

    private void initAdViewPosition() {
        if (mUIPlayerContext.videoType.equals(AdTools.videoType)) {
            ValueAnimator mAnimator = null;
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(30.0f), DisplayUtils.convertDipToPixel(0));
            mAnimator.setDuration(100);
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        if (mVideoSkin.getBigPicAdView().getTranslationY() != 0) {
                            mVideoSkin.getPlayAdView().bringToFront();
                            mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                            mVideoSkin.getBigPicAdView().invalidate();
                        }
                    }
                }
            });
            mAnimator.setRepeatCount(0);
            mAnimator.start();
        }
    }

    private void doOrientationPortrait() {
        mPullToRefreshListView.setVisibility(View.VISIBLE);
        if (mLandScapeContainer != null) {
            mLandScapeContainer.setVisibility(View.GONE);
        }

        mActivity.setTabVisible(View.VISIBLE);
        setFullscreen(false);
        if (needRecover) {
            needRecover = false;
            recoverUI();
        } else {
            picAdapter.back2PortraitAndContinuePlay();
        }
        setViewPagerScanScroll(true);

    }


    private void doOrientationLandscape() {

        if (mVideoViewWrapper.getParent() != null) {
            ViewGroup parent = (ViewGroup) mVideoViewWrapper.getParent();
            parent.removeView(mVideoViewWrapper);
        }
        mLandScapeContainer.addView(mVideoViewWrapper);

        mPullToRefreshListView.setVisibility(View.GONE);
        mActivity.setTabVisible(View.GONE);
        mActivity.hideOnLineView();
        mLandScapeContainer.setVisibility(View.VISIBLE);
        setFullscreen(true);
        setViewPagerScanScroll(false);
        if (null != mVideoSkin && null != mVideoSkin.getDanmuView() && mVideoSkin.getDanmuView().isShown()) {
            if (!isDanmuSendStatus) {
                mVideoSkin.getDanmuEditView().hideView();
            } else {
                mVideoSkin.getDanmuEditView().showView();
            }
        }

        if (picAdapter.isPlaying) {
            picAdapter.continuePlay();
        }
    }

    private void setViewPagerScanScroll(boolean scroll) {
        mActivity.getFragmentHomePage().getViewPager().setScanScroll(scroll);
    }


    private void setFullscreen(boolean on) {
        Window win = getActivity().getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object obj = parent.getAdapter().getItem(position);
        if (obj == null) {
            return;
        }
        if (obj instanceof String) {
            requestNet(requireTime, DataInterface.PAGESIZE_6, Status.REFRESH, ChannelDao.REFRESH, ChannelConstants.DOWN, String.valueOf(++mUpTimes));
        } else if (obj instanceof ChannelBean.HomePageBean) {
            ChannelBean.HomePageBean bean = (ChannelBean.HomePageBean) obj;
            bean.setWatched(true);

            CommonStatictisListUtils.getInstance().sendHomeFeedYoukuConstatic(bean, CommonStatictisListUtils.YK_NEXT);

            String simId = bean.getMemberItem() == null ? "" : bean.getMemberItem().getSimId();
            String rToken = bean.getMemberItem() == null ? "" : bean.getMemberItem().getrToken();
            int subPosition = position - mHeadViewCount + 1;
            PageActionTracker.clickHomeItem(String.valueOf(subPosition), simId, rToken);

            dispatchClickEvent(bean);



        }
    }


    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        super.onRefresh(refreshView);
        requestNet(requireTime, DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DOWN, String.valueOf(++mUpTimes));
    }

    @Override
    public void onLastItemVisible() {
        super.onLastItemVisible();
        logger.debug("onMyLastItemVisible");
        ChannelBean.HomePageBean bean = mAdapter.getLastItem();
        if (bean != null && !TextUtils.isEmpty(bean.getItemId())) {
            requestNet(bean.getItemId(), DataInterface.PAGESIZE_20, Status.LOAD_MORE, ChannelDao.LOAD_MORE, ChannelConstants.UP, "");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mVideoHelper != null) {
            mVideoHelper.onDestroy();
            if (null != mVideoSkin.getDanmuView()) {
                mVideoSkin.getDanmuView().onDestory();
            }
        }
        IfengApplication.getInstance().setWellChosenTimestamp(0);
        sendStaticList();
    }

    private void sendStaticList() {
        mFocusList = CommonStatictisListUtils.wellChosenFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(mFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.wellChosenFocusList.clear();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mVideoHelper != null && hasRegisterReceiver) {
            hasRegisterReceiver = false;
            mVideoHelper.onPause();
        }
        if (isWellDanmaShow()) {
            mVideoSkin.getDanmuView().onPause();
        }

        if (mActivity.isLandScape()) {
            needRecover = true;
        } else {
            recoverUI();
        }
        IfengApplication.getInstance().setWellChosenTimestamp(System.currentTimeMillis());
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        logger.debug("well:{}", isVisibleToUser);
        isVisible = isVisibleToUser;
        SharePreUtils.getInstance().getVisibleMap().put(mChannel_id, isVisible);
        if (isVisibleToUser) {
            if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
                hasRegisterReceiver = true;
                mVideoHelper.onResume();
            }
            danmuResume();

            requestNetIfNeed();
        } else {
//            if (mVideoHelper != null && hasRegisterReceiver) {
//                hasRegisterReceiver = false;
//                mVideoHelper.onPause();
//            }
//            if (isWellDanmaShow()) {
//                mVideoSkin.getDanmuView().onPause();
//            }
            recoverUI();
            IfengApplication.getInstance().setWellChosenTimestamp(System.currentTimeMillis());
        }
    }

    private boolean isWellDanmaShow() {
        return null != mVideoSkin && null != mVideoSkin.getDanmuView() &&
                null != mVideoSkin.getDanmuView().getDanmakuView() &&
                mVideoSkin.getDanmuView().getDanmakuView().isShown();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
            hasRegisterReceiver = true;
            mVideoHelper.onResume();
        }

        danmuResume();

        needRecover = false;
        requestNetIfNeed();
    }

    public void requestNetIfNeed() {
        final long lastTimestamp = IfengApplication.getInstance().getWellChosenTimestamp();
        IfengApplication.getInstance().setWellChosenTimestamp(0);
        if (!isLoading && lastTimestamp != 0 && System.currentTimeMillis() - lastTimestamp > PageRefreshConstants.HOMEPAGE_REFRESH_DATA_INTERVAL) {
            logger.debug("currentTimeMillis()--" + System.currentTimeMillis() + "lastTimestamp:" + lastTimestamp);
            requestNet();
        }
    }

    @Override
    public void onPlayComplete() {
        if (mActivity.isLandScape()) {
            needRecover = true;
//            mActivity.toPortrait();
        }
        if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
            mVideoSkin.showControllerView();
        } else {
            recoverUI();
        }

        if (picAdapter.adVideoRecord != null) {
            picAdapter.adVideoRecord.setCompleteNum();
            picAdapter.adVideoRecord.stopPlayTime();
        }

    }

    public void recoverUI() {
        if (picAdapter != null) {
            if (mUIPlayerContext.videoType != null) {
                if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                    initAdViewPosition();
                }
            }
            //destoryAndCreatDanmakuView();
            picAdapter.recoverUI();
        }
    }

    private FloatVideoPlayController.OnListViewScrollToPiPVideoView mOnListViewScrollToPiPVideoView = new FloatVideoPlayController.OnListViewScrollToPiPVideoView() {
        @Override
        public void onPlayToVisibleView(String guid, FloatVideoViewSkin skin) {
            if (isVisible) {
                mCurrentPiPVideoGuid = guid;
            }
        }
    };

    public void recoverUI(boolean showPiP) {
        if (picAdapter != null) {
            if (mUIPlayerContext.videoType != null) {
                if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                    initAdViewPosition();
                }
            }
            //destoryAndCreatDanmakuView();
            picAdapter.recoverUI(showPiP, mOnListViewScrollToPiPVideoView);
        }
    }

    @Override
    public void onNoNetWorkClick() {
        pauseAndHideDanmuView();
        picAdapter.continuePlay();
    }

    private void pauseAndHideDanmuView() {
        if (null != mVideoSkin.getDanmuView()) {
            if (null == mVideoSkin.getDanmuView().getDanmakuView()) {
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
            }
            mVideoSkin.getDanmuView().getDanmakuView().pause();
            mVideoSkin.getDanmuView().getDanmakuView().hide();
        }
    }

    private void destoryAndCreatDanmakuView() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().clearDanmaku();
            mVideoSkin.getDanmuView().onDestory();

            mVideoSkin.getDanmuView().showView();
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getDanmuView().showDanmukuView();
            } else {
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
            }
        }
    }

    @Override
    public void onMobileClick() {
        pauseAndHideDanmuView();
        picAdapter.continuePlay();
    }

    @Override
    public void onLoadFailedListener() {
        pauseAndHideDanmuView();
        picAdapter.continuePlay();
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
    }

    private boolean isRecovered = false;

    @Override
    public void onScroll(final AbsListView absListView, int firstVisibleItem, int visibleItemCount, int i2) {
        //TODO 抽出来
        if (picAdapter.getClickToPlayPositon() > -1 &&
                (firstVisibleItem + visibleItemCount - mHeadViewCount == picAdapter.getClickToPlayPositon()
                        || firstVisibleItem + visibleItemCount == picAdapter.getClickToPlayPositon()
                        || picAdapter.getClickToPlayPositon() + 2 == firstVisibleItem - mHeadViewCount
                        || picAdapter.getClickToPlayPositon() + 1 == firstVisibleItem - mHeadViewCount)) {

            if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                if (mUIPlayerContext.status == IPlayer.PlayerState.STATE_PLAYING) {
                    isRecovered = true;
                }
                if (mUIPlayerContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    isRecovered = false;
                }
            }
            if (picAdapter.playStatus == STATUS_PLAYING) {
                recoverUI(SharePreUtils.getInstance().getPiPModeState());
            } else {
                recoverUI();
            }
        }
        if (EmptyUtils.isNotEmpty(mActivity) && mActivity.getPiPMode()) {
            List<ChannelBean.HomePageBean> list = picAdapter.getDataList();
            for (int i = 0; i < list.size(); i++) {
                if (EmptyUtils.isNotEmpty(list.get(i).getMemberItem())
                        && EmptyUtils.isNotEmpty(list.get(i).getMemberItem().getGuid())
                        && list.get(i).getMemberItem().getGuid().equals(mCurrentPiPVideoGuid)
                        && !CheckIfengType.isAdBackend(list.get(i).getMemberType())) {
                    ListView listView = mPullToRefreshListView.getRefreshableView();
                    i += listView.getHeaderViewsCount();
                    if (i >= firstVisibleItem && i <= (firstVisibleItem + visibleItemCount - 1)) {
                        scollToCurrentPiPPositionAndPlay(500, true, i - listView.getHeaderViewsCount());
                    }
                    break;
                }
            }
        }
    }

    private void scollToCurrentPiPPositionAndPlay(final int mScrollDuration, final boolean isPortrait, final int index) {
        logger.debug("scollToNextPositionAndPlay:");
        final HomePageBeanBase base = picAdapter.getDataList().get(index);
        if (base == null) {
            return;
        }
        final ListView listView = mPullToRefreshListView.getRefreshableView();
        if (CheckIfengType.isAD(base.getMemberType()) || AdTools.videoType.equals(base.getMemberItem().adConditions.showType)) {
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int i = index;
                    scollToCurrentPiPPositionAndPlay(mScrollDuration, isPortrait, i++);
                }
            }, mScrollDuration);
        } else {
            final int next_index = index;
            mVideoHelper.setBookMark(mActivity != null ? mActivity.getPiPPlayerLastPosition() : 0);
            picAdapter.autoPlayNext(base, next_index, getViewByPosition(listView, next_index), isPortrait);
            ((ActivityMainTab) getActivity()).removePiPViews();
        }
    }

    private View getViewByPosition(ListView listView, int adapterPosition) {
        final int headerCount = listView.getHeaderViewsCount();
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = listView.getLastVisiblePosition();
        int position = adapterPosition + headerCount;
        logger.debug("headviewCount1", "firstListItemPosition:" + firstListItemPosition + "" +
                "\n lastListItemPosition:" + lastListItemPosition + "" + "\n adapterPosition:" + adapterPosition +
                "\n position:" + position);

        if (position < firstListItemPosition || position > lastListItemPosition) {
            logger.debug("越界将导致播放异常");
//            return listView.getAdapter().getView(position-headerCount, null, listView);
            return null;
        } else {
            final int childIndex = position - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    @Override
    public void showControllerView() {
        ValueAnimator mAnimator = null;
        mVideoSkin.getBigPicAdView().clearAnimation();

        if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(0), -DisplayUtils.convertDipToPixel(30.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getPlayAdView().bringToFront();
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });
            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        } else {
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(0), -DisplayUtils.convertDipToPixel(40.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().bringToFront();
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });

            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();
        }
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mVideoSkin.bringChildToFront(mVideoSkin.getBigPicAdView());
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    @Override
    public void hiddenControllerView() {
        ValueAnimator mAnimator = null;
        mVideoSkin.getBigPicAdView().clearAnimation();

        if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            mAnimator = ValueAnimator.ofFloat(-DisplayUtils.convertDipToPixel(30.0f), DisplayUtils.convertDipToPixel(0));
            mAnimator.setDuration(200);
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getPlayAdView().bringToFront();
                        if (mVideoSkin.getBigPicAdView().getTranslationY() != 0) {
                            mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                            mVideoSkin.getBigPicAdView().invalidate();
                        }
                    }
                }
            });
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        } else {
            mAnimator = ValueAnimator.ofFloat(-DisplayUtils.convertDipToPixel(40.0f), DisplayUtils.convertDipToPixel(0.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    //将子view置顶
                    if (mVideoSkin.getBigPicAdView() != null) {
                        if (mVideoSkin.getBigPicAdView().getTranslationY() != 0) {

                            mVideoSkin.getBigPicAdView().bringToFront();

                            mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                            mVideoSkin.getBigPicAdView().invalidate();
                        }
                    }
                }
            });

            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        }

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mVideoSkin.bringChildToFront(mVideoSkin.getBigPicAdView());

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    public void onPlayClick() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }

    @Override
    public void prePlayClick() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.setPrePlayNum();
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }

    @Override
    public void onClickStopUrl() {
        if (!ScreenUtils.isLand()) {
            recoverUI();
        }
    }

    @Override
    public void onPausePlayButton() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.setPauseNum();
            picAdapter.adVideoRecord.stopPlayTime();
            initAdViewPosition();
        }
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }
        picAdapter.playStatus = STATUS_PAUSE;
    }

    @Override
    public void onPlayButton() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }
        picAdapter.playStatus = STATUS_PLAYING;
    }

    @Override
    public void onClickAdToKnow() {
        if (!ScreenUtils.isLand()) {
            recoverUI();
        }
    }

    private String preGuid = "";

    public void setDanmaData(String guid, List<VideoDanmuItem> danmaData) {
        if (null == mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
        }

        // 待用
        if (!TextUtils.isEmpty(preGuid) && preGuid.equalsIgnoreCase(guid)) {
            mVideoSkin.getDanmuView().currentVideoPosition((int) (mVideoHelper.getLastPosition() / 1000));
        } else {
            preGuid = guid;
            mVideoSkin.getDanmuView().currentVideoPosition(0);
        }

        mVideoSkin.getDanmuView().addDataSource(danmaData);

    }

    @Override
    public void onClickEditButton() {
        if (ScreenUtils.isLand()) {
            if (mVideoHelper != null) {
                mVideoHelper.onPause();
            }
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
                mVideoSkin.getDanmuView().getDanmakuView().pause();
            }
            showEditDanmaWindow("", "");
        } else {
            //播放底页
            picAdapter.toVideoActivityForDanmu();
        }

    }

    public void updateLocalDanmu(String comment) {

        if (null != mVideoHelper) {
            mVideoHelper.onResume();
        }

        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }

        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().sendTextMessage(comment);
        }
    }


    private DanmakuCommentFragment mDanmakuEditFragment;

    public void showEditDanmaWindow(String comments, String comment_id) {
        if (mDanmakuEditFragment == null) {
            mDanmakuEditFragment = new DanmakuCommentFragment();
        }

        mDanmakuEditFragment.setShowInputMethod(false);
        mDanmakuEditFragment.setCommends(comments, comment_id);
        mDanmakuEditFragment.setVideoItem(mUIPlayerContext.videoItem);
        mDanmakuEditFragment.setNormalVideoPlayer(mVideoHelper);
        mDanmakuEditFragment.setFragment(this);

        if (!mDanmakuEditFragment.isAdded()) {
            mDanmakuEditFragment.show(getActivity().getSupportFragmentManager(), "danmuDialog");

        }
    }

    @Override
    public void showDanmuView(boolean isShow) {
        if (isShow) {
            mVideoSkin.getDanmuView().showView();
            if (isDanmuSendStatus) {
                mVideoSkin.getDanmuEditView().showView();
            }
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            mVideoSkin.getDanmuView().showDanmukuView();
            IfengApplication.danmaSwitchStatus = true;

            if (mVideoSkin.getDanmuView().getDanmakuView().isPaused()) {
                mVideoSkin.getDanmuView().getDanmakuView().resume();
            }
            if (mUIPlayerContext.status != IPlayer.PlayerState.STATE_PAUSED) {
                mVideoSkin.getDanmuView().resumeDanmaku();
            } else {
                mVideoSkin.getDanmuView().getDanmakuView().pause();
            }
        } else {
            mVideoSkin.getDanmuView().hideView();
            mVideoSkin.getDanmuView().hideDanmakuView();
            mVideoSkin.getDanmuEditView().hideView();
            IfengApplication.danmaSwitchStatus = false;
        }
    }

    private boolean isDanmuSendStatus;

    public void setDanmuSendStatus(boolean isSend) {
        this.isDanmuSendStatus = isSend;
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().setShowEditView(isSend);
        }
    }
}
