package com.ifeng.newvideo.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.*;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.ui.live.TVProgramDialogFragment;
import com.ifeng.newvideo.ui.live.adapter.LiveListAdapter;
import com.ifeng.newvideo.ui.maintab.LiveData;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.widget.skin.TitleView;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.*;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.LiveDao;
import com.ifeng.video.dao.db.model.live.LiveListInfo;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.dao.db.model.live.TVLiveListInfo;
import com.ifeng.video.dao.db.model.live.TVLiveProgramInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 直播fragment
 * Created by Vincent on 2014/11/10.
 */
public class FragmentLive extends FragmentBase implements NotifyShareCallback {
    private static final Logger logger = LoggerFactory.getLogger(FragmentLive.class);

    private View titleLayout;//顶部标题
    private View headerView;//ListView的HeaderView
    private UIStatusLayout uiStatusLayout;
    private PullToRefreshListView listView;//直播列表ListView
    private LiveListAdapter liveListAdapter;//直播列表适配器
    private RecyclerView recyclerView;//电视台列表View
    private TVListAdapter tvListAdapter;

    /**
     * 视频播放相关的
     */
    private NormalVideoHelper mPlayerHelper;
    private VideoSkin mVideoSkin;
    private UIPlayContext mPlayContext;

    private List<TVLiveListInfo.LiveInfoEntity> tvList = new ArrayList<>();//电视台列表
    private List<LiveListInfo.BodyEntity> liveList = new ArrayList<>();//直播列表

    private boolean isPullRequestLivedData = false;//是否上拉加载更多直播数据

    private OneKeyShare mOneKeyShare;
    private String channelName;//频道名称

    private int currentPage = 1;//当前页码
    private String positionId = "";
    private TVLiveInfo tvLiveInfo;
    private int currentPlayPosition = 0;//当前正在播放的电视台位置，默认第一个
    private boolean isRegisterReceiver;

    private LiveData liveData;//直播页需要的数据，包括channelId,echid,chid
    private ActivityMainTab mActivity;
    private NofiticationControlFinishReceiver mNofiticationControlFinishReceiver;

    private TVProgramDialogFragment dialogFragment;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ActivityMainTab) {
            mActivity = (ActivityMainTab) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mActivity != null) {
            mActivity.setFragmentLive(this);
        }
        handleNotificationFinishReceiver(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity.setFragmentLive(null);
        }
        handleNotificationFinishReceiver(false);
        if (mPlayerHelper != null) {
            mPlayerHelper.onDestroy();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_live, container, false);
        initHeaderView(inflater);
        initView(root);
        initSkin(root);
        measureViewHeight(titleLayout, mVideoSkin);
        initAdapter();
        mOneKeyShare = new OneKeyShare(mActivity);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        liveListAdapter.showLoadingView();
        liveData = mActivity.getLiveData();//取直播页数据
        refreshLiveData(true);
        logger.debug("liveData={}", liveData.toString());
    }

    private boolean isHidden = false;//是否不可见

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (mPlayerHelper != null) {
            mPlayerHelper.setLeaveStateIsPause(false);//切换Tab重置当前是否是暂停状态，保证再次切Tab进入直播会开始播放而不是暂停状态
        }
        isHidden = hidden;
        if (mPlayerHelper != null && !mPlayerHelper.isPlayAudio()) {
            currentPlayPosition = 0;//从底部Tab切回来，播放的不是音频则重置当前播放的位置，保证播第一个电视台
        }
        if (mPlayerHelper != null) {
            mPlayerHelper.setIsPlayWhenSurfaceCreated(!hidden);//surface创建成功是否播放视频，如果当前不是直播Tab，则不播放，否则切出直播页锁屏、置后台再唤醒，直播会播放视频。
        }
        if (hidden) {
            liveData.resetLiveData();//重置直播页数据，保证再次进入数据正确
            if (mPlayerHelper != null && isRegisterReceiver) {
                mPlayerHelper.onPause();
                isRegisterReceiver = false;
            }
            PageActionTracker.endPageLive();
        } else {
            liveData = mActivity.getLiveData();//取直播页数据
            mPlayContext.channelId = liveData.getEchid();
            PlayQueue.getInstance().setEchid(liveData.getEchid());
            refreshLiveData(true);
            if (mPlayerHelper != null && !isRegisterReceiver) {
                mPlayerHelper.onResume();
                isRegisterReceiver = true;
            }
            PageActionTracker.enterPage();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isHidden) {
            liveData = mActivity.getLiveData();//取直播数据
            mPlayContext.channelId = liveData.getEchid();
            PlayQueue.getInstance().setEchid(liveData.getEchid());
            if (liveData.isFromPush()) {//从推送进入要播视频（如果当前为暂停状态，要改为播放状态）
                if (mPlayerHelper != null) {
                    mPlayerHelper.setLeaveStateIsPause(false);//重置暂停状态
                }
            }
            refreshLiveData(false);
            //Page 统计
            PageActionTracker.enterPage();
            if (liveData.isFromPush()) {
                PageActionTracker.lastPage = PageIdConstants.REF_PUSH;
            }
        }
        if (mPlayerHelper != null && !isRegisterReceiver && !isHidden) {
            mPlayerHelper.onResume();
            isRegisterReceiver = true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mActivity.getLiveData().setFromPush(false);//重置是否来自推送,目的是保证onResume()时如果是暂停状态还是保持暂停状态
        mActivity.getLiveData().setChannelId("");//重置ChannelId，保证onResume()时播放为当前播放的电视台，而不是推送过来的电视台Id
        if (mPlayerHelper != null && isRegisterReceiver && !isHidden) {
            mPlayerHelper.onPause();
            isRegisterReceiver = false;
        }
        if (!isHidden) {
            PageActionTracker.endPageLive();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mPlayerHelper != null) {
            mPlayerHelper.onConfigureChange(newConfig);
        }
        boolean landScape = isLandScape();
        if (landScape && dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }
        updateTitleStatus(landScape);
        changeOrientation(landScape);
    }

    private void updateTitleStatus(boolean isLandScape) {
        if (isLandScape) {
            mVideoSkin.getLoadView().showBackView();
            mVideoSkin.getNoNetWorkView().showBackView();
        } else {
            mVideoSkin.getLoadView().hideBackView();
            mVideoSkin.getNoNetWorkView().hideBackView();
        }
    }

    private void changeOrientation(boolean isLandScape) {
        if (!isLandScape && tvListAdapter != null) {
            tvListAdapter.notifyDataSetChanged();
        }
        titleLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mActivity.setTabVisible(isLandScape ? View.GONE : View.VISIBLE);
        setFullscreen(isLandScape);
        if (!isHidden) {
            PageActionTracker.endPageTvPlay(!isLandScape, "", "");
        }
    }

    private void setFullscreen(boolean on) {
        Window win = mActivity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    private int titleHeight;
    private int videoHeight;

    /**
     * 测量View高度
     */
    private void measureViewHeight(final View title, final View video) {
        if (title == null || video == null) {
            return;
        }
        ViewTreeObserver observer = title.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                titleHeight = title.getHeight();
                videoHeight = video.getHeight();
                title.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                //title.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    /**
     * 刷新直播数据
     *
     * @param isRefreshLiveList 是否刷新直播列表数据
     */
    private void refreshLiveData(boolean isRefreshLiveList) {
        isPullRequestLivedData = false;
        currentPage = 1;
        positionId = "";
        if (isRefreshLiveList) {
            getLiveList();
        }
        getTVLiveList(isRefreshLiveList);
    }

    private void initView(View root) {
        titleLayout = root.findViewById(R.id.title_layout);
        View searchLayout = root.findViewById(R.id.search);
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_HOME, PageIdConstants.PAGE_LIVE);
//                IntentUtils.toSearchActivity(getActivity());
//                Intent intent = new Intent(mActivity, ActivityVideoPlayerLive.class);
//                intent.putExtra("channelId", liveData.getChannelId());
//                startActivity(intent);

            }
        });

        uiStatusLayout = (UIStatusLayout) root.findViewById(R.id.ui_status_layout);
        uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
        uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.DISABLED);
        listView = uiStatusLayout.getListView();
        listView.setShowIndicator(false);
        listView.getRefreshableView().addHeaderView(headerView);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                isPullRequestLivedData = true;
                getLiveList();
            }
        });
    }

    /**
     * 初始化HeaderView
     */
    private void initHeaderView(LayoutInflater inflater) {
        headerView = inflater.inflate(R.layout.fragment_live_header_view, null);
        recyclerView = (RecyclerView) headerView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    /**
     * 初始化皮肤
     */
    private void initSkin(View view) {
        mVideoSkin = (VideoSkin) view.findViewById(R.id.video_skin);
        mPlayContext = new UIPlayContext();
        mPlayContext.skinType = VideoSkin.SKIN_TYPE_TV;
        mPlayerHelper = new NormalVideoHelper();
        mPlayerHelper.init(mVideoSkin, mPlayContext);
        mVideoSkin.setNoNetWorkListener(new VideoSkin.OnNetWorkChangeListener() {
            @Override
            public void onNoNetWorkClick() {
                reLoadLiveList();
                rePlayVideo();
            }

            @Override
            public void onMobileClick() {
                rePlayVideo();
            }
        });
        mVideoSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener() {
            @Override
            public void onLoadFailedListener() {
                reLoadLiveList();
                rePlayVideo();
            }
        });

        if (null != mVideoSkin.getTitleView()) {
            mVideoSkin.getTitleView().setOnShareProgramLClickListener(new TitleView.OnShareProgramLClickListener() {
                @Override
                public void onShareClick() {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE, PageIdConstants.PAGE_LIVE);
                    showSharePop();
                }

                @Override
                public void onLiveProgramClick() {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_EPG, PageIdConstants.PAGE_LIVE);
                    dialogFragment = TVProgramDialogFragment.newInstance(tvLiveInfo.getChannelId());
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    dialogFragment.show(ft, "TVProgramDialog");
                }
            });
        }
    }

    /**
     * 重新播放视频
     */
    private void rePlayVideo() {
        if (tvLiveInfo == null) {
            getTVLiveList(false);
        } else {
            playVideo();
        }
    }

    /**
     * 播放
     */
    private void playVideo() {
        if (isHidden) {
            return;
        }
        if (NetUtils.isMobile(mActivity.getApplicationContext()) && IfengApplication.mobileNetCanPlay) {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }
        mPlayContext.isFromPush = liveData != null && liveData.isFromPush();
        mPlayContext.title = channelName;
        mPlayContext.tvLiveInfo = tvLiveInfo;
        String url;
        if (mPlayerHelper.isPlayAudio()) {//当前播音频
            url = tvLiveInfo.getAudio();//取音频地址
            PlayQueue.getInstance().setTVLiveInfo(tvLiveInfo);
        } else {//否则取视频地址
            url = StreamUtils.getMediaUrlForTV(tvLiveInfo);
        }
        mPlayerHelper.openVideo(url);
        logger.debug("playVideo() url={}", url);
        mPlayContext.streamType = StreamUtils.getLiveStreamType();
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        liveListAdapter = new LiveListAdapter();
        listView.getRefreshableView().setAdapter(liveListAdapter);
        liveListAdapter.setOnLoadFailClick(new LiveListAdapter.OnLoadFailedClick() {
            @Override
            public void onLoadFailedClick() {
                liveListAdapter.showLoadingView();
                refreshLiveData(true);
                boolean isRePlayVideo = mVideoSkin.getErrorView().getVisibility() == View.VISIBLE
                        || mVideoSkin.getNoNetWorkView().getVisibility() == View.VISIBLE;
                if (isRePlayVideo) {
                    rePlayVideo();
                }
            }
        });

        tvListAdapter = new TVListAdapter();
        recyclerView.setAdapter(tvListAdapter);
        if (ListUtils.isEmpty(tvList)) {
            tvList.clear();
            tvList.addAll(getTVFakeData());
        }
        tvListAdapter.setData(tvList);
        tvListAdapter.setOnTvClick(new TVListAdapter.OnTvClick() {
            @Override
            public void onTvClick(int position, TVLiveListInfo.LiveInfoEntity entity) {
                if (mPlayerHelper != null) {//切换电视台，重置当前是否是暂停状态，保证切换到其他电视台能正常播放而不是暂停状态
                    mPlayerHelper.setLeaveStateIsPause(false);
                }
                if (currentPlayPosition == position) {//如果选择的电视台跟当前播放的一致，返回
                    return;
                }

                mPlayContext.channelId = "";
                PlayQueue.getInstance().setEchid("");
                currentPlayPosition = position;//设置电视台位置
                tvListAdapter.setPlayPosition(position);//重新选择电视台后重置选中的状态
                getProgramList(entity, false);//数据获取成功取当前电视台节目单数据
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_TV_ITEM, PageIdConstants.PAGE_LIVE);
            }
        });
    }

    /**
     * 获取电视台直播列表
     */
    private void getTVLiveList(final boolean isRefresh) {
        LiveDao.getTVLiveAddress(TVLiveListInfo.class,
                new Response.Listener<TVLiveListInfo>() {
                    @Override
                    public void onResponse(TVLiveListInfo response) {
                        if (response != null && !ListUtils.isEmpty(response.getLiveInfo())) {
                            tvList.clear();
                            tvList.addAll(response.getLiveInfo());
                            tvListAdapter.setData(tvList);
                            int size = tvList.size();
                            for (int i = 0; i < size; i++) {
                                TVLiveListInfo.LiveInfoEntity entity = tvList.get(i);
                                if (TextUtils.isEmpty(entity.getChannelId())) {
                                    continue;
                                }
                                // 拿传进来的channelId去接口中匹配channelId或者title
                                boolean isChannelIdExist = entity.getChannelId().toLowerCase().equalsIgnoreCase(liveData.getChannelId())
                                        || entity.getTitle().toLowerCase().equalsIgnoreCase(liveData.getChannelId());
                                if (isChannelIdExist) {
                                    currentPlayPosition = i;//找到对应的电视台索引，没找到就是当前正在播放的电视台索引
                                }
                                logger.debug("currentPlayPosition={}", currentPlayPosition);
                            }
                            if (currentPlayPosition >= size) {
                                currentPlayPosition = 0;
                            }
                            getProgramList(tvList.get(currentPlayPosition), liveData.isPlayVideo());//数据获取成功取当前电视台节目单数据
                            tvListAdapter.setPlayPosition(currentPlayPosition);
                        } else {
                            if (ListUtils.isEmpty(tvList)) {
                                tvList.addAll(getTVFakeData());
                            }
                            tvListAdapter.setData(tvList);
                            initTVLiveData(tvList.get(currentPlayPosition), liveData.isPlayVideo(), "");
                            tvListAdapter.setPlayPosition(currentPlayPosition);
                        }
                        //默认报一次选中视频
                        if (!isRefresh) {
                            PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_TV_ITEM, PageIdConstants.PAGE_LIVE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //默认报一次选中视频
                        if (!isRefresh) {
                            PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_TV_ITEM, PageIdConstants.PAGE_LIVE);
                        }
                        if (ListUtils.isEmpty(tvList)) {
                            tvList.addAll(getTVFakeData());
                        }
                        tvListAdapter.setData(tvList);
                        initTVLiveData(tvList.get(currentPlayPosition), liveData.isPlayVideo(), "");
                        tvListAdapter.setPlayPosition(currentPlayPosition);
                    }
                }, false);
    }

    /**
     * 重新获取直播列表数据
     */
    private void reLoadLiveList() {
        if (currentPage == 1 && ListUtils.isEmpty(liveList)) {
            liveListAdapter.showLoadingView();
            isPullRequestLivedData = false;
            positionId = "";
            getLiveList();
        }
    }

    /**
     * 获取直播列表
     */
    private void getLiveList() {
        final String channelLid = "";
        final int isNotModified = 0;
        LiveDao.getLiveList(channelLid, isNotModified, positionId,
                DataInterface.PAGESIZE_20, LiveListInfo.class,
                new Response.Listener<LiveListInfo>() {
                    @Override
                    public void onResponse(LiveListInfo response) {
                        listView.onRefreshComplete();
                        handleResponseEvent(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listView.onRefreshComplete();
                        boolean success = loadLiveListCacheDataSuccess(channelLid, isNotModified, positionId, DataInterface.PAGESIZE_20);
                        if (success) {//获取缓存数据成功
                            return;
                        }
                        boolean isNetWorkError = error instanceof NetworkError;//是否是无网错误
                        if (currentPage == 1 && ListUtils.isEmpty(liveList)) {
                            if (isNetWorkError) {
                                liveListAdapter.showNoNetView();
                            } else {
                                liveListAdapter.showErrorView();
                            }
                            uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.DISABLED);
                        } else {
                            ToastUtils.getInstance().showShortToast(isNetWorkError ? R.string.toast_no_net : R.string.toast_no_more);
                        }
                    }
                }, true);

        if (!"".equals(positionId)) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.PAGE_LIVE);
        }
    }

    /**
     * 处理响应事件
     */
    private void handleResponseEvent(LiveListInfo response) {
        if (response != null && !ListUtils.isEmpty(response.getBody())) {
            if (!NetUtils.isNetAvailable(mActivity)) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);//直播有缓存，断网，从其他页面回来需有无网toast提示
            }
            if (currentPage == 1) {
                liveList.clear();
            }
            List<LiveListInfo.BodyEntity> handleList = handleLiveData(response);
            if (handleList.size() > 0) {
                liveList.addAll(handleList);
            } else {
                if (currentPage > 1 && isPullRequestLivedData) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                }
            }
            currentPage++;//获取数据成功之后
            uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.PULL_FROM_END);
            liveListAdapter.setData(liveList);
        } else {
            if (currentPage == 1 && ListUtils.isEmpty(liveList)) {
                liveListAdapter.showErrorView();
                uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.DISABLED);
            } else {
                uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.PULL_FROM_END);
                boolean isNetAvailable = NetUtils.isNetAvailable(mActivity.getApplicationContext());
                ToastUtils.getInstance().showShortToast(isNetAvailable ? R.string.toast_no_more : R.string.toast_no_net);
            }
        }
    }

    /**
     * 获取直播列表缓存数据是否成功
     */
    private boolean loadLiveListCacheDataSuccess(String channelId, int isNotModified, String positionId, String pageSize) {
        String url = DataInterface.liveRecommend(channelId, isNotModified, positionId, pageSize);
        String jsonString = CommonDao.getCache(url);
        LiveListInfo liveListInfo = JsonUtils.parseObject(jsonString, LiveListInfo.class);
        if (liveListInfo == null || ListUtils.isEmpty(liveListInfo.getBody())) {
            return false;
        }
        if (TextUtils.isEmpty(positionId)) {
            liveList.clear();
        }
        List<LiveListInfo.BodyEntity> handleList = handleLiveData(liveListInfo);
        if (handleList.size() == 0) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
        }
        liveList.addAll(handleList);
        uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.PULL_FROM_END);
        liveListAdapter.setData(liveList);
        return true;
    }

    /**
     * 处理直播列表数据
     *
     * @param response 服务端返回的数据
     */
    private List<LiveListInfo.BodyEntity> handleLiveData(LiveListInfo response) {
        List<LiveListInfo.BodyEntity> list = new ArrayList<>();
        for (LiveListInfo.BodyEntity entity : response.getBody()) {
            if (TextUtils.isEmpty(entity.getUrl()) || liveList.contains(entity)) {//容错、去重处理
                continue;
            }
            list.add(entity);
        }
        if (response.getBody().size() > 0) {
            int lastIndex = response.getBody().size() - 1;
            positionId = response.getBody().get(lastIndex).getItemId();
        }
        return list;
    }

    private String currentChannelId = "";//当前正在播放的电视台id

    /**
     * 初始化节目信息
     *
     * @param liveInfoEntity 直播实体类
     * @param changeToVideo  是否切换成视频模式（如果当前是音频）
     * @param title          当前播放节目标题
     */
    private void initTVLiveData(TVLiveListInfo.LiveInfoEntity liveInfoEntity, boolean changeToVideo, String title) {
        tvLiveInfo = new TVLiveInfo();
        tvLiveInfo.setAudio(liveInfoEntity.getAudio());
        tvLiveInfo.setChannelId(liveInfoEntity.getChannelId());
        tvLiveInfo.setmUrl(liveInfoEntity.getMUrl());
        tvLiveInfo.setTitle(liveInfoEntity.getTitle());
        tvLiveInfo.setcName(liveInfoEntity.getCName());
        tvLiveInfo.setVideo(liveInfoEntity.getVideo());
        tvLiveInfo.setVideoInReview(liveInfoEntity.getVideoInReview());
        tvLiveInfo.setVideoH(liveInfoEntity.getVideoH());
        tvLiveInfo.setVideoM(liveInfoEntity.getVideoM());
        tvLiveInfo.setVideoL(liveInfoEntity.getVideoL());
        tvLiveInfo.setBigIconURL(liveInfoEntity.getBigIconURL());
        tvLiveInfo.setSmallIconURL(liveInfoEntity.getSmallIconURL());
        tvLiveInfo.setDescription(liveInfoEntity.getDescription());
        tvLiveInfo.setImg490_490(liveInfoEntity.getImg490_490());

        List<TVLiveInfo.Schedule> scheduleList = new ArrayList<>();
        TVLiveInfo.Schedule schedule = tvLiveInfo.new Schedule();
        schedule.setProgramTitle(title);
        scheduleList.add(schedule);
        tvLiveInfo.setSchedule(scheduleList);//添加当前正在播放的节目单

        channelName = tvLiveInfo.getcName();
        mOneKeyShare.initShareStatisticsData(liveInfoEntity.getChannelId(), liveData.getEchid(), "", "", PageIdConstants.PLAY_LIVE_V);
        mOneKeyShare.initSmartShareData(UserOperatorConst.TYPE_LIVE, channelName, tvLiveInfo.getTitle());

        if (changeToVideo) {
            currentChannelId = "";
        }
        if (!currentChannelId.equals(liveInfoEntity.getChannelId())) {//channelId不同时重新播放，否则播放当前的，保证音频模式下会续播而不是重新加载播放
            currentChannelId = liveInfoEntity.getChannelId();
            playVideo();
        }

    }


    /**
     * 分享
     */
    private void showSharePop() {
        if (tvLiveInfo != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            String name = channelName;
            String image = tvLiveInfo.getBigIconURL();
            //精选首页进来，分享标题和图片为推荐位标题和图片
            if (!TextUtils.isEmpty(liveData.getShareTitle()) && !TextUtils.isEmpty(liveData.getShareImg())) {
                name = liveData.getShareTitle();
                image = liveData.getShareImg();
            }
            mOneKeyShare.shareLive(tvLiveInfo.getmUrl(), mVideoSkin, name, this, tvLiveInfo.getcName(), image, false);
        }
    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageLive();
        } else {
            PageActionTracker.enterPage();
        }
    }

    /**
     * 直播列表适配器
     */
    static class TVListAdapter extends RecyclerView.Adapter<RecyclerHolder> {
        private List<TVLiveListInfo.LiveInfoEntity> list = new ArrayList<>();

        private int playPosition = 0;//哪个电视台正在播放，默认是第一个电视台

        public void setPlayPosition(int playPosition) {
            this.playPosition = playPosition;
            notifyDataSetChanged();
        }

        private OnTvClick onTvClick;

        public interface OnTvClick {
            /**
             * 点击电视台回调
             *
             * @param position 点击的位置
             * @param entity   点击的电视台实体
             */
            void onTvClick(int position, TVLiveListInfo.LiveInfoEntity entity);
        }

        public void setOnTvClick(OnTvClick onTvClick) {
            this.onTvClick = onTvClick;
        }

        public void setData(List<TVLiveListInfo.LiveInfoEntity> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_tv_live_item_layout, viewGroup, false);
            RecyclerHolder holder = new RecyclerHolder(view);
            holder.rootView = view.findViewById(R.id.view_parent);
            holder.tvName = (TextView) view.findViewById(R.id.tv_name);
            holder.tvShape = (ImageView) view.findViewById(R.id.tv_shape);
            holder.tvImg = (NetworkImageView) view.findViewById(R.id.tv_icon);
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ClickUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (onTvClick != null) {
                        int position = (int) view.getTag(R.id.view_parent);
                        onTvClick.onTvClick(position, list.get(position));
                    }
                }
            });

            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerHolder recyclerHolder, int i) {
            TVLiveListInfo.LiveInfoEntity entity = list.get(i);
            if (entity != null) {
                recyclerHolder.tvName.setText(entity.getCName());
                recyclerHolder.tvImg.setImageUrl(entity.getImg490_490(), VolleyHelper.getImageLoader());
                if (i == playPosition) {
                    recyclerHolder.tvShape.setImageResource(R.drawable.icon_live_shape);
                } else {
                    recyclerHolder.tvShape.setImageResource(R.drawable.shape_tv_no_play);
                }
                recyclerHolder.tvImg.setDefaultImageResId(R.drawable.icon_default_tv_live);
                recyclerHolder.tvImg.setErrorImageResId(R.drawable.icon_default_tv_live);
            }
            recyclerHolder.rootView.setTag(R.id.view_parent, i);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    static class RecyclerHolder extends RecyclerView.ViewHolder {

        private RecyclerHolder(View itemView) {
            super(itemView);
        }

        private View rootView;//
        private TextView tvName;//电视台名称
        private NetworkImageView tvImg;//电视台图标
        private ImageView tvShape;//电视台图标边框
    }


    /**
     * 获取电视台假数据
     */
    private List<TVLiveListInfo.LiveInfoEntity> getTVFakeData() {
        List<TVLiveListInfo.LiveInfoEntity> list = new ArrayList<>();
        TVLiveListInfo.LiveInfoEntity ziXun = new TVLiveListInfo.LiveInfoEntity();
        ziXun.setChannelId("4AC51C17-9FBE-47F2-8EE0-8285A66EAFF5");
        ziXun.setTitle("info");
        ziXun.setCName("资讯台");
        ziXun.setVideo("http://zv.3gv.ifeng.com/live/zixun.m3u8");
        ziXun.setAudio("http://zv.3gv.ifeng.com/live/zixun64kaudio.m3u8");
        ziXun.setImg490_490("http://y1.ifengimg.com/a/2016_16/94eb9bbac81aa7d.png");

        TVLiveListInfo.LiveInfoEntity zhongWen = new TVLiveListInfo.LiveInfoEntity();
        zhongWen.setChannelId("270DE943-3CDF-45E1-8445-9403F93E80C4");
        ziXun.setTitle("chinese");
        zhongWen.setCName("中文台");
        zhongWen.setVideo("http://zv.3gv.ifeng.com/live/zhongwen.m3u8");
        zhongWen.setAudio("http://zv.3gv.ifeng.com/live/zhongwen64kaudio.m3u8");
        zhongWen.setImg490_490("http://y2.ifengimg.com/a/2016_16/3c47ac7abc05e8b.png");

        TVLiveListInfo.LiveInfoEntity hongKong = new TVLiveListInfo.LiveInfoEntity();
        hongKong.setChannelId("2c942450-2165-4750-80de-7dff9c224153");
        ziXun.setTitle("hongkong");
        hongKong.setCName("香港台");
        hongKong.setVideo("http://zv.3gv.ifeng.com/live/hongkong.m3u8");
        hongKong.setAudio("http://zv.3gv.ifeng.com/live/hongkong64kaudio.m3u8");
        hongKong.setImg490_490("http://y0.ifengimg.com/a/2016_16/1711140b6b2491f.png");

        TVLiveListInfo.LiveInfoEntity tianJin = new TVLiveListInfo.LiveInfoEntity();
        tianJin.setChannelId("35383695-26c3-4ce5-b535-0001abce11e4");
        ziXun.setTitle("tjws");
        tianJin.setCName("天津卫视");
        tianJin.setVideo("http://zv.3gv.ifeng.com/live/CQWS.m3u8");
        tianJin.setAudio("http://zv.3gv.ifeng.com/live/166.m3u8");
        tianJin.setImg490_490("http://y2.ifengimg.com/a/2016_16/18d2325f353f669.png");

        list.add(ziXun);
        list.add(zhongWen);
        list.add(hongKong);
        list.add(tianJin);
        return list;
    }

    public void switchAudioVideoController(boolean shouldPlay) {
        if (mPlayerHelper != null) {
            if (shouldPlay) {
                mPlayerHelper.doVideoAudioChange();
            } else {
                mPlayerHelper.switchToVideoControllerAndUI();
            }
        }
    }

    public void handleNotificationFinishReceiver(boolean register) {

        if (register) {
            if (mNofiticationControlFinishReceiver == null) {
                mNofiticationControlFinishReceiver = new NofiticationControlFinishReceiver(FragmentLive.this);
            }
            IntentFilter filter = new IntentFilter();
            filter.addAction(AudioService.ACTION_NOTIFICATION_FINISH);
            filter.addAction(AudioService.ACTION_VIRTUAL_FINISH_LIVE);
            LocalBroadcastManager.getInstance(mActivity.getApplicationContext()).registerReceiver(mNofiticationControlFinishReceiver, filter);
        } else {
            LocalBroadcastManager.getInstance(mActivity.getApplicationContext()).unregisterReceiver(mNofiticationControlFinishReceiver);
            mNofiticationControlFinishReceiver = null;
        }
    }

    private static class NofiticationControlFinishReceiver extends BroadcastReceiver {
        WeakReference<FragmentLive> weakReference;

        public NofiticationControlFinishReceiver(FragmentLive fragmentLive) {
            weakReference = new WeakReference<>(fragmentLive);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            FragmentLive fragmentLive = weakReference.get();
            if (null == fragmentLive) {
                return;
            }
            if (null == intent) {
                return;
            }
            String action = intent.getAction();
            if (AudioService.ACTION_NOTIFICATION_FINISH.equals(action) || AudioService.ACTION_VIRTUAL_FINISH_LIVE.equals(action)) {
                logger.debug("onReceive:{}", action);
                fragmentLive.switchAudioVideoController(false);
            }
        }
    }


    /**
     * 获取当前电视台当天播放的节目列表
     *
     * @param entity 电视台
     */
    private void getProgramList(final TVLiveListInfo.LiveInfoEntity entity, final boolean changeToVideo) {
        LiveDao.getTVLiveProgramInfo(entity.getChannelId(), 1, TVLiveProgramInfo.class,
                new Response.Listener<TVLiveProgramInfo>() {
                    @Override
                    public void onResponse(TVLiveProgramInfo response) {
                        if (response == null || ListUtils.isEmpty(response.getSchedule())) {
                            initTVLiveData(entity, changeToVideo, "");
                        } else {
                            Date date = DateUtils.getDateFormat(System.currentTimeMillis());
                            int focusPosition = LiveUtils.handleTVLiveState(response, date);
                            TVLiveProgramInfo.ScheduleEntity scheduleEntity = response.getSchedule().get(focusPosition);
                            initTVLiveData(entity, changeToVideo, scheduleEntity.getProgramTitle());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        initTVLiveData(entity, changeToVideo, "");
                    }
                }, false);
    }
}
