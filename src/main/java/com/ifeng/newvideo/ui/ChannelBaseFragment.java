package com.ifeng.newvideo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.android.volley.NetworkError;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.ui.adapter.HeaderViewPagerAdapter;
import com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.List;


public abstract class ChannelBaseFragment extends BaseFragment implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener, MyPullToRefreshListView.OnLastItemVisibleListener {
    protected String mChannel_id;
    protected String mChannel_Type;
    protected MyPullToRefreshListView mPullToRefreshListView;
    protected Activity mActivity;
    protected HeaderViewPagerAdapter mHeaderViewPagerAdapter;
    protected boolean isLoading;//网络请求发起之前判断之前的网络请求是否已完成
    protected ChannelBaseAdapter mAdapter;


    public String getChannel_id() {
        return mChannel_id;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mChannel_id = bundle.getString(IntentKey.CHANNELID);
            mChannel_Type = bundle.getString(IntentKey.CHANNEL_TYPE);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPullToRefreshListView != null) {
            mPullToRefreshListView.setOnItemClickListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // 点击后的item置灰刷新
        // if (mAdapter != null) {
        //     mAdapter.notifyDataSetChanged();
        // }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object obj = parent.getAdapter().getItem(position);
        if (obj == null) {
            return;
        }
        if (obj instanceof ChannelBean.HomePageBean) {
            ChannelBean.HomePageBean bean = (ChannelBean.HomePageBean) obj;
            dispatchClickEvent(bean);
        }
    }


    public void dispatchClickEvent(ChannelBean.HomePageBean bean) {
        if (bean == null || TextUtils.isEmpty(bean.getMemberType())) {
            return;
        }
        String memberType = bean.getMemberType();
        if (CheckIfengType.isVideo(memberType)) {
            handleVideo(bean);
        } else if (CheckIfengType.isTopicType(memberType)) {
            handleTopic(bean);
        } else if (CheckIfengType.isLiveType(memberType)) {
            handleLive(bean);
        } else if (CheckIfengType.isAD(memberType)) {
            handleAD(bean);
        } else if (CheckIfengType.isVRLive(memberType)) {
            handleVRLive(bean);
        } else if (CheckIfengType.isVRVideo(memberType)) {
            handleVRVideo(bean);
        }

    }

    private void handleVRVideo(ChannelBean.HomePageBean bean) {
        List<String> videoList = new ArrayList<>();
        videoList.add(bean.getInfoId());
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        String chid = memberItem != null ? memberItem.getSearchPath() : "";
        IntentUtils.startVRVideoActivity(getActivity(), 0, mChannel_id, chid, 0, videoList, true);
    }

    private void handleVRLive(ChannelBean.HomePageBean bean) {
        String vrId = bean.getInfoId();
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        String vrUrl = memberItem != null ? memberItem.getLiveUrl() : "";
        String vrProgramTitle = bean.getTitle();
        String echid = mChannel_id;
        String shareUrl = memberItem != null ? memberItem.getmUrl() : "";
        String weMediaId = bean.getWeMedia() != null ? bean.getWeMedia().getId() : "";
        String weMediaName = bean.getWeMedia() != null ? bean.getWeMedia().getName() : "";
        String chid = memberItem != null ? memberItem.getSearchPath() : "";
        IntentUtils.startVRLiveActivity(getActivity(), vrId, vrUrl, vrProgramTitle, echid, chid, weMediaId, weMediaName, "", shareUrl);
    }

    private void handleAD(ChannelBean.HomePageBean bean) {
        String title = bean.getTitle();
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        String imageUrl = getImageUrl(bean, memberItem);
        String adType = "";
        String clickUrl = "";
        String appUrl = memberItem != null ? memberItem.getAppUrl() : "";
        String appScheme = memberItem != null ? memberItem.getAppScheme() : "";
        if (CheckIfengType.isAdnew(bean.getMemberType())) {
            //已安装新闻客户端：取schema数据，拉起新闻客户端; 未安装：使用appurl跳转到内部webview页面
            //schema 样例：comifengnewsclient://call?type=doc&aid=102722589
            if (PackageUtils.checkAppExist(mActivity, PackageUtils.NEWS_PACKAGE_NAME)) {
                IntentUtils.startIfengNewsApp(mActivity, appScheme, mChannel_id);
                return;
            } else {
                adType = bean.getMemberType();
                clickUrl = appUrl;
            }
        } else if (CheckIfengType.isAdapp(bean.getMemberType())) {
            adType = bean.getMemberType();
            clickUrl = appUrl;
        } else if (IfengType.TYPE_ADVERT.equalsIgnoreCase(bean.getMemberType())) {
            adType = bean.getMemberItem().getClickType();
            clickUrl = bean.getMemberItem().getClickUrl();
        } else {
            adType = memberItem != null ? memberItem.adAction.type : "";
            clickUrl = memberItem != null ? memberItem.adAction.url : "";
        }
        String shareUrl = memberItem != null ? memberItem.getmUrl() : "";
        String adId = "";
        if (bean instanceof ChannelBean.HomePageBean) {
            adId = bean.getInfoId();
        } else {
            adId = bean.getItemId();
        }
        if (CheckIfengType.isAdBackend(bean.getMemberType())) {
            adId = memberItem != null ? memberItem.adId : "";
        }

        List<String> list = new ArrayList<>();
        if (null != memberItem) {
            if (!ListUtils.isEmpty(memberItem.adAction.async_downloadCompletedurl)) {
                list.addAll(memberItem.adAction.async_downloadCompletedurl);
            }
            if (!ListUtils.isEmpty(memberItem.adAction.downloadCompletedurl)) {
                list.addAll(memberItem.adAction.downloadCompletedurl);
            }
        }
        AdvertExposureDao.sendAdvertClickReq(adId, memberItem != null ? memberItem.adAction.async_click : null);//广告点击曝光

        ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, shareUrl,
                appUrl, appScheme, getActivity(), list, mChannel_id,
                memberItem != null ? memberItem.getSearchPath() : "",
                memberItem != null ? memberItem.adAction.async_download : null);
    }

    private String getImageUrl(ChannelBean.HomePageBean bean, ChannelBean.MemberItemBean memberItem) {
        List<ChannelBean.HomePageBean.ImageBean> imageList = bean.getImageList();
        String imageUrl = "";
        if (!ListUtils.isEmpty(imageList)) {
            imageUrl = imageList.get(0).getImage();
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = memberItem.imageURL;
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = memberItem.getImage();
        }
        return imageUrl;
    }

    private void handleLive(ChannelBean.HomePageBean bean) {
        toFragmentLiveTab(bean);
        //String weMediaId = bean.getWeMedia() != null ? bean.getWeMedia().getId() : "";
        //String weMediaName = bean.getWeMedia() != null ? bean.getWeMedia().getName() : "";
        //IntentUtils.addHomePageShareData(this, bean);
        //IntentUtils.startActivityTVLive(mActivity, channelId, mChannel_id, chid, weMediaId, weMediaName, false);
    }

    private void handleTopic(ChannelBean.HomePageBean bean) {
        String topicId = bean.getTopicId();
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        if (TextUtils.isEmpty(topicId) && memberItem != null) {
            topicId = memberItem.getTopicId();
        }
        IntentUtils.addHomePageShareData(this, bean);
        IntentUtils.toTopicDetailActivity(mActivity, memberItem != null ? memberItem.getGuid() : "",
                topicId, mChannel_id, bean.getMemberType(), false, 0, "");
    }

    private void handleVideo(ChannelBean.HomePageBean bean) {
        IntentUtils.addHomePageShareData(this, bean);
        IntentUtils.toVodDetailActivity(mActivity, bean.getMemberItem() != null ? bean.getMemberItem().getGuid() : "",
                mChannel_id, false, false, 0, "");
    }

    /**
     * 切到直播Tab页
     */
    private void toFragmentLiveTab(ChannelBean.HomePageBean bean) {
        if (getActivity() instanceof ActivityMainTab) {
            String shareTitle = bean.getTitle();
            String shareImg = "";
            List<HomePageBeanBase.ImageBean> imageList = bean.getImageList();
            if (!ListUtils.isEmpty(imageList)) {
                shareImg = imageList.get(0).getImage();
            } else if (!TextUtils.isEmpty(bean.getImage())) {
                shareImg = bean.getImage();
            }
            if (CheckIfengType.isAD(bean.getMemberType())) {
                shareImg = bean.getImage();
            }
            String channelId = bean.getMemberItem() != null ? bean.getMemberItem().getGuid() : "";
            String chid = bean.getMemberItem() != null ? bean.getMemberItem().getSearchPath() : "";
            ((ActivityMainTab) getActivity()).setCurrentTabToLive(channelId, mChannel_id, chid, shareTitle, shareImg, true);
        }
    }

    protected List<ChannelBean.HomePageBean> filterList(List<ChannelBean.HomePageBean> beanList,
                                                        List<ChannelBean.HomePageBean> existList, Status refresh, boolean isFocus) {
        if (!ListUtils.isEmpty(beanList)) {
            String time = null;
            if (Status.REFRESH == refresh) {
                time = DateUtils.getFormatTime(LiveUtils.getCurrentTime() + "");
            }
            List<ChannelBean.HomePageBean> returnList = new ArrayList<>();
            for (ChannelBean.HomePageBean homePageBean : beanList) {
                ChannelBean.HomePageBean newBean = filterHomePageBean(homePageBean, isFocus);

                if (newBean != null && !returnList.contains(newBean) && (ListUtils.isEmpty(existList) || !existList.contains(newBean))) {
                    if (Status.REFRESH == refresh && !TextUtils.isEmpty(time)) {
                        newBean.setUpdateDate(time);
                    }
                    returnList.add(newBean);
                } else {
                    //广告空曝光
                    if (!TextUtils.isEmpty(homePageBean.getMemberType()) && CheckIfengType.isAD(homePageBean.getMemberType())) {
                        returnList.add(homePageBean);
                    }
                }
            }
            return returnList;
        }
        return null;
    }

/*    protected static List<ChannelBean.HomePageBean> filterList(List<ChannelBean.HomePageBean> beanList, List<ChannelBean.HomePageBean> existList, boolean isFocus) {
        if (!ListUtils.isEmpty(beanList)) {
            List<ChannelBean.HomePageBean> returnList = new ArrayList<>();
            for (ChannelBean.HomePageBean homePageBean : beanList) {
                ChannelBean.HomePageBean bean = filterHomePageBean(homePageBean, isFocus);
                if (bean != null && !returnList.contains(homePageBean) && (ListUtils.isEmpty(existList) || !existList.contains(bean))) {
                    returnList.add(bean);
                }
            }
            return returnList;
        }
        return null;
    }*/

    private static ChannelBean.HomePageBean filterHomePageBean(ChannelBean.HomePageBean bean, boolean isFocus) {
        if (bean == null) {
            return null;
        }
        String type = bean.getMemberType();
        String title = bean.getTitle();
        boolean invalid = isFocus && TextUtils.isEmpty(bean.getImage())
                && (ListUtils.isEmpty(bean.getImageList()) || TextUtils.isEmpty(bean.getImageList().get(0).getImage()));

        if (TextUtils.isEmpty(type) || TextUtils.isEmpty(title) || invalid) {
            return null;
        } else if (CheckIfengType.isTopicType(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            String topicId = bean.getTopicId();
            if (TextUtils.isEmpty(topicId) && memberItem != null) {
                topicId = memberItem.getTopicId();
            }
            if (TextUtils.isEmpty(topicId)) {
                return null;
            }
        } else if (CheckIfengType.isLiveType(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getGuid())) {
                return null;
            }
        } else if (CheckIfengType.isVideo(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getGuid())) {
                return null;
            }
        } else if (CheckIfengType.isVideoGroup(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || ListUtils.isEmpty(memberItem.getVideos())) {
                return null;
            }
        } else if (CheckIfengType.isVRLive(type)) {
            //add VRLive
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getLiveUrl())) {
                return null;
            }
        } else if (CheckIfengType.isAdnew(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getAppScheme())) {
                return null;
            }
        } else if (CheckIfengType.isAdapp(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getAppUrl())) {
                return null;
            }
        }
        return bean;
    }


    @Override
    public void onLastItemVisible() {
        if (isLoading || ListUtils.isEmpty(mAdapter.getDataList())) {
            return;
        }
        if (mPullToRefreshListView != null) {
            final ListView listView = mPullToRefreshListView.getRefreshableView();
            int visibleCount = listView.getLastVisiblePosition() - listView.getFirstVisiblePosition();
            if (mAdapter.getDataList().size() > visibleCount) {
                mPullToRefreshListView.showFootView();
            }
        }
    }

    protected void handlleNetUseless(Status status) {
        ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        if (status == Status.LOAD_MORE && mPullToRefreshListView != null) {
            mPullToRefreshListView.hideFootView();
        } else if (Status.REFRESH == status && mPullToRefreshListView != null) {
            mPullToRefreshListView.onRefreshComplete();
        } else if (status == Status.FIRST) {
            updateViewStatus(Status.REQUEST_NET_FAIL);
        }
    }

    protected void handleRequestError(Status status, VolleyError error) {
        if (status == Status.REFRESH && mPullToRefreshListView != null) {
            mPullToRefreshListView.onRefreshComplete();
        } else if (status == Status.LOAD_MORE && mPullToRefreshListView != null) {
            mPullToRefreshListView.hideFootView();
            mPullToRefreshListView.onRefreshComplete();
        } else {
            if (error != null && error instanceof NetworkError) {
                updateViewStatus(Status.REQUEST_NET_FAIL);
            } else {
                updateViewStatus(Status.DATA_ERROR);
            }
        }
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        if (mAdapter != null) {
            mAdapter.setRefreshTimes(true);
        }
    }

}
