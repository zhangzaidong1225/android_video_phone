package com.ifeng.newvideo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.adapter.ListAdapter4VRChannel;
import com.ifeng.newvideo.ui.live.vr.VRBaseActivity;
import com.ifeng.newvideo.ui.live.vr.VRVideoActivity;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.LastDocUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * VR频道页
 */
public class VRChannelFragment extends ChannelBaseFragment implements NotifyShareCallback {
    private static final Logger logger = LoggerFactory.getLogger(VRChannelFragment.class);

    private UIStatusLayout uiStatusLayout;
    private PullToRefreshListView listView;
    private ListAdapter4VRChannel adapter;

    private String positionId = "";
    private int currentPage = 1;
    private List<ChannelBean.HomePageBean> list = new ArrayList<>();

    private OneKeyShare mOneKeyShare;
    private int mUpTimes;


    public static VRChannelFragment newInstance() {
        return new VRChannelFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vr_video, container, false);
        initView(view);
        initAdapter();
        mFocusList = new ArrayList<>(20);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mOneKeyShare = new OneKeyShare(getActivity());
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        currentPage = 1;
        positionId = "";
        getVRDataList(positionId, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && listView != null) {
            final ListView listView = this.listView.getRefreshableView();
            final int headCount = listView.getHeaderViewsCount();
            final int position = data.getIntExtra(VRBaseActivity.CURRENT_POSITION_IN_VIDEO_LIST, 0);
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listView.requestFocusFromTouch();
                    listView.setSelection(position + headCount);
                }
            }, 200);
        }
    }

    private void initView(View view) {
        uiStatusLayout = (UIStatusLayout) view.findViewById(R.id.ui_status_layout);
        uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.BOTH);
        uiStatusLayout.setRequest(new RequestData() {
            @Override
            public void requestData() {
                uiStatusLayout.setStatus(UIStatusLayout.LOADING);
                currentPage = 1;
                positionId = "";
                getVRDataList(positionId, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
            }
        });
        listView = uiStatusLayout.getListView();
        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        listView.setShowIndicator(false);
        listView.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_NEVER);
        listView.getRefreshableView().setFooterDividersEnabled(true);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                getVRDataList(positionId, ChannelDao.REFRESH, ChannelConstants.DOWN, String.valueOf(++mUpTimes));
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getVRDataList(positionId, ChannelDao.LOAD_MORE, ChannelConstants.UP, "");
            }
        });
    }

    private void initAdapter() {
        adapter = new ListAdapter4VRChannel(mChannel_id);
        listView.getRefreshableView().setAdapter(adapter);
        adapter.setOnVideoItemClick(new ListAdapter4VRChannel.OnVideoItemClick() {
            @Override
            public void onShareClick(View view, ChannelBean.HomePageBean bean) {
                OneKeyShareContainer.oneKeyShare = mOneKeyShare;
                String weMediaId = bean.getWeMedia() != null ? bean.getWeMedia().getId() : "";
                String chid = bean.getMemberItem().getSearchPath();
                mOneKeyShare.initShareStatisticsData(bean.getMemberItem().getGuid(), mChannel_id, chid, weMediaId, PageIdConstants.PAGE_HOME);
                mOneKeyShare.initSmartShareData(bean.getShowType(), bean.getWeMedia().getName(), bean.getTitle());
                String imgUrl = !ListUtils.isEmpty(bean.getImageList()) ? bean.getImageList().get(0).getImage() : "";
                mOneKeyShare.shareVRVodWithPopWindow(true, bean.getTitle(), imgUrl,
                        bean.getMemberItem().getmUrl(), view, VRChannelFragment.this, false);
            }

            @Override
            public void onWeMediaClick(ChannelBean.HomePageBean bean, int position) {
                ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
                if (weMedia != null) {
                    Intent intent = new Intent(getActivity(), WeMediaHomePageActivity.class);
                    intent.putExtra(IntentKey.WE_MEIDA_ID, weMedia.getId());
                    getActivity().startActivity(intent);
                }

                String simId = bean.getMemberItem() == null ? "" : bean.getMemberItem().getSimId();
                String rToken = bean.getMemberItem() == null ? "" : bean.getMemberItem().getrToken();
                PageActionTracker.clickHomeItem(String.valueOf(position), simId, rToken);
            }

            @Override
            public void onPlayVideoClick(int position) {
                List<String> videoList = new ArrayList<>();
                for (ChannelBean.HomePageBean model : list) {
                    videoList.add(model.getMemberItem().getGuid());
                }
                startToVRVideoActivity(position, (ArrayList<String>) videoList);

                String simId = list.get(position).getMemberItem() == null ? ""
                        : list.get(position).getMemberItem().getSimId();
                String rToken = list.get(position).getMemberItem() == null ? ""
                        : list.get(position).getMemberItem().getrToken();
                PageActionTracker.clickHomeItem(String.valueOf(position), simId, rToken);
            }
        });
    }

    private void getVRDataList(final String position_id, final int action, final String operation, String upTimes) {
        logger.debug("requestNet");
        final boolean net = NetUtils.isNetAvailable(getActivity().getApplicationContext());
        if (!net && !ListUtils.isEmpty(list)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (isLoading) {
            listView.onRefreshComplete();
            return;
        }
        isLoading = true;
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String nw = NetUtils.getNetType(getActivity());
        ChannelDao.requestChannelData(
                mChannel_id,
                mChannel_Type,
                DataInterface.PAGESIZE_20,
                position_id,
                0,
                ChannelBean.class,
                true, null, action, SharePreUtils.getInstance().getInreview(), operation, new User(IfengApplication.getAppContext()).getUid(),
                PhoneConfig.userKey, LastDocUtils.getLastDoc(), upTimes,
                province, city, nw, PhoneConfig.publishid,
                new Response.Listener<ChannelBean>() {
                    @Override
                    public void onResponse(ChannelBean response) {

                        if (response != null && !ListUtils.isEmpty(response.getBodyList())) {
                            if (currentPage == 1) {
                                list.clear();
                            }
                            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
                            List<ChannelBean.HomePageBean> tempList = response.getBodyList();
                            positionId = tempList.get(tempList.size() - 1).getItemId();
                            list.addAll(response.getBodyList());
                            adapter.setDataList(list);
                            if (currentPage == 1) {
                                listView.getRefreshableView().setSelection(0);
                            }
                            currentPage++;
                        } else {
                            if (currentPage == 1 && ListUtils.isEmpty(list)) {
                                uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                            } else {
                                if (currentPage > 1) {
                                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                                }
                            }
                        }

                        listView.onRefreshComplete();
                        isLoading = false;

                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        boolean isNetError = error != null && error instanceof NetworkError;
                        if (currentPage == 1 && ListUtils.isEmpty(list)) {
                            uiStatusLayout.setStatus(isNetError ? UIStatusLayout.NO_NET : UIStatusLayout.ERROR);
                        } else {
                            ToastUtils.getInstance().showShortToast(isNetError ? R.string.toast_no_net : R.string.toast_no_more);
                        }

                        listView.onRefreshComplete();
                        isLoading = false;

                    }
                });

        PageActionTracker.pullCh(action == ChannelDao.LOAD_MORE, mChannel_id);

    }

    /**
     * 得到根Fragment
     */
    private Fragment getRootFragment() {
        Fragment fragment = getParentFragment();
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment;
    }

    private void startToVRVideoActivity(int position, ArrayList<String> videoList) {
        Intent intent = new Intent(getActivity(), VRVideoActivity.class);
        intent.putExtra(IntentKey.VR_CURRENT_POSITION_IN_VIDEO_LIST, position);
        intent.putExtra(IntentKey.VR_LIVE_ECHID, mChannel_id);
        intent.putExtra(IntentKey.VR_LIVE_CHID, "");
        intent.putExtra(IntentKey.VR_SEEKTO_POSITION, 0);
        intent.putExtra(IntentKey.VR_VIDEO_IS_FROM_HISTORY, false);
        intent.putStringArrayListExtra(IntentKey.VR_VIDEO_LIST, videoList);
        getRootFragment().startActivityForResult(intent, 100);
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        super.onRefresh(refreshView);
        mUpTimes = 0;
        getVRDataList(positionId, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
    }

    @Override
    protected void requestNet() {

    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageHomeCh(mChannel_id);
        } else {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFocusList = CommonStatictisListUtils.vRFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(CommonStatictisListUtils.vRFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.vRFocusList.clear();
    }
}
