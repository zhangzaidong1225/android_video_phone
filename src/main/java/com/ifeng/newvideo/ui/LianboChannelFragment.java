package com.ifeng.newvideo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.ui.adapter.LianBoHeaderViewPagerAdapter;
import com.ifeng.newvideo.ui.adapter.ListAdapter4Lianbo;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.LastDocUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.widget.LianBoHeadFlowView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.ChannelBeanSpecial;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.List;

public class LianboChannelFragment extends BaseFragment implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener, MyPullToRefreshListView.OnLastItemVisibleListener {

    private String mChannel_id;
    private String mChannel_Type;
    private MyPullToRefreshListView mPullToRefreshListView;
    private Activity mActivity;
    private boolean isLoading;//网络请求发起之前判断之前的网络请求是否已完成
    private ListAdapter4Lianbo mAdapter;


    //note:联播台头图做成动态展示,只有一张焦点图的限制放在后台
    //FIXME:因为焦点图做成动态,其点击跳转事件也跟随之前逻辑放在HeaderViewPagerAdapterForHomeMain中且有标记isTopic
    private LianBoHeadFlowView mHeadFlowView;
    private FrameLayout mHeaderWrap;
    private LianBoHeaderViewPagerAdapter mHeaderViewPagerAdapter;
    private int mUpTimes;
//    private String requireTime=""; 联播太不需要此字段

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        logger.debug("page onAttach {}", mChannel_id);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mChannel_id = bundle.getString(IntentKey.CHANNELID);
            mChannel_Type = bundle.getString(IntentKey.CHANNEL_TYPE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void requestNet() {
        updateViewStatus(Status.LOADING);
        mUpTimes = 0;
        requestNet("", DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
    }

    private void requestNet(String position_id_or_requre_time, String count, final Status status, int action, String operation, String upTimes) {

        final boolean net = NetUtils.isNetAvailable(getActivity().getApplicationContext());
        if (!net) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
        if (isLoading) {
            return;
        }
        isLoading = true;
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String nw = NetUtils.getNetType(getActivity());
        ChannelDao.requestChannelData(mChannel_id, mChannel_Type, count, position_id_or_requre_time, 0,
                ChannelBeanSpecial.class, true, null, action, SharePreUtils.getInstance().getInreview(), operation,
                new User(IfengApplication.getAppContext()).getUid(), PhoneConfig.userKey,
                LastDocUtils.getLastDoc(), upTimes,
                province, city, nw, PhoneConfig.publishid,
                new Response.Listener<ChannelBeanSpecial>() {
                    @Override
                    public void onResponse(ChannelBeanSpecial response) {
                        isLoading = false;
                        mPullToRefreshListView.onRefreshComplete();
                        mPullToRefreshListView.hideFootView();
                        if (response != null && (!ListUtils.isEmpty(response.getBodyList()) || !ListUtils.isEmpty(response.getHeader()))) {
                            if (Status.FIRST == status) {
                                List<HomePageBeanBase> headers_new = filterInvalidData(response.getHeader(), null, true);
                                List<HomePageBeanBase> body_new = filterInvalidData(response.getBodyList(), null, false);
                                updateViewStatus(Status.REQUEST_NET_SUCCESS);
                                if (!ListUtils.isEmpty(body_new)) {
                                    mAdapter.setData(body_new);
                                    mAdapter.notifyDataSetChanged();
                                }
                                if (!ListUtils.isEmpty(headers_new)) {
                                    mHeadFlowView.setVisibility(View.VISIBLE);
                                    mHeaderViewPagerAdapter.setData(headers_new);
                                    mHeaderViewPagerAdapter.notifyDataSetChanged();
                                    if (mHeaderViewPagerAdapter.getCount() > 1) {
                                        mHeadFlowView.setCurrentItem(20 * headers_new.size());
                                    }
                                } else {
                                    mHeadFlowView.setVisibility(View.GONE);
                                }

                            } else if (Status.REFRESH == status) {
                                mPullToRefreshListView.onRefreshComplete();
                                List<HomePageBeanBase> headers_new = filterInvalidData(response.getHeader(), null, true);
                                List<HomePageBeanBase> body_new = filterInvalidData(response.getBodyList(), null, false);
                                if (!ListUtils.isEmpty(headers_new)) {
                                    mHeaderViewPagerAdapter.setData(headers_new);
                                }
                                mHeaderViewPagerAdapter.notifyDataSetChanged();
                                if (!ListUtils.isEmpty(body_new)) {
                                    mAdapter.setData(body_new);
                                    mAdapter.notifyDataSetChanged();
                                }
                            } else {
                                mPullToRefreshListView.hideFootView();
                                List<HomePageBeanBase> body_new = filterInvalidData(response.getBodyList(), mAdapter == null ? null : mAdapter.getDataList(), false);
                                if (ListUtils.isEmpty(body_new)) {
                                    if (net) {
                                        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                                    }
                                } else {
                                    if (mAdapter != null) {
                                        mAdapter.addData(body_new, false);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } else {
                            if (net && Status.LOAD_MORE == status) {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                            } else if (Status.FIRST == status) {
                                updateViewStatus(Status.DATA_ERROR);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("volley onErrorResponse ! {}", error);
                        isLoading = false;
                        handleRequestError(status, error);
                        if (net && status == Status.LOAD_MORE) {
                            ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                        }
                    }
                });

        PageActionTracker.pullCh(status == Status.LOAD_MORE, mChannel_id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAdapter = new ListAdapter4Lianbo(mActivity, mChannel_id);
        mHeaderWrap = new FrameLayout(mActivity);
        LayoutInflater.from(mActivity).inflate(R.layout.lianbo_headview, mHeaderWrap);
        mHeadFlowView = (LianBoHeadFlowView) mHeaderWrap.findViewById(R.id.lianbo_headFlowView);
        mHeaderViewPagerAdapter = new LianBoHeaderViewPagerAdapter(this, mActivity, mChannel_id);
        mHeadFlowView.setViewPagerAdapter(mHeaderViewPagerAdapter);
        initView(inflater);
        if (mPullToRefreshListView != null) {
            mPullToRefreshListView.setAdapter(mAdapter);
            mPullToRefreshListView.setOnItemClickListener(this);
            mPullToRefreshListView.setOnLastItemVisibleListener(this);
            mPullToRefreshListView.getRefreshableView().addHeaderView(mHeaderWrap);
        }
        return mPullToRefreshListView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestNet();
        mFocusList = new ArrayList<>(20);
    }


    private void initView(LayoutInflater inflater) {
        mPullToRefreshListView = (MyPullToRefreshListView) inflater.inflate(R.layout.phone_childfragment_video, null);
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mPullToRefreshListView.setShowIndicator(false);
        mPullToRefreshListView.setOnLastItemVisibleListener(this);
        mPullToRefreshListView.setOnRefreshListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object obj = parent.getAdapter().getItem(position);
        if (obj == null) {
            return;
        }
        if (obj instanceof HomePageBeanBase) {
            HomePageBeanBase bean = (HomePageBeanBase) obj;
            bean.setWatched(true);
            dispatchClickEvent(bean);
//            CommonStatictisListUtils.getInstance().sendHomeFeedYoukuConstatic(bean, CommonStatictisListUtils.YK_NEXT);

            String simId = bean.getMemberItem() == null ? "" : bean.getMemberItem().getSimId();
            String rToken = bean.getMemberItem() == null ? "" : bean.getMemberItem().getrToken();
            PageActionTracker.clickHomeItem(String.valueOf(position), simId, rToken);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFocusList = CommonStatictisListUtils.lianboFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(CommonStatictisListUtils.lianboFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.lianboFocusList.clear();
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        mAdapter.setRefreshTimes(true);
        mHeaderViewPagerAdapter.increaseRefreshTimes();
        requestNet("", DataInterface.PAGESIZE_20, Status.REFRESH, ChannelDao.REFRESH, ChannelConstants.UP, String.valueOf(++mUpTimes));
    }

    @Override
    public void onLastItemVisible() {
        if (mAdapter != null) {
            HomePageBeanBase homePageBean = mAdapter.getLastItem();
            if (homePageBean != null && !TextUtils.isEmpty(homePageBean.getItemId())) {
                requestNet(homePageBean.getItemId(), DataInterface.PAGESIZE_20, Status.LOAD_MORE, ChannelDao.LOAD_MORE, ChannelConstants.DOWN, "");
            }
        }

    }

    private List<HomePageBeanBase> filterInvalidData(List<? extends HomePageBeanBase> beanList, List<? extends HomePageBeanBase> existList, boolean isFocus) {
        if (!ListUtils.isEmpty(beanList)) {
            List<HomePageBeanBase> returnList = new ArrayList<>();
            for (HomePageBeanBase homePageBean : beanList) {
                HomePageBeanBase bean = filterHomePageBean(homePageBean, isFocus);
                if (bean != null && !returnList.contains(homePageBean) && (ListUtils.isEmpty(existList) || !existList.contains(bean))) {
                    returnList.add(bean);
                }
            }
            return returnList;
        }
        return null;
    }

    private HomePageBeanBase filterHomePageBean(HomePageBeanBase bean, boolean isFocus) {
        if (bean == null) {
            return null;
        }
        String type = bean.getMemberType();
        String title = bean.getTitle();
        boolean invalid = isFocus && TextUtils.isEmpty(bean.getImage())
                && (ListUtils.isEmpty(bean.getImageList()) || TextUtils.isEmpty(bean.getImageList().get(0).getImage()));
        if (TextUtils.isEmpty(type) || TextUtils.isEmpty(title) || invalid) {
            return null;
        } else if (CheckIfengType.isTopicType(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            String topicId = bean.getTopicId();
            if (TextUtils.isEmpty(topicId) && memberItem != null) {
                topicId = memberItem.getTopicId();
            }
            if (TextUtils.isEmpty(topicId)) {
                return null;
            }
        } else if (CheckIfengType.isLiveType(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getGuid())) {
                return null;
            }
        } else if (CheckIfengType.isVideo(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getGuid())) {
                return null;
            }
        } else if (CheckIfengType.isVRLive(type)) {
            //add VRLive
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getLiveUrl())) {
                return null;
            }
        } else if (CheckIfengType.isAdnew(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getAppScheme())) {
                return null;
            }
        } else if (CheckIfengType.isAdapp(type)) {
            ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
            if (memberItem == null || TextUtils.isEmpty(memberItem.getAppUrl())) {
                return null;
            }
        }
        return bean;
    }


    private void handleRequestError(Status status, VolleyError error) {
        if (status == Status.REFRESH && mPullToRefreshListView != null) {
            mPullToRefreshListView.onRefreshComplete();
        } else if (status == Status.LOAD_MORE && mPullToRefreshListView != null) {
            mPullToRefreshListView.hideFootView();
            mPullToRefreshListView.onRefreshComplete();
        } else {
            if (error != null && error instanceof NetworkError) {
                updateViewStatus(Status.REQUEST_NET_FAIL);
            } else {
                updateViewStatus(Status.DATA_ERROR);
            }
        }
    }

    private void dispatchClickEvent(HomePageBeanBase bean) {
        if (bean == null || TextUtils.isEmpty(bean.getMemberType())) {
            return;
        }
        String memberType = bean.getMemberType();
        if (memberType == null) {
            return;
        }
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        if (CheckIfengType.isVideo(memberType)) {
            IntentUtils.addHomePageShareData(this, bean);
            IntentUtils.toVodDetailActivity(mActivity, memberItem != null ? memberItem.getGuid() : "",
                    mChannel_id, false, false, 0, "");
        } else if (CheckIfengType.isTopicType(memberType)) {
            String topicId = bean.getTopicId();
            if (TextUtils.isEmpty(topicId) && memberItem != null) {
                topicId = memberItem.getTopicId();
            }
            IntentUtils.addHomePageShareData(this, bean);
            IntentUtils.toTopicDetailActivity(mActivity, memberItem != null ? memberItem.getGuid() : "",
                    topicId, mChannel_id, memberType, false, 0, "");
        } else if (CheckIfengType.isLiveType(memberType)) {
            toFragmentLiveTab(bean);
            //String weMediaId = bean.getWeMedia() != null ? bean.getWeMedia().getId() : "";
            //String weMediaName = bean.getWeMedia() != null ? bean.getWeMedia().getName() : "";
            //IntentUtils.addHomePageShareData(this,bean);
            //IntentUtils.startActivityTVLive(mActivity, channelId, mChannel_id, chid, weMediaId, weMediaName, false);
        } else if (CheckIfengType.isAD(memberType)) {
            String title = bean.getTitle();
            List<HomePageBeanBase.ImageBean> imageList = bean.getImageList();
            String imageUrl = "";
            if (!ListUtils.isEmpty(imageList)) {
                imageUrl = imageList.get(0).getImage();
            }
            String adType = "";
            String clickUrl = "";
            String appUrl = memberItem != null ? memberItem.getAppUrl() : "";
            String appScheme = memberItem != null ? memberItem.getAppScheme() : "";
            if (CheckIfengType.isAdnew(memberType)) {
                //已安装新闻客户端：取schema数据，拉起新闻客户端; 未安装：使用appurl跳转到内部webview页面
                //schema 样例：comifengnewsclient://call?type=doc&aid=102722589
                if (PackageUtils.checkAppExist(mActivity, PackageUtils.NEWS_PACKAGE_NAME)) {
                    IntentUtils.startIfengNewsApp(mActivity, appScheme, mChannel_id);
                    return;
                } else {
                    adType = memberType;
                    clickUrl = appUrl;
                }
            } else if (CheckIfengType.isAdapp(memberType)) {
                adType = memberType;
                clickUrl = appUrl;
            } else {
                adType = memberItem != null ? memberItem.adAction.type : "";
                clickUrl = memberItem != null ? memberItem.adAction.url : "";
            }
            String shareUrl = memberItem != null ? memberItem.getmUrl() : "";
            String adId = bean.getItemId();
            List<String> list = new ArrayList<>();
            if (null != memberItem) {
                if (!ListUtils.isEmpty(memberItem.adAction.async_downloadCompletedurl)) {
                    list.addAll(memberItem.adAction.async_downloadCompletedurl);
                }
                if (!ListUtils.isEmpty(memberItem.adAction.downloadCompletedurl)) {
                    list.addAll(memberItem.adAction.downloadCompletedurl);
                }
            }
            AdvertExposureDao.sendAdvertClickReq(adId, memberItem != null ? memberItem.adAction.async_click : null);//广告点击曝光

            ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, shareUrl, appUrl, appScheme, getActivity(),
                    list, mChannel_id, memberItem != null ? memberItem.getSearchPath() : "",
                    memberItem != null ? memberItem.adAction.async_download : null);
        } else if (CheckIfengType.isVRLive(memberType)) {
            String vrId = ((ChannelBean.HomePageBean) bean).getInfoId();
            String vrUrl = memberItem != null ? memberItem.getLiveUrl() : "";
            String vrProgramTitle = bean.getTitle();
            String echid = mChannel_id;
            String shareUrl = memberItem != null ? memberItem.getmUrl() : "";
            String weMediaId = bean.getWeMedia() != null ? bean.getWeMedia().getId() : "";
            String weMediaName = bean.getWeMedia() != null ? bean.getWeMedia().getName() : "";
            String chid = memberItem != null ? memberItem.getSearchPath() : "";
            IntentUtils.startVRLiveActivity(getActivity(), vrId, vrUrl, vrProgramTitle, echid, chid, weMediaId, weMediaName, "", shareUrl);
        } else if (CheckIfengType.isVRVideo(memberType)) {
            List<String> videoList = new ArrayList<>();
            videoList.add(((ChannelBean.HomePageBean) bean).getInfoId());
            String chid = memberItem != null ? memberItem.getSearchPath() : "";
            IntentUtils.startVRVideoActivity(getActivity(), 0, mChannel_id, chid, 0, videoList, true);
        }

    }

    private void toFragmentLiveTab(HomePageBeanBase bean) {
        if (getActivity() instanceof ActivityMainTab) {
            String shareTitle = bean.getTitle();
            String shareImg = "";
            List<HomePageBeanBase.ImageBean> imageList = bean.getImageList();
            if (!ListUtils.isEmpty(imageList)) {
                shareImg = imageList.get(0).getImage();
            } else if (!TextUtils.isEmpty(bean.getImage())) {
                shareImg = bean.getImage();
            }

            if (CheckIfengType.isAD(bean.getMemberType())) {
                shareImg = bean.getImage();
            }
            String channelId = bean.getMemberItem() != null ? bean.getMemberItem().getGuid() : "";
            String chid = bean.getMemberItem() != null ? bean.getMemberItem().getSearchPath() : "";
            ((ActivityMainTab) getActivity()).setCurrentTabToLive(channelId, mChannel_id, chid, shareTitle, shareImg, true);
        }
    }
}
