package com.ifeng.newvideo.ui.ad;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.widget.BaseAdapter;
import com.alibaba.fastjson.JSONArray;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.dao.*;
import com.ifeng.video.dao.db.model.ADInfoModel;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.db.model.VideoAdConfigModel;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guolc on 2016/8/20.
 */
public class AdTools {

    public static final String videoType = "newvideo";


    public static String getAdMoreRequestUrl(String positon_id, Context context) {
        if (VideoAdConfigDao.adConfig == null || VideoAdConfigDao.adConfig.getInfoAD() == null || TextUtils.isEmpty(positon_id)) {
            return null;
        }
        VideoAdConfigModel.InfoADBean infoAD = VideoAdConfigDao.adConfig.getInfoAD();
        String adHost = infoAD.getAdMoreUrl();
        StringBuilder params = new StringBuilder();
        params.append(adHost);
        params.append("?adids=");
        params.append(positon_id + ",");
        appendParams(params, context);
        return params.toString();
    }


    public static String getRefreshRequestAdUrl(List<MainAdInfoModel> focus, List<MainAdInfoModel> body, Context context) {
        if (VideoAdConfigDao.adConfig == null || VideoAdConfigDao.adConfig.getInfoAD() == null) {
            return null;
        }
        if (ListUtils.isEmpty(focus) && ListUtils.isEmpty(body)) {
            return null;
        }
        long current_time = System.currentTimeMillis();
        VideoAdConfigModel.InfoADBean infoAD = VideoAdConfigDao.adConfig.getInfoAD();
        String adHost = infoAD.getAdHost();
        StringBuilder params = new StringBuilder();
        params.append(adHost);
        params.append("?adids=");
        boolean expired = false;

        StringBuilder adIdsBuilder = new StringBuilder();
        if (!ListUtils.isEmpty(focus)) {
            for (MainAdInfoModel mainAdInfoModel : focus) {
                if (current_time - mainAdInfoModel.getCreate_time() > mainAdInfoModel.getCacheTime() * 1000) {//过期时间是以秒为单位
                    adIdsBuilder.append(mainAdInfoModel.getAdPositionId()).append(',');
                    expired = true;
                }
            }
        }
        if (!ListUtils.isEmpty(body)) {
            for (MainAdInfoModel mainAdInfoModel : body) {
                if (current_time - mainAdInfoModel.getCreate_time() > mainAdInfoModel.getCacheTime() * 1000) {
                    adIdsBuilder.append(mainAdInfoModel.getAdPositionId()).append(',');
                    expired = true;
                }
            }
        }
        String adIds = adIdsBuilder.toString();
        // 如果adIds为空，或者只有一个','  数据出错
        if (TextUtils.isEmpty(adIds) || adIds.equals(',')) {
            return "";
        }
        if (!expired) {
            return "";
        }
        params.append(adIds);
        appendParams(params, context);
        return params.toString();
    }


    public static void appendParams(StringBuilder params, Context context) {
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String country = sharePreUtils != null ? sharePreUtils.getCountry() : "";
        String province = sharePreUtils != null ? sharePreUtils.getProvince() : "";
        String city = sharePreUtils != null ? sharePreUtils.getCity() : "";
        String lat = sharePreUtils != null ? sharePreUtils.getLatitude() : "";
        String lon = sharePreUtils != null ? sharePreUtils.getLongitude() : "";
        String screen;

        params.append("&uid=").append(PhoneConfig.UID).
                append("&gv=").append(PhoneConfig.softversion).
                append("&proid=").append(ClientInfoDAO.APP_NAME).
                append("&os=android_").append(Build.VERSION.SDK_INT).
                append("&network=").append(NetUtils.getNetType(context));
        try {
            screen = URLEncoderUtils.encodeInUTF8(PhoneConfig.getScreenForAd());
            country = URLEncoderUtils.encodeInUTF8(country);
            province = URLEncoderUtils.encodeInUTF8(province);
            city = URLEncoderUtils.encodeInUTF8(city);
        } catch (UnsupportedEncodingException e) {
            screen = "";
            country = "";
            province = "";
            city = "";
        }
        params.append("&screen=").append(screen).
                append("&country=").append(country).
                append("&province=").append(province).
                append("&city=").append(city).
                append("&lat=").append(lat).
                append("&lon=").append(lon).
                append("&t=").append(System.currentTimeMillis());
    }

    @NonNull
    public static String getRequestADUrl(String channel_id, boolean hasFocusView, Context context) {
        if (VideoAdConfigDao.adConfig == null || VideoAdConfigDao.adConfig.getInfoAD() == null) {
            return "";
        }

        VideoAdConfigModel.InfoADBean infoAD = VideoAdConfigDao.adConfig.getInfoAD();
        String adHost = infoAD.getAdHost();
        List<VideoAdConfigModel.InfoADBean.TextbarBean> textbar = infoAD.getTextbar();
        List<VideoAdConfigModel.InfoADBean.FocusBean> focusBeen = infoAD.getFocus();
        if (ListUtils.isEmpty(textbar) && ListUtils.isEmpty(focusBeen)) {
            return "";
        }
        StringBuilder params = new StringBuilder();
        params.append(adHost);
        params.append("?adids=");

        StringBuilder adIdsBuilder = new StringBuilder();
        for (VideoAdConfigModel.InfoADBean.TextbarBean textbarBean : textbar) {
            if (channel_id.equals(textbarBean.getChannelId())) {
                adIdsBuilder.append(textbarBean.getAdId()).append(',');
            }
        }
        if (hasFocusView) {
            for (VideoAdConfigModel.InfoADBean.FocusBean focus : focusBeen) {
                if (channel_id.equals(focus.getChannelId())) {
                    adIdsBuilder.append(focus.getAdId()).append(',');
                }
            }
        }

        String adIds = adIdsBuilder.toString();
        // 如果adIds为空，或者只有一个','  数据出错
        if (TextUtils.isEmpty(adIds) || adIds.equals(',')) {
            return "";
        }

        params.append(adIds);
        appendParams(params, context);
        return params.toString();
    }


    // public static ChannelBean.HomePageBean convert2HomePageBean(MainAdInfoModel.AdMaterial adMaterial, Context context, String position_id) {
    //
    //     if (adMaterial == null || context == null || TextUtils.isEmpty(position_id)) {
    //         return null;
    //     }
    //     ChannelBean.HomePageBean body = new ChannelBean.HomePageBean();
    //
    //     body.setKkrand(adMaterial.getKkrand());
    //     MainAdInfoModel.AdMaterial.Icon icon = adMaterial.getIcon();
    //     if (icon != null && icon.getShowIcon() == 1) {
    //         body.setTag(TextUtils.isEmpty(icon.getText()) ? "广告" : icon.getText());
    //     }
    //     body.setAd(true);
    //     body.setNeed2Expose(true);
    //     String adTitle = adMaterial.getText();
    //     body.setTitle(adTitle);
    //     body.setItemId(adMaterial.getAdId());
    //     body.setInfoId(adMaterial.getAdId());
    //     body.setAdPositionId(position_id);
    //     body.setPhotos(adMaterial.getPhotos());
    //     List<ChannelBean.HomePageBean.ImageBean> imageList = new ArrayList<>();
    //     ChannelBean.HomePageBean.ImageBean imageBean = new ChannelBean.HomePageBean.ImageBean();
    //     imageBean.setImage(adMaterial.getImageURL());
    //     imageList.add(imageBean);
    //     body.setImageList(imageList);
    //
    //     body.setImage(adMaterial.getImageURL());
    //
    //     body.setAbstractDesc(adMaterial.getVdescription());
    //     body.setMemberType(IfengType.TYPE_ADVERT);
    //
    //     ChannelBean.MemberItemBean memberItem = new ChannelBean.MemberItemBean();
    //     memberItem.setName(adTitle);
    //     final MainAdInfoModel.AdMaterial.AdAction adAction = adMaterial.getAdAction();
    //     MainAdInfoModel.AdMaterial.AdConditions adConditions = adMaterial.getAdConditions();
    //     if (adAction != null) {
    //         memberItem.setClickType(IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType()) ? IfengType.TYPE_BROWSER : IfengType.TYPE_WEBFULL);
    //         memberItem.setmUrl(adAction.getUrl());
    //         memberItem.setAsync_download(adAction.getAsync_download());
    //         if (adConditions != null && ADJumpType.AD_SHOWTYPE_APP.equals(adConditions.getShowType())) {
    //             memberItem.setDownloadCompletedurl(adAction.getDownloadCompletedurl());
    //             memberItem.setClickUrl(getAdClickUrl(adAction.getLoadingurl(), context));
    //         } else {
    //             memberItem.setClickUrl(getAdClickUrl(adAction.getUrl(), context));
    //         }
    //
    //         List<String> ls = adAction.getAsync_click();
    //         if (ls instanceof ArrayList) {
    //             body.setAsync_click((ArrayList<String>) ls);//传值字段
    //         }
    //
    //         List<String> pvList = adAction.getPvurl();
    //         if (!ListUtils.isEmpty(pvList) && pvList instanceof ArrayList) {
    //             body.setPvurl((ArrayList<String>) pvList);
    //         }
    //     }
    //     if (adConditions != null) {
    //         body.setShowType(adConditions.getShowType());
    //     }
    //     body.setMemberItem(memberItem);
    //     return body;
    // }

    public static void exposeAdPvUrl(ChannelBean.MemberItemBean bean) {
        // 广告部的异步点击地址SDK上报
        ChannelBean.MemberItemBean.AdAction adAction = bean.adAction;
        if (adAction != null && !ListUtils.isEmpty(adAction.pvurl)) {
            List<String> list = adAction.pvurl;
            if (list instanceof ArrayList) {
                AdvertExposureDao.sendAdvertClickReq(bean.adId, (ArrayList<String>) list);
            }
        }
    }

    public static void exposeAdPvUrl(MainAdInfoModel.AdMaterial material) {
        // 广告部的异步点击地址SDK上报
        MainAdInfoModel.AdMaterial.AdAction adAction = material.getAdAction();
        if (adAction != null && !ListUtils.isEmpty(adAction.getPvurl())) {
            List<String> list = adAction.getPvurl();
            if (list instanceof ArrayList) {
                AdvertExposureDao.sendAdvertClickReq(material.getAdId(), (ArrayList<String>) list);
            }
        }
    }

    public static void exposeAdPvUrl(HomePageBeanBase bean) {
        // 广告部的异步点击地址SDK上报
        if (bean != null && !ListUtils.isEmpty(bean.getMemberItem().adAction.pvurl)) {
//            AdvertExposureDao.sendAdvertClickReq(bean.getItemId(), bean.getPvurl());
            if (!TextUtils.isEmpty(bean.getMemberItem().imageURL)){
                AdvertExposureDao.addIfengAdvExposureForChannel(bean.getMemberItem().adId, null, bean.getMemberItem().adAction.pvurl, bean.getMemberItem().adAction.adpvurl);
            } else {
                AdvertExposureDao.addIfengAdvExposureStatistics(bean.getMemberItem().adId, null, bean.getMemberItem().adAction.pvurl);
            }

        }
    }

    // public static HomePageBeanBase convert2HomePageBeanBase(MainAdInfoModel.AdMaterial adMaterial, Context context, String position_id) {
    //
    //     if (adMaterial == null || context == null || TextUtils.isEmpty(position_id)) {
    //         return null;
    //     }
    //     HomePageBeanBase body = new HomePageBeanBase();
    //     body.setKkrand(adMaterial.getKkrand());
    //     MainAdInfoModel.AdMaterial.Icon icon = adMaterial.getIcon();
    //     if (icon != null && icon.getShowIcon() == 1) {
    //         body.setTag(TextUtils.isEmpty(icon.getText()) ? "广告" : icon.getText());
    //     }
    //     body.setPhotos(adMaterial.getPhotos());
    //     body.setAd(true);
    //     body.setNeed2Expose(true);
    //     String adTitle = adMaterial.getText();
    //     body.setTitle(adTitle);
    //     body.setItemId(adMaterial.getAdId());
    //     body.setAdPositionId(position_id);
    //
    //     List<ChannelBean.HomePageBean.ImageBean> imageList = new ArrayList<>();
    //     ChannelBean.HomePageBean.ImageBean imageBean = new ChannelBean.HomePageBean.ImageBean();
    //     imageBean.setImage(adMaterial.getImageURL());
    //     imageList.add(imageBean);
    //     body.setImageList(imageList);
    //
    //     body.setImage(adMaterial.getImageURL());
    //
    //     body.setAbstractDesc(adMaterial.getVdescription());
    //     body.setMemberType(IfengType.TYPE_ADVERT);
    //
    //     ChannelBean.MemberItemBean memberItem = new ChannelBean.MemberItemBean();
    //     memberItem.setName(adTitle);
    //     final MainAdInfoModel.AdMaterial.AdAction adAction = adMaterial.getAdAction();
    //     MainAdInfoModel.AdMaterial.AdConditions adConditions = adMaterial.getAdConditions();
    //     if (adAction != null) {
    //         memberItem.setClickType(IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType()) ? IfengType.TYPE_BROWSER : IfengType.TYPE_WEBFULL);
    //         memberItem.setmUrl(adAction.getUrl());
    //         memberItem.setAsync_download(adAction.getAsync_download());
    //         if (adConditions != null && ADJumpType.AD_SHOWTYPE_APP.equals(adConditions.getShowType())) {
    //             memberItem.setDownloadCompletedurl(adAction.getDownloadCompletedurl());
    //             memberItem.setClickUrl(getAdClickUrl(adAction.getLoadingurl(), context));
    //         } else {
    //             memberItem.setClickUrl(getAdClickUrl(adAction.getUrl(), context));
    //         }
    //
    //         List<String> ls = adAction.getAsync_click();
    //         if (ls instanceof ArrayList) {
    //             body.setAsync_click((ArrayList<String>) ls);//传值字段
    //         }
    //
    //         List<String> pvList = adAction.getPvurl();
    //         if (!ListUtils.isEmpty(pvList) && pvList instanceof ArrayList) {
    //             body.setPvurl((ArrayList<String>) pvList);
    //         }
    //     }
    //     if (adConditions != null) {
    //         body.setShowType(adConditions.getShowType());
    //     }
    //     body.setMemberItem(memberItem);
    //     return body;
    // }

    public static String getAdClickUrl(String url, Context context) {
        String str = "?";
        if (!TextUtils.isEmpty(url) && url.contains("?")) {
            str = "&";
        }
        return url + str + getAdClickParams(context);
    }

    public static String getAdClickParams(Context context) {
        final SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        final String province = sharePreUtils != null ? sharePreUtils.getProvince() : "";
        final String city = sharePreUtils != null ? sharePreUtils.getCity() : "";

        return "gv=" + PhoneConfig.softversion
                + "&os=android_" + Build.VERSION.SDK_INT
                + "&uid=" + PhoneConfig.UID
                + "&deviceid=" + PhoneConfig.IMEI
                + "&screen=" + PhoneConfig.getScreenForAd()
                + "&publishid=" + PhoneConfig.publishid
                + "&network=" + NetUtils.getNetType(context)
                + "&country=CN"
                + "&proid=ifengvideo"
                + "&df=androidphone"
                + "&t=" + System.currentTimeMillis()
                + "&province=" + province
                + "&city=" + city;
    }

    /**
     * 是否为有效数据
     */
    public static boolean isValidAdModel(MainAdInfoModel mainAdInfoModel) {
        return mainAdInfoModel != null && !ListUtils.isEmpty(mainAdInfoModel.getAdMaterials());
    }

    /**
     * 首页信息流广告过滤 id img text
     */
    public static MainAdInfoModel filterMainADModel(MainAdInfoModel adInfoModel) {
        if (!isValidAdModel(adInfoModel)) {
            return null;
        }
        List<MainAdInfoModel.AdMaterial> adMaterials = adInfoModel.getAdMaterials();
        List<MainAdInfoModel.AdMaterial> validAdMaterials = new ArrayList<>();
        for (MainAdInfoModel.AdMaterial adMaterial : adMaterials) {
            if (adMaterial == null) {
                continue;
            }
            // 余量
            boolean isMoreAd = adMaterial.getNeedMoreAd() == 1;

            // 关键字段
            boolean isValidAd = !TextUtils.isEmpty(adMaterial.getAdId())
                    && !TextUtils.isEmpty(adMaterial.getImageURL())
                    && !TextUtils.isEmpty(adMaterial.getText());

            if (isMoreAd || isValidAd) {
                validAdMaterials.add(adMaterial);
            }
        }
        adInfoModel.setAdMaterials(validAdMaterials);
        return adInfoModel;
    }

    /**
     * 点播底页信息流广告过滤   id img
     */
    public static MainAdInfoModel filterVideoBannerADModel(MainAdInfoModel adInfoModel) {
        if (!isValidAdModel(adInfoModel)) {
            return null;
        }
        List<MainAdInfoModel.AdMaterial> adMaterials = adInfoModel.getAdMaterials();
        List<MainAdInfoModel.AdMaterial> validAdMaterials = new ArrayList<>();
        for (MainAdInfoModel.AdMaterial adMaterial : adMaterials) {
            if (adMaterial == null) {
                continue;
            }
            // 余量
            boolean isMoreAd = adMaterial.getNeedMoreAd() == 1;

            // 关键字段
            boolean isValidAd = !TextUtils.isEmpty(adMaterial.getAdId())
                    && !TextUtils.isEmpty(adMaterial.getImageURL());

            if (isMoreAd || isValidAd) {
                validAdMaterials.add(adMaterial);
            }
        }
        adInfoModel.setAdMaterials(validAdMaterials);
        return adInfoModel;
    }

    /**
     * 请求余量广告
     */
    public static void requestADMore(Context context, String position_id, String channle_tag,
                                     final MainAdInfoModel.AdMaterial src_material, final Object adapter) {
        if (context == null || src_material == null || adapter == null) {
            return;
        }
        String url = AdTools.getAdMoreRequestUrl(position_id, context);
        if (TextUtils.isEmpty(url)) {
            return;
        }
        // 取消之前余量请求
        VolleyHelper.getRequestQueue().cancelAll(channle_tag);
        ADInfoDAO.getBodyAd(url, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (response == null || TextUtils.isEmpty(response.toString())) {
                    src_material.setHave_content(true);
                    if (adapter instanceof BaseAdapter) {
                        ((BaseAdapter) adapter).notifyDataSetChanged();
                    }
                    return;
                }
                List<MainAdInfoModel> mainAdInfoModels = null;
                try {
                    mainAdInfoModels = JSONArray.parseArray(response.toString(), MainAdInfoModel.class);
                } catch (Exception e) {
                    src_material.setHave_content(true);
                    if (adapter instanceof BaseAdapter) {
                        ((BaseAdapter) adapter).notifyDataSetChanged();
                    }
                    return;
                }
                if (ListUtils.isEmpty(mainAdInfoModels)) {
                    src_material.setHave_content(true);
                    if (adapter instanceof BaseAdapter) {
                        ((BaseAdapter) adapter).notifyDataSetChanged();
                    }
                    return;
                }
                MainAdInfoModel model = mainAdInfoModels.get(0);
                if (model != null && !ListUtils.isEmpty(model.getAdMaterials())) {
                    List<MainAdInfoModel.AdMaterial> list = model.getAdMaterials();
                    if (!ListUtils.isEmpty(list)) {
                        MainAdInfoModel.AdMaterial material_new = list.get(0);
                        if (material_new != null) {
                            src_material.setText(material_new.getText());
                            src_material.setIcon(material_new.getIcon());
                            src_material.setAdStartTime(material_new.getAdStartTime());
                            src_material.setAdEndTime(material_new.getAdEndTime());
                            src_material.setAdAction(material_new.getAdAction());
                            src_material.setAdId(material_new.getAdId());
                            src_material.setAdConditions(material_new.getAdConditions());
                            src_material.setKkrand(material_new.getKkrand());
                            src_material.setPhotos(material_new.getPhotos());
                            src_material.setVdescription(material_new.getVdescription());
                            src_material.setImageURL(material_new.getImageURL());
                            src_material.setNeedMoreAd(1);//手动置1,保证下次还继续请求新的余量广告
                            src_material.setHave_content(true);

                            AdTools.exposeAdPvUrl(src_material);//余量广告曝光
                            if (adapter instanceof BaseAdapter) {
                                ((BaseAdapter) adapter).notifyDataSetChanged();
                            } else if (adapter instanceof PagerAdapter) {
                                ((PagerAdapter) adapter).notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                src_material.setHave_content(true);
                if (adapter instanceof BaseAdapter) {
                    ((BaseAdapter) adapter).notifyDataSetChanged();
                }
            }
        }, channle_tag);
    }

    public static void loadImpressionUrl(HomePageBeanBase bean) {
        if (bean != null && CheckIfengType.isAD(bean.getMemberType()) && bean.getMemberItem().getImpressions() != null) {
            ArrayList<String> urls = new ArrayList<>();
            String[] ss = bean.getMemberItem().getImpressions();
            for (int i = 0; i < ss.length; i++) {
                String s = ss[i];
                if (!TextUtils.isEmpty(s)) {
                    urls.add(s);
                }
            }
            if (urls.size() > 0) {
                for (final String url : urls) {
                    CommonDao.sendRequest(url, null, null, null, CommonDao.RESPONSE_TYPE_GET_JSON);
                }
            }
        }
    }

    //启动页空广告
    public static boolean splashADIsEmpty(ADInfoModel adInfoModel) {
        return (null != adInfoModel && (TextUtils.isEmpty(adInfoModel.getADId()) || "0".equals(adInfoModel.getADId())));
    }
    //AdBanner 空广告
    public static boolean adBannerIsEmpty(MainAdInfoModel model) {
        return ListUtils.isEmpty(model.getAdMaterials()) || TextUtils.isEmpty(model.getAdMaterials().get(0).getAdId());
    }
}
