package com.ifeng.newvideo.ui.ad;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.coustomshare.SharePlatform;
import com.ifeng.newvideo.coustomshare.SharePlatformUtils;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.live.vr.VRBaseActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.utils.BuildUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 广告页面，更多PopWindow
 * Created by fengfan on 2015/8/10,10:33.
 */
public class ADMorePopWindow extends LinearLayout implements View.OnClickListener, SharePlatform {

    private static final Logger logger = LoggerFactory.getLogger(ADMorePopWindow.class);

    private PopupWindow mPop;
    private OneKeyShare oneKeyShare;
    private Platform mPlatform;
    private String pageUrl;
    private String mTitle;

    ImageView ivQQ;
    ImageView ivSina;
    ImageView ivWechat;
    ImageView ivWechatMoment;
    ImageView ivQzone;
    ImageView ivAlipay;
    View copy;
    View browser;
    View ivRefresh;
    View refresh;
    TextView tvCancel;
    View rootView;
    View shareContainer;
    Animation enterAnimation;
    Animation exitAnimation;
    private RefreshCallBack mRefreshCallback;
    boolean isFromVR = false;

    public ADMorePopWindow(Context context, RefreshCallBack refreshCallback) {
        super(context);
        mRefreshCallback = refreshCallback;
        initView(context);
    }

    public ADMorePopWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPop(PopupWindow mPop) {
        this.mPop = mPop;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public void setOneKeyShare(OneKeyShare oneKeyShare) {
        this.oneKeyShare = oneKeyShare;
    }

    private void initView(Context context) {
        if (context instanceof VRBaseActivity) {
            isFromVR = true;
        }
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.ad_more_popwindow, null);

        ivQQ = (ImageView) view.findViewById(R.id.iv_qq);
        ivQQ.setOnClickListener(this);

        ivSina = (ImageView) view.findViewById(R.id.iv_sina);
        ivSina.setOnClickListener(this);

        ivWechat = (ImageView) view.findViewById(R.id.iv_wechat);
        ivWechat.setOnClickListener(this);

        ivWechatMoment = (ImageView) view.findViewById(R.id.iv_wechat_moment);
        ivWechatMoment.setOnClickListener(this);

        ivQzone = (ImageView) view.findViewById(R.id.iv_qzone);
        ivQzone.setOnClickListener(this);

        ivAlipay = (ImageView) view.findViewById(R.id.iv_alipay);
        ivAlipay.setOnClickListener(this);

        if (SharePlatformUtils.hasWebChat()) {
            ivWechat.setImageResource(R.drawable.btn_share_white_wechat);
            ivWechatMoment.setImageResource(R.drawable.btn_share_pyq);
        } else {
            ivWechat.setImageResource(R.drawable.btn_share_white_wechat_none);
            ivWechatMoment.setImageResource(R.drawable.btn_share_white_pyq_none);
        }
        if (SharePlatformUtils.hasQQ()) {
            ivQQ.setImageResource(R.drawable.btn_share_white_qq);
            ivQzone.setImageResource(R.drawable.btn_share_qzone);
        } else {
            ivQQ.setImageResource(R.drawable.btn_share_white_qq_none);
            ivQzone.setImageResource(R.drawable.btn_share_qzone_none);
        }

        if (SharePlatformUtils.hasAlipay()) {
            ivAlipay.setImageResource(R.drawable.btn_share_alipay);
        } else {
            ivAlipay.setImageResource(R.drawable.btn_share_alipay_none);
        }

        copy = view.findViewById(R.id.copy);
        copy.setOnClickListener(this);

        browser = view.findViewById(R.id.browser);
        browser.setOnClickListener(this);

        refresh = view.findViewById(R.id.refresh);
        ivRefresh = view.findViewById(R.id.iv_refresh);
        ivRefresh.setOnClickListener(this);

        tvCancel = (TextView) view.findViewById(R.id.cancel);
        tvCancel.setOnClickListener(this);

        rootView = view.findViewById(R.id.rootView);
        shareContainer = view.findViewById(R.id.shareContainer);
        rootView.setOnClickListener(this);

        exitAnimation = AnimationUtils.loadAnimation(IfengApplication.getInstance(), R.anim.common_popup_exit);
        enterAnimation = AnimationUtils.loadAnimation(IfengApplication.getInstance(), R.anim.common_popup_enter);
        startAnim();

        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        addView(view);
    }

    public void startAnim() {
        shareContainer.startAnimation(enterAnimation);
    }

    public void hideCopyAndBrowser() {
        copy.setVisibility(View.GONE);
        browser.setVisibility(View.GONE);
    }

    public void hideCopyBrowserRefreshView() {
        copy.setVisibility(View.GONE);
        browser.setVisibility(View.GONE);
        refresh.setVisibility(View.GONE);
    }

    //分享图标是否可以点击，如果mUrl为空的话，不可点击，默认可以点击
    private boolean isClickable = true;

    public void shareIconClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_sina:
                if (isClickable) {
                    mPlatform = ShareSDK.getPlatform(SinaWeibo.NAME);
                    shareToPlatform(mPlatform, true);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_SINA, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.iv_wechat:
                if (isClickable) {
                    mPlatform = ShareSDK.getPlatform(Wechat.NAME);
                    shareToPlatform(mPlatform, false);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WX, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.iv_qq:
                if (isClickable) {
                    mPlatform = ShareSDK.getPlatform(QQ.NAME);
                    shareToPlatform(mPlatform, false);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQ, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.iv_wechat_moment:
                if (isClickable) {
                    mPlatform = ShareSDK.getPlatform(WechatMoments.NAME);
                    shareToPlatform(mPlatform, false);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WXF, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.iv_qzone:
                if (isClickable) {
                    mPlatform = ShareSDK.getPlatform(QZone.NAME);
                    shareToPlatform(mPlatform, false);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQZONE, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.iv_alipay:
                if (isClickable) {
                    mPlatform = ShareSDK.getPlatform(Alipay.NAME);
                    shareToPlatform(mPlatform, false);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_ALIPAY, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.copy:
                if (mPop != null && mPop.isShowing()) {
                    mPop.dismiss();
                }
                copy();
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_COPY, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                break;
            case R.id.browser:
                if (!TextUtils.isEmpty(pageUrl)) {
                    IntentUtils.startBrowser(IfengApplication.getInstance(), pageUrl);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_BROWSER, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                }
                break;
            case R.id.iv_refresh:
                if (mPop != null && mPop.isShowing()) {
                    mPop.dismiss();
                }
                if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (mRefreshCallback != null) {
                    mRefreshCallback.refresh();
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_REFRESH, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                break;
            case R.id.cancel:
            case R.id.rootView:
                exitAnim();
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_CANCEL, isFromVR ? PageIdConstants.SHARE_H : PageIdConstants.SHARE_V);
                break;
        }
    }


    public void exitAnim() {
        if (mPop != null && mPop.isShowing()) {
            shareContainer.startAnimation(exitAnimation);
            exitAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mPop.dismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void copy() {
        if (!TextUtils.isEmpty(pageUrl)) {
            ClipboardManager myClipboard = (ClipboardManager) IfengApplication.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
            if (BuildUtils.hasHoneycomb()) {
                ClipData clipData = ClipData.newPlainText("pageUrl", pageUrl);
                myClipboard.setPrimaryClip(clipData);
            }
            ToastUtils.getInstance().showShortToast(R.string.copied_to_clipboard);
        }
    }

    public interface RefreshCallBack {
        void refresh();
    }

    @Override
    public void shareToPlatform(Platform platform, boolean isShowEditPage) {
        if (mPop != null && mPop.isShowing()) {
            mPop.dismiss();
        }

        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare.shareToPlatform(platform, isShowEditPage);
            if (!isShowEditPage && !(platform.getName()).equals("QQ") && !(platform.getName().equals("QZone"))) {
                OneKeyShareContainer.oneKeyShare = null;
            }
        }
    }

}
