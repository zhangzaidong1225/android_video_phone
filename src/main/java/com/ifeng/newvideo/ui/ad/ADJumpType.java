package com.ifeng.newvideo.ui.ad;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.NewsSilenceInstallUtils;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;

import java.util.ArrayList;
import java.util.List;

/**
 * 根据广告点击类型进行跳转
 * 广告点击类型有gin/gout/adin/adout/webfull/browser
 * gin/adin/webfull跳转到内部webView界面
 * gout/adout/browser弹出dialog用浏览器打开
 */
public class ADJumpType {

    /**
     * dialog：提示用户使用浏览器打开url
     */
    private static Dialog dialog;

    /**
     * 无线广告展示类型
     */
    public static final String AD_SHOWTYPE_APP = "app";
    public static final String AD_SHOWTYPE_IMG = "img";
    public static final String AD_SHOWTYPE_PHOTO = "photos";
    public static final String AD_SHOWTYPE_VIDEO = "video";
    public static final String AD_SHOWTYPE_LARGE = "large";

    /**
     * 广告跳转
     *
     * @param adId                  无线广告id,普通填null
     * @param adType                6.10.0首页HomePageModel.MemberItem
     * @param title                 广告标题
     * @param imageUrl              广告图片
     * @param clickUrl              广告点击地址
     * @param shareUrl              分享地址
     * @param appUrl                新闻客户端下载地址，广告类型为adnew时，未安装新闻客户端时使用地址
     * @param appScheme             广告类型为adnew时，跳转到新闻客户端指定页面
     * @param downloadCompletedUrls 广告下载完成上报地址
     * @param echid
     * @param chid
     * @param asyncDownloadUrl
     */
    public static void jump(String adId, String adType, String title, String imageUrl, String clickUrl,
                            String shareUrl, String appUrl, String appScheme, Context context,
                            List<String> downloadCompletedUrls, String echid, String chid, List<String> asyncDownloadUrl) {
        if (TextUtils.isEmpty(adType) || TextUtils.isEmpty(clickUrl)) {
            return;
        }

        if (CheckIfengType.isAdnew(adType)) {
            //已安装新闻客户端：取schema数据，拉起新闻客户端; 未安装：使用appurl跳转到内部webview页面
            //appScheme 样例：comifengnewsclient://call?type=doc&aid=102722589
            if (PackageUtils.checkAppExist(context, PackageUtils.NEWS_PACKAGE_NAME)) {
                IntentUtils.startIfengNewsApp(context, appScheme, echid);
                return;
            } else {
                if (NewsSilenceInstallUtils.isDownloadComplete()) {
                    NewsSilenceInstallUtils.installApkIfNeeded();
                    return;
                }

                clickUrl = appUrl;

                PageActionTracker.clickBtn(ActionIdConstants.CLICK_JUMP_NEWS_APP, ActionIdConstants.ACT_NO,
                        String.format(PageIdConstants.HOME_CHANNEL, echid));
            }
        }

        //应用内拉起广告
        if (IfengType.TYPE_AD_IN.equalsIgnoreCase(adType)
                || IfengType.TYPE_G_IN.equalsIgnoreCase(adType)
                || IfengType.TYPE_WEBFULL.equalsIgnoreCase(adType)
                //将adapp  adnew不属于广告类型，但同广告一样跳转到webView及显示“推广”标签，暂归到广告类型
                || IfengType.TYPE_AD_APP.equalsIgnoreCase(adType)
                || IfengType.TYPE_AD_NEW.equalsIgnoreCase(adType)) {

            String function = getFunction(adType, shareUrl);
            String finalShareUrl = getShareUrl(adType, clickUrl, shareUrl);

            IntentUtils.startADActivity(context, adId, clickUrl, finalShareUrl, function, title, null, imageUrl, echid, chid,
                    (ArrayList<String>) downloadCompletedUrls, (ArrayList<String>) asyncDownloadUrl);
        }
        //应用外拉起广告
        else if (IfengType.TYPE_AD_OUT.equalsIgnoreCase(adType)
                || IfengType.TYPE_G_OUT.equalsIgnoreCase(adType)
                || IfengType.TYPE_BROWSER.equalsIgnoreCase(adType)) {
            if (!(context instanceof Activity)) {
                return;
            }
            // 提示用户启动系统浏览器拉起广告页Dialog
            showOpenWithBrowserDialog(clickUrl, context);
        }

    }

    /**
     * 如果接口返回为空，赋值为clickUrl
     */
    private static String getShareUrl(String adType, String clickUrl, String shareUrl) {
        if (TextUtils.isEmpty(shareUrl)) {
            shareUrl = clickUrl;
        }
        return shareUrl;
    }

    /**
     * 是否为由编辑控制的广告
     */
    private static boolean isADFromEditor(String adType) {
        return IfengType.TYPE_AD_IN.equalsIgnoreCase(adType)
                || IfengType.TYPE_AD_OUT.equalsIgnoreCase(adType)
                || IfengType.TYPE_G_IN.equalsIgnoreCase(adType)
                || IfengType.TYPE_G_OUT.equalsIgnoreCase(adType);
    }

    /**
     * 根据产品需求，控制广告展示页面的一些功能
     * 如：编辑控制的广告，如果编辑填写了shareUrl，广告页面点击更多不支持复制和浏览器功能
     */
    private static String getFunction(String adType, String shareUrl) {
        if (CheckIfengType.isAdBackend(adType) || IfengType.TYPE_BROWSER.equalsIgnoreCase(adType)
                || IfengType.TYPE_WEBFULL.equalsIgnoreCase(adType) || CheckIfengType.isAdnew(adType)
                || CheckIfengType.isAdapp(adType)) {
            return ADActivity.FUNCTION_VALUE_AD;
        }
        if (isADFromEditor(adType)) {
            return ADActivity.FUNCTION_VALUE_HIDE_COPY_BROWSER;
        }
        return null;
    }

    /**
     * 弹出使用浏览器打开url的dialog
     *
     * @param clickUrl 要打开的url
     */
    private static void showOpenWithBrowserDialog(final String clickUrl, final Context context) {
        dialog = AlertUtils.getInstance().showTwoBtnDialog(context, context.getString(R.string.ad_dialog_hint_browse),
                context.getString(R.string.common_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        IntentUtils.startBrowser(context, clickUrl);
                        dismissDialog();
                    }
                }, context.getString(R.string.common_btn_cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog();
                    }
                }
        );
    }

    private static void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
