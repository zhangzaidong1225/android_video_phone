package com.ifeng.newvideo.ui.ad;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.dao.VideoAdInfoDao;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.db.model.VideoAdInfoModel;
import com.ifeng.video.player.IfengMediaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 视频暂停播放时显示的广告PopWindow
 * Created by Vincent on 2015/3/4.
 */
public class PauseAdPopWindow extends RelativeLayout implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(PauseAdPopWindow.class);
    private ImageView ivPauseAd;
    private RelativeLayout rlPauseClose;
    private PopupWindow pop;
    private boolean isNeedShow;
    private boolean isLandscape;
    private String param;
    private SharedPreferences preferences;
    private VideoAdInfoModel model;
    private IfengMediaController.MediaPlayerControl mMediaPlayerControl;
    private String adId;

    public PauseAdPopWindow(Context context) {
        super(context);
        initView(context);
        initPopWindow();
        initData(context);
    }

    public void setmMediaPlayerControl(IfengMediaController.MediaPlayerControl mMediaPlayerControl) {
        this.mMediaPlayerControl = mMediaPlayerControl;
    }

    private void initData(Context context) {
        preferences = context.getSharedPreferences("province", Context.MODE_PRIVATE);
    }

    public boolean isShowing() {
        return pop != null && pop.isShowing();
    }

    public PauseAdPopWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPop(PopupWindow pop) {
        this.pop = pop;
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.pause_ad_layout, null);
        ivPauseAd = (ImageView) view.findViewById(R.id.pause_ad_image);
        rlPauseClose = (RelativeLayout) view.findViewById(R.id.rl_pause_ad_close);
        ivPauseAd.setOnClickListener(this);
        rlPauseClose.setOnClickListener(this);
        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        addView(view);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_pause_ad_close:
                dismissAd();
                break;
            case R.id.pause_ad_image:
                break;
        }
    }

    private void initPopWindow() {

        int mAdWidth = DisplayUtils.getWindowHeight() / 2 + DisplayUtils.convertDipToPixel(18);
        int mAdHeight = mAdWidth / 16 * 9;
        pop = new PopupWindow(this, mAdWidth, mAdHeight, false);
        pop.setBackgroundDrawable(new BitmapDrawable());
        pop.setOutsideTouchable(false);
    }

    public void showPopAd(boolean isLandScape) {
        if (VideoAdConfigDao.adConfig == null)
            return;

        this.isLandscape = isLandScape;
        getPauseAdInfo();
    }


    public void setParam(String typeNum, String type, int playCount, PlayerInfoModel model, String mColumn, String echId, String topicId, String openFirst) throws UnsupportedEncodingException {
        adId = typeNum;
        String province = preferences.getString("province", "");
        String city = preferences.getString("city", "");
        StringBuilder builder = new StringBuilder();
        builder.append(typeNum);
        builder.append("&c=").append(System.currentTimeMillis());
        builder.append("&FID=").append(PhoneConfig.UID);
        builder.append("&CUSTOM=");
        StringBuilder paramBuilder = new StringBuilder();
        try {
            paramBuilder.append("mFrom=").append(URLEncoderUtils.encodeInUTF8(type));
            paramBuilder.append("&OpenFirst=").append(URLEncoderUtils.encodeInUTF8(openFirst)).append("&mCount=").append(playCount);
            paramBuilder.append("&mVid=").append(URLEncoderUtils.encodeInUTF8(model.getGuid()));
            paramBuilder.append("&mDuration=").append(model.getDuration());
            paramBuilder.append("&mColumn=").append(URLEncoderUtils.encodeInUTF8(mColumn)).append("&mChannel=").
                    append(URLEncoderUtils.encodeInUTF8(echId)).append("&mSubject=").append(URLEncoderUtils.encodeInUTF8(topicId));
            paramBuilder.append("&mKeyword=").append(URLEncoderUtils.encodeInUTF8(model.getName()));
            paramBuilder.append("&mProv=").append(URLEncoderUtils.encodeInUTF8(province)).append("&mCity=").append(URLEncoderUtils.encodeInUTF8(city));
            builder.append(URLEncoderUtils.encodeInUTF8(paramBuilder.toString()));
        } catch (UnsupportedEncodingException e) {
            builder.append(paramBuilder.toString());
        }
        param = builder.toString();
        logger.debug("VideoAdInfoDao getParam ====={}", param);
    }

    private String getParam() {
        return param;
    }


    public void dismissAd() {
        pop.dismiss();
        isNeedShow = false;
    }

    /**
     * 转屏幕时调用
     *
     * @param isLandScape
     */
    public void isHorizontalNeedShowOrVerDiss(boolean isLandScape) {
        if (VideoAdConfigDao.adConfig == null)
            return;

        if (isLandScape && isNeedShow) {
            pop.showAtLocation(this, Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, -20);
            exposureData(model.getStart());
            ADRecord.addAdShow(model.getId(), ADRecord.AdRecordModel.ADTYPE_INFO);
        } else if (!isLandScape)
            pop.dismiss();
    }


    private void getPauseAdInfo() {
        VideoAdInfoDao.getAdInfo(VideoAdConfigDao.adConfig.getVideoAD().getInterfaceUrl(), getParam(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                logger.error("VideoAdInfoDao is url {}", VideoAdConfigDao.adConfig.getVideoAD().getInterfaceUrl() + getParam());
                try {
                    model = new VideoAdInfoModel(response);
                    model.setId(adId);
                    getAdImage(model.getUrl());
                } catch (Exception e) {
                    logger.error("VideoAdInfoDao 暂停广告，解析错误");
                }
            }
        }, new GetPauseAdErrorListener());
    }

    private void getAdImage(String url) {
        VolleyHelper.getImageLoader().get(url, new ImageLoader.ImageListener() {

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() == null)
                    return;

                ivPauseAd.setImageBitmap(response.getBitmap());
                if (mMediaPlayerControl.isPlaying())
                    return;

                boolean isLand = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
                if (isLand) {
                    pop.showAtLocation(PauseAdPopWindow.this, Gravity.CENTER_HORIZONTAL, 0, -20);
                    exposureData(model.getStart());
                    ADRecord.addAdShow(model.getId(), ADRecord.AdRecordModel.ADTYPE_INFO);
                }
                isNeedShow = true;
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("图片下载超时!!!");
            }
        });
    }

    /**
     * 数据曝光，接口为空会报空指针。
     *
     * @param list
     */
    private void exposureData(final List<String> list) {
        if (list == null) {
            return;
        }
        logger.debug("暂停广告 url====={}", list.toString());
        VideoAdInfoDao.exposeAdData(list, new PauseAdExposureSuccessListener(), new PauseAdExposureErrorListener());
    }

    private static class GetPauseAdErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            logger.error("VideoAdInfoDao 获取暂停广告失败");
        }
    }

    private static class PauseAdExposureSuccessListener implements Response.Listener<JSONObject> {
        @Override
        public void onResponse(JSONObject response) {
            logger.error("暂停广告曝光成功");
        }
    }

    private static class PauseAdExposureErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            logger.error("暂停广告曝光失败");
        }
    }
}
