package com.ifeng.newvideo.ui.ad;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.sharesdk.framework.Platform;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.appstore.WebViewDownLoadListener;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.dialogUI.LiveCommentUtil;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.basic.BaseLiveActivity;
import com.ifeng.newvideo.ui.live.weblive.JsBridge;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.widget.BaseWebViewClient;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.player.ChoosePlayerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;

/**
 * 广告页面
 */
public class ADActivity extends BaseLiveActivity implements OnClickListener, ADMorePopWindow.RefreshCallBack, NotifyShareCallback, JsBridge.JSDispatchListener {

    private static final Logger logger = LoggerFactory.getLogger(ADActivity.class);

    private static final String AD_APP_URL = "http://v.ifeng.com/apps/app-android.shtml";


    /**
     * 广告页面的功能key，根据不同value执行不同功能（如FUNCTION_HIDE_COPY_BROWSER，隐藏复制&浏览器功能）
     */
    public static final String KEY_FUNCTION = "KEY_FUNCTION";

    /**
     * 隐藏复制和浏览器打开
     */
    public static final String FUNCTION_VALUE_HIDE_COPY_BROWSER = "FUNCTION_HIDE_COPY_BROWSER";

    /**
     * 加载h5直播
     */
    public static final String FUNCTION_VALUE_H5_LIVE = "FUNCTION_H5_LIVE";

    /**
     * 广告
     */
    public static final String FUNCTION_VALUE_AD = "FUNCTION_AD";

    /**
     * 加载功能H5页面，比如：会员特权
     */
    public static final String FUNCTION_VALUE_H5_STATIC = "FUNCTION_H5_STATIC";

    /**
     * 是否为广告的标识
     */
    public static final String KEY_AD_FLAG = "AD";
    /**
     * 广告url
     */
    public static final String KEY_AD_CLICK_URL = "AD_CLICK_URL";
    /**
     * 广告url
     */
    public static final String KEY_AD_SHARE_URL = "AD_SHARE_URL";
    /**
     * 广告标题
     */
    public static final String KEY_AD_TITLE = "title";
    /**
     * 广告图片，用于分享到第三方中展示
     */
    public static final String KEY_AD_IMAGE = "image";
    /**
     * 广告Id
     */
    public static final String KEY_AD_ID = "adId";
    /**
     * Echid
     */
    public static final String KEY_ECHID = "echid";
    /**
     * Echid
     */
    public static final String KEY_CHID = "chid";
    /**
     * 广告下载完成监测地址
     */
    public static final String KEY_AD_DOWNLOAD_COMPLETED_URL = "downloadCompletedUrl";

    /**
     * 广告下载开始监测地址
     */
    public static final String KEY_AD_DOWNLOAD_START_URL = "async_download";

    /**
     * 分享标题
     */
    public static final String KEY_AD_SHARE_TITLE = "share_title";
    /**
     * 页面标题
     */
    // FIXME 有待优化，title逻辑有点乱
    private String title;
    private String webTitle;
    private View toolBar;

    private TextView titleView;
    private View backView;
    private View moreView;
    private View noNetView;
    private View loadFailView;
    private WebView webView;
    private ProgressBar progressBar;

    /**
     * 当前页面地址
     */
    private String mCurrentUrl;
    private String pageUrl;

    private String mShareTitle;
    private String mShareUrl;
    private String mShareImageUrl;

    private String adId;
    private ArrayList<String> downloadCompletedUrls;
    private ArrayList<String> asyncDownloadUrls;
    private String function;
    private boolean isAD;

    private boolean isLoadFailed = false;


    private static final int STATUS_LOAD_STARTED = 0;
    private static final int STATUS_LOAD_FINISHED = 1;
    private static final int STATUS_NET_ERR = 2;
    private static final int STATUS_DATA_ERR = 3;

    private static int sLoadStatus = STATUS_LOAD_STARTED;

    private OneKeyShare mOneKeyShare;

    private boolean isKeyDown = false;

    private boolean mIsNewRegisterReceiver = true;
    private String echid;
    private String chid;
    private VodRecord mRecord;
//    private JsBridge mJsBridge;
    /**
     * 网络变化监听
     */
    private BroadcastReceiver mConnectionReceiver = new ConnectionReceiver(this);

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageH5(title);
        } else {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public void dispatch(String type, String url, String category, String errurl, String documentid) {
        runOnUiThread(new ClickRunnable(this, type, url, errurl, documentid, category));

    }

    @Override
    public void dispatch(Map<String, String> paramMaps) {
        runOnUiThread(new BaseLiveActivity.ClickRunnable(paramMaps));

    }

    private static class ConnectionReceiver extends BroadcastReceiver {
        WeakReference<ADActivity> weakReference;

        ConnectionReceiver(ADActivity a) {
            weakReference = new WeakReference<>(a);
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            ADActivity activity = weakReference.get();
            if (activity == null) {
                return;
            }

            if (activity.mIsNewRegisterReceiver) {
                activity.mIsNewRegisterReceiver = false;
                return;
            }
            if (NetUtils.isNetAvailable(IfengApplication.getInstance())) {//从无网到有网的时候,将重新请求
                activity.refresh();
            } else {
                if (activity.noNetView.getVisibility() != View.VISIBLE) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                }
            }
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d("jsbridge", "ADLive");

            switch (msg.what) {
                case OPEN_INPUT_LAYOUT:
                    showEditCommentWindow("");
                    break;
                case OPEN_SHARE_POP:
                    Bundle bundle = msg.getData();
                    String shareUrl = bundle.getString("shareUrl");
                    String title = bundle.getString("title");
                    String desc = bundle.getString("desc");
                    String thumbnail = bundle.getString("thunbnail");
                    String documentId = bundle.getString("documentid");
                    showSharePop(shareUrl, title, desc, webView, thumbnail, documentId);
                    break;
                case HIDE_TITLE_BAR:
                    if (!isFinishing() && null != toolBar) {
                        toolBar.setVisibility(View.GONE);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ad_activity);
        checkUrl(getIntent());
        initData(getIntent());
        //统计V中增加h5类型的播放
        initRecord();
        initViews();
        initWebView();
        registerNet();
    }

    private void initRecord() {
        boolean useIJKPlayer = ChoosePlayerUtils.useIJKPlayer(this);
        mRecord = new VodRecord(adId, title, chid, echid, "", "", "", VodRecord.P_TYPE_H5, "", "", mRecord, "", useIJKPlayer, "", "");
        mRecord.startPlayTime();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkUrl(intent);
        initData(intent);

        if (webView != null) {
            webView.loadUrl(pageUrl);
        } else {
            initWebView();
        }
    }

    private void registerNet() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mConnectionReceiver, intentFilter);
    }

    private void initData(Intent intent) {
        sLoadStatus = STATUS_LOAD_STARTED;
        if (intent == null) {
            return;
        }
        if (isAD) {
            title = intent.getStringExtra(KEY_AD_TITLE);
            //分享标题默认为：凤凰视频客户端，webView标题默认为：推广
            if (TextUtils.isEmpty(title) || "null".equalsIgnoreCase(title)) {
                title = getResources().getString(R.string.app_client);
            }
            mShareImageUrl = intent.getStringExtra(KEY_AD_IMAGE);
            if (TextUtils.isEmpty(mShareImageUrl)) {
                mShareImageUrl = getResources().getString(R.string.share_default_image);
            }
        } else {
            title = getResources().getString(R.string.app_client);
            mShareImageUrl = getResources().getString(R.string.share_default_image);
        }
        webTitle = title;
        echid = intent.getStringExtra(KEY_ECHID);
        chid = intent.getStringExtra(KEY_CHID);
        function = intent.getStringExtra(KEY_FUNCTION);
        adId = intent.getStringExtra(KEY_AD_ID);
        downloadCompletedUrls = intent.getStringArrayListExtra(KEY_AD_DOWNLOAD_COMPLETED_URL);
        asyncDownloadUrls = intent.getStringArrayListExtra(KEY_AD_DOWNLOAD_START_URL);
        mShareTitle = intent.getStringExtra(KEY_AD_SHARE_TITLE);
    }

    private void checkUrl(Intent intent) {
        pageUrl = getPageUrl(intent);
        if (TextUtils.isEmpty(pageUrl)) {
            finish();
        }
        mShareUrl = getShareUrl(intent);
    }

    private String getPageUrl(Intent intent) {
        isAD = intent.getBooleanExtra(KEY_AD_FLAG, false);
        if (isAD) {
            return intent.getStringExtra(KEY_AD_CLICK_URL);
        } else {
            return AD_APP_URL;
        }
    }

    private String getShareUrl(Intent intent) {
        String url = intent.getStringExtra(KEY_AD_SHARE_URL);
        return TextUtils.isEmpty(url) ? pageUrl : url;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        webView = (WebView) findViewById(R.id.web_page_view);
        WebSettings settings = webView.getSettings();
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setSupportZoom(false);
        settings.setTextZoom(100);
        settings.setBuiltInZoomControls(true);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setBlockNetworkImage(false);
        mJsBridge = new JsBridge(this, webView);
        webView.addJavascriptInterface(mJsBridge, "JsBridge");
        webView.addJavascriptInterface(new DuibaJSInterface(), "duiba");
        webView.addJavascriptInterface(mJsBridge, "grounds");
        mJsBridge.setOutHandler(handler);
        mJsBridge.setDispatchListener(this);

        // 支持localStorage
        settings.setAllowFileAccess(true);
        settings.setDomStorageEnabled(true);
        settings.setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        settings.setAppCacheEnabled(true);

        webView.setWebViewClient(new ADWebViewClient(this));
        webView.setWebChromeClient(new MWebChromeClient(this));
        webView.loadUrl(pageUrl);

        WebViewDownLoadListener webViewDownLoadListener = new WebViewDownLoadListener(ADActivity.this);
        webViewDownLoadListener.setAdId(adId);
        webViewDownLoadListener.setDownloadCompletedUrls(downloadCompletedUrls);
        webViewDownLoadListener.setAsyncDownloadUrls(asyncDownloadUrls);
        webView.setDownloadListener(webViewDownLoadListener);
    }


    private void initViews() {
        mOneKeyShare = new OneKeyShare(this);
        mOneKeyShare.setShareType(Platform.SHARE_WEBPAGE);
        mOneKeyShare.notifyShareCallback = this;
        mOneKeyShare.initShareStatisticsData(adId, echid, chid, "", PageIdConstants.H5);
        mOneKeyShare.initSmartShareData(UserOperatorConst.TYPE_WEB, "", webTitle);
        titleView = (TextView) findViewById(R.id.tool_bar_title);
        titleView.setText(webTitle);
        toolBar = findViewById(R.id.tool_bar_view_id);

        noNetView = findViewById(R.id.net_err);
        noNetView.setVisibility(View.GONE);
        noNetView.setOnClickListener(this);

        loadFailView = findViewById(R.id.data_err);
        loadFailView.setVisibility(View.GONE);
        loadFailView.setOnClickListener(this);

        backView = findViewById(R.id.tool_bar_btn_left);
        backView.setOnClickListener(this);

        moreView = findViewById(R.id.more_btn);
        moreView.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.web_page_progress);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tool_bar_btn_left://返回
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.H5);
                finish();
                break;
            case R.id.more_btn://更多
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_MORE, PageIdConstants.H5);
                OneKeyShareContainer.oneKeyShare = mOneKeyShare;
                mOneKeyShare.shareAd(mShareUrl, TextUtils.isEmpty(mShareTitle) ? title : mShareTitle, mShareImageUrl, this);
                if (FUNCTION_VALUE_HIDE_COPY_BROWSER.equals(function)) {
                    mOneKeyShare.getADMorePopWindow().hideCopyAndBrowser();
                }
                mOneKeyShare.showOrDismissADPopupWindow(moreView);
                break;
            case R.id.net_err://无网络View
            case R.id.data_err://加载失败View
                refresh();
                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        intent.putExtra("orderStatus", SharePreUtils.getInstance().getMobileOrderStatus());
        setResult(220, intent);
        super.finish();
    }

    //处理返回键
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mOneKeyShare != null && mOneKeyShare.getPop() != null && mOneKeyShare.getPop().isShowing()) {
                mOneKeyShare.getPop().dismiss();
                return true;
            }
            // if (webView != null && webView.canGoBack()) {
            //     webView.goBack();
            // } else {
            if (isKeyDown) {
                if (!ScreenUtils.isLand()) {
                    finish();
                } else {
                    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
            // }
            isKeyDown = false;
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        isKeyDown = true;
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void refresh() {
        if (webView == null || pageUrl == null) {
            return;
        }
        sLoadStatus = STATUS_LOAD_STARTED;
        webView.loadUrl(pageUrl);
    }

    private static class MWebChromeClient extends WebChromeClient {

        WeakReference<ADActivity> weakReference;

        MWebChromeClient(ADActivity a) {
            weakReference = new WeakReference<>(a);
        }

        @Override
        public void onProgressChanged(WebView view, int progress) {
            ADActivity adActivity = weakReference.get();
            if (adActivity == null) {
                return;
            }
            adActivity.progressBar.setProgress(progress);
        }
    }

    private static class ADWebViewClient extends BaseWebViewClient {
        WeakReference<ADActivity> weakReference;

        ADWebViewClient(ADActivity a) {
            weakReference = new WeakReference<>(a);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            logger.debug("webView shouldOverrideUrlLoading url: {}", url);
            ADActivity adActivity = weakReference.get();
            if (adActivity == null) {
                return false;
            }
            adActivity.mCurrentUrl = url;

            boolean isVideoScheme = url.startsWith(IfengType.TYPE_H5_HIGHCASE) || url.startsWith(IfengType.TYPE_H5_LOWCASE);
            if (isVideoScheme) {
                return true;
            }
            boolean isNewsScheme = url.startsWith("comifengnewsclient");
            if (isNewsScheme) {
                if (PackageUtils.hasInstalled(adActivity, PackageUtils.NEWS_PACKAGE_NAME)) {
                    return false;
                }
                return true;
            }
            //http://v.ifeng.com/?wabp_result=000
            getOrderStatus(url);
            if (url.contains("v.ifeng.com")) {
                view.loadUrl(url);
                return false;
            }
            if (url.contains("oauth_verifier")) {
                adActivity.pageUrl = url;
                adActivity.showDialog(-1);
                return true;
            }
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            ADActivity adActivity = weakReference.get();
            if (adActivity == null) {
                return;
            }
            sLoadStatus = STATUS_LOAD_STARTED;
            super.onPageStarted(view, url, favicon);
            setViewsVisibility(adActivity);
            if (FUNCTION_VALUE_AD.equals(adActivity.function)) {
                UserPointManager.addRewards(UserPointManager.PointType.addByOpenAD);
            }
        }

        @Override
        public void onReceivedError(WebView wv, int errorCode, String description, String failingUrl) {
            logger.error("webView onReceivedError ad load filed : {}", failingUrl);
            ADActivity adActivity = weakReference.get();
            if (adActivity == null || failingUrl.startsWith(IfengType.TYPE_H5_HIGHCASE) || failingUrl.startsWith(IfengType.TYPE_H5_LOWCASE)) {
                return;
            }
            if (NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                sLoadStatus = STATUS_DATA_ERR;
            } else {
                sLoadStatus = STATUS_NET_ERR;
            }

          /*  String errorHtml = "<html><body><h1> </h1></body></html>";
            wv.loadData(errorHtml, "text/html", "UTF-8");*/
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            ADActivity adActivity = weakReference.get();
            if (adActivity == null) {
                return;
            }
            // 因为会先走onReceivedError，再走onPageFinished，
            // 所以当不是错误状态时，才改为STATUS_LOAD_FINISHED, 不然不会显示错误状态view
            if (sLoadStatus != STATUS_NET_ERR && sLoadStatus != STATUS_DATA_ERR) {
                sLoadStatus = STATUS_LOAD_FINISHED;
            }
            super.onPageFinished(view, url);
            setViewsVisibility(adActivity);
        }

        private void setViewsVisibility(ADActivity adActivity) {
            if (adActivity == null
                    || adActivity.progressBar == null
                    || adActivity.webView == null
                    || adActivity.noNetView == null
                    || adActivity.loadFailView == null) {
                return;
            }

            switch (sLoadStatus) {
                case STATUS_LOAD_STARTED:
                    adActivity.progressBar.setVisibility(View.VISIBLE);
                    adActivity.webView.setVisibility(View.VISIBLE);
                    adActivity.noNetView.setVisibility(View.GONE);
                    adActivity.loadFailView.setVisibility(View.GONE);
                    break;
                case STATUS_LOAD_FINISHED:
                    adActivity.progressBar.setVisibility(View.GONE);
                    adActivity.webView.setVisibility(View.VISIBLE);
                    adActivity.noNetView.setVisibility(View.GONE);
                    adActivity.loadFailView.setVisibility(View.GONE);
                    break;
                case STATUS_DATA_ERR:
                    adActivity.progressBar.setVisibility(View.GONE);
                    adActivity.webView.setVisibility(View.GONE);
                    adActivity.noNetView.setVisibility(View.GONE);
                    adActivity.loadFailView.setVisibility(View.VISIBLE);
                    break;
                case STATUS_NET_ERR:
                    adActivity.progressBar.setVisibility(View.GONE);
                    adActivity.webView.setVisibility(View.GONE);
                    adActivity.noNetView.setVisibility(View.VISIBLE);
                    adActivity.loadFailView.setVisibility(View.GONE);
                    break;
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        PageActionTracker.enterPage();
//        mJsBridge.onWebViewResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        PageActionTracker.endPageH5(title);
//        mJsBridge.onWebViewPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //为了使WebView退出时音频或视频关闭
        mJsBridge.onWebViewDestroy();
        if (webView != null) {
            webView.removeAllViews();
            webView.destroy();
            webView = null;
        }
        unregisterReceiver(mConnectionReceiver);
        mConnectionReceiver = null;
        mOneKeyShare = null;
        sendRecord();
/*        MobclickAgent.onKillProcess(this);
        android.os.Process.killProcess(android.os.Process.myPid());*/
    }

    private void sendRecord() {
        if (mRecord != null) {
            mRecord.stopPlayTime();
            CustomerStatistics.sendVODRecord(mRecord);
        }
    }

    private static void getOrderStatus(String weburl) {
        if (weburl.contains("wabp_result")) {
            String webpStatus = weburl.substring(weburl.indexOf("=") + 1, weburl.length());
            if ("000".equalsIgnoreCase(webpStatus)) {
                SharePreUtils.getInstance().setMobileOrderStatus(1);
            } else {
                SharePreUtils.getInstance().setMobileOrderStatus(0);
            }
        }
    }

    private boolean initShowToolbarState(String isShow, String url) {
        if (!TextUtils.isEmpty(url)) {
            return true;
        }

        if (TextUtils.isEmpty(isShow) || TextUtils.isEmpty(url)) {
            return true;
        }
        return Boolean.getBoolean(isShow);
    }

    /**
     * 是否兑吧
     */
    private boolean isDuiba() {
        return !TextUtils.isEmpty(mCurrentUrl) && mCurrentUrl.contains("duiba");
    }

    /**
     * 积分兑换  兑吧JS
     */
    private class DuibaJSInterface {

        /**
         * 分享string的组成格式是：分享链接|分享图片链接|标题|副标题
         */
        @JavascriptInterface
        public void getShareInfo(String shareInfo) {
            logger.info("duiba shareInfo = {}", shareInfo);
            if (!TextUtils.isEmpty(shareInfo)) {
                String[] s = shareInfo.split("\\|");
                if (s.length == 4) {
                    mShareUrl = (s[0]);
                    mShareImageUrl = s[1];
                    mShareTitle = (s[2]);
                }
            }
        }
    }

    private LiveCommentUtil mCommentEditFragment;

    public void showEditCommentWindow(String comments) {
        if (mCommentEditFragment == null) {
            mCommentEditFragment = new LiveCommentUtil();
        }

        mCommentEditFragment.setJSBridge(mJsBridge);
        mCommentEditFragment.setShowInputMethod(true);
        mCommentEditFragment.setCommends(comments);
        if (!mCommentEditFragment.isAdded() && !this.isFinishing()) {
            mCommentEditFragment.show(getSupportFragmentManager(), "dialog");

        }
    }

    private void showSharePop(String shareUrl, String title, String desc, View clickView, String thumbnail, String documentid) {
        if (!TextUtils.isEmpty(shareUrl)) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            mOneKeyShare.shareH5Live(shareUrl,
                    clickView,
                    title,
                    this,
                    desc,
                    thumbnail,
                    false);

        }
    }

    public class ClickRunnable implements Runnable {

        Context context;
        String type;
        String url;
        String errUrl;
        String documentId;
        String category;

        String pageRef;
        String pageTag;

        public ClickRunnable(Context context, String type, String url, String errUrl, String documentId, String category) {
            this.context = context;
            this.type = type;
            this.url = url;
            this.errUrl = errUrl == null ? "http:\\www.ifeng.com" : errUrl;
            this.documentId = documentId;
            this.category = category;
        }

        public ClickRunnable(Map<String, String> maps) {

            this.url = maps.get(JsBridge.PARAM_URL);

            this.type = maps.get(JsBridge.PARAM_TYPE);

            this.pageRef = maps.get(JsBridge.PARAM_REF);

            this.pageTag = maps.get(JsBridge.PARAM_TAG);
        }

        @Override
        public void run() {
            if (TextUtils.isEmpty(url)) {
                return;
            }

            boolean isWeb = "web".equals(type);
            if (!isWeb) {
                url = String.format(H5_LIVE_URl, url);
            }
            sendPageInfo();
            mJsBridge.savePageData(true);
            webView.loadUrl(url);

        }

    }

    private static String pageInfo;

    private void sendPageInfo() {
        pageInfo = SharePreUtils.getInstance().getLivePageJson();
        if (!TextUtils.isEmpty(pageInfo)) {
            PageLiveRecord pageLiveRecord = new Gson().fromJson(pageInfo, PageLiveRecord.class);

            long currentTime = System.currentTimeMillis() / 1000;
            long startTime = SharePreUtils.getInstance().getLivePageStartTime();
            long durTime = currentTime - startTime;

            CommonStatictisListUtils.getInstance().sendPageLive(new PageLiveRecord(pageLiveRecord.getId(), pageLiveRecord.ref, pageLiveRecord.type, durTime + ""));
            SharePreUtils.getInstance().setLivePageStartTime(System.currentTimeMillis() / 1000);
            SharePreUtils.getInstance().setLivePageJson("");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USER_LOGIN_REQUESTCODE) {
            if (resultCode == RESULT_OK) {
                if (webView != null) {
                    setToken(pageUrl);
                    webView.loadUrl(pageUrl);
                } else {
                    initWebView(webView);
                }
            }
        }
    }

}
