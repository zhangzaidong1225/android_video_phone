package com.ifeng.newvideo.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.badge.BadgeUtils;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.P2PStatisticRatioUtils;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.utils.IPushUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.ShortCutUtil;
import com.ifeng.newvideo.utils.TimeUtils;
import com.ifeng.video.core.net.RequestString;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ADInfoDAO;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.ConfigDao;
import com.ifeng.video.dao.db.dao.LaunchAppDAO;
import com.ifeng.video.dao.db.dao.MobileDao;
import com.ifeng.video.dao.db.dao.StatisticDAO;
import com.ifeng.video.dao.db.model.ADInfoModel;
import com.ifeng.video.dao.db.model.ConfigModel;
import com.ifeng.video.dao.db.model.LaunchAppModel;
import com.ifeng.video.dao.db.model.MobileUserModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.db.model.StatisticModel;
import imageload.ImageLoaderManager;
import imageload.ImageLoaderOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 启动闪屏页
 * Created by antboyqi on 14-7-21.
 */
public class ActivitySplash extends BaseFragmentActivity implements View.OnClickListener {
    private static final Logger logger = LoggerFactory.getLogger(ActivitySplash.class);

    private final String SPLASH_AD_POSITION_ID = "10000315";
    private View mLayout;// 广告整体父view
    private ImageView mAdImageView;// 广告图片view
    private View mAdTag;// 广告标签view
    private TextView mCountDownTextView; // 广告倒计时
    private View mAdSkipView; // 广告跳过

    private Bitmap mAdBitmap = null;
    private BitmapDrawable mAdDrawable = null;
    private ImageLoader mAdImageLoader;
    /**
     * 倒计时开始数字
     */
    private int mCountDownNum = 3;

    /**
     * 启动页白屏显示时间 单位ms
     */
    private static final int SPLASH_HOLD_TIME = 500;
    /**
     * 广告倒计间隔 单位ms
     */
    private static final int AD_COUNTDOWN_INTERVAL = 1000;

    private StatisticDAO mStatisticDAO;

    private ADInfoModel mAdInfoModel;
    private String mAdImageUrl = null;
    private boolean mIsAdShowing = false;//广告图是否展示


    /**
     * 作用同activity的isFinish，用于控制由于有handler的延迟操作，可能会有一些界面反复出现
     */
    private boolean isQuit;

    private String mCurrentNetState;

    private static final int MSG_LEAST_LAZY_LAUNCH = 20;
    private static final int MSG_TO_NEXT_ACTIVITY = 30;
    private static final int MSG_AD_COUNTDOWN = 0x400;
    /**
     * 广告倒计时以及无网Dialog展示逻辑
     * <p/>
     * 1.上次已经加载出广告图，将进行倒计时展示，之后有网进入首页，无网弹出无网Dialog
     * 2.上次未加载出广告图，跳过广告倒计时直接判断网络情况，有网进入首页，无网弹出无网Dialog
     */
    private Handler mLazyLaunchHandler = new LazyHandler(this);

    boolean hasBindInEndService = false;
    private Context mContext;

    private static class LazyHandler extends Handler {
        WeakReference<ActivitySplash> weakReference;

        LazyHandler(ActivitySplash activitySplash) {
            weakReference = new WeakReference<>(activitySplash);
        }

        @Override
        public void handleMessage(Message msg) {
            ActivitySplash activitySplash = weakReference.get();
            if (activitySplash == null) {
                return;
            }

            switch (msg.what) {
                case MSG_LEAST_LAZY_LAUNCH:
                    if (activitySplash.isQuit) {
                        return;
                    }
                    activitySplash.doLeastLazyLaunch();
                    break;

                case MSG_TO_NEXT_ACTIVITY:
                    activitySplash.toNextActivity();
                    break;

                case MSG_AD_COUNTDOWN:
                    if (activitySplash.mCountDownNum > 0) {
                        activitySplash.mAdSkipView.setVisibility(View.VISIBLE);
                        if (!PhoneConfig.isGooglePlay() && activitySplash.mAdInfoModel != null && activitySplash.mAdInfoModel.getShouldShowADTag()) {
                            activitySplash.mAdTag.setVisibility(View.VISIBLE);
                        }
                        activitySplash.mCountDownTextView.setText(activitySplash.mCountDownNum + "s");
                        activitySplash.mCountDownNum--;
                        activitySplash.removeADCountDownMsg();
                        activitySplash.mLazyLaunchHandler.sendEmptyMessageDelayed(MSG_AD_COUNTDOWN, AD_COUNTDOWN_INTERVAL);
                    } else {
                        activitySplash.toNextActivity();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        mContext = this;
        initViews();
        startShortcutTask();
        updateAppStartTime();
        setExitWithToast(true);
        requestData();
        mCurrentNetState = NetUtils.getNetType(getApp());
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerNetBroadcast();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BadgeUtils.clearBadge(this);
        if (mIsAdShowing) {
            sendADCountdownMsg();
        } else {
            setAdImage();
        }
    }

    private void sendADEmptyExpose() {

        String url = DataInterface.getAdEmptyExpose(SPLASH_AD_POSITION_ID, PhoneConfig.softversion);
        Log.d("splash", url + "");

        CommonDao.sendRequest(url, null, new Response.Listener<Object>() {
            @Override
            public void onResponse(Object response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, CommonDao.RESPONSE_TYPE_GET_JSON);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdImageUrl != null) {
            VolleyHelper.getAdRequestImageQueue().cancelAll(mAdImageUrl);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mNetBroadReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAdDrawable != null) {
            mAdDrawable.setCallback(null);
        }
        if (hasBindInEndService) {
            unbindStatisticsInEndService();
        }
        removeADCountDownMsg();
        removeLazyLaunchMsg();
    }

    private void initViews() {
        mAdImageView = (ImageView) findViewById(R.id.iv_ad);
        mLayout = findViewById(R.id.splash_layout);
        mAdTag = findViewById(R.id.tv_ad_tag);
        mLayout.setOnClickListener(this);

        mAdImageLoader = VolleyHelper.getAdImageLoader();
        mCountDownTextView = (TextView) findViewById(R.id.tv_count_down);
        mAdSkipView = findViewById(R.id.tv_ad_skip);

        mAdSkipView.setOnClickListener(this);

        // setLedFont();
    }

    private void setLedFont() {
        try {
            Typeface typeface = Typeface.createFromAsset(getAssets(), "lcdn.ttf");
            mCountDownTextView.setTypeface(typeface);
        } catch (Exception e) {
            logger.error("setLedFont error! {}", e.toString());
        }
    }

    private void registerNetBroadcast() {
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mNetBroadReceiver, intentFilter);
    }

    /**
     * 快捷方式
     */
    private void startShortcutTask() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ShortCutUtil.handleShortCut(new WeakReference<Activity>(ActivitySplash.this).get());
                return null;
            }
        }.execute();
    }

    private void handleByNetState() {
        actionByFineNet();
    }


    private void actionByFineNet() {
        if (mIsAdShowing) {
            sendADCountdownMsg();
        } else {
            setAdImage();
        }
        requestData();
    }

    /**
     * 请求网络数据
     */
    private void requestData() {
        if (!NetUtils.isNetAvailable(getApp())) {
            return;
        }
//        requestMobileFreeVideoEnterUrl();
//        requestMobileFreeVideoCheckUrl();
        requestLaunchApp();
        requestConfig();

    }

    private final NetBroadcastReceiver mNetBroadReceiver = new NetBroadcastReceiver(this);

    static class NetBroadcastReceiver extends BroadcastReceiver {
        WeakReference<ActivitySplash> weakReference;

        NetBroadcastReceiver(ActivitySplash activitySplash) {
            weakReference = new WeakReference<>(activitySplash);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            ActivitySplash activitySplash = weakReference.get();
            if (activitySplash == null) {
                return;
            }

            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                if (NetUtils.getNetType(IfengApplication.getInstance()).equals(activitySplash.mCurrentNetState)) {
                    return;
                }
                activitySplash.mCurrentNetState = NetUtils.getNetType(context);
                logger.debug("!!!!! BroadcastReceiver  = handleByNetState()");
                activitySplash.handleByNetState();
            }
        }
    }

    ;

    @Override
    public void onClick(View v) {
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.splash_layout:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SPLASH, PageIdConstants.PAGE_SPLASH);
                finish();
                toADActivity();
                break;

            case R.id.tv_ad_skip:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SPLASH_JUMP_AD, PageIdConstants.PAGE_SPLASH);
                toNextActivity();
                break;
        }
    }

    private void toADActivity() {
        IntentUtils.startADActivityFromSplash(this, mAdInfoModel);
    }

    private void sendADCountdownMsg() {
        if (mLazyLaunchHandler != null) {
            mLazyLaunchHandler.removeMessages(MSG_AD_COUNTDOWN);
            mLazyLaunchHandler.sendEmptyMessage(MSG_AD_COUNTDOWN);
        }
    }

    private void removeADCountDownMsg() {
        if (mLazyLaunchHandler != null) {
            mLazyLaunchHandler.removeMessages(MSG_AD_COUNTDOWN);
        }
    }

    private void sendLazyLaunchMsg() {
        if (mLazyLaunchHandler != null) {
            mLazyLaunchHandler.removeMessages(MSG_LEAST_LAZY_LAUNCH);
            mLazyLaunchHandler.sendEmptyMessageDelayed(MSG_LEAST_LAZY_LAUNCH, SPLASH_HOLD_TIME);
        }
    }

    private void removeLazyLaunchMsg() {
        if (mLazyLaunchHandler != null) {
            mLazyLaunchHandler.removeMessages(MSG_LEAST_LAZY_LAUNCH);
        }
    }

    /**
     * 请求启动接口，详见接口文档2.6.2
     */
    private void requestLaunchApp() {
        LaunchAppDAO.getLaunchApp(
                new Response.Listener<LaunchAppModel>() {
                    @Override
                    public void onResponse(LaunchAppModel launchAppModel) {
                        if (launchAppModel == null) {
                            logger.debug("launchAppModel is null");
                            return;
                        }
                        logger.debug("launchAppModel {}", launchAppModel.toString());
                        IfengApplication.getInstance().setAttribute(LaunchAppModel.LAUNCH_APP_MODEL, launchAppModel);
                        final SharePreUtils sharePreUtils = SharePreUtils.getInstance();
                        sharePreUtils.setServerDate(launchAppModel.getDate());
                        try {
                            sharePreUtils.setInreview(launchAppModel.getInreview());
                            initChannelRedPoint(sharePreUtils, launchAppModel);

                            Long serverTime = Long.valueOf(launchAppModel.getSystemTime());
                            sharePreUtils.setServerTime(serverTime);
                            TimeUtils.setTimeDiff(launchAppModel.getSystemTime());

                            Long timeDiff = serverTime - System.currentTimeMillis();
                            IfengApplication.getInstance().setAttribute(IntentKey.LIVE_SERVICE_TIME, timeDiff);
                            LiveUtils.setLiveTimeDiffIsSuccess(true);

                        } catch (Exception e) {
                            LiveUtils.setLiveTimeDiffIsSuccess(false);
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setAdImage();
                        LiveUtils.setLiveTimeDiffIsSuccess(false);
                    }
                });
    }

    private void requestMobileFreeVideoEnterUrl() {
        MobileDao.requestMobileFreeVideoEnterUrl(PhoneConfig.userKey, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (response == null) {
                    logger.debug("mobileEnterUrl is null");
                    return;
                }
                com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(response.toString());
                String url = jsonObject.getString("resultUrl");
                logger.debug("mobileEnterUrl:{}", response.toString());
                SharePreUtils.getInstance().setMobileEnterUrl(url);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("mobileEnterUrl: {}", error.getMessage());
            }
        });
    }

    private void requestMobileFreeVideoCheckUrl() {
        MobileDao.requestMobileFreeVideoCheckUrl(PhoneConfig.userKey, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (response == null) {
                    logger.debug("mobileCheckUrl is null");
                    return;
                }
                com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(response.toString());
                String url = jsonObject.getString("resultUrl");
                logger.debug("mobileCheckUrl: {}", response.toString());
                SharePreUtils.getInstance().setMobileCheckUrl(url);
                requestMobilePcid();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("mobileCheckUrlError:{} ", error.getMessage());
            }
        });
    }

    private void requestMobilePcid() {
        MobileDao.requestMobielPcid(SharePreUtils.getInstance().getMobileCheckUrl(),
                MobileUserModel.class, new Response.Listener<MobileUserModel>() {
                    @Override
                    public void onResponse(MobileUserModel response) {
                        if (response == null) {
                            logger.debug("mobileUserModel is null");
                            return;
                        }
                        logger.debug("mobileUserModel: {}", response.toString());
                        String statusCode = response.getResultcode();
                        if ("0".equals(statusCode)) {
                            String pcid = response.getPcId();
                            SharePreUtils.getInstance().setMobilePcid(pcid);
                            requestMobileOrderStatus();
                        } else {
                            SharePreUtils.getInstance().setMobilePcid("");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("mobileUserModelError: {}", error.getMessage());
                    }
                });
    }

    private void requestMobileOrderStatus() {
        MobileDao.requestMobielOrderStatus(PhoneConfig.userKey,
                SharePreUtils.getInstance().getMobilePcid(),
                MobileUserModel.MobileOrder.class,
                new Response.Listener<MobileUserModel.MobileOrder>() {
                    @Override
                    public void onResponse(MobileUserModel.MobileOrder response) {
                        if (response == null) {
                            logger.debug("mobileOrder is null");
                            return;
                        }
                        logger.debug("mobileOrder:{}", response.toString());
                        int resultStatus = response.getResult();
                        SharePreUtils.getInstance().setMobileOrderStatus(resultStatus);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("mobileOrderError:{} ", error.getMessage());
                    }
                });
    }

    private void initChannelRedPoint(SharePreUtils sharePreUtils, LaunchAppModel model) {
        if (model == null) {
            return;
        }
        String local = sharePreUtils.getChannelUpdate();
        if (local == null && "1".equals(model.getChannelStatus())) {
            sharePreUtils.setRedPointShow(true);
        } else if (local != null && !local.equals(model.getChannelUpdate()) && "1".equals(model.getChannelStatus())) {
            sharePreUtils.setRedPointShow(true);
        }
    }

    /**
     * 请求配置接口，详见接口文档2.6.4
     */
    private void requestConfig() {
        ConfigDao.getConfigInfo(
                new Response.Listener<ConfigModel>() {
                    @Override
                    public void onResponse(ConfigModel configModel) {
                        logger.debug("requestConfig configModel {}", configModel == null ? "null" : configModel.toString());
                        if (configModel == null || configModel.getFunList() == null) {
                            return;
                        }
                        try {
                            ConfigModel.Fun errStaRatio = null;
                            ConfigModel.Fun bitrate = null;
                            ConfigModel.Fun pushTagSP = null;
                            ConfigModel.Fun pushTagPSP = null;
                            ConfigModel.Fun p2pStatisticsRatio = null;
                            ConfigModel.Fun claritySFun = null;
                            ConfigModel.Fun player = null;
                            ConfigModel.Fun installNewsAppDelay = null;
                            ConfigModel.Fun newsStatisticsSend = null;
                            ConfigModel.Fun adPlayTimes = null;
                            ConfigModel.Fun newsSilenceInstall = null;
                            for (ConfigModel.Fun fun : configModel.getFunList()) {
                                if ("errStatisticsRatio".equalsIgnoreCase(fun.getName())) {
                                    errStaRatio = fun;
                                } else if ("bitrate".equalsIgnoreCase(fun.getName())) {
                                    bitrate = fun;
                                } else if ("pushTagSPNo".equalsIgnoreCase(fun.getName())) {
                                    pushTagSP = fun;
                                } else if ("pushTagPSPNo".equalsIgnoreCase(fun.getName())) {
                                    pushTagPSP = fun;
                                } else if ("p2pStatisticsRatio".equalsIgnoreCase(fun.getName())) {
                                    p2pStatisticsRatio = fun;
                                } else if ("clarityS".equalsIgnoreCase(fun.getName())) {
                                    claritySFun = fun;
                                } else if ("player".equalsIgnoreCase(fun.getName())) {
                                    player = fun;
                                } else if ("newsAd".equalsIgnoreCase(fun.getName())) {
                                    installNewsAppDelay = fun;
                                } else if ("newsStatistics".equals(fun.getName())) {
                                    newsStatisticsSend = fun;
                                } else if ("adPlayTimes".equals(fun.getName())) {
                                    adPlayTimes = fun;
                                } else if ("newsInstall".equals(fun.getName())) {
                                    newsSilenceInstall = fun;
                                }
                            }
                            final SharePreUtils sharePreUtils = SharePreUtils.getInstance();
                            // 设置点播默认播放清晰度
                            int currentStream = ConfigDao.DEFAULT_BITRATE;
                            String loginTime = PhoneConfig.getLoginTimeForStatistic();
                            if (!sharePreUtils.hasUserSelectedVodStream() && bitrate != null) {
                                currentStream = ConfigDao.getPlayDefaultBitrate(bitrate, PhoneConfig.publishid, PhoneConfig.softversion, loginTime);
                                sharePreUtils.setVodCurrentStream(currentStream);
                            }

                            // 设置err采样率
                            int errStatisticRatio = ConfigDao.DEFAULT_ERR_STATISTICS_RATIO;
                            if (errStaRatio != null) {
                                errStatisticRatio = ConfigDao.getErrStaRatio(errStaRatio, PhoneConfig.publishid, PhoneConfig.softversion, loginTime);
                                sharePreUtils.setErrStatisticRatio(errStatisticRatio);
                            }
                            logger.debug("requestConfig onResponse errStaRatio {}, bitrate currentStream {}", errStatisticRatio, currentStream);

                            // 设置播放器
                            int defaultPlayer = ConfigDao.DEFAULT_PLAYER_IJK;
                            if (player != null) {
                                defaultPlayer = ConfigDao.getFunCode(player, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, ConfigDao.DEFAULT_PLAYER_IJK);
                            }
                            boolean shouldUseIJKPlayer = defaultPlayer != ConfigDao.DEFAULT_PLAYER_IFENG;
                            sharePreUtils.setShouldUseIJKPlayer(shouldUseIJKPlayer);
                            logger.debug("requestConfig onResponse player ijk={} ", shouldUseIJKPlayer);

                            // 设置自有播放器 点播超清默认播放的接口地址取值
                            int clarityS = ConfigDao.DEFAULT_CLARITY_S_H264;
                            if (claritySFun != null) {
                                clarityS = ConfigDao.getFunCode(claritySFun, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, ConfigDao.DEFAULT_CLARITY_S_H264);
                                sharePreUtils.setClarityS(clarityS);
                                PlayerInfoModel.CLARITY_S = clarityS;
                            }
                            logger.debug("requestConfig onResponse clarityS {} ", clarityS);

                            // 设置返回提示安装新闻客户端的开关和delay提示时间
                            int appDaysLater = -1;
                            if (installNewsAppDelay != null) {
                                appDaysLater = ConfigDao.getFunCode(installNewsAppDelay, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, -1);
                                sharePreUtils.setInstallNewsAppDelay(appDaysLater);
                            }
                            logger.debug("requestConfig onResponse installNewsAppDelay {} ", appDaysLater);

                            // 设置新闻客户端统计的开关
                            if (newsStatisticsSend != null) {
                                int funCode = ConfigDao.getFunCode(newsStatisticsSend, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, -1);
                                sharePreUtils.setNewsStatisticsSend(funCode);
                            }
                            bindStatisticInEndService();
                            hasBindInEndService = true;
                            // 设置PushTag
                            IPushUtils.setPushTagNo(pushTagSP, pushTagPSP);
                            IPushUtils.setIPushTag(true);

                            // 设置P2P采样率
                            P2PStatisticRatioUtils.setP2PConfig(p2pStatisticsRatio);

                            // 当天贴片广告播放数
                            if (adPlayTimes != null) {
                                // 要重新设置次数的情况：第一次打开app  || 不是同一天内
                                boolean shouldSet = sharePreUtils.getStartTime() == 1 || !TimeUtils.isSameDay();
                                if (shouldSet) {
                                    int funCode = ConfigDao.getFunCode(adPlayTimes, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, -1);
                                    sharePreUtils.setAdPlayTimes(funCode);
                                }
                            }

                            // 静默下载、安装
                            if (newsSilenceInstall != null) {
                                boolean isNewsInstallInReview = "1".equalsIgnoreCase(newsSilenceInstall.getFunCode());
                                boolean isSpecialChannelId = !TextUtils.isEmpty(newsSilenceInstall.getChannelId()) &&
                                        newsSilenceInstall.getChannelId().contains(PhoneConfig.publishid);

                                sharePreUtils.setNewsSilenceInstallInreview(isNewsInstallInReview);
                                sharePreUtils.setIsSpecialChannelId(isSpecialChannelId);
                            }
                        } catch (NumberFormatException e) {
                            logger.error("requestConfig  onResponse err! {}", e);
                        }
                    }
                },
                new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("requestConfig onErrorResponse err! {}", error);
                    }
                }

        );
    }


    private void toNextActivity() {
        if (isQuit) {
            return;
        }
        if (false) {
            // if (Util4act.shouldShowGuideByVersion(getApp())) {
            IntentUtils.launchActByAction(this, IntentUtils.ACTION_SPLASHGUIDE2MAINTAB, ActivityNewVerGuide.class);
            mSharePreUtils.setHomeGuideGestureState(true);
        } else {
            IntentUtils.startMainTabActivity(this);
        }
        isQuit = true;
        finish();
    }

    /**
     * 显示广告
     */
    private void doLeastLazyLaunch() {
        if (mAdBitmap == null || TextUtils.isEmpty(mAdImageUrl)) {
            toNextActivity();
            return;
        }
        requestADImpressionUrl();
        mIsAdShowing = true;
        mAdDrawable = new BitmapDrawable(mAdBitmap);
        if (!PhoneConfig.isGooglePlay()) {
            boolean isGif = mAdImageUrl.contains(".gif");
            if (isGif) {
                ImageLoaderOptions options = new ImageLoaderOptions.Builder(mAdImageView, mAdImageUrl).asGif(true).isBackground(true).build();
                ImageLoaderManager.getInstance().showImage(options);
            } else {
                mAdImageView.setBackgroundDrawable(mAdDrawable);
            }
        }
        mAdImageView.setVisibility(View.VISIBLE);

        sendADCountdownMsg();
    }

    /**
     * 设置广告图
     */
    private void setAdImage() {
        mStatisticDAO = new StatisticDAO(IfengApplication.getInstance());
        if (getRandomAdImage()) {
            logger.debug("getCoverStory getRandomAdImage success! {}", mAdImageLoader.toString());
            mAdImageUrl = mAdInfoModel.getImage();
        }
        if (!TextUtils.isEmpty(mAdImageUrl)) {
            getADBitmap();
        } else {
            if (AdTools.splashADIsEmpty(mAdInfoModel)) {
                sendADEmptyExpose();
            }
            mLazyLaunchHandler.removeMessages(MSG_TO_NEXT_ACTIVITY);
            mLazyLaunchHandler.sendEmptyMessageDelayed(MSG_TO_NEXT_ACTIVITY, SPLASH_HOLD_TIME);
        }
    }

    private void getADBitmap() {
        mAdImageLoader.get(mAdImageUrl, new ImageLoader.ImageListener() {

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    mAdBitmap = response.getBitmap();
                }
                sendLazyLaunchMsg();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                mAdBitmap = null;
            }
        });
    }

    /**
     * 请求广告曝光地址，在加载广告图的时候去请求即可，不需要处理返回结果
     */
    private void requestADImpressionUrl() {
        // 技术部自己的广告曝光统计
        ADRecord.addAdShow(mAdInfoModel.getADId(), ADRecord.AdRecordModel.ADTYPE_INFO);

        String impression = mAdInfoModel.getImpressions();
        String[] impressionArray = new String[0];
        if (!StringUtils.isBlank(impression)) {
            impressionArray = impression.split(";");
        }
        ArrayList<String> impressUrls = new ArrayList<>();
        impressUrls.addAll(Arrays.asList(impressionArray));

        boolean isFromAd = ADInfoModel.LEVEL_1.equals(mAdInfoModel.getLevel());
        if (isFromAd) { // 广告部曝光，即使puvrl为空也发，广告sdk有个默认曝光
            AdvertExposureDao.sendAdvertClickReq(mAdInfoModel.getADId(), impressUrls);
        } else { // 视频部广告曝光
            for (final String impressUrl : impressionArray) {
                RequestString request = new RequestString(Request.Method.GET, impressUrl, null,
                        new LoadImpSuccessListener(impressUrl),
                        new LoadImpErrorListener(impressUrl));
                VolleyHelper.getRequestQueue().add(request);
            }
        }
    }

    /**
     * 随机广告的对象是否成功
     */
    private boolean getRandomAdImage() {
        ADInfoDAO adDao = ADInfoDAO.getInstance(getApp());
        if (!adDao.isExist()) {
            return false;
        }
        mAdInfoModel = adDao.getRandomAd();
        return mAdInfoModel != null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // 拦截从左往右滑动时，走基类方法退出
        return true;
    }

    private static class LoadImpSuccessListener implements Response.Listener<String> {
        private final String impressUrl;

        public LoadImpSuccessListener(String url) {
            impressUrl = url;
        }

        @Override
        public void onResponse(String response) {
            logger.debug("loadImpressionUrl::{} success!!", impressUrl);
        }
    }

    private class LoadImpErrorListener implements Response.ErrorListener {
        private final String impressUrl;

        public LoadImpErrorListener(String url) {
            impressUrl = url;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            logger.debug("loadImpressionUrl::{} fail!!", impressUrl);
            //如果发送曝光失败，则保存到统计数据库中以便下次发送统计时一起发送曝光
            StatisticModel model = new StatisticModel();
            model.setPostUrl(impressUrl);
            mStatisticDAO.saveStatisticData(model);
        }

    }

    /**
     * 记录应用打开次数，视频播放次数
     */
    private void updateAppStartTime() {
        int appStartTime = mSharePreUtils.getStartTime();
        if (appStartTime < 3) {
            appStartTime++;
        } else { // put a integer value much bigger than 3
            appStartTime = 100;
        }
        mSharePreUtils.setStartTime(appStartTime);
        // 用于播放页面展示新手提示层
        IfengApplication.getInstance().setAttribute(IntentKey.VIDEO_START_TIME, appStartTime <= 3 ? 0 : -1);
    }
}
