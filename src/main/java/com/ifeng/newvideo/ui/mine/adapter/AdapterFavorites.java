package com.ifeng.newvideo.ui.mine.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.FavoritesModel;
import com.ifeng.video.dao.db.model.HistoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AdapterFavorites extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(AdapterFavorites.class);

    private Context context;
    private LayoutInflater inflate;
    private boolean isInEditMode;
    private Set<String> selectedIds = new HashSet<>();
    private List<FavoritesModel> list = new ArrayList<>();

    private static final int TYPE_VIDEO = 0;//视频
    private static final int TYPE_AUDIO = 1;//音频

    public AdapterFavorites(Context context) {
        this.context = context;
        inflate = LayoutInflater.from(context);
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getResource().equals(HistoryModel.RESOURCE_VIDEO) ? TYPE_VIDEO : TYPE_AUDIO;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        OnClick onClick = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ViewHolder();
            onClick = new OnClick();
            convertView = inflate.inflate(type == TYPE_VIDEO ? R.layout.history_video_list_item : R.layout.history_audio_list_item, null);
            holder.initView(convertView);
            convertView.setTag(holder);
            convertView.setTag(R.id.item_view, onClick);
        } else {
            holder = (ViewHolder) convertView.getTag();
            onClick = (OnClick) convertView.getTag(R.id.item_view);
        }
        onClick.setPosition(position);
        holder.item_view.setOnClickListener(onClick);
        FavoritesModel program = list.get(position);
        initItemData(holder, program);
        return convertView;
    }

    private void initItemData(ViewHolder holder, FavoritesModel model) {
        holder.itemTitle.setText(TextUtils.isEmpty(model.getName()) ? "" : model.getName());
        holder.itemImg.setImageUrl(model.getImgUrl(), VolleyHelper.getImageLoader());
        holder.itemImg.setDefaultImageResId(R.drawable.bg_default_small);
        holder.itemImg.setErrorImageResId(R.drawable.bg_default_small);
        // holder.itemDes.setText(holder.itemDes.getResources().getString(R.string.play_to)
        //         + StringUtils.changeDuration(model.getBookMark()));
        holder.itemDes.setVisibility(View.GONE);
        holder.itemTime.setText(StringUtils.changeDuration((int) model.getVideoDuration()));
        if (isInEditMode) {// 编辑模式
            holder.itemImgCheck.setVisibility(View.VISIBLE);
            holder.itemDes.setVisibility(View.VISIBLE);
            if (model.getIsChecked() == 0) {
                holder.itemImgCheck.setImageResource(R.drawable.common_edit_on_no);
            } else {
                holder.itemImgCheck.setImageResource(R.drawable.common_edit_on);
            }
        } else {// 非编辑模式
            holder.itemImgCheck.setVisibility(View.GONE);
        }
    }

    public Set<String> getSelectedIds() {
        return selectedIds;
    }

    public void setList(List<FavoritesModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setInEditMode(boolean b) {
        isInEditMode = b;
        notifyDataSetChanged();
    }

    /**
     * 处理选中未选中逻辑
     */
    private void handleCheckLogic(FavoritesModel model) {
        if (model.getIsChecked() == 0) {//未选中置为选中状态
            model.setIsChecked(1);
            selectedIds.add(model.getProgramGuid());
        } else {
            model.setIsChecked(0);//选中状态置为未选中
            selectedIds.remove(model.getProgramGuid());
        }
        notifyDataSetChanged();
        if (checkedNumber != null) {
            checkedNumber.getCheckedNum(selectedIds.size());
        }
    }

    /**
     * 处理跳转逻辑
     */
    private void handleJumpLogic(FavoritesModel program) {
        String proGuid = program.getProgramGuid();
        long bookMark = Math.round(program.getBookMark() / 1000) == program.getVideoDuration() ? 0 : program.getBookMark();
        if (FavoritesModel.RESOURCE_AUDIO.equals(program.getResource())) {//如果是音频
            IntentUtils.toAudioFMActivity(context, proGuid, program.getFmProgramId(), program.getName(),
                    program.getFmType(), program.getFmUrl(), program.getImgUrl(), bookMark, VodRecord.V_TAG_FM_COLLECT, "", (int) program.getVideoDuration(), program.getFileSize());
        } else {//如果是视频
            IntentUtils.toVodDetailActivity(context, proGuid, null, false, false, 0, program.getExtra1());
        }
    }

    static class ViewHolder {
        View item_view;
        ImageView itemImgCheck;//选中、未选中图标
        TitleTextView itemTitle;//视频标题
        TextView itemDes;//观看时长
        NetworkImageView itemImg;//视频封面
        TextView itemTime;//视频时长
        View bottomLine;//底部下划线

        private void initView(View contentView) {
            item_view = contentView.findViewById(R.id.item_view);
            itemImg = (NetworkImageView) contentView.findViewById(R.id.history_item_img);
            itemImgCheck = (ImageView) contentView.findViewById(R.id.check_img);
            itemTime = (TextView) contentView.findViewById(R.id.item_time);
            itemDes = (TextView) contentView.findViewById(R.id.item_des);
            itemTitle = (TitleTextView) contentView.findViewById(R.id.item_title);
            bottomLine = contentView.findViewById(R.id.item_bottom_line);
        }
    }

    class OnClick implements View.OnClickListener {
        int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            FavoritesModel model = list.get(position);
            if (isInEditMode) {
                PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_MY_CHOOSE, getItemViewType(position) == TYPE_VIDEO);
                handleCheckLogic(model);
            } else {
                handleJumpLogic(model);
            }
        }
    }

    private ICheckedNumber checkedNumber;

    public void setCheckedNumber(ICheckedNumber checkedNumber) {
        this.checkedNumber = checkedNumber;
    }
}
