package com.ifeng.newvideo.ui.mine.favorites;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.mine.adapter.AdapterFavorites;
import com.ifeng.newvideo.ui.mine.adapter.ICheckedNumber;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.FavoritesDAO;
import com.ifeng.video.dao.db.model.FavoritesModel;
import com.ifeng.video.dao.db.model.HistoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * favorites modules
 * create by huhailong
 */
public class ActivityFavorites extends BaseFragmentActivity implements OnClickListener, ICheckedNumber {

    private static final Logger logger = LoggerFactory.getLogger(ActivityFavorites.class);

    protected TextView tvEdit;//顶部右侧编辑、完成文字
    private TextView titleVideo;//视频
    private TextView titleAudio;//音频
    private TextView emptyText;
    private ImageView emptyImg;
    private ListView listView;
    private TextView delTextView;
    private View empty_view;//没有数据布局
    private View editorBottom;//底部布局

    private AdapterFavorites adapter;
    private FavoritesDAO favoritesDao;
    private boolean isInEditMode = false;//是否是编辑模式
    private boolean isSelectVideo = true;//选择的是否是视频

    private List<FavoritesModel> favoritesModels = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_activity);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initFavoritesDAO();
        initView();
        initAdapter();
        new LoadDataTask().execute();
        adapter.getSelectedIds().clear();
        getCheckedNum(0);//重新设置选中数量
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_COLLECTED);
                finish();
                break;
            case R.id.title_video:
                if (isInEditMode || isSelectVideo) {
                    return;
                }
                isSelectVideo = true;
                setTitleStyle();
                adapter.getSelectedIds().clear();
                new LoadDataTask().execute();
                PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_TAB_VIDEO, null);
                break;
            case R.id.title_audio:
                if (isInEditMode || !isSelectVideo) {
                    return;
                }
                isSelectVideo = false;
                setTitleStyle();
                adapter.getSelectedIds().clear();
                new LoadDataTask().execute();
                PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_TAB_AUDIO, null);
                break;
            case R.id.title_right_text:
                if (isInEditMode) {
                    PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_MY_EDIT_FINISH, isSelectVideo);
                } else {
                    PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_MY_EDIT, isSelectVideo);
                }
                handleEditMode();
                break;
            case R.id.btn_delete:
                if (adapter.getSelectedIds().size() == 0) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_unchecked_any_video);
                } else {
                    new DeleteSelectedTask().execute(adapter.getSelectedIds().size() == FavoritesDAO.MAX_NUM);
                }
                PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_MY_DELETE, isSelectVideo);
                break;
            case R.id.btn_clear:
                showClearConfirmDialog();
                break;
            default:
                break;
        }
    }

    @Override
    public void getCheckedNum(int number) {
        delTextView.setText(this.getString(R.string.common_delete) + "(" + number + ")");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                new LoadDataTask().execute();
                adapter.getSelectedIds().clear();
                getCheckedNum(0);
                break;
        }
    }

    /**
     * 初始化View
     */
    private void initView() {
        //标题
        findViewById(R.id.back).setOnClickListener(this);
        titleVideo = ((TextView) findViewById(R.id.title_video));
        titleVideo.setText(R.string.setting_video);
        titleVideo.setOnClickListener(this);
        titleAudio = ((TextView) findViewById(R.id.title_audio));
        titleAudio.setText(R.string.setting_FM);
        titleAudio.setOnClickListener(this);
        //标题右侧编辑、完成文字
        tvEdit = (TextView) findViewById(R.id.title_right_text);
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
        tvEdit.setText(getString(R.string.edit));
        tvEdit.setVisibility(View.VISIBLE);
        tvEdit.setOnClickListener(this);
        //底部清空、删除按钮
        editorBottom = findViewById(R.id.editor_bottom_bar);
        delTextView = (TextView) findViewById(R.id.btn_delete);
        delTextView.setOnClickListener(this);
        findViewById(R.id.btn_clear).setOnClickListener(this);
        listView = (ListView) findViewById(R.id.favorites_listview);
        empty_view = findViewById(R.id.empty);
        emptyText = (TextView) findViewById(R.id.empty_first_note);
        emptyImg = (ImageView) findViewById(R.id.empty_img);
        setTitleStyle();
        //默认报一次选中视频
        PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_TAB_VIDEO, null);
    }

    /**
     * 设置标题样式
     */
    private void setTitleStyle() {
        titleVideo.setTextColor(getResources().getColor(isSelectVideo ? R.color.white : R.color.cache_paused_text_color));
        titleVideo.setBackgroundResource(isSelectVideo ? R.color.cache_paused_text_color : R.drawable.common_mine_title_bar_left);
        titleAudio.setTextColor(getResources().getColor(isSelectVideo ? R.color.cache_paused_text_color : R.color.white));
        titleAudio.setBackgroundResource(isSelectVideo ? R.drawable.common_mine_title_bar_right : R.color.cache_paused_text_color);
    }

    private void initAdapter() {
        adapter = new AdapterFavorites(this);
        adapter.setCheckedNumber(this);
        listView.setAdapter(adapter);
    }

    /**
     * 初始化FavoritesDAO
     */
    private void initFavoritesDAO() {
        try {
            favoritesDao = FavoritesDAO.getInstance(this);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * 显示有数据的布局
     */
    public void showNormalLayout(boolean isShow) {
        empty_view.setVisibility(isShow ? View.GONE : View.VISIBLE);
        emptyImg.setImageResource(isSelectVideo ? R.drawable.favorites_empty : R.drawable.favorites_empty);
        emptyText.setText(isSelectVideo ? R.string.favorites_none_first_note_video : R.string.favorites_none_first_note_audio);
        tvEdit.setVisibility(isShow ? View.VISIBLE : View.GONE);
        listView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 处理编辑模式
     */
    private void handleEditMode() {
        if (!isInEditMode) {
            entryEditMode();
        } else {
            exitEditMode();
        }
    }

    /**
     * 进入编辑模式
     */
    private void entryEditMode() {
        adapter.setInEditMode(true);
        editorBottom.setVisibility(View.VISIBLE);
        tvEdit.setText(getString(R.string.finish));
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_edit_text_color));
        isInEditMode = true;
    }

    /**
     * 退出编辑模式
     */
    public void exitEditMode() {
        adapter.setInEditMode(false);
        editorBottom.setVisibility(View.GONE);
        tvEdit.setText(getString(R.string.edit));
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
        isInEditMode = false;
        resetCheckedState();
    }

    private Dialog dialog;

    private void showClearConfirmDialog() {
        Resources res = this.getResources();
        dialog = AlertUtils.getInstance().showTwoBtnDialog(ActivityFavorites.this,
                res.getString(R.string.common_delete_video_confirm_msg),
                res.getString(R.string.common_popup_layout_clear),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PageActionTracker.clickMyList(2, ActionIdConstants.CLICK_MY_CLEAR, isSelectVideo);
                        dismissClearConfirmDialog();
                        new DeleteSelectedTask().execute(true);//清空
                    }
                }, res.getString(R.string.common_cancel),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissClearConfirmDialog();
                    }
                });
    }

    private void dismissClearConfirmDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * 重置选中状态
     */
    private void resetCheckedState() {
        adapter.getSelectedIds().clear();
        getCheckedNum(0);
        for (FavoritesModel model : favoritesModels) {
            model.setIsChecked(0);
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * 加载数据库中数据
     */
    private class LoadDataTask extends AsyncTask<Void, Void, ArrayList<FavoritesModel>> {
        @Override
        protected ArrayList<FavoritesModel> doInBackground(Void... params) {
            ArrayList<FavoritesModel> favList = null;
            try {
                favList = (ArrayList<FavoritesModel>) favoritesDao.getAllFavoritesData(isSelectVideo ? HistoryModel.RESOURCE_VIDEO : HistoryModel.RESOURCE_AUDIO);
            } catch (Exception e) {
                logger.error("LoadDataTask  doInBackground Exception is {}", e);
            }
            return favList;
        }

        @Override
        protected void onPostExecute(ArrayList<FavoritesModel> result) {
            try {
                showNormalLayout(result.size() > 0);
                favoritesModels.clear();
                favoritesModels.addAll(result);
                adapter.setList(favoritesModels);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**
     * 异步删除
     */
    private class DeleteSelectedTask extends AsyncTask<Boolean, Void, Void> {
        private Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(ActivityFavorites.this, R.style.common_Theme_Dialog);
            dialog.setContentView(R.layout.common_progress_layout);
            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText(ActivityFavorites.this.getString(R.string.favorites_wait_delete));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            if (params != null && params[0]) {//全部删除
                try {
                    favoritesDao.deleteAll(isSelectVideo ? HistoryModel.RESOURCE_VIDEO : HistoryModel.RESOURCE_AUDIO);
                } catch (Exception e) {
                    logger.error("Exception is {}", e);
                }
            } else {// 删除部分
                for (String id : adapter.getSelectedIds()) {
                    try {
                        favoritesDao.delete(id, isSelectVideo ? HistoryModel.RESOURCE_VIDEO : HistoryModel.RESOURCE_AUDIO);
                    } catch (Exception e) {
                        logger.error("Exception is {}", e);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                dialog.dismiss();
                exitEditMode();
                new LoadDataTask().execute();
                adapter.getSelectedIds().clear();
                getCheckedNum(0);//设置删除按钮数量为0
            } catch (Exception e) {
                logger.error("Exception is {}", e);
            }
        }
    }
}
