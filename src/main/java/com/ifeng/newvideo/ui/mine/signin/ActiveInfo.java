package com.ifeng.newvideo.ui.mine.signin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**活动详情
 * Created by wangqing1 on 2015/1/27.
 */
public class ActiveInfo implements Serializable{
    private static final Logger logger = LoggerFactory.getLogger(ActiveInfo.class);
    public String introUrl = "http://v.ifeng.com/special/apphezuo/bjmcontent.shtml";//活动详情
    public String shareContent = "看视频易，送流量不易，且签且珍惜。现在动动手指签到即有机会获得免费上网流量包啦~20,000个流量红包等你拿，先到先得乐抢不停！活动页面：http://v.ifeng.com/special/bjmdownload/index.shtml";//分享
    public ActiveInfo(String introUrl,String shareContent){
        this.introUrl = introUrl;
        this.shareContent = shareContent;
    }

    public ActiveInfo(){

    }


}
