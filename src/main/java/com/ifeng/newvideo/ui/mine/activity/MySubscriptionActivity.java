package com.ifeng.newvideo.ui.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.AllWeMediaActivity;
import com.ifeng.newvideo.ui.subscribe.adapter.SubscribeListAdapter;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshGridView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeList;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的订阅
 * Created by Administrator on 2016/7/28.
 */
public class MySubscriptionActivity extends BaseFragmentActivity implements SubscribeListAdapter.ClickListener {
    private static final Logger logger = LoggerFactory.getLogger(MySubscriptionActivity.class);

    private static final Long PAGE_SIZE = 18L;//每页加载18条
    private int currentPage = 1;//当前页码
    private String positionId = "";

    private View loading_layout;//Loading
    private View no_net_layer;//无网
    private View empty_layout;//还没有订阅内容
    private PullToRefreshGridView gridView;
    private SubscribeListAdapter adapter;

    private List<SubscribeList.WeMediaListEntity> subscribeList = new ArrayList<>();//转化后的数据集合

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscription_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initView();
        showStatusLayout(LOADING);
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentPage = 1;
        positionId = "";
        mySubscribeWeMedia();
    }

    @Override
    public void onItemClickListener(SubscribeList.WeMediaListEntity entity) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_SUB_ITEM, PageIdConstants.MY_SUB);
        IntentUtils.startWeMediaHomePageActivity(MySubscriptionActivity.this, entity.getWeMediaID() + "", "");
    }

    @Override
    public void onSubscribeClickListener(SubscribeList.WeMediaListEntity entity) {
        if (!NetUtils.isNetAvailable(MySubscriptionActivity.this)) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        int state = entity.getFollowed();
        state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
        subscribe(entity, getUserId(), state);
        PageActionTracker.clickWeMeidaSub(state == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.MY_SUB);
    }

    private void initView() {
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_SUB);
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(R.string.my_subscribe);
        ((TextView) findViewById(R.id.title_right_text)).setText(R.string.all);
        findViewById(R.id.title_right_text).setVisibility(View.VISIBLE);
        findViewById(R.id.title_right_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_WEMEDIA_ALL, PageIdConstants.MY_SUB);
                Intent intent = new Intent(MySubscriptionActivity.this, AllWeMediaActivity.class);
                intent.putExtra(IntentKey.LOCATE_TO_TV, true);
                startActivity(intent);
            }
        });

        loading_layout = findViewById(R.id.loading_layout);
        no_net_layer = findViewById(R.id.no_net_layer);
        no_net_layer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showStatusLayout(LOADING);
                currentPage = 1;
                positionId = "";
                mySubscribeWeMedia();
            }
        });
        empty_layout = findViewById(R.id.subscribe_all_empty);
        ((TextView) empty_layout.findViewById(R.id.tv_no_subscriptions)).setText(R.string.mine_no_subscription_text);
        gridView = (PullToRefreshGridView) findViewById(R.id.gridView);
        gridView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        gridView.getRefreshableView().setNumColumns(3);//展示3列
        gridView.setShowIndicator(false);
        gridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                mySubscribeWeMedia();
            }
        });
        adapter = new SubscribeListAdapter();
        adapter.setClickListener(this);
        gridView.getRefreshableView().setAdapter(adapter);
    }

    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final SubscribeList.WeMediaListEntity entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(entity.getWeMediaID(), uid, type, System.currentTimeMillis() + "", SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        int followNo = 0;
                        if (TextUtils.isDigitsOnly(entity.getFollowNo())) {
                            followNo = Integer.parseInt(entity.getFollowNo());
                        }
                        boolean isSubscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        entity.setFollowed(type);
                        entity.setFollowNo(String.valueOf(isSubscribe ? (++followNo) : (--followNo)));
                        adapter.notifyDataSetChanged();
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, isSubscribe, entity.getName());
                        ToastUtils.getInstance().showShortToast(isSubscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    private void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(MySubscriptionActivity.this, action, title, "");
    }

    /**
     * 获取我的订阅数据
     */
    private void mySubscribeWeMedia() {
        WeMediaDao.getUserSubscribeWeMediaList(
                new User(MySubscriptionActivity.this).getUid(),
                PAGE_SIZE + "",
                positionId,
                System.currentTimeMillis() + "",
                SubscribeList.class,
                new Response.Listener<SubscribeList>() {
                    @Override
                    public void onResponse(SubscribeList response) {
                        gridView.onRefreshComplete();
                        handleResponseEvent(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        gridView.onRefreshComplete();
                        handleErrorEvent(error);
                    }
                }, false);

        if (!"".equals(positionId)) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.MY_SUB);
        }
    }

    /**
     * 处理响应事件
     */
    private void handleResponseEvent(SubscribeList response) {
        if (response != null && !ListUtils.isEmpty(response.getWeMediaList())) {
            if (currentPage == 1) {
                subscribeList.clear();
            }
            List<SubscribeList.WeMediaListEntity> handleList = handleData(response.getWeMediaList());
            if (handleList.size() > 0) {
                subscribeList.addAll(handleList);
            } else {
                if (currentPage > 1) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                }
            }
            showStatusLayout(NORMAL);
            adapter.setList(subscribeList);
            currentPage++;
        } else {
            if (currentPage == 1) {
                showStatusLayout(NO_DATA);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 处理错误响应
     */
    private void handleErrorEvent(VolleyError error) {
        if (currentPage == 1) {
            if (error != null && error instanceof NetworkError) {
                showStatusLayout(NO_NET);
            } else {
                showStatusLayout(NO_DATA);
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 转化数据
     */
    private List<SubscribeList.WeMediaListEntity> handleData(List<SubscribeList.WeMediaListEntity> weMediaList) {
        if (weMediaList.size() > 0) {
            positionId = weMediaList.get(weMediaList.size() - 1).getWeMediaID();
        }

        List<SubscribeList.WeMediaListEntity> list = new ArrayList<>();
        for (SubscribeList.WeMediaListEntity entity : weMediaList) {
            if (TextUtils.isEmpty(entity.getName())
                    || TextUtils.isEmpty(entity.getWeMediaID())
                    || subscribeList.contains(entity)) {//容错、去重
                continue;
            }
            list.add(entity);
        }
        return list;
    }

    private static final int LOADING = 0;//加载中
    private static final int NORMAL = 1;//正常
    private static final int NO_DATA = 2;//无数据
    private static final int NO_NET = 3;//无网

    /**
     * 显示状态布局
     */
    private void showStatusLayout(int status) {
        switch (status) {
            case LOADING:
                loading_layout.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                empty_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NORMAL:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
                empty_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NO_DATA:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
                empty_layout.setVisibility(View.VISIBLE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NO_NET:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
                empty_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.VISIBLE);
                break;
        }
    }
}
