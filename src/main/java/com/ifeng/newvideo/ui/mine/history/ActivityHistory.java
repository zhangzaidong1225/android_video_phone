package com.ifeng.newvideo.ui.mine.history;


import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.mine.adapter.AdapterHistoryAll;
import com.ifeng.newvideo.ui.mine.adapter.ICheckedNumber;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.model.HistoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * history modules
 * create by huhailong
 */
public class ActivityHistory extends BaseFragmentActivity implements OnClickListener, ICheckedNumber {
    private static final Logger logger = LoggerFactory.getLogger(ActivityHistory.class);

    private TextView tvEdit;//顶部右侧编辑、完成文字
    private TextView titleVideo;//视频
    private TextView titleAudio;//音频
    private TextView emptyText;
    private ImageView emptyImg;
    private ListView listView;
    private View emptyLayout;
    private View editorBottomView;
    private TextView delTextView;
    private AdapterHistoryAll adapter;
    private HistoryDAO historyDao;
    private boolean isEditMode = false;//是否是编辑模式
    private boolean isSelectVideo = true;//选择的是否是视频

    private List<HistoryModel> historyModels = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initHistoryDao();
        initView();
        initAdapter();
        new LoadDataTask().execute();//获取数据
        adapter.getSelectedIds().clear();
        getCheckedNum(0);//重新设置选中数量
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_PLAYED);
                finish();
                break;
            case R.id.title_video:
                if (isEditMode || isSelectVideo) {
                    return;
                }
                isSelectVideo = true;
                setTitleStyle();
                adapter.getSelectedIds().clear();
                new LoadDataTask().execute();
                PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_TAB_VIDEO, null);
                break;
            case R.id.title_audio:
                if (isEditMode || !isSelectVideo) {
                    return;
                }
                isSelectVideo = false;
                setTitleStyle();
                adapter.getSelectedIds().clear();
                new LoadDataTask().execute();
                PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_TAB_AUDIO, null);
                break;
            case R.id.title_right_text:
                if (isEditMode) {
                    PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_MY_EDIT_FINISH, isSelectVideo);
                } else {
                    PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_MY_EDIT, isSelectVideo);
                }
                handleEditMode();
                break;
            case R.id.btn_delete:
                if (adapter.getSelectedIds().size() == 0) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_unchecked_any_video);
                } else {
                    new DeleteSelectedTask().execute(adapter.getSelectedIds().size() == HistoryDAO.MAX_NUM);
                }
                PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_MY_DELETE, isSelectVideo);
                break;
            case R.id.btn_clear:
                showClearConfirmDialog();
                break;
            default:
                break;
        }
    }

    @Override
    public void getCheckedNum(int number) {
        delTextView.setText(this.getString(R.string.common_delete) + "(" + number + ")");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                new LoadDataTask().execute();//获取数据
                adapter.getSelectedIds().clear();
                getCheckedNum(0);//重新设置选中数量
                break;
        }
    }

    /**
     * 初始化View
     */
    private void initView() {
        //标题
        findViewById(R.id.back).setOnClickListener(this);
        titleVideo = ((TextView) findViewById(R.id.title_video));
        titleVideo.setText(R.string.setting_video);
        titleVideo.setOnClickListener(this);
        titleAudio = ((TextView) findViewById(R.id.title_audio));
        titleAudio.setText(R.string.setting_FM);
        titleAudio.setOnClickListener(this);
        //标题右侧编辑、完成文字
        tvEdit = (TextView) findViewById(R.id.title_right_text);
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
        tvEdit.setText(getString(R.string.edit));
        tvEdit.setVisibility(View.VISIBLE);
        tvEdit.setOnClickListener(this);
        //底部清空、删除
        editorBottomView = findViewById(R.id.editor_bottom_bar);
        delTextView = (TextView) findViewById(R.id.btn_delete);
        delTextView.setOnClickListener(this);
        findViewById(R.id.btn_clear).setOnClickListener(this);
        emptyLayout = findViewById(R.id.watch_empty);
        emptyText = (TextView) findViewById(R.id.watch_empty_fisrt_note);
        emptyImg = (ImageView) findViewById(R.id.watch_empty_img);
        listView = (ListView) findViewById(R.id.watch_list);
        setTitleStyle();

        //默认报一次选中视频
        PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_TAB_VIDEO, null);
    }

    /**
     * 设置标题样式
     */
    private void setTitleStyle() {
        titleVideo.setTextColor(getResources().getColor(isSelectVideo ? R.color.white : R.color.cache_paused_text_color));
        titleVideo.setBackgroundResource(isSelectVideo ? R.color.cache_paused_text_color : R.drawable.common_mine_title_bar_left);
        titleAudio.setTextColor(getResources().getColor(isSelectVideo ? R.color.cache_paused_text_color : R.color.white));
        titleAudio.setBackgroundResource(isSelectVideo ? R.drawable.common_mine_title_bar_right : R.color.cache_paused_text_color);
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        adapter = new AdapterHistoryAll(this);
        adapter.setCheckedNumber(this);
        listView.setAdapter(adapter);
    }

    /**
     * 初始化Dao
     */
    private void initHistoryDao() {
        try {
            historyDao = HistoryDAO.getInstance(this);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * 是否显示有数据的布局
     *
     * @param isShow 显示或不显示
     */
    public void showNormalLayout(boolean isShow) {
        emptyLayout.setVisibility(isShow ? View.GONE : View.VISIBLE);
        emptyImg.setImageResource(isSelectVideo ? R.drawable.watched_empty : R.drawable.watched_empty);
        emptyText.setText(isSelectVideo ? R.string.history_none_video : R.string.history_none_audio);
        tvEdit.setVisibility(isShow ? View.VISIBLE : View.GONE);
        listView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 处理编辑、完成逻辑
     */
    private void handleEditMode() {
        if (!isEditMode) {
            entryEditMode();
        } else {
            exitEditMode();
        }
    }

    /**
     * 进入编辑模式
     */
    private void entryEditMode() {
        isEditMode = true;
        adapter.setInEditMode(true);
        editorBottomView.setVisibility(View.VISIBLE);
        tvEdit.setText(getString(R.string.finish));
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_edit_text_color));
    }

    /**
     * 退出编辑模式
     */
    public void exitEditMode() {
        isEditMode = false;
        adapter.setInEditMode(false);
        editorBottomView.setVisibility(View.GONE);
        tvEdit.setText(getString(R.string.edit));
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
        resetCheckedState();
    }

    /**
     * 重置选中状态
     */
    private void resetCheckedState() {
        adapter.getSelectedIds().clear();
        getCheckedNum(0);
        for (HistoryModel model : historyModels) {
            model.setIsChecked(0);
        }
        adapter.notifyDataSetChanged();
    }

    private Dialog dialog;

    /**
     * 弹出是否清空对话框
     */
    private void showClearConfirmDialog() {
        Resources res = this.getResources();
        dialog = AlertUtils.getInstance().showTwoBtnDialog(this,
                res.getString(R.string.common_delete_video_confirm_msg),
                res.getString(R.string.common_popup_layout_clear),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PageActionTracker.clickMyList(3, ActionIdConstants.CLICK_MY_CLEAR, isSelectVideo);
                        dismissClearConfirmDialog();
                        new DeleteSelectedTask().execute(true);//清空
                    }
                },
                res.getString(R.string.common_cancel),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissClearConfirmDialog();
                    }
                });
    }

    /**
     * 关闭对话框
     */
    private void dismissClearConfirmDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * 加载数据库中数据
     */
    private class LoadDataTask extends AsyncTask<Void, Void, ArrayList<HistoryModel>> {

        @Override
        protected ArrayList<HistoryModel> doInBackground(Void... voids) {
            ArrayList<HistoryModel> list = null;
            try {
                list = (ArrayList<HistoryModel>) historyDao.getAllHistoryData(isSelectVideo ? HistoryModel.RESOURCE_VIDEO : HistoryModel.RESOURCE_AUDIO);
            } catch (Exception e) {
                logger.error("Exception is {}", e);
            }
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<HistoryModel> result) {
            try {
                showNormalLayout(result.size() > 0);
                historyModels.clear();
                historyModels.addAll(result);
                adapter.setList(historyModels);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**
     * 删除数据
     */
    private class DeleteSelectedTask extends AsyncTask<Boolean, Void, Void> {
        private Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(ActivityHistory.this, R.style.common_Theme_Dialog);
            dialog.setContentView(R.layout.common_progress_layout);
            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText(ActivityHistory.this.getString(R.string.history_wait_delete));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            // 全部删除
            if (params != null && params[0]) {
                try {
                    historyDao.deleteAll(isSelectVideo ? HistoryModel.RESOURCE_VIDEO : HistoryModel.RESOURCE_AUDIO);
                } catch (Exception e) {
                    logger.error("Exception is {}", e);
                }
            } else { // 删除部分
                for (String id : adapter.getSelectedIds()) {
                    try {
                        historyDao.delete(id);
                    } catch (Exception e) {
                        logger.error("Exception is {}", e);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                dialog.dismiss();
                exitEditMode();
                new LoadDataTask().execute();
                adapter.getSelectedIds().clear();
                getCheckedNum(0);//设置删除按钮数量为0
            } catch (Exception e) {
                logger.error("Exception is {}", e);
            }
        }
    }
}
