package com.ifeng.newvideo.ui.mine.msg;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangzd
 */
public class ActivityMsg extends BaseFragmentActivity implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(ActivityMsg.class);

    private TextView title;
    private ImageView iv_back;

    private List<Fragment> fragmentList = new ArrayList<>();
    private ViewPager mPageVp;
    private FragmentPageAdapter mFragmentAdapter;
    private TextView mTabComment, mTabMsgReply;
    private View redView;

    private UserCommentFragment commentFragment;
    private UserReplyFragment replyFragment;

    public static final int TAB_COMMENT = 0;
    public static final int TAB_REPLY = 1;
    private int mCurrentTab;

    private String mRefPageIdForPageComment = PageIdConstants.PAGE_MY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(false);
        initView();
        initTab();
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_MESSAGE_REPLY);
                finish();
            }
        });

        mTabComment = (TextView) findViewById(R.id.title_comment);
        mTabComment.setText("评论");
        mTabComment.setOnClickListener(this);
        mTabMsgReply = (TextView) findViewById(R.id.title_msg_reply);
        mTabMsgReply.setText("回复");
        mTabMsgReply.setOnClickListener(this);

        redView = findViewById(R.id.ll_red_line);

        mPageVp = (ViewPager) findViewById(R.id.vp_msg);
    }

    private void initTab() {
        commentFragment = new UserCommentFragment();

        replyFragment = new UserReplyFragment();
        boolean timerStatus = getIntent().getBooleanExtra(IntentKey.MSG_STATUS, false);
        Bundle bundle = new Bundle();
        bundle.putBoolean(IntentKey.MSG_STATUS, timerStatus);
        replyFragment.setArguments(bundle);

        fragmentList.add(commentFragment);
        fragmentList.add(replyFragment);

        mFragmentAdapter = new FragmentPageAdapter(getSupportFragmentManager(), fragmentList);

        mPageVp.setAdapter(mFragmentAdapter);
        mPageVp.setCurrentItem(TAB_COMMENT);

        mPageVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                resetTextView();

                if (mCurrentTab == TAB_COMMENT) {
                    PageActionTracker.endPageMessageComment(mRefPageIdForPageComment);
                } else {
                    PageActionTracker.endPageMessageReply();
                }

                switch (position) {
                    case TAB_COMMENT:
                        mTabComment.setTextColor(Color.parseColor("#f54343"));
                        startLineMoveLeft();
                        break;

                    case TAB_REPLY:
                        mRefPageIdForPageComment = PageIdConstants.MY_MESSAGE_REPLY;

                        mTabMsgReply.setTextColor(Color.parseColor("#f54343"));
                        startLineMoveRight();
                        break;
                    default:
                        break;
                }
                mCurrentTab = position;
                PageActionTracker.enterPage();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        PageActionTracker.enterPage();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCurrentTab == TAB_COMMENT) {
            PageActionTracker.endPageMessageComment(mRefPageIdForPageComment);
        } else {
            PageActionTracker.endPageMessageReply();
        }
    }

    private void resetTextView() {
        mTabComment.setTextColor(Color.parseColor("#000000"));
        mTabMsgReply.setTextColor(Color.parseColor("#000000"));
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.title_comment:
                resetTextView();
                mPageVp.setCurrentItem(TAB_COMMENT);
                startLineMoveLeft();
                break;

            case R.id.title_msg_reply:
                resetTextView();
                mPageVp.setCurrentItem(TAB_REPLY);
                startLineMoveRight();
                break;

            default:
                break;
        }
    }


    private void startLineMoveRight() {
        redView.clearAnimation();
        ValueAnimator mAnimator = null;

        mAnimator = ValueAnimator.ofFloat(0, DisplayUtils.convertDipToPixel(72.0f));
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (redView != null) {
                    redView.bringToFront();
                    redView.setTranslationX((Float) valueAnimator.getAnimatedValue());
                    redView.invalidate();
                }
            }
        });
        mAnimator.setDuration(100);
        mAnimator.setRepeatCount(0);
        mAnimator.start();
    }

    private void startLineMoveLeft() {
        redView.clearAnimation();
        ValueAnimator mAnimator = null;

        mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(72.0f), 0);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (redView != null) {
                    redView.bringToFront();
                    redView.setTranslationX((Float) valueAnimator.getAnimatedValue());
                    redView.invalidate();
                }
            }
        });
        mAnimator.setDuration(100);
        mAnimator.setRepeatCount(0);
        mAnimator.start();
    }
}
