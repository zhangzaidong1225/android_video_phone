package com.ifeng.newvideo.ui.mine.msg;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.ui.adapter.MsgCommentAdapter;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.videoplayer.bean.TopicDetailList;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.dao.TopicVideoDao;
import com.ifeng.newvideo.videoplayer.dao.VideoDao;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.model.CommentItem;
import com.ifeng.video.dao.db.model.UserCommentsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangzd
 */
public class UserCommentFragment extends FragmentBase {

    private RelativeLayout tvNoMsg;
    private List<CommentItem> comments = new ArrayList<>();
    private MsgCommentAdapter listAdapter;
    private int page = 1;
    private int pageSize = 5;
    private LinearLayout mLoadingView;
    private List<CommentItem> commentList = new ArrayList<>(); //评论有效list
    private List<VideoItem> tmpVideoList = new ArrayList<>(); //有效视频list
    private int totalCommentNum;
    private UIStatusLayout loadingLayout;
    private PullToRefreshListView mLv;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_msg_comment_layout, null);
        loadingLayout = (UIStatusLayout) rootView.findViewById(R.id.video_comment_uistatus_layout);
        mLv = loadingLayout.getListView();
        tvNoMsg = (RelativeLayout) rootView.findViewById(R.id.rl_no_msg_comment);
        tvNoMsg.setVisibility(View.GONE);

        mLv.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        mLv.setShowIndicator(false);
        mLv.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        mLv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                requestCommentData();
            }
        });
        mLv.getRefreshableView().setFooterDividersEnabled(true);
        listAdapter = new MsgCommentAdapter();
        mLv.getRefreshableView().setAdapter(listAdapter);

        loadingLayout.setStatus(UIStatusLayout.LOADING);
        requestCommentData();

        return rootView;
    }

    private void requestCommentData() {
        tmpVideoList.clear();
        commentList.clear();
        topicList.clear();

        CommentDao.getUserCommentList(User.getUid(),
                page,
                pageSize,
                new Response.Listener<UserCommentsModel>() {
                    @Override
                    public void onResponse(UserCommentsModel response) {
                        if (response != null && !ListUtils.isEmpty(response.getComments())) {
                            handleResponseEvent(response.getComments());
                        } else {
                            dataEmpty();
                        }
                        mLv.onRefreshComplete();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mLv.onRefreshComplete();
                        totalCommentNum = 0;
                        handleErrorEvent(error);
                    }
                });
    }


    private void handleResponseEvent(List<CommentItem> dataList) {
        if (ListUtils.isEmpty(dataList)) {
            if (ListUtils.isEmpty(comments)) {
                tvNoMsg.setVisibility(View.VISIBLE);
                loadingLayout.setVisibility(View.GONE);
            } else {
                dataEmpty();
            }
        } else {
            totalCommentNum = 0;
            commentList = handleData(dataList);
            if (ListUtils.isEmpty(commentList)) {
                dataEmpty();
            } else {
                loadingLayout.setStatus(UIStatusLayout.NORMAL);
                page++;
            }
        }
    }

    private void filterVideoItem() {
        if (!ListUtils.isEmpty(commentList)) {
            updateAdapter(comments);
        }
    }

    private void dataEmpty() {
        if (ListUtils.isEmpty(comments)) {
            tvNoMsg.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        } else {
            tvNoMsg.setVisibility(View.GONE);
            ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
        }
    }

    private List<CommentItem> handleData(List<CommentItem> list) {
        List<CommentItem> tempList = new ArrayList<>();
        for (CommentItem comment : list) {
            if (comment == null
                    || TextUtils.isEmpty(comment.getDoc_url())
                    || comment.getDoc_url().contains("sub_")
                    || comment.getDoc_url().contains("ifengnews_")
                    || comments.contains(comment)) {
                continue;
            }
            tempList.add(comment);
            ++totalCommentNum;
            loadVideoInfo(comment.getDoc_url());
        }
        return tempList;
    }


    private void updateAdapter(List<CommentItem> list) {
        listAdapter.setData(list);
        listAdapter.notifyDataSetChanged();
        tvNoMsg.setVisibility(View.GONE);

    }

    private void handleErrorEvent(VolleyError error) {
        if (page == 1) {
            if (error instanceof NetworkError) {
                loadingLayout.setStatus(UIStatusLayout.NO_NET);
            } else {
                loadingLayout.setStatus(UIStatusLayout.ERROR);
            }
        } else {
            if (error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                tvNoMsg.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CommentDao.cancelAll();
    }


    private void loadVideoInfo(final String guid) {
        if (!TextUtils.isEmpty(guid)) {
            //是专题或者新闻
            boolean isTopic = guid.contains("http");
            if (!isTopic) {
                VideoDao.getVideoInformationById(guid, "",
                        new Response.Listener<VideoItem>() {
                            @Override
                            public void onResponse(VideoItem response) {
                                if (null == response || TextUtils.isEmpty(response.toString())) {
                                    return;
                                }
                                updateCommentInfo(response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
//                                --totalCommentNum;
                                handleErrorData(guid);
                                tmpVideoList.add(new VideoItem());
                                updateCommentList();
                            }
                        });
            } else {
                String topicId = null;
                String itemType = null;
                String tmp = listAdapter.getTopicId(guid);

                if (null != tmp && !TextUtils.isEmpty(tmp)) {
                    int firstHen = tmp.indexOf("/");
                    itemType = tmp.substring(0, firstHen);
                    if (listAdapter.topics[3].equalsIgnoreCase(itemType)) {
                        itemType = listAdapter.topics[0];
                    }
                    topicId = tmp.substring(firstHen + 1, tmp.length());
                }

                if (!TextUtils.isEmpty(topicId) && !TextUtils.isEmpty(itemType)) {

                    TopicVideoDao.getTopicDetailList(topicId, itemType, new Response.Listener<TopicDetailList>() {
                                @Override
                                public void onResponse(TopicDetailList response) {
                                    if (null == response || TextUtils.isEmpty(response.toString())) {
                                        return;
                                    }
                                    updateTopicList(response);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
//                                    --totalCommentNum;
                                    handleErrorData(guid);
                                    topicList.add(new TopicDetailList());
                                    updateCommentList();
                                }
                            });
                } else {
                    //新闻
//                    --totalCommentNum;
                    handleErrorData(guid);
                    topicList.add(new TopicDetailList());
                    updateCommentList();
                }

            }
        }

    }

    private void handleErrorData(String guid) {
        for (CommentItem comment : commentList) {
            if (!comment.getDoc_url().equals(guid)) {
                continue;
            }
            comment.setWeMediaName("");
            comment.setWeMediaId("");
            comment.setDuration(0);
            comment.setCover("error");
            comment.setPlaytime("");
            if (!comments.contains(comment)) {
                comments.add(comment);
            }
        }
    }


    private void updateCommentList() {
        int videoAndTopicNum;
        if (!ListUtils.isEmpty(tmpVideoList) && ListUtils.isEmpty(topicList)) {
            videoAndTopicNum = tmpVideoList.size();
        } else if (!ListUtils.isEmpty(topicList) && ListUtils.isEmpty(tmpVideoList)) {
            videoAndTopicNum = topicList.size();
        } else {
            videoAndTopicNum = tmpVideoList.size() + topicList.size();
        }

        if (videoAndTopicNum == totalCommentNum) {
            filterVideoItem();
        }
    }


    private void updateCommentInfo(VideoItem item) {

        if (null != item && !TextUtils.isEmpty(item.guid)) {
            for (CommentItem comment : commentList) {
                if (!comment.getDoc_url().equals(item.guid)) {
                    continue;
                }
                comment.setWeMediaName(item.weMedia.name);
                comment.setWeMediaId(item.weMedia.id);
                comment.setDuration(item.duration);
                comment.setCover(item.image);
                comment.setPlaytime(item.playTime);
                if (!comments.contains(comment)) {
                    comments.add(comment);
                }
            }
            tmpVideoList.add(item);
        } else {
            --totalCommentNum;
        }

        if (!ListUtils.isEmpty(tmpVideoList)) {
            updateCommentList();
        }
    }

    private List<TopicDetailList> topicList = new ArrayList<>();

    private void updateTopicList(TopicDetailList response) {
        if (!TextUtils.isEmpty(response.topicShareURL)) {
            for (CommentItem comment : commentList) {
                if (!comment.getDoc_url().equals(response.topicShareURL)) {
                    continue;
                }
                comment.setWeMediaName("");
                comment.setWeMediaId("");
                comment.setDuration(0);
                comment.setCover("");
                comment.setPlaytime("");
                if (!comments.contains(comment)) {
                    comments.add(comment);
                }
            }
            topicList.add(response);
        } else {
            --totalCommentNum;
        }


        if (!ListUtils.isEmpty(topicList)) {
            updateCommentList();
        }
    }


}
