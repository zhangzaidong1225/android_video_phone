package com.ifeng.newvideo.ui.mine.adapter;

/**
 * 该接口用于处理看过，收藏，缓存选中或未选中Item状态的数量回调
 * Created by Administrator on 2016/8/20.
 */
public interface ICheckedNumber {
    void getCheckedNum(int num);
}
