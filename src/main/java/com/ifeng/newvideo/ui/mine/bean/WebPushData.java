package com.ifeng.newvideo.ui.mine.bean;

import java.util.List;

/**
 * 我的推荐数据实体类
 * Created by Administrator on 2016/8/1.
 */
public class WebPushData {

    /**
     * webPush : [{"title":"推荐位1","image":"http://y2.ifengimg.com/a/2014_46/41404c56d1dc41b.jpg","url":"http://www.ifeng.com"},{"title":"推荐位2","image":"http://y0.ifengimg.com/a/2014_46/812d623ec61174b.jpg","url":"http://v.ifeng.com"},{"title":"推荐位3","image":"http://y0.ifengimg.com/a/2016_16/6933146b4e354a0.png","url":"http://www.ifeng.com"}]
     */
    private List<WebPushEntity> webPush;

    public void setWebPush(List<WebPushEntity> webPush) {
        this.webPush = webPush;
    }

    public List<WebPushEntity> getWebPush() {
        return webPush;
    }

    public static class WebPushEntity {
        /**
         * "title": " 充值中心",
         * "url": "http://hjll.ifeng.com/Public/VoucherCenter/ShiPin/index.html",
         * "image": "http://p2.ifengimg.com/a/2017/0829/1477caa930a9f55size3_w57_h57.png",
         * "isNeedParameters": "yes",
         * "isOpenInApp": "yes"
         */
        private String title;
        private String image;
        private String url;
        private String isNeedParameters;
        private String isOpenInApp; // yes no

        public String getIsNeedParameters() {
            return isNeedParameters;
        }

        public void setIsNeedParameters(String isNeedParameters) {
            this.isNeedParameters = isNeedParameters;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public String getImage() {
            return image;
        }

        public String getUrl() {
            return url;
        }

        public String getIsOpenInApp() {
            return isOpenInApp;
        }

        public void setIsOpenInApp(String isOpenInApp) {
            this.isOpenInApp = isOpenInApp;
        }
    }
}
