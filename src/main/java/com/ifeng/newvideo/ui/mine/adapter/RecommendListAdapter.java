package com.ifeng.newvideo.ui.mine.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.mine.bean.WebPushData;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.net.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的界面中推荐ListView适配器
 * Created by Administrator on 2016/7/31.
 */
public class RecommendListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<WebPushData.WebPushEntity> list = new ArrayList<>();

    public RecommendListAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        OnClick onClick = null;
        if (view == null) {
            viewHolder = new ViewHolder();
            onClick = new OnClick();
            view = layoutInflater.inflate(R.layout.adapter_mine_recommend_item_layout, null);
            viewHolder.initView(view);
            viewHolder.itemView.setOnClickListener(onClick);
            view.setTag(viewHolder);
            view.setTag(R.id.recommend_container, onClick);
        } else {
            viewHolder = (ViewHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.recommend_container);
        }
        onClick.setPosition(i);
        viewHolder.recommendIcon.setImageUrl(list.get(i).getImage(), VolleyHelper.getImageLoader());
        viewHolder.recommendIcon.setDefaultImageResId(R.drawable.avatar_default);
        viewHolder.recommendIcon.setErrorImageResId(R.drawable.avatar_default);
        viewHolder.recommendName.setText(list.get(i).getTitle());

        //最后一条View底部的下划线要Gone掉
        if (i == list.size() - 1) {
            viewHolder.view_underline.setVisibility(View.GONE);
        } else {
            viewHolder.view_underline.setVisibility(View.VISIBLE);
        }
        return view;
    }

    public void setList(List<WebPushData.WebPushEntity> list) {
        this.list = list;
    }

    static class ViewHolder {
        RelativeLayout itemView;
        NetworkImageView recommendIcon;//游戏图标
        TextView recommendName;//游戏名称
        View view_underline;//下划线

        private void initView(View view) {
            itemView = (RelativeLayout) view.findViewById(R.id.recommend_container);
            recommendIcon = (NetworkImageView) view.findViewById(R.id.icon_recommend);
            recommendName = (TextView) view.findViewById(R.id.tv_recommend_name);
            view_underline = view.findViewById(R.id.view_underline);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.recommend_container:
                    String image = list.get(position).getImage();
                    String title = list.get(position).getTitle();
                    String url = handleUrlParams(list.get(position).getUrl());
                    boolean isOpenInApp = TextUtils.isEmpty(list.get(position).getIsOpenInApp()) ||
                            "yes".equalsIgnoreCase(list.get(position).getIsOpenInApp());
                    if (isOpenInApp) {
                        IntentUtils.startADActivity(context, null, url, null, ADActivity.FUNCTION_VALUE_H5_STATIC, title,
                                null, image, "", "", null, null);
                    } else {
                        IntentUtils.startBrowser(context, url);
                    }
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_MY_H5 + title, PageIdConstants.PAGE_MY);
                    break;
            }
        }

        /**
         * 一些url需要拼接參數
         */
        private String handleUrlParams(String url) {
            boolean isNeedParams = "yes".equalsIgnoreCase(list.get(position).getIsNeedParameters());
            if (!isNeedParams) {
                return url;
            }

            boolean hasQuestionMark = !TextUtils.isEmpty(url) && url.contains("?");
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append(url);
            urlBuilder.append(hasQuestionMark ? "&" : "?");
            urlBuilder.append("gv=").append(PhoneConfig.softversion);
            urlBuilder.append("&deviceid=").append(PhoneConfig.userKey);
            urlBuilder.append("&proid=videoapp");
            urlBuilder.append("&os=").append(PhoneConfig.mos);
            urlBuilder.append("&publishid=").append(PhoneConfig.publishid);
            urlBuilder.append("&guid=").append(User.getUid());
            urlBuilder.append("&token=").append(User.getIfengToken());
            return urlBuilder.toString();
        }
    }
}
