package com.ifeng.newvideo.ui.mine.signin.calendar;


public interface OnItemRender {
	
	void onRender(CheckableLayout v, CardGridItem item);

}
