package com.ifeng.newvideo.ui.mine.signin.calendar;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;


public class CalendarCard extends RelativeLayout {

    private TextView cardTitle;
    private int itemLayout = R.layout.card_item_simple;
    private OnItemRender mOnItemRender;
    private OnItemRender mOnItemRenderDefault;
    private Calendar dateDisplay;
    private final ArrayList<CheckableLayout> cells = new ArrayList<CheckableLayout>();
    private LinearLayout cardGrid;
    private View layout;
    private Calendar mCalendar;

    public CalendarCard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public CalendarCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CalendarCard(Context context) {
        super(context);
        init(context);
    }

    public void updateChecked(String[] b, String today) {
        ArrayList<String> arr = new ArrayList<String>();
        Collections.addAll(arr, b);
        for (CheckableLayout cell : cells) {
            CardGridItem item = (CardGridItem) cell.getTag();
            if (item != null && item.isEnabled()) {
                if (arr.contains(item.getDayOfMonth() + "")) {
                    cell.findViewById(R.id.iv_checked).setVisibility(View.VISIBLE);
                }
                if (today.equals(item.getDayOfMonth() + "")) {
                    cell.getChildAt(0).setBackgroundResource(R.drawable.card_item_bg_red);
                }
            }
        }
    }

    private void init(Context ctx) {
        if (isInEditMode()) return;

        layout = LayoutInflater.from(ctx).inflate(R.layout.card_view, null, false);

        if (dateDisplay == null) dateDisplay = Calendar.getInstance();

        cardTitle = (TextView) layout.findViewById(R.id.cardTitle);
        cardGrid = (LinearLayout) layout.findViewById(R.id.cardGrid);
        int month = (dateDisplay.get(Calendar.MONTH)) + 1;
        cardTitle.setText(month + ctx.getString(R.string.calendar_month_title));

        setDayOfWeekText();

        LayoutInflater la = LayoutInflater.from(ctx);
        for (int y = 0; y < cardGrid.getChildCount(); y++) {
            LinearLayout row = (LinearLayout) cardGrid.getChildAt(y);
            for (int x = 0; x < row.getChildCount(); x++) {
                CheckableLayout cell = (CheckableLayout) row.getChildAt(x);
                View cellContent = la.inflate(itemLayout, cell, false);
                cell.addView(cellContent);
                cells.add(cell);
            }
        }

        addView(layout);

        mOnItemRenderDefault = new OnItemRender();

        updateCells();
    }

    /**
     * 设置周日------周一
     */
    private void setDayOfWeekText() {
        mCalendar = Calendar.getInstance();
        mCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
//        setWeekText(R.id.cardDay1);
//        setWeekText(R.id.cardDay2);
//        setWeekText(R.id.cardDay3);
//        setWeekText(R.id.cardDay4);
//        setWeekText(R.id.cardDay5);
//        setWeekText(R.id.cardDay6);
//        setWeekText(R.id.cardDay7);
    }

//    private void setWeekText(int viewId) {
//        String dayOfWeek = mCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
//        dayOfWeek = dayOfWeek.substring(1);
//        ((TextView) layout.findViewById(viewId)).setText(dayOfWeek);
//        mCalendar.add(Calendar.DAY_OF_WEEK, 1);
//    }

    private int getDaySpacing(int dayOfWeek) {
        if (Calendar.SUNDAY == dayOfWeek) {
            return 6;
        }
        return dayOfWeek - 2;
    }

    private int getDaySpacingEnd(int dayOfWeek) {
        return 8 - dayOfWeek;
    }

    private void updateCells() {
        Calendar cal;
        Integer counter = 0;
        if (dateDisplay != null) {
            cal = (Calendar) dateDisplay.clone();
        } else {
            cal = Calendar.getInstance();
        }
        cal.set(Calendar.DAY_OF_MONTH, 1);

        int daySpacing = getDaySpacing(cal.get(Calendar.DAY_OF_WEEK));

        // INFO : wrong calculations of first line - fixed
        if (daySpacing > 0) {
            Calendar prevMonth = (Calendar) cal.clone();
            prevMonth.add(Calendar.MONTH, -1);
            prevMonth.set(Calendar.DAY_OF_MONTH, prevMonth.getActualMaximum(Calendar.DAY_OF_MONTH) - daySpacing + 1);
            for (int i = 0; i < daySpacing; i++) {
                CheckableLayout cell = cells.get(counter);
                cell.setTag(new CardGridItem(Integer.valueOf(prevMonth.get(Calendar.DAY_OF_MONTH))).setEnabled(false));
                cell.setEnabled(false);
                (mOnItemRender == null ? mOnItemRenderDefault : mOnItemRender).onRender(cell, (CardGridItem) cell.getTag());
                counter++;
                prevMonth.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        int firstDay = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        int lastDay = cal.get(Calendar.DAY_OF_MONTH) + 1;
        for (int i = firstDay; i < lastDay; i++) {
            cal.set(Calendar.DAY_OF_MONTH, i - 1);
            Calendar date = (Calendar) cal.clone();
            date.add(Calendar.DAY_OF_MONTH, 1);
            CheckableLayout cell = cells.get(counter);
            cell.setTag(new CardGridItem(i).setEnabled(true).setDate(date));
            cell.setEnabled(true);
            cell.setVisibility(View.VISIBLE);
            (mOnItemRender == null ? mOnItemRenderDefault : mOnItemRender).onRender(cell, (CardGridItem) cell.getTag());
            counter++;
        }

        if (dateDisplay != null) {
            cal = (Calendar) dateDisplay.clone();
        } else {
            cal = Calendar.getInstance();
        }
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));

        daySpacing = getDaySpacingEnd(cal.get(Calendar.DAY_OF_WEEK));

        if (daySpacing > 0) {
            for (int i = 0; i < daySpacing; i++) {
                CheckableLayout cell = cells.get(counter);
                cell.setTag(new CardGridItem(i + 1).setEnabled(false)); // .setDate((Calendar)cal.clone())
                cell.setEnabled(false);
                cell.setVisibility(View.VISIBLE);
                (mOnItemRender == null ? mOnItemRenderDefault : mOnItemRender).onRender(cell, (CardGridItem) cell.getTag());
                counter++;
            }
        }

        if (counter < cells.size()) {
            for (int i = counter; i < cells.size(); i++) {
                cells.get(i).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (changed && cells.size() > 0) {
            int size = (r - l) / 7;
            for (CheckableLayout cell : cells) {
                cell.getLayoutParams().height = size;
            }
        }
    }

    public int getItemLayout() {
        return itemLayout;
    }

    public void setItemLayout(int itemLayout) {
        this.itemLayout = itemLayout;
        // mCardGridAdapter.setItemLayout(itemLayout);
    }

    public OnItemRender getOnItemRender() {
        return mOnItemRender;
    }

    public void setOnItemRender(OnItemRender mOnItemRender) {
        this.mOnItemRender = mOnItemRender;
        // mCardGridAdapter.setOnItemRender(mOnItemRender);
    }

    public Calendar getDateDisplay() {
        return dateDisplay;
    }

    public void setDateDisplay(Calendar dateDisplay) {
        this.dateDisplay = dateDisplay;
        cardTitle.setText(new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(dateDisplay.getTime()));
    }


    /**
     * call after change any input data - to refresh view
     */
    public void notifyChanges() {
        // mCardGridAdapter.init();
        updateCells();
    }

    private static class OnItemRender implements com.ifeng.newvideo.ui.mine.signin.calendar.OnItemRender {
        @Override
        public void onRender(CheckableLayout v, CardGridItem item) {
            ((TextView) v.findViewById(R.id.tv_day)).setText(item.getDayOfMonth().toString());
            if (item.isEnabled()) {
                ((TextView) v.findViewById(R.id.tv_day)).setTextColor(Color.parseColor("#6b4452"));
            } else {
                ((TextView) v.findViewById(R.id.tv_day)).setTextColor(Color.parseColor("#b5a8a8"));

            }
        }
    }
}
