package com.ifeng.newvideo.ui.mine.signin;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.View;
import android.widget.*;
import cn.sharesdk.framework.Platform;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.SignInRecord;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.mine.signin.calendar.CalendarCard;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 签到界面
 * Created by wangqing1 on 2015/1/26.
 */


public class SignInActivity extends BaseFragmentActivity implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(SignInActivity.class);
    private ImageView iv_return;
    private ImageView rl_share;
    private ImageView iv_activeinfo;
    private RelativeLayout rl_sign_in_editText;
    private EditText et_sign_in_mobile_no;
    private ImageView iv_sign_in_clear;
    private Button btn_sign_in;
    private TextView tv_sign_in_no;
    private ActiveInfo activeInfo = new ActiveInfo();
    private String month;//当前月
    private String day;//当前日
    private CalendarCard card;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        iv_return = (ImageView) findViewById(R.id.back);
        rl_share = (ImageView) findViewById(R.id.editor);
        rl_share.setVisibility(View.VISIBLE);
        rl_share.setImageResource(R.drawable.sign_in_share);
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.sign_in_title));
        iv_activeinfo = (ImageView) findViewById(R.id.iv_activeinfo);
        rl_sign_in_editText = (RelativeLayout) findViewById(R.id.rl_sign_in_editText);
        et_sign_in_mobile_no = (EditText) findViewById(R.id.et_sign_in_no);
        iv_sign_in_clear = (ImageView) findViewById(R.id.iv_sign_in_clear);
        btn_sign_in = (Button) findViewById(R.id.btn_sign_in);
        tv_sign_in_no = (TextView) findViewById(R.id.tv_sign_in_no);
        card = (CalendarCard) findViewById(R.id.calendar);
    }

    private void initData() {
        initDate();
        String mobileNu = SharePreUtils.getInstance().getSignInNo();
        if (!TextUtils.isEmpty(mobileNu)) {
            tv_sign_in_no.setText(mobileNu + "   " + getResources().getString(R.string.sign_in_welcome_back));
            tv_sign_in_no.setVisibility(View.VISIBLE);
            rl_sign_in_editText.setVisibility(View.GONE);
        }
        if (SharePreUtils.getInstance().hasSigned(month, day)) {
            btn_sign_in.setClickable(false);
            btn_sign_in.setText(R.string.sign_in_btn_signed);
            btn_sign_in.setBackgroundResource(R.drawable.sign_in_finish);
        }
        getSignInInfo();
        card.updateChecked(SharePreUtils.getInstance().getSignDayArr(), day);
    }

    /**
     * 初始化月日
     */
    private void initDate() {
        Time time = new Time("GMT+8");
        time.setToNow();
        month = time.month + "";
        day = time.monthDay + "";
    }

    private void initListener() {
        iv_return.setOnClickListener(this);
        rl_share.setOnClickListener(this);
        iv_sign_in_clear.setOnClickListener(this);
        iv_activeinfo.setOnClickListener(this);
        btn_sign_in.setOnClickListener(this);
        et_sign_in_mobile_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (StringUtils.isBlank(s.toString())) {
                    iv_sign_in_clear.setVisibility(View.GONE);
                } else {
                    iv_sign_in_clear.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.editor:
                OneKeyShareContainer.oneKeyShare = new OneKeyShare(SignInActivity.this);
                OneKeyShareContainer.oneKeyShare.setShareType(Platform.SHARE_WEBPAGE);
                OneKeyShareContainer.oneKeyShare.shareSignIn(activeInfo);
                break;
            case R.id.iv_activeinfo:
                IntentUtils.startSignInActiveInfoActivity(this, activeInfo.introUrl, getResources().getString(R.string.sign_in_webView_title), "");
                break;
            case R.id.iv_sign_in_clear:
                et_sign_in_mobile_no.setText("");
                break;
            case R.id.btn_sign_in:
                if (SharePreUtils.getInstance().hasSigned(month, day)) {
                    return;
                }
                String mobileNumber = SharePreUtils.getInstance().getSignInNo();
                if (TextUtils.isEmpty(mobileNumber)) {
                    mobileNumber = et_sign_in_mobile_no.getText().toString();
                }
                if (StringUtils.isMobileNumber(mobileNumber)) {
                    signin(mobileNumber);
                } else {
                    ToastUtils.getInstance().showShortToast(R.string.sign_in_number_wrong);
                }
                break;
        }

    }

    /**
     * 发送签到统计
     *
     * @param mobileNu
     */
    private void signin(String mobileNu) {
        String add = mobileNu + "_" + DateUtils.getDateFormatForSignIn();
        if (NetUtils.isNetAvailable(this)) {
            CustomerStatistics.sendSignInRecord(new SignInRecord("signin", add, "signin"));
            ToastUtils.getInstance().showShortToast(R.string.sign_in_success);
            SharePreUtils.getInstance().setSignInNo(mobileNu);
            btn_sign_in.setClickable(false);
            btn_sign_in.setBackgroundResource(R.drawable.sign_in_finish);
            btn_sign_in.setText(R.string.sign_in_btn_signed);
            tv_sign_in_no.setText(mobileNu + "   " + getResources().getString(R.string.sign_in_welcome_back));
            tv_sign_in_no.setVisibility(View.VISIBLE);
            rl_sign_in_editText.setVisibility(View.GONE);
            SharePreUtils.getInstance().addSignDay(month, day);
            card.updateChecked(SharePreUtils.getInstance().getSignDayArr(), day);
        } else {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
    }

    /**
     * 获取签到活动的介绍信息
     */
    private void getSignInInfo() {
        CommonDao.sendRequest(DataInterface.getSignInUrl(), null, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                logger.warn(response + "");
                if (response == null) {
                    return;
                }
                JSONArray array = response.getJSONArray("signinInfo");
                if (array == null) {
                    return;
                }
                JSONObject jo = array.getJSONObject(0);
                activeInfo = new ActiveInfo(jo.getString("introUrl"), jo.getString("shareContent"));
                logger.debug("activeInfo" + jo.getString("introUrl"));

            }
        }, new ErrorListener(), CommonDao.RESPONSE_TYPE_GET_JSON);

    }

    private static class ErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {

        }
    }
}
