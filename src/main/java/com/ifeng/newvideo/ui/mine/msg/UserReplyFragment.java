package com.ifeng.newvideo.ui.mine.msg;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.adapter.MsgReplyListAdapter;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.model.CommentsModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangzd
 */
public class UserReplyFragment extends FragmentBase {

    private static final Logger logger = LoggerFactory.getLogger(UserReplyFragment.class);

    private ListView listView;
    private RelativeLayout tvNoMsg;
    private TextView allMsg;
    private List<CommentsModel.Comment> newestList = new ArrayList<>();
    private MsgReplyListAdapter listAdapter;
    private View bottomView;
    private String timer = "";
    private boolean timerStatus;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            timerStatus = getArguments().getBoolean(IntentKey.MSG_STATUS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_msg_reply_layout, null);
        tvNoMsg = (RelativeLayout) rootView.findViewById(R.id.rl_no_msg_reply);
        tvNoMsg.setVisibility(View.GONE);
        bottomView = inflater.inflate(R.layout.listview_more_button_layout, null);
        allMsg = (TextView) bottomView.findViewById(R.id.tv_look_all);
        allMsg.setVisibility(View.VISIBLE);
        bottomView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_MSG_LOOK_MORE, PageIdConstants.MY_MESSAGE_REPLY);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        allMsg.setText("加载中...");
                        sendRequestMsg("");
                    }
                });
            }
        });
        bottomView.setVisibility(View.GONE);

        listView = (ListView) rootView.findViewById(R.id.lv_msg_reply);
        listView.addFooterView(bottomView);

        listAdapter = new MsgReplyListAdapter();
        listAdapter.setShowBottomLine(true);
        listView.setAdapter(listAdapter);

//        sendRequestMsg("");
        Log.d("timerStatus", timerStatus + "------11111");
        if (!timerStatus) {
            sendRequestMsg("");
        } else {
            if (SharePreUtils.getInstance().getMsgTimer() != null) {
                timer = String.valueOf(SharePreUtils.getInstance().getMsgTimer());
            }
            sendRequestMsg(timer);
        }

        return rootView;
    }

    private void sendRequestMsg(String time) {
        CommentDao.userVideoMsg(User.getUid(), time,
                CommentsModel.class,
                new Response.Listener<CommentsModel>() {
                    @Override
                    public void onResponse(CommentsModel response) {
                        if (response != null && !ListUtils.isEmpty(response.getComments())) {
                            handleResponseEvent(response.getComments());
                            logger.debug("Msg:{}", response.toString());
                        } else {
                            dataEmpty();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("MsgError:{}", error.getMessage());
                        handleErrorEvent(error);
                    }
                });
    }

    private void handleResponseEvent(List<CommentsModel.Comment> dataList) {
        if (!ListUtils.isEmpty(dataList)) {
            List<CommentsModel.Comment> list = handleData(dataList);
            if (!ListUtils.isEmpty(list) && list.size() > 0) {
                newestList.addAll(list);
                long timer = Long.valueOf(newestList.get(0).getCreate_time());
                SharePreUtils.getInstance().setMsgTimer(timer);
                allMsg.setText(getResources().getString(R.string.toast_check_more));
                updateAdapter(newestList);
            } else {
                dataEmpty();
            }

        } else {
            tvNoMsg.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
    }

    private void dataEmpty() {
        if (ListUtils.isEmpty(newestList)) {
            tvNoMsg.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            bottomView.setVisibility(View.GONE);
        } else {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            bottomView.setVisibility(View.VISIBLE);
            allMsg.setText(getResources().getString(R.string.toast_no_more));
        }
    }

    private List<CommentsModel.Comment> handleData(List<CommentsModel.Comment> list) {
        List<CommentsModel.Comment> tempList = new ArrayList<>();
        for (CommentsModel.Comment comment : list) {
            if (comment == null
                    || TextUtils.isEmpty(comment.getDoc_url())
                    || comment.getDoc_url().contains("http")
                    || comment.getDoc_url().contains("sub_")
                    || comment.getDoc_url().contains("ifengnews_")
                    || newestList.contains(comment)) {
                continue;
            }
            tempList.add(comment);
        }
        return tempList;
    }

    private void updateAdapter(List<CommentsModel.Comment> list) {
        listAdapter.setData(list);
        bottomView.setVisibility(View.VISIBLE);
        listAdapter.notifyDataSetChanged();
        tvNoMsg.setVisibility(View.GONE);
    }

    private void handleErrorEvent(VolleyError error) {
        if (error != null && error instanceof NetworkError) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
        } else {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            tvNoMsg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
