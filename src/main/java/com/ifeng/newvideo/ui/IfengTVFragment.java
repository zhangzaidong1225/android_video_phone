package com.ifeng.newvideo.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.bean.OnInnerItemClickListener;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.adapter.HeaderViewPagerAdapter;
import com.ifeng.newvideo.ui.adapter.ListAdapter4MixTextPic;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.ui.subscribe.AllWeMediaActivity;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.LastDocUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.widget.HeadFlowView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import com.ifeng.video.dao.db.model.subscribe.WeMediaList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guolc on 2016/7/20.
 * 卫视频道
 */
public class IfengTVFragment extends ChannelBaseFragment implements View.OnClickListener {

    private HeadFlowView mHeadFlowView;
    private List<WeMediaList.WeMediaListEntity> mWeMediaList;
    private View mViewMoreProgram;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mRecyclerAdapter;
    private String requireTime = "";
    private View mHeadView;
    private View mRecyclerHeadErr;
    private ImageView mRecyclerHeadErr_img;
    private TextView mRecyclerHeadErr_tv;
    private boolean mIsFetchWemediaErr = false;
    private boolean mIsFetchListViewErr = false;
    private ListAdapter4MixTextPic listAdapter4MixTextPic;
    private int mUpTimes = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger.debug("onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ifeng_tv, container, false);
        mPullToRefreshListView = (MyPullToRefreshListView) view.findViewById(R.id.tv_program_listView);
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mPullToRefreshListView.setShowIndicator(false);
        mHeadView = inflater.inflate(R.layout.ifeng_tv_header, null);
        mHeadFlowView = (HeadFlowView) mHeadView.findViewById(R.id.ifeng_tv_headFlowView);
        mRecyclerView = (RecyclerView) mHeadView.findViewById(R.id.rv_program_ifeng_tv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mViewMoreProgram = mHeadView.findViewById(R.id.rl_all_tv);
        initAdapter();
        initListener();
        mPullToRefreshListView.getRefreshableView().addHeaderView(mHeadView);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestNet();
        mFocusList = new ArrayList<>(20);
    }

    private void fetchWeMediaData() {
        //自媒体pageSize传"",获取全部
        ChannelDao.requestWeMediaList(WeMediaInfoList.WE_MEDIA_CID_IFENG, "", "", "", "", System.currentTimeMillis() + "", WeMediaList.class, new Response.Listener<WeMediaList>() {
            @Override
            public void onResponse(WeMediaList response) {
                if (response != null && !ListUtils.isEmpty(response.getWeMediaList())) {
                    mIsFetchWemediaErr = false;
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mWeMediaList = response.getWeMediaList();
                    mRecyclerView.setBackgroundColor(Color.WHITE);
                    mRecyclerAdapter.notifyDataSetChanged();
                    updateViewStatus(Status.REQUEST_NET_SUCCESS);
                } else {
                    mIsFetchWemediaErr = true;
                    if (mIsFetchListViewErr && mIsFetchWemediaErr && ListUtils.isEmpty(mAdapter.getDataList()) && ListUtils.isEmpty(mWeMediaList)) {
                        updateViewStatus(Status.DATA_ERROR);
                        return;
                    }
                    showRecyclerViewHeadErr(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.debug("volley onErrorResponse ! {}", error);
                mIsFetchWemediaErr = true;
                if (mIsFetchListViewErr && mIsFetchWemediaErr && ListUtils.isEmpty(mAdapter.getDataList()) && ListUtils.isEmpty(mWeMediaList)) {
                    if (error != null && error instanceof NetworkError) {
                        updateViewStatus(Status.REQUEST_NET_FAIL);
                    } else {
                        updateViewStatus(Status.DATA_ERROR);
                    }
                    return;
                }
                if (error != null && error instanceof NetworkError) {
                    showRecyclerViewHeadErr(true);
                } else {
                    showRecyclerViewHeadErr(false);
                }

            }
        }, true, null);
    }

    private void showRecyclerViewHeadErr(boolean isNetErr) {
        if (mHeadView != null && mRecyclerHeadErr == null) {
            ViewStub viewStub = (ViewStub) mHeadView.findViewById(R.id.head_recycle_err);
            mRecyclerHeadErr = viewStub.inflate();
            mRecyclerHeadErr_img = (ImageView) mRecyclerHeadErr.findViewById(R.id.iv_status_ifeng_tv);
            mRecyclerHeadErr_tv = (TextView) mRecyclerHeadErr.findViewById(R.id.tv_status_hint_ifengtv);
            mRecyclerHeadErr.setOnClickListener(this);
        }
        mRecyclerView.setVisibility(View.GONE);
        mRecyclerHeadErr.setVisibility(View.VISIBLE);
        if (isNetErr) {
            mRecyclerHeadErr_img.setImageResource(R.drawable.commen_net_err);
            mRecyclerHeadErr_tv.setText(R.string.common_net_useless_try_again);
        } else {
            mRecyclerHeadErr_img.setImageResource(R.drawable.commen_load_data_err);
            mRecyclerHeadErr_tv.setText(R.string.common_load_data_error);
        }
    }

    private void initListener() {
        mViewMoreProgram.setOnClickListener(this);
        mPullToRefreshListView.setOnItemClickListener(this);
        mPullToRefreshListView.setOnRefreshListener(this);
        mPullToRefreshListView.setOnLastItemVisibleListener(this);
    }


    @Override
    protected void requestNet() {
        updateViewStatus(Status.LOADING);
        mIsFetchListViewErr = false;
        mIsFetchWemediaErr = false;
        requestNet(requireTime, DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
        fetchWeMediaData();
    }

    private void requestNet(String position_id, String count, final Status status, int action, String operation, String upTimes) {
        final boolean net = NetUtils.isNetAvailable(IfengApplication.getInstance());
        if (!net) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
        if (isLoading) {
            mPullToRefreshListView.onRefreshComplete();
            mPullToRefreshListView.hideFootView();
            return;
        }
        isLoading = true;
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String nw = NetUtils.getNetType(getActivity());
        ChannelDao.requestChannelData(mChannel_id, mChannel_Type, count, position_id, 0, ChannelBean.class,
                true, null, action, SharePreUtils.getInstance().getInreview(), operation, new User(IfengApplication.getAppContext()).getUid(),
                PhoneConfig.userKey, LastDocUtils.getLastDoc(), upTimes,
                province, city, nw, PhoneConfig.publishid,

                new Response.Listener<ChannelBean>() {
                    @Override
                    public void onResponse(ChannelBean response) {
                        isLoading = false;
                        if (response != null && (!ListUtils.isEmpty(response.getBodyList()) || !ListUtils.isEmpty(response.getHeader()))) {
                            if (Status.FIRST == status) {
                                updateViewStatus(Status.REQUEST_NET_SUCCESS);
                                mIsFetchListViewErr = false;
                                mPullToRefreshListView.onRefreshComplete();
                                List<ChannelBean.HomePageBean> list_new = filterList(response.getBodyList(), null, status, false);
                                List<ChannelBean.HomePageBean> heads_new = filterList(response.getHeader(), null, status, true);
                                requireTime = response.getSystemTime();
                                if (!ListUtils.isEmpty(heads_new)) {
                                    mHeadFlowView.setVisibility(View.VISIBLE);
                                    mHeaderViewPagerAdapter.setData(heads_new);
                                    mHeaderViewPagerAdapter.notifyDataSetChanged();
                                    if (mHeaderViewPagerAdapter.getCount() > 1) {
                                        mHeadFlowView.setCurrentItem(20 * heads_new.size());
                                    }
                                } else {
                                    mHeadFlowView.setVisibility(View.GONE);
                                }
                                if (!ListUtils.isEmpty(list_new)) {
                                    listAdapter4MixTextPic.setIsShowBodyErr(false);
                                    mPullToRefreshListView.setVisibility(View.VISIBLE);
                                    mPullToRefreshListView.getRefreshableView().setVisibility(View.VISIBLE);
                                    mAdapter.setData(list_new);
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    mPullToRefreshListView.onRefreshComplete();
                                    listAdapter4MixTextPic.setIsShowBodyErr(true);
                                    listAdapter4MixTextPic.notifyDataSetChanged();
                                }
                            } else if (Status.REFRESH == status) {
                                mPullToRefreshListView.onRefreshComplete();
                                List<ChannelBean.HomePageBean> list_new = filterList(response.getBodyList(), null, status, false);
                                List<ChannelBean.HomePageBean> heads_new = filterList(response.getHeader(), null, status, true);

                                if (!ListUtils.isEmpty(heads_new)) {
                                    mHeaderViewPagerAdapter.setData(heads_new);
                                }
                                mHeaderViewPagerAdapter.notifyDataSetChanged();

                                if (!ListUtils.isEmpty(list_new)) {
                                    mAdapter.setData(list_new);
                                }
                                mAdapter.notifyDataSetChanged();

                                requireTime = response.getSystemTime();
                            } else {
                                List<ChannelBean.HomePageBean> list_new = filterList(response.getBodyList(), mAdapter.getDataList(), status, false);
                                if (net && ListUtils.isEmpty(list_new)) {
//                                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                                } else {
                                    mAdapter.addData(list_new, false);
                                    mAdapter.notifyDataSetChanged();
                                }
                                mPullToRefreshListView.hideFootView();
                            }
                        } else {
                            if (Status.LOAD_MORE == status) {
                                mPullToRefreshListView.hideFootView();
                                if (net) {
                                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                                }
                            } else if (Status.FIRST == status) {
                                mIsFetchListViewErr = true;
                                mPullToRefreshListView.onRefreshComplete();
                                if (mIsFetchListViewErr && mIsFetchWemediaErr && ListUtils.isEmpty(mAdapter.getDataList()) && ListUtils.isEmpty(mWeMediaList)) {
                                    updateViewStatus(Status.DATA_ERROR);
                                    return;
                                }
                                mHeadFlowView.setVisibility(View.GONE);
                                listAdapter4MixTextPic.setIsShowBodyErr(true);
                                listAdapter4MixTextPic.notifyDataSetChanged();
                            } else {
                                mPullToRefreshListView.onRefreshComplete();
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("volley onErrorResponse ! {}", error);
                        isLoading = false;
                        mIsFetchListViewErr = true;
                        if (mIsFetchListViewErr && mIsFetchWemediaErr && ListUtils.isEmpty(mAdapter.getDataList()) && ListUtils.isEmpty(mWeMediaList)) {
                            if (error != null && error instanceof NetworkError) {
                                updateViewStatus(Status.REQUEST_NET_FAIL);
                            } else {
                                updateViewStatus(Status.DATA_ERROR);
                            }
                            return;
                        }
                        if (status == Status.LOAD_MORE) {
                            mPullToRefreshListView.hideFootView();
                            if (net) {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                            }
                        } else if (status == Status.REFRESH) {
                            mPullToRefreshListView.onRefreshComplete();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mPullToRefreshListView.onRefreshComplete();
                            if (ListUtils.isEmpty(mHeaderViewPagerAdapter.getData())) {
                                mHeadFlowView.setVisibility(View.GONE);
                            }
                            if (error != null && error instanceof NetworkError) {
                                listAdapter4MixTextPic.setmIsNetErr(true);
                            } else {
                                listAdapter4MixTextPic.setmIsNetErr(false);
                            }
                            listAdapter4MixTextPic.setIsShowBodyErr(true);
                            listAdapter4MixTextPic.notifyDataSetChanged();
                        }
                    }
                });


        PageActionTracker.pullCh(status == Status.LOAD_MORE, mChannel_id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_recycle_err:
                mIsFetchWemediaErr = false;
                mRecyclerHeadErr_img.setImageResource(R.drawable.icon_common_loading);
                mRecyclerHeadErr_tv.setText(R.string.common_onloading);
                fetchWeMediaData();
                break;
            case R.id.rl_all_tv:
                Intent intent = new Intent(mActivity, AllWeMediaActivity.class);
                intent.putExtra(IntentKey.LOCATE_TO_TV, true);
                intent.putExtra(IntentKey.E_CHID, mChannel_id);
                startActivity(intent);
                PageActionTracker.clickHomeChBtn(ActionIdConstants.CLICK_TV_ALL, mChannel_id);
                break;

        }
    }

    private void toWeMediaHomePageActivity(String weMediaID) {
        Intent intent = new Intent(mActivity, WeMediaHomePageActivity.class);
        intent.putExtra(IntentKey.WE_MEIDA_ID, weMediaID);
        intent.putExtra(IntentKey.E_CHID, mChannel_id);
        startActivity(intent);
    }

    @Override
    public void dispatchClickEvent(ChannelBean.HomePageBean bean) {
        super.dispatchClickEvent(bean);
        if (bean != null) {
            bean.setWatched(true);
        }
    }

    private void initAdapter() {
        mAdapter = new ListAdapter4MixTextPic(getActivity(), mChannel_id) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (isShowBodyErr()) {
                    View view = LayoutInflater.from(mContext).inflate(R.layout.ifeng_tv_body_err, parent, false);
                    final ImageView imageView = (ImageView) view.findViewById(R.id.iv_status_ifeng_tv);
                    final TextView textView = (TextView) view.findViewById(R.id.tv_status_hint_ifengtv);
                    if (ismIsNetErr()) {
                        imageView.setImageResource(R.drawable.commen_net_err);
                    } else {
                        imageView.setImageResource(R.drawable.commen_load_data_err);
                    }
                    view.findViewById(R.id.rl_container).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            imageView.setImageResource(R.drawable.icon_common_loading);
                            textView.setText(R.string.common_onloading);
                            mUpTimes = 0;
                            requestNet(requireTime, DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
                        }
                    });
                    return view;
                } else {
                    return super.getView(position, convertView, parent);
                }
            }
        };
        listAdapter4MixTextPic = (ListAdapter4MixTextPic) mAdapter;
        mHeaderViewPagerAdapter = new HeaderViewPagerAdapter(this, mActivity, mChannel_id);
        mPullToRefreshListView.setAdapter(mAdapter);
        mHeadFlowView.setViewPagerAdapter(mHeaderViewPagerAdapter);
        mRecyclerAdapter = new RecyclerView.Adapter<RecyclerHolder>() {

            @Override
            public RecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(mActivity).inflate(R.layout.item_we_media, viewGroup, false);
                RecyclerHolder holder = new RecyclerHolder(view);
                holder.mNetworkImageView = (NetworkImageView) view.findViewById(R.id.img_column);
                holder.mTextView = (TextView) view.findViewById(R.id.tv_column_name);
                view.setOnClickListener(new OnInnerItemClickListener() {
                    @Override
                    public void onItemInnerClick(View view, int positon) {
                        WeMediaList.WeMediaListEntity entity = mWeMediaList.get(positon);
                        toWeMediaHomePageActivity(entity.getWeMediaID());
                        PageActionTracker.clickHomeChBtn(ActionIdConstants.CLICK_TV_ITEM, mChannel_id);
                    }
                });
                return holder;
            }

            @Override
            public void onBindViewHolder(RecyclerHolder recyclerHolder, int position) {
                WeMediaList.WeMediaListEntity entity = mWeMediaList.get(position);
                if (entity != null) {
                    recyclerHolder.mTextView.setText(entity.getName());
                    recyclerHolder.mNetworkImageView.setImageUrl(entity.getHeadPic(), VolleyHelper.getImageLoader());
                    recyclerHolder.mNetworkImageView.setDefaultImageResId(R.drawable.avatar_default);
                    recyclerHolder.mNetworkImageView.setErrorImageResId(R.drawable.avatar_default);
                    CommonStatictisListUtils.getInstance().addRecyclerViewFocusList(entity.getWeMediaID(), position,
                            PageIdConstants.REFTYPE_EDITOR, mChannel_id, CommonStatictisListUtils.RECYVLERVIEW_TYPE, "", "", "");
                }
                recyclerHolder.itemView.setTag(R.id.tag_key_click, position);
            }

            @Override
            public int getItemCount() {
                if (mWeMediaList != null) {
                    return mWeMediaList.size();
                }
                return 0;
            }
        };
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        super.onRefresh(refreshView);
        if (mHeaderViewPagerAdapter != null) {
            mHeaderViewPagerAdapter.increaseRefreshTimes();
        }
        mUpTimes = 0;
        if (listAdapter4MixTextPic.isShowBodyErr()) {
            requestNet(requireTime, DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
        } else {
            requestNet(requireTime, DataInterface.PAGESIZE_20, Status.REFRESH, ChannelDao.REFRESH, ChannelConstants.DOWN, "");
        }
    }

    @Override
    public void onLastItemVisible() {
        super.onLastItemVisible();
        if (mAdapter != null && mAdapter.getLastItem() != null) {
            ChannelBean.HomePageBean bean = mAdapter.getLastItem();
            if (!TextUtils.isEmpty(bean.getItemId())) {
                requestNet(bean.getItemId(), DataInterface.PAGESIZE_20, Status.LOAD_MORE, ChannelDao.LOAD_MORE, ChannelConstants.UP, "");
            }
        }

    }

    private class RecyclerHolder extends RecyclerView.ViewHolder {

        private RecyclerHolder(View itemView) {
            super(itemView);
        }

        private TextView mTextView;
        private NetworkImageView mNetworkImageView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFocusList = CommonStatictisListUtils.ifengTvFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(mFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.ifengTvFocusList.clear();
    }
}
