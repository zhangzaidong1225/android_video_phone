package com.ifeng.newvideo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.videoplayer.fragment.CommentFragment;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.model.PlayerInfoModel;

/**
 * Created by jieyz on 2015/12/14.
 */
public class CommentActivity extends BaseFragmentActivity implements View.OnClickListener {

    private FragmentManager mFragmentManager;
    private PlayerInfoModel mCurrentPlayerInfoModel;
    private RelativeLayout mSendLayout;
    public String echid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_container);
        ImageView aboutBack = (ImageView) findViewById(R.id.back);
        aboutBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.video_comment));
        enableExitWithSlip(true);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        Intent intent = getIntent();
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction tr = mFragmentManager.beginTransaction();

        Bundle bundle = intent.getBundleExtra(IntentKey.BUNDLE_EXTRAS);
        echid = bundle.getString(IntentKey.E_CHID);
        bundle.putBoolean(IntentKey.IS_CONVERT_SHAREINFO, false);
        mCurrentPlayerInfoModel = bundle.getParcelable(IntentKey.CURRENT_PROGRAM);
        Fragment commentFragment = CommentFragment.newInstance(bundle, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        tr.add(R.id.activity_comment_frame, commentFragment, CommentActivity.class.getSimpleName());
        tr.commit();

        mSendLayout = (RelativeLayout) findViewById(R.id.activity_comment_send_ll);
        mSendLayout.setOnClickListener(this);
    }
//
//    @Override
//    public PlayerInfoModel callGetCurrentProgram() {
//        return mCurrentPlayerInfoModel;
//    }
//
//    @Override
//    public void callShowEditCommentWindow(String comments) {
//        if (commentEditFragment == null) {
//            commentEditFragment = new CommentEditFragment();
//        }
//        commentEditFragment.showCommentDialog(comments);
//    }
//
//    @Override
//    public CommentFragment callGetCommentFragment() {
//        return (CommentFragment) mFragmentManager.findFragmentByTag(CommentActivity.class.getSimpleName());
//    }
//
//    @Override
//    public void callEditCommentWindowShow() {
//
//    }
//
//    @Override
//    public void callEditCommentWindowHide() {
//
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_comment_send_ll:
                if (!NetUtils.isNetAvailable(this)) {
                    return;
                }
                //callShowEditCommentWindow("");
                break;
        }
    }
}
