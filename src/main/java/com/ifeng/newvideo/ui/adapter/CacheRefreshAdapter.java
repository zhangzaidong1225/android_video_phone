package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.videoplayer.activity.listener.VideoDetailDownloadClickListener;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.CacheBaseModel;

import java.util.ArrayList;
import java.util.List;

public class CacheRefreshAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private Context mContext;
    private List<VideoItem> mList = new ArrayList<>();
    private String guid;
    private int selectItem = -1;
    private int downloadedItem = -1;

    private static final int DOWNLOAD_DEFFAULT = 310;
    private static final int DOWNLOADING = 311;
    private static final int DOWNLOADED = 312;

    private List<CacheBaseModel> cacheVideoModels = new ArrayList<>();
    private List<CacheBaseModel> cacheVideoIngModels = new ArrayList<>();
    private List<Integer> downloadList = new ArrayList<>();


    public CacheRefreshAdapter(Context context, List<VideoItem> mList, String guid) {
        inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mList = mList;
        this.guid = guid;

    }

    public void getCacheList() {
        cacheVideoModels.clear();
        cacheVideoIngModels.clear();
        cacheVideoModels.addAll(CacheManager.getDownloadAllList(false));
        cacheVideoIngModels.addAll(CacheManager.getDownloadingList(false));
        if (mCacheVideoNo != null) {
            mCacheVideoNo.onStartDownloading(cacheVideoIngModels.size());
        }
    }

    @Override
    public int getCount() {
        if (!ListUtils.isEmpty(mList)) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        CacheHolder holder = null;
        OnClick onClick = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_cache_recycler, viewGroup, false);
            onClick = new OnClick();
            holder = new CacheHolder();
            holder.itemView = convertView.findViewById(R.id.view_parent);
            holder.tv_timer = (TextView) convertView.findViewById(R.id.tv_timer);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.iv_download = (ImageView) convertView.findViewById(R.id.iv_download_status);
            convertView.setTag(R.id.view_parent, onClick);
            convertView.setTag(mList.get(position));
            convertView.setTag(R.id.tag_key_click, holder);
        } else {
            holder = (CacheHolder) convertView.getTag(R.id.tag_key_click);
            onClick = (OnClick) convertView.getTag(R.id.view_parent);
        }

        onClick.setPosition(position);
        holder.itemView.setOnClickListener(onClick);

        VideoItem videoItem = mList.get(position);
        holder.tv_timer.setText(videoItem.createDate);
        holder.tv_title.setText(videoItem.title);

        for (CacheBaseModel model : cacheVideoIngModels) {
            if (model.getGuid().equals(videoItem.guid)) {
                downloadList.set(position, DOWNLOADING);
                break;
            }
        }

        for (CacheBaseModel model : cacheVideoModels) {
            if (model.getGuid().equals(videoItem.guid)) {
                downloadList.set(position, DOWNLOADED);
                break;
            }
        }


        if (downloadList.get(position) == DOWNLOAD_DEFFAULT) {
            holder.iv_download.setVisibility(View.GONE);
        } else {
            holder.iv_download.setVisibility(View.VISIBLE);
        }

        if (position == selectItem) {
            if (downloadList.get(position) == DOWNLOADING) {
                holder.iv_download.setImageResource(R.drawable.iv_download);
            } else if (downloadList.get(position) == DOWNLOADED) {
                holder.iv_download.setImageResource(R.drawable.iv_download_over);
            }
            holder.tv_title.setTextColor(Color.parseColor("#F54343"));
        } else if (position == downloadedItem) {
            if (downloadList.get(position) == DOWNLOADING) {
                holder.iv_download.setImageResource(R.drawable.iv_download);
            } else if (downloadList.get(position) == DOWNLOADED) {
                holder.iv_download.setImageResource(R.drawable.iv_download_over);
                holder.tv_title.setTextColor(Color.parseColor("#262626"));
            }
        } else {
            if (downloadList.get(position) == DOWNLOADING) {
                holder.iv_download.setImageResource(R.drawable.iv_download);
            } else if (downloadList.get(position) == DOWNLOADED) {
                holder.iv_download.setImageResource(R.drawable.iv_download_over);
                holder.tv_title.setTextColor(Color.parseColor("#262626"));
            }
            holder.tv_title.setTextColor(Color.parseColor("#000000"));
        }

        return convertView;
    }

    private static class CacheHolder {
        private View itemView;
        private TextView tv_timer;
        private TextView tv_title;
        private ImageView iv_download;
    }

    class OnClick implements View.OnClickListener {

        private int position;

        public void setPosition(int position) {
            this.position = position;
        }


        @Override
        public void onClick(View view) {
            if (ListUtils.isEmpty(mList)) {
                return;
            }

            if (downloadList.get(position) == DOWNLOAD_DEFFAULT) {
                setDownloadItem(position, DOWNLOADING);
                new VideoDetailDownloadClickListener(mContext, IfengType.TYPE_VIDEO).onClick(view);
                if (mCacheVideoNo != null) {
                    mCacheVideoNo.onStartDownloading(1);
                }
                view.setEnabled(true);

            } else if (downloadList.get(position) == DOWNLOADING) {
                ToastUtils.getInstance().showShortToast(mContext.getResources().getString(R.string.video_cache_ing));
            } else if (downloadList.get(position) == DOWNLOADED) {
                ToastUtils.getInstance().showShortToast(mContext.getResources().getString(R.string.video_cache_end));
            }
            notifyDataSetChanged();

        }
    }

    // 播放中
    public void setSelectItem(int selectItem) {
        this.selectItem = selectItem;
    }

    // 下载中
    public void setDownloadItem(int selectItem, int downloadStatus) {
        this.downloadedItem = selectItem;
        downloadList.set(selectItem, downloadStatus);

    }

    // 下载完成
    public void setDownloadStatusList(List<VideoItem> dataList) {
        for (int i = 0; i < dataList.size(); i++) {
            downloadList.add(DOWNLOAD_DEFFAULT);
        }
    }

    private downloadCacheNo mCacheVideoNo;

    public interface downloadCacheNo {
        void onStartDownloading(int num);
    }

    public void setDownloadVideo(downloadCacheNo listener) {
        this.mCacheVideoNo = listener;
    }
}
