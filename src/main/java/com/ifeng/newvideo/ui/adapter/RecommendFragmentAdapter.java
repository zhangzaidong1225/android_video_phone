package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.bean.OnInnerItemClickListener;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.statistics.domains.AdVideoRecord;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter;
import com.ifeng.newvideo.ui.adapter.holder.AdBigPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdBigPictureChannelHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdMixTextPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdVideoLargeHolder;
import com.ifeng.newvideo.ui.adapter.holder.BigPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.KKHolder;
import com.ifeng.newvideo.ui.adapter.holder.MixTextPictureItemHolder;
import com.ifeng.newvideo.ui.adapter.holder.SlideHolder;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.newvideo.utils.ViewUtils;
import com.ifeng.newvideo.videoplayer.bean.BigAdItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.widget.CustomNetworkImageView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BitmapUtils;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.List;

public class RecommendFragmentAdapter extends ChannelBaseAdapter {

    //上次刷新的位置
    private int scan_note_position = -1;
    private int mItemViewTypeCount;
    private static final int BIG_PICTURE = 5;
    private static final int MIX_TEXT_PICTURE = 6;
    private static final int NORMAL_CELL3 = 7;
    private static final int NORMAL_BANNER = 8;
    // 上次看到这里，点击刷新
    private static final int SCAN_NOTE = 9;
    //    private static final int SLIDE = 10;
    private static final int AD_BIG_VIDEO = 10;

    public RecommendFragmentAdapter(int itemViewTypeCount, Context context, String channel_id) {
        mItemViewTypeCount = itemViewTypeCount;
        this.mContext = context;
        this.mChannelId = channel_id;
    }

    @Override
    public boolean addData(List<ChannelBean.HomePageBean> list, boolean add2head) {
        if (add2head && !ListUtils.isEmpty(list)) {
            scan_note_position = list.size();
        }
        return super.addData(list, add2head);
    }

    @Override
    public int getCount() {
        if (mDataList != null) {
            if (scan_note_position > 0) {
                return mDataList.size() + 1;
            } else {
                return mDataList.size();
            }
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (ListUtils.isEmpty(mDataList)) {
            return null;
        }
        if (scan_note_position == position) {
            return "position_note";
        }
        if (scan_note_position > 0 && position > scan_note_position) {
            position--;
        }
        return mDataList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);
        ChannelBean.HomePageBean homePageBean = null;
        if (scan_note_position != position) {
            if (scan_note_position > 0 && position > scan_note_position) {
                position--;
            }
        }

        homePageBean = mDataList.get(position);
        if (!CheckIfengType.isAD(homePageBean.getMemberType())) {
            CommonStatictisListUtils.getInstance().addPullRefreshViewFocusList(homePageBean.getInfoId(), position, PageIdConstants.REFTYPE_EDITOR, mChannelId,
                    type, CommonStatictisListUtils.HEADERVIEW_TYPE, CommonStatictisListUtils.wellChosen, homePageBean.getMemberItem().getSimId(), homePageBean.getMemberItem().getrToken(), homePageBean.getMemberItem().getPayload());
        }

        return getView(convertView, parent, homePageBean, type, position);
    }

    private View getView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bean, int type, int position) {
        String tag = null;
        String imgUrl = null;
        String name = null;
        String headPic = null;
        List<HomePageBeanBase.ImageBean> imageList = null;
        String playTimes = null;
        String abstractDesc = null;
        String title = null;
        String when = null;
        int duration = 0;
        if (bean != null) {
            imageList = bean.getImageList();
            abstractDesc = bean.getAbstractDesc();
            // 标签
            tag = bean.getTag();
            if (CheckIfengType.isAdBackend(bean.getMemberType())) {
                boolean isShowAdIcon = bean.getMemberItem().icon.showIcon == 1;
                tag = isShowAdIcon ? bean.getMemberItem().icon.text : "";
                abstractDesc = bean.getMemberItem().vdescription;
            }

            imgUrl = getImageUrl(bean);

            if (CheckIfengType.isAD(bean.getMemberType())) {
                title = bean.getTitle();
                if (SharePreUtils.getInstance().getVisibleMap().containsKey(mChannelId)) {
                    if (SharePreUtils.getInstance().getVisibleMap().get(mChannelId)) {
                        AdTools.exposeAdPvUrl(bean);
                    }
                }
                ADRecord.addAdShow(bean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
                CommonStatictisListUtils.getInstance().sendADInfo(bean.getMemberItem().getAdPositionId(), bean.getInfoId(), mChannelId);
            } else {
                ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
                ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
                if (weMedia != null && !TextUtils.isEmpty(weMedia.getId()) && !TextUtils.isEmpty(weMedia.getName())) {
                    name = weMedia.getName();
                    headPic = weMedia.getHeadPic();
                }
                if (memberItemBean != null) {
                    playTimes = memberItemBean.getPlayTime();
                    if (!TextUtils.isEmpty(playTimes)) {
                        playTimes = StringUtils.changePlayTimes(playTimes);
                    }
                }
                title = bean.getTitle();
                AdTools.loadImpressionUrl(bean);
                CommonStatictisListUtils.getInstance().sendHomeFeedYoukuConstatic(bean, CommonStatictisListUtils.YK_EXPOSURE);
            }
        }
        switch (type) {
            case AD_BANNER:
                if (convertView == null || !(convertView.getTag() instanceof KKHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kk_live, parent, false);
                }
                configAdBannerConvertView(convertView, tag, title, imgUrl, bean.getMemberItem().kkrand);
                break;

            case AD_MIX_TEXT_PIC:
                if (convertView == null || !(convertView.getTag() instanceof AdMixTextPicHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_mix_text_pic, parent, false);
                }
                configAdMixTextPicConvertView(convertView, tag, title, bean.getMemberItem().adConditions.showType, imgUrl, abstractDesc);
                convertView.setTag(R.id.tag_key_click, bean);
                break;
            case AD_CELL_3:
                if (convertView == null || !(convertView.getTag() instanceof AdMixTextPicHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_cell_3_pic, parent, false);
                }
                configAdCell3ConvertView(convertView, tag, title, abstractDesc, bean.getMemberItem().photos);
                break;
            case AD_BIG_PIC:
                if (convertView == null || !(convertView.getTag() instanceof AdBigPicHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_big_pic, parent, false);
                }
                configAdBigPic(convertView, tag, title, imgUrl, abstractDesc);
                break;
            case AD_VIDEO_LARGE:
                if (convertView == null || !(convertView.getTag() instanceof AdVideoLargeHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_video_large, parent, false);
                }
                configAdVideoLarge(convertView, tag, title, imgUrl, abstractDesc);
                break;
            case BIG_PICTURE:
                convertView = getBigPicView(convertView, parent, bean, imgUrl, name, headPic, playTimes, title, when);
                break;
            case MIX_TEXT_PICTURE:
                convertView = getMixTextPicView(convertView, parent, bean, imgUrl, name, headPic, playTimes, title, when, duration);
                break;
            case NORMAL_CELL3:
                convertView = getNormalCell3View(convertView, parent, bean, name, headPic, imageList, playTimes, title);
                break;
            case NORMAL_BANNER:
                convertView = getNormalBannerView(convertView, parent, bean, imgUrl, name, headPic, playTimes, title, when);
                break;
            case SCAN_NOTE:
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_listview_last_position, parent, false);
                break;
//            case SLIDE:
//                convertView = getSlidePicView(convertView, parent, bean, imgUrl, name, headPic,playTimes, title,when, duration);
//                break;
            case AD_BIG_VIDEO:
                convertView = getADBigPicView(convertView, parent, bean, title);
                convertView.setTag(R.id.tag_key_click, position);
                break;

            case AD_EMPTY_TYPE:
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_ad_empty_layout, parent, false);
//                CommonStatictisListUtils.getInstance().sendADEmptyExpose(bean.getMemberItem().adPositionId);
                break;
        }
        return convertView;
    }

    private View getBigPicView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean, String imgUrl,
                               String name, String headPic, String playTimes, String title, String when) {
        int duration;
        if (convertView == null || !(convertView.getTag() instanceof BigPicHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.well_chosen_normal_big_pic, parent, false);
        }
        BigPicHolder bigPicHolder = BigPicHolder.getBigPicHolder(convertView);
        markTextGray(bigPicHolder.title, bodyListBean);
        if (TextUtils.isEmpty(title)) {
            bigPicHolder.title.setVisibility(View.GONE);
        } else {
            bigPicHolder.title.setText(title);
            bigPicHolder.title.setVisibility(View.VISIBLE);
        }
//        if (TextUtils.isEmpty(headPic) && TextUtils.isEmpty(name)) {
//            bigPicHolder.avatar.setVisibility(View.GONE);
//        } else {
//            setAvatar(bigPicHolder.avatar, headPic);
//            bigPicHolder.avatar.setVisibility(View.VISIBLE);
//        }
        if (TextUtils.isEmpty(name)) {
            bigPicHolder.author.setVisibility(View.GONE);
        } else {
            bigPicHolder.author.setText(name);
            bigPicHolder.author.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(playTimes)) {
            bigPicHolder.playTimes.setVisibility(View.GONE);
        } else {
            bigPicHolder.playTimes.setText(playTimes);
            bigPicHolder.playTimes.setVisibility(View.VISIBLE);
        }
        String status = getLiveStatus(bodyListBean, bigPicHolder.liveStatus);
        if (TextUtils.isEmpty(status)) {
            bigPicHolder.liveStatus.setVisibility(View.GONE);
        } else {
            bigPicHolder.liveStatus.setText(status);
            bigPicHolder.liveStatus.setVisibility(View.VISIBLE);
        }
        if (bodyListBean != null && !TextUtils.isEmpty(bodyListBean.getUpdateDate())) {
            when = DateUtils.parseTime(LiveUtils.getCurrentTime() + "", bodyListBean.getUpdateDate());
        }
        if (TextUtils.isEmpty(when)) {
            bigPicHolder.when.setVisibility(View.GONE);
        } else {
            bigPicHolder.when.setText(when);
            bigPicHolder.when.setVisibility(View.VISIBLE);
        }

        if (bodyListBean != null) {
            duration = bodyListBean.getMemberItem().getDuration();
            if (duration > 0) {
                bigPicHolder.duration.setText(DateUtils.getTimeStr(duration));
                bigPicHolder.duration.setVisibility(View.VISIBLE);
            } else {
                bigPicHolder.duration.setVisibility(View.GONE);
            }
        } else {
            bigPicHolder.duration.setVisibility(View.GONE);
        }
        showOnLine(bodyListBean, bigPicHolder.onlineCount);
        setTag(bigPicHolder.label, bodyListBean);
        setImageUrl(bigPicHolder.picture, imgUrl, R.drawable.bg_default_pic);
        return convertView;
    }

    private View getNormalBannerView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean,
                                     String imgUrl, String name, String headPic, String playTimes, String title, String when) {
        if (convertView == null || !(convertView.getTag() instanceof LiveHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_listview_well_chosen_banner, parent, false);
        }
        LiveHolder liveHolder = (LiveHolder) getHolder(convertView, NORMAL_BANNER);
        if (TextUtils.isEmpty(title)) {
            liveHolder.title.setVisibility(View.GONE);
        } else {
            markTextGray(liveHolder.title, bodyListBean);
            liveHolder.title.setText(title);
            liveHolder.title.setVisibility(View.VISIBLE);
        }
        setImageUrl(liveHolder.banner, imgUrl, R.drawable.tab_live_item_bg);
        setTag(liveHolder.tag, bodyListBean);
        String status_3 = getLiveStatus(bodyListBean, liveHolder.liveStatus);
        if (TextUtils.isEmpty(status_3)) {
            liveHolder.liveStatus.setVisibility(View.GONE);
        } else {
            liveHolder.liveStatus.setVisibility(View.VISIBLE);
            liveHolder.liveStatus.setText(status_3);
        }
        if (TextUtils.isEmpty(headPic) && TextUtils.isEmpty(name)) {
            liveHolder.head.setVisibility(View.GONE);
            liveHolder.head_mask.setVisibility(View.GONE);
        } else {
            setImageUrl(liveHolder.head, headPic, R.drawable.avatar_default);
            liveHolder.head.setVisibility(View.VISIBLE);
            liveHolder.head_mask.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(name)) {
            liveHolder.name.setVisibility(View.GONE);
        } else {
            liveHolder.name.setText(name);
            liveHolder.name.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(playTimes)) {
            liveHolder.play_times.setVisibility(View.GONE);
        } else {
            liveHolder.play_times.setText(playTimes);
            liveHolder.play_times.setVisibility(View.VISIBLE);
        }
        if (bodyListBean != null) {
            when = DateUtils.parseTime(LiveUtils.getCurrentTime() + "", bodyListBean.getUpdateDate());
        }
        if (TextUtils.isEmpty(when)) {
            liveHolder.when.setVisibility(View.GONE);
        } else {
            liveHolder.when.setText(when);
            liveHolder.when.setVisibility(View.VISIBLE);
        }
        showOnLine(bodyListBean, liveHolder.online_count);
        return convertView;
    }

    private View getNormalCell3View(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean, String name,
                                    String headPic, List<HomePageBeanBase.ImageBean> imageList, String playTimes, String title) {
        String when;
        if (convertView == null || !(convertView.getTag() instanceof AlbumHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.normal_cell_3_pic, parent, false);
        }
        AlbumHolder albumHolder = (AlbumHolder) getHolder(convertView, NORMAL_CELL3);
        if (TextUtils.isEmpty(title)) {
            albumHolder.title.setVisibility(View.GONE);
        } else {
            albumHolder.title.setVisibility(View.VISIBLE);
            markTextGray(albumHolder.title, bodyListBean);
            albumHolder.title.setText(title);
        }
        if (TextUtils.isEmpty(playTimes)) {
            albumHolder.play_times.setVisibility(View.GONE);
        } else {
            albumHolder.play_times.setText(playTimes);
            albumHolder.play_times.setVisibility(View.VISIBLE);
        }
        when = DateUtils.parseTime(LiveUtils.getCurrentTime() + "", bodyListBean.getUpdateDate());
        if (!TextUtils.isEmpty(when)) {
            albumHolder.when.setText(when);
            albumHolder.when.setVisibility(View.VISIBLE);
        } else {
            albumHolder.when.setVisibility(View.GONE);
        }
        String status_1 = getLiveStatus(bodyListBean, albumHolder.live_status);
        if (TextUtils.isEmpty(status_1)) {
            albumHolder.live_status.setVisibility(View.GONE);
        } else {
            albumHolder.live_status.setVisibility(View.VISIBLE);
            albumHolder.live_status.setText(status_1);
        }
        setTag(albumHolder.tag, bodyListBean);
        String imgUrl0 = "";
        String imgUrl1 = "";
        String imgUrl2 = "";
        if (!ListUtils.isEmpty(imageList)) {
            imgUrl0 = imageList.get(0).getImage();
            if (imageList.size() > 1) {
                imgUrl1 = imageList.get(1).getImage();
            }
            if (imageList.size() > 2) {
                imgUrl2 = imageList.get(2).getImage();
            }
        }
        setImageUrl(albumHolder.pic_0, imgUrl0, R.drawable.bg_default_mid);
        setImageUrl(albumHolder.pic_1, imgUrl1, R.drawable.bg_default_mid);
        setImageUrl(albumHolder.pic_2, imgUrl2, R.drawable.bg_default_mid);
        if (TextUtils.isEmpty(headPic) && TextUtils.isEmpty(name)) {
            albumHolder.cell_3_head.setVisibility(View.GONE);
            albumHolder.cell_3_head_mask.setVisibility(View.GONE);
        } else {
            setImageUrl(albumHolder.cell_3_head, headPic, R.drawable.bg_default_mid);
            albumHolder.cell_3_head.setVisibility(View.VISIBLE);
            albumHolder.cell_3_head_mask.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(name)) {
            albumHolder.wemedia_name.setVisibility(View.GONE);
        } else {
            albumHolder.wemedia_name.setText(name);
            albumHolder.wemedia_name.setVisibility(View.VISIBLE);
        }
        showOnLine(bodyListBean, albumHolder.online_count);
        return convertView;
    }

    private View getMixTextPicView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean, String imgUrl,
                                   String name, String headPic, String playTimes, String title, String when, int duration) {
        if (convertView == null || !(convertView.getTag() instanceof MixTextPictureItemHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_listview_mix_text_picture, parent, false);
        }
        MixTextPictureItemHolder textPictureItemHolder = MixTextPictureItemHolder.getHolder(convertView);
        markTextGray(textPictureItemHolder.title, bodyListBean);
        textPictureItemHolder.title.setText(title);
        textPictureItemHolder.author.setText(name);
        if (TextUtils.isEmpty(playTimes)) {
            textPictureItemHolder.playTimes.setVisibility(View.GONE);
        } else {
            textPictureItemHolder.playTimes.setText(playTimes);
            textPictureItemHolder.playTimes.setVisibility(View.VISIBLE);
        }
        if (bodyListBean != null) {
            duration = bodyListBean.getMemberItem().getDuration();
        }
        if (duration > 0) {
            textPictureItemHolder.duration.setText(DateUtils.getTimeStr(duration));
        }
        setTag(textPictureItemHolder.label, bodyListBean);
        String status_2 = getLiveStatus(bodyListBean, textPictureItemHolder.liveStatus);
        if (TextUtils.isEmpty(status_2)) {
            textPictureItemHolder.liveStatus.setVisibility(View.GONE);
        } else {
            textPictureItemHolder.liveStatus.setText(status_2);
            textPictureItemHolder.liveStatus.setVisibility(View.VISIBLE);
        }
        /**
         *  要求左文右图板式的点播和专辑不显示假时间
         */
        if (!CheckIfengType.isVideo(bodyListBean.getMemberType()) && !CheckIfengType.isTopicType(bodyListBean.getMemberType())) {
            when = DateUtils.parseTime(LiveUtils.getCurrentTime() + "", bodyListBean.getUpdateDate());
        }
        if (TextUtils.isEmpty(when)) {
            textPictureItemHolder.when.setVisibility(View.GONE);
        } else {
            textPictureItemHolder.when.setText(when);
            textPictureItemHolder.when.setVisibility(View.VISIBLE);
        }
        setImageUrl(textPictureItemHolder.right_picture, imgUrl, R.drawable.bg_default_mid);
//        setImageUrl(textPictureItemHolder.avatar, headPic, R.drawable.avatar_default);
        textPictureItemHolder.duration.setVisibility(duration <= 0 ? View.GONE : View.VISIBLE);
        textPictureItemHolder.author.setVisibility(TextUtils.isEmpty(name) ? View.GONE : View.VISIBLE);
        int visible = TextUtils.isEmpty(headPic) && TextUtils.isEmpty(name) ? View.GONE : View.VISIBLE;
//        textPictureItemHolder.avatar.setVisibility(visible);
//        textPictureItemHolder.avatarMask.setVisibility(visible);

        showOnLine(bodyListBean, textPictureItemHolder.onLineCount);
        return convertView;
    }

    private List<ChannelBean.VideosBean> memList = new ArrayList<>();


    private View getSlideView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean, String imgUrl,
                              String name, String headPic, String playTimes, String title, String when, int duration) {
        if (convertView == null || !(convertView.getTag() instanceof SlideHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.well_chosen_slide_layout, parent, false);
        }
        SlideHolder slideHolder = SlideHolder.getSlideHolder(convertView);
        slideHolder.title.setText(title);
        RecyclerView recyclerView = slideHolder.recyclerView;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerView.Adapter slideRecyclerAdapter = new SlideRecyclerAdatper();
        List<ChannelBean.HomePageBean> bodyList = getDataList();
        if (bodyList != null) {
            memList = getSlideList(bodyList);
        }
        slideRecyclerAdapter.notifyDataSetChanged();

        recyclerView.setAdapter(slideRecyclerAdapter);


        return convertView;
    }

    private List getSlideList(List<ChannelBean.HomePageBean> list) {
        int size = list.size();
        if (!ListUtils.isEmpty(list)) {
            for (int i = 0; i < size; i++) {
                if (ChannelConstants.SLIDE_FORMAT.equalsIgnoreCase(list.get(i).getShowType())) {
                    memList = list.get(i).getMemberItem().getVideos();
                    return memList;
                }
            }
        }
        return memList;
    }

    private class SlideRecyclerAdatper extends RecyclerView.Adapter<SlideRecyclerAdatper.SlideRecyclerHolder> {

        @Override
        public SlideRecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View containerView = LayoutInflater.from(mContext).inflate(R.layout.item_well_chosen_slide, viewGroup, false);
            SlideRecyclerHolder slideRecyclerHolder = new SlideRecyclerHolder(containerView);
            containerView.setOnClickListener(new OnInnerItemClickListener() {
                @Override
                public void onItemInnerClick(View view, int positon) {
                    ChannelBean.VideosBean bean = memList.get(positon);
                    IntentUtils.toVodDetailActivity(mContext, bean.getGuid() != null ? bean.getGuid() : "",
                            mChannelId, false, false, 0, "");
                }
            });

            return slideRecyclerHolder;
        }

        @Override
        public void onBindViewHolder(SlideRecyclerHolder slideRecyclerHolder, int position) {
            ChannelBean.VideosBean entity = memList.get(position);
            if (entity != null) {
                slideRecyclerHolder.title.setText(entity.getTitle());
                slideRecyclerHolder.headerPic.setDefaultImageResId(R.drawable.bg_default_small);
                slideRecyclerHolder.headerPic.setErrorImageResId(R.drawable.bg_default_small);
                slideRecyclerHolder.headerPic.setImageUrl(entity.getImage(), VolleyHelper.getImageLoader());
                CommonStatictisListUtils.getInstance().addRecyclerViewFocusList(entity.getWeMediaId(), position, PageIdConstants.REFTYPE_EDITOR, mChannelId,
                        CommonStatictisListUtils.SLIDE_TYPE, "", "", "");
            }
            slideRecyclerHolder.itemView.setTag(R.id.tag_key_click, position);

        }

        @Override
        public int getItemCount() {
            if (!ListUtils.isEmpty(memList)) {
                return memList.size();
            }
            return 0;
        }

        class SlideRecyclerHolder extends RecyclerView.ViewHolder {
            TextView title;
            NetworkImageView headerPic;

            public SlideRecyclerHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.tv_slide_title);
                headerPic = (NetworkImageView) itemView.findViewById(R.id.iv_slide_pic);
            }
        }
    }


    private AdBigPictureChannelHolder adBigPictureChannelHolder;
    public AdVideoRecord adVideoRecord;

    public AdBigPictureChannelHolder getAdHolder() {
        return adBigPictureChannelHolder;
    }

    private View getADBigPicView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean, String title) {
        if (convertView == null || !(convertView.getTag() instanceof AdBigPictureChannelHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_well_big_picture_ad, parent, false);
        }
        ChannelBean.HomePageBean bean = bodyListBean;
        AdBigPictureChannelHolder holder = getHolder(convertView, bodyListBean);
        adBigPictureChannelHolder = holder;
        AdTools.loadImpressionUrl(bean);

        holder.title.setText(title);
        markTextGray(holder.title, bean);
        List<HomePageBeanBase.ImageBean> imageBeanList = bean.getImageList();
        if (!ListUtils.isEmpty(imageBeanList)) {
            setImageUrl(holder.networkImageView, imageBeanList.get(0).getImage(), R.drawable.bg_default_pic);
        }
        holder.tv_know.setText("了解详情");
//        holder.tv_use.setText("立即试驾");
        ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
        if (memberItemBean != null) {
            holder.tv_use.setText(memberItemBean.adConditions.stoptext);
            if (!TextUtils.isEmpty(memberItemBean.icon.text) && memberItemBean.icon.showIcon == 1) {
                holder.adTag.setText(memberItemBean.icon.text);
            } else {
                holder.adTag.setVisibility(View.GONE);
            }
            setImageUrl(holder.iv_ad, memberItemBean.adConditions.stopimageURL, R.drawable.bg_default_small);
        }

        return convertView;
    }

    private int mClickToPlayPositon = -1;

    public AdBigPictureChannelHolder getHolder(final View convertView, final ChannelBean.HomePageBean bodyListBean) {
        AdBigPictureChannelHolder holder = null;
        if (convertView.getTag() == null || !(convertView.getTag() instanceof AdBigPictureChannelHolder)) {
            holder = new AdBigPictureChannelHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_title);
            holder.playStatus = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_play_status_ad);
            holder.playStatus.setImageResource(R.drawable.btn_play);
            holder.networkImageView = (NetworkImageView) convertView.findViewById(R.id.cniv_channel_big_pic_ad);
            holder.networkImageView.setDefaultImageResId(R.drawable.bg_default_pic);
            holder.networkImageView.setErrorImageResId(R.drawable.login_ifeng_logo);
            holder.iv_ad = (CustomNetworkImageView) convertView.findViewById(R.id.iv_ad);
            holder.iv_ad.setDefaultImageResId(R.drawable.bg_default_pic);
            holder.iv_ad.setErrorImageResId(R.drawable.login_ifeng_logo);
            holder.tv_use = (TextView) convertView.findViewById(R.id.tv_ad_left_ad);
            holder.tv_know = (TextView) convertView.findViewById(R.id.tv_video_ad_detail);
            holder.adTag = (TextView) convertView.findViewById(R.id.tv_ad_tag);
            holder.download = (TextView) convertView.findViewById(R.id.tv_ad_download);

            View.OnClickListener toVideoListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }

                    IntentUtils.startADActivity(mContext, bodyListBean.getMemberItem().adId, bodyListBean.getMemberItem().adAction.url, "", ADActivity.FUNCTION_VALUE_AD,
                            bodyListBean.getTitle(), null, "", "", "", null, null);
                    recoverUI();
                }
            };

            holder.title.setOnClickListener(toVideoListener);


            final AdBigPictureChannelHolder finalHolder = holder;
            holder.playStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int position = (int) obj;

                    if (mClickToPlayPositon != -1 && mClickToPlayPositon != position) {
                        recoverUI();
                    }

                    HomePageBeanBase bean = mDataList.get(position);

                    if (bean != null) {
                        finalHolder.title.setTextColor(Color.parseColor("#a0a0a0"));
                        mPortraitPlayingContainer = (ViewGroup) view.getParent();
                        openVideo(bean, position, true);
                        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, true, PageIdConstants.PLAY_VIDEO_V);
                    }
                }
            });

            holder.tv_use.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IntentUtils.startADActivity(mContext, bodyListBean.getMemberItem().adId, bodyListBean.getMemberItem().adConditions.stopurl, "", ADActivity.FUNCTION_VALUE_AD,
                            bodyListBean.getTitle(), null, "", "", "", null, null);
                }
            });

            holder.tv_know.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IntentUtils.startADActivity(mContext, bodyListBean.getMemberItem().adId, bodyListBean.getMemberItem().adAction.url, "", ADActivity.FUNCTION_VALUE_AD,
                            bodyListBean.getTitle(), null, "", "", "", null, null);

                }
            });

            holder.download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }

                    int position = (int) obj;
                    HomePageBeanBase bean = mDataList.get(position);
                    ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();

                    List<String> downloadCompleteUrlList = new ArrayList<>();
                    if (!ListUtils.isEmpty(memberItemBean.adAction.async_downloadCompletedurl)) {
                        downloadCompleteUrlList.addAll(memberItemBean.adAction.async_downloadCompletedurl);
                    }
                    if (!ListUtils.isEmpty(memberItemBean.adAction.downloadCompletedurl)) {
                        downloadCompleteUrlList.addAll(memberItemBean.adAction.downloadCompletedurl);
                    }
                    AdvertExposureDao.sendAdvertClickReq(memberItemBean.adId, memberItemBean.adAction.async_click);

                    DownLoadUtils.download(IfengApplication.getAppContext(), memberItemBean.adId,
                            memberItemBean.adAction.loadingurl,
                            (ArrayList<String>) memberItemBean.adAction.async_download,
                            (ArrayList<String>) downloadCompleteUrlList
                    );
                }
            });

        } else {
            holder = (AdBigPictureChannelHolder) convertView.getTag();
        }

        return holder;
    }

    private ViewGroup mPortraitPlayingContainer;//横屏切换回竖屏时原listview的item的container
    public boolean isPlaying;
    private String currentPlayingFile;
    private int needReopenPositon = -1;

    private NormalVideoHelper mVideoHelper;
    private FrameLayout mWrapper;
    private UIPlayContext mUIPlayContext;

    public ViewGroup getPortraitPlayingContainer() {
        return mPortraitPlayingContainer;
    }

    public void setVideoHelper(NormalVideoHelper videoHelper) {
        this.mVideoHelper = videoHelper;
    }

    public void setVideoSkinWrapper(FrameLayout wrapper) {
        this.mWrapper = wrapper;
    }

    public void setUIPlayContext(UIPlayContext playContext) {
        this.mUIPlayContext = playContext;
    }

    public void autoPlayNext(final HomePageBeanBase bean, final int next_position, View itemView, boolean isPortrait) {
        if (itemView != null && itemView.getTag() instanceof AdBigPictureChannelHolder) {
            AdBigPictureChannelHolder holder = (AdBigPictureChannelHolder) itemView.getTag();
            mPortraitPlayingContainer = (ViewGroup) holder.playStatus.getParent();
        }
        openVideo(bean, next_position, isPortrait);
    }


    public void openVideo(HomePageBeanBase bean, int position, boolean portrait) {
        logger.debug("openVideo");
        if (bean == null) {
            return;
        }

        bean.setWatched(true);
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        if (memberItem == null) {
            return;
        }
        if (null == adVideoRecord) {
            adVideoRecord = new AdVideoRecord(PhoneConfig.IMEI, memberItem.adId, memberItem.adConditions.oid,
                    memberItem.adConditions.pid, memberItem.adConditions.videotime);
        }
        adVideoRecord.ptime = 0;
        adVideoRecord.startPlayTime();

        String videoUrl = memberItem.adConditions.videourl;
        if (!TextUtils.isEmpty(videoUrl)) {
            String title = bean.getTitle();
//            String stream = StreamUtils.getMediaUrlForPic(videoUrl);
//            String stream = videoUrl;
//            if (TextUtils.isEmpty(stream)) {
//                return;
//            }

            currentPlayingFile = videoUrl;
//            if (stream.equals(currentPlayingFile)) {
//                //自动联播时滑动状态下点击同一条目可能出现的问题
//                return;
//            } else {
//                currentPlayingFile = stream;
//            }
//            mUIPlayContext.videoFilesBeanList = videoFilesBeanList;
//            mUIPlayContext.streamType = StreamUtils.getStreamType();
            mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYING;
            List<HomePageBeanBase.ImageBean> imageList = bean.getImageList();
            String image = "";
            if (!ListUtils.isEmpty(imageList)) {
                image = imageList.get(0).getImage();
            }
            mUIPlayContext.image = image;
            mUIPlayContext.title = TextUtils.isEmpty(title) ? memberItem.getName() : title;
            mUIPlayContext.videoItem = getVideoItem(bean, image);
            mUIPlayContext.videoType = bean.getMemberItem().adConditions.showType;
            BigAdItem adItem = new BigAdItem(bean.getTitle(), bean.getMemberItem().adConditions.stoptext,
                    bean.getMemberItem().adConditions.stopurl, bean.getMemberItem().adAction.url, bean.getMemberItem().adConditions.stopimageURL);
            mUIPlayContext.adItem = adItem;

            if (position == needReopenPositon) {
                needReopenPositon = -1;
                mVideoHelper.reOpenVideo(currentPlayingFile);
            } else {
                mVideoHelper.openVideo(currentPlayingFile);
            }
            isPlaying = true;
            if (portrait) {
                ViewUtils.hideChildrenView(mPortraitPlayingContainer);
                ViewParent parent = mWrapper.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(mWrapper);
                }
                //皮肤显示
                mWrapper.setVisibility(View.VISIBLE);
                if (mPortraitPlayingContainer != null) {
                    mPortraitPlayingContainer.addView(mWrapper);
                }
            }
            mClickToPlayPositon = position;
            if (IfengApplication.mobileNetCanPlay && NetUtils.isMobile(mContext)) {
                ToastUtils.getInstance()
                        .showShortToast(R.string.play_moblie_net_hint);
            }
        }
    }


    public void recoverUI() {
        if (isPlaying) {
            if (mWrapper != null && mWrapper.getParent() != null) {
                ViewGroup group = (ViewGroup) mWrapper.getParent();
                group.removeView(mWrapper);
            }
            if (mPortraitPlayingContainer != null) {
                ViewUtils.showChildrenView(mPortraitPlayingContainer);
                mPortraitPlayingContainer = null;
            }
            currentPlayingFile = null;
            // 从头开始播放
            if (AdTools.videoType.equalsIgnoreCase(mUIPlayContext.videoType)) {
                mVideoHelper.setBookMark(0);
            }

            isPlaying = false;
            mUIPlayContext.videoType = "video";
            sendAdVideoStatictis();
        }
    }

    private void sendAdVideoStatictis() {
        if (adVideoRecord != null) {
            if (mUIPlayContext.status != IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                adVideoRecord.stopPlayTime();
            }
            if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED) {
                adVideoRecord.setPauseNum();
            }

            adVideoRecord.pcomplete = adVideoRecord.getCompleteNum();
            adVideoRecord.preplay = adVideoRecord.getPrePlayNum();
            adVideoRecord.pstop = adVideoRecord.getPauseNum();
            Log.d("advideo", "sendAdVideoStatidtis: --" + adVideoRecord.toString());

            CommonStatictisListUtils.getInstance().sendAdVideo(adVideoRecord);
            adVideoRecord.resetNum();
        }
    }

    private VideoItem getVideoItem(HomePageBeanBase bean, String imgUrl) {
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        VideoItem item = new VideoItem();
        item.guid = memberItem.getGuid();
        item.simId = memberItem.getSimId();
        item.cpName = memberItem.getCpName();
        item.title = TextUtils.isEmpty(bean.getTitle()) ? memberItem.getName() : bean.getTitle();//title为空取name
        item.searchPath = memberItem.getSearchPath();
        item.image = imgUrl;
        item.duration = memberItem.getDuration();
        return item;
    }

    public void back2PortraitAndContinuePlay() {

        if (isPlaying && mPortraitPlayingContainer != null) {
            ViewUtils.hideChildrenView(mPortraitPlayingContainer);
            if (mWrapper.getParent() != null) {
                ((ViewGroup) mWrapper.getParent()).removeView(mWrapper);
            }
            mPortraitPlayingContainer.addView(mWrapper);

            continuePlay();
        }
    }

    public void continuePlay() {
//        currentPlayingFile = StreamUtils.getMediaUrlForPic(mUIPlayContext.videoFilesBeanList);
        if (mVideoHelper != null && currentPlayingFile != null) {
            mVideoHelper.openVideo(currentPlayingFile);
        }
    }

    public int getClickToPlayPositon() {
        return mClickToPlayPositon;
    }

    private void setAvatar(final ImageView imageView, String iconUrl) {
        if (imageView == null) {
            return;
        }

        VolleyHelper.getImageLoader().get(iconUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    Bitmap bitmap = BitmapUtils.makeRoundCorner(response.getBitmap());
                    imageView.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Bitmap bitmap = BitmapUtils.makeRoundCorner(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.avatar_default));
                imageView.setImageBitmap(bitmap);
            }
        });
    }

    @Override
    public int getViewTypeCount() {
        return mItemViewTypeCount <= 0 ? 1 : mItemViewTypeCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (scan_note_position == position) {
            return SCAN_NOTE;
        }
        if (scan_note_position > 0 && position > scan_note_position) {
            position--;
        }
        ChannelBean.HomePageBean bean = mDataList.get(position);
        if (bean != null) {
            String showType = bean.getShowType();
            // 先判断adbackend，因为CheckIfengType.isAD中包含adbackend，先这么处理，后期有时间再细分
            if (CheckIfengType.isAdBackend(bean.getMemberType())) {
                if (AdTools.videoType.equalsIgnoreCase(bean.getMemberItem().adConditions.showType)) {
                    return AD_BIG_VIDEO;
                }
                //广告空曝光
                if (TextUtils.isEmpty(bean.getImageList().get(0).getImage())) {
                    return AD_EMPTY_TYPE;
                }
                return getAdShowType(bean.getMemberItem().adConditions.showType);
            }
            if (ChannelConstants.BIG_PICTURE_FORMAT.equalsIgnoreCase(showType)) {
                return BIG_PICTURE;
            }
            if (ChannelConstants.BANNER_FORMAT.equalsIgnoreCase(showType)) {
                return NORMAL_BANNER;
            }
            if (ChannelConstants.TOPIC_FORMAT.equalsIgnoreCase(showType)) {
                return NORMAL_CELL3;
            }
            if (ChannelConstants.STANDARD_FORMAT.equalsIgnoreCase(showType)) {
                return MIX_TEXT_PICTURE;
            }
//            if (ChannelConstants.SLIDE_FORMAT.equalsIgnoreCase(showType)) {
//                return SLIDE;
//            }

        }
        return BIG_PICTURE;
    }

    private Object getHolder(final View convertView, int type) {
        if (type == NORMAL_CELL3) {
            AlbumHolder albumHolder = null;
            if (convertView.getTag() == null) {
                albumHolder = new AlbumHolder();
                albumHolder.title = (TextView) convertView.findViewById(R.id.tv_ad_cell_3_title);
                albumHolder.pic_0 = (NetworkImageView) convertView.findViewById(R.id.niv_cell_1);
                albumHolder.pic_1 = (NetworkImageView) convertView.findViewById(R.id.niv_cell_2);
                albumHolder.pic_2 = (NetworkImageView) convertView.findViewById(R.id.niv_cell_3);
                albumHolder.pic_0.setDefaultImageResId(R.drawable.bg_default_mid);
                albumHolder.pic_1.setDefaultImageResId(R.drawable.bg_default_mid);
                albumHolder.pic_2.setDefaultImageResId(R.drawable.bg_default_mid);

                albumHolder.pic_0.setErrorImageResId(R.drawable.bg_default_mid);
                albumHolder.pic_1.setErrorImageResId(R.drawable.bg_default_mid);
                albumHolder.pic_2.setErrorImageResId(R.drawable.bg_default_mid);
                albumHolder.live_status = (TextView) convertView.findViewById(R.id.tv_normal_cell_3_live_status);
                albumHolder.cell_3_head = (NetworkImageView) convertView.findViewById(R.id.niv_nomal_cell_3_head);
                albumHolder.cell_3_head_mask = convertView.findViewById(R.id.niv_nomal_cell_3_head_mask);
                albumHolder.wemedia_name = (TextView) convertView.findViewById(R.id.tv_nomal_cell_3_name);
                albumHolder.play_times = (TextView) convertView.findViewById(R.id.tv_normal_cell_3_play_times);
                albumHolder.when = (TextView) convertView.findViewById(R.id.tv_normal_cell_3_scan_time);
                albumHolder.tag = (TextView) convertView.findViewById(R.id.tv_normal_cell_3_tag);
                albumHolder.online_count = (TextView) convertView.findViewById(R.id.tv_normal_cell_3_online_count);
                convertView.setTag(albumHolder);
                View.OnClickListener onInnerItemClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Object obj = convertView.getTag(R.id.tag_key_click);
                        if (obj == null || !(obj instanceof Integer)) {
                            return;
                        }
                        int positon = (int) obj;
                        HomePageBeanBase bean = mDataList.get(positon);
                        if (bean != null) {
                            ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
                            if (weMedia == null) {
                                return;
                            }
                            Intent intent = new Intent(mContext, WeMediaHomePageActivity.class);
                            intent.putExtra(IntentKey.WE_MEIDA_ID, weMedia.getId());
                            mContext.startActivity(intent);
                        }
                    }
                };
                albumHolder.cell_3_head.setOnClickListener(onInnerItemClickListener);
                albumHolder.wemedia_name.setOnClickListener(onInnerItemClickListener);
            } else {
                albumHolder = (AlbumHolder) convertView.getTag();
            }
            return albumHolder;
        } else if (type == NORMAL_BANNER) {
            LiveHolder liveHolder = null;
            if (convertView.getTag() == null) {
                liveHolder = new LiveHolder();
                liveHolder.title = (TextView) convertView.findViewById(R.id.tv_live_title);
                liveHolder.banner = (NetworkImageView) convertView.findViewById(R.id.niv_live_picture);
                liveHolder.liveStatus = (TextView) convertView.findViewById(R.id.tv_live_live_status);
                liveHolder.head = (NetworkImageView) convertView.findViewById(R.id.niv_live_head);
                liveHolder.head_mask = (ImageView) convertView.findViewById(R.id.iv_live_head_mask);
                liveHolder.name = (TextView) convertView.findViewById(R.id.tv_live_name);
                liveHolder.play_times = (TextView) convertView.findViewById(R.id.tv_live_play_times);
                liveHolder.when = (TextView) convertView.findViewById(R.id.tv_live_scan_time);
                liveHolder.tag = (TextView) convertView.findViewById(R.id.tv_live_tag);
                liveHolder.online_count = (TextView) convertView.findViewById(R.id.tv_well_chosen_banner_online_count);
                convertView.setTag(liveHolder);
            } else {
                liveHolder = (LiveHolder) convertView.getTag();
            }
            return liveHolder;
        }
        return null;
    }

    private void setTag(TextView view, HomePageBeanBase bodyListBean) {
        if (bodyListBean != null) {
            if (TextUtils.isEmpty(bodyListBean.getTag()) && !TextUtils.isEmpty(getLiveStatus(bodyListBean, view))) {
                view.setVisibility(View.GONE);
            } else {
                if (bodyListBean.getMemberItem() != null) {
                    String tag = TagUtils.getTagTextForList(bodyListBean.getTag(), bodyListBean.getMemberType(),
                            bodyListBean.getMemberItem().adAction.type);
                    view.setText(tag);
//                view.setBackgroundResource(TagUtils.getTagBackground(bodyListBean.getMemberType()));
                    view.setBackgroundResource(R.drawable.home_item_tag_red);
//                view.setTextColor(TagUtils.getTagTextColor(bodyListBean.getMemberType()));
                    view.setTextColor(Color.parseColor("#f54343"));
                    view.setVisibility(TextUtils.isEmpty(tag) ? View.GONE : View.VISIBLE);
                }
            }
        }
    }


    private class AlbumHolder {
        public TextView title;
        public NetworkImageView pic_0;
        public NetworkImageView pic_1;
        public NetworkImageView pic_2;
        public TextView live_status;
        public NetworkImageView cell_3_head;
        public View cell_3_head_mask;
        public TextView wemedia_name;
        public TextView play_times;
        public TextView when;
        public TextView tag;
        public TextView online_count;
    }

    private class LiveHolder {
        public TextView title;
        public NetworkImageView banner;
        public TextView liveStatus;
        public NetworkImageView head;
        public ImageView head_mask;
        public TextView name;
        public TextView play_times;
        public TextView when;
        public TextView tag;
        public TextView online_count;
    }


    private String getLiveStatus(HomePageBeanBase bean, TextView LiveStatusTv) {
        if (null != bean) {
            if (IfengType.TYPE_ADVERT.equalsIgnoreCase(bean.getMemberType())) {
                ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
                String status = memberItemBean.getLiveStatus();
                if ("0".equals(status)) {
                    LiveStatusTv.setBackgroundResource(R.drawable.tag_tv_complete);
                    LiveStatusTv.setTextColor(Color.parseColor("#262626"));
                    return "预告";
                } else if ("1".equals(status)) {
                    LiveStatusTv.setBackgroundResource(R.drawable.tag_tv_living);
                    LiveStatusTv.setTextColor(Color.parseColor("#ffffff"));
                    return "直播中";
                } else if ("2".equals(status)) {
                    LiveStatusTv.setBackgroundResource(R.drawable.tag_tv_complete);
                    LiveStatusTv.setTextColor(Color.parseColor("#999999"));
                    return "已结束";
                }
            }
        }
        return null;
    }

    private void showOnLine(ChannelBean.HomePageBean bodyListBean, TextView textView) {
        if (bodyListBean == null || textView == null) {
            return;
        }
        String memberType = bodyListBean.getMemberType();
        if (CheckIfengType.isLiveType(memberType) || CheckIfengType.isVRLive(memberType)) {
            ChannelBean.MemberItemBean memberItemBean = bodyListBean.getMemberItem();
            final String nolineNum = memberItemBean.getOnlineNo();
            if (memberItemBean == null || TextUtils.isEmpty(nolineNum)) {
                textView.setVisibility(View.GONE);
            } else {

                textView.setText(StringUtils.changeNumberMoreThen10000(nolineNum) + "人在线");
                textView.setVisibility(View.VISIBLE);
            }
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setData(List<ChannelBean.HomePageBean> mDataList) {
        super.setData(mDataList);
        scan_note_position = -1;
        refresh_times = 0;
    }
}
