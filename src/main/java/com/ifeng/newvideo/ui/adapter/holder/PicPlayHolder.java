package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;


public class PicPlayHolder {

    public TextView title;
    public TextView tv_comment;
    public ImageView playStatus;
    public TextView playTimes;
    public TextView duration;
    public NetworkImageView networkImageView;
    //    public NetworkImageView small_head_view;
    public ImageView toShare;
    public TextView userName;
    public ImageView mask;
    public View bottom;

    public static PicPlayHolder getPicPlayHolder(View convertView) {
        PicPlayHolder picPlayHolder = null;
        if (convertView.getTag() == null) {
            picPlayHolder = new PicPlayHolder();
            picPlayHolder.title = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_pic_title);
            picPlayHolder.tv_comment = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_comment);
            picPlayHolder.playStatus = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_play_status);
            picPlayHolder.playTimes = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_play_times);
            picPlayHolder.duration = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_duration);
            picPlayHolder.networkImageView = (NetworkImageView) convertView.findViewById(R.id.cniv_channel_big_pic);
//            picPlayHolder.small_head_view = (NetworkImageView) convertView.findViewById(R.id.niv_channel_big_pic_head);
            picPlayHolder.toShare = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_to_share);
            picPlayHolder.userName = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_user_name);
            picPlayHolder.mask = (ImageView) convertView.findViewById(R.id.niv_left_avatar_mask);
            picPlayHolder.bottom = convertView.findViewById(R.id.layout_intro);
            convertView.setTag(picPlayHolder);
        } else {
            picPlayHolder = (PicPlayHolder) convertView.getTag();
        }

        return picPlayHolder;
    }
}
