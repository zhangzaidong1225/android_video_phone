package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter;
import com.ifeng.newvideo.ui.adapter.holder.AdBigPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdMixTextPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdVideoLargeHolder;
import com.ifeng.newvideo.ui.adapter.holder.KKHolder;
import com.ifeng.newvideo.ui.adapter.holder.MixTextPictureItemHolder;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.ChannelBean;

/**
 * Created by guolc on 2016/7/25.
 */
public class ListAdapter4MixTextPic extends ChannelBaseAdapter {

    private boolean mIsShowBodyErr;
    private static final int err_type = 6;
    protected static final int NORMAL_MIX_TEXT_PIC = 5;

    public boolean ismIsNetErr() {
        return mIsNetErr;
    }

    public void setmIsNetErr(boolean mIsNetErr) {
        this.mIsNetErr = mIsNetErr;
    }

    private boolean mIsNetErr;

    public boolean isShowBodyErr() {
        return mIsShowBodyErr;
    }

    public void setIsShowBodyErr(boolean mIsShowBodyErr) {
        this.mIsShowBodyErr = mIsShowBodyErr;
    }

    public ListAdapter4MixTextPic(Context mContext, String channel_id) {
        this.mContext = mContext;
        this.mChannelId = channel_id;
    }

    @Override
    public int getCount() {
        if (mIsShowBodyErr) {
            return 1;
        }

        if (mDataList != null && mAdDataList != null) {
            return mDataList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (ListUtils.isEmpty(mDataList)) {
            return null;
        }
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 7;
    }

    @Override
    public int getItemViewType(int position) {
        if (mIsShowBodyErr) {
            return err_type;
        }
        ChannelBean.HomePageBean bean = mDataList.get(position);
        if (bean != null) {
            if (CheckIfengType.isAdBackend(bean.getMemberType())) {
                return getAdShowType(bean.getMemberItem().adConditions.showType);
            }
        }
        return NORMAL_MIX_TEXT_PIC;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChannelBean.HomePageBean bean = mDataList.get(position);
        boolean isAd = CheckIfengType.isAD(bean.getMemberType());
        int type = getItemViewType(position);
        if (isAd) {
            AdTools.exposeAdPvUrl(bean);
            ADRecord.addAdShow(bean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
            CommonStatictisListUtils.getInstance().sendADInfo(bean.getMemberItem().getAdPositionId(), bean.getInfoId(), mChannelId);
        } else {
            AdTools.loadImpressionUrl(bean);
        }
        String title = bean.getTitle();
        String tag = bean.getTag();
        String imageUrl = getImageUrl(bean);
        if (CheckIfengType.isAdBackend(bean.getMemberType())) {
            boolean isShowAdIcon = bean.getMemberItem().icon.showIcon == 1;
            tag = isShowAdIcon ? bean.getMemberItem().icon.text : "";
        }
        if (!CheckIfengType.isAD(bean.getMemberType())) {
            CommonStatictisListUtils.getInstance().addPullRefreshViewFocusList(bean.getInfoId(), position, PageIdConstants.REFTYPE_EDITOR, mChannelId,
                    type, CommonStatictisListUtils.PULLTOREFRESHVIEW_TYPE, CommonStatictisListUtils.ifengTv);
        }
        switch (type) {
            case NORMAL_MIX_TEXT_PIC:
                convertView = configNormalMixTextView(convertView, parent, bean, imageUrl);
                break;
            case AD_MIX_TEXT_PIC:
                if (convertView == null || !(convertView.getTag() instanceof MixTextPictureItemHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_mix_text_pic, parent, false);
                }
                configAdMixTextPicConvertView(convertView, tag, title, bean.getMemberItem().adConditions.showType,
                        imageUrl, bean.getMemberItem().vdescription);
                convertView.setTag(R.id.tag_key_click, bean);
                break;
            case AD_BANNER:
                if (convertView == null || !(convertView.getTag() instanceof KKHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kk_live, parent, false);
                }
                configAdBannerConvertView(convertView, tag, title, imageUrl, bean.getMemberItem().kkrand);
                break;
            case AD_CELL_3:
                if (convertView == null || !(convertView.getTag() instanceof AdMixTextPicHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_cell_3_pic, parent, false);
                }
                configAdCell3ConvertView(convertView, tag, title, bean.getAbstractDesc(), bean.getMemberItem().photos);
                break;
            case AD_BIG_PIC:
                if (convertView == null || !(convertView.getTag() instanceof AdBigPicHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_big_pic, parent, false);
                }
                configAdBigPic(convertView, tag, title, imageUrl, bean.getAbstractDesc());
                break;
            case AD_VIDEO_LARGE:
                if (convertView == null || !(convertView.getTag() instanceof AdVideoLargeHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_video_large, parent, false);
                }
                configAdVideoLarge(convertView, tag, title, imageUrl, bean.getAbstractDesc());
                break;
        }
        return convertView;
    }

    private View configNormalMixTextView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bean, String image) {
        String tag;
        if (convertView == null || !(convertView.getTag() instanceof MixTextPictureItemHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_listview_mix_text_picture, parent, false);
        }
        MixTextPictureItemHolder textPictureItemHolder = MixTextPictureItemHolder.getHolder(convertView);

        String author = null;
        String playtime = null;
        String headPic = null;
        int dura = 0;
        textPictureItemHolder.title.setText(bean.getTitle());
        markTextGray(textPictureItemHolder.title, bean);
        ChannelBean.WeMediaBean weMediaBean = bean.getWeMedia();
        if (weMediaBean != null && !TextUtils.isEmpty(weMediaBean.getId()) && !TextUtils.isEmpty(weMediaBean.getName())) {
            headPic = weMediaBean.getHeadPic();
            author = weMediaBean.getName();
            textPictureItemHolder.author.setText(author);
        }
        String clickType = "";
        if (bean.getMemberItem() != null) {
            playtime = bean.getMemberItem().getPlayTime();
            dura = bean.getMemberItem().getDuration();
            textPictureItemHolder.playTimes.setText(StringUtils.changePlayTimes(playtime));
            if (dura > 0) {
                textPictureItemHolder.duration.setText(StringUtils.changeDuration(dura));
            }
            clickType = bean.getMemberItem().adAction.type;
        }
        tag = TagUtils.getTagTextForList(bean.getTag(), bean.getMemberType(), clickType);
        textPictureItemHolder.label.setText(tag);
        textPictureItemHolder.label.setTextColor(TagUtils.getTagTextColor(bean.getMemberType()));
        textPictureItemHolder.label.setBackgroundResource(TagUtils.getTagBackground(bean.getMemberType()));
//        textPictureItemHolder.avatar.setImageUrl(headPic, VolleyHelper.getImageLoader());
        textPictureItemHolder.right_picture.setImageUrl(image, VolleyHelper.getImageLoader());
        textPictureItemHolder.right_picture.setDefaultImageResId(R.drawable.bg_default_mid);
        textPictureItemHolder.right_picture.setErrorImageResId(R.drawable.bg_default_mid);
        textPictureItemHolder.duration.setVisibility(dura <= 0 ? View.GONE : View.VISIBLE);
        textPictureItemHolder.label.setVisibility(TextUtils.isEmpty(tag) ? View.GONE : View.VISIBLE);
        textPictureItemHolder.playTimes.setVisibility(TextUtils.isEmpty(playtime) ? View.GONE : View.VISIBLE);
        textPictureItemHolder.author.setVisibility(TextUtils.isEmpty(author) ? View.GONE : View.VISIBLE);
//        if (TextUtils.isEmpty(headPic) && TextUtils.isEmpty(author)) {
//            textPictureItemHolder.avatar.setVisibility(View.GONE);
//            textPictureItemHolder.avatarMask.setVisibility(View.GONE);
//        } else {
//            textPictureItemHolder.avatar.setErrorImageResId(R.drawable.avatar_default);
//            textPictureItemHolder.avatar.setDefaultImageResId(R.drawable.avatar_default);
//            textPictureItemHolder.avatar.setVisibility(View.VISIBLE);
//            textPictureItemHolder.avatarMask.setVisibility(View.VISIBLE);
//        }
        return convertView;
    }
}
