package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.FMInfoModel;
import com.ifeng.video.dao.util.IfengImgUrlUtils;

import java.util.List;


public class ListAdapterFMPage extends BaseAdapter implements View.OnClickListener {

    private final LayoutInflater inflater;
    private final Context mContext;
    private List<FMInfoModel.ResourceInfo> resourceInfos;
    private String channelId;

    public ListAdapterFMPage(Context context, String channelId) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        this.channelId = channelId;
    }

    public void setData(List<FMInfoModel.ResourceInfo> resourceInfos) {
        this.resourceInfos = resourceInfos;
    }

    @Override
    public int getCount() {
        return resourceInfos == null ? 0 : resourceInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return resourceInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FMListViewHolder viewHolder = null;
        FMInfoModel.ResourceInfo bean = resourceInfos.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.home_list_item_fmchannel, null);
            viewHolder = new FMListViewHolder();
            initViewHolder(convertView, viewHolder);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (FMListViewHolder) convertView.getTag();
        }
        bindDataToViewHolder(viewHolder, position);

        CommonStatictisListUtils.getInstance().addPullRefreshViewFocusList(bean.getId(), position, PageIdConstants.REFTYPE_EDITOR, channelId,
                0, CommonStatictisListUtils.HEADERVIEW_TYPE, CommonStatictisListUtils.fm);
        return convertView;
    }

    private void bindDataToViewHolder(FMListViewHolder viewHolder, int position) {
        FMInfoModel.ResourceInfo resourceInfo = resourceInfos.get(position);
        boolean isTitleView = !TextUtils.isEmpty(resourceInfo.getNodeId());
        if (isTitleView) {
            if (position != 0) {
                viewHolder.fmDividerUp.setVisibility(View.VISIBLE);
            }
            viewHolder.fmTitleRl.setVisibility(View.VISIBLE);
            viewHolder.bannerTitle.setText(resourceInfo.getCardTitle());
            viewHolder.fmMore.setTag("more" + position);
            viewHolder.fmMore.setOnClickListener(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
            lp.setMargins(0, 0, 0, 0);
            viewHolder.fmDivider.setLayoutParams(lp);
        } else {
            viewHolder.fmDividerUp.setVisibility(View.GONE);
            viewHolder.fmTitleRl.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
            lp.setMargins(DisplayUtils.convertDipToPixel(10), 0, DisplayUtils.convertDipToPixel(10), 0);
            viewHolder.fmDivider.setLayoutParams(lp);
        }
        viewHolder.fmImg.setDefaultImageResId(R.drawable.common_default_bg_fm);
        viewHolder.fmImg.setErrorImageResId(R.drawable.common_default_bg_fm);
        viewHolder.fmImg.setImageUrl(IfengImgUrlUtils.getAudioImgUrl(resourceInfo.getImg370_370()), VolleyHelper.getImageLoader());
        viewHolder.fmTitle.setText(resourceInfo.getTitle());
        markTextGray(viewHolder.fmTitle, resourceInfo);
        viewHolder.fmDes.setText(resourceInfo.getProgramName());
        if (!TextUtils.isEmpty(resourceInfo.getDuration())) {
            viewHolder.fmTime.setText(DateUtils.getTimeStr(Integer.parseInt(resourceInfo.getDuration())));
        } else {
            viewHolder.fmTime.setText("0");
        }
        viewHolder.fmNum.setText(StringUtils.changePlayTimes(resourceInfo.getListenNumShow()));
        viewHolder.fmItemClick.setTag("" + position);
        viewHolder.fmItemClick.setOnClickListener(this);
    }

    private void initViewHolder(View convertView, FMListViewHolder viewHolder) {
        viewHolder.fmDividerUp = convertView.findViewById(R.id.fm_divider_up);
        viewHolder.fmTitleRl = (RelativeLayout) convertView.findViewById(R.id.fm_general_title_rl);
        viewHolder.bannerTitle = (TextView) convertView.findViewById(R.id.fm_general_title);
        viewHolder.fmImg = (NetworkImageView) convertView.findViewById(R.id.fm_item_img);
        viewHolder.fmTitle = (TitleTextView) convertView.findViewById(R.id.fm_item_title);
        viewHolder.fmDes = (TextView) convertView.findViewById(R.id.fm_item_des);
        viewHolder.fmTime = (TextView) convertView.findViewById(R.id.fm_item_video_duration);
        viewHolder.fmNum = (TextView) convertView.findViewById(R.id.fm_item_video_playtime);
        viewHolder.fmMore = (TextView) convertView.findViewById(R.id.fm_column_general_title);
        viewHolder.fmItemClick = (LinearLayout) convertView.findViewById(R.id.fm_item_click);
        viewHolder.fmDivider = convertView.findViewById(R.id.fm_item_divider);

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onClick(View v) {
        String position = (String) v.getTag();
        int pos;
        FMInfoModel.ResourceInfo resourceInfo;
        if (position.contains("more")) {
            //更多
            pos = Integer.parseInt(position.substring(4));
            resourceInfo = resourceInfos.get(pos);
            IntentUtils.startFMListActivity(mContext, resourceInfo.getNodeName(), resourceInfo.getNodeId());
            PageActionTracker.clickHomeChBtn(ActionIdConstants.CLICK_FM_MORE, channelId);
        } else {
            pos = Integer.parseInt(position);
            resourceInfo = resourceInfos.get(pos);
            resourceInfos.get(pos).setWatched(true);
            notifyDataSetChanged();
            int duration = string2Int(resourceInfo.getDuration());
            int fileSize;
            if (!ListUtils.isEmpty(resourceInfo.getAudioList()) && resourceInfo.getAudioList().get(0) != null) {
                fileSize = string2Int(resourceInfo.getAudioList().get(0).getSize());
            } else {
                fileSize = 0;
            }
            IntentUtils.toAudioFMActivity(mContext, resourceInfo.getId(), resourceInfo.getProgramId(), resourceInfo.getTitle(),
                    resourceInfo.getNodeName(), resourceInfo.getAudio().getFilePath(), resourceInfo.getImg370_370(), 0,
                    VodRecord.V_TAG_FM_HPAGE, channelId, duration, fileSize);
        }
        PageActionTracker.clickHomeItem(String.valueOf(pos), "", "");
    }

    private int string2Int(String s) {
        return TextUtils.isDigitsOnly(s) ? Integer.parseInt(s) : 0;
    }

    private static class FMListViewHolder {
        private View fmDividerUp;
        private RelativeLayout fmTitleRl;
        private TextView bannerTitle;
        private NetworkImageView fmImg;
        private TitleTextView fmTitle;
        private TextView fmDes;
        private TextView fmTime;
        private TextView fmNum;
        private TextView fmMore;
        private LinearLayout fmItemClick;
        private View fmDivider;

    }

    protected void markTextGray(TextView textView, FMInfoModel.ResourceInfo bean) {
        if (bean != null && bean.isWatched()) {
            textView.setTextColor(Color.parseColor("#a0a0a0"));
        } else {
            textView.setTextColor(Color.parseColor("#262626"));
        }
    }
}
