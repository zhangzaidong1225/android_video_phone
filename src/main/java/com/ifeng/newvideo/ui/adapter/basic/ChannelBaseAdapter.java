package com.ifeng.newvideo.ui.adapter.basic;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.adapter.holder.AdBigPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdCell3Holder;
import com.ifeng.newvideo.ui.adapter.holder.AdMixTextPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdVideoLargeHolder;
import com.ifeng.newvideo.ui.adapter.holder.KKHolder;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 频道 Adapter
 * Created by guolc on 2016/8/18.
 */
public abstract class ChannelBaseAdapter extends BaseAdapter {

    public static final Logger logger = LoggerFactory.getLogger(ChannelBaseAdapter.class);
    protected static final int AD_BANNER = 0;
    protected static final int AD_MIX_TEXT_PIC = 1;
    protected static final int AD_CELL_3 = 2;
    protected static final int AD_BIG_PIC = 3;
    protected static final int AD_VIDEO_LARGE = 4;
    protected static final String tag_body = "body";
    protected static final int AD_EMPTY_TYPE = 12;

    public static final String AD_TYPE_IMG = "img";
    public static final String AD_TYPE_APP = "app";
    public static final String AD_TYPE_PHOTOS = "photos";
    public static final String AD_TYPE_LARGE = "large";
    public static final String AD_TYPE_IBIGLARGE = "biglarge";
    public static final String AD_TYPE_VIDEOLARGE = "videolarge";


    protected Context mContext;
    protected String mChannelId;
    protected int refresh_times;
    protected List<MainAdInfoModel> mAdDataList = new ArrayList<>();
    protected List<ChannelBean.HomePageBean> mDataList = new ArrayList<>();
    protected List<ChannelBean.HomePageBean> insertList = new ArrayList<>();


    public void setRefreshTimes(boolean refresh) {
        if (refresh) {
            refresh_times++;
        }
    }

    public boolean addData(List<ChannelBean.HomePageBean> list, boolean add2head) {
        if (!ListUtils.isEmpty(list)) {
            if (add2head) {
                return this.mDataList.addAll(0, list);
            } else {
                return this.mDataList.addAll(list);
            }
        }
        return false;
    }

    public void setData(List<ChannelBean.HomePageBean> mDataList) {
        if (!ListUtils.isEmpty(mDataList)) {
            this.mDataList.clear();
            this.mDataList.addAll(mDataList);
        }
    }

    public List<ChannelBean.HomePageBean> getDataList() {
        return mDataList;
    }

    public ChannelBean.HomePageBean getLastItem() {
        try {
            return mDataList.get(mDataList.size() - 1);
        } catch (Exception e) {
            logger.error("getLastItem error ! {}", e);
        }
        return null;
    }


    protected void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
    }

    protected int getAdShowType(String showType) {
        if (AD_TYPE_LARGE.equalsIgnoreCase(showType)) {
            return AD_BANNER;
        } else if (AD_TYPE_PHOTOS.equalsIgnoreCase(showType)) {
            return AD_CELL_3;
        } else if (AD_TYPE_IBIGLARGE.equalsIgnoreCase(showType)) {
            return AD_BIG_PIC;
        } else if (AD_TYPE_VIDEOLARGE.equalsIgnoreCase(showType)) {
            return AD_VIDEO_LARGE;
        } else {
            return AD_MIX_TEXT_PIC;
        }
    }

    protected void configAdBannerConvertView(View convertView, String tag, String title, String imgUrl, String kkbrand) {

        KKHolder kkHolder = KKHolder.getKKHolder(convertView);
        if (TextUtils.isEmpty(tag)) {
            kkHolder.label.setVisibility(View.GONE);
        } else {
            kkHolder.label.setText(tag);
            kkHolder.label.setVisibility(View.VISIBLE);
        }
        kkHolder.title.setText(title);
        if (TextUtils.isEmpty(kkbrand)) {
            kkHolder.online_count.setVisibility(View.GONE);
        } else {
            kkHolder.online_count.setVisibility(View.VISIBLE);
            kkHolder.online_count.setText(kkbrand);
        }
        setImageUrl(kkHolder.image, imgUrl, R.drawable.bg_default_ad_banner_big);
    }

    protected void configAdMixTextPicConvertView(final View convertView, String tag, String title, String showType, String imgUrl, String abstractDesc) {

        AdMixTextPicHolder adMixTextPicHolder = AdMixTextPicHolder.getHolder(convertView);
        if (AD_TYPE_APP.equalsIgnoreCase(showType)) {
            adMixTextPicHolder.download.setVisibility(View.VISIBLE);
            if (adMixTextPicHolder.download.getParent() instanceof View) {
                ((View) adMixTextPicHolder.download.getParent()).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Object obj = convertView.getTag(R.id.tag_key_click);
                        if (obj == null || !(obj instanceof ChannelBean.HomePageBean)) {
                            return;
                        }
                        ChannelBean.HomePageBean bean = (ChannelBean.HomePageBean) obj;
                        ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
                        if (memberItemBean == null) {
                            return;
                        }
                        List<String> downloadCompleteUrlList = new ArrayList<>();
                        if (!ListUtils.isEmpty(memberItemBean.adAction.async_downloadCompletedurl)) {
                            downloadCompleteUrlList.addAll(memberItemBean.adAction.async_downloadCompletedurl);
                        }
                        if (!ListUtils.isEmpty(memberItemBean.adAction.downloadCompletedurl)) {
                            downloadCompleteUrlList.addAll(memberItemBean.adAction.downloadCompletedurl);
                        }
                        AdvertExposureDao.sendAdvertClickReq(memberItemBean.adId, memberItemBean.adAction.async_click);

                        DownLoadUtils.download(IfengApplication.getAppContext(), memberItemBean.adId,
                                memberItemBean.adAction.loadingurl,
                                (ArrayList<String>) memberItemBean.adAction.async_download,
                                (ArrayList<String>) downloadCompleteUrlList
                        );
                    }
                });
            }
        } else {
            adMixTextPicHolder.download.setVisibility(View.GONE);
            if (adMixTextPicHolder.download.getParent() != null) {
                adMixTextPicHolder.download.setOnClickListener(null);
            }
        }
        if (TextUtils.isEmpty(title)) {
            adMixTextPicHolder.title.setVisibility(View.GONE);
        } else {
            adMixTextPicHolder.title.setText(title);
            adMixTextPicHolder.title.setVisibility(View.VISIBLE);
        }
        setImageUrl(adMixTextPicHolder.right_pic, imgUrl, R.drawable.bg_default_mid);
        if (TextUtils.isEmpty(abstractDesc)) {
            adMixTextPicHolder.ad_des.setVisibility(View.GONE);
        } else {
            adMixTextPicHolder.ad_des.setText(abstractDesc);
            adMixTextPicHolder.ad_des.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(tag)) {
            adMixTextPicHolder.tag.setVisibility(View.GONE);
        } else {
            adMixTextPicHolder.tag.setText(tag);
            adMixTextPicHolder.tag.setVisibility(View.VISIBLE);
        }
    }

    protected void configAdCell3ConvertView(View convertView, String tag, String title, String abstractDesc, List<String> photos) {

        AdCell3Holder adCell3Holder = AdCell3Holder.getHolder(convertView);
        if (TextUtils.isEmpty(title)) {
            adCell3Holder.title.setVisibility(View.GONE);
        } else {
            adCell3Holder.title.setText(title);
            adCell3Holder.title.setVisibility(View.VISIBLE);
        }

        String ad_imgUrl0 = "";
        String ad_imgUrl1 = "";
        String ad_imgUrl2 = "";
        if (!ListUtils.isEmpty(photos)) {
            ad_imgUrl0 = photos.get(0);
            if (photos.size() > 1) {
                ad_imgUrl1 = photos.get(1);
            }
            if (photos.size() > 2) {
                ad_imgUrl2 = photos.get(2);
            }
        }
        setImageUrl(adCell3Holder.pic_0, ad_imgUrl0, R.drawable.bg_default_mid);
        setImageUrl(adCell3Holder.pic_1, ad_imgUrl1, R.drawable.bg_default_mid);
        setImageUrl(adCell3Holder.pic_2, ad_imgUrl2, R.drawable.bg_default_mid);
        if (TextUtils.isEmpty(abstractDesc)) {
            adCell3Holder.ad_des.setVisibility(View.GONE);
        } else {
            adCell3Holder.ad_des.setText(abstractDesc);
            adCell3Holder.ad_des.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(tag)) {
            adCell3Holder.tag.setVisibility(View.GONE);

        } else {
            adCell3Holder.tag.setText(tag);
            adCell3Holder.tag.setTextColor(Color.parseColor("#2a90d7"));
            adCell3Holder.tag.setBackgroundResource(R.drawable.home_item_tag_blue);
            adCell3Holder.tag.setVisibility(View.VISIBLE);
        }
    }

    protected void configAdBigPic(View convertView, String tag, String title, String imgUrl, String abstractDesc) {

        AdBigPicHolder adBigPicHolder = AdBigPicHolder.getHolder(convertView);
        if (TextUtils.isEmpty(title)) {
            adBigPicHolder.ad_title.setVisibility(View.GONE);
        } else {
            adBigPicHolder.ad_title.setText(title);
            adBigPicHolder.ad_title.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(abstractDesc)) {
            adBigPicHolder.ad_des.setVisibility(View.GONE);
        } else {
            adBigPicHolder.ad_des.setText(title);
            adBigPicHolder.ad_des.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(tag)) {
            adBigPicHolder.ad_tag.setVisibility(View.GONE);
        } else {
            adBigPicHolder.ad_tag.setText(tag);
            adBigPicHolder.ad_tag.setVisibility(View.VISIBLE);
        }
        setImageUrl(adBigPicHolder.ad_pic, imgUrl, R.drawable.bg_default_pic);
    }

    protected void configAdVideoLarge(View convertView, String tag, String title, String imgUrl, String abstractDesc) {

        AdVideoLargeHolder adVideoLargeHolder = AdVideoLargeHolder.getHolder(convertView);
        if (TextUtils.isEmpty(title)) {
            adVideoLargeHolder.title.setVisibility(View.GONE);
        } else {
            adVideoLargeHolder.title.setText(title);
            adVideoLargeHolder.title.setVisibility(View.VISIBLE);
        }
        setImageUrl(adVideoLargeHolder.pic, imgUrl, R.drawable.bg_default_live_big);
        if (TextUtils.isEmpty(abstractDesc)) {
            adVideoLargeHolder.des.setVisibility(View.GONE);
        } else {
            adVideoLargeHolder.des.setText(abstractDesc);
            adVideoLargeHolder.des.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(tag)) {
            adVideoLargeHolder.tag.setVisibility(View.GONE);
        } else {
            adVideoLargeHolder.tag.setText(tag);
            adVideoLargeHolder.tag.setVisibility(View.VISIBLE);
        }
    }

    protected void markTextGray(TextView textView, HomePageBeanBase bean) {
        if (bean != null && bean.isWatched()) {
            textView.setTextColor(Color.parseColor("#a0a0a0"));
        } else {
            textView.setTextColor(Color.parseColor("#262626"));
        }
    }


    public String getImageUrl(ChannelBean.HomePageBean bean) {
        String imgUrl;
        if (!ListUtils.isEmpty(bean.getImageList())) {
            imgUrl = bean.getImageList().get(0).getImage();
        } else {
            imgUrl = bean.getImage();
        }
        if (TextUtils.isEmpty(imgUrl)) {
            imgUrl = bean.getMemberItem().imageURL;
        }
        return imgUrl;
    }
}
