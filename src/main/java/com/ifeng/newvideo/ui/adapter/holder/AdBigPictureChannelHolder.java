package com.ifeng.newvideo.ui.adapter.holder;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.widget.CustomNetworkImageView;

public class AdBigPictureChannelHolder {
    public TextView title;
    public CustomNetworkImageView iv_ad;
    public TextView duration;
    public TextView playTimes;
    public TextView tv_use;
    public ImageView playStatus;
    public NetworkImageView networkImageView;
    public NetworkImageView small_head_view;
    public View bottom;
    public ImageView mask;
    public TextView tv_know;
    public TextView adTag;
    public TextView download;

}
