package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by android-dev on 2016/10/19.
 */
public class AdBigPicHolder {
    public TextView ad_title;
    public TextView ad_des;
    public TextView ad_tag;
    public NetworkImageView ad_pic;
    public static AdBigPicHolder getHolder(View convertView){
        AdBigPicHolder holder = null;
        if (convertView.getTag() == null) {
            holder = new AdBigPicHolder();
            holder.ad_title = (TextView) convertView.findViewById(R.id.tv_ab_big_pic_title);
            holder.ad_des= (TextView) convertView.findViewById(R.id.tv_ad_big_pic_des);
            holder.ad_tag= (TextView) convertView.findViewById(R.id.tv_ad_big_pic_tag);
            holder.ad_pic= (NetworkImageView) convertView.findViewById(R.id.cniv_ad_big_pic);
            convertView.setTag(holder);
        } else {
            holder = (AdBigPicHolder) convertView.getTag();
        }
        return holder;
    }
}
