package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by android-dev on 2016/10/19.
 */
public class AdMixTextPicHolder {
    public TextView title;
    public TextView ad_des;
    public TextView tag;
    public TextView download;
    public NetworkImageView right_pic;

    public static AdMixTextPicHolder getHolder(final View convertView) {
        AdMixTextPicHolder holder = null;
        if (convertView.getTag() == null) {
            holder = new AdMixTextPicHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_left_title);
            holder.ad_des = (TextView) convertView.findViewById(R.id.tv_ad_desc);
            holder.tag = (TextView) convertView.findViewById(R.id.tv_ad_tag);
            holder.download = (TextView) convertView.findViewById(R.id.tv_ad_download);
            holder.right_pic = (NetworkImageView) convertView.findViewById(R.id.niv_right_picture);
            convertView.setTag(holder);
        } else {
            holder = (AdMixTextPicHolder) convertView.getTag();
        }
        return holder;
    }
}
