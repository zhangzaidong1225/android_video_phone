package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.subscribe.RecommendBaseBean;
import com.ifeng.video.dao.db.model.subscribe.RecommendVideoItem;
import com.ifeng.video.dao.db.model.subscribe.RecommendWeMediaItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 订阅页面--精选推荐中ListView适配器
 * Created by Administrator on 2016/7/20.
 */
public class SubscribeRecommendListAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SubscribeRecommendListAdapter.class);

    //获取条目类型0:自媒体信息 1：自媒体相关视频
    public final static int TYPE_WEMEDIA_INFO = 0;
    public final static int TYPE_WEMEDIA_VIDEO = 1;

    private List<RecommendBaseBean> list = new ArrayList<>();

    public SubscribeRecommendListAdapter() {
        isError = false;
        isLoading = false;
        isEmpty = false;
    }

    @Override
    public Object getItem(int i) {
        if (isError || isLoading || list.isEmpty()) {
            return null;
        }
        return list.get(i);
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        if (isError || isLoading || isEmpty) {
            return -1;
        }
        return list.get(position).getItemType();
    }

    @Override
    public int getCount() {
        if (isLoading || isError || isEmpty) {
            return 1;
        }
        return list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (isLoading) {
            return getLoadingView(viewGroup);
        } else if (isError) {
            return getErrorView(viewGroup);
        } else if (isEmpty) {
            return getEmptyView(viewGroup);
        }
        Context context = viewGroup.getContext();
        ViewHolderWeMedia mediaHolder;
        ViewHolderVideo videoHolder;
        OnClick onClick;

        int type = getItemViewType(i);
        switch (type) {
            case TYPE_WEMEDIA_INFO:
                if (view == null || !((view.getTag() instanceof ViewHolderWeMedia))) {
                    onClick = new OnClick();
                    mediaHolder = new ViewHolderWeMedia();
                    view = LayoutInflater.from(context).inflate(R.layout.adapter_subscribe_list_wemedia_item_layout, viewGroup, false);
                    mediaHolder.initView(view);
                    view.setTag(mediaHolder);
                    view.setTag(R.id.view_parent_wemedia, onClick);
                } else {
                    mediaHolder = (ViewHolderWeMedia) view.getTag();
                    onClick = (OnClick) view.getTag(R.id.view_parent_wemedia);
                }

                onClick.setPosition(i);
                mediaHolder.itemView.setOnClickListener(onClick);
                mediaHolder.tv_subscribe.setOnClickListener(onClick);
                RecommendWeMediaItem weMediaItem = (RecommendWeMediaItem) list.get(i);
                mediaHolder.img_header.setImageUrl(weMediaItem.getHeadPic(), VolleyHelper.getImageLoader());
                mediaHolder.img_header.setDefaultImageResId(R.drawable.icon_login_default_header);
                mediaHolder.img_header.setErrorImageResId(R.drawable.icon_login_default_header);
                mediaHolder.tv_wemeida_name.setText(weMediaItem.getName());
                boolean flag = TextUtils.isEmpty(weMediaItem.getFollowNo()) || "0".equals(weMediaItem.getFollowNo());//如果订阅人数为空或者为0
                mediaHolder.tv_subscribe_num.setVisibility(flag ? View.INVISIBLE : View.VISIBLE);
                mediaHolder.tv_subscribe_num.setText(StringUtils.changeNumberMoreThen10000(weMediaItem.getFollowNo()) + "人订阅");
                if (weMediaItem.getFollowed() == 0) {
                    mediaHolder.tv_subscribe.setText(R.string.subscribe);
                    mediaHolder.tv_subscribe.setTextColor(context.getResources().getColor(R.color.add_subscribe_text_color));
                    mediaHolder.tv_subscribe.setBackgroundResource(R.drawable.btn_subscribe_shape_border);
                } else {
                    mediaHolder.tv_subscribe.setText(R.string.subscribed);
                    mediaHolder.tv_subscribe.setTextColor(context.getResources().getColor(R.color.subscribed_text_color));
                    mediaHolder.tv_subscribe.setBackgroundResource(R.drawable.btn_subscribe_shape_gray_border);
                }
                break;
            case TYPE_WEMEDIA_VIDEO:
                if (view == null || !((view.getTag() instanceof ViewHolderVideo))) {
                    onClick = new OnClick();
                    videoHolder = new ViewHolderVideo();
                    view = LayoutInflater.from(context).inflate(R.layout.item_listview_mix_text_picture, viewGroup, false);
                    videoHolder.initView(view);
                    view.setTag(videoHolder);
                    view.setTag(R.id.view_parent, onClick);
                } else {
                    videoHolder = (ViewHolderVideo) view.getTag();
                    onClick = (OnClick) view.getTag(R.id.view_parent);
                }
                onClick.setPosition(i);

                videoHolder.itemView.setOnClickListener(onClick);
                RecommendVideoItem videoItem = (RecommendVideoItem) list.get(i);
                int duration = (int) videoItem.getMemberItem().getDuration();
                videoHolder.tvVideoTime.setText(StringUtils.changeDuration(duration));
                videoHolder.tvVideoTitle.setText(videoItem.getTitle());

                String playTime = videoItem.getMemberItem().getPlayTime();
                boolean isShowPlayTime = !TextUtils.isEmpty(playTime);
                videoHolder.tvVideoWatchTime.setVisibility(isShowPlayTime ? View.VISIBLE : View.GONE);
                videoHolder.tvVideoWatchTime.setText(StringUtils.changePlayTimes(playTime));

                boolean isShowTag = !TextUtils.isEmpty(videoItem.getTag());//是否显示Tag标签
                videoHolder.tvVideoType.setVisibility(isShowTag ? View.VISIBLE : View.GONE);
                videoHolder.tvVideoType.setText(TagUtils.getTagTextForList(videoItem.getTag(), videoItem.getMemberType(), ""));
                videoHolder.tvVideoType.setTextColor(TagUtils.getTagTextColor(videoItem.getMemberType()));
                videoHolder.tvVideoType.setBackgroundResource(TagUtils.getTagBackground(videoItem.getMemberType()));

                if (videoItem.getImageList().size() > 0) {
                    videoHolder.videoCover.setImageUrl(videoItem.getImageList().get(0).getImage(), VolleyHelper.getImageLoader());
                }
                videoHolder.videoCover.setDefaultImageResId(R.drawable.bg_default_small);
                videoHolder.videoCover.setErrorImageResId(R.drawable.bg_default_small);
                break;
        }
        return view;
    }

    public void setList(List<RecommendBaseBean> list) {
        isError = false;
        isLoading = false;
        this.list = list;
        notifyDataSetChanged();
    }

    static class ViewHolderVideo {
        View itemView;
        TextView tvVideoTitle;//视频标题
        TextView tvVideoType;//Tag类型
        TextView tvVideoWatchTime;//观看次数
        NetworkImageView videoCover;//视频封面
        TextView tvVideoTime;//视频时长
        NetworkImageView avatar;
        View divider;
        ImageView avatarMask;
        TextView author;//自媒体名称

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvVideoTitle = (TextView) view.findViewById(R.id.tv_left_title);
            tvVideoType = (TextView) view.findViewById(R.id.tv_category_label);
            tvVideoWatchTime = (TextView) view.findViewById(R.id.tv_play_times);
            videoCover = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
            tvVideoTime = (TextView) view.findViewById(R.id.tv_duration);
            author = (TextView) view.findViewById(R.id.tv_author);
            avatar = (NetworkImageView) view.findViewById(R.id.niv_left_emoji);
            avatarMask = (ImageView) view.findViewById(R.id.niv_left_avatar_mask);
            divider = view.findViewById(R.id.divider);

            author.setVisibility(View.GONE);
            avatar.setVisibility(View.GONE);
            avatarMask.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
        }
    }

    static class ViewHolderWeMedia {
        View itemView;
        NetworkImageView img_header;//自媒体头像
        TextView tv_wemeida_name;//自媒体昵称
        TextView tv_subscribe_num;//订阅数量
        TextView tv_subscribe;//订阅

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent_wemedia);
            img_header = (NetworkImageView) view.findViewById(R.id.img_header);
            tv_wemeida_name = (TextView) view.findViewById(R.id.tv_wemeida_name);
            tv_subscribe_num = (TextView) view.findViewById(R.id.tv_subscribe_num);
            tv_subscribe = (TextView) view.findViewById(R.id.tv_subscribe);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (clickListener == null) {
                return;
            }
            switch (view.getId()) {
                case R.id.tv_subscribe:
                    clickListener.onSubscribeClickListener((RecommendWeMediaItem) list.get(position));
                    break;
                case R.id.view_parent_wemedia:
                    clickListener.onWeMediaItemClickListener((RecommendWeMediaItem) list.get(position));
                    break;
                case R.id.view_parent:
                    clickListener.onVideoItemClickListener((RecommendVideoItem) list.get(position));
                    break;
            }
        }
    }

    private volatile boolean isError;
    private volatile boolean isLoading;
    private volatile boolean isEmpty;

    public synchronized void showLoadingView() {
        isLoading = true;
        isError = false;
        isEmpty = false;
        notifyDataSetChanged();
    }

    public synchronized void showErrorView() {
        isLoading = false;
        isError = true;
        isEmpty = false;
        notifyDataSetChanged();
    }

    public synchronized void showEmptyView() {
        isLoading = false;
        isError = false;
        isEmpty = true;
        notifyDataSetChanged();
    }

    /**
     * LoadingView
     */
    private View getLoadingView(ViewGroup viewGroup) {
        return inflaterView(viewGroup, R.layout.subscribe_recommend_loading_layout);
    }

    /**
     * 加载数据失败的View
     */
    private View getErrorView(ViewGroup viewGroup) {
        View view = inflaterView(viewGroup, R.layout.subscribe_recommend_error_layout);
        ImageView icon = (ImageView) view.findViewById(R.id.common_load_icon);
        TextView textView = (TextView) view.findViewById(R.id.empty_tv);
        icon.setImageResource(R.drawable.icon_common_load_fail);
        textView.setText(R.string.common_load_data_error);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onLoadFailedClick();
                }
            }
        });
        return view;
    }

    /**
     * 空白view
     */
    private View getEmptyView(ViewGroup viewGroup) {
        View view = inflaterView(viewGroup, R.layout.subscribe_recommend_empty_layout);
        return view;
    }

    /**
     * 加载一个View并加载其布局参数，但不挂载到root
     **/
    private View inflaterView(ViewGroup root, @LayoutRes int layoutId) {
        View view = LayoutInflater.from(root.getContext())
                .inflate(layoutId, root, false);
        ViewParent parent = view.getParent();

        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        }
        return view;
    }

    /**
     * 订阅按钮、ItemView点击接口
     */
    private ClickListener clickListener;

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onWeMediaItemClickListener(RecommendWeMediaItem entity);

        void onSubscribeClickListener(RecommendWeMediaItem entity);

        void onVideoItemClickListener(RecommendVideoItem entity);
    }

    /**
     * 数据加载失败重新获取数据
     */
    private OnLoadFailedClick mListener;

    public void setOnLoadFailClick(OnLoadFailedClick listener) {
        this.mListener = listener;
    }

    public interface OnLoadFailedClick {
        void onLoadFailedClick();
    }
}
