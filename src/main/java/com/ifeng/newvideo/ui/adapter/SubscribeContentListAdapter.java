package com.ifeng.newvideo.ui.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;

import java.util.ArrayList;
import java.util.List;

/**
 * 订阅页面--订阅内容ListView适配器
 * Created by Administrator on 2016/7/31.
 */
public class SubscribeContentListAdapter extends BaseAdapter {

    public static final int SIZE_THREE = 3;//展示三个
    public static final int SIZE_MAX = Integer.MAX_VALUE;//不限制
    private int mSize = SIZE_MAX;
    private boolean mIsShowBottomLine = true;//是否显示底部横线

    private List<WeMediaInfoList.InfoListEntity> mList = new ArrayList<>();

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        int tmpSize = mList.size();
        return tmpSize >= mSize ? mSize : tmpSize;
    }

    public void setList(List<WeMediaInfoList.InfoListEntity> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderVideo videoHolder = null;
        OnClick onClick = null;
        if (view == null) {
            videoHolder = new ViewHolderVideo();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_listview_mix_text_picture, viewGroup, false);
            videoHolder.initView(view);
            view.setTag(R.id.view_parent, onClick);
            view.setTag(videoHolder);
        } else {
            videoHolder = (ViewHolderVideo) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        videoHolder.itemView.setOnClickListener(onClick);

        WeMediaInfoList.InfoListEntity.WeMediaEntity weMediaEntity = mList.get(i).getWeMedia();//自媒体信息
        WeMediaInfoList.InfoListEntity.BodyListEntity bodyListEntity = mList.get(i).getBodyList().get(0);//视频信息


        videoHolder.tvVideoTime.setText(StringUtils.changeDuration(bodyListEntity.getMemberItem().getDuration()));
        videoHolder.tvVideoTitle.setText(bodyListEntity.getTitle());

        String playTime = bodyListEntity.getMemberItem().getPlayTime();
        boolean isShowPlayTime = !TextUtils.isEmpty(playTime);
        videoHolder.tvVideoWatchTime.setVisibility(isShowPlayTime ? View.VISIBLE : View.GONE);
        videoHolder.tvVideoWatchTime.setText(StringUtils.changePlayTimes(playTime));

        boolean isShowTag = !TextUtils.isEmpty(bodyListEntity.getTag());//是否显示Tag标签
        videoHolder.tvVideoType.setVisibility(isShowTag ? View.VISIBLE : View.GONE);
        videoHolder.tvVideoType.setText(TagUtils.getTagTextForList(bodyListEntity.getTag(), bodyListEntity.getMemberType(), ""));
        videoHolder.tvVideoType.setTextColor(TagUtils.getTagTextColor(bodyListEntity.getMemberType()));
        videoHolder.tvVideoType.setBackgroundResource(TagUtils.getTagBackground(bodyListEntity.getMemberType()));

        if (!ListUtils.isEmpty(bodyListEntity.getImagelist())) {
            String img = bodyListEntity.getImagelist().get(0).getImage();
            videoHolder.videoCover.setImageUrl(img, VolleyHelper.getImageLoader());
        }
        videoHolder.videoCover.setDefaultImageResId(R.drawable.bg_default_small);
        videoHolder.videoCover.setErrorImageResId(R.drawable.bg_default_small);

        videoHolder.author.setText(weMediaEntity.getName());
        videoHolder.avatar.setImageUrl(weMediaEntity.getHeadPic(), VolleyHelper.getImageLoader());//自媒体头像
        if (mIsShowBottomLine) {
            videoHolder.divider.setVisibility(View.VISIBLE);
        } else {
            videoHolder.divider.setVisibility((i == getCount() - 1) ? View.GONE : View.VISIBLE);//最后一条数据隐藏底部横线
        }

        CommonStatictisListUtils.getInstance().sendWeMediaYoukuConstatic(bodyListEntity.getMemberItem(), CommonStatictisListUtils.YK_EXPOSURE, CommonStatictisListUtils.YK_HOMEPAGE_FEEDS);

        return view;
    }

    public void setShowBottomLine(boolean showBottomLine) {
        mIsShowBottomLine = showBottomLine;
    }

    public void setSize(int size) {
        this.mSize = size;
    }

    static class ViewHolderVideo {
        View itemView;
        TextView tvVideoTitle;//视频标题
        TextView tvVideoType;//类型：逻辑思维
        TextView tvVideoWatchTime;//观看次数
        NetworkImageView videoCover;//视频封面
        NetworkImageView avatar;//自媒体头像
        ImageView avatarMask;//自媒体头像圆形遮罩
        TextView tvVideoTime;//视频时长
        TextView author;//自媒体名称
        View divider;//底部一条线

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvVideoTitle = (TextView) view.findViewById(R.id.tv_left_title);
            tvVideoType = (TextView) view.findViewById(R.id.tv_category_label);
            tvVideoWatchTime = (TextView) view.findViewById(R.id.tv_play_times);
            videoCover = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
            tvVideoTime = (TextView) view.findViewById(R.id.tv_duration);
            author = (TextView) view.findViewById(R.id.tv_author);
            avatar = (NetworkImageView) view.findViewById(R.id.niv_left_emoji);
            avatarMask = (ImageView) view.findViewById(R.id.niv_left_avatar_mask);
            divider = view.findViewById(R.id.divider);
            author.setVisibility(View.VISIBLE);
            avatar.setVisibility(View.VISIBLE);
            avatarMask.setVisibility(View.VISIBLE);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_ITEM, PageIdConstants.PAGE_SUB);
            if (ListUtils.isEmpty(mList) || ListUtils.isEmpty(mList.get(position).getBodyList())) {
                return;
            }
            WeMediaInfoList.InfoListEntity.BodyListEntity bodyListEntity = mList.get(position).getBodyList().get(0);
            String guid = bodyListEntity.getMemberItem().getGuid();
            if (!TextUtils.isEmpty(guid)) {
                if (CheckIfengType.isVRVideo(bodyListEntity.getMemberType())) {
                    List<String> videoList = new ArrayList<>();
                    videoList.add(guid);
                    IntentUtils.startVRVideoActivity(view.getContext(), 0, "", "", 0, videoList, true);
                } else {
                    IntentUtils.toVodDetailActivity(view.getContext(), guid, "", false, false, 0, "");
                }
            }
        }
    }
}
