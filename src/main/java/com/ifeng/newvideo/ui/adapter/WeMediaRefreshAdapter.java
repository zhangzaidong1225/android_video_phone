package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class WeMediaRefreshAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private Context mContext;
    private List<VideoItem> mList = new ArrayList<>();
    private String guid;
    private int selectItem = -1;


    public WeMediaRefreshAdapter(Context context, List<VideoItem> mList, String guid) {
        inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mList = mList;
        this.guid = guid;
    }

    @Override
    public int getCount() {
        if (!ListUtils.isEmpty(mList)) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        WeMediaHolder holder = null;
        OnClick onClick = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_wemedia_recycler, viewGroup, false);
            onClick = new OnClick();
            holder = new WeMediaHolder();
            holder.itemView = convertView.findViewById(R.id.view_parent);
            holder.tv_timer = (TextView) convertView.findViewById(R.id.tv_timer);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(R.id.view_parent, onClick);
            convertView.setTag(holder);
        } else {
            holder = (WeMediaHolder) convertView.getTag();
            onClick = (OnClick) convertView.getTag(R.id.view_parent);
        }

        onClick.setPosition(position);
        holder.itemView.setOnClickListener(onClick);

        VideoItem videoItem = mList.get(position);
        holder.tv_timer.setText(videoItem.createDate.substring(0, 10));
        holder.tv_title.setText(videoItem.title);

        if (position == selectItem) {
            holder.tv_title.setTextColor(Color.parseColor("#F54343"));
        } else {
            holder.tv_title.setTextColor(Color.parseColor("#262626"));
        }

        return convertView;
    }

    private static class WeMediaHolder {
        private View itemView;
        private TextView tv_timer;
        private TextView tv_title;
    }

    class OnClick implements View.OnClickListener {

        private int position;

        public void setPosition(int position) {
            this.position = position;
        }


        @Override
        public void onClick(View view) {
            if (ListUtils.isEmpty(mList)) {
                return;
            }
            String guid = mList.get(position).guid;
            setSelectItem(position);
//            notifyDataSetChanged();
            notifyDataSetInvalidated();
            if (mProgramContent != null) {
                mProgramContent.onClickProgramContent(guid);
            }

        }
    }

    public void setSelectItem(int selectItem) {
        this.selectItem = selectItem;
    }

    private WeMediaProgramContent mProgramContent;

    public interface WeMediaProgramContent {
        void onClickProgramContent(String guid);
    }

    public void setProgramContent(WeMediaProgramContent listener) {
        this.mProgramContent = listener;
    }
}
