package com.ifeng.newvideo.ui.adapter.holder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.ifeng.newvideo.R;

public class SlideHolder {
    public TextView title;
    public RecyclerView recyclerView;

    public static SlideHolder getSlideHolder(View convertView) {
        SlideHolder slideHolder = null;
        if (convertView.getTag() == null) {
            slideHolder = new SlideHolder();
            slideHolder.title = (TextView) convertView.findViewById(R.id.tv_slide);
            slideHolder.recyclerView = (RecyclerView) convertView.findViewById(R.id.rv_slide);
            convertView.setTag(slideHolder);
        } else {
            slideHolder = (SlideHolder) convertView.getTag();
        }

        return slideHolder;
    }
}
