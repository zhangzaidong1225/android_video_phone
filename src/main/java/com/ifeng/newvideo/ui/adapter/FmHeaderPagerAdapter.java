package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.HeadFlowView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.model.FMInfoModel;

import java.util.ArrayList;
import java.util.List;

public class FmHeaderPagerAdapter extends PagerAdapter {
    Context context;
    List<FMInfoModel.Header> headers;
    HeadFlowView mHeadFlowView;
    String channel_id;

    public FmHeaderPagerAdapter(Context context, HeadFlowView headFlowView, String channel_id) {
        this.context = context;
        this.headers = new ArrayList<>();
        this.mHeadFlowView = headFlowView;
        this.channel_id = channel_id;
    }

    public void setData(List<FMInfoModel.Header> headers) {
        if (headers != null) {
            this.headers.clear();
            this.headers.addAll(headers);
        }
    }


    @Override
    public int getCount() {
        if (headers == null) {
            return 0;
        }
        return (headers.size() <= 1) ? headers.size() : Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (headers != null && headers.size() != 0) {

            mHeadFlowView.setPointView(headers.size(), position % headers.size());
            // 去掉title
            // FMInfoModel.Header bean = headers.get(position % headers.size());
            // mHeadFlowView.setTitleText(bean.getBannerTitle());
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        int index = position % headers.size();
        FMInfoModel.Header bean = headers.get(index);
        if (headers != null) {
            view = getView(bean, position);
        }
        if (view != null) {
            container.addView(view);
        }

        CommonStatictisListUtils.getInstance().addHeaderViewFocusList(bean.getRedirectId(), position, PageIdConstants.REFTYPE_EDITOR, channel_id,
                headers.size(), CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.fm);
        return view;
    }

    private View getView(final FMInfoModel.Header header, final int position) {
        int index = position % headers.size();
        NetworkImageView imageView = initItemView(header, index);
        imageView.setImageUrl(headers.get(index).getImage(), VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(R.drawable.common_default_header_bg);
        imageView.setErrorImageResId(R.drawable.common_default_header_bg);
        return imageView;
    }

    private NetworkImageView initItemView(final FMInfoModel.Header header, int index) {
        NetworkImageView imageView = new NetworkImageView(context);
        final int focusNum = index;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context == null) {
                    return;
                }
                FMInfoModel.ResourceInfo info = header.getResourceInfo();
                int duration = string2Int(info.getDuration());
                int fileSize;
                if (!ListUtils.isEmpty(info.getAudioList()) && info.getAudioList().get(0) != null) {
                    fileSize = string2Int(info.getAudioList().get(0).getSize());
                } else {
                    fileSize = 0;
                }
                IntentUtils.toAudioFMActivity(context, info.getId(), info.getProgramId(), info.getTitle(),
                        info.getNodeName(), info.getAudio().getFilePath(), info.getImg370_370(), 0,
                        VodRecord.V_TAG_FM_HPAGE, channel_id, duration, fileSize);

                PageActionTracker.clickHomeChFocusX(channel_id, focusNum);

//                PageActionTracker.clickHomeItem(focusNum, "", "");

            }
        });
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = DisplayUtils.convertDipToPixel(202);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

    private int string2Int(String s) {
        return TextUtils.isDigitsOnly(s) ? Integer.parseInt(s) : 0;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (object instanceof View) {
            container.removeView((View) object);
        }
    }
}