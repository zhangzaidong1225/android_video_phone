package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by guolc on 2016/7/21.
 */
public class LianboItemHolder {
    public TextView title;
    public TextView author;
    public TextView duration;
    public TextView label;
    public TextView playTimes;
    public TextView createDate;
    public NetworkImageView right_picture;
    public NetworkImageView avatar;
    public ImageView avatarMask;
    public TextView adDesc;

    public static LianboItemHolder getHolder(View convertView) {
        LianboItemHolder textPictureItemHolder = null;
        if (convertView.getTag() == null) {
            textPictureItemHolder = new LianboItemHolder();
            textPictureItemHolder.title = (TextView) convertView.findViewById(R.id.tv_left_title);
            textPictureItemHolder.author = (TextView) convertView.findViewById(R.id.tv_author);
            textPictureItemHolder.playTimes = (TextView) convertView.findViewById(R.id.tv_play_times);
            textPictureItemHolder.createDate = (TextView) convertView.findViewById(R.id.tv_create_date);
            textPictureItemHolder.label = (TextView) convertView.findViewById(R.id.tv_category_label);
            textPictureItemHolder.duration = (TextView) convertView.findViewById(R.id.tv_duration);
            textPictureItemHolder.adDesc = (TextView) convertView.findViewById(R.id.tv_ad_desc);
            textPictureItemHolder.right_picture = (NetworkImageView) convertView.findViewById(R.id.niv_right_picture);
            textPictureItemHolder.avatar = (NetworkImageView) convertView.findViewById(R.id.niv_left_emoji);
            textPictureItemHolder.avatarMask = (ImageView) convertView.findViewById(R.id.niv_left_avatar_mask);
            convertView.setTag(textPictureItemHolder);
        } else {
            textPictureItemHolder = (LianboItemHolder) convertView.getTag();
        }
        return textPictureItemHolder;
    }
}
