package com.ifeng.newvideo.ui.adapter.basic;

import android.widget.BaseAdapter;

import java.util.List;

/**
 * 频道页面视频列表基类adapter
 *
 * @author antboyqi
 */
public abstract class AdapterChannel<T> extends BaseAdapter {

    public abstract void setData(T t);

    public abstract void setData(List<T> t);

    public abstract void clearData();

    public abstract Object lastItemInfo();
}
