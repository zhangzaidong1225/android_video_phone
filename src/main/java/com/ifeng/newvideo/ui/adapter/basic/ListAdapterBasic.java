package com.ifeng.newvideo.ui.adapter.basic;

import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * 转几列的adapter
 * Created by antboyqi on 14-8-27.
 */
public abstract class ListAdapterBasic<T> extends AdapterChannel<T> {

    protected List<T> mBodyLists;
    private final List<T[]> mCount2List = new ArrayList<>();

    private int mLineCount = 1;

    public static int LINE_COUNT_ONE = 1;
    protected static final int LINE_COUNT_TWO = 2;
    public static int LINE_COUNT_THREE = 3;

    private final Class<T> mTClass;

    protected ListAdapterBasic(int lineCount, Class<T> clazz) {
        mLineCount = lineCount;
        mTClass = clazz;
    }

    @Override
    public void setData(T t) {
    }

    @Override
    public void setData(List<T> bodyLists) {
        mBodyLists = bodyLists;
        mCount2List.clear();
        initCountList();
        notifyDataSetInvalidated();
    }

    @Override
    public void clearData() {
        if (mBodyLists != null) {
            mBodyLists.clear();
        }
        mCount2List.clear();
    }

    @Override
    public int getCount() {
        if (mCount2List == null) {
            return 0;
        }
        return mCount2List.size();
    }

    @Override
    public Object getItem(int position) {
        return mCount2List.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void initCountList() {
        int totalSize = mBodyLists.size();
        int x = totalSize / mLineCount;
        int index;
        for (int i = 0; i < x; i++) {
            T[] blArray = (T[]) Array.newInstance(mTClass, mLineCount);
            index = i * mLineCount;
            for (int j = 0; j < mLineCount; j++) {
                blArray[j] = mBodyLists.get(index);
                index += 1;
            }
            mCount2List.add(blArray);
        }
        int moreCount = totalSize % mLineCount;
        if (moreCount != 0) {
            T[] blArray = (T[]) Array.newInstance(mTClass, moreCount);
            index = totalSize - moreCount;
            for (int c = 0; c < moreCount; c++) {
                blArray[c] = mBodyLists.get(index);
                index += 1;
            }
            mCount2List.add(blArray);
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
