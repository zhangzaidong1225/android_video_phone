package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.dao.db.model.FMMoreInfoModel;
import com.ifeng.video.dao.util.IfengImgUrlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class ListAdapterFMList extends BaseAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ListAdapterFMList.class);
    private final LayoutInflater inflater;
    private List<FMMoreInfoModel.ReList> reLists = new ArrayList<>();
    private Context mContext;

    public ListAdapterFMList(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void setData(List<FMMoreInfoModel.ReList> t) {
        reLists = t;
    }

    @Override
    public int getCount() {
        return reLists == null ? 0 : reLists.size();
    }

    @Override
    public Object getItem(int position) {
        return reLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_fmlist_item, null);
            viewHolder = new ViewHolder();
            viewHolder.img = (NetworkImageView) convertView.findViewById(R.id.fmlist_item_img);
            viewHolder.tv_title = (TitleTextView) convertView.findViewById(R.id.fmlist_item_title);
            viewHolder.tv_time = (TextView) convertView.findViewById(R.id.fmlist_item_time);
            viewHolder.tv_des = (TextView) convertView.findViewById(R.id.fmlist_item_des);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        FMMoreInfoModel.ReList relist = reLists.get(position);
        viewHolder.tv_title.setText(relist.getProgramName());
        if (!TextUtils.isEmpty(relist.getResourceCreateTime())) {
            viewHolder.tv_time.setText(DateUtils.getFMTime(Long.parseLong(relist.getResourceCreateTime()) * 1000));
        } else {
            viewHolder.tv_time.setText("");
        }
        viewHolder.tv_des.setText(relist.getResourceTitle());
        viewHolder.img.setDefaultImageResId(R.drawable.common_default_bg_fm);
        viewHolder.img.setErrorImageResId(R.drawable.common_default_bg_fm);
        viewHolder.img.setImageUrl(IfengImgUrlUtils.getAudioImgUrl(relist.getImg370_370()), VolleyHelper.getImageLoader());
        return convertView;
    }


    private static class ViewHolder {
        public TitleTextView tv_title;
        public TextView tv_time;
        public TextView tv_des;
        public NetworkImageView img;
    }
}
