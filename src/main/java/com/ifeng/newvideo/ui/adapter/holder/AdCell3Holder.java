package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by android-dev on 2016/10/19.
 */
public class AdCell3Holder {
    public TextView title;
    public TextView ad_des;
    public TextView tag;
    public NetworkImageView pic_0;
    public NetworkImageView pic_1;
    public NetworkImageView pic_2;

    public static AdCell3Holder getHolder(View convertView) {
        AdCell3Holder holder = null;
        if (convertView.getTag() == null) {
            holder = new AdCell3Holder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_ad_cell_3_title);
            holder.ad_des= (TextView) convertView.findViewById(R.id.tv_ad_cell_3_des);
            holder.tag= (TextView) convertView.findViewById(R.id.tv_ad_cell_3_tag);
            holder.pic_0= (NetworkImageView) convertView.findViewById(R.id.niv_ad_cell_0);
            holder.pic_1= (NetworkImageView) convertView.findViewById(R.id.niv_ad_cell_1);
            holder.pic_2= (NetworkImageView) convertView.findViewById(R.id.niv_ad_cell_2);
            convertView.setTag(holder);
        } else {
            holder = (AdCell3Holder) convertView.getTag();
        }
        return holder;
    }
}
