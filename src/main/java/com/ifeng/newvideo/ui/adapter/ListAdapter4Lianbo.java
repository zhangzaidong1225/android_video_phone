package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.holder.KKHolder;
import com.ifeng.newvideo.ui.adapter.holder.LianboItemHolder;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.TagUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guolc on 2016/7/25.
 */
public class ListAdapter4Lianbo extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(ListAdapter4Lianbo.class);

    private static final int AD_BANNER = 0;
    private static final int MIX_TEXT_PICTURE = 1;
    private static final String tag_body = "body";
    private static final String tag_head = "head";

    private Context mContext;
    private String mChannelId;
    private int refresh_times;
    private List<MainAdInfoModel> mAdDataList = new ArrayList<>();
    private List<HomePageBeanBase> mDataList = new ArrayList<>();
    private List<HomePageBeanBase> insertList = new ArrayList<>();

    public ListAdapter4Lianbo(Context context, String channelId) {
        mContext = context;
        mChannelId = channelId;
    }

    @Override
    public int getCount() {
        if (mDataList != null) {
            return mDataList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (ListUtils.isEmpty(mDataList)) {
            return null;
        }
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    protected int getAdShowType(String showType) {
        if ("large".equalsIgnoreCase(showType)) {
            return AD_BANNER;
        } else {
            return MIX_TEXT_PICTURE;
        }
    }

    @Override
    public int getItemViewType(int position) {
        HomePageBeanBase homePageBean = mDataList.get(position);
        if (homePageBean != null) {
            if (CheckIfengType.isAdBackend(homePageBean.getMemberType())) {
                return getAdShowType(homePageBean.getMemberItem().adConditions.showType);
            }
        }
        return MIX_TEXT_PICTURE;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HomePageBeanBase bean = mDataList.get(position);
        boolean isAd = CheckIfengType.isAD(bean.getMemberType());
        if (isAd) {
            ADRecord.addAdShow(bean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
            AdTools.exposeAdPvUrl(bean);
            CommonStatictisListUtils.getInstance().sendADInfo(bean.getMemberItem().getAdPositionId(), bean.getTopicId(), mChannelId);

        } else {
            AdTools.loadImpressionUrl(bean);
            CommonStatictisListUtils.getInstance().sendHomeFeedYoukuConstatic(bean, CommonStatictisListUtils.YK_EXPOSURE);

        }
        String desc = "";
        String tag = "";
        String image = bean.getImage();
        if (!CheckIfengType.isAD(bean.getMemberType())) {
            CommonStatictisListUtils.getInstance().addPullRefreshViewFocusList(bean.getTopicId(), position, PageIdConstants.REFTYPE_EDITOR, mChannelId,
                    0, CommonStatictisListUtils.HEADERVIEW_TYPE, CommonStatictisListUtils.lianbo);
        }
        switch (getItemViewType(position)) {
            case MIX_TEXT_PICTURE:
                if (convertView == null || !(convertView.getTag() instanceof LianboItemHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_listview_lianbo, parent, false);
                }
                LianboItemHolder lianboItemHolder = LianboItemHolder.getHolder(convertView);
                if (isAd) {
                    lianboItemHolder.title.setText(bean.getTitle());
                    lianboItemHolder.right_picture.setImageUrl(image, VolleyHelper.getImageLoader());
                    lianboItemHolder.createDate.setVisibility(View.GONE);
                    desc = bean.getAbstractDesc();
                    tag = bean.getTag();
                    if (TextUtils.isEmpty(tag)) {
                        lianboItemHolder.label.setVisibility(View.GONE);
                    } else {
                        lianboItemHolder.label.setText(tag);
                        lianboItemHolder.label.setTextColor(Color.parseColor("#2a90d7"));
                        lianboItemHolder.label.setBackgroundResource(R.drawable.home_item_tag_blue);
                        lianboItemHolder.label.setVisibility(View.VISIBLE);
                    }
                } else {
                    String createdate = null;
                    int duration = 0;
                    String playtime = null;
                    String headPic = null;
                    String author = null;
                    lianboItemHolder.title.setText(bean.getTitle());
                    if (bean.isWatched()) {
                        lianboItemHolder.title.setTextColor(Color.parseColor("#a0a0a0"));
                    } else {
                        lianboItemHolder.title.setTextColor(Color.parseColor("#262626"));
                    }
                    createdate = bean.getCreateDate();
                    if (!TextUtils.isEmpty(createdate)) {
                        lianboItemHolder.createDate.setText(DateUtils.parseUpdateDate(createdate));
                    }
                    if (!ListUtils.isEmpty(bean.getImageList())) {
                        lianboItemHolder.right_picture.setImageUrl(bean.getImageList().get(0).getImage(), VolleyHelper.getImageLoader());
                    } else {
                        lianboItemHolder.right_picture.setImageUrl(image, VolleyHelper.getImageLoader());
                    }
                    ChannelBean.WeMediaBean weMediaBean = bean.getWeMedia();
                    if (weMediaBean != null && !TextUtils.isEmpty(weMediaBean.getId()) && !TextUtils.isEmpty(weMediaBean.getName())) {
                        headPic = weMediaBean.getHeadPic();
                        author = weMediaBean.getName();
                        lianboItemHolder.author.setText(author);
                    }
                    desc = bean.getAbstractDesc();
                    String clickType = "";
                    if (bean.getMemberItem() != null) {
                        playtime = bean.getMemberItem().getPlayTime();
                        duration = bean.getMemberItem().getDuration();
                        lianboItemHolder.playTimes.setText(StringUtils.changePlayTimes(playtime));
                        if (duration > 0) {
                            lianboItemHolder.duration.setText(StringUtils.changeDuration(duration));
                        }
                        clickType = bean.getMemberItem().adAction.type;
                    }
                    tag = TagUtils.getTagTextForList(bean.getTag(), bean.getMemberType(), clickType);
                    lianboItemHolder.label.setText(tag);
                    lianboItemHolder.label.setTextColor(TagUtils.getTagTextColor(bean.getMemberType()));
                    lianboItemHolder.label.setBackgroundResource(TagUtils.getTagBackground(bean.getMemberType()));
                    lianboItemHolder.createDate.setVisibility(TextUtils.isEmpty(createdate) ? View.GONE : View.VISIBLE);
                    lianboItemHolder.duration.setVisibility(duration <= 0 ? View.GONE : View.VISIBLE);
                    lianboItemHolder.playTimes.setVisibility(TextUtils.isEmpty(playtime) ? View.GONE : View.VISIBLE);
                    lianboItemHolder.author.setVisibility(TextUtils.isEmpty(author) ? View.GONE : View.VISIBLE);
                    lianboItemHolder.avatar.setVisibility(TextUtils.isEmpty(headPic) ? View.GONE : View.VISIBLE);
                    lianboItemHolder.avatarMask.setVisibility(TextUtils.isEmpty(headPic) ? View.GONE : View.VISIBLE);

                }
                lianboItemHolder.right_picture.setDefaultImageResId(R.drawable.bg_default_mid);
                lianboItemHolder.right_picture.setErrorImageResId(R.drawable.bg_default_mid);
                lianboItemHolder.label.setVisibility(isAd && !TextUtils.isEmpty(tag) ? View.VISIBLE : View.GONE);
                lianboItemHolder.adDesc.setText(desc);
                lianboItemHolder.adDesc.setVisibility(isAd && !TextUtils.isEmpty(desc) ? View.VISIBLE : View.GONE);
                break;
            case AD_BANNER:
                if (convertView == null || !(convertView.getTag() instanceof KKHolder)) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kk_live, parent, false);
                }
                KKHolder kkHolder = getKKHolder(convertView);
                if (isAd) {
                    tag = bean.getTag();
                    if (TextUtils.isEmpty(tag)) {
                        kkHolder.label.setVisibility(View.GONE);
                    } else {
                        kkHolder.label.setText(tag);
                        kkHolder.label.setVisibility(View.VISIBLE);
                    }
                    kkHolder.online_count.setVisibility(View.GONE);
                    kkHolder.title.setText(bean.getTitle());
                    setImageUrl(kkHolder.image, image, R.drawable.bg_default_ad_banner_big);
                    break;
                }
            default:
                break;
        }
        return convertView;
    }


    private void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultRes) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultRes);
        imageView.setErrorImageResId(defaultRes);
    }

    public void setRefreshTimes(boolean refresh) {
        if (refresh) {
            refresh_times++;
        }
    }

    public boolean addData(List<? extends HomePageBeanBase> list, boolean add2head) {
        if (!ListUtils.isEmpty(list)) {
            if (add2head) {
                return this.mDataList.addAll(0, list);
            } else {
                return this.mDataList.addAll(list);
            }
        }
        return false;
    }

    public void setData(List<HomePageBeanBase> mDataList) {
        if (!ListUtils.isEmpty(mDataList)) {
            this.mDataList.clear();
            this.mDataList.addAll(mDataList);
        }
    }

    public List<HomePageBeanBase> getDataList() {
        return mDataList;
    }

    public List<MainAdInfoModel> getAdDataList() {
        return mAdDataList;
    }

    public void setRefreshAdData(List<MainAdInfoModel> adDadta, long create_time) {

        for (MainAdInfoModel main : mAdDataList) {
            for (MainAdInfoModel refresh : adDadta) {
                String positonId = main.getAdPositionId();
                if (!TextUtils.isEmpty(positonId) && positonId.equals(refresh.getAdPositionId())) {
                    main.setAdDescription(refresh.getAdDescription());
                    main.setAdMaterials(refresh.getAdMaterials());
                    main.setAdMaterialType(refresh.getAdMaterialType());
                    main.setCacheTime(refresh.getCacheTime());
                    main.setCreate_time(create_time);
                    break;
                }
            }
        }
    }

    private KKHolder getKKHolder(View convertView) {
        KKHolder kkHolder = null;
        if (convertView.getTag() == null) {
            kkHolder = new KKHolder();
            kkHolder.label = (TextView) convertView.findViewById(R.id.tv_ad_banner_label);
            kkHolder.title = (TextView) convertView.findViewById(R.id.tv_kk_title);
            kkHolder.online_count = (TextView) convertView.findViewById(R.id.tv_online_count);
            kkHolder.image = (NetworkImageView) convertView.findViewById(R.id.iv_right_picture_0);
            convertView.setTag(kkHolder);
        } else {
            kkHolder = (KKHolder) convertView.getTag();
        }
        return kkHolder;
    }

    public HomePageBeanBase getLastItem() {
        int count = mDataList.size();
        for (int i = count - 1; i >= 0; i--) {
            HomePageBeanBase bean = mDataList.get(i);
            if (!CheckIfengType.isAD(bean.getMemberType())) {
                return bean;
            }
        }
        return null;
    }

}
