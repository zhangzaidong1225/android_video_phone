package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by guolc on 2016/7/21.
 */
public class MixTextPictureItemHolder {
    public TextView title;
    public TextView author;
    public TextView duration;
    public TextView label;
    public TextView playTimes;
    public TextView when;
    public NetworkImageView right_picture;
    //    public NetworkImageView avatar;
//    public ImageView avatarMask;
    public TextView liveStatus;
    public TextView onLineCount;

    public static MixTextPictureItemHolder getHolder(View convertView) {
        MixTextPictureItemHolder textPictureItemHolder = null;
        if (convertView.getTag() == null) {
            textPictureItemHolder = new MixTextPictureItemHolder();
            textPictureItemHolder.title = (TextView) convertView.findViewById(R.id.tv_left_title);
            textPictureItemHolder.author = (TextView) convertView.findViewById(R.id.tv_author);
            textPictureItemHolder.playTimes = (TextView) convertView.findViewById(R.id.tv_play_times);
            textPictureItemHolder.when = (TextView) convertView.findViewById(R.id.tv_normal_mix_text_pic_when);
            textPictureItemHolder.label = (TextView) convertView.findViewById(R.id.tv_category_label);
            textPictureItemHolder.duration = (TextView) convertView.findViewById(R.id.tv_duration);
            textPictureItemHolder.right_picture = (NetworkImageView) convertView.findViewById(R.id.niv_right_picture);
//            textPictureItemHolder.avatar = (NetworkImageView) convertView.findViewById(R.id.niv_left_emoji);
//            textPictureItemHolder.avatarMask = (ImageView) convertView.findViewById(R.id.niv_left_avatar_mask);
            textPictureItemHolder.liveStatus = (TextView) convertView.findViewById(R.id.tv_mix_text_live_status);
            textPictureItemHolder.onLineCount = (TextView) convertView.findViewById(R.id.tv_normal_mix_text_pic_online_count);
            convertView.setTag(textPictureItemHolder);
        } else {
            textPictureItemHolder = (MixTextPictureItemHolder) convertView.getTag();
        }
        return textPictureItemHolder;
    }
}
