package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.statistics.domains.AdVideoRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.UniversalChannelFragment;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter;
import com.ifeng.newvideo.ui.adapter.holder.AdBigPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdBigPictureChannelHolder;
import com.ifeng.newvideo.ui.adapter.holder.AdMixTextPicHolder;
import com.ifeng.newvideo.ui.adapter.holder.BigPictureChannelHolder;
import com.ifeng.newvideo.ui.adapter.holder.KKHolder;
import com.ifeng.newvideo.ui.adapter.holder.MixTextPictureItemHolder;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.utils.ViewUtils;
import com.ifeng.newvideo.videoplayer.bean.BigAdItem;
import com.ifeng.newvideo.videoplayer.bean.DanmuItem;
import com.ifeng.newvideo.videoplayer.bean.VideoDanmuItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.DanmuView;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.widget.CustomNetworkImageView;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by guolc on 2016/7/20.
 */
public class ListAdapter4BigPictureChannel extends ChannelBaseAdapter implements NotifyShareCallback {

    public int mClickToPlayPositon = -1;
    private UniversalChannelFragment mFragment;

    //    private int index;//联播模式下游标,可以指向广告位置

    public void setPortraitPlayingContainer(ViewGroup mPortraitPlayingContainer) {
        this.mPortraitPlayingContainer = mPortraitPlayingContainer;
    }

    private ViewGroup mPortraitPlayingContainer;//横屏切换回竖屏时原listview的item的container
    public static int STATUS_PLAYING = 1;
    public static int STATUS_PAUSE = 0;
    public static int STATUS_DEFAULT = -1;
    public int playStatus = -1;
    public boolean isPlaying;
    private OneKeyShare mOneKeyShare;
    private String currentPlayingFile;
    private int needReopenPositon = -1;
    private static final int NORMAL_BIG_PICTURE = 4;
    private static final int AD_BIG_VIDEO = 5;
    private static final String AI = "57";
    private int mItemViewTypeCount;
    private static final int AD_EMPTY_TYPE = 6;


    public void increaseClickToPlayPosition() {
        this.mClickToPlayPositon++;
    }

    public int getClickToPlayPositon() {
        return mClickToPlayPositon;
    }

    public ViewGroup getPortraitPlayingContainer() {
        return mPortraitPlayingContainer;
    }

    public ListAdapter4BigPictureChannel(int itemViewTypeCount, Context context, UniversalChannelFragment fragment, String channel_id) {
        mItemViewTypeCount = itemViewTypeCount;
        this.mContext = context;
        this.mFragment = fragment;
        this.mOneKeyShare = new OneKeyShare(mContext);
        this.mChannelId = channel_id;
    }

    @Override
    public int getItemViewType(int position) {
        ChannelBean.HomePageBean homePageBean = mDataList.get(position);
        if (homePageBean != null && CheckIfengType.isAdBackend(homePageBean.getMemberType())) {
            if (AdTools.videoType.equals(homePageBean.getMemberItem().adConditions.showType)) {
                return AD_BIG_VIDEO;
            }
            //广告空曝光
            if (TextUtils.isEmpty(homePageBean.getMemberItem().imageURL)) {
                return AD_EMPTY_TYPE;
            }
            return AD_BANNER;
            // return getAdShowType(homePageBean.getShowType());
        }

        return NORMAL_BIG_PICTURE;
    }

    @Override
    public int getViewTypeCount() {
        return mItemViewTypeCount <= 0 ? 1 : mItemViewTypeCount;
    }

    @Override
    public int getCount() {
        if (mDataList != null && mAdDataList != null) {
            return mDataList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (ListUtils.isEmpty(mDataList)) {
            return null;
        }
        ChannelBean.HomePageBean bean = mDataList.get(position);
        if (bean != null && CheckIfengType.isAD(bean.getMemberType())) {
            return bean;
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        final ChannelBean.HomePageBean bean = mDataList.get(position);
        if (bean != null) {
            if (CheckIfengType.isAD(bean.getMemberType())) {
                ADRecord.addAdShow(bean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
                if (SharePreUtils.getInstance().getVisibleMap().containsKey(mChannelId)) {
                    if (SharePreUtils.getInstance().getVisibleMap().get(mChannelId)) {
                        AdTools.exposeAdPvUrl(bean);
                    }
                }

                CommonStatictisListUtils.getInstance().sendADInfo(bean.getMemberItem().getAdPositionId(), bean.getInfoId(), mChannelId);
//                AdTools.exposeAdPvUrl(bean);
            } else {
                AdTools.loadImpressionUrl(bean);
                CommonStatictisListUtils.getInstance().sendHomeFeedYoukuConstatic(bean, CommonStatictisListUtils.YK_EXPOSURE);

            }
            String title = bean.getTitle();
            String tag = bean.getTag();
            if (CheckIfengType.isAdBackend(bean.getMemberType())) {
                boolean isShowAdIcon = bean.getMemberItem().icon.showIcon == 1;
                tag = isShowAdIcon ? bean.getMemberItem().icon.text : "";
            }

            if (!CheckIfengType.isAD(bean.getMemberType())) {
                String videoId = bean.getInfoId();
                int insertPosition = position;
                if (EmptyUtils.isEmpty(videoId)) {
                    //发生item插入行为
                    videoId = bean.getMemberItem().getGuid();
                    ++insertPosition;
                }

                if (mChannelId.equals(AI)) {//猜你喜欢
                    CommonStatictisListUtils.getInstance().addPullRefreshViewNormalFocusList(mFragment, videoId, insertPosition, PageIdConstants.REFTYPE_AI, mChannelId,
                            type, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.normal);
                } else {

                    CommonStatictisListUtils.getInstance().addPullRefreshViewNormalFocusList(mFragment, videoId, insertPosition, PageIdConstants.REFTYPE_EDITOR, mChannelId,
                            type, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.normal);
                }
            }
            switch (type) {
                case NORMAL_BIG_PICTURE:
                    if (convertView == null || !(convertView.getTag() instanceof BigPictureChannelHolder)) {
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_channel_big_picture, parent, false);
                    }
                    BigPictureChannelHolder holder = getHolder(convertView);
                    AdTools.loadImpressionUrl(bean);

                    holder.playStatus.setImageResource(R.drawable.btn_play);
                    holder.title.setText(title);
                    markTextGray(holder.title, bean);
                    List<HomePageBeanBase.ImageBean> imageBeanList = bean.getImageList();
                    if (!ListUtils.isEmpty(imageBeanList)) {
                        setImageUrl(holder.networkImageView, imageBeanList.get(0).getImage(), R.drawable.bg_default_pic);
                    }
                    ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
                    String headPic = null;
                    String wemediaName = null;
                    if (weMedia != null && !TextUtils.isEmpty(weMedia.getId()) && !TextUtils.isEmpty(weMedia.getName())) {
                        headPic = weMedia.getHeadPic();
                        wemediaName = weMedia.getName();
                    }
                    if (TextUtils.isEmpty(headPic) && TextUtils.isEmpty(wemediaName)) {
                        holder.small_head_view.setVisibility(View.GONE);
                        holder.mask.setVisibility(View.GONE);
                    } else {
                        setImageUrl(holder.small_head_view, headPic, R.drawable.avatar_default);
                        holder.small_head_view.setVisibility(View.VISIBLE);
                        holder.mask.setVisibility(View.VISIBLE);
                    }
                    if (TextUtils.isEmpty(wemediaName)) {
                        holder.userName.setVisibility(View.GONE);
                    } else {
                        holder.userName.setText(wemediaName);
                        holder.userName.setVisibility(View.VISIBLE);
                    }
                    ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
                    if (memberItemBean != null) {
                        holder.duration.setText(StringUtils.changeDuration(memberItemBean.getDuration()));
                        holder.playTimes.setText(StringUtils.changePlayTimes(memberItemBean.getPlayTime()));
                        String commentNum = StringUtils.changeNumberMoreThen10000(memberItemBean.getCommentNo());
                        holder.tv_comment.setText("0".equals(commentNum) ? "" : commentNum);
                        holder.tv_comment.setCompoundDrawablePadding("0".equals(commentNum) ? 0 : DisplayUtils.convertDipToPixel(6));
                    }
                    convertView.setTag(R.id.tag_key_click, position);
                    break;
                case AD_MIX_TEXT_PIC:
                    if (convertView == null || !(convertView.getTag() instanceof MixTextPictureItemHolder)) {
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_mix_text_pic, parent, false);
                    }
                    configAdMixTextPicConvertView(convertView, tag, title, bean.getMemberItem().adConditions.showType,
                            bean.getMemberItem().imageURL, bean.getMemberItem().vdescription);
                    convertView.setTag(R.id.tag_key_click, bean);
                    break;
                case AD_BANNER:
                    if (convertView == null || !(convertView.getTag() instanceof KKHolder)) {
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kk_live, parent, false);
                    }
                    configAdBannerConvertView(convertView, tag, title, bean.getMemberItem().imageURL, bean.getMemberItem().kkrand);
                    break;
                case AD_CELL_3:
                    if (convertView == null || !(convertView.getTag() instanceof AdMixTextPicHolder)) {
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_cell_3_pic, parent, false);
                    }
                    configAdCell3ConvertView(convertView, tag, title, bean.getAbstractDesc(), bean.getMemberItem().photos);
                    break;
                case AD_BIG_PIC:
                    if (convertView == null || !(convertView.getTag() instanceof AdBigPicHolder)) {
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.ad_big_pic, parent, false);
                    }
                    configAdBigPic(convertView, tag, title, bean.getImage(), bean.getAbstractDesc());
                    break;
                case AD_BIG_VIDEO:
                    convertView = getADBigPicView(convertView, parent, bean, title);
                    convertView.setTag(R.id.tag_key_click, position);
                    break;
                case AD_EMPTY_TYPE:
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_ad_empty_layout, parent, false);
                    break;

                default:
                    break;

            }
        }
        return convertView;
    }


    public void recoverUI() {
        if (playStatus != STATUS_DEFAULT) {
            if (mWrapper != null && mWrapper.getParent() != null) {
                ViewGroup group = (ViewGroup) mWrapper.getParent();
                group.removeView(mWrapper);
            }

            if (mPortraitPlayingContainer != null) {
                ViewUtils.showChildrenView(mPortraitPlayingContainer);
                mPortraitPlayingContainer = null;
            }
            currentPlayingFile = null;

            if (null != danmuView) {
                Log.d("danmu", "clean data");
                danmuView.clearDanmaku();
                danmuView.onDestory();
            }
            // 从头开始播放
            if (AdTools.videoType.equalsIgnoreCase(mUIPlayContext.videoType)) {
                mVideoHelper.setBookMark(0);
            }
            playStatus = STATUS_DEFAULT;
            isPlaying = false;
            mUIPlayContext.videoType = "video";
            sendAdVideoStatictis();
        }
    }

    public void recoverUI(boolean showPiP, FloatVideoPlayController.OnListViewScrollToPiPVideoView listener) {
        if (playStatus != STATUS_DEFAULT) {
            if (mWrapper != null && mWrapper.getParent() != null) {
                ViewGroup group = (ViewGroup) mWrapper.getParent();
                group.removeView(mWrapper);
                if (showPiP && !CheckIfengType.isAdBackend(mUIPlayContext.videoItem.memberType)) {
                    ((ActivityMainTab) mWrapper.getContext()).addVideoView2FloatView(mUIPlayContext, mDataList, listener, mVideoHelper.getLastPosition());
                }
            }

            if (mPortraitPlayingContainer != null) {
                ViewUtils.showChildrenView(mPortraitPlayingContainer);
                mPortraitPlayingContainer = null;
            }
            currentPlayingFile = null;

            if (null != danmuView) {
                Log.d("danmu", "clean data");
                danmuView.clearDanmaku();
                danmuView.onDestory();
            }
            // 从头开始播放
            if (AdTools.videoType.equalsIgnoreCase(mUIPlayContext.videoType)) {
                mVideoHelper.setBookMark(0);
            }
            playStatus = STATUS_DEFAULT;
            isPlaying = false;
            mUIPlayContext.videoType = "video";
            sendAdVideoStatictis();
        }
    }

    private void sendAdVideoStatictis() {
        if (adVideoRecord != null) {
            if (mUIPlayContext.status != IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                adVideoRecord.stopPlayTime();
            }
            if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED) {
                adVideoRecord.setPauseNum();
            }

            adVideoRecord.pcomplete = adVideoRecord.getCompleteNum();
            adVideoRecord.preplay = adVideoRecord.getPrePlayNum();
            adVideoRecord.pstop = adVideoRecord.getPauseNum();

            Log.d("advideo", "sendAdVideoStatidtis: --" + adVideoRecord.toString());
            CommonStatictisListUtils.getInstance().sendAdVideo(adVideoRecord);
            adVideoRecord.resetNum();
        }
    }


    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageHomeCh(mChannelId);
        } else {
            PageActionTracker.enterPage();
        }
    }

    private BigPictureChannelHolder getHolder(final View convertView) {
        BigPictureChannelHolder holder = null;
        if (convertView.getTag() == null || !(convertView.getTag() instanceof BigPictureChannelHolder)) {
            holder = new BigPictureChannelHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_title);
            holder.playStatus = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_play_status);
            holder.networkImageView = (NetworkImageView) convertView.findViewById(R.id.cniv_channel_big_pic);
            holder.networkImageView.setDefaultImageResId(R.drawable.bg_default_pic);
            holder.networkImageView.setErrorImageResId(R.drawable.login_ifeng_logo);
            holder.small_head_view = (NetworkImageView) convertView.findViewById(R.id.niv_channel_big_pic_head);
            holder.small_head_view.setDefaultImageResId(R.drawable.bg_default_pic);
            holder.small_head_view.setErrorImageResId(R.drawable.login_ifeng_logo);
            holder.mask = (ImageView) convertView.findViewById(R.id.niv_left_avatar_mask);
            holder.playTimes = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_play_times);
            holder.tv_comment = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_comment);
            holder.toShare = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_to_share);
            holder.duration = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_duration);
            holder.userName = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_user_name);
            holder.bottom = convertView.findViewById(R.id.layout_intro);
            holder.videoContainer = (RelativeLayout) convertView.findViewById(R.id.rl_locate);

            View.OnClickListener toVideoListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int position = (int) obj;
                    long bookmark = 0;
                    if (mClickToPlayPositon != -1 && mClickToPlayPositon == position) {
                        bookmark = mVideoHelper == null ? 0 : mVideoHelper.getCurrentPosition();
                    }
                    if (bookmark > 0) {
                        toVideoActivity(position, false, true, bookmark, false);
                    } else {
                        toVideoActivity(position, false, false, bookmark, false);
                    }
                }
            };
            holder.title.setOnClickListener(toVideoListener);
            holder.bottom.setOnClickListener(toVideoListener);

            final BigPictureChannelHolder finalHolder = holder;
            holder.videoContainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int position = (int) obj;
                    if (mClickToPlayPositon != -1 && mClickToPlayPositon != position) {
                        recoverUI();
                    }

                    HomePageBeanBase bean = mDataList.get(position);
                    if (bean != null) {
                        finalHolder.title.setTextColor(Color.parseColor("#a0a0a0"));
                        bean.setWatched(true);
                        mPortraitPlayingContainer = (ViewGroup) view;
                        openVideo(bean, position, true);
                        ((ActivityMainTab) mWrapper.getContext()).removePiPViews();
                        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, true, PageIdConstants.PLAY_VIDEO_V);
                    }
                }
            });
            holder.tv_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int position = (int) obj;
                    long bookmark = 0;
                    if (mClickToPlayPositon != -1 && mClickToPlayPositon == position) {
                        bookmark = mVideoHelper == null ? 0 : mVideoHelper.getCurrentPosition();
                    }
                    if (bookmark > 0) {
                        toVideoActivity(position, true, true, bookmark, false);
                    } else {
                        toVideoActivity(position, true, false, bookmark, false);
                    }
                    PageActionTracker.clickHomeChBtn(ActionIdConstants.CLICK_COMMENT_ICON, mChannelId);

                }
            });
            holder.toShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int positon = (int) obj;
                    HomePageBeanBase bean = mDataList.get(positon);
                    OneKeyShareContainer.oneKeyShare = mOneKeyShare;
                    String weMediaId = bean.getWeMedia() != null ? bean.getWeMedia().getId() : "";
                    String chid = bean.getMemberItem().getSearchPath();
                    mOneKeyShare.initShareStatisticsData(bean.getMemberItem().getGuid(), mChannelId, chid, weMediaId, PageIdConstants.PAGE_HOME);
                    mOneKeyShare.initSmartShareData(bean.getShowType(), bean.getWeMedia().getName(), bean.getTitle());
                    String imgUrl = !ListUtils.isEmpty(bean.getImageList()) ? bean.getImageList().get(0).getImage() : "";
                    mOneKeyShare.shareVodWithPopWindow(bean.getTitle(),
                            imgUrl,
                            bean.getMemberItem().getmUrl(),
                            view, ListAdapter4BigPictureChannel.this, false);
                    PageActionTracker.clickHomeChBtn(ActionIdConstants.CLICK_SHARE, mChannelId);
                }
            });

            View.OnClickListener onInnerItemClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int position = (int) obj;
                    HomePageBeanBase bean = mDataList.get(position);
                    if (bean != null) {
                        ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
                        if (weMedia == null) {
                            return;
                        }
                        Intent intent = new Intent(mContext, WeMediaHomePageActivity.class);
                        intent.putExtra(IntentKey.WE_MEIDA_ID, weMedia.getId());
                        mContext.startActivity(intent);
                        PageActionTracker.clickHomeChBtn(ActionIdConstants.CLICK_WEMEDIA, mChannelId);

                        String simId = bean.getMemberItem() != null ? bean.getMemberItem().getSimId() : "";
                        String rToken = bean.getMemberItem() != null ? bean.getMemberItem().getrToken() : "";
                        PageActionTracker.clickHomeItem(String.valueOf(position), simId, rToken);
                    }
                }
            };
            holder.userName.setOnClickListener(onInnerItemClickListener);
            holder.small_head_view.setOnClickListener(onInnerItemClickListener);
            convertView.setTag(holder);
        } else {
            holder = (BigPictureChannelHolder) convertView.getTag();
        }
        return holder;
    }

    private AdBigPictureChannelHolder adBigPictureChannelHolder;

    public AdBigPictureChannelHolder getAdHolder() {
        return adBigPictureChannelHolder;
    }


    private View getADBigPicView(View convertView, ViewGroup parent, ChannelBean.HomePageBean bodyListBean, String title) {
        if (convertView == null || !(convertView.getTag() instanceof AdBigPictureChannelHolder)) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_universal_big_picture, parent, false);
        }
        ChannelBean.HomePageBean bean = bodyListBean;
        AdBigPictureChannelHolder holder = getAdBigVideoHolder(convertView, bodyListBean);
        adBigPictureChannelHolder = holder;
        AdTools.loadImpressionUrl(bean);

        holder.playStatus.setImageResource(R.drawable.btn_play);
        holder.title.setText(title);
        markTextGray(holder.title, bean);
        List<HomePageBeanBase.ImageBean> imageBeanList = bean.getImageList();
        if (!ListUtils.isEmpty(imageBeanList)) {
            setImageUrl(holder.networkImageView, imageBeanList.get(0).getImage(), R.drawable.bg_default_pic);
            setImageUrl(holder.iv_ad, imageBeanList.get(0).getImage(), R.drawable.bg_default_pic);
        }
        holder.tv_use.setText("了解详情");
        ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();

        if (memberItemBean != null) {
            holder.tv_use.setText(memberItemBean.adConditions.stoptext);
            if (!TextUtils.isEmpty(memberItemBean.icon.text) && memberItemBean.icon.showIcon == 1) {
                holder.adTag.setText(memberItemBean.icon.text);
            } else {
                holder.adTag.setVisibility(View.GONE);
            }
            setImageUrl(holder.iv_ad, memberItemBean.adConditions.stopimageURL, R.drawable.bg_default_small);
        }
        return convertView;
    }

    private AdBigPictureChannelHolder getAdBigVideoHolder(final View convertView, final ChannelBean.HomePageBean bodyListBean) {
        AdBigPictureChannelHolder holder = null;
        if (convertView.getTag() == null || !(convertView.getTag() instanceof AdBigPictureChannelHolder)) {
            holder = new AdBigPictureChannelHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_title);
            holder.playStatus = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_play_status_ad);
            holder.networkImageView = (NetworkImageView) convertView.findViewById(R.id.cniv_channel_big_pic_ad);
            holder.networkImageView.setDefaultImageResId(R.drawable.bg_default_pic);
            holder.networkImageView.setErrorImageResId(R.drawable.login_ifeng_logo);
            holder.iv_ad = (CustomNetworkImageView) convertView.findViewById(R.id.iv_ad);
            holder.iv_ad.setDefaultImageResId(R.drawable.bg_default_pic);
            holder.iv_ad.setErrorImageResId(R.drawable.login_ifeng_logo);
            holder.tv_use = (TextView) convertView.findViewById(R.id.tv_ad_left_ad);
            holder.tv_know = (TextView) convertView.findViewById(R.id.tv_video_ad_detail);
            holder.adTag = (TextView) convertView.findViewById(R.id.tv_ad_tag);
            holder.download = (TextView) convertView.findViewById(R.id.tv_ad_download);

            View.OnClickListener toVideoListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }

                    IntentUtils.startADActivity(mContext, bodyListBean.getMemberItem().adId, bodyListBean.getMemberItem().adAction.url, "", ADActivity.FUNCTION_VALUE_AD,
                            bodyListBean.getTitle(), null, "", "", "", null, null);
                    recoverUI();

                    PageActionTracker.clickHomeItem((String) obj, bodyListBean.getMemberItem().getSimId(), bodyListBean.getMemberItem().getrToken());
                }
            };

            holder.title.setOnClickListener(toVideoListener);


            final AdBigPictureChannelHolder finalHolder = holder;
            holder.playStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);
                    if (obj == null || !(obj instanceof Integer)) {
                        return;
                    }
                    int position = (int) obj;
                    if (mClickToPlayPositon != -1 && mClickToPlayPositon != position) {
                        recoverUI();
                    }

                    HomePageBeanBase bean = mDataList.get(position);
                    if (bean != null) {
                        finalHolder.title.setTextColor(Color.parseColor("#a0a0a0"));
                        mPortraitPlayingContainer = (ViewGroup) view.getParent();
                        openVideo(bean, position, true);
                        ((ActivityMainTab) mWrapper.getContext()).removePiPViews();
                        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, true, PageIdConstants.PLAY_VIDEO_V);
                    }
                }
            });
            holder.tv_use.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);

                    IntentUtils.startADActivity(mContext, bodyListBean.getMemberItem().adId, bodyListBean.getMemberItem().adConditions.stopurl,
                            "", ADActivity.FUNCTION_VALUE_AD,
                            bodyListBean.getTitle(), null, "", "", "", null, null);

                    PageActionTracker.clickHomeItem((String) obj, bodyListBean.getMemberItem().getSimId(), bodyListBean.getMemberItem().getrToken());
                }
            });

            holder.tv_know.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object obj = convertView.getTag(R.id.tag_key_click);

                    IntentUtils.startADActivity(mContext, bodyListBean.getMemberItem().adId, bodyListBean.getMemberItem().adAction.url, "", ADActivity.FUNCTION_VALUE_AD,
                            bodyListBean.getTitle(), null, "", "", "", null, null);

                    PageActionTracker.clickHomeItem((String) obj, bodyListBean.getMemberItem().getSimId(), bodyListBean.getMemberItem().getrToken());
                }
            });
        } else {
            holder = (AdBigPictureChannelHolder) convertView.getTag();
        }

        return holder;
    }

    public void toVideoActivityForDanmu() {

        int positon = mClickToPlayPositon;
        long bookmark = mVideoHelper == null ? 0 : mVideoHelper.getCurrentPosition();
        if (bookmark > 0) {
            toVideoActivity(positon, false, true, bookmark, true);
        } else {
            toVideoActivity(positon, false, false, bookmark, true);
        }
    }


    private void toVideoActivity(int position, boolean anchor, boolean isFromHistory, long bookMark, boolean isEditDanma) {

        HomePageBeanBase bean = mDataList.get(position);
        if (bean != null) {
            bean.setWatched(true);
            ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
            if (memberItemBean == null) {
                return;
            }
            IntentUtils.toVodDetailActivity(mContext, bean.getMemberItem() != null ? bean.getMemberItem().getGuid() : "",
                    mFragment.getChannelId(), anchor, isFromHistory, bookMark, "", isEditDanma);
            PageActionTracker.clickHomeItem(String.valueOf(position), memberItemBean.getSimId(), memberItemBean.getrToken());
        }
        recoverUI();
    }

    public void back2PortraitAndContinuePlay() {
        if (isPlaying && mPortraitPlayingContainer != null) {
            ViewUtils.hideChildrenView(mPortraitPlayingContainer);
            if (mWrapper.getParent() != null) {
                ((ViewGroup) mWrapper.getParent()).removeView(mWrapper);
            }
            mPortraitPlayingContainer.addView(mWrapper);
            continuePlay();
        }
    }


    public void continuePlay() {
        if (!AdTools.videoType.equals(mUIPlayContext.videoType)) {
            currentPlayingFile = StreamUtils.getMediaUrlForPic(mUIPlayContext.videoFilesBeanList);
        }

        if (mVideoHelper != null && currentPlayingFile != null) {
            mVideoHelper.openVideo(currentPlayingFile);
        }
    }

    private NormalVideoHelper mVideoHelper;
    private FrameLayout mWrapper;
    private UIPlayContext mUIPlayContext;
    private DanmuView danmuView;

    public void setVideoHelper(NormalVideoHelper videoHelper) {
        this.mVideoHelper = videoHelper;
    }

    public void setVideoSkinWrapper(FrameLayout wrapper) {
        this.mWrapper = wrapper;
    }

    public void setUIPlayContext(UIPlayContext playContext) {
        this.mUIPlayContext = playContext;
    }

    public void setDanmuView(DanmuView danmuView) {
        this.danmuView = danmuView;
    }

    private VideoItem getVideoItem(HomePageBeanBase bean, String imgUrl) {
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        VideoItem item = new VideoItem();
        ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
        if (weMedia != null) {
            item.weMedia = new WeMedia();
            item.weMedia.id = weMedia.getId();
            item.weMedia.name = weMedia.getName();
            item.weMedia.headPic = weMedia.getHeadPic();
            item.weMedia.desc = weMedia.getDesc();
        }
        item.guid = memberItem.getGuid();
        item.simId = memberItem.getSimId();
        item.cpName = memberItem.getCpName();
        item.title = TextUtils.isEmpty(bean.getTitle()) ? memberItem.getName() : bean.getTitle();//title为空取name
        item.searchPath = memberItem.getSearchPath();
        item.image = imgUrl;
        item.duration = memberItem.getDuration();
        item.mUrl = bean.getMemberItem().getmUrl();
        item.memberType = bean.getMemberType();
        return item;
    }

    public void setCurrentPositionNeedReopen() {
        needReopenPositon = mClickToPlayPositon;
    }

    public int getNeedReopenPositon() {
        return needReopenPositon;
    }

    public void autoPlayNext(final HomePageBeanBase bean, final int next_position, View itemView, boolean isPortrait) {
        if (itemView != null && itemView.getTag() instanceof BigPictureChannelHolder) {
            BigPictureChannelHolder holder = (BigPictureChannelHolder) itemView.getTag();
            mPortraitPlayingContainer = (ViewGroup) holder.playStatus.getParent();
        }

        if (itemView != null && itemView.getTag() instanceof AdBigPictureChannelHolder) {
            AdBigPictureChannelHolder holder = (AdBigPictureChannelHolder) itemView.getTag();
            mPortraitPlayingContainer = (ViewGroup) holder.playStatus.getParent();
        }

        openVideo(bean, next_position, isPortrait);
    }

    public HomePageBeanBase getNextBeanWhenPlayComplete() {
        if (mClickToPlayPositon != -1 && mClickToPlayPositon < mDataList.size() - 1) {
            final int position = mClickToPlayPositon + 1;
            HomePageBeanBase bean = mDataList.get(position);
            return bean;
        }
        return null;
    }

    public AdVideoRecord adVideoRecord;


    private void openVideo(HomePageBeanBase bean, int position, boolean portrait) {
        logger.debug("openVideo");
        if (bean == null) {
            return;
        }
        bean.setWatched(true);
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        if (memberItem == null) {
            return;
        }


        List<ChannelBean.VideoFilesBean> videoFilesBeanList = memberItem.getVideoFiles();
        String title = bean.getTitle();
        if (!ListUtils.isEmpty(videoFilesBeanList)) {
            String stream = StreamUtils.getMediaUrlForPic(videoFilesBeanList);
            if (TextUtils.isEmpty(stream)) {
                return;
            }
            if (stream.equals(currentPlayingFile)) {
                //自动联播时滑动状态下点击同一条目可能出现的问题
                return;
            } else {
                currentPlayingFile = stream;
            }
            mUIPlayContext.videoFilesBeanList = videoFilesBeanList;
            mUIPlayContext.streamType = StreamUtils.getStreamType();
            mUIPlayContext.videoType = "video";
        } else {
            String videoUrl = memberItem.adConditions.videourl;
            currentPlayingFile = videoUrl;
            mUIPlayContext.videoType = bean.getMemberItem().adConditions.showType;
            if (null == adVideoRecord) {
                adVideoRecord = new AdVideoRecord(PhoneConfig.IMEI, memberItem.adId, memberItem.adConditions.oid,

                        memberItem.adConditions.pid, memberItem.adConditions.videotime);
            }
            adVideoRecord.ptime = 0;
            adVideoRecord.startPlayTime();
            BigAdItem adItem = new BigAdItem(bean.getTitle(), bean.getMemberItem().adConditions.stoptext, bean.getMemberItem().adConditions.stopurl,
                    bean.getMemberItem().adAction.url, bean.getMemberItem().adConditions.stopimageURL);
            mUIPlayContext.adItem = adItem;

        }
        List<HomePageBeanBase.ImageBean> imageList = bean.getImageList();
        String image = "";
        if (!ListUtils.isEmpty(imageList)) {
            image = imageList.get(0).getImage();
        }
        mUIPlayContext.image = image;
        mUIPlayContext.title = TextUtils.isEmpty(title) ? memberItem.getName() : title;
        mUIPlayContext.videoItem = getVideoItem(bean, image);

        if (position == needReopenPositon) {
            needReopenPositon = -1;
            mVideoHelper.reOpenVideo(currentPlayingFile);
        } else {
            mVideoHelper.openVideo(currentPlayingFile);
        }
        playStatus = STATUS_PLAYING;
        isPlaying = true;
        if (portrait) {
            ViewUtils.hideChildrenView(mPortraitPlayingContainer);

            ViewParent parent = mWrapper.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(mWrapper);
            }

            mWrapper.setVisibility(View.VISIBLE);
            if (mPortraitPlayingContainer != null) {
                mPortraitPlayingContainer.addView(mWrapper);
            }
        }
        mClickToPlayPositon = position;
        if (IfengApplication.mobileNetCanPlay && NetUtils.isMobile(mContext)) {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }

        requestVideoDanmuData(bean.getMemberItem().getGuid());
//        requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
        requesetDanmaAllowSend(bean.getMemberItem().getGuid());

    }

    public boolean hasNext(int adapterPosition) {
        if (ListUtils.isEmpty(mDataList)) {
            return false;
        }
        return adapterPosition < mDataList.size() - 1;
    }

    private List<String> keyList = new ArrayList<>();
    private List<VideoDanmuItem> danmuItems = new ArrayList<>();
    private List<Integer> tmpList = new ArrayList<>();

    public List<VideoDanmuItem> getDanmuItems() {
        return danmuItems;
    }

//    private boolean isShowDanma;

//    public void setShowDanma(boolean isShow) {
//        //this.isShowDanma = isShow;
//    }

    //处理随机key值
    private void requestVideoDanmuData(final String guid) {
        String url = DataInterface.getVideoDanmuUrl(guid);
        Log.d("danmu", "guid:" + url);

        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }

                        try {
                            JSONObject jsonObj = new JSONObject(response.toString());
                            String responseStr = response.toString();
                            if (responseStr.contains("code")) {
                                if (!jsonObj.getString("code").equals("1")) {
                                    if (IfengApplication.danmaSwitchStatus) {
                                        ToastUtils.getInstance().showShortToast("获取弹幕信息失败，请稍后重试。");
                                    }
                                    return;
                                }
                            }
                            keyList.clear();
                            tmpList.clear();
                            Iterator<String> iterable = jsonObj.keys();
                            while (iterable.hasNext()) {
                                String str = iterable.next();
                                int index = str.indexOf("t");
                                tmpList.add(Integer.parseInt(str.substring(index + 1, str.length())));
                            }
                            Collections.sort(tmpList);
                            for (int i = 0; i < tmpList.size(); i++) {
                                String tmpStr = "t" + tmpList.get(i);
                                keyList.add(tmpStr);
                            }
                            getVideoDammukuData(guid, response.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    private void getVideoDammukuData(String guid, String jsonStr) {
        if (TextUtils.isEmpty(jsonStr)) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);

            danmuItems.clear();
            int arratSize = keyList.size();
            for (int i = 0; i < arratSize; i++) {
                JSONArray array = jsonObject.getJSONArray(keyList.get(i));
                int size = array.length();
                for (int j = 0; j < size; j++) {
                    VideoDanmuItem item = new Gson().fromJson(array.get(j).toString(), VideoDanmuItem.class);
                    item.setFromUser(false);
                    danmuItems.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (null != danmuView) {
            if (mFragment != null) {
                mFragment.setDanmaData(guid, danmuItems);
            }
        }
    }

    private boolean isCanPost;

    private void requesetDanmaAllowSend(String guid) {
        String url = DataInterface.getVideoEditUrl(guid);
        Log.d("danmu", "IsSend:" + url);
        CommonDao.sendRequest(url, DanmuItem.class, new Response.Listener<DanmuItem>() {
            @Override
            public void onResponse(DanmuItem response) {
                if (response == null || TextUtils.isEmpty(response.toString())) {
                    return;
                }
                DanmuItem item = response;
                if (!TextUtils.isEmpty(item.getCode())) {
                    if (item.getCode().equals("1")) {
                        if ("1".equals(item.getData().getCan_post())) {
                            isCanPost = true;
                            if (null != mFragment) {
                                mFragment.setDanmuSendStatus(isCanPost);
                            }
                        } else {
                            isCanPost = false;
                            if (null != mFragment) {
                                mFragment.setDanmuSendStatus(isCanPost);
                            }
                        }
                    } else {
                        isCanPost = false;
                        if (null != mFragment) {
                            mFragment.setDanmuSendStatus(isCanPost);
                        }
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }


}
