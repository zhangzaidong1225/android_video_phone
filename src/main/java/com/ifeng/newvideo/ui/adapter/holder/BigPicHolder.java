package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by guolc on 2016/7/25.
 */
public class BigPicHolder {
    public TextView title;
    public NetworkImageView picture;
    public TextView author;
    public TextView playTimes;
    public TextView when;
    public TextView label;
    public TextView duration;
    //    public NetworkImageView avatar;
    public TextView liveStatus;
    public TextView onlineCount;

    public static BigPicHolder getBigPicHolder(View convertView) {
        BigPicHolder bigPicHolder = null;
        if (convertView.getTag() == null) {
            bigPicHolder = new BigPicHolder();
            bigPicHolder.title = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_pic_title);
            bigPicHolder.picture = (NetworkImageView) convertView.findViewById(R.id.cniv_ad_big_pic);
            bigPicHolder.duration = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_pic_duration);
            bigPicHolder.liveStatus = (TextView) convertView.findViewById(R.id.tv_big_pic_live_status);
//            bigPicHolder.avatar = (NetworkImageView) convertView.findViewById(R.id.niv_well_chosen_big_pic_head);
            bigPicHolder.author = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_pic_author);
            bigPicHolder.playTimes = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_pic_play_times);
            bigPicHolder.when = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_when);
            bigPicHolder.label = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_pic_tag);
            bigPicHolder.onlineCount = (TextView) convertView.findViewById(R.id.tv_well_chosen_big_online_count);
            convertView.setTag(bigPicHolder);
        } else {
            bigPicHolder = (BigPicHolder) convertView.getTag();
        }
        return bigPicHolder;
    }
}
