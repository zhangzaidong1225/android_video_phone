package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.FmChannelFragment;
import com.ifeng.newvideo.ui.IfengTVFragment;
import com.ifeng.newvideo.ui.WellChosenFragment;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.utils.ActivityUtils;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.ChannelBean.HomePageBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * listView 的头部ViewPager的adapter
 * Created by antboyqi on 14-8-27.
 */
public class HeaderViewPagerAdapter extends PagerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(HeaderViewPagerAdapter.class);
    private List<ChannelBean.HomePageBean> mHeaders;
    private List<MainAdInfoModel> mAdDataList = new ArrayList<>();
    private List<ChannelBean.HomePageBean> insertList = new ArrayList<>();
    public OnDataChanged onDataChanged;
    private int refresh_times;
    private String channel_id;
    private Context mContext;
    private BaseFragment fragment;
    protected static final String tag_head = "head";
    private static int currentPosition = -1;
    private int adEmptyPosition = -1;
    private List<HomePageBean> tmpList;
    private int adEmptyNo;

    public interface OnDataChanged {

        void onDataChanged(ChannelBean.HomePageBean header, int size, int pos);
    }

    public void increaseRefreshTimes() {
        refresh_times++;
    }

    public HeaderViewPagerAdapter(BaseFragment fragment, Context context, String channel_id) {
        this.fragment = fragment;
        mContext = context;
        mHeaders = new ArrayList<>();
        tmpList = new ArrayList<>();
        this.channel_id = channel_id;
    }

    @Override
    public int getCount() {
        int count = mHeaders.size();
        return (count <= 1) ? count : Integer.MAX_VALUE;
    }

    public void setData(List<ChannelBean.HomePageBean> headers) {
        if (headers != null) {
            mHeaders.clear();
            for (HomePageBean bean : headers) {
                if (null != bean && !TextUtils.isEmpty(bean.getImageList().get(0).getImage())) {
                    mHeaders.add(bean);
                }
            }
            tmpList.clear();
            tmpList.addAll(headers);
            adEmptyNo = headers.size() - mHeaders.size();
        }
    }

    public List<MainAdInfoModel> getAdDataList() {
        return mAdDataList;
    }

    public void clearData() {
        mHeaders.clear();
    }

    public List<ChannelBean.HomePageBean> getData() {
        return mHeaders;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int index = position % mHeaders.size();
        HomePageBean bean = mHeaders.get(index);

        View view = getView(bean, index);
        if (view != null) {
            container.addView(view);
            view.setTag(position);
        }

        if (!CheckIfengType.isAD(bean.getMemberType())) {
            addFragmentHeaderList(fragment, bean.getInfoId(), position, mHeaders.size(), bean.getMemberItem().getSimId(), bean.getMemberItem().getrToken(), bean.getMemberItem().getPayload());
        }
        return view;
    }

    private void addFragmentHeaderList(BaseFragment fragment, String id, int position, int count, String simid, String rToken, String payload) {
        if (fragment instanceof WellChosenFragment) {
            CommonStatictisListUtils.getInstance().addHeaderViewFocusList(id, position, PageIdConstants.REFTYPE_EDITOR, channel_id,
                    count, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.wellChosen, simid, rToken, payload);
        }

        if (fragment instanceof FmChannelFragment) {
            CommonStatictisListUtils.getInstance().addHeaderViewFocusList(id, position, PageIdConstants.REFTYPE_EDITOR, channel_id,
                    count, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.fm, simid, rToken, payload);
        }

        if (fragment instanceof IfengTVFragment) {
            CommonStatictisListUtils.getInstance().addHeaderViewFocusList(id, position, PageIdConstants.REFTYPE_EDITOR, channel_id,
                    count, CommonStatictisListUtils.HEADERVIEW_TYPE, CommonStatictisListUtils.ifengTv, simid, rToken, payload);
        }

    }

    private NetworkImageView initItemView(final ChannelBean.HomePageBean header, int index) {
        NetworkImageView imageView = new NetworkImageView(mContext);
        final int focusNum = index;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext == null) {
                    return;
                }
                switchTypeJump(header);
                PageActionTracker.clickHomeChFocusX(channel_id, focusNum);

//                String simId = header.getMemberItem() == null ? "" : header.getMemberItem().getSimId();
//                String rToken = header.getMemberItem() == null ? "" : header.getMemberItem().getrToken();
//                PageActionTracker.clickHomeItem(focusNum, simId, rToken);
            }
        });

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = 300;
        imageView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        String imageUrl = "";
        if (header != null) {
            AdTools.loadImpressionUrl(header);//曝光
            imageUrl = header.getImage();

            if (TextUtils.isEmpty(imageUrl) && !ListUtils.isEmpty(header.getImageList())) {
                imageUrl = header.getImageList().get(0).getImage();
            }
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageView = null;
            return imageView;
        }
        imageView.setImageUrl(imageUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(R.drawable.bg_default_pic);
        imageView.setErrorImageResId(R.drawable.bg_default_pic);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (object instanceof View) {
            container.removeView((View) object);
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (onDataChanged != null && mHeaders != null && mHeaders.size() != 0) {
            int index = position % mHeaders.size();
            logger.debug("index-----：{}", index);
            ChannelBean.HomePageBean homePageBean = mHeaders.get(index);
            onDataChanged.onDataChanged(homePageBean, mHeaders.size(), index);

            if (currentPosition != position) {
                currentPosition = position;
                sendBannerADShow(container, homePageBean);

                //处理广告空曝光
                if (index == mHeaders.size() - 1 && adEmptyNo != 0) {
                    while (adEmptyNo != 0) {
                        index += 1;
                        sendAdEmptyExpose(index);
                        adEmptyNo -= 1;
                    }
                } else {
                    sendAdEmptyExpose(index);
                }
            }

        }
        super.setPrimaryItem(container, position, object);
    }

    private void sendAdEmptyExpose(int index) {
        HomePageBean adBean = tmpList.get(index);
        if (null != adBean && TextUtils.isEmpty(adBean.getMemberItem().imageURL)) {
            //广告空曝光
            if (CheckIfengType.isAD(adBean.getMemberType())) {
                //adEmptyPosition = position;
                //在此发送空曝光
                Log.d("header:{}", "---adEmptyShow--" + index);
                AdTools.exposeAdPvUrl(adBean);
                ADRecord.addAdShow(adBean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);//曝光
//                CommonStatictisListUtils.getInstance().sendADEmptyExpose(adBean.getMemberItem().adPositionId);
            }
        }
    }

    private void sendBannerADShow(ViewGroup container, HomePageBean homePageBean) {
        //view 的可见性
        Rect globalRect = new Rect();
        boolean globalVisibility = container.getGlobalVisibleRect(globalRect);

        boolean isVisibility = ActivityUtils.isApplicationBroughtToBackground(mContext);

        if (globalVisibility && globalRect.top != 0 && !isVisibility) {
            if (CheckIfengType.isAD(homePageBean.getMemberType())) {
                logger.debug("header:{}", "adShow");
                Log.d("header:{}", "adShow");
                AdTools.exposeAdPvUrl(homePageBean);
                ADRecord.addAdShow(homePageBean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
                CommonStatictisListUtils.getInstance().sendADInfo(homePageBean.getMemberItem().getAdPositionId(), homePageBean.getInfoId(), channel_id);
            } else {
                AdTools.loadImpressionUrl(homePageBean);
            }
        }
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private View getView(final ChannelBean.HomePageBean header, int index) {
        return initItemView(header, index);
    }

    /**
     * 广告曝光地址，在加载广告图的时候去请求即可，不需要处理返回结果
     */
    private void switchTypeJump(ChannelBean.HomePageBean header) {
        if (header == null) {
            return;
        }
        String type = header.getMemberType();
        if (TextUtils.isEmpty(type)) {
            return;
        }
        ChannelBean.MemberItemBean memberItem = header.getMemberItem();

        // 广告
        if (CheckIfengType.isAD(type)) {
            //无线广告点击曝光
            String adId = header.getInfoId();
            if (CheckIfengType.shouldClickExposure(memberItem.adAction.type)) {
                ADRecord.addAdClick(adId, ADRecord.AdRecordModel.ADTYPE_INFO);
                AdvertExposureDao.sendAdvertClickReq(adId, memberItem.adAction.async_click);
            }
            String title = header.getTitle();
            String imageUrl = getImageUrl(header, memberItem);
            String adType = "";
            String clickUrl = "";
            String liveStatus = "";
            String shareUrl = memberItem.getmUrl();
            String appScheme = memberItem.getAppScheme();
            String appUrl = memberItem.getAppUrl();
            if (CheckIfengType.isAdnew(type)) {
                //已安装新闻客户端：取schema数据，拉起新闻客户端; 未安装：使用appurl跳转到内部webview页面
                //appScheme 样例：comifengnewsclient://call?type=doc&aid=102722589
                if (PackageUtils.checkAppExist(mContext, PackageUtils.NEWS_PACKAGE_NAME)) {
                    IntentUtils.startIfengNewsApp(mContext, appScheme, channel_id);
                    return;
                } else {
                    adType = type;
                    clickUrl = appUrl;
                }
            } else if (CheckIfengType.isAdapp(type)) {
                adType = type;
                clickUrl = appUrl;
            } else if (IfengType.TYPE_ADVERT.equalsIgnoreCase(type)) {
                adType = memberItem.getClickType();
                clickUrl = memberItem.getClickUrl();
                liveStatus = memberItem.getLiveStatus();
            } else {
                adType = memberItem != null ? memberItem.adAction.type : "";
                clickUrl = memberItem != null ? memberItem.adAction.url : "";
            }
            List<String> list = new ArrayList<>();
            if (null != memberItem) {
                if (!ListUtils.isEmpty(memberItem.adAction.async_downloadCompletedurl)) {
                    list.addAll(memberItem.adAction.async_downloadCompletedurl);
                }
                if (!ListUtils.isEmpty(memberItem.adAction.downloadCompletedurl)) {
                    list.addAll(memberItem.adAction.downloadCompletedurl);
                }
            }
            ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, shareUrl, appUrl,
                    appScheme, mContext, list, channel_id,
                    memberItem != null ? memberItem.getSearchPath() : "", memberItem.adAction.async_download);
        }
        // 专题
        else if (CheckIfengType.isTopicType(type)) {
            String guid = "";
            if (memberItem != null) {
                guid = memberItem.getGuid();
            }
            String topicId = header.getTopicId();
            if (TextUtils.isEmpty(topicId) && memberItem != null) {
                topicId = memberItem.getTopicId();
            }
            IntentUtils.addHomePageShareData(fragment, header);
            IntentUtils.toTopicDetailActivity(mContext, guid, topicId, channel_id, type, false, 0, "");
        }
        // 直播
        else if (CheckIfengType.isLiveType(type)) {
            toFragmentLiveTab(header);
            /*String guid = header.getMemberItem() != null ? header.getMemberItem().getGuid() : "";
            String chid = header.getMemberItem() != null ? header.getMemberItem().getSearchPath() : "";
            IntentUtils.addHomePageShareData(fragment, header);
            IntentUtils.startActivityTVLive(mContext, guid, channel_id, chid,
                    header.getWeMedia() != null ? header.getWeMedia().getId() : "",
                    header.getWeMedia() != null ? header.getWeMedia().getName() : "", false);*/
        }
        // 点播
        else if (CheckIfengType.isVideo(type)) {
            String guid = "";
            if (memberItem != null) {
                guid = memberItem.getGuid();
            }
            IntentUtils.addHomePageShareData(fragment, header);
            IntentUtils.toVodDetailActivity(mContext, guid, channel_id, false, false, 0, "");
        }
        // 签到
        else if (CheckIfengType.isSignIn(type)) {
            IntentUtils.startSignInActivity(mContext);
        } else if (CheckIfengType.isVRLive(type)) {
            if (memberItem == null || TextUtils.isEmpty(memberItem.getLiveUrl())) {
                return;
            }
            String vrLiveUrl = memberItem.getLiveUrl();
            String vrGroupTitle = header.getTitle();
            String imageUrl = header.getImage();
            String vrId = header.getInfoId();
            String shareUrl = header.getMemberItem().getmUrl();
            String weMediaId = header.getWeMedia() != null ? header.getWeMedia().getId() : "";
            String weMediaName = header.getWeMedia() != null ? header.getWeMedia().getName() : "";
            String chid = memberItem.getSearchPath();
            IntentUtils.startVRLiveActivity(mContext, vrId, vrLiveUrl, vrGroupTitle, channel_id, chid,
                    weMediaId, weMediaName, imageUrl, shareUrl);
        } else if (CheckIfengType.isVRVideo(type)) {
            List<String> videoList = new ArrayList<>();
            videoList.add(header.getInfoId());
            String chid = memberItem != null ? memberItem.getSearchPath() : "";
            IntentUtils.startVRVideoActivity(mContext, 0, channel_id, chid, 0, videoList, true);
        }
    }

    private String getImageUrl(ChannelBean.HomePageBean bean, ChannelBean.MemberItemBean memberItem) {
        List<ChannelBean.HomePageBean.ImageBean> imageList = bean.getImageList();
        String imageUrl = "";
        if (!ListUtils.isEmpty(imageList)) {
            imageUrl = imageList.get(0).getImage();
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = memberItem.imageURL;
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = memberItem.getImage();
        }
        return imageUrl;
    }

    private void toFragmentLiveTab(HomePageBean header) {
        if (fragment.getActivity() instanceof ActivityMainTab) {
            String shareTitle = header.getTitle();
            String shareImg = "";
            List<HomePageBeanBase.ImageBean> imageList = header.getImageList();
            if (!ListUtils.isEmpty(imageList)) {
                shareImg = imageList.get(0).getImage();
            } else if (!TextUtils.isEmpty(header.getImage())) {
                shareImg = header.getImage();
            }

            if (CheckIfengType.isAD(header.getMemberType())) {
                shareImg = header.getImage();
            }
            String guid = header.getMemberItem() != null ? header.getMemberItem().getGuid() : "";
            String chid = header.getMemberItem() != null ? header.getMemberItem().getSearchPath() : "";
            ((ActivityMainTab) fragment.getActivity()).setCurrentTabToLive(guid, channel_id, chid, shareTitle, shareImg, true);
        }
    }


}
