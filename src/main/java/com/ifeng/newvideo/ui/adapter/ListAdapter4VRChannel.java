package com.ifeng.newvideo.ui.adapter;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.List;

/**
 * VR频道页适配器
 * Created by Administrator on 2016/11/22.
 */
public class ListAdapter4VRChannel extends BaseAdapter {

    private String channelId;

    public ListAdapter4VRChannel(String channelId) {
        this.channelId = channelId;
    }

    protected List<ChannelBean.HomePageBean> mDataList = new ArrayList<>();

    @Override
    public Object getItem(int i) {
        return mDataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        OnClick onClickListener;
        ChannelBean.HomePageBean bean = mDataList.get(position);
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_channel_big_picture, viewGroup, false);
            onClickListener = new OnClick();
            holder = new ViewHolder();
            holder.initHolderView(view);
            view.setTag(holder);
            view.setTag(R.id.layout_intro, onClickListener);

        } else {
            holder = (ViewHolder) view.getTag();
            onClickListener = (OnClick) view.getTag(R.id.layout_intro);
        }

        onClickListener.setPosition(position);
        bindData(holder, mDataList.get(position), onClickListener);
        if (CheckIfengType.isAD(bean.getMemberType())) {
            CommonStatictisListUtils.getInstance().sendADInfo(bean.getMemberItem().getAdPositionId(), bean.getInfoId(), channelId);
        } else {
            CommonStatictisListUtils.getInstance().addPullRefreshViewFocusList(bean.getInfoId(), position, PageIdConstants.REFTYPE_EDITOR, channelId,
                    0, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.vr);
        }
        return view;
    }

    private void bindData(ViewHolder holder, ChannelBean.HomePageBean bean, OnClick click) {
        holder.playStatus.setImageResource(R.drawable.btn_play);
        holder.playStatus.setOnClickListener(click);
        holder.toShare.setOnClickListener(click);
        holder.small_head_view.setOnClickListener(click);
        holder.mask.setOnClickListener(click);
        holder.userName.setOnClickListener(click);
        holder.title.setOnClickListener(click);
        holder.bottom.setOnClickListener(click);
        holder.playLayout.setOnClickListener(click);

        holder.title.setText(bean.getTitle());
        markTextGray(holder.title, bean);
        List<HomePageBeanBase.ImageBean> imageBeanList = bean.getImageList();
        if (!ListUtils.isEmpty(imageBeanList)) {
            setImageUrl(holder.networkImageView, imageBeanList.get(0).getImage(), R.drawable.bg_default_pic);
        }
        ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
        String headPic = null;
        String wemediaName = null;
        if (weMedia != null && !TextUtils.isEmpty(weMedia.getId()) && !TextUtils.isEmpty(weMedia.getName())) {
            headPic = weMedia.getHeadPic();
            wemediaName = weMedia.getName();
        }
        boolean isWeMediaName = TextUtils.isEmpty(wemediaName);
        if (TextUtils.isEmpty(headPic) && isWeMediaName) {
            holder.small_head_view.setVisibility(View.GONE);
            holder.mask.setVisibility(View.GONE);
        } else {
            setImageUrl(holder.small_head_view, headPic, R.drawable.avatar_default);
            holder.small_head_view.setVisibility(View.VISIBLE);
            holder.mask.setVisibility(View.VISIBLE);
        }
        holder.userName.setText(wemediaName);
        holder.userName.setVisibility(isWeMediaName ? View.GONE : View.VISIBLE);

        ChannelBean.MemberItemBean memberItemBean = bean.getMemberItem();
        if (memberItemBean != null) {
            holder.duration.setText(StringUtils.changeDuration(memberItemBean.getDuration()));
            holder.playTimes.setText(StringUtils.changePlayTimes(memberItemBean.getPlayTime()));
        }
    }

    protected void markTextGray(TextView textView, HomePageBeanBase bean) {
        if (bean != null && bean.isWatched()) {
            textView.setTextColor(Color.parseColor("#a0a0a0"));
        } else {
            textView.setTextColor(Color.parseColor("#262626"));
        }
    }

    protected void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
    }

    public void setDataList(List<ChannelBean.HomePageBean> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (onVideoItemClick == null) {
                return;
            }
            ChannelBean.HomePageBean bean = mDataList.get(position);
            switch (view.getId()) {
                case R.id.iv_channel_big_pic_to_share:
                    onVideoItemClick.onShareClick(view, bean);
                    break;
                case R.id.tv_channel_big_pic_user_name:
                case R.id.niv_left_avatar_mask:
                case R.id.niv_channel_big_pic_head:
                    onVideoItemClick.onWeMediaClick(bean, position);
                    break;
                case R.id.tv_channel_big_pic_title:
                case R.id.iv_channel_big_pic_play_status:
                case R.id.rl_locate:
                case R.id.layout_intro:
                    onVideoItemClick.onPlayVideoClick(position);
                    bean.setWatched(true);
                    notifyDataSetChanged();
                    break;
            }
        }
    }

    static class ViewHolder {
        TextView title;
        TextView tv_comment;
        TextView duration;
        TextView playTimes;
        TextView userName;
        ImageView playStatus;
        View playLayout;
        ImageView toShare;
        NetworkImageView networkImageView;
        NetworkImageView small_head_view;
        View bottom;
        ImageView mask;


        private void initHolderView(View convertView) {
            title = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_title);
            playStatus = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_play_status);
            playLayout= convertView.findViewById(R.id.rl_locate);
            networkImageView = (NetworkImageView) convertView.findViewById(R.id.cniv_channel_big_pic);
            networkImageView.setDefaultImageResId(R.drawable.bg_default_pic);
            networkImageView.setErrorImageResId(R.drawable.login_ifeng_logo);
            small_head_view = (NetworkImageView) convertView.findViewById(R.id.niv_channel_big_pic_head);
            small_head_view.setDefaultImageResId(R.drawable.bg_default_pic);
            small_head_view.setErrorImageResId(R.drawable.login_ifeng_logo);
            mask = (ImageView) convertView.findViewById(R.id.niv_left_avatar_mask);
            playTimes = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_play_times);
            tv_comment = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_comment);
            tv_comment.setVisibility(View.GONE);
            toShare = (ImageView) convertView.findViewById(R.id.iv_channel_big_pic_to_share);
            duration = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_duration);
            userName = (TextView) convertView.findViewById(R.id.tv_channel_big_pic_user_name);
            bottom = convertView.findViewById(R.id.layout_intro);
        }
    }

    private OnVideoItemClick onVideoItemClick;

    public void setOnVideoItemClick(OnVideoItemClick onVideoItemClick) {
        this.onVideoItemClick = onVideoItemClick;
    }

    public interface OnVideoItemClick {

        void onShareClick(View view, ChannelBean.HomePageBean bean);

        void onWeMediaClick(ChannelBean.HomePageBean bean, int position);

        void onPlayVideoClick(int position);
    }
}
