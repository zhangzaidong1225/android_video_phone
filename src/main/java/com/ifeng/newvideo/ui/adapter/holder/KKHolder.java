package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by guolc on 2016/8/19.
 */
public class KKHolder {
    public TextView online_count;
    public TextView title;
    public TextView label;
    public NetworkImageView image;

    public static final KKHolder getKKHolder(View convertView) {
        KKHolder kkHolder = null;
        if (convertView.getTag() == null) {
            kkHolder = new KKHolder();
            kkHolder.label= (TextView) convertView.findViewById(R.id.tv_ad_banner_label);
            kkHolder.title = (TextView) convertView.findViewById(R.id.tv_kk_title);
            kkHolder.online_count = (TextView) convertView.findViewById(R.id.tv_online_count);
            kkHolder.image = (NetworkImageView) convertView.findViewById(R.id.iv_right_picture_0);
            convertView.setTag(kkHolder);
        } else {
            kkHolder = (KKHolder) convertView.getTag();
        }
        return kkHolder;
    }
}
