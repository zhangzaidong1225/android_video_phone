package com.ifeng.newvideo.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * listView 的头部ViewPager的adapter
 * Created by antboyqi on 14-8-27.
 */
public class LianBoHeaderViewPagerAdapter extends PagerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LianBoHeaderViewPagerAdapter.class);
    private List<HomePageBeanBase> mHeaders;
    private List<ChannelBean.HomePageBean> insertList = new ArrayList<>();
    private List<MainAdInfoModel> mAdDataList = new ArrayList<>();
    public OnDataChanged onDataChanged;
    private int refresh_times;
    private String channel_id;
    private Context mContext;
    private BaseFragment fragment;
    private static final String tag_head = "head";

    public interface OnDataChanged {
        void onDataChanged(HomePageBeanBase header, int size, int pos);
    }

    public void increaseRefreshTimes() {
        refresh_times++;
    }

    public LianBoHeaderViewPagerAdapter(BaseFragment fragment, Context context, String channel_id) {
        mContext = context;
        this.fragment = fragment;
        mHeaders = new ArrayList<>();
        this.channel_id = channel_id;
    }

    @Override
    public int getCount() {
        int count = mHeaders.size();
        return (count <= 1) ? count : Integer.MAX_VALUE;
    }

    public void setData(List<HomePageBeanBase> headers) {
        if (headers != null) {
            mHeaders.clear();
            mHeaders.addAll(headers);
        }
    }

    public void clearData() {
        mHeaders.clear();
    }

    public List<HomePageBeanBase> getData() {
        return mHeaders;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int index = position % mHeaders.size();
        HomePageBeanBase bean = mHeaders.get(index);
        if (CheckIfengType.isAD(bean.getMemberType())) {
//            ADRecord.addAdShow(bean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
            if (!TextUtils.isEmpty(bean.getMemberItem().getImage())) {
                ADRecord.addAdShow(bean.getMemberItem().adId, ADRecord.AdRecordModel.ADTYPE_INFO);
            }
            CommonStatictisListUtils.getInstance().sendADInfo(bean.getMemberItem().getAdPositionId(), bean.getItemId(), channel_id);
            if (bean.isNeed2Expose()) {
                bean.setNeed2Expose(false);
                AdTools.exposeAdPvUrl(bean);
            }
        } else {
            AdTools.loadImpressionUrl(bean);
        }
        View view = getView(bean, index);
        if (!CheckIfengType.isAD(bean.getMemberType())) {
            CommonStatictisListUtils.getInstance().addHeaderViewFocusList(bean.getItemId(), position, PageIdConstants.REFTYPE_EDITOR, channel_id,
                    mHeaders.size(), CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.lianbo);
        }
        if (view != null) {
            container.addView(view);
        }
        return view;
    }


    private NetworkImageView initItemView(final HomePageBeanBase header, int index) {
        NetworkImageView imageView = new NetworkImageView(mContext);
        final int focusNum = index;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext == null) {
                    return;
                }
                switchTypeJump(header);
                PageActionTracker.clickHomeChFocusX(channel_id, focusNum);

//                String simId = header.getMemberItem() == null ? "" : header.getMemberItem().getSimId();
//                String rToken = header.getMemberItem() == null ? "" : header.getMemberItem().getrToken();
//                PageActionTracker.clickHomeItem(focusNum, simId, rToken);
            }
        });

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = 300;
        imageView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        String imageUrl = "";
        if (header != null) {
            AdTools.loadImpressionUrl(header);//曝光
            imageUrl = header.getImage();

            if (TextUtils.isEmpty(imageUrl) && !ListUtils.isEmpty(header.getImageList())) {
                imageUrl = header.getImageList().get(0).getImage();
            }
        }
        imageView.setImageUrl(imageUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(R.drawable.bg_default_pic);
        imageView.setErrorImageResId(R.drawable.bg_default_pic);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (object instanceof View) {
            container.removeView((View) object);
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (onDataChanged != null && mHeaders != null && mHeaders.size() != 0) {
            HomePageBeanBase homePageBean = mHeaders.get(position % mHeaders.size());
            onDataChanged.onDataChanged(homePageBean, mHeaders.size(), position % mHeaders.size());
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    private View getView(final HomePageBeanBase header, int index) {
        return initItemView(header, index);
    }

    /**
     * 广告曝光地址，在加载广告图的时候去请求即可，不需要处理返回结果
     */
    private void switchTypeJump(HomePageBeanBase header) {
        if (header == null) {
            return;
        }
        String type = header.getMemberType();
        if (TextUtils.isEmpty(type)) {
            return;
        }
        ChannelBean.MemberItemBean memberItem = header.getMemberItem();

        // 广告
        if (CheckIfengType.isAD(type)) {
            //无线广告点击曝光
            String adId = memberItem.getItemId();
            if (CheckIfengType.shouldClickExposure(memberItem.adAction.type)) {
                ADRecord.addAdClick(adId, ADRecord.AdRecordModel.ADTYPE_INFO);
                // AdvertExposureDao.sendAdvertClickReq(adId, memberItem.getAsync_click());
                // AdvertExposureDao.sendAdvertClickReq(adId, memberItem.getAsync_download());
            }
            String imageUrl = getImageUrl(header, memberItem);
            String title = header.getTitle();
            String shareUrl = memberItem.getmUrl();
            String adType = "";
            String clickUrl = "";
            String appScheme = memberItem.getAppScheme();
            String appUrl = memberItem.getAppUrl();
            if (CheckIfengType.isAdnew(type)) {
                //已安装新闻客户端：取schema数据，拉起新闻客户端; 未安装：使用appurl跳转到内部webview页面
                //appScheme 样例：comifengnewsclient://call?type=doc&aid=102722589
                if (PackageUtils.checkAppExist(mContext, PackageUtils.NEWS_PACKAGE_NAME)) {
                    IntentUtils.startIfengNewsApp(mContext, appScheme, channel_id);
                    return;
                } else {
                    adType = type;
                    clickUrl = appUrl;
                }
            } else if (CheckIfengType.isAdapp(type)) {
                adType = type;
                clickUrl = appUrl;
            } else if (IfengType.TYPE_ADVERT.equalsIgnoreCase(type)) {
                adType = memberItem.getClickType();
                clickUrl = memberItem.getClickUrl();
            } else {
                adType = memberItem.adAction.type;
                clickUrl = memberItem.adAction.url;
            }
            List<String> list = new ArrayList<>();
            if (null != memberItem) {
                if (!ListUtils.isEmpty(memberItem.adAction.async_downloadCompletedurl)) {
                    list.addAll(memberItem.adAction.async_downloadCompletedurl);
                }
                if (!ListUtils.isEmpty(memberItem.adAction.downloadCompletedurl)) {
                    list.addAll(memberItem.adAction.downloadCompletedurl);
                }
            }
            ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, shareUrl, appUrl,
                    appScheme, mContext, list, channel_id,
                    memberItem != null ? memberItem.getSearchPath() : "", memberItem.adAction.async_download);
        }
        // 专题
        else if (CheckIfengType.isTopicType(type)) {
            String guid = "";
            if (memberItem != null) {
                guid = memberItem.getGuid();
            }
            String topicId = header.getTopicId();
            if (TextUtils.isEmpty(topicId) && memberItem != null) {
                topicId = memberItem.getTopicId();
            }
            IntentUtils.addHomePageShareData(fragment, header);
            IntentUtils.toTopicDetailActivity(mContext, guid, topicId, channel_id, type, false, 0, "");
        }
        // 直播
        else if (CheckIfengType.isLiveType(type)) {
            toFragmentLiveTab(header);
            //String guid = header.getMemberItem() != null ? header.getMemberItem().getGuid() : "";
            //String chid = header.getMemberItem() != null ? header.getMemberItem().getSearchPath() : "";
            //IntentUtils.addHomePageShareData(fragment, header);
            //IntentUtils.startActivityTVLive(mContext, guid, channel_id, chid,
            //        header.getWeMedia() != null ? header.getWeMedia().getId() : "",
            //        header.getWeMedia() != null ? header.getWeMedia().getName() : "", false);
        }
        // 点播
        else if (CheckIfengType.isVideo(type)) {
            String guid = "";
            if (memberItem != null) {
                guid = memberItem.getGuid();
            }
            IntentUtils.addHomePageShareData(fragment, header);
            IntentUtils.toVodDetailActivity(mContext, guid, channel_id, false, false, 0, "");
        }
        // 签到
        else if (CheckIfengType.isSignIn(type)) {
            IntentUtils.startSignInActivity(mContext);
        } else if (CheckIfengType.isVRLive(type)) {
            if (memberItem == null || TextUtils.isEmpty(memberItem.getLiveUrl())) {
                return;
            }
            String vrLiveUrl = memberItem.getLiveUrl();
            String vrGroupTitle = header.getTitle();
            String imageUrl = header.getImage();
            String vrId = header.getItemId();
            String shareUrl = header.getMemberItem().getmUrl();
            String weMediaId = header.getWeMedia() != null ? header.getWeMedia().getId() : "";
            String weMediaName = header.getWeMedia() != null ? header.getWeMedia().getName() : "";
            String chid = memberItem.getSearchPath();
            IntentUtils.startVRLiveActivity(mContext, vrId, vrLiveUrl, vrGroupTitle, channel_id, chid,
                    weMediaId, weMediaName, imageUrl, shareUrl);
        } else if (CheckIfengType.isVRVideo(type)) {
            List<String> videoList = new ArrayList<>();
            videoList.add(header.getItemId());
            IntentUtils.startVRVideoActivity(mContext, 0, channel_id, memberItem.getSearchPath(), 0, videoList, true);
        }

    }

    private String getImageUrl(HomePageBeanBase bean, ChannelBean.MemberItemBean memberItem) {
        List<ChannelBean.HomePageBean.ImageBean> imageList = bean.getImageList();
        String imageUrl = "";
        if (!ListUtils.isEmpty(imageList)) {
            imageUrl = imageList.get(0).getImage();
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = memberItem.imageURL;
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = memberItem.getImage();
        }
        return imageUrl;
    }

    private void toFragmentLiveTab(HomePageBeanBase header) {
        if (fragment.getActivity() instanceof ActivityMainTab) {
            String shareTitle = header.getTitle();
            String shareImg = "";
            List<HomePageBeanBase.ImageBean> imageList = header.getImageList();
            if (!ListUtils.isEmpty(imageList)) {
                shareImg = imageList.get(0).getImage();
            } else if (!TextUtils.isEmpty(header.getImage())) {
                shareImg = header.getImage();
            }

            if (CheckIfengType.isAD(header.getMemberType())) {
                shareImg = header.getImage();
            }
            String guid = header.getMemberItem() != null ? header.getMemberItem().getGuid() : "";
            String chid = header.getMemberItem() != null ? header.getMemberItem().getSearchPath() : "";
            ((ActivityMainTab) fragment.getActivity()).setCurrentTabToLive(guid, channel_id, chid, shareTitle, shareImg, true);
        }
    }


}
