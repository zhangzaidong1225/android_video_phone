package com.ifeng.newvideo.ui.adapter;


import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.CommentItem;

import java.util.ArrayList;
import java.util.List;

public class MsgCommentAdapter extends BaseAdapter {

    public static final int SIZE_THREE = 3;//展示三个
    public static final int SIZE_MAX = Integer.MAX_VALUE;//不限制
    private int mSize = SIZE_MAX;
    private boolean mIsShowBottomLine = true;//是否显示底部横线

    private List<CommentItem> mList = new ArrayList<>();


    @Override
    public int getCount() {
        int tmpSize = mList.size();
        return tmpSize >= mSize ? mSize : tmpSize;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setData(List<CommentItem> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MsgHolder msgHolder = null;
        OnClick onClick = null;
        if (view == null) {
            msgHolder = new MsgHolder();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_msg_comment_layout, viewGroup, false);
            msgHolder.initView(view);
            view.setTag(R.id.view_parent, onClick);
            view.setTag(msgHolder);
        } else {
            msgHolder = (MsgHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        msgHolder.itemView.setOnClickListener(onClick);
        final CommentItem comment = mList.get(i);


        if (!TextUtils.isEmpty(comment.getUname())) {
            msgHolder.tvWemedia.setText(comment.getUname());
        } else {
            msgHolder.tvWemedia.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(comment.getComment_date())) {
            msgHolder.tvTimer.setText(DateUtils.getCommentTime(comment.getComment_date()));
        } else {
            msgHolder.tvTimer.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(comment.getDoc_name())) {
            msgHolder.title.setText(comment.getDoc_name());
        } else {
            msgHolder.title.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(comment.getComment_contents())) {

            String commentContent = comment.getComment_contents();
            String[] commentFirst = commentContent.split("//");
            if (commentFirst.length > 0) {
                msgHolder.tvContent.setText(commentFirst[0]);
            } else {
                msgHolder.tvContent.setText(comment.getComment_contents());
            }
        } else {
            msgHolder.tvContent.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(comment.getFaceurl())) {
            msgHolder.userHeader.setImageUrl(comment.getFaceurl(), VolleyHelper.getImageLoader());
            msgHolder.userHeader.setDefaultImageResId(R.drawable.avatar_default);
            msgHolder.userHeader.setErrorImageResId(R.drawable.avatar_default);
        } else {
            msgHolder.userHeader.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(comment.getWeMediaName())) {
            String tmp = "评论了" + comment.getWeMediaName() + "的视频";
            int length = comment.getWeMediaName().length();
            SpannableStringBuilder builder = new SpannableStringBuilder(tmp);
            ForegroundColorSpan span = new ForegroundColorSpan(Color.RED);
            builder.setSpan(span, 3, 3 + length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            msgHolder.tvWemedia.setText(builder);
            msgHolder.tvWemedia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (TextUtils.isEmpty(comment.getWeMediaId()) || TextUtils.isEmpty(comment.getWeMediaName())) {
                        return;
                    }
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA, PageIdConstants.PLAY_VIDEO_V);
                    IntentUtils.startWeMediaHomePageActivity(view.getContext(), comment.getWeMediaId(), "");
                }
            });
        }

        if (TextUtils.isEmpty(comment.getCover())) {
            msgHolder.videoCover.setImageUrl(comment.getCover(), VolleyHelper.getImageLoader());
            msgHolder.videoCover.setDefaultImageResId(R.drawable.bg_msg_topic_small);
            msgHolder.videoCover.setErrorImageResId(R.drawable.bg_msg_topic_small);
        } else {
            msgHolder.videoCover.setImageUrl(comment.getCover(), VolleyHelper.getImageLoader());
            msgHolder.videoCover.setDefaultImageResId(R.drawable.bg_default_small);
            msgHolder.videoCover.setErrorImageResId(R.drawable.bg_default_small);
        }

        if (comment.getDuration() != 0) {
            msgHolder.tvDuration.setText(StringUtils.changeDuration(comment.getDuration()));
        } else {
            msgHolder.tvDuration.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(comment.getPlaytime())) {
            msgHolder.tvPlayTimes.setText(StringUtils.changePlayTimes(comment.getPlaytime()));
        } else {
            msgHolder.tvPlayTimes.setVisibility(View.VISIBLE);
        }
//        if (mIsShowBottomLine) {
//            msgHolder.divider.setVisibility(View.VISIBLE);
//        } else {
//            msgHolder.divider.setVisibility(View.GONE);
//        }
        return view;
    }

    public void setShowBottomLine(boolean showBottomLine) {
        mIsShowBottomLine = showBottomLine;
    }

    public void setSize(int size) {
        this.mSize = size;
    }

    static class MsgHolder {
        View itemView;
        TextView tvWemedia;
        TextView tvTimer;
        TextView tvContent;
        TextView title;
        TextView tvDuration;
        TextView tvPlayTimes;
        NetworkImageView userHeader;
        NetworkImageView videoCover;
        View divider;

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvWemedia = (TextView) view.findViewById(R.id.tv_wemedia);
            tvTimer = (TextView) view.findViewById(R.id.tv_comment_timer);
            tvContent = (TextView) view.findViewById(R.id.tv_comment_content);
            title = (TextView) view.findViewById(R.id.tv_left_title);
            tvDuration = (TextView) view.findViewById(R.id.tv_duration);
            tvPlayTimes = (TextView) view.findViewById(R.id.tv_play_times);
            userHeader = (NetworkImageView) view.findViewById(R.id.iv_header);
            videoCover = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
//            divider = view.findViewById(R.id.divider);

        }
    }

    public String[] topics = new String[]{"cmpptopic", "lianbo", "focus", "special"};


    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (ListUtils.isEmpty(mList)) {
                return;
            }
            CommentItem item = mList.get(position);
            String guid = item.getDoc_url();
            String cover = item.getCover();
            boolean isTopic = guid.contains("http");
            if (!isTopic) {
                if ("error".equals(cover)) {
                    ToastUtils.getInstance().showShortToast("抱歉，该视频已下线。");
                } else {
                    IntentUtils.toVodDetailActivity(view.getContext(), guid, "", false, false, 0, "");
                }
            } else {
                String topicId = null;
                String itemType = null;
                String tmp = getTopicId(guid);
                if (!TextUtils.isEmpty(tmp)) {
                    int firstHen = tmp.indexOf("/");
                    itemType = tmp.substring(0, firstHen);
                    if (topics[3].equals(itemType)) {
                        itemType = topics[0];
                    }
                    topicId = tmp.substring(firstHen + 1, tmp.length());
                }
                if (!TextUtils.isEmpty(topicId) && !TextUtils.isEmpty(itemType)) {
                    IntentUtils.toTopicDetailActivity(view.getContext(), "", topicId, "", itemType, false, 0, "");
                }

            }
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_MSG_ITEM, PageIdConstants.MY_MESSAGE_REPLY);
        }
    }

    public String getTopicId(String guid) {
        String topicStr = null;
        int length = topics.length;
        for (int i = 0; i < length; i++) {
            if (guid.contains(topics[i])) {
                int first = guid.lastIndexOf(topics[i]);
                int last = guid.lastIndexOf("/");
                topicStr = guid.substring(first, last);
                break;
            }
        }
        return topicStr;
    }

    private boolean isTopic(String url) {
        if (!url.contains("-") && url.contains("http") && (url.contains("cmpptopic")
                || url.contains("lianbo")
                || url.contains("focus")
                || url.contains("special"))) {
            return true;
        }
        return false;
    }
}
