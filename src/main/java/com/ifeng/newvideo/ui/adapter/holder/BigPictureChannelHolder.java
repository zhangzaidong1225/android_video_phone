package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by guolc on 2016/7/20.
 */
public class BigPictureChannelHolder {
    public TextView title;
    public TextView tv_comment;
    public TextView duration;
    public TextView playTimes;
    public TextView userName;
    public ImageView playStatus;
    public ImageView toShare;
    public NetworkImageView networkImageView;
    public NetworkImageView small_head_view;
    public View bottom;
    public ImageView mask;
    public RelativeLayout videoContainer;
}
