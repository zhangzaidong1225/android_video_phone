package com.ifeng.newvideo.ui.adapter;


import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.model.CommentsModel;

import java.util.ArrayList;
import java.util.List;

public class MsgReplyListAdapter extends BaseAdapter {

    public static final int SIZE_THREE = 3;//展示三个
    public static final int SIZE_MAX = Integer.MAX_VALUE;//不限制
    private int mSize = SIZE_MAX;
    private boolean mIsShowBottomLine = true;//是否显示底部横线

    private List<CommentsModel.Comment> mList = new ArrayList<>();

    @Override
    public int getCount() {
        int tmpSize = mList.size();
        return tmpSize >= mSize ? mSize : tmpSize;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setData(List<CommentsModel.Comment> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MsgHolder msgHolder = null;
        OnClick onClick = null;
        if (view == null) {
            msgHolder = new MsgHolder();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_listview_msg_layout, viewGroup, false);
            msgHolder.initView(view);
            view.setTag(R.id.view_parent, onClick);
            view.setTag(msgHolder);
        } else {
            msgHolder = (MsgHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        msgHolder.itemView.setOnClickListener(onClick);
        CommentsModel.Comment comment = mList.get(i);
        ;

        msgHolder.tvUserName.setText(comment.getUname());
        msgHolder.tvTimer.setText(DateUtils.getCommentTime(comment.getComment_date()).replaceAll("-", "."));

        String commentContent = comment.getComment_contents();
        String[] commentFirst = commentContent.split("//");
        if (commentFirst.length > 0) {
            msgHolder.tvContent.setText(commentFirst[0]);
        } else {
            msgHolder.tvContent.setText(comment.getComment_contents());
        }

        String firstContent = comment.getParent().get(0).getComment_contents();
        String[] contentArray = firstContent.split("//");
        if (contentArray.length > 0) {
            int length = contentArray[0].getBytes().length;
            Log.d("ByteLength:", contentArray[0] + "--" + length);
            if (length > 15) {
                msgHolder.tvDesc.setGravity(Gravity.LEFT);
            } else {
                msgHolder.tvDesc.setGravity(Gravity.RIGHT);
            }
            msgHolder.tvDesc.setText(contentArray[0]);
        } else {
            int length = comment.getParent().get(0).getComment_contents().getBytes().length;
            Log.d("ByteLength:", comment.getParent().get(0).getComment_contents() + "--" + length);
            if (length > 15) {
                msgHolder.tvDesc.setGravity(Gravity.LEFT);
            } else {
                msgHolder.tvDesc.setGravity(Gravity.RIGHT);
            }
            msgHolder.tvDesc.setText(comment.getParent().get(0).getComment_contents());

        }

        if (comment.getFaceurl() != null) {
            msgHolder.userHeader.setImageUrl(comment.getFaceurl(), VolleyHelper.getImageLoader());
            msgHolder.userHeader.setDefaultImageResId(R.drawable.avatar_default);
            msgHolder.userHeader.setErrorImageResId(R.drawable.avatar_default);
        }
//        if (mIsShowBottomLine) {
//            msgHolder.divider.setVisibility(View.VISIBLE);
//        } else {
//            msgHolder.divider.setVisibility(View.GONE);
//        }
        return view;
    }

    public void setShowBottomLine(boolean showBottomLine) {
        mIsShowBottomLine = showBottomLine;
    }

    public void setSize(int size) {
        this.mSize = size;
    }

    static class MsgHolder {
        View itemView;
        TextView tvUserName;
        TextView tvTimer;
        TextView tvContent;
        TextView tvDesc;
        NetworkImageView userHeader;
        View divider;

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvUserName = (TextView) view.findViewById(R.id.tv_user_name);
            tvTimer = (TextView) view.findViewById(R.id.tv_user_time);
            tvContent = (TextView) view.findViewById(R.id.tv_msg_content);
            tvDesc = (TextView) view.findViewById(R.id.tv_msg_desc);
            userHeader = (NetworkImageView) view.findViewById(R.id.iv_header);
//            divider = view.findViewById(R.id.divider);

        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (ListUtils.isEmpty(mList)) return;
            String guid = mList.get(position).getDoc_url();
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_MSG_ITEM, PageIdConstants.MY_MESSAGE_REPLY);
            IntentUtils.toVodDetailActivity(view.getContext(), guid, "", false, false, 0, "");
        }
    }
}
