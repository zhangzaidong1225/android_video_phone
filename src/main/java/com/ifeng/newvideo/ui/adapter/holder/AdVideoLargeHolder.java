package com.ifeng.newvideo.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;

/**
 * Created by android-dev on 2016/10/21.
 */
public class AdVideoLargeHolder {
    public TextView title;
    public TextView tag;
    public TextView des;
    public NetworkImageView pic;

    public static AdVideoLargeHolder getHolder(View convertView){
        AdVideoLargeHolder holder = null;
        if (convertView.getTag() == null) {
            holder = new AdVideoLargeHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_ad_video_large_title);
            holder.des= (TextView) convertView.findViewById(R.id.tv_ad_video_large_des);
            holder.tag=(TextView) convertView.findViewById(R.id.tv_ad_video_large_tag);
            holder.pic= (NetworkImageView) convertView.findViewById(R.id.niv_ad_video_large_picture);
            convertView.setTag(holder);
        } else {
            holder = (AdVideoLargeHolder) convertView.getTag();
        }
        return holder;
    }
}
