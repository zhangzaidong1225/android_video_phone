package com.ifeng.newvideo.ui;

import android.view.MotionEvent;

/**
 * Created by guolc on 2016/8/29.
 */
public interface WrapperOnGestureListener {
    void onTouch(MotionEvent event);
}
