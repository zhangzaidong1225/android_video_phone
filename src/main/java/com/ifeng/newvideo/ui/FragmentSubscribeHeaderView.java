package com.ifeng.newvideo.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.adapter.SubscribeContentListAdapter;
import com.ifeng.newvideo.ui.subscribe.AllWeMediaActivity;
import com.ifeng.newvideo.ui.subscribe.adapter.SubscribeListAdapter;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.newvideo.widget.ListViewForScrollView;
import com.ifeng.newvideo.widget.UnScrollGridView;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeList;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 订阅更新
 * 订阅ListView的HeaderView
 * Created by Administrator on 2016/8/14.
 */
public class FragmentSubscribeHeaderView extends RelativeLayout implements View.OnClickListener, SubscribeListAdapter.ClickListener {
    private static final Logger logger = LoggerFactory.getLogger(FragmentSubscribeHeaderView.class);

    public static final int NO_LOGIN = 0;//未登录
    public static final int NO_SUBSCRIBE = 1;//没有订阅内容
    public static final int NORMAL = 2;//展示订阅内容
    public static final int LOADING = 3;//加载中
    public static final int ERROR = 4;//加载失败错误

    private Context context;
    private View contentLoading;
    private View contentError;
    private TextView tvNoLogin;//提示信息：未登录
    private View noSubscribe;//提示信息：未订阅
    private View allSubscribe;//查看全部订阅
    private View view_separate;//高24px的分隔线
    private ListViewForScrollView listView;//订阅内容ListView
    private SubscribeContentListAdapter contentAdapter;//订阅内容适配器

    private RelativeLayout rl_ifeng_recommend_bar;
    private RelativeLayout rl_ifeng_recommend_content;
    private RelativeLayout rl_recommend_bar;
    private RelativeLayout rl_mine_subscribe;
    private RelativeLayout rl_mine_subscribe_content;


    private View ifengTVLoading;
    private View ifengTVError;
    private UnScrollGridView gridView;
    private SubscribeListAdapter ifengTVAdapter;//凤凰卫视适配器
    private SubscribeList.WeMediaListEntity entity;//凤凰推荐自媒体实体类

    private boolean isClickSubscribeBtn = false;////是否点击了凤凰卫视Item中订阅按钮

    public FragmentSubscribeHeaderView(Context context) {
        this(context, null);
    }

    public FragmentSubscribeHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FragmentSubscribeHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.fragment_subscribe_header_layout, this, true);
        initView();
        initContentAdapter();
        initIfengTVAdapter();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        registerLoginBroadcast();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        context.unregisterReceiver(mLoginReceiver);
    }

    @Override
    public void onItemClickListener(SubscribeList.WeMediaListEntity entity) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_IFENG_ITEM, PageIdConstants.PAGE_SUB);
        IntentUtils.startWeMediaHomePageActivity(context, entity.getWeMediaID() + "", "");
    }

    @Override
    public void onSubscribeClickListener(SubscribeList.WeMediaListEntity entity) {
        if (!NetUtils.isNetAvailable(context)) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        this.entity = entity;
        if (User.isLogin()) {
            int state = entity.getFollowed();
            state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            subscribe(entity, new User(context).getUid(), state);
            PageActionTracker.clickWeMeidaSub(state == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.PAGE_SUB);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(context);
        }
    }

    private void initView() {
        initSubscribeContentView();
        initIfengTVView();
        initSubscribeRecommendView();
    }

    /**
     * 初始化订阅内容View
     */
    private void initSubscribeContentView() {
        contentLoading = findViewById(R.id.content_loading_layout);
        contentError = findViewById(R.id.content_load_fail_layout);
        contentError.setOnClickListener(this);
        rl_mine_subscribe = (RelativeLayout) findViewById(R.id.rl_mine_subscribe);
        rl_mine_subscribe_content = (RelativeLayout) findViewById(R.id.rl_mine_subscribe_content);
        //订阅内容展示布局
        listView = (ListViewForScrollView) findViewById(R.id.listView);
        //未登录
        tvNoLogin = (TextView) findViewById(R.id.tv_no_login);
        SpannableStringBuilder builder = new SpannableStringBuilder(tvNoLogin.getText().toString());
        ForegroundColorSpan span = new ForegroundColorSpan(Color.RED);
        builder.setSpan(span, 7, 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        UnderlineSpan underSpan = new UnderlineSpan();
        builder.setSpan(underSpan, 7, 9, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvNoLogin.setText(builder);
        tvNoLogin.setOnClickListener(this);
        //未订阅
        noSubscribe = findViewById(R.id.no_subscribe);
        findViewById(R.id.subscribe_wemedia).setOnClickListener(this);
        //查看全部
        allSubscribe = findViewById(R.id.rl_all_subscribe);
        allSubscribe.setOnClickListener(this);
        //分割线
        view_separate = findViewById(R.id.view_separate);
    }

    public void showMineSubscribe() {
        rl_mine_subscribe.setVisibility(View.VISIBLE);
        rl_mine_subscribe_content.setVisibility(View.VISIBLE);
        allSubscribe.setVisibility(View.VISIBLE);
    }

    /**
     * 初始化凤凰卫视View
     */
    private void initIfengTVView() {
        findViewById(R.id.see_all_ifeng_tv).setOnClickListener(this);
        ifengTVLoading = findViewById(R.id.ifeng_loading_layout);
        ifengTVError = findViewById(R.id.ifeng_load_fail_layout);
        ifengTVError.setOnClickListener(this);
        gridView = (UnScrollGridView) findViewById(R.id.gridView);

        rl_ifeng_recommend_bar = (RelativeLayout) findViewById(R.id.rl_ifeng_recommend_bar);
        rl_ifeng_recommend_content = (RelativeLayout) findViewById(R.id.rl_ifeng_recommend_content);
    }

    /**
     * 初始化精选推荐View
     */
    private void initSubscribeRecommendView() {
        findViewById(R.id.see_all_recommend).setOnClickListener(this);
        rl_recommend_bar = (RelativeLayout) findViewById(R.id.rl_recommend_bar);

    }

    /**
     * 初始化订阅内容适配器
     */
    private void initContentAdapter() {
        contentAdapter = new SubscribeContentListAdapter();
        contentAdapter.setSize(SubscribeContentListAdapter.SIZE_THREE);
        contentAdapter.setShowBottomLine(false);
        listView.setAdapter(contentAdapter);
    }

    /**
     * 初始化凤凰卫视适配器
     */
    private void initIfengTVAdapter() {
        ifengTVAdapter = new SubscribeListAdapter();
        ifengTVAdapter.setClickListener(this);
        ifengTVAdapter.setSize(SubscribeListAdapter.SIZE_MAX);//不限制显示数量
        ifengTVAdapter.setShowSubscribeNum(true);//显示订阅数量
        gridView.setAdapter(ifengTVAdapter);
    }

    @Override
    public void onClick(View view) {
        Context context = view.getContext();
        switch (view.getId()) {
            case R.id.tv_no_login://未登录
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_LOGIN, PageIdConstants.PAGE_SUB);
                IntentUtils.startLoginActivity(context);
                break;
            case R.id.rl_all_subscribe://查看全部订阅
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_ALL, PageIdConstants.PAGE_SUB);
                IntentUtils.startAllSubscribeActivity(context);
                break;
            case R.id.see_all_recommend://查看全部精选推荐
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_REC_MORE, PageIdConstants.PAGE_SUB);
                IntentUtils.startAllMediaActivity(context);
                break;
            case R.id.subscribe_wemedia:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_MEDIA, PageIdConstants.PAGE_SUB);
                IntentUtils.startAllMediaActivity(context);
                break;
            case R.id.see_all_ifeng_tv:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SUB_IFENG_ALL, PageIdConstants.PAGE_SUB);
                Intent intent = new Intent(context, AllWeMediaActivity.class);
                intent.putExtra(IntentKey.LOCATE_TO_TV, true);
                context.startActivity(intent);
                break;
            case R.id.content_load_fail_layout://订阅内容加载失败
                if (requestHeaderViewData != null) {
                    requestHeaderViewData.requestContentData();
                }
                break;
            case R.id.ifeng_load_fail_layout://凤凰推荐自媒体加载失败
                if (requestHeaderViewData != null) {
                    requestHeaderViewData.requestIfengTVData();
                }
                break;
        }
    }

    /**
     * 设置订阅内容显示状态
     */
    public void setContentStatus(int status) {
        switch (status) {
            case NO_LOGIN:
                tvNoLogin.setVisibility(VISIBLE);//未登录
                noSubscribe.setVisibility(GONE);//没有订阅内容
                allSubscribe.setVisibility(GONE);//全部订阅
                listView.setVisibility(GONE);//展示订阅内容
                contentLoading.setVisibility(GONE);//Loading
                contentError.setVisibility(GONE);//Error
                view_separate.setVisibility(VISIBLE);//分割线
                break;
            case NO_SUBSCRIBE:
                tvNoLogin.setVisibility(GONE);
                noSubscribe.setVisibility(VISIBLE);
                listView.setVisibility(GONE);//展示订阅内容
                contentLoading.setVisibility(GONE);//Loading
                contentError.setVisibility(GONE);//Error
                allSubscribe.setVisibility(GONE);
                view_separate.setVisibility(VISIBLE);
                break;
            case NORMAL:
                tvNoLogin.setVisibility(GONE);
                noSubscribe.setVisibility(GONE);
                listView.setVisibility(VISIBLE);//展示订阅内容
                contentLoading.setVisibility(GONE);//Loading
                contentError.setVisibility(GONE);//Error
                allSubscribe.setVisibility(isDataCountOver3 ? VISIBLE : GONE);//该View的显示隐藏依赖于订阅内容数量
                view_separate.setVisibility(isDataCountOver3 ? GONE : VISIBLE);//
                break;
            case LOADING:
                tvNoLogin.setVisibility(GONE);
                noSubscribe.setVisibility(GONE);
                listView.setVisibility(GONE);//展示订阅内容
                contentLoading.setVisibility(VISIBLE);//Loading
                contentError.setVisibility(GONE);//Error
                allSubscribe.setVisibility(GONE);
                view_separate.setVisibility(VISIBLE);
                break;
            case ERROR:
                tvNoLogin.setVisibility(GONE);
                noSubscribe.setVisibility(GONE);
                listView.setVisibility(GONE);//展示订阅内容
                contentLoading.setVisibility(GONE);//Loading
                contentError.setVisibility(VISIBLE);//Error
                allSubscribe.setVisibility(GONE);
                view_separate.setVisibility(VISIBLE);
                break;
        }
    }

    /**
     * 设置凤凰推荐自媒体显示状态
     */
    public void setIfengTVStatus(int status) {
        switch (status) {
            case NORMAL:
                gridView.setVisibility(VISIBLE);//展示凤凰推荐自媒体
                ifengTVLoading.setVisibility(GONE);//Loading
                ifengTVError.setVisibility(GONE);//Error
                rl_ifeng_recommend_content.setVisibility(View.VISIBLE);
                rl_ifeng_recommend_bar.setVisibility(View.VISIBLE);
                break;
            case LOADING:
                gridView.setVisibility(GONE);//展示凤凰推荐自媒体
                ifengTVLoading.setVisibility(VISIBLE);//Loading
                ifengTVError.setVisibility(GONE);//Error
                rl_ifeng_recommend_content.setVisibility(View.VISIBLE);
                rl_ifeng_recommend_bar.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                gridView.setVisibility(GONE);//展示凤凰推荐自媒体
                ifengTVLoading.setVisibility(GONE);//Loading
                ifengTVError.setVisibility(VISIBLE);//Error
                rl_ifeng_recommend_content.setVisibility(View.GONE);
                rl_ifeng_recommend_bar.setVisibility(View.GONE);
                break;
        }
    }

    public void setIfengRecommendErrorStatus(int status) {
        switch (status) {
            case NORMAL:
                rl_recommend_bar.setVisibility(View.VISIBLE);
                break;
            case LOADING:
                rl_recommend_bar.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                rl_recommend_bar.setVisibility(View.GONE);

                break;
        }
    }

    /**
     * 订阅数量是否超过三个
     */
    private boolean isDataCountOver3 = true;

    public void setIsDataCountOver3(boolean isDataCountOver3) {
        this.isDataCountOver3 = isDataCountOver3;
    }

    /**
     * 更新订阅内容布局
     *
     * @param subscribeList 订阅内容数据
     */
    public void updateContentView(List<WeMediaInfoList.InfoListEntity> subscribeList) {
        contentAdapter.setList(subscribeList);
    }

    /**
     * 更新凤凰推荐布局
     *
     * @param list 订阅内容数据
     */
    public void updateIfengView(List<SubscribeList.WeMediaListEntity> list) {
        ifengTVAdapter.setList(list);
    }

    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final SubscribeList.WeMediaListEntity entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(entity.getWeMediaID(), uid, type, System.currentTimeMillis() + "", SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        int followNo = 0;
                        if (TextUtils.isDigitsOnly(entity.getFollowNo())) {
                            followNo = Integer.parseInt(entity.getFollowNo());
                        }
                        boolean isSubscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        entity.setFollowed(type);
                        entity.setFollowNo(String.valueOf(isSubscribe ? (++followNo) : (--followNo)));
                        ifengTVAdapter.notifyDataSetChanged();
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, isSubscribe, entity.getName());
                        if (onSubscribeSuccess != null) {
                            onSubscribeSuccess.onSubscribeSuccess();
                        }
                        ToastUtils.getInstance().showShortToast(isSubscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    private void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(getContext(), action, title, "");
    }

    /**
     * 注册登录广播
     */
    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        context.registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING && isClickSubscribeBtn && entity != null) {
                    subscribe(entity, new User(context).getUid(), SubscribeRelationModel.SUBSCRIBE);
                    isClickSubscribeBtn = false;
                } else {
                    isClickSubscribeBtn = false;
                }
            }
        }
    };

    public boolean isClickSubscribeBtn() {
        return isClickSubscribeBtn;
    }

    private RequestHeaderViewData requestHeaderViewData;

    public interface RequestHeaderViewData {
        /**
         * 获取订阅内容
         */
        void requestContentData();

        /**
         * 获取凤凰推荐自媒体内容
         */
        void requestIfengTVData();
    }

    public void setRequestHeaderViewData(RequestHeaderViewData requestHeaderViewData) {
        this.requestHeaderViewData = requestHeaderViewData;
    }

    /**
     * 订阅自媒体成功回调接口
     */
    public interface OnSubscribeSuccess {
        void onSubscribeSuccess();
    }

    private OnSubscribeSuccess onSubscribeSuccess;

    public void setOnSubscribeSuccess(OnSubscribeSuccess onSubscribeSuccess) {
        this.onSubscribeSuccess = onSubscribeSuccess;
    }
}
