package com.ifeng.newvideo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.ui.adapter.ListAdapterFMList;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.FMDao;
import com.ifeng.video.dao.db.model.FMMoreInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 电台更多界面
 */
public class ActivityFmMore extends BaseFragmentActivity implements RequestData, AdapterView.OnItemClickListener {

    private static final Logger logger = LoggerFactory.getLogger(ActivityFmMore.class);
    private Intent mIntent;
    private String nodeId;
    private String nodeName;
    private UIStatusLayout uiStatusLayout;
    private PullToRefreshListView mPullToRefreshListView;
    private ListAdapterFMList fmListAdapter;
    private int PAGE_NUM = 1;
    private FMMoreInfoModel mFmMoreInfoModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fmmore_activity);
        mIntent = getIntent();
        nodeId = mIntent.getStringExtra("id");
        nodeName = mIntent.getStringExtra("name");
        initView();
        initListener();
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void initView() {
        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.FM_MORE);
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(nodeName);
        uiStatusLayout = (UIStatusLayout) findViewById(R.id.ui_status_layout);
        uiStatusLayout.setRequest(this);
        mPullToRefreshListView = uiStatusLayout.getListView();
        mPullToRefreshListView.setShowIndicator(false);
        uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.BOTH);
        fmListAdapter = new ListAdapterFMList(this);
        mPullToRefreshListView.setAdapter(fmListAdapter);
    }

    private void initListener() {
        mPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                requestData(true);
                PageActionTracker.clickBtn(ActionIdConstants.PULL_REFRESH, PageIdConstants.FM_MORE);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                requestData(false);
                PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.FM_MORE);
            }
        });

        mPullToRefreshListView.setOnItemClickListener(this);

        uiStatusLayout.setStatus(UIStatusLayout.LOADING);
        requestData(true);
    }

    @Override
    public void requestData() {
        requestData(true);
    }

    /**
     * @param isFirst
     */
    private void requestData(final boolean isFirst) {
        if (isFirst) {
            PAGE_NUM = 1;
            if (fmListAdapter != null && fmListAdapter.getCount() == 0) {
                uiStatusLayout.setStatus(UIStatusLayout.LOADING);
            }
        } else {
            PAGE_NUM++;
        }
        String params = "?cid=" + nodeId + "&pagenum=" + PAGE_NUM + "&listtype=1";
        FMDao.getFMMoreInfoModel(params, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                refreshUI(response, isFirst);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mPullToRefreshListView.onRefreshComplete();
                if (isFirst) {
                    if (!NetUtils.isNetAvailable(ActivityFmMore.this)) {
                        uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
                    } else {
                        uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                    }
                } else {
                    if (!NetUtils.isNetAvailable(ActivityFmMore.this)) {
                        ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    }
                    PAGE_NUM--;
                }
            }
        });

    }

    /**
     * 刷新数据
     *
     * @param response
     * @param isFirst
     */
    private void refreshUI(Object response, boolean isFirst) {
        if (response == null) {
            return;
        }
        FMMoreInfoModel newFMMoreInfoModel = (FMMoreInfoModel) response;
        mPullToRefreshListView.onRefreshComplete();

        if (isFirst) {
            if (newFMMoreInfoModel.getData() == null || newFMMoreInfoModel.equals(mFmMoreInfoModel)) {
                logger.debug("in activityFmList first data is null or equals ,so return");
                if (!NetUtils.isNetAvailable(ActivityFmMore.this)) {
                    uiStatusLayout.setStatus(UIStatusLayout.NO_NET);
                } else {
                    uiStatusLayout.setStatus(UIStatusLayout.ERROR);
                }
                return;
            }
            mFmMoreInfoModel = newFMMoreInfoModel;
            setAdapterData();
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.BOTH);
        } else {
            if (newFMMoreInfoModel.getData() == null || newFMMoreInfoModel.equals(mFmMoreInfoModel)) {
                logger.debug("in activityFmList more data is null or equals ,so return");
                return;
            }
            List<FMMoreInfoModel.ReList> newReList = mFmMoreInfoModel.getData().getReList();
            List<FMMoreInfoModel.ReList> reList = newFMMoreInfoModel.getData().getReList();
            if (reList != null && reList.size() > 0) {
                uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.BOTH);
            } else {
                uiStatusLayout.setPullToRefreshMode(PullToRefreshBase.Mode.PULL_FROM_START);
                ToastUtils.getInstance().showShortToast("没有更多了");
                return;
            }
            newReList.addAll(newFMMoreInfoModel.getData().getReList());
            mFmMoreInfoModel.getData().setReList(newReList);
            setAdapterData();
        }
    }

    private void setAdapterData() {
        if (mFmMoreInfoModel != null && mFmMoreInfoModel.getData() != null) {
            fmListAdapter.setData(mFmMoreInfoModel.getData().getReList());
            fmListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        IntentUtils.toAudioFMActivity(this, mFmMoreInfoModel.getData().getReList().get(position - 1).getResourceId(),
                mFmMoreInfoModel.getData().getReList().get(position - 1).getId(), nodeName, VodRecord.V_TAG_FM_MORE, "");
    }


}
