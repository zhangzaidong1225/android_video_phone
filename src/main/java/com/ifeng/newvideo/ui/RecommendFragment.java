package com.ifeng.newvideo.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.PageRefreshConstants;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.adapter.RecommendFragmentAdapter;
import com.ifeng.newvideo.ui.basic.Status;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.LastDocUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.BigPicAdView;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayAdButton;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayButton;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.model.ChannelBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class RecommendFragment extends ChannelBaseFragment implements AbsListView.OnScrollListener, IPlayController.OnPlayCompleteListener,
        VideoSkin.OnNetWorkChangeListener, VideoSkin.OnLoadFailedListener, VideoSkin.onClickVideoSkin,
        PlayAdButton.onPlayAndPreplayClickListener, PlayButton.OnPlayOrPauseListener, BigPicAdView.OnClickAdView {

    /**
     * 离开此页面判读是否刷新的阈值
     */
    private String requireTime = "";

    private int mUpTimes = 0;
    //    private View mSearchView;
//    private View mHeadView;
//    private HeadFlowView mHeadFlowView;
    private static final String TAG = RecommendFragment.class.getName();

    private ViewGroup mLandScapeContainer;
    private VideoSkin mVideoSkin;
    private NormalVideoHelper mVideoHelper;
    private UIPlayContext mUIPlayerContext;
    private FrameLayout mVideoViewWrapper;
    private ActivityMainTab mActivity;
    private boolean isVisible;
    private Resources mResource;
    private int mHeadViewCount;
    private boolean needRecover;
    private boolean hasRegisterReceiver;
    private boolean mHidden;
    private RecommendFragmentAdapter picAdapter;

    private Comparator<ChannelBean.HomePageBean> comparator = new Comparator<ChannelBean.HomePageBean>() {
        @Override
        public int compare(ChannelBean.HomePageBean lhs, ChannelBean.HomePageBean rhs) {
            if (lhs != null && rhs != null) {
                String update_1 = lhs.getUpdateDate();
                String update_2 = rhs.getUpdateDate();
                if (!TextUtils.isEmpty(update_1) && !TextUtils.isEmpty(update_2)) {
                    return -update_1.compareTo(update_2);
                }
            }
            return 0;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActivityMainTab) activity;
        mResource = mActivity.getResources();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requireTime=SharePreUtils.getInstance(mActivity).getChannelDataLastTime(mChannel_id);
        mVideoViewWrapper = new FrameLayout(mActivity);
        initVideoSkin();
    }

    private void initVideoSkin() {
        mUIPlayerContext = new UIPlayContext();
        mUIPlayerContext.skinType = VideoSkin.SKIN_TYPE_PIC;
        mUIPlayerContext.channelId = mChannel_id;
        mVideoSkin = new VideoSkin(mActivity);
        mVideoSkin.setId(R.id.video_skin);
        mVideoSkin.setVideoMargin(true);
        mVideoSkin.setOnLoadFailedListener(this);
        mVideoSkin.setNoNetWorkListener(this);
        mVideoSkin.setOnClickVideoSkin(this);
        mVideoSkin.setClipChildren(false);
        mVideoSkin.setClipToPadding(false);
        mVideoSkin.setOnClickVideoSkin(this);
        mVideoSkin.setClipToPadding(false);
        mVideoSkin.setClipChildren(false);
        mVideoViewWrapper.addView(mVideoSkin);
        mVideoHelper = new NormalVideoHelper();
        mVideoHelper.init(mVideoSkin, mUIPlayerContext);
        mVideoHelper.setOnPlayCompleteListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_recommend_layout, container, false);
//        mSearchView = inflater.inflate(R.layout.well_chosen_search_layout, null);
//        mHeadView = inflater.inflate(R.layout.well_chosen_header_banner,null);
//        mHeadView.setVisibility(View.VISIBLE);
//        mHeadFlowView = (HeadFlowView) mHeadView.findViewById(R.id.headFlowView);
        mFocusList = new ArrayList<>(20);
        initView(contentView);
        initAdapterAndListener();
        isVisible = true;
        mHidden = false;
        setVideoSkinListener();
        return contentView;
    }

    private void setVideoSkinListener() {
        if (null != mVideoSkin.getPlayAdView()) {
            mVideoSkin.getPlayAdView().setOnPlayAndPrePlayClickListener(this);
        }
        if (null != mVideoSkin.getPlayView()) {
            mVideoSkin.getPlayView().setPlayOrPauseListener(this);
        }
        if (null != mVideoSkin.getBigPicAdView()) {
            mVideoSkin.getBigPicAdView().setOnClickAdView(this);
        }
    }

    private void initView(View contentView) {
        mPullToRefreshListView = (MyPullToRefreshListView) contentView.findViewById(R.id.page_listView);
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mPullToRefreshListView.setShowIndicator(false);
//        mPullToRefreshListView.getRefreshableView().addHeaderView(mHeadView);
        mHeadViewCount = mPullToRefreshListView.getRefreshableView().getHeaderViewsCount();

        mLandScapeContainer = (FrameLayout) contentView.findViewById(R.id.rl_landscape_container);
        mLandScapeContainer.setVisibility(View.GONE);
    }

    private void initAdapterAndListener() {
        mAdapter = new RecommendFragmentAdapter(13, mActivity, mChannel_id);
        picAdapter = (RecommendFragmentAdapter) mAdapter;
        picAdapter.setVideoHelper(mVideoHelper);
        picAdapter.setVideoSkinWrapper(mVideoViewWrapper);
        picAdapter.setUIPlayContext(mUIPlayerContext);
        mPullToRefreshListView.setAdapter(mAdapter);
        mPullToRefreshListView.getRefreshableView().setOnItemClickListener(this);
        mPullToRefreshListView.setOnRefreshListener(this);
        mPullToRefreshListView.setOnLastItemVisibleListener(this);
        mPullToRefreshListView.setOnScrollListener(this);

//        mHeaderViewPagerAdapter = new HeaderViewPagerAdapter(this,mActivity,mChannel_id);
//        mHeadFlowView.setViewPagerAdapter(mHeaderViewPagerAdapter);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestNet();
    }

    @Override
    protected void requestNet() {
        updateViewStatus(Status.LOADING);
        mUpTimes = 0;
        requestNet("", DataInterface.PAGESIZE_20, Status.FIRST, ChannelDao.REFRESH, ChannelConstants.DEFAULT, "");
    }

    private void requestNet(String position_id_or_requre_time, final String count,
                            final Status status, int action, String operation, String upTimes) {
        final boolean netAvailable = NetUtils.isNetAvailable(IfengApplication.getInstance());
        if (!netAvailable) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
        if (isLoading) {
            mPullToRefreshListView.onRefreshComplete();
            mPullToRefreshListView.hideFootView();
            return;
        }
        isLoading = true;
        VolleyHelper.getRequestQueue().cancelAll(TAG);
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String nw = NetUtils.getNetType(getActivity());

        ChannelDao.requestChannelData(mChannel_id, mChannel_Type, count, position_id_or_requre_time, 0, ChannelBean.class,
                true, null, action, SharePreUtils.getInstance().getInreview(), operation, new User(IfengApplication.getAppContext()).getUid(), PhoneConfig.userKey,
                LastDocUtils.getLastDoc(), upTimes, province, city, nw, PhoneConfig.publishid,
                new Response.Listener<ChannelBean>() {
                    @Override
                    public void onResponse(ChannelBean response) {
                        isLoading = false;
                        if (response != null) {
                            if (Status.FIRST == status) {
                                List<ChannelBean.HomePageBean> returnList = filterList(response.getBodyList(), null, status, false);
                                requireTime = response.getSystemTime();
                                logger.debug("first requireTime:{}", requireTime);
                                if (!ListUtils.isEmpty(returnList)) {
                                    mAdapter.setData(returnList);
                                    freshStatus(true);
                                }
                            } else if (Status.REFRESH == status) {
                                List<ChannelBean.HomePageBean> returnList = filterList(response.getBodyList(), mAdapter.getDataList(), status, false);
                                mPullToRefreshListView.onRefreshComplete();
                                requireTime = response.getSystemTime();
                                logger.debug("refresh requireTime:{}", requireTime);
                                if (netAvailable) {
                                    if (!ListUtils.isEmpty(returnList)) {
                                        Collections.sort(returnList, comparator);
                                        mAdapter.addData(returnList, true);
                                    } else {
                                        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                                    }
                                    freshStatus(true);
                                }
                            } else {
                                List<ChannelBean.HomePageBean> returnList = filterList(response.getBodyList(), mAdapter.getDataList(), status, false);
                                if (netAvailable && ListUtils.isEmpty(returnList)) {
                                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                                    mPullToRefreshListView.hideFootView();
                                    return;
                                }
                                mAdapter.addData(returnList, false);
                                freshStatus(false);
                            }
                            updateViewStatus(Status.REQUEST_NET_SUCCESS);
                        } else {
                            logger.debug("volley response null");
                            onDataEmpty(status, netAvailable);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("volley onErrorResponse ! {}", error);
                        handleRequestError(status, error);
                        isLoading = false;
                        if (Status.REFRESH == status) {
                            mPullToRefreshListView.getRefreshableView().setSelection(2);
                        } else if (Status.LOAD_MORE == status) {
                            if (netAvailable) {
                                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                            }
                        }
                    }
                });

        PageActionTracker.pullCh(status == Status.LOAD_MORE, mChannel_id);
    }

    private void onDataEmpty(Status status, boolean net) {
        if (Status.FIRST == status) {
            updateViewStatus(Status.DATA_ERROR);
        } else if (Status.LOAD_MORE == status) {
            mPullToRefreshListView.hideFootView();
            if (net) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        } else if (net && status == Status.REFRESH) {
            freshStatus(true);
        }
    }


    private void freshStatus(boolean showSearchBar) {
        mAdapter.notifyDataSetChanged();
        mPullToRefreshListView.hideFootView();
        mPullToRefreshListView.onRefreshComplete();
//        if (showSearchBar) {
//            mPullToRefreshListView.getRefreshableView().setSelection(1);
//        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        logger.debug(mChannel_id + "--onHiddenChanged:{}", hidden);
        mHidden = hidden;
        if (hidden) {
            if (mVideoHelper != null && hasRegisterReceiver) {
                hasRegisterReceiver = false;
                mVideoHelper.onPause();
            }
            recoverUI();
        } else {
            if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
                hasRegisterReceiver = true;
                mVideoHelper.onResume();
            }
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!isVisible || mHidden) {
            logger.debug(mChannel_id + "--onConfigurationChanged isVisible:{}--mHidden:{}", isVisible, mHidden);
            return;
        }

        mVideoHelper.onConfigureChange(newConfig);
        //用流的标记模拟不发送V的标记
        mUIPlayerContext.isBigReportVideo = true;
        initAdViewPosition();

        if (Configuration.ORIENTATION_LANDSCAPE == mResource.getConfiguration().orientation) {
            doOrientationLandscape();
            PageActionTracker.endPageHomeCh(mChannel_id);
        } else if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            //切竖屏的时候，报横屏播放器page的统计
            PageActionTracker.endPageVideoPlay(true, "", "");
            doOrientationPortrait();
        }
        //用流的标记模拟不发送V的标记
        mUIPlayerContext.isBigReportVideo = false;
    }

    private void initAdViewPosition() {
        if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
            ValueAnimator mAnimator = null;
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(30.0f), DisplayUtils.convertDipToPixel(0));
            mAnimator.setDuration(100);
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        if (mVideoSkin.getBigPicAdView().getTranslationY() != 0) {
                            mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                            mVideoSkin.getBigPicAdView().invalidate();
                        }
                    }
                }
            });
            mAnimator.setRepeatCount(0);
            mAnimator.start();
        }
    }

    private void doOrientationPortrait() {
        mPullToRefreshListView.setVisibility(View.VISIBLE);
        if (mLandScapeContainer != null) {
            mLandScapeContainer.setVisibility(View.GONE);
        }

        mActivity.setTabVisible(View.VISIBLE);
        setFullscreen(false);
        if (needRecover) {
            needRecover = false;
            recoverUI();
        } else {
            picAdapter.back2PortraitAndContinuePlay();
        }
        setViewPagerScanScroll(true);

    }


    private void doOrientationLandscape() {

        if (mVideoViewWrapper.getParent() != null) {
            ViewGroup parent = (ViewGroup) mVideoViewWrapper.getParent();
            parent.removeView(mVideoViewWrapper);
        }
        mLandScapeContainer.addView(mVideoViewWrapper);

        mPullToRefreshListView.setVisibility(View.GONE);
        mActivity.setTabVisible(View.GONE);
        mLandScapeContainer.setVisibility(View.VISIBLE);
        setFullscreen(true);
        setViewPagerScanScroll(false);
        if (picAdapter.isPlaying) {
            picAdapter.continuePlay();
        }
    }

    private void setViewPagerScanScroll(boolean scroll) {
        mActivity.getFragmentHomePage().getViewPager().setScanScroll(scroll);
    }


    private void setFullscreen(boolean on) {
        Window win = getActivity().getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object obj = parent.getAdapter().getItem(position);
        if (obj == null) {
            return;
        }
        if (obj instanceof String) {
            mPullToRefreshListView.getRefreshableView().smoothScrollToPosition(0);
            requestNet(requireTime, DataInterface.PAGESIZE_6, Status.REFRESH, ChannelDao.REFRESH, ChannelConstants.DOWN, String.valueOf(++mUpTimes));
        } else if (obj instanceof ChannelBean.HomePageBean) {
            ChannelBean.HomePageBean bean = (ChannelBean.HomePageBean) obj;
            bean.setWatched(true);
            String simId = bean.getMemberItem() == null ? "" : bean.getMemberItem().getSimId();
            String rToken = bean.getMemberItem() == null ? "" : bean.getMemberItem().getrToken();
            PageActionTracker.clickHomeItem(String.valueOf(position), simId, rToken);

            CommonStatictisListUtils.getInstance().sendHomeFeedYoukuConstatic(bean, CommonStatictisListUtils.YK_NEXT);

            dispatchClickEvent(bean);


        }
    }

    @Override
    public void onRefresh(PullToRefreshBase refreshView) {
        super.onRefresh(refreshView);
        requestNet(requireTime, DataInterface.PAGESIZE_6, Status.REFRESH, ChannelDao.REFRESH, ChannelConstants.DOWN, String.valueOf(++mUpTimes));
    }

    @Override
    public void onLastItemVisible() {
        super.onLastItemVisible();
        logger.debug("onMyLastItemVisible");
        ChannelBean.HomePageBean bean = mAdapter.getLastItem();
        if (bean != null && !TextUtils.isEmpty(bean.getItemId())) {
            requestNet(bean.getItemId(), DataInterface.PAGESIZE_20, Status.LOAD_MORE, ChannelDao.LOAD_MORE, ChannelConstants.UP, "");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVideoHelper != null) {
            mVideoHelper.onDestroy();
        }
        IfengApplication.getInstance().setWellChosenTimestamp(0);
        sendStaticList();
    }

    private void sendStaticList() {
        mFocusList = CommonStatictisListUtils.wellChosenFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(mFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.wellChosenFocusList.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mVideoHelper != null && hasRegisterReceiver) {
            hasRegisterReceiver = false;
            mVideoHelper.onPause();
        }
        if (mActivity.isLandScape()) {
            needRecover = true;
        } else {
            recoverUI();
        }
        IfengApplication.getInstance().setWellChosenTimestamp(System.currentTimeMillis());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        SharePreUtils.getInstance().getVisibleMap().put(mChannel_id, isVisible);
        if (isVisibleToUser) {
            if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
                hasRegisterReceiver = true;
                mVideoHelper.onResume();
            }
            requestNetIfNeed();
        } else {
            if (mVideoHelper != null && hasRegisterReceiver) {
                hasRegisterReceiver = false;
                mVideoHelper.onPause();
            }
            recoverUI();
            IfengApplication.getInstance().setWellChosenTimestamp(System.currentTimeMillis());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible && !mHidden && mVideoHelper != null && !hasRegisterReceiver) {
            hasRegisterReceiver = true;
            mVideoHelper.onResume();
        }
        needRecover = false;
        requestNetIfNeed();
    }

    public void requestNetIfNeed() {
        final long lastTimestamp = IfengApplication.getInstance().getWellChosenTimestamp();
        IfengApplication.getInstance().setWellChosenTimestamp(0);
        if (!isLoading && lastTimestamp != 0 && System.currentTimeMillis() - lastTimestamp > PageRefreshConstants.HOMEPAGE_REFRESH_DATA_INTERVAL) {
            logger.debug("currentTimeMillis()--" + System.currentTimeMillis() + "lastTimestamp:" + lastTimestamp);
            requestNet();
        }
    }

    @Override
    public void onPlayComplete() {
        if (mActivity.isLandScape()) {
            needRecover = true;
//            mActivity.toPortrait();
        }
        mVideoSkin.showControllerView();
        if (picAdapter.adVideoRecord != null) {
            picAdapter.adVideoRecord.setCompleteNum();
            picAdapter.adVideoRecord.stopPlayTime();
        }
    }

    public void recoverUI() {
        if (picAdapter != null) {
            if (mUIPlayerContext.videoType != null) {
                if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                    initAdViewPosition();
                }
            }
            picAdapter.recoverUI();
        }


    }

    @Override
    public void onNoNetWorkClick() {
        picAdapter.continuePlay();
    }

    @Override
    public void onMobileClick() {
        picAdapter.continuePlay();
    }

    @Override
    public void onLoadFailedListener() {
        picAdapter.continuePlay();
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
    }

    private boolean isRecovered = false;

    @Override
    public void onScroll(final AbsListView absListView, int firstVisibleItem, int visibleItemCount, int i2) {

        Log.d("onScroll", picAdapter.getClickToPlayPositon() + "--" + firstVisibleItem + "--" + visibleItemCount + "--" + mHeadViewCount);

        if (picAdapter.getClickToPlayPositon() > -1 &&
                (firstVisibleItem + visibleItemCount - mHeadViewCount == picAdapter.getClickToPlayPositon()
                        || firstVisibleItem + visibleItemCount == picAdapter.getClickToPlayPositon()
                        || picAdapter.getClickToPlayPositon() + 2 == firstVisibleItem - mHeadViewCount
                        || picAdapter.getClickToPlayPositon() + 1 == firstVisibleItem - mHeadViewCount)) {
            if (AdTools.videoType.equals(mUIPlayerContext.videoType)) {
                if (mUIPlayerContext.status == IPlayer.PlayerState.STATE_PLAYING) {
                    isRecovered = true;
                }
                if (mUIPlayerContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    isRecovered = false;
                }
            }
            recoverUI();

        }
    }

    private View getViewByPosition(ListView listView, int adapterPosition) {
        final int headerCount = listView.getHeaderViewsCount();
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = listView.getLastVisiblePosition();
        int position = adapterPosition + headerCount;
        logger.debug("headviewCount1", "firstListItemPosition:" + firstListItemPosition + "" +
                "\n lastListItemPosition:" + lastListItemPosition + "" + "\n adapterPosition:" + adapterPosition +
                "\n position:" + position);

        if (position < firstListItemPosition || position > lastListItemPosition) {
            logger.debug("越界将导致播放异常");
//            return listView.getAdapter().getView(position-headerCount, null, listView);
            return null;
        } else {
            final int childIndex = position - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    @Override
    public void showControllerView() {
        ValueAnimator mAnimator = null;
        mVideoSkin.getBigPicAdView().clearAnimation();

        if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(0), -DisplayUtils.convertDipToPixel(30.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });
            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        } else {
            mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(0), -DisplayUtils.convertDipToPixel(40.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().bringToFront();
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });

            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();
        }
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mVideoSkin.bringChildToFront(mVideoSkin.getBigPicAdView());
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    @Override
    public void hiddenControllerView() {
        ValueAnimator mAnimator = null;
        mVideoSkin.getBigPicAdView().clearAnimation();

        if (Configuration.ORIENTATION_PORTRAIT == mResource.getConfiguration().orientation) {
            mAnimator = ValueAnimator.ofFloat(-DisplayUtils.convertDipToPixel(30.0f), DisplayUtils.convertDipToPixel(0));
            mAnimator.setDuration(200);
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mVideoSkin.getBigPicAdView() != null) {
                        if (mVideoSkin.getBigPicAdView().getTranslationY() != 0) {
                            mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                            mVideoSkin.getBigPicAdView().invalidate();
                        }
                    }
                }
            });
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        } else {
            mAnimator = ValueAnimator.ofFloat(-DisplayUtils.convertDipToPixel(40.0f), DisplayUtils.convertDipToPixel(0.0f));
            mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    //将子view置顶
                    if (mVideoSkin.getBigPicAdView() != null) {
                        mVideoSkin.getBigPicAdView().bringToFront();
                        mVideoSkin.getBigPicAdView().setTranslationY((Float) valueAnimator.getAnimatedValue());
                        mVideoSkin.getBigPicAdView().invalidate();
                    }
                }
            });

            mAnimator.setDuration(200);
            mAnimator.setRepeatCount(0);
            mAnimator.start();

        }

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mVideoSkin.bringChildToFront(mVideoSkin.getBigPicAdView());

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    public void onPlayClick() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }

    @Override
    public void prePlayClick() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.setPrePlayNum();
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }

    @Override
    public void onClickStopUrl() {
        recoverUI();
    }

    @Override
    public void onClickAdToKnow() {
        recoverUI();
    }

    @Override
    public void onPausePlayButton() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.setPauseNum();
            picAdapter.adVideoRecord.stopPlayTime();
            initAdViewPosition();
        }
    }

    @Override
    public void onPlayButton() {
        if (null != picAdapter.adVideoRecord) {
            picAdapter.adVideoRecord.startPlayTime();
            initAdViewPosition();
        }
    }
}
