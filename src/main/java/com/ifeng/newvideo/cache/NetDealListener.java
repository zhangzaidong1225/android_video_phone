package com.ifeng.newvideo.cache;

/**
 * Created by cuihz on 2014/11/18.
 */
public interface NetDealListener {
    //无处理动作。无网或不允许3G下缓存
    int NO_ACTION = 0;

    void onDealByState(int state);
}
