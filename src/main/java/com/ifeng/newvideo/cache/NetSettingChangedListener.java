package com.ifeng.newvideo.cache;

import android.content.Context;

/**
 * 设置中“允许2g/3g/4g缓存/播放”开关改变回调接口，此为老版本形式，待与播放模块、设置模块统一讨论
 * Created by cuihz on 2014/7/17.
 */
public interface NetSettingChangedListener {
    int SETTING_SWITCH_NET_OFF = 1;
    int SETTING_SWITCH_NET_ON = 2;

    void onNetSettingSwitchChanged(Context context, int event);
}
