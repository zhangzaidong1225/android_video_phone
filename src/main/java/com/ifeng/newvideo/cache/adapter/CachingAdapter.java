package com.ifeng.newvideo.cache.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.cache.CircleProgress;
import com.ifeng.newvideo.cache.NetDealListener;
import com.ifeng.newvideo.cache.NetDealManager;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.mine.adapter.ICheckedNumber;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.exception.DownloadDBException;
import com.ifeng.video.core.exception.IllegalParamsException;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 正在缓冲列表adapter类
 */

public class CachingAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(CachingAdapter.class);
    private static final int MAX_PROGRESS = 100;
    private static final double CHANGE_M = 1024;

    private Context context;
    private boolean isInEditMode = false;//是否是编辑模式
    private boolean isDeleting = false;//是否正在删除
    private final NetDealManager netDealManager;
    private List<CacheVideoModel> selectedDownload = new ArrayList<>();//选中的数据
    private List<CacheVideoModel> showList = new ArrayList<>();//展示的数据

    private static final int TYPE_VIDEO = 0;//视频
    private static final int TYPE_AUDIO = 1;//音频

    public CachingAdapter(Context context) {
        this.context = context;
        netDealManager = new NetDealManager(context);
    }

    @Override
    public int getItemViewType(int position) {
        return IfengType.TYPE_AUDIO.equals(showList.get(position).getType()) ? TYPE_AUDIO : TYPE_VIDEO;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return showList.size();
    }

    @Override
    public Object getItem(int position) {
        return showList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DownViewHolder holder;
        MyViewOnClickListener onClickListener;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new DownViewHolder();
            onClickListener = new MyViewOnClickListener();
            convertView = LayoutInflater.from(context).inflate(type == TYPE_VIDEO ? R.layout.caching_video_list_item : R.layout.caching_audio_list_item, null);
            holder.initView(convertView);
            convertView.setTag(holder);
            convertView.setTag(R.id.caching_item, onClickListener);
        } else {
            holder = (DownViewHolder) convertView.getTag();
            onClickListener = (MyViewOnClickListener) convertView.getTag(R.id.caching_item);
        }

        onClickListener.setPosition(position);
        onClickListener.setTextView(holder.state_text);
        holder.root.setOnClickListener(onClickListener);
        CacheVideoModel cacheVideoModel = showList.get(position);
        holder.cacheVideoModel = cacheVideoModel;
        initItemData(holder, cacheVideoModel);
        return convertView;
    }

    /**
     * 添加Item数据
     */
    private void initItemData(DownViewHolder holder, CacheVideoModel cacheVideoModel) {
        String iconUrl = cacheVideoModel.getImgUrl();
        holder.item_icon_bg.setImageUrl(iconUrl, VolleyHelper.getImageLoader());
        holder.item_icon_bg.setDefaultImageResId(R.drawable.common_default_bg);
        holder.item_icon_bg.setErrorImageResId(R.drawable.common_default_bg);
        holder.title.setText(cacheVideoModel.getName());
        holder.state_text.setText(getStateText(cacheVideoModel));
        if (isInEditMode) {
            holder.down_check_img.setVisibility(View.VISIBLE);
            if (cacheVideoModel.getCheckState() == 1) {
                holder.down_check_img.setImageResource(R.drawable.common_edit_on);
            } else {
                holder.down_check_img.setImageResource(R.drawable.common_edit_on_no);
            }
        } else {
            holder.down_check_img.setVisibility(View.GONE);
        }

        double serverSize = formatVideoSize(cacheVideoModel.getTotalSize());
        holder.totalDownloadSize.setText(String.format("%.1f", serverSize) + "M");
        holder.changeProgress(cacheVideoModel.getProgress(), cacheVideoModel.getTotalSize());
        //将holder添加至下载监听中
        CacheManager.DownHandler handler = (CacheManager.DownHandler) CacheUtil.getDownloadQueue(context).getDownloadHandlerById(cacheVideoModel.getId());
        if (handler == null) {
            handler = new CacheManager.DownHandler(cacheVideoModel.getId(), null);
            CacheUtil.getDownloadQueue(context).registerDownloadHandler(cacheVideoModel.getId(), handler);
        }
        handler.setHolder(holder);
    }

    /**
     * 格式化视频大小
     */
    private double formatVideoSize(long size) {
        if (size > 0) {
            return size * 1.0 / CHANGE_M / CHANGE_M == 0 ? 1.0 : size * 1.0 / CHANGE_M / CHANGE_M;
        } else {
            return 0;
        }
    }

    /**
     * 设置是否是编辑模式
     */
    public void setInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
        //selectedDownload.clear();//清空
        notifyDataSetChanged();
    }

    /**
     * 获取选中的数据
     */
    public List<CacheVideoModel> getSelectedDownload() {
        return selectedDownload;
    }

    /**
     * 设置数据源
     */
    public void setShowList(List<CacheVideoModel> showList) {
        this.showList = showList;
        notifyDataSetChanged();
    }

    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setDeleting(boolean deleting) {
        isDeleting = deleting;
    }

    /**
     * 获取下载状态
     */
    private String getStateText(CacheVideoModel cacheVideoModel) {
        boolean isMobileOpen = SharePreUtils.getInstance().getCacheVideoState();
        switch (cacheVideoModel.getState()) {
            case DownloadOrder.STATE_DOWNING:
                return context.getString(R.string.cache_caching);
            case DownloadOrder.STATE_WAIT_DOWN:
                if (NetUtils.isNetAvailable(context)
                        && NetUtils.isMobile(context)
                        && !isMobileOpen) {
                    return context.getString(R.string.cache_only_wifi);
                } else {
                    return context.getString(R.string.cache_wait);
                }
            default:
                return context.getString(R.string.cache_pause);
        }
    }

    @Override
    public void notifyDataSetChanged() {
        if (!isDeleting) {
            super.notifyDataSetChanged();
        }
    }

    /**
     * OnClick事件
     */
    class MyViewOnClickListener implements View.OnClickListener {
        private int position;
        private TextView textView;

        public void setPosition(int position) {
            this.position = position;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }

        @Override
        public void onClick(View v) {
            CacheVideoModel cacheVideoModel = showList.get(position);
            if (isInEditMode) {
                dealInEditMode(cacheVideoModel);
                PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_MY_CHOOSE, getItemViewType(position) == TYPE_VIDEO);
            } else {
                dealNotInEditMode(cacheVideoModel, textView);
            }
        }
    }

    /**
     * 处理在编辑模式时的点击事件，点击item时添加到删除List中 ，并改变删除文字个数
     */
    private void dealInEditMode(CacheVideoModel cacheVideoModel) {
        if (cacheVideoModel.getCheckState() == 0) {//未选中置为选中状态
            cacheVideoModel.setCheckState(1);
            selectedDownload.add(cacheVideoModel);
        } else {
            cacheVideoModel.setCheckState(0);//选中状态置为未选中
            selectedDownload.remove(cacheVideoModel);
        }
        notifyDataSetChanged();
        if (checkedNumber != null) {
            checkedNumber.getCheckedNum(selectedDownload.size());
        }
    }

    /**
     * 处理不在编辑模式时的点击事件，
     * 点击item时改变文字缓存状态和执行相应的操作
     */
    private void dealNotInEditMode(final CacheVideoModel cacheVideoModel, TextView state_text) {
        String state = state_text.getText().toString();
        try {
            if (state.equals(context.getString(R.string.cache_pause))) {
                netDealManager.dealNetTypeWithSetting(new NetDealListener() {
                    @Override
                    public void onDealByState(int state) {
                        if (state == DownloadOrder.STATE_WAIT_DOWN) {
                            try {
                                CacheUtil.getDownloadQueue(context).changeStateById(cacheVideoModel.getId(), DownloadOrder.STATE_WAIT_DOWN);
                            } catch (IllegalParamsException e) {
                                logger.error(e.toString(), e);
                            } catch (DownloadDBException e) {
                                logger.error(e.toString(), e);
                            }
                            cacheVideoModel.setState(DownloadOrder.STATE_WAIT_DOWN);
                        }
                    }
                });
            } else if (state.equals(context.getString(R.string.cache_wait))) {
                CacheUtil.getDownloadQueue(context).changeStateById(cacheVideoModel.getId(), DownloadOrder.STATE_PAUSE);
                cacheVideoModel.setState(DownloadOrder.STATE_PAUSE);
            } else if (state.equals(context.getString(R.string.cache_caching))) {
                CacheUtil.getDownloadQueue(context).changeStateById(cacheVideoModel.getId(), DownloadOrder.STATE_PAUSE);
                cacheVideoModel.setState(DownloadOrder.STATE_PAUSE);
            } else if (state.equals(context.getString(R.string.cache_only_wifi))) {
                ToastUtils.getInstance().showShortToast("当前设置只在WiFi下缓存视频");
            }
        } catch (Exception e) {
            logger.error("change download state error \n", e);
        }
    }

    public static class DownViewHolder {
        private View root;//itemView
        private TitleTextView title;//标题
        private ImageView down_check_img;//选中未选中图标
        private TextView totalDownloadSize;//视频大小
        private TextView curDownloadSize;//当前下载视频大小
        private NetworkImageView item_icon_bg;//视频封面
        private CircleProgress progress;//环形进度条
        private View bottomLine;//底部横线
        public TextView state_text;//缓存状态
        public CacheVideoModel cacheVideoModel;

        /**
         * 初始化View
         */
        private void initView(View convertView) {
            root = convertView.findViewById(R.id.caching_item);
            convertView.findViewById(R.id.circleProgress_layout).getBackground().setAlpha(153);
            down_check_img = (ImageView) convertView.findViewById(R.id.caching_check_icon);
            item_icon_bg = (NetworkImageView) convertView.findViewById(R.id.thumbnail_bg);
            progress = (CircleProgress) convertView.findViewById(R.id.caching_progress);
            title = (TitleTextView) convertView.findViewById(R.id.caching_title);
            state_text = (TextView) convertView.findViewById(R.id.caching_state);
            totalDownloadSize = (TextView) convertView.findViewById(R.id.caching_totalSize);
            curDownloadSize = (TextView) convertView.findViewById(R.id.caching_cur_size);
            bottomLine = convertView.findViewById(R.id.cache_item_gray_line);
        }

        public void changeProgress(int progress, long totalSize) {
            this.progress.setMaxProgress(MAX_PROGRESS);
            if (progress > 0) {
                this.progress.setCurProgress(progress);
                curDownloadSize.setText(String.format("%.1f", progress * totalSize / 100 * 1.0 / CHANGE_M / CHANGE_M) + "M");
            } else {
                this.progress.setCurProgress(0);
                curDownloadSize.setText("0.0M");
            }
        }
    }

    private ICheckedNumber checkedNumber;

    public void setCheckedNumber(ICheckedNumber checkedNumber) {
        this.checkedNumber = checkedNumber;
    }
}
