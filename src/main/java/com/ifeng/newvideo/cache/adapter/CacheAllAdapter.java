package com.ifeng.newvideo.cache.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.mine.adapter.ICheckedNumber;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.widget.TitleTextView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.CacheBaseModel;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 已缓存列表adapter，包含“正在缓存”
 */
public class CacheAllAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(CacheAllAdapter.class);

    private Context context;
    private boolean isEditMode = false;//是否是编辑模式
    private final List<CacheBaseModel> selectedList;//选中的Item
    private List<? extends CacheBaseModel> datas = new ArrayList<>();//数据源

    private static final int TYPE_VIDEO = 0;//视频
    private static final int TYPE_AUDIO = 1;//音频

    public CacheAllAdapter(Context context) {
        this.context = context;
        selectedList = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        return IfengType.TYPE_AUDIO.equals(datas.get(position).getType()) ? TYPE_AUDIO : TYPE_VIDEO;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        OnClick onClick;
        int type = getItemViewType(position);
        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            onClick = new OnClick();
            convertView = LayoutInflater.from(context).inflate(type == TYPE_VIDEO ? R.layout.cache_video_list_item : R.layout.cache_audio_list_item, null);
            holder.initView(convertView);
            convertView.setTag(holder);
            convertView.setTag(R.id.itemView, onClick);
        } else {
            holder = (ViewHolder) convertView.getTag();
            onClick = (OnClick) convertView.getTag(R.id.itemView);
        }
        onClick.setPosition(position);
        holder.itemView.setOnClickListener(onClick);
        CacheBaseModel listInfoModel = datas.get(position);
        setListItemView(holder, (CacheVideoModel) listInfoModel);
        return convertView;
    }

    /**
     * 为每个Item设置数据
     */
    private void setListItemView(ViewHolder holder, CacheVideoModel cacheBaseModel) {
        double serverSize = formatVideoSize(cacheBaseModel.getTotalSize());
        holder.videoSize.setText(String.format("%.1f", serverSize) + "M");
        holder.title.setText(cacheBaseModel.getName());
        String duration = DateUtils.convertMMTime(cacheBaseModel.getDuration() * 1000);
        holder.item_time.setText(duration);
        holder.videoImg.setImageUrl(cacheBaseModel.getImgUrl(), VolleyHelper.getImageLoader());
        holder.videoImg.setDefaultImageResId(R.drawable.common_default_bg);
        holder.videoImg.setErrorImageResId(R.drawable.common_default_bg);
        if (isEditMode) {//如果是编辑状态
            holder.check_img.setVisibility(View.VISIBLE);
            if (cacheBaseModel.getCheckState() == 0) {
                holder.check_img.setImageResource(R.drawable.common_edit_on_no);
            } else {
                holder.check_img.setImageResource(R.drawable.common_edit_on);
            }
        } else {//非编辑状态
            holder.check_img.setVisibility(View.GONE);
        }
    }

    /**
     * 格式化视频大小数据
     */
    private double formatVideoSize(long size) {
        int CHANGE_M = 1024;
        if (size > 0) {
            return size * 1.0 / CHANGE_M / CHANGE_M == 0 ? 1.0 : size * 1.0 / CHANGE_M / CHANGE_M;
        } else {
            return 0;
        }
    }

    /**
     * 获取选中的Item数据
     */
    public List<CacheBaseModel> getSelectedList() {
        return selectedList;
    }

    /**
     * 获取所有数据
     */
    public List<CacheBaseModel> getAllSelectedList() {
        selectedList.clear();
        selectedList.addAll(datas);
        return selectedList;
    }

    /**
     * 设置是否是编辑模式
     */
    public void setInEditMode(boolean mode) {
        this.isEditMode = mode;
        notifyDataSetChanged();
    }

    public void setDatas(List<? extends CacheBaseModel> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        private View itemView;
        private NetworkImageView videoImg;//封面
        private TitleTextView title;//标题
        private TextView videoSize;//视频大小
        private TextView item_time;//时长
        private ImageView check_img;//选中未选中
        private View line;//下划线

        private void initView(View convertView) {
            itemView = convertView.findViewById(R.id.itemView);
            videoImg = (NetworkImageView) convertView.findViewById(R.id.cache_all_item_video_img);
            title = (TitleTextView) convertView.findViewById(R.id.cache_all_item_title);
            videoSize = (TextView) convertView.findViewById(R.id.cache_item_size);
            item_time = (TextView) convertView.findViewById(R.id.item_time);
            check_img = (ImageView) convertView.findViewById(R.id.cache_all_check_img);
            line = convertView.findViewById(R.id.line);
        }
    }

    /**
     * 处理编辑模式下选中未选中逻辑
     */
    private void handleCheckLogic(CacheVideoModel model) {
        if (model.getCheckState() == 0) {
            model.setCheckState(1);
            selectedList.add(model);
        } else {
            model.setCheckState(0);
            selectedList.remove(model);
        }
        notifyDataSetChanged();
        if (checkedNumber != null) {
            checkedNumber.getCheckedNum(selectedList.size());
        }
    }

    /**
     * 处理非编辑模式下跳转逻辑
     */
    private void handleJumpLogic(CacheVideoModel model, int position) {
        boolean isAudio = IfengType.TYPE_AUDIO.equals(model.getType());
        IntentUtils.startActivityCacheVideoPlayer(context, model, (ArrayList<CacheVideoModel>) datas);
    }

    /**
     * 点击事件
     */
    class OnClick implements View.OnClickListener {
        int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            CacheVideoModel model = (CacheVideoModel) datas.get(position);
            if (isEditMode) {
                handleCheckLogic(model);
                PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_MY_CHOOSE, getItemViewType(position)==TYPE_VIDEO);
            } else {
                handleJumpLogic(model, position);
            }
        }
    }

    private ICheckedNumber checkedNumber;

    public void setCheckedNumber(ICheckedNumber checkedNumber) {
        this.checkedNumber = checkedNumber;
    }
}
