package com.ifeng.newvideo.cache;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.smart.SmartStatistic;
import com.ifeng.newvideo.statistics.smart.domains.NetChange;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.download.DownloadQueue;
import com.ifeng.video.core.exception.DownloadDBException;
import com.ifeng.video.core.exception.IllegalParamsException;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * 网络状态切换处理类
 * Created by cuihz on 2014/7/17.
 */
public class NetReceiver extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(NetReceiver.class);

    public static NetDealManager netDealManager;

    private static List<Integer> cachingAndWeit;

    private static int lastType = -1;
    private static final int NO_NET = -1000;


    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            sendSmartChangeStatistic(context);
            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connManager.getActiveNetworkInfo();
            //监听网络状态切换
            int type;
            if (info == null) {
                logger.debug("receiver: no net");
                type = NO_NET;
            } else {
                type = info.getType();
                logger.debug("receiver: {}", type);
            }
            if (type == lastType) {
            } else {
                lastType = type;
                if (CacheManager.isCaching4NetReceiver(context) || (cachingAndWeit != null && cachingAndWeit.size() > 0)) {
                    if (CacheManager.isCaching4NetReceiver(context)) {
                        //CacheUtil.getDownloadQueue(context).hold(true);
                        try {
                            holdDownload(context, true);
                        } catch (DownloadDBException e) {
                            logger.error(e.toString(), e);
                        } catch (IllegalParamsException e) {
                            logger.error(e.toString(), e);
                        }
                    }
                    if (lastType == NO_NET) {
                        try {
                            CacheUtil.getDownloadQueue(context).changeStateByGroup(DownloadOrder.STATE_PAUSE, IfengType.TYPE_All);
                        } catch (IllegalParamsException e) {
                            logger.error(e.toString(), e);
                        } catch (DownloadDBException e) {
                            logger.error(e.toString(), e);
                        }
                        ToastUtils.getInstance().showLongToast(R.string.cache_curDownloadVideo_paused);
                        return;
                    }

                    if (netDealManager == null) {
                        netDealManager = new NetDealManager(context);
                    }
                    netDealManager.dealNetTypeWithSetting(new NetDealListener() {
                        @Override
                        public void onDealByState(int state) {
                            if (state == DownloadOrder.STATE_WAIT_DOWN) {
                                //CacheUtil.getDownloadQueue(context).hold(false);
                                try {
                                    holdDownload(context, false);
                                } catch (DownloadDBException e) {
                                    logger.error(e.toString(), e);
                                } catch (IllegalParamsException e) {
                                    logger.error(e.toString(), e);
                                }
                            }
                            if (cachingAndWeit != null) {
                                cachingAndWeit.clear();
                            }
                        }
                    });
                }
            }
        }
    }


    private void holdDownload(Context context, boolean toHold) throws DownloadDBException, IllegalParamsException {
        if (toHold) {
            if (CacheManager.mDownloadingList == null) {
                return;
            }
            cachingAndWeit = new ArrayList<Integer>();
            logger.debug("----holdDownload CacheManager.mDownloadingList  = {}", CacheManager.mDownloadingList.size());
            for (CacheVideoModel cacheVideoModel : CacheManager.mDownloadingList) {
                logger.debug("----holdDownload cacheVideoModel.getState()  = {}", cacheVideoModel.getState());
                if (cacheVideoModel.getState() == DownloadOrder.STATE_DOWNING) {
                    cachingAndWeit.add(0, cacheVideoModel.getId());
                } else if (cacheVideoModel.getState() == DownloadOrder.STATE_WAIT_DOWN || cacheVideoModel.getState() == DownloadOrder.STATE_FAILED) {
                    cachingAndWeit.add(cacheVideoModel.getId());
                }
            }
            logger.debug("----holdDownload  cachingAndWeit  = {}", cachingAndWeit.size());
//            CacheUtil.getDownloadQueue(context).changeStateByGroup(DownloadOrder.STATE_PAUSE);
        } else {
            if (cachingAndWeit == null || cachingAndWeit.size() <= 0) {
                return;
            }
            DownloadQueue queue = CacheUtil.getDownloadQueue(context);
            queue.setDownloaderPriority(cachingAndWeit.get(0), DownloadOrder.PRIORITY_DOWNING);
            queue.changeStateByIds(cachingAndWeit, DownloadOrder.STATE_WAIT_DOWN);
            cachingAndWeit.clear();
        }
    }

    /**
     * 发送网络改变智能统计
     */
    private void sendSmartChangeStatistic(Context context) {
        NetChange netChange = new NetChange();
        netChange.setUserId(new User(context).getUid());
        netChange.setDeviceId(PhoneConfig.userKey);
        netChange.setDataType(UserOperatorConst.DATA_TYPE);
        netChange.setNet(NetUtils.getNetTypeForSmartStatistics(context));
        netChange.setCarrierName(NetUtils.getNetName(context));
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.CHANGE_URL, netChange);
    }
}
