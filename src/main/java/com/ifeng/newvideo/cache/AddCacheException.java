package com.ifeng.newvideo.cache;

import com.ifeng.video.core.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 添加缓存失败exception
 * Created by cuihz on 2014/8/1.
 */
public class AddCacheException extends BaseException {

    private static final Logger logger = LoggerFactory.getLogger(AddCacheException.class);

    private AddCacheException(String msg) {
        super(msg);
    }

    private AddCacheException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    private AddCacheException(Throwable throwable) {
        super(throwable);
    }


}
