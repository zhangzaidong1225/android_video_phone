package com.ifeng.newvideo.cache;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.*;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.ColumnUtils;
import com.ifeng.newvideo.utils.TransformTopicData;
import com.ifeng.newvideo.videoplayer.adapter.VideoDownLoadFragmentAdapter;
import com.ifeng.video.core.exception.IllegalParamsException;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.dao.VideoPlayDao;
import com.ifeng.video.dao.db.model.*;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by hu on 2014/10/27.
 */
public class CacheMoreVideoActivity extends BaseFragmentActivity implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(CacheMoreVideoActivity.class);

    private View progressView;
    private View emptyview_RL;
    private TextView empty_tv;
    private VideoDownLoadFragmentAdapter adapter;
    private static final String TAG = "RelatedFragment";
    private ListView listview;
    public Bundle bundle;
    public AdapterView.OnItemClickListener mOnItemClickListener;
    public boolean isById = false;
    private TextView spinerBtnText;
    private CacheFolderModel infoModel;
    /**
     * progressBar最大进度
     */
    private static final int MAX_PROGRESS = 100;
    /**
     * 存储卡容量信息Layout
     */
    private View mSDcardInfoView;
    /**
     * 存储卡容量信息进度条
     */
    private ProgressBar mSDcardCapacityProgress;
    /**
     * 存储卡容量信息
     */
    private TextView mSDcardCapacityText;
    /**
     * 存储卡广播接收器
     */
    private BroadcastReceiver mSDcardBroadReceiver;
    private SharedPreferences mSharedPreferences;
    private Button offCancelBtn;
    private Button offDownLoadBtn;
    private LinearLayout offBtnLL;
    private LinearLayout offSpinerLL;

    private TextView spinerOBtn;
    private TextView spinerSBtn;
    private TextView spinerHBtn;
    private TextView spinerMBtn;
    private TextView spinerLBtn;
    private List<PlayerInfoModel> PlayerInfoModelList;
    private List<SubColumnVideoListInfo.VideoItem> VideoItemList;
    private String id;
    private String type;
    private int currentDownStream = MediaConstants.STREAM_HIGH;
    //    private ImageView backBtn;
    private final ArrayList<PlayerInfoModel> mDownLoadTopicList = new ArrayList<PlayerInfoModel>();
    private final ArrayList<SubColumnVideoListInfo.VideoItem> mDownLoadColumnList = new ArrayList<SubColumnVideoListInfo.VideoItem>();
    private int mDownLoadSize = 0;
    private SubColumnVideoListInfo subColumnVideoListInfo;
    private CMPPSubTopicModel cmppSubTopicModel;
    private ImageView streamImage;

    private NetDealManager netDealManager;

    private AtomicInteger columnCount;
    private List<PlayerInfoModel> mDownloadColumnPlayInfos;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cache_activity_more);

        netDealManager = new NetDealManager(this);
        initViews();
        Intent intent = getIntent();
        id = intent.getStringExtra(IntentKey.FOLDER_GUID);
        type = intent.getStringExtra(IntentKey.FOLDER_TYPE);
        infoModel = (CacheFolderModel) intent.getSerializableExtra(IntentKey.CACHE_FOLDER_MODEL);
        for (CacheBaseModel info : CacheManager.mDownloadAllList) {
            if (info.getId() == CacheUtil.getIdFromGuid(type, id)) {
                infoModel = (CacheFolderModel) info;
            }
        }

        initData(id, type);
        initSDcardBR();


        spinerBtnText.setText(this.getResources().getString(R.string.common_video_high));
        overridePendingTransition(R.anim.common_below_in, R.anim.common_below_out);
    }

    private void getDataFromTopic() {
        PlayerInfoModelList = new ArrayList<PlayerInfoModel>();
        List<String> list = new ArrayList<String>();
        if (cmppSubTopicModel != null) {


            for (int i = 0; i < cmppSubTopicModel.subTopicList.size(); i++) {
                for (int j = 0; j < cmppSubTopicModel.subTopicList.get(i).detailList.size(); j++) {
                    CMPPSubTopicModel.SubTopicList.TopicDetail detail = cmppSubTopicModel.subTopicList.get(i).detailList.get(j);
                    if (detail == null || detail.memberItem == null || detail.memberItem.guid == null) {
                        continue;
                    }
                    PlayerInfoModel model = TransformTopicData.topicDetailToPlayerInfoModel(detail);
                    PlayerInfoModelList.add(model);
                    list.add(model.title);
                }

            }

            try {
                setListToAdapter(list);
            } catch (IllegalParamsException e) {
                logger.error(e.toString(), e);
            }
        } else {
            setEmptyVisible();
        }
    }

    private void getDataFromColumn() {
        logger.debug("getDataFromColumn");
        VideoItemList = new ArrayList<SubColumnVideoListInfo.VideoItem>();
        List<String> list = new ArrayList<String>();
        if (subColumnVideoListInfo != null) {
            logger.debug("column size:{}", subColumnVideoListInfo.getSubVideoListList().size());
            for (int i = 0; i < subColumnVideoListInfo.getSubVideoListList().size(); i++) {
                SubColumnVideoListInfo.VideoItem item = subColumnVideoListInfo.getSubVideoListList().get(i);
                if (item.getName() != null) {
                    list.add(ColumnUtils.convertColumnTitle(item));
                }
                VideoItemList.add(item);
            }
            try {
                setListToAdapter(list);
            } catch (IllegalParamsException e) {
                logger.error(e.toString(), e);
            }

        } else {
            setEmptyVisible();
        }
    }

    private void refreshView() throws IllegalParamsException {
        logger.debug("refreshView");
        if (CheckIfengType.isTopicType(type)) {
            getDataFromTopic();
        } else if (CheckIfengType.isColumn(type)) {
            getDataFromColumn();
        }
    }

    private void initListViewSelected() {
        if (CheckIfengType.isTopicType(type)) {
            initTopicListViewSelected();
        } else {
            initColumnListViewSelected();
        }
    }

    private void initTopicListViewSelected() {
        if (PlayerInfoModelList != null && PlayerInfoModelList.size() > 0) {
            for (int i = 0; i < PlayerInfoModelList.size(); i++) {
                PlayerInfoModel model = PlayerInfoModelList.get(i);
                if (model != null && CacheManager.isInCache(this, model.guid)) {
                    adapter.mViewStateList[i] = VideoDownLoadFragmentAdapter.DOWNLOADED;
                }
            }
        }
    }

    private void initColumnListViewSelected() {
        if (VideoItemList != null && VideoItemList.size() > 0) {
            for (int i = 0; i < VideoItemList.size(); i++) {
                SubColumnVideoListInfo.VideoItem model = VideoItemList.get(i);
                if (model != null && CacheManager.isInCache(this, model.guid)) {
                    adapter.mViewStateList[i] = VideoDownLoadFragmentAdapter.DOWNLOADED;
                }
            }
        }
    }

    private void setListToAdapter(List<String> list) throws IllegalParamsException {
        adapter = new VideoDownLoadFragmentAdapter(this, IfengType.TYPE_VIDEO);
        adapter.setList(list);
        initListViewSelected();
        listview.setAdapter(adapter);
        adapter.resetSelected();
        emptyview_RL.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

    private void setEmptyVisible() {
        progressView.setVisibility(View.GONE);
        emptyview_RL.setVisibility(View.VISIBLE);
    }


    private void getTopicList(String type, String id) {
        logger.debug("type:{},id:{}", type, id);
        VideoPlayDao.getTopicList(type, id,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object result) {
                        logger.debug("request success");
                        if (result != null && result.toString().length() > 0) {
                            CMPPSubTopicModel tempCmppSubTopicModel = JSONObject.parseObject(result.toString(), CMPPSubTopicModel.class);
                            List<CMPPSubTopicModel.SubTopicList> subTopicList = tempCmppSubTopicModel.subTopicList;

                            //数据去重
                            try {
                                for (CMPPSubTopicModel.SubTopicList subList : subTopicList) {
                                    List<CMPPSubTopicModel.SubTopicList.TopicDetail> topicDetails = subList.detailList;
                                    for (int i = 0; i < topicDetails.size(); i++) {
                                        for (int j = i + 1; j < topicDetails.size(); j++) {
                                            String iMemberType = topicDetails.get(i).memberType;
                                            String jMemberType = topicDetails.get(j).memberType;
                                            if (iMemberType != null && iMemberType.equals(jMemberType)) {
                                                if (CheckIfengType.isVideo(iMemberType)) {
                                                    if (topicDetails.get(i).memberItem != null && topicDetails.get(j).memberItem != null && topicDetails.get(i).memberItem.guid.equals(topicDetails.get(j).memberItem.guid)) {
                                                        topicDetails.remove(j);
                                                        j--;
                                                    }
                                                } else if (CheckIfengType.isCmppTopic(iMemberType) || CheckIfengType.isLiveType(iMemberType)) {
                                                    if (topicDetails.get(i).memberId.equals(topicDetails.get(j).memberId)) {
                                                        topicDetails.remove(j);
                                                        j--;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                cmppSubTopicModel = tempCmppSubTopicModel;
                            } catch (Exception e) {
                                cmppSubTopicModel = JSONObject.parseObject(result.toString(), CMPPSubTopicModel.class);
                                logger.error("缓存专题数据去重发生异常  :::  {}", e.getMessage());
                            }
                            try {
                                refreshView();
                            } catch (IllegalParamsException e) {
                                ToastUtils.getInstance().showShortToast(CacheMoreVideoActivity.this.getResources().getString(R.string.common_net_useless));
                            }
                        } else {
                            setEmptyVisible();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        logger.debug("request failed");
                        setEmptyVisible();
                    }
                });
    }


    private void getColumnList(String id) {
        logger.debug("getColumnList");
        VideoPlayDao.requestColumnVideoList(id,
                new Response.Listener<SubColumnVideoListInfo>() {

                    @Override
                    public void onResponse(SubColumnVideoListInfo response) {
                        logger.debug("getColumnList,onResponse");
                        if (response != null && response.toString().length() > 0) {
                            subColumnVideoListInfo = response;
                            try {
                                refreshView();
                            } catch (IllegalParamsException e) {
                                ToastUtils.getInstance().showShortToast(CacheMoreVideoActivity.this.getResources().getString(R.string.common_net_useless));
                            }
                        } else {
                            setEmptyVisible();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        logger.debug("request failed");
                        setEmptyVisible();
                    }
                });
    }


    private void getPlayInfoByGuid(final int state, String guid) {
        if (!NetUtils.isNetAvailable(CacheMoreVideoActivity.this)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        VideoPlayDao.getSingleVideoByGuidForDownload(guid, new PlayerInfoModel(), new Response.Listener<PlayerInfoModel>() {
            @Override
            public void onResponse(PlayerInfoModel response) {
                if (response == null) {
                    isColumnOver(state);
                    return;
                }
                mDownloadColumnPlayInfos.add(response);
                isColumnOver(state);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isColumnOver(state);
            }
        });
    }

    private void isColumnOver(int state) {
        int i = columnCount.decrementAndGet();
        if (i <= 0) {
            CacheManager.addDownloadList(CacheMoreVideoActivity.this, mDownloadColumnPlayInfos, state, currentDownStream, IfengType.TYPE_VIDEO, infoModel);
        }
    }

    private void initData(String id, String type) {
        progressView.setVisibility(View.VISIBLE);
        logger.debug("init data");
        if (CheckIfengType.isTopicType(type)) {
            logger.debug("isTopicType");
            getTopicList(type, id);
        } else if (CheckIfengType.isColumn(type)) {
            logger.debug("isColumn");
            getColumnList(id);
        }

    }

    private void initViews() {
//        backBtn = (ImageView) findViewById(R.id.back);
        streamImage = (ImageView) findViewById(R.id.stream_imageview);
        streamImage.setOnClickListener(this);
//        backBtn.setOnClickListener(this);
        progressView = findViewById(R.id.video_detail_loading_progress_layout);
        emptyview_RL = findViewById(R.id.emptyView_RL);
        empty_tv = (TextView) findViewById(R.id.empty_tv);
        offCancelBtn = (Button) findViewById(R.id.off_line_cancel);
        offDownLoadBtn = (Button) findViewById(R.id.off_line_down_load);
        offBtnLL = (LinearLayout) findViewById(R.id.off_line_btn_LL);
        offSpinerLL = (LinearLayout) findViewById(R.id.off_line_spinner_LL);
        spinerBtnText = (TextView) findViewById(R.id.off_line_btn_tx);
        offBtnLL.setOnClickListener(this);
        offSpinerLL.setOnClickListener(this);
        offCancelBtn.setOnClickListener(this);
        offDownLoadBtn.setOnClickListener(this);
        spinerOBtn = (TextView) findViewById(R.id.spinner_original_btn);
        spinerSBtn = (TextView) findViewById(R.id.spinner_supper_btn);
        spinerHBtn = (TextView) findViewById(R.id.spinner_high_btn);
        spinerMBtn = (TextView) findViewById(R.id.spinner_normal_btn);
        spinerLBtn = (TextView) findViewById(R.id.spinner_low_btn);
        spinerHBtn.setOnClickListener(this);
        spinerOBtn.setOnClickListener(this);
        spinerSBtn.setOnClickListener(this);
        spinerMBtn.setOnClickListener(this);
        spinerLBtn.setOnClickListener(this);
        listview = (ListView) findViewById(R.id.rank_list);
        mSDcardInfoView = findViewById(R.id.capacity_info_FL);
        mSDcardCapacityProgress = (ProgressBar) findViewById(R.id.capacity_progress);
        mSDcardCapacityText = (TextView) findViewById(R.id.capacity_tv);
        listview.setOnItemClickListener(new DetailItemClickListener());
        emptyview_RL.setOnClickListener(this);
        emptyview_RL.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
    }

    private void updateStreamSeleterView() {
        updateStreamBtnListView(true);
    }

    private void clearList() {
        if (CheckIfengType.isTopicType(type)) {
            mDownLoadTopicList.clear();
        } else {
            mDownLoadColumnList.clear();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            clearList();
            mDownLoadSize = 0;
            finish();
            overridePendingTransition(R.anim.common_below_in, R.anim.common_below_out);
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public void onClick(View v) {
        logger.debug("on click");
        switch (v.getId()) {
            case R.id.emptyView_RL:
                refreshData();
                break;
            case R.id.off_line_btn_LL://缓存fragment 清晰度按钮
                logger.debug("off_line_btn_LL");
                streamImage.setSelected(true);
                offBtnLL.setBackgroundResource(R.drawable.common_corners_blue);
                spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengwhite));
                updateStreamSeleterView();
                break;
            case R.id.spinner_original_btn://缓存fragment 原画按钮
                logger.debug("spiner_height_btn");
                streamImage.setSelected(false);
                offBtnLL.setBackgroundResource(R.drawable.common_corners);
                spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
                handleDownloadOriginalStreamBtn();
                break;
            case R.id.spinner_supper_btn://缓存fragment 超清按钮
                logger.debug("spiner_height_btn");
                streamImage.setSelected(false);
                offBtnLL.setBackgroundResource(R.drawable.common_corners);
                spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
                handleDownloadSupperStreamBtn();
                break;
            case R.id.spinner_high_btn://缓存fragment 高清按钮
                logger.debug("spiner_height_btn");
                streamImage.setSelected(false);
                offBtnLL.setBackgroundResource(R.drawable.common_corners);
                spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
                handleDownloadHightStreamBtn();
                break;
            case R.id.spinner_normal_btn://缓存fragment 中清按钮
                streamImage.setSelected(false);
                offBtnLL.setBackgroundResource(R.drawable.common_corners);
                spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
                handleDownloadMidStreamBtn();
                break;
            case R.id.spinner_low_btn://缓存fragment 低清按钮
                streamImage.setSelected(false);
                offBtnLL.setBackgroundResource(R.drawable.common_corners);
                spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
                handleDownloadLowStreamBtn();
                break;
//            case R.id.back:
//                clearList();
//                mDownLoadSize = 0;
//                finish();
//                overridePendingTransition(R.anim.common_below_in, R.anim.common_below_out);
//                break;
            case R.id.off_line_cancel:
                clearList();
                mDownLoadSize = 0;
                finish();
                overridePendingTransition(R.anim.common_below_in, R.anim.common_below_out);
                break;
            case R.id.off_line_down_load:
                if (!isNoHavePreDownLoad()) {
                    netDealManager.dealNetTypeWithSetting(new NetDealListener() {
                        @Override
                        public void onDealByState(int state) {
                            if (state != NetDealListener.NO_ACTION) {
                                cacheByState(state);
                            }
                            CacheMoreVideoActivity.this.finish();
                        }
                    });

                }
                break;
        }
    }

    private void cacheByState(int state) {
        if (CheckIfengType.isTopicType(type)) {
            CacheManager.addDownloadList(this, mDownLoadTopicList, state, currentDownStream, IfengType.TYPE_VIDEO, infoModel);
        } else {
            //do column request by guid
            columnCount = new AtomicInteger(mDownLoadColumnList.size());
            mDownloadColumnPlayInfos = new ArrayList<PlayerInfoModel>();
            for (int i = 0; i < mDownLoadColumnList.size(); i++) {
                getPlayInfoByGuid(state, mDownLoadColumnList.get(i).guid);
            }
        }
    }

    private void updateStreamBtnListView(boolean isCanPlayHight) {
        if (offSpinerLL != null) {
            if (offSpinerLL.getVisibility() == View.GONE) {
                offSpinerLL.setVisibility(View.VISIBLE);
                if (spinerBtnText != null && spinerOBtn != null && spinerSBtn != null && spinerHBtn != null && spinerMBtn != null & spinerLBtn != null) {
                    if (isCanPlayHight) {
                        spinerHBtn.setVisibility(View.VISIBLE);
                        offSpinerLL.getLayoutParams().height = DisplayUtils.convertDipToPixel(94);
                        offSpinerLL.setPadding(DisplayUtils.convertDipToPixel(1), 0, DisplayUtils.convertDipToPixel(1),
                                DisplayUtils.convertDipToPixel(1));
                    } else {
                        spinerHBtn.setVisibility(View.GONE);
                        offSpinerLL.getLayoutParams().height = DisplayUtils.convertDipToPixel(63);
                        offSpinerLL.setPadding(DisplayUtils.convertDipToPixel(1), 0, DisplayUtils.convertDipToPixel(1),
                                DisplayUtils.convertDipToPixel(1));
                    }
                    offSpinerLL.requestLayout();
                    if (this.getResources().getString(R.string.common_video_high).equalsIgnoreCase(spinerBtnText.getText().toString())) {
                        spinerSBtn.setSelected(false);
                        spinerOBtn.setSelected(false);
                        spinerHBtn.setSelected(true);
                        spinerMBtn.setSelected(false);
                        spinerLBtn.setSelected(false);
                    } else if (this.getResources().getString(R.string.common_video_mid).equalsIgnoreCase(spinerBtnText.getText().toString())) {
                        spinerSBtn.setSelected(false);
                        spinerOBtn.setSelected(false);
                        spinerHBtn.setSelected(false);
                        spinerMBtn.setSelected(true);
                        spinerLBtn.setSelected(false);
                    } else if (this.getResources().getString(R.string.common_video_supper).equalsIgnoreCase(spinerBtnText.getText().toString())) {
                        spinerHBtn.setSelected(false);
                        spinerOBtn.setSelected(false);
                        spinerSBtn.setSelected(true);
                        spinerMBtn.setSelected(false);
                        spinerLBtn.setSelected(false);
                    } else if (this.getResources().getString(R.string.common_video_original).equalsIgnoreCase(spinerBtnText.getText().toString())) {
                        spinerHBtn.setSelected(false);
                        spinerSBtn.setSelected(false);
                        spinerOBtn.setSelected(true);
                        spinerMBtn.setSelected(false);
                        spinerLBtn.setSelected(false);
                    } else {
                        spinerSBtn.setSelected(false);
                        spinerOBtn.setSelected(false);
                        spinerHBtn.setSelected(false);
                        spinerMBtn.setSelected(false);
                        spinerLBtn.setSelected(true);
                    }
                }
            } else {
                offSpinerLL.setVisibility(View.GONE);
            }
        }
    }

    private void handleDownloadLowStreamBtn() {
        currentDownStream = MediaConstants.STREAM_LOW;
        spinerBtnText.setText(this.getResources().getString(R.string.common_video_low));
        spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
        updateStreamBtnListView(true);
    }

    private void handleDownloadMidStreamBtn() {
        currentDownStream = MediaConstants.STREAM_MID;
        spinerBtnText.setText(this.getResources().getString(R.string.common_video_mid));
        spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
        updateStreamBtnListView(true);
    }

    private void handleDownloadOriginalStreamBtn() {
        currentDownStream = MediaConstants.STREAM_ORIGINAL;
        spinerBtnText.setText(this.getResources().getString(R.string.common_video_original));
        spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
        updateStreamBtnListView(true);
    }

    private void handleDownloadSupperStreamBtn() {
        currentDownStream = MediaConstants.STREAM_SUPPER;
        spinerBtnText.setText(this.getResources().getString(R.string.common_video_supper));
        spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
        updateStreamBtnListView(true);
    }

    private void handleDownloadHightStreamBtn() {
        currentDownStream = MediaConstants.STREAM_HIGH;
        spinerBtnText.setText(this.getResources().getString(R.string.common_video_high));
        spinerBtnText.setTextColor(this.getResources().getColor(R.color.common_ifengblue));
        updateStreamBtnListView(true);
    }


    private boolean isNoHavePreDownLoad() {
        if (CheckIfengType.isTopicType(type)) {
            return mDownLoadTopicList == null || mDownLoadTopicList.size() == 0;
        } else {
            return mDownLoadColumnList == null || mDownLoadColumnList.size() == 0;
        }
    }

    private void removeOb(Object ob) {
        if (CheckIfengType.isTopicType(type)) {
            mDownLoadTopicList.remove(ob);
        } else {
            mDownLoadColumnList.remove(ob);
        }
    }

    private void addOb(Object ob) {
        if (CheckIfengType.isTopicType(type)) {
            mDownLoadTopicList.add((PlayerInfoModel) ob);
        } else {
            mDownLoadColumnList.add((SubColumnVideoListInfo.VideoItem) ob);
        }
    }

    private int listSize() {
        if (CheckIfengType.isTopicType(type)) {
            return mDownLoadTopicList.size();
        } else {
            return mDownLoadColumnList.size();
        }
    }

    private class DetailItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
            // 点击的是headView或者footerView
            if (id <= -1) {
                return;
            }

            PlayerInfoModel topicProgram = null;
            SubColumnVideoListInfo.VideoItem columnProgram = null;
            Object ob = null;

            int realPosition = (int) id;
            String guid;
            if (CheckIfengType.isTopicType(type)) {
                if (PlayerInfoModelList != null && PlayerInfoModelList.size() > 0) {
                    topicProgram = PlayerInfoModelList.get(realPosition);//adapter.getList().get(realPosition);
                    guid = topicProgram.guid;
                    ob = topicProgram;
                } else {
                    return;
                }
            } else {

                if (VideoItemList != null && VideoItemList.size() > 0) {
                    columnProgram = VideoItemList.get(realPosition);//adapter.getList().get(realPosition);
                    guid = columnProgram.guid;
                    ob = columnProgram;
                    logger.debug("DetailItemClickListener column");
                } else {
                    return;
                }
            }


            if (adapter.mViewStateList[position] == VideoDownLoadFragmentAdapter.PREPAREDOWNLOAD) {
                if (CacheManager.isInCache(CacheMoreVideoActivity.this, guid)) {
                    ToastUtils.getInstance().showShortToast(CacheMoreVideoActivity.this.getResources().getString(R.string.common_already_to_download));
                    return;
                }
                adapter.mViewStateList[position] = VideoDownLoadFragmentAdapter.NODOWNLOAD;
                adapter.notifyDataSetChanged();
                logger.debug("DetailItemClickListener remove");
                removeOb(ob);

                if (listSize() != 0) {
                    offDownLoadBtn.setEnabled(true);
                    offDownLoadBtn.setText(CacheMoreVideoActivity.this.getResources().getString(
                            R.string.download_continue_start)
                            + "(" + listSize() + ")");
                } else {
                    offDownLoadBtn.setEnabled(false);
                    offDownLoadBtn.setText(CacheMoreVideoActivity.this.getResources().getString(
                            R.string.download_continue_start));
                }
                return;
            }


            if (!adapter.isSelectedPos(position)
                    && adapter.mViewStateList[position] == VideoDownLoadFragmentAdapter.NODOWNLOAD) {

                if (ob != null) {
                    if (CacheManager.isInCache(CacheMoreVideoActivity.this, guid)) {
                        ToastUtils.getInstance().showShortToast(CacheMoreVideoActivity.this.getString(R.string.common_already_to_download));
                        return;
                    }
                    logger.debug("DetailItemClickListener add");
                    adapter.mViewStateList[position] = VideoDownLoadFragmentAdapter.PREPAREDOWNLOAD;
                    adapter.notifyDataSetChanged();
                    addOb(ob);
                    offDownLoadBtn.setEnabled(true);
                    offDownLoadBtn.setText(CacheMoreVideoActivity.this.getString(
                            R.string.download_continue_start)
                            + "(" + listSize() + ")");
                }

                return;
            }
            if (!adapter.isSelectedPos(position) && adapter.mViewStateList[position] == VideoDownLoadFragmentAdapter.DOWNLOADED) {
                if (ob != null) {
                    if (CacheManager.isInCache(CacheMoreVideoActivity.this, guid)) {
                        ToastUtils.getInstance().showShortToast(CacheMoreVideoActivity.this.getResources().getString(R.string.common_already_to_download));
                    }
                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            listview.setSelection(adapter.getSelectedPos());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSDcardBroadReceiver != null) {
            this.unregisterReceiver(mSDcardBroadReceiver);
        }
        adapter = null;
    }

    private void refreshData() {
        emptyview_RL.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);
        initData(id, type);
    }


    /**
     * 注册sd卡监听广播
     */
    private void initSDcardBR() {
        mSDcardBroadReceiver = new SDcardReceiver(this, mSDcardCapacityProgress, mSDcardCapacityText);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addDataScheme(CacheManager.SDCARD_DATASCHEME);
        registerReceiver(mSDcardBroadReceiver, filter);
    }


}
