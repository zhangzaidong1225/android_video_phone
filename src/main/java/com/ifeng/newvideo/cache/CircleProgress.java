package com.ifeng.newvideo.cache;

/**
 * @author guoxiaotian
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.DisplayUtils;


/**
 * 进度条
 */
public class CircleProgress extends View {

    private static final int DEFAULT_MAX_VALUE = 100; // 默认进度条最大值
    private static final int DEFAULT_PAINT_WIDTH = 10;
    private static final int DEFAULT_BOTTOMPAINT_WIDTH = 14; // 默认画笔宽度
    private static final int DEFAULT_PAINT_COLOR = 0x80ffffff; // 默认画笔颜色
    private static final boolean DEFAULT_FILL_MODE = true; // 默认填充模式
    private static final int DEFAULT_INSIDE_VALUE = 0; // 默认缩进距离

    private int mSaveMax;
    private int mMaxProgress = DEFAULT_MAX_VALUE; // 进度条最大值
    private int mMainCurProgress = 0; // 主进度条当前值
    private RectF mRoundOval; // 圆形所在矩形区域
    private RectF mRoundProgress; // 进度所在矩形区域
    private boolean mBRoundPaintsFill; // 是否填充以填充模式绘制圆形
    private int mSidePaintInterval; // 圆形向里缩进的距离
    private int mPaintWidth; // 圆形画笔宽度（填充模式下无视）
    private int mPaintColor; // 画笔颜色 （即主进度条画笔颜色，子进度条画笔颜色为其半透明值）
    private int mDrawPos; // 绘制圆形的起点（默认为-90度即12点钟方向）

    private Paint mMainPaints; // 主进度条画笔
    private Paint mBottomPaint;
    private Paint mBottomMianPaint;
    private Drawable mBackgroundPicture; // 背景图

    private int bttompaintWidth;

    public CircleProgress(Context context) {
        super(context);
        defaultParam();
    }

    public CircleProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        defaultParam();
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.Cache_CircleProgressBar);
        mMaxProgress = array.getInteger(R.styleable.Cache_CircleProgressBar_max, DEFAULT_MAX_VALUE); // 获取进度条最大值
        mBRoundPaintsFill = array.getBoolean(R.styleable.Cache_CircleProgressBar_fill, DEFAULT_FILL_MODE); // 获取填充模式
        int paintWidth = array.getInt(R.styleable.Cache_CircleProgressBar_Paint_Width, DEFAULT_PAINT_WIDTH); // 获取画笔宽度
        bttompaintWidth = DisplayUtils.convertDipToPixel(array.getInt(R.styleable.Cache_CircleProgressBar_Paint_Width, DEFAULT_BOTTOMPAINT_WIDTH));
        if (!mBRoundPaintsFill) {
            mMainPaints.setStrokeWidth(paintWidth);
            mBottomPaint.setStrokeWidth(bttompaintWidth);
            mBottomMianPaint.setStrokeWidth(paintWidth);
        }
        if (mBRoundPaintsFill) {
            mMainPaints.setStyle(Paint.Style.FILL);
            //mBottomPaint.setStyle(Paint.Style.FILL);
            //20141110改动
            mBottomPaint.setStyle(Paint.Style.STROKE);
            mBottomPaint.setStrokeWidth(bttompaintWidth);

            mBottomMianPaint.setStyle(Paint.Style.FILL);
        } else {
            mMainPaints.setStyle(Paint.Style.STROKE);
            mBottomPaint.setStyle(Paint.Style.STROKE);
            mBottomMianPaint.setStyle(Paint.Style.STROKE);
        }
        int paintColor = getResources().getColor(R.color.common_white);
        mMainPaints.setColor(paintColor);

        mSidePaintInterval = array.getInt(R.styleable.Cache_CircleProgressBar_Inside_Interval, DEFAULT_INSIDE_VALUE);// 圆环缩进距离

        array.recycle(); // 一定要调用，否则会有问题

    }

    /*
     * 默认参数
     */
    private void defaultParam() {
        mRoundOval = new RectF();
        mRoundProgress = new RectF();
        mBRoundPaintsFill = DEFAULT_FILL_MODE;
        mSidePaintInterval = DEFAULT_INSIDE_VALUE;
        mPaintWidth = 0;
        mPaintColor = DEFAULT_PAINT_COLOR;
        mDrawPos = -90;

        mMainPaints = new Paint();
        mMainPaints.setAntiAlias(true);
        mMainPaints.setStyle(Paint.Style.FILL);
        mMainPaints.setStrokeWidth(mPaintWidth);
        mMainPaints.setColor(mPaintColor);

        mBottomPaint = new Paint();
        mBottomPaint.setAntiAlias(true);
        mBottomPaint.setStyle(Paint.Style.FILL);
        mBottomPaint.setStrokeWidth(14);
        mBottomPaint.setColor(getResources().getColor(R.color.common_white));

        mBottomMianPaint = new Paint();
        mBottomMianPaint.setAntiAlias(true);
        mBottomMianPaint.setStyle(Paint.Style.FILL);
        mBottomMianPaint.setStrokeWidth(mPaintWidth);
        mBottomMianPaint.setColor(getResources().getColor(R.color.common_paintColor_bg));

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) { // 设置视图大小
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        mBackgroundPicture = getBackground();
        if (mBackgroundPicture != null) {
            width = mBackgroundPicture.getMinimumWidth();
            height = mBackgroundPicture.getMinimumHeight();
        }

        setMeasuredDimension(resolveSize(width, widthMeasureSpec), resolveSize(width, heightMeasureSpec));
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        autoFix(w, h);

    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mBackgroundPicture == null) { // 没背景图的话就绘制底色
//            mBottomPaint.setAlpha(50);
//            mBottomMianPaint.setAlpha(153);
//            canvas.drawArc(mRoundOval, 0, 360, true, mBottomPaint);
//            canvas.drawArc(mRoundOval, 0, 360, true, mBottomMianPaint);
            //20141110改动
            mBottomPaint.setAlpha(255);
            canvas.drawArc(mRoundOval, 0, 360, true, mBottomPaint);
        }
        if (mMainCurProgress > 0) {
            float rate = (float) mMainCurProgress / mMaxProgress;
            float sweep = 360 * rate;
//            mMainPaints.setAlpha(80);
//            canvas.drawArc(mRoundOval, mDrawPos, sweep, true, mMainPaints);
            //20141110改动
            mMainPaints.setAlpha(225);
            canvas.drawArc(mRoundProgress, mDrawPos + sweep, 360 - sweep, true, mMainPaints);
        }
    }

    /*
     * 设置主进度值
     */
    public synchronized void setCurProgress(int progress) {
        mMainCurProgress = progress;
        if (mMainCurProgress < 0) {
            mMainCurProgress = 0;
        }
        if (mMainCurProgress > mMaxProgress) {
            mMainCurProgress = mMaxProgress;
        }

        postInvalidate();// invalidate();
    }

    public synchronized int getCurProgress() {
        return mMainCurProgress;
    }

    public synchronized void setMaxProgress(int maxProgress) {
        if (maxProgress <= 0) {
            return;
        }
        mMaxProgress = maxProgress;
        if (mMainCurProgress > maxProgress) {
            mMainCurProgress = maxProgress;
        }
        mSaveMax = mMaxProgress;
        postInvalidate();
    }

    /*
     * 自动修正
     */
    private void autoFix(int w, int h) {
        if (mSidePaintInterval != 0) {
            mRoundOval.set(mPaintWidth / 2 + mSidePaintInterval, mPaintWidth / 2 + mSidePaintInterval, w - mPaintWidth / 2
                    - mSidePaintInterval, h - mPaintWidth / 2 - mSidePaintInterval);
            mRoundProgress.set(mPaintWidth / 2 + mSidePaintInterval + bttompaintWidth * 2, mPaintWidth / 2 + mSidePaintInterval + bttompaintWidth * 2,
                    w - mPaintWidth / 2 - mSidePaintInterval - bttompaintWidth * 2, h - mPaintWidth / 2 - mSidePaintInterval - bttompaintWidth * 2);
        } else {
            int sl = getPaddingLeft();
            int sr = getPaddingRight();
            int st = getPaddingTop();
            int sb = getPaddingBottom();

            mRoundOval.set(sl + mPaintWidth / 2, st + mPaintWidth / 2, w - sr - mPaintWidth / 2, h - sb - mPaintWidth / 2);
            mRoundProgress.set(sl + mPaintWidth / 2 + bttompaintWidth * 2, st + mPaintWidth / 2 + bttompaintWidth * 2,
                    w - sr - mPaintWidth / 2 - bttompaintWidth * 2, h - sb - mPaintWidth / 2 - bttompaintWidth * 2);
        }
    }

}
