package com.ifeng.newvideo.cache;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ifeng.video.core.download.DownloadInfo;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.download.Downloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yongq_000 on 2015/10/26.
 */
public class MemoryValueOverFlowReceiver extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(MemoryValueOverFlowReceiver.class);

    private final UnicomShowDialog unicomShowDialog;

    public MemoryValueOverFlowReceiver(
            UnicomShowDialog unicomShowDialog) {
        this.unicomShowDialog = unicomShowDialog;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        DownloadInfo downloadInfo = (DownloadInfo) intent.getSerializableExtra(DownloadOrder.ACTION_SHOW_UNICOM_OVERFLOW_DIALOG_NAME);
        logger.debug("--MemoryValueOverFlowReceiver--  onReceive = {}", action);
        if (action.equals(DownloadOrder.ACTION_SHOW_UNICOM_OVERFLOW_DIALOG)) {
            unicomShowDialog.onUnicomDialogCallback(downloadInfo);
        }
    }

    public interface UnicomShowDialog {
        void onUnicomDialogCallback(DownloadInfo downloadInfo);
    }

}

