package com.ifeng.newvideo.cache;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.activity.CacheAllActivity;
import com.ifeng.newvideo.cache.activity.CachingActivity;
import com.ifeng.newvideo.cache.adapter.CachingAdapter;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.domains.DownloadRecord;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.download.DownloadHelper;
import com.ifeng.video.core.download.DownloadInfo;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.download.DownloadQueue;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.FileUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.CacheVideoDao;
import com.ifeng.video.dao.db.model.CacheBaseModel;
import com.ifeng.video.dao.db.model.CacheFolderModel;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 缓存控制类
 * Created by cuihz on 2014/7/18.
 */
public class CacheManager {

    private static final Logger logger = LoggerFactory.getLogger(CacheManager.class);

    private static CacheAllActivity cacheAllActivity;
    private static CachingActivity cachingActivity;

    /**
     * 已下载完的列表
     */
    public static List<CacheBaseModel> mDownloadAllList;
    public static List<CacheBaseModel> mDownloadAllAudioList;
    public static List<CacheBaseModel> mDownloadAllVideoList;
    /**
     * 正在下载列表
     */
    public static List<CacheVideoModel> mDownloadingList;
    public static List<CacheVideoModel> mDownloadingAudioList;
    public static List<CacheVideoModel> mDownloadingVideoList;

    public static final String SDCARD_DATASCHEME = "file";
    public static final int MAX_RECOUNT = 2;

    /**
     * “已添加至缓存” toast上次弹出时间，用于防止多次弹出 *
     */
    private static long addToastLastTime = 0;

    private static NetSettingChangedListener netSettingChangedListener;
    private static StorageSettingChangedListener storageSettingChangedListener;

    public static final String CACHE_TYPE_VIDEO = "cache_type_video";
    public static final String CACHE_TYPE_AUDIO = "cache_type_audio";

    /**
     * @return 获取视频列表
     */
    public static synchronized void getDownloadVideos(Context context) {
        boolean hasNewData = false;
        logger.debug("CacheManager  ---  getDownloadVideos  mDownloadingList = null  is  {}  ,  size =  {}",
                mDownloadingList == null, mDownloadingList != null ? mDownloadingList.size() : -1);
        try {
            if (mDownloadingList == null) {
                hasNewData = true;
                mDownloadingList = new ArrayList<CacheVideoModel>();
                mDownloadingList = CacheVideoDao.getList(context, null, false, CacheVideoModel.CREATE_TIME, true);
                for (int i = 0; i < mDownloadingList.size(); i++) {
                    CacheVideoModel cacheVideoModel = mDownloadingList.get(i);
                    if (cacheVideoModel.getState() == DownloadOrder.STATE_STOP) {
                        mDownloadingList.remove(cacheVideoModel);
                        try {
                            CacheUtil.getDownloadQueue(context).changeStateById(cacheVideoModel.getId(), DownloadOrder.STATE_STOP);
                        } catch (Exception e) {
                            logger.error("delete download failed changeStateById() \n", e);
                        }
                        try {
                            CacheVideoDao.delete(context, cacheVideoModel.getId());
                        } catch (Exception e) {
                            logger.error("delete download failed delete() \n", e);
                        }
                        i--;
                        continue;
                    }
                    CacheManager.DownHandler handler = (CacheManager.DownHandler) CacheUtil.getDownloadQueue(context).getDownloadHandlerById(cacheVideoModel.getId());
                    if (handler == null) {
                        handler = new CacheManager.DownHandler(cacheVideoModel.getId(), null);
                        CacheUtil.getDownloadQueue(context).registerDownloadHandler(cacheVideoModel.getId(), handler);
                    }
                }
            }
            if (mDownloadingAudioList == null || mDownloadingVideoList == null) {
                mDownloadingAudioList = new ArrayList<CacheVideoModel>();
                mDownloadingVideoList = new ArrayList<CacheVideoModel>();
                for (CacheVideoModel cacheVideoModel : mDownloadingList) {
                    cacheVideoModel.setCheckState(0);//默认为未选中模式
                    if (cacheVideoModel.getType().equals(IfengType.TYPE_AUDIO)) {
                        mDownloadingAudioList.add(cacheVideoModel);
                    }
                    if (cacheVideoModel.getType().equals(IfengType.TYPE_VIDEO)) {
                        mDownloadingVideoList.add(cacheVideoModel);
                    }
                }
            }

            if (mDownloadAllAudioList == null) {
                mDownloadAllAudioList = new ArrayList<CacheBaseModel>();
            }
            if (mDownloadAllVideoList == null) {
                mDownloadAllVideoList = new ArrayList<CacheBaseModel>();
            }
            if (mDownloadAllList == null) {
                hasNewData = true;
                mDownloadAllList = new ArrayList<CacheBaseModel>();

                //获取单条已下载视频列表
                Map<String, String> params = new HashMap<String, String>();
                params.put(CacheVideoModel.FOLDER_ID, "0");
                List<CacheVideoModel> videoList = CacheVideoDao.getList(context, params, true, CacheVideoModel.DOWN_OVER_TIME, false);
                mDownloadAllList.addAll(checkFileExist(videoList));
                if (videoList != null && videoList.size() > 0) {
                    for (CacheVideoModel cacheVideoModel : videoList) {
                        if (cacheVideoModel.getType().equals(IfengType.TYPE_AUDIO)) {
                            mDownloadAllAudioList.add(cacheVideoModel);
                        }
                        if (cacheVideoModel.getType().equals(IfengType.TYPE_VIDEO)) {
                            mDownloadAllVideoList.add(cacheVideoModel);
                        }
                    }

                }
            }

        } catch (Exception e) {
            logger.error("get download list failed \n", e);
        } finally {
            if (hasNewData) {
                refreshUI();
            }
        }
    }


    /**
     * 获取该专题类的符合条件的视频
     *
     * @param context
     * @param isDowned 是否已完成
     * @param folderId 文件夹的唯一标识
     * @return
     */
    public static List<CacheVideoModel> getFolderVideos(Context context, boolean isDowned, int folderId) {
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put(CacheVideoModel.FOLDER_ID, folderId + "");
            List<CacheVideoModel> videoList = CacheVideoDao.getList(context, params, isDowned, CacheVideoModel.VIDEO_CREATE_TIME, false);
            return checkFileExist(videoList);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return null;
    }

    /**
     * 获取该专题类的符合条件的音频
     *
     * @param context
     * @param folderId 文件夹的唯一标识
     * @return
     */
    public static List<CacheVideoModel> getFolderVideos(Context context, int folderId) {
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put(CacheVideoModel.FOLDER_ID, folderId + "");
            List<CacheVideoModel> videoList = CacheVideoDao.getList(context, params, false, CacheVideoModel.VIDEO_CREATE_TIME, false);
            return videoList;
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return null;
    }


    /**
     * 强制刷新列表
     * 用于删除视频后，清空list重新去数据库中数据，防止出现已删除视频
     *
     * @param context
     */
    public static void refreshDownloadVideos(Context context) {
        if (mDownloadingList != null) {
            mDownloadingList.clear();
        }
        if (mDownloadingAudioList != null) {
            mDownloadingAudioList.clear();
        }
        if (mDownloadingVideoList != null) {
            mDownloadingVideoList.clear();
        }


        if (mDownloadAllList != null) {
            mDownloadAllList.clear();
        }
        if (mDownloadAllAudioList != null) {
            mDownloadAllAudioList.clear();
        }
        if (mDownloadAllVideoList != null) {
            mDownloadAllVideoList.clear();
        }
        mDownloadingList = null;
        mDownloadingAudioList = null;
        mDownloadingVideoList = null;

        mDownloadAllList = null;
        mDownloadAllAudioList = null;
        mDownloadAllVideoList = null;
        getDownloadVideos(context);
    }


    /**
     * 去除文件路径有问题的，用于sd卡插拔
     *
     * @param cacheVideoModels
     * @return
     */
    private static List<CacheVideoModel> checkFileExist(List<CacheVideoModel> cacheVideoModels) {
        for (int i = 0; i < cacheVideoModels.size(); i++) {
            if (!DownloadHelper.isFileDownloaded(cacheVideoModels.get(i).getPath())) {
                cacheVideoModels.remove(i);
                i--;
            }
        }
        return cacheVideoModels;
    }

    /**
     * 获取全部下载完成的视频或音频
     *
     * @param isSelectedAudio 是否是音频
     */
    public static List<CacheBaseModel> getDownloadAllList(boolean isSelectedAudio) {
        if (isSelectedAudio) {
            return mDownloadAllAudioList;
        } else {
            return mDownloadAllVideoList;
        }
    }

    /**
     * 获取全部正在下载的视频或音频
     *
     * @param isSelectedAudio 是否是音频
     */
    public static List<CacheVideoModel> getDownloadingList(boolean isSelectedAudio) {
        if (isSelectedAudio) {
            return mDownloadingAudioList;
        } else {
            return mDownloadingVideoList;
        }
    }

    /**
     * @param context
     * @param type    Audio / video /all
     * @return
     */
    public static boolean isCaching(Context context, String type) {
        List<CacheVideoModel> mCurrentDownloadingList;
        getDownloadVideos(context);
        if (IfengType.TYPE_AUDIO.equals(type)) {
            mCurrentDownloadingList = mDownloadingAudioList;
        } else if (IfengType.TYPE_VIDEO.equals(type)) {
            mCurrentDownloadingList = mDownloadingVideoList;
        } else {
            mCurrentDownloadingList = mDownloadingList;
        }

        for (CacheVideoModel cacheVideoModel : mCurrentDownloadingList) {
            if (cacheVideoModel.getState() == DownloadOrder.STATE_DOWNING) {
                return true;
            }
            if (cacheVideoModel.getState() == DownloadOrder.STATE_WAIT_DOWN) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCaching4NetReceiver(Context context) {
        getDownloadVideos(context);
        for (CacheVideoModel cacheVideoModel : mDownloadingList) {
            if (cacheVideoModel.getState() == DownloadOrder.STATE_DOWNING) {
                return true;
            }
            if (cacheVideoModel.getState() == DownloadOrder.STATE_WAIT_DOWN) {
                return true;
            }
            if (cacheVideoModel.getState() == DownloadOrder.STATE_FAILED) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAllCaching(Context context, boolean isSelectedAudio) {
        getDownloadVideos(context);
        if (isSelectedAudio) {
            for (CacheVideoModel cacheVideoModel : mDownloadingAudioList) {
                if ((cacheVideoModel.getState() != DownloadOrder.STATE_DOWNING) && (cacheVideoModel.getState() != DownloadOrder.STATE_WAIT_DOWN)) {
                    return false;
                }
            }
        } else {
            for (CacheVideoModel cacheVideoModel : mDownloadingVideoList) {
                if ((cacheVideoModel.getState() != DownloadOrder.STATE_DOWNING) && (cacheVideoModel.getState() != DownloadOrder.STATE_WAIT_DOWN)) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * 该条视频是否已经缓存
     *
     * @param guid
     * @return
     */
    public static boolean isInCache(Context context, String guid) {
        try {
            int id = CacheUtil.getIdFromGuid(IfengType.TYPE_VIDEO, guid);
            CacheVideoModel cacheVideoModel = CacheVideoDao.get(context, id);
            return CacheVideoDao.has(context, id) && !(!FileUtils.isFileExist(cacheVideoModel.getPath()) && cacheVideoModel.getState() == DownloadOrder.STATE_SUCCESS);
        } catch (Exception e) {
            logger.error(e.toString(), e);
            return false;
        }
    }


    /**
     * 获取缓存数目，包括已完成和未完成
     *
     * @param context
     * @return int
     */
    public static int getCacheCount(Context context) {
        getDownloadVideos(context);
        return mDownloadingList.size() + getCachedCount();
    }

    private static int getCachedCount() {
        int count = 0;
        for (CacheBaseModel cacheBaseModel : mDownloadAllList) {
            if (cacheBaseModel instanceof CacheVideoModel) {
                count++;
            } else {
                count += ((CacheFolderModel) cacheBaseModel).getFileCount();
            }
        }
        return count;
    }


    /**
     * 添加单条至缓存
     *
     * @param context          上下文对象
     * @param down             缓存的playerInfo
     * @param state            下载状态,如 DownloadOrder.STATE_SUCCESS
     * @param stream           视频码流，如 MediaConstants.STREAM_MID ， 高清/标清
     * @param type             资源类型，如 video/audio，取值为：IfengType.TYPE_AUDIO,IfengType.TYPE_VIDEO
     * @param cacheFolderModel 传null即可
     */
    public synchronized static void addDownload(Context context, PlayerInfoModel down, int state, int stream, String type, CacheFolderModel cacheFolderModel) {
        List<PlayerInfoModel> downList = new ArrayList<PlayerInfoModel>();
        downList.add(down);
        addDownloadList(context, downList, state, stream, type, cacheFolderModel);
    }


    /**
     * 添加列表至缓存
     *
     * @param context
     * @param downList 缓存的playerInfo列表
     * @param state    缓存的初始状态
     */
    public synchronized static void addDownloadList(Context context, List<PlayerInfoModel> downList, int state, int stream, String type, CacheFolderModel cacheFolderModel) {
        AddCacheAsyncTask addCacheAsyncTask = new AddCacheAsyncTask(context, downList, state, stream, type, cacheFolderModel);
        addCacheAsyncTask.execute();
        for (PlayerInfoModel playerInfoModel : downList) {
            String audioPicUrl = playerInfoModel.poster_url_big;
            if (StringUtils.isBlank(audioPicUrl)) {
                continue;
            }
            VolleyHelper.getImageLoader().get(audioPicUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    logger.debug("Load audio image success  bitmap = {}", response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error("Load audio image fail {}", error.getMessage());
                }
            });
        }
    }

    private static class AddCacheAsyncTask extends AsyncTask<Void, Void, Void> {
        private final Context context;
        private final List<PlayerInfoModel> downList;
        private final int state;
        private final int stream;
        private final String type;
        private final CacheFolderModel cacheFolderModel;
        private List<CacheVideoModel> cacheVideoModels;

        public AddCacheAsyncTask(Context context, List<PlayerInfoModel> downList, int state, int stream, String type, CacheFolderModel cacheFolderModel) {
            this.context = context;
            this.downList = downList;
            this.state = state;
            this.stream = stream;
            this.type = type;
            this.cacheFolderModel = cacheFolderModel;
        }

        @Override
        protected void onPreExecute() {
            getDownloadVideos(context);
        }

        @Override
        protected Void doInBackground(Void... params) {
            logger.debug("add cache task start");
            cacheVideoModels = new ArrayList<CacheVideoModel>();
            List<DownloadInfo> downloadInfos = new ArrayList<DownloadInfo>();
            for (PlayerInfoModel playerInfoModel : downList) {
                if (playerInfoModel == null) {
                    continue;
                }
                String path = CacheUtil.getDownloadPath(context, playerInfoModel.guid);
                CacheVideoModel cacheVideoModel = CacheUtil.playInfoToCacheVideoModel(playerInfoModel, state, stream, type, path);
                cacheVideoModel.setCreateTime(System.currentTimeMillis());
                if (cacheFolderModel != null) {
                    cacheVideoModel.setFolderId(cacheFolderModel.getId());
                }

                //统计
                String page = PageActionTracker.getPageName(false, context);
                cacheVideoModel.setPageId(page);
                DownloadRecord record = new DownloadRecord(cacheVideoModel.getGuid(), cacheVideoModel.getName(), "start", cacheVideoModel.getPageId());
                CustomerStatistics.sendDownload(record);

                cacheVideoModels.add(cacheVideoModel);
                DownloadInfo downloadInfo = CacheUtil.cacheVideoModelToDownloadInfo(cacheVideoModel);
                downloadInfos.add(downloadInfo);
                DownloadQueue queue = CacheUtil.getDownloadQueue(context);
                queue.registerDownloadHandler(downloadInfo.getId(), new DownHandler(cacheVideoModel.getId(), cacheFolderModel));

            }
            try {
                CacheUtil.getDownloadQueue(context).addDownloadByList(downloadInfos);
                CacheVideoDao.saveList(context, cacheVideoModels);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            } finally {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            mDownloadingList.addAll(cacheVideoModels);
            for (CacheVideoModel cacheVideoModel : mDownloadingList) {
                if (cacheVideoModel.getType().equals(IfengType.TYPE_AUDIO)) {
                    mDownloadingAudioList.add(cacheVideoModel);
                }
                if (cacheVideoModel.getType().equals(IfengType.TYPE_VIDEO)) {
                    mDownloadingVideoList.add(cacheVideoModel);
                }
            }
            if (System.currentTimeMillis() - addToastLastTime > 5000) {
                addToastLastTime = System.currentTimeMillis();
                if (NetReceiver.netDealManager != null) {
                    NetReceiver.netDealManager.initDialogState();
                }
                if (NetUtils.isNetAvailable(context) && !NetUtils.isMobile(context)) {
                    ToastUtils.getInstance().showLongToast(context.getString(R.string.common_already_to_download));
                }
            }
            refreshUI();
            logger.debug("add cache task end");
        }
    }


    public static void setCacheAllActivity(CacheAllActivity mCacheAllActivity) {
        cacheAllActivity = mCacheAllActivity;
    }

    public static void setCachingActivity(CachingActivity mCachingActivity) {
        cachingActivity = mCachingActivity;
    }


    /** ----------------------下载回调处理开始------------------------ **/

    /**
     * 下载Handler类，处理下载事件
     */
    public static class DownHandler extends Handler {
        private CachingAdapter.DownViewHolder viewHolder;
        private final int id;
        private final CacheFolderModel mCacheFolderModel;

        public DownHandler(int id, CacheFolderModel cacheFolderModel) {
            super(Looper.getMainLooper());
            this.id = id;
            mCacheFolderModel = cacheFolderModel;
        }

        public void setHolder(CachingAdapter.DownViewHolder holder) {
            this.viewHolder = holder;
        }

        @Override
        public void handleMessage(Message msg) {
            CacheVideoModel info = getCacheVideoModelFromCachingList(id);
            if (info == null) {
                return;
            }
            switch (msg.what) {
                case DownloadOrder.STATE_WAIT_DOWN:
                    boolean isMobileOpen = SharePreUtils.getInstance().getCacheVideoState();
                    if (viewHolder != null && viewHolder.state_text != null) {
                        if (NetUtils.isNetAvailable(IfengApplication.getInstance())
                                && NetUtils.isMobile(IfengApplication.getInstance())
                                && !isMobileOpen) {
                            viewHolder.state_text.setText(IfengApplication.getInstance().getString(R.string.cache_only_wifi));
                            viewHolder.state_text.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.cache_state_text_color));
                        } else {
                            viewHolder.state_text.setText(IfengApplication.getInstance().getString(R.string.cache_wait));
                            viewHolder.state_text.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.cache_state_text_color));
                        }
                    }
                    break;
                case DownloadOrder.STATE_DOWNING:
                    if (viewHolder != null && viewHolder.state_text != null) {
                        viewHolder.state_text.setText(IfengApplication.getInstance().getString(R.string.cache_caching));
                        viewHolder.state_text.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.cache_state_text_color));
                        viewHolder.changeProgress(msg.arg1, info.getTotalSize());
                    }
                    info.setProgress(msg.arg1);
                    break;
                case DownloadOrder.STATE_SUCCESS:
                    downSuccess(info);
                    break;
                case DownloadOrder.STATE_PAUSE:
                case DownloadOrder.STATE_FAILED:
                    if (viewHolder != null && viewHolder.state_text != null) {
                        viewHolder.state_text.setText(IfengApplication.getInstance().getString(R.string.cache_pause));
                        viewHolder.state_text.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.cache_state_text_color));
                    }
                    if (msg.arg1 != 0) {
                        preDownFailed(msg.arg1, String.valueOf(msg.obj), info);
                    }
                    break;
            }
            info.setState(msg.what);
            try {
                CacheVideoDao.save(IfengApplication.getInstance(), info);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
            refreshCachingView();
        }
    }


    private static CacheVideoModel getCacheVideoModelFromCachingList(int id) {
        for (CacheVideoModel cacheVideoModel : mDownloadingList) {
            if (cacheVideoModel.getId() == id) {
                return cacheVideoModel;
            }
        }
        return null;
    }


    /**
     * 准备下载失败后的处理
     *
     * @param
     * @param code
     */
    private static void preDownFailed(int code, String errorInfo, CacheVideoModel info) {
        switch (code) {
            case DownloadOrder.FAILED_URL_UNCONNECT:
                ToastUtils.getInstance().showLongToast(R.string.cache_url_error);
                break;
            case DownloadOrder.FAILED_STORAGE_NOT_ENOUPH:
                ToastUtils.getInstance().showLongToast(R.string.cache_not_enough_room);
                break;
            case DownloadOrder.FAILED_SDCARD_UNMOUNT:
                ToastUtils.getInstance().showLongToast(R.string.cache_no_sdcard);
                break;
            case DownloadOrder.FAILED_SIZE_ERROR:
                int recount = info.getRecount();
                recount++;
                info.setRecount(recount);
                if (recount == MAX_RECOUNT) {
                    UserFeed.sendDownLoadFailInfo(getFeedInfo(info, errorInfo));
                }
                break;
        }
    }

    private static String getFeedInfo(CacheVideoModel info, String errorInfo) {
        String feedInfo = "realSize:fileSize=%s, guid=%s, type=%s, url=%s, path=%s";
        feedInfo = String.format(feedInfo, errorInfo, info.getGuid(), info.getType(), info.getDownUrl(), info.getPath());
        return feedInfo;
    }

    /**
     * 下载成功
     *
     * @param
     */
    private static void downSuccess(CacheVideoModel cacheVideoModel) {
        logger.info("{} down success", cacheVideoModel.getName());
        int index;
        if (mDownloadingAudioList != null && mDownloadingAudioList.contains(cacheVideoModel)) {
            index = mDownloadingAudioList.indexOf(cacheVideoModel);
            mDownloadingAudioList.remove(index);
        } else if (mDownloadingVideoList != null && mDownloadingVideoList.contains(cacheVideoModel)) {
            index = mDownloadingVideoList.indexOf(cacheVideoModel);
            mDownloadingVideoList.remove(index);

        }
        if (mDownloadingList != null) {
            logger.debug("开始处理下载完成");
            index = mDownloadingList.indexOf(cacheVideoModel);
            if (index != -1) {
                mDownloadingList.remove(index);
                addToDowned(cacheVideoModel);
                cacheVideoModel.setDownOverTime(System.currentTimeMillis());

                if (IfengType.TYPE_VIDEO.equals(cacheVideoModel.getType())) {
                    AudioCacheManager.getAudioFromVideo(cacheVideoModel);
                }

                //统计
                DownloadRecord record = new DownloadRecord(cacheVideoModel.getGuid(), cacheVideoModel.getName(), "end", cacheVideoModel.getPageId());
                CustomerStatistics.sendDownload(record);
            }
            refreshUI();
        }
    }

    /**
     * 下载完成后添加进已下载列表
     *
     * @param
     */
    private static void addToDowned(CacheVideoModel cacheVideoModel) {
        if (cacheVideoModel.getFolderId() == 0) {
            if (!mDownloadAllList.contains(cacheVideoModel)) {
                mDownloadAllList.add(cacheVideoModel);
                cacheVideoModel.setCheckState(0);//状态设置为未选中状态
                if (cacheVideoModel.getType().equals(IfengType.TYPE_AUDIO)) {
                    mDownloadAllAudioList.add(0, cacheVideoModel);
                } else if (cacheVideoModel.getType().equals(IfengType.TYPE_VIDEO)) {
                    mDownloadAllVideoList.add(0, cacheVideoModel);//插入到第一个位置
                }
            }
        }
    }


    /**
     * ----------------------下载回调处理结束------------------------ *
     */
    private static void refreshUI() {
        if (cacheAllActivity != null) {
            cacheAllActivity.refreshUI();
        }
        if (cachingActivity != null) {
            cachingActivity.refreshUI();
        }
    }

    private static void refreshCachingView() {
        if (cacheAllActivity != null) {
            cacheAllActivity.updateCachingView();
        }
    }

    /**
     * 下载和缓存数据库同步
     * 删除出错的时候立刻进行同步，防止出现错误数据
     *
     * @param context
     */
    public static void synchronizeDB(Context context) {
        logger.debug("删除出错，开始进行数据同步");
        try {
            List<CacheVideoModel> cacheVideoModels = new ArrayList<CacheVideoModel>();
            cacheVideoModels.addAll(CacheVideoDao.getList(context, null, false, null, false));
            cacheVideoModels.addAll(CacheVideoDao.getList(context, null, true, null, false));
            DownloadQueue queue = CacheUtil.getDownloadQueue(context);
            for (CacheVideoModel cacheVideoModel : cacheVideoModels) {
                if (!queue.hasDownload(cacheVideoModel.getId())) {
                    CacheVideoDao.delete(context, cacheVideoModel.getId());
                }
            }
        } catch (Exception e) {
            logger.error("同步数据也出错了。。。。", e);
        }
    }


    /**
     * 获取离线播放地址
     *
     * @return
     */
    public static String getPlayPathForOffline(Context context, String guid) throws Exception {
        CacheVideoModel cacheVideoModel = CacheVideoDao.get(context, CacheUtil.getIdFromGuid(IfengType.TYPE_VIDEO, guid));
        if (cacheVideoModel != null) {
            return cacheVideoModel.getPath();
        } else {
            throw new SQLException("guid is error");
        }
    }


    public static String getAudioPath(Context context, String guid) throws Exception {
        CacheVideoModel cacheVideoModel = CacheVideoDao.get(context, CacheUtil.getIdFromGuid(IfengType.TYPE_VIDEO, guid));
        if (cacheVideoModel != null) {
            String audioPath = AudioCacheManager.getAudioPath(cacheVideoModel.getPath(), cacheVideoModel.getType());
            if (FileUtils.isFileExist(audioPath)) {
                return audioPath;
            } else {
                return "";
            }
        } else {
            throw new SQLException("guid is error");
        }
    }

    /**
     * 暂停正在缓存的视频
     *
     * @param context
     */
    public static void pauseCaching(Context context) {
        try {
            List<CacheVideoModel> list = CacheVideoDao.getList(context, null, false, null, false);
            for (CacheVideoModel cacheVideoModel : list) {
                cacheVideoModel.setState(DownloadOrder.STATE_PAUSE);
            }
            logger.debug("list.toString  ::: {}", list.toString());
            CacheVideoDao.saveList(context, list);
            CacheUtil.getDownloadQueue(context).pauseDownloading(context, DownloadOrder.STATE_PAUSE);
        } catch (Exception e) {
            logger.error("pauseCaching error ！  DownloadDBException ::::: {}", e.toString());
        }
    }

    /**
     * 当进程被异常Kill之后，将状态为“正在缓存”但未加入下载队列中的资源状态改为“等待缓存”
     *
     * @param context
     */
    public static void waitingCaching(Context context) {
        try {
            List<CacheVideoModel> list = CacheVideoDao.getList(context, null, false, null, false);
            for (CacheVideoModel cacheVideoModel : list) {
                if (cacheVideoModel.getState() == DownloadOrder.STATE_DOWNING) {
                    cacheVideoModel.setState(DownloadOrder.STATE_WAIT_DOWN);
                }
            }
            logger.debug("list.toString  ::: {}", list.toString());
            CacheVideoDao.saveList(context, list);
            CacheUtil.getDownloadQueue(context).pauseDownloading(context, DownloadOrder.STATE_WAIT_DOWN);
        } catch (Exception e) {
            logger.error("waitingCaching error ！  DownloadDBException ::::: {}", e.toString());
        }
    }

    /**
     * 应用启动时开启缓存中的任务
     *
     * @param context
     */
    public static void startCaching(Context context) {
        getDownloadVideos(context);
        CacheUtil.getDownloadQueue(context).hold(false);
    }

    /**
     * 应用关闭时停止缓存中的任务
     *
     * @param context
     */
    public static void stopCaching(Context context) {
        CacheUtil.getDownloadQueue(context).hold(true);
    }

    public static NetSettingChangedListener getNetSettingChangedListener() {
        if (netSettingChangedListener == null) {
            netSettingChangedListener = new NetDealManager();
        }
        return netSettingChangedListener;
    }

    public static StorageSettingChangedListener getStorageSettingChangedListener() {
        if (storageSettingChangedListener == null) {
            storageSettingChangedListener = new SDcardReceiver();
        }
        return storageSettingChangedListener;
    }
}
