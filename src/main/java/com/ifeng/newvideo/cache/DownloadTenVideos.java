package com.ifeng.newvideo.cache;

import android.app.Activity;
import android.text.TextUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.activity.CacheAllActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.FileUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.core.DBHelper;
import com.ifeng.video.dao.db.dao.CacheDao;
import com.ifeng.video.dao.db.dao.CacheVideoDao;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.db.model.HomePageModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.util.CacheUtil;
import com.j256.ormlite.dao.Dao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 精选一键缓存
 * Created by cuihz on 2014/7/31.
 */
public class DownloadTenVideos {

    private static final Logger logger = LoggerFactory.getLogger(DownloadTenVideos.class);
    private static int HOME_MODULE_COUNT = 2; //缓存首页列表数据中模块个数 (取前两个模块，并且去除非“video”类型数据)
    private static int threadCompleteCount;
    private static int cacheDataCount;
    private static List<PlayerInfoModel> tenVideos;
    private static CacheAllActivity activity;
    private static boolean hasDifferentCacheData = false;

    static void downloadTenVideos(CacheAllActivity mActivity, final int state) {
        logger.debug("down ten videos now");
        activity = mActivity;
        tenVideos = new ArrayList<PlayerInfoModel>();
        CacheDao.getCacheVideos(
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        try {
                            parseCacheResponse(state, response);
                        } catch (Exception error) {
                            handleOnError(error);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleOnError(error);
                    }
                });
    }

    private static void handleOnError(Exception error) {
        CacheAllActivity.tenDownloading = false;
        showOneButtonDialog(activity, activity.getString(R.string.cache_no_refresh_choice_down));
        if (error.toString() != null) {
            logger.error(error.toString());
        }
    }

/*    private static void parseResponse(int state, String response) {
        JSONObject responseJson = JSONObject.parseObject(response);
        JSONArray bodyList = responseJson.getJSONArray("bodyList");
        int count = 0;
        for (int i = 0; i < bodyList.size(); i++) {
            JSONObject bodyObj = bodyList.getJSONObject(i);
            JSONArray videoList = bodyObj.getJSONArray("videoList");
            for (int j = 0; j < videoList.size(); j++) {
                if (count >= MAX_COUNT) {
                    if (atomicCount.get() <= 0) {
                        showOneButtonDialog(activity, activity.getString(R.string.cache_no_refresh_choice_down));
                        CacheAllActivity.tenDownloading = false;
                    }
                    return;
                }
                JSONObject videoObj = videoList.getJSONObject(j);
                if (videoObj.getString("memberType").equals(IfengType.TYPE_VIDEO)) {
                    JSONObject memberItem = videoObj.getJSONObject("memberItem");
                    if (!CacheManager.isInCache(activity, memberItem.getString("guid"))) {
                        getPlayInfoById(state, videoObj.getString("memberId"));
                    } else {
                        atomicCount.decrementAndGet();
                    }
                    count++;
                }
            }
        }
    }*/

    private static void parseCacheResponse(int state, Object response) {
        if (response == null) {
            return;
        }
        HomePageModel homePageModel = (HomePageModel) response;
        HomePageModel.BodyRecommend bodyRecommend = homePageModel.getBodyRecommend();
        if (bodyRecommend == null) {
            return;
        }
        List<HomePageModel.BodyRecommend.RecommendList> recommendList = bodyRecommend.getRecommendList();
        if (recommendList == null) {
            return;
        }

        Dao<CacheVideoModel, Integer> dao;
        threadCompleteCount = 0;
        cacheDataCount = 0;
        int size = recommendList.size();
        HOME_MODULE_COUNT = size > 2 ? HOME_MODULE_COUNT : size;
        DBHelper helper = DBHelper.getHelper(activity);
        try {
            dao = helper.getCacheVideoDao();
            for (int i = 0; i < HOME_MODULE_COUNT; i++) {
                List<HomePageModel.BodyRecommend.RecommendList.GroupList> groupList = recommendList.get(i).getGroupList();
                if (groupList == null) {
                    return;
                }
                for (int j = 0; j < groupList.size(); j++) {
//                if (threadCompleteCount != 0 && cacheDataCount == threadCompleteCount) {
//                showOneButtonDialog(activity, activity.getString(R.string.cache_no_refresh_choice_down));
//                CacheAllActivity.tenDownloading = false;
//                    return;
//                }
                    if (groupList.get(j).getMemberType().equals(IfengType.TYPE_VIDEO)) {
                        HomePageModel.MemberItem memberItem = groupList.get(j).getMemberItem();
                        if (memberItem == null) {
                            return;
                        }
                        int idFromGuid = CacheUtil.getIdFromGuid(IfengType.TYPE_VIDEO, memberItem.getGuid());
                        CacheVideoModel cacheVideoModel = CacheVideoDao.get(activity, idFromGuid);
                        if (!(dao.idExists(idFromGuid) && !(!FileUtils.isFileExist(cacheVideoModel.getPath()) && cacheVideoModel.getState() == DownloadOrder.STATE_SUCCESS))) {
                            cacheDataCount++;
                            getPlayInfoById(state, groupList.get(j).getMemberId());
                        } else {
                            ;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("DownLoad DB Exception = {}", e.toString());
        } finally {
            helper.close();
        }


        if (cacheDataCount == 0) {
            showOneButtonDialog(activity, activity.getString(R.string.cache_no_refresh_choice_down));
            CacheAllActivity.tenDownloading = false;

        }
    }

    private static void getPlayInfoById(final int state, String id) {
        CacheDao.getVideoInfoByMemberId(id,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            isOver(state);
                            return;
                        }
                        PlayerInfoModel playerInfoModel = new PlayerInfoModel();
                        playerInfoModel = playerInfoModel.parsesJson(response.toString());
                        if (playerInfoModel != null) {
                            tenVideos.add(playerInfoModel);
                        }
                        isOver(state);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isOver(state);
                        if (error.toString() != null) {
                            logger.error(error.toString());
                        }
                    }
                });
    }


    private static void isOver(int state) {
        threadCompleteCount++;
        if (cacheDataCount == threadCompleteCount) {
            //根据Guid进行数据去重
            for (int i = 0; i < tenVideos.size(); i++) {
                for (int j = i + 1; j < tenVideos.size(); j++) {
                    if (tenVideos.get(i).getGuid().equals(tenVideos.get(j).getGuid())) {
                        tenVideos.remove(j);
                        j--;
                    }
                }
            }
            CacheManager.addDownloadList(activity, tenVideos, state, MediaConstants.STREAM_HIGH, IfengType.TYPE_VIDEO, null);
            IntentUtils.startCachingActivity(activity, IfengType.TYPE_VIDEO);
            CacheAllActivity.tenDownloading = false;
        }
    }


    /**
     * 一个按钮“我知道了”提示框
     *
     * @param message
     */
    private static void showOneButtonDialog(Activity activity, String message) {
        if (activity != null && !activity.isFinishing()) {
            AlertUtils.getInstance().showOneBtnDialog(activity, message, activity.getString(R.string.cache_i_know), null);
        }
    }

}
