package com.ifeng.newvideo.cache;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.activity.CacheAllActivity;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.download.DownloadHelper;
import com.ifeng.video.core.download.DownloadInfo;
import com.ifeng.video.core.utils.StorageUtils;
import com.ifeng.video.dao.db.dao.CacheVideoDao;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * sd卡监听类
 */
public class SDcardReceiver extends BroadcastReceiver implements StorageSettingChangedListener {

    private static final Logger logger = LoggerFactory.getLogger(CacheAllActivity.class);
    /**
     * progressBar最大进度
     */
    private static final int MAX_PROGRESS = 100;
    /**
     * 暂时用于卸装载sd卡延迟
     */
    private static final int SDCARD_DELAY = 3000;
    /**
     * 存储卡容量信息进度条
     */
    private ProgressBar mSDcardCapacityProgressBar;
    /**
     * 存储卡容量信息
     */
    private TextView mSDcardCapacityText;

    public SDcardReceiver() {
    }

    public SDcardReceiver(Context context, ProgressBar mSDcardCapacityProgressBar, TextView mSDcardCapacityText) {
        this.mSDcardCapacityText = mSDcardCapacityText;
        this.mSDcardCapacityProgressBar = mSDcardCapacityProgressBar;
        capacityInit(context);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        logger.info("Receive SDCard Mount/UnMount!");
        try {
            Thread.sleep(SDCARD_DELAY);
        } catch (InterruptedException e) {
            logger.error(e.toString(), e);
        }
        capacityInit(context);
        CacheManager.refreshDownloadVideos(context);
    }

    private void capacityInit(Context context) {
        mSDcardCapacityProgressBar.setMax(MAX_PROGRESS);
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        boolean cache_sd = sharePreUtils.getCachePathState();
        if (cache_sd) {
            initSdCardCapacity(context);
            sharePreUtils.setCachePathState(true);
        } else {
            initInnerCardCapacity(context);
            sharePreUtils.setCachePathState(false);
        }
    }

    private void initInnerCardCapacity(Context context) {
        float innerTotalSize = StorageUtils.getInstance().getTotalInternalMemorySize() / 1024 / 1024 / 1024f;// 单位G
        // 总空间
        float innerAvailSize = StorageUtils.getInstance().getAvailableInternalMemorySize() / 1024 / 1024 / 1024f;// 单位G
        // 总空间
        mSDcardCapacityProgressBar.setProgress((int) ((Math.abs(innerTotalSize - innerAvailSize) / innerTotalSize) * MAX_PROGRESS));
        mSDcardCapacityText.setText(context.getString(R.string.cache_sd_content) + String.format("%.2f", innerTotalSize)
                + context.getString(R.string.cache_available_size) + String.format("%.2f", innerAvailSize) + "G");
    }

    private void initSdCardCapacity(Context context) {
        if (StorageUtils.getInstance().isExternalMemoryAvailable()) {
            float availablesize = StorageUtils.getInstance().getAvailableSdCardMemorySize() / 1024 / 1024 / 1024f;// 单位G
            float totalSize = StorageUtils.getInstance().getTotalSdCardMemorySize() / 1024 / 1024 / 1024f;// 单位G
            mSDcardCapacityProgressBar.setProgress((int) ((Math.abs(totalSize - availablesize) / totalSize) * MAX_PROGRESS));
            mSDcardCapacityText.setText(context.getString(R.string.cache_sd_content) + String.format("%.2f", totalSize)
                    + context.getString(R.string.cache_available_size) + String.format("%.2f", availablesize) + "G");
        } else {
            mSDcardCapacityText.setText(context.getString(R.string.cache_please_insert_sdcard));
        }
    }

    @Override
    public void onStorageSettingChanged(Context context) {
        logger.debug("storage setting changed");
        List<DownloadInfo> downloadInfos = new ArrayList<DownloadInfo>();
        for (CacheVideoModel cacheVideoModel : CacheManager.mDownloadingList) {
            if (!DownloadHelper.isFileDownloading(cacheVideoModel.getPath())) {
                String path = CacheUtil.getDownloadPath(context, cacheVideoModel.getGuid());
                cacheVideoModel.setPath(path);
                DownloadInfo info = CacheUtil.cacheVideoModelToDownloadInfo(cacheVideoModel);
                downloadInfos.add(info);
            }
        }
        try {
            CacheUtil.getDownloadQueue(context).changeDownloadInfoByList(downloadInfos);
            CacheVideoDao.saveList(context, CacheManager.mDownloadingList);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}