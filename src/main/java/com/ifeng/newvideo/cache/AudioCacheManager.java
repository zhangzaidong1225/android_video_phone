package com.ifeng.newvideo.cache;

import android.os.AsyncTask;
import com.ifeng.newvideo.utils.VideoUtils;
import com.ifeng.video.core.utils.FileUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by cuihz on 2014/11/24.
 */
public class AudioCacheManager {

    private static final Logger logger = LoggerFactory.getLogger(AudioCacheManager.class);

    private static final String AUDIO_EX = ".auo";

    public static void getAudioFromVideo(CacheVideoModel cacheVideoModel) {
        AudioAsyncTask audioAsyncTask = new AudioAsyncTask();
        audioAsyncTask.execute(cacheVideoModel);
    }

    private static class AudioAsyncTask extends AsyncTask<CacheVideoModel, Void, Void> {
        @Override
        protected Void doInBackground(CacheVideoModel... params) {
            VideoUtils.getAudioViaVideo(params[0].getPath(), params[0].getPath() + AUDIO_EX);
            return null;
        }
    }

    public static void deleteAudio(CacheVideoModel cacheVideoModel) {
        File file = new File(cacheVideoModel.getPath() + AUDIO_EX);
        FileUtils.delete(file);
    }

    public static String getAudioPath(String videoPath, String type) {
        if (IfengType.TYPE_AUDIO.equals(type)) {
            return videoPath;
        }
        return videoPath + AUDIO_EX;
    }

}
