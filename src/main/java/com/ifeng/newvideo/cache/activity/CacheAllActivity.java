package com.ifeng.newvideo.cache.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.*;
import com.ifeng.newvideo.cache.adapter.CacheAllAdapter;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.DownloadRecord;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.mine.adapter.ICheckedNumber;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.CacheDao;
import com.ifeng.video.dao.db.dao.CacheVideoDao;
import com.ifeng.video.dao.db.dao.VideoPlayDao;
import com.ifeng.video.dao.db.model.CacheBaseModel;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * 我的缓存界面
 */
public class CacheAllActivity extends BaseFragmentActivity implements View.OnClickListener, ICheckedNumber {
    private static final Logger logger = LoggerFactory.getLogger(CacheAllActivity.class);

    private TextView titleVideo;
    private TextView titleAudio;
    private TextView tvEdit;//顶部右侧编辑、完成文字
    private ListView listView;// 已下载完的ListView
    private View mEditBottomView;//编辑模式下，底部出现的“删除”、“清空”View
    private TextView mDeleteBtn;//删除按钮
    private View mEmptyView;//数据为空时显示的view
    private TextView emptyText;
    private ImageView emptyImg;

    private View mSDcardInfoView;//存储卡容量信息Layout
    private ProgressBar mSDcardCapacityProgress;//存储卡容量信息进度条
    private TextView mSDcardCapacityText;//存储卡容量信息
    private BroadcastReceiver mSDcardBroadReceiver;//存储卡广播接收器

    //视频HeaderView
    private View mVideoCachingView;//正在缓存View
    private ImageView mVideoCachingFolderView;//正在缓存文件夹View
    private TextView mVideoDownloadingCountText;//正在下载的视频个数
    private TextView mVideoDownloadingVideoTitle;//正在下载的视频标题
    private TextView mVideoDownloadingSpeedText;//下载速度TextView
    private CircleProgress mVideoDownloadingProgress;//正在下载进度圈
    private FrameLayout mVideoDownloadingFolder;//文件夹图片及圆形进度
    private View videoBottomLine;//底部下划线
    private ImageView mVideoCheckImg;//选中图片

    //音频HeaderView
    private View mAudioCachingView;//正在缓存View
    private ImageView mAudioCachingFolderView;//正在缓存文件夹View
    private TextView mAudioDownloadingCountText;//正在下载的视频个数
    private TextView mAudioDownloadingVideoTitle;//正在下载的视频标题
    private TextView mAudioDownloadingSpeedText;//下载速度TextView
    private CircleProgress mAudioDownloadingProgress;//正在下载进度圈
    private FrameLayout mAudioDownloadingFolder;//文件夹图片及圆形进度
    private View audioBottomLine;//底部下划线
    private ImageView mAudioCheckImg;//选中图片

    private static long mSpeed = 0;//下载速度
    private static final int MAX_PROGRESS = 100;//progressBar最大进度
    private boolean isCachingSelected = false;//"正在缓存"是否选中

    private CacheAllAdapter adapter;//已下载完列表adapter
    private boolean mIsEdit = false;//是否在编辑模式
    private NetDealManager netDealManager;
    public static boolean tenDownloading = false;//
    private boolean isSelectVideo = true;//选择的是否是视频

    private List<CacheBaseModel> cacheVideoModels = new ArrayList<>();

    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cache_activity_all);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initView();
        initCachingVideoView();
        initCachingAudioView();
        initAdapter();
        setEmptyView(isSelectVideo);
        netDealManager = new NetDealManager(this);
        registerSDcardBR();
        updateCachingView();
        startFormPush(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        CacheManager.setCacheAllActivity(this);
        refreshUI();
    }

    @Override
    protected void onPause() {
        CacheManager.setCacheAllActivity(null);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mSDcardBroadReceiver != null) {
            unregisterReceiver(mSDcardBroadReceiver);
        }
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        startFormPush(intent);
    }

    /**
     * 推送拉起下载
     */
    private void startFormPush(final Intent intent) {
        if (intent == null) {
            return;
        }
        collapseStatusBar();
        String guid = intent.getStringExtra(IntentKey.VOD_GUID);
        String id = intent.getStringExtra(IntentKey.VOD_ID);
        logger.debug("cache push intent: guid={}，id={}", guid, id);
        if (TextUtils.isEmpty(guid)) {
            if (TextUtils.isEmpty(id)) {
                return;
            }
            CacheDao.getVideoInfoByMemberId(id,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            if (response == null || TextUtils.isEmpty(response.toString())) {
                                ToastUtils.getInstance().showLongToast(R.string.cache_url_error);
                                return;
                            }
                            PlayerInfoModel playerInfoModel = new PlayerInfoModel();
                            playerInfoModel = playerInfoModel.parsesJson(response.toString());
                            if (CacheManager.isInCache(CacheAllActivity.this, playerInfoModel.getGuid())) {
                                ToastUtils.getInstance().showLongToast(R.string.common_already_to_download);
                                return;
                            }
                            addDownload(playerInfoModel, intent);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            logger.error("getVideoInfoByMemberId()  error={}", error);
                            ToastUtils.getInstance().showLongToast(R.string.cache_url_error);
                        }
                    });
        } else {
            if (CacheManager.isInCache(this, guid)) {
                ToastUtils.getInstance().showLongToast(R.string.common_already_to_download);
                return;
            }
            VideoPlayDao.getSingleVideoByGuidForDownload(guid, new PlayerInfoModel(),
                    new Response.Listener<PlayerInfoModel>() {
                        @Override
                        public void onResponse(PlayerInfoModel response) {
                            if (response == null || TextUtils.isEmpty(response.getGuid())) {
                                ToastUtils.getInstance().showLongToast(R.string.cache_url_error);
                                return;
                            }
                            addDownload(response, intent);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            logger.error("getSingleVideoByGuidForDownload()  error={}", error);
                            ToastUtils.getInstance().showLongToast(R.string.cache_url_error);
                        }
                    });
        }
    }

    /**
     * 添加到缓存
     */
    private void addDownload(final PlayerInfoModel playerInfoModel, final Intent intent) {
        netDealManager.dealNetTypeWithSetting(new NetDealListener() {
            @Override
            public void onDealByState(int state) {
                if (state != NO_ACTION) {
                    CacheManager.addDownload(CacheAllActivity.this, playerInfoModel, state, 0, IfengType.TYPE_VIDEO, null);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back: // 返回
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_CACHED);
                finish();
                break;
            case R.id.title_video:
                if (mIsEdit || isSelectVideo) {
                    return;
                }
                mAudioCachingView.setVisibility(View.GONE);
                audioBottomLine.setVisibility(View.GONE);
                isSelectVideo = true;
                setTitleStyle();
                adapter.setDatas(getAdapterData());
                PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_TAB_VIDEO, null);
                break;
            case R.id.title_audio:
                if (mIsEdit || !isSelectVideo) {
                    return;
                }
                mVideoCachingView.setVisibility(View.GONE);
                videoBottomLine.setVisibility(View.GONE);
                isSelectVideo = false;
                setTitleStyle();
                adapter.setDatas(getAdapterData());
                PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_TAB_AUDIO, null);
                break;
            case R.id.title_right_text:// 编辑
                if (mIsEdit) {
                    PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_MY_EDIT_FINISH, isSelectVideo);
                } else {
                    PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_MY_EDIT, isSelectVideo);
                }
                dealEditMode();
                break;
            case R.id.btn_delete:// 删除
                if (adapter.getSelectedList().size() == 0 && !isCachingSelected) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_unchecked_any_video);
                } else {
                    new DeleteSelectedDownLoadAsyncTask().execute(false);
                }
                PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_MY_DELETE, isSelectVideo);
                break;
            case R.id.btn_clear: // 清空
                cachingFolderSelectedIsFromClear = true;
                isCachingSelected = true;
                showClearConfirmDialog();
                break;
            default:
                break;
        }
    }

    private void initView() {
        findViewById(R.id.back).setOnClickListener(this);
        titleVideo = (TextView) findViewById(R.id.title_video);
        titleVideo.setText(R.string.setting_video);
        titleVideo.setOnClickListener(this);
        titleAudio = (TextView) findViewById(R.id.title_audio);
        titleAudio.setText(R.string.setting_FM);
        titleAudio.setOnClickListener(this);
        mEmptyView = findViewById(R.id.cache_all_empty);
        emptyText = (TextView) findViewById(R.id.cache_all_empty_tips);
        emptyImg = (ImageView) findViewById(R.id.cache_all_empty_img);
        //标题右侧编辑、完成文字
        tvEdit = (TextView) findViewById(R.id.title_right_text);
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
        tvEdit.setText(getString(R.string.edit));
        tvEdit.setVisibility(View.VISIBLE);
        tvEdit.setOnClickListener(this);
        //删除、清空信息
        mEditBottomView = findViewById(R.id.cache_all_editor_bottombar);
        mDeleteBtn = (TextView) findViewById(R.id.btn_delete);
        mDeleteBtn.setOnClickListener(this);
        findViewById(R.id.btn_clear).setOnClickListener(this);
        //底部存储卡信息
        mSDcardInfoView = findViewById(R.id.capacity_info_FL);
        mSDcardCapacityProgress = (ProgressBar) findViewById(R.id.capacity_progress);
        mSDcardCapacityText = (TextView) findViewById(R.id.capacity_tv);
        listView = (ListView) findViewById(R.id.cache_all_listView);
        setTitleStyle();

        //默认报一次选中视频
        PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_TAB_VIDEO, null);
    }

    /**
     * 设置标题样式
     */
    private void setTitleStyle() {
        titleVideo.setTextColor(getResources().getColor(isSelectVideo ? R.color.white : R.color.cache_paused_text_color));
        titleVideo.setBackgroundResource(isSelectVideo ? R.color.cache_paused_text_color : R.drawable.common_mine_title_bar_left);
        titleAudio.setTextColor(getResources().getColor(isSelectVideo ? R.color.cache_paused_text_color : R.color.white));
        titleAudio.setBackgroundResource(isSelectVideo ? R.drawable.common_mine_title_bar_right : R.color.cache_paused_text_color);
    }

    /**
     * 设置adapter、正在缓存View数据。 会在以下几处执行：1、进入界面 2、下载过程中 3、删除之后
     */
    protected void initAdapter() {
        adapter = new CacheAllAdapter(this);
        adapter.setCheckedNumber(this);
        adapter.setDatas(getAdapterData());
        listView.setAdapter(adapter);
    }

    private void collapseStatusBar() {
        try {
            Object statusBarManager = getSystemService("statusbar");
            Method collapse;
            if (Build.VERSION.SDK_INT <= 16) {
                collapse = statusBarManager.getClass().getMethod("collapse");
            } else {
                collapse = statusBarManager.getClass().getMethod("collapsePanels");
            }
            collapse.invoke(statusBarManager);
        } catch (Exception localException) {
            logger.error("", localException);
        }
    }

    private long lastRxBytes = 0;
    private long lastTime = 0;

    /**
     * 设置下载进度、速度textView。每隔一段时间读取一次总流量，然后用本次和前一次的差除以间隔时间来获取平均速度，再换算为 K/s M/s 等单位，
     */
    private void setProgressAndSpeedText() {
        long curRxBytes = getTotalRxBytes();
        long curTime = System.currentTimeMillis();
        if ((curTime - lastTime) != 0 && (curRxBytes - lastRxBytes) != 0) {
            mSpeed = (curRxBytes - lastRxBytes) / (curTime - lastTime);
        }
        lastRxBytes = curRxBytes;
        lastTime = curTime;

        if (mSpeed < 1024) {
            if (isSelectVideo) {
                mVideoDownloadingSpeedText.setText(mSpeed + " KB/s");
            } else {
                mAudioDownloadingSpeedText.setText(mSpeed + " KB/s");
            }
        } else {
            float size = (float) mSpeed / 1024;
            DecimalFormat df = new DecimalFormat("0.0");
            String speedSize = df.format(size);
            if (isSelectVideo) {
                mVideoDownloadingSpeedText.setText(speedSize + " MB/s");
            } else {
                mAudioDownloadingSpeedText.setText(speedSize + " MB/s");
            }
        }
    }


    /**
     * 获取总的接受字节数，包含Mobile和WiFi等
     *
     * @return 总的接受字节数
     */
    private long getTotalRxBytes() {
        return TrafficStats.getTotalRxBytes() == TrafficStats.UNSUPPORTED ? 0 : (TrafficStats.getTotalRxBytes());
    }

    /**
     * 设置“正在缓存”View
     */
    public void updateCachingView() {
        List<CacheVideoModel> cacheVideoModelList = CacheManager.getDownloadingList(!isSelectVideo);
        if (!ListUtils.isEmpty(cacheVideoModelList)) {//正在下载的数据不为空
            if (isSelectVideo) {
                mVideoCachingView.setVisibility(View.VISIBLE);
                videoBottomLine.setVisibility(View.VISIBLE);
                mAudioCachingView.setVisibility(View.GONE);
                audioBottomLine.setVisibility(View.GONE);
            } else {
                mVideoCachingView.setVisibility(View.GONE);
                videoBottomLine.setVisibility(View.GONE);
                mAudioCachingView.setVisibility(View.VISIBLE);
                audioBottomLine.setVisibility(View.VISIBLE);
            }
            if (isSelectVideo) {
                mVideoDownloadingCountText.setText(getString(R.string.cache_caching) + "(" + CacheManager.getDownloadingList(!isSelectVideo).size() + ")");
            } else {
                mAudioDownloadingCountText.setText(getString(R.string.cache_caching) + "(" + CacheManager.getDownloadingList(!isSelectVideo).size() + ")");
            }
            CacheVideoModel cacheVideoModel = null;
            CacheVideoModel waitCacheVideoModel = null;
            CacheVideoModel pauseCacheVideoModel = null;

            for (CacheVideoModel obj : cacheVideoModelList) {
                if (obj.getState() == DownloadOrder.STATE_PAUSE) {
                    pauseCacheVideoModel = obj;
                    break;
                }
            }

            for (CacheVideoModel obj : cacheVideoModelList) {
                switch (obj.getState()) {
                    case DownloadOrder.STATE_DOWNING:
                        cacheVideoModel = obj;
                        break;
                    case DownloadOrder.STATE_WAIT_DOWN:
                        waitCacheVideoModel = obj;
                        break;
                }
            }
            if (NetUtils.isNetAvailable(this) && cacheVideoModel != null) {// 如果正在下载，显示下载标题和速度、进度，否则不显示
                if (isSelectVideo) {
                    mVideoDownloadingVideoTitle.setText(cacheVideoModel.getName());
                    mVideoDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_info_list_title));
                    mVideoDownloadingSpeedText.setVisibility(View.VISIBLE);
                    mVideoDownloadingProgress.setCurProgress(cacheVideoModel.getProgress());
                    mVideoDownloadingProgress.setMaxProgress(MAX_PROGRESS);
                    mVideoDownloadingVideoTitle.setTextSize(16);
                    mVideoDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_state_text_color));
                    mVideoCachingView.setVisibility(View.VISIBLE);
                    videoBottomLine.setVisibility(View.VISIBLE);
                } else {
                    mAudioDownloadingVideoTitle.setText(cacheVideoModel.getName());
                    mAudioDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_info_list_title));
                    mAudioDownloadingSpeedText.setVisibility(View.VISIBLE);
                    mAudioDownloadingProgress.setCurProgress(cacheVideoModel.getProgress());
                    mAudioDownloadingProgress.setMaxProgress(MAX_PROGRESS);
                    mAudioDownloadingVideoTitle.setTextSize(16);
                    mAudioDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_state_text_color));
                    mAudioCachingView.setVisibility(View.VISIBLE);
                    audioBottomLine.setVisibility(View.VISIBLE);
                }
                setProgressAndSpeedText();
            } else if (NetUtils.isNetAvailable(this)
                    && NetUtils.isMobileNet(this) && waitCacheVideoModel != null
                    && !SharePreUtils.getInstance().getCacheVideoState()) {//手机网络，未开启运营商网络缓存
                if (isSelectVideo) {
                    mVideoDownloadingVideoTitle.setText(getString(R.string.cache_only_wifi));
                    mVideoDownloadingVideoTitle.setTextSize(12);
                    mVideoDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_paused_text_color));
                    mVideoDownloadingSpeedText.setVisibility(View.GONE);
                    mVideoCachingView.setVisibility(View.VISIBLE);
                    videoBottomLine.setVisibility(View.VISIBLE);
                } else {
                    mAudioDownloadingVideoTitle.setText(getString(R.string.cache_only_wifi));
                    mAudioDownloadingVideoTitle.setTextSize(12);
                    mAudioDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_paused_text_color));
                    mAudioDownloadingSpeedText.setVisibility(View.GONE);
                    mAudioCachingView.setVisibility(View.VISIBLE);
                    audioBottomLine.setVisibility(View.VISIBLE);
                }
            } else {//如果不是正在缓存状态，把标题改为已暂停，并且对文字大小、颜色进行更改
                if (isSelectVideo) {
                    mVideoDownloadingVideoTitle.setText(getString(R.string.cache_paused_1));
                    mVideoDownloadingVideoTitle.setTextSize(12);
                    mVideoDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_paused_text_color));
                    if (pauseCacheVideoModel != null) {
                        mVideoDownloadingProgress.setCurProgress(pauseCacheVideoModel.getProgress());
                        mVideoDownloadingProgress.setMaxProgress(MAX_PROGRESS);
                    }
                    mVideoDownloadingSpeedText.setVisibility(View.GONE);
                    mVideoCachingView.setVisibility(View.VISIBLE);
                    videoBottomLine.setVisibility(View.VISIBLE);
                } else {
                    mAudioDownloadingVideoTitle.setText(getString(R.string.cache_paused_1));
                    mAudioDownloadingVideoTitle.setTextSize(12);
                    mAudioDownloadingVideoTitle.setTextColor(getResources().getColor(R.color.cache_paused_text_color));
                    if (pauseCacheVideoModel != null) {
                        mAudioDownloadingProgress.setCurProgress(pauseCacheVideoModel.getProgress());
                        mAudioDownloadingProgress.setMaxProgress(MAX_PROGRESS);
                    }
                    mAudioDownloadingSpeedText.setVisibility(View.GONE);
                    mAudioCachingView.setVisibility(View.VISIBLE);
                    audioBottomLine.setVisibility(View.VISIBLE);
                }

            }
            setCachingFolerEdit();
        } else {//否则不显示正在下载布局
            mVideoCachingView.setVisibility(View.GONE);
            videoBottomLine.setVisibility(View.GONE);
            mAudioCachingView.setVisibility(View.GONE);
            audioBottomLine.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化视频正在缓存View
     */
    private void initCachingVideoView() {
        if (mVideoCachingView != null) {
            //mVideoDownloadingFolder.setLayoutParams(Util4act.staticHomeLayoutImageViewRatio4Single(mVideoDownloadingFolder.getContext(), mVideoDownloadingFolder.getLayoutParams()));
            mVideoCachingFolderView.setImageResource(R.drawable.common_video_folder_bg);
        } else {
            View folderLayout = getLayoutInflater().inflate(R.layout.cache_caching_video_folder, null);
            mVideoCachingView = folderLayout.findViewById(R.id.folder_layout);
            mVideoDownloadingCountText = (TextView) folderLayout.findViewById(R.id.folder_entry_count);
            mVideoDownloadingVideoTitle = (TextView) folderLayout.findViewById(R.id.folder_entry_introduce);
            mVideoDownloadingSpeedText = (TextView) folderLayout.findViewById(R.id.folder_entry_velocity);
            mVideoCheckImg = (ImageView) folderLayout.findViewById(R.id.folder_check_img);
            mVideoDownloadingFolder = (FrameLayout) folderLayout.findViewById(R.id.folder_caching);
            //mVideoDownloadingFolder.setLayoutParams(Util4act.staticHomeLayoutImageViewRatio4Single(mVideoDownloadingFolder.getContext(), mVideoDownloadingFolder.getLayoutParams()));
            mVideoCachingFolderView = (ImageView) folderLayout.findViewById(R.id.folder_caching_img);
            //mVideoDownloadingFolder.setLayoutParams(Util4act.staticHomeLayoutImageViewRatio4Single(mVideoDownloadingFolder.getContext(), mVideoDownloadingFolder.getLayoutParams()));
            mVideoCachingFolderView.setImageResource(R.drawable.common_video_folder_bg);
            mVideoDownloadingProgress = (CircleProgress) folderLayout.findViewById(R.id.folder_caching_progress);
            videoBottomLine = folderLayout.findViewById(R.id.header_video_bottom_line);
            listView.addHeaderView(folderLayout);
            folderLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIsEdit) {
                        if (isCachingSelected) {
                            isCachingSelected = false;
                            cachingFolderSelectedIsFromOnClick = false;
                            mVideoCheckImg.setImageResource(R.drawable.common_edit_on_no);
                        } else {
                            isCachingSelected = true;
                            cachingFolderSelectedIsFromOnClick = true;
                            mVideoCheckImg.setImageResource(R.drawable.common_edit_on);
                        }
                        getCheckedNum(checkedNum);
                    } else {
                        IntentUtils.startCachingActivity(CacheAllActivity.this, IfengType.TYPE_VIDEO);
                    }
                }
            });
        }
    }

    private boolean cachingFolderSelectedIsFromClear = false;//选中缓存文件夹是否来自清空
    private boolean cachingFolderSelectedIsFromOnClick = false;//选中缓存文件夹是否来自点击事件

    /**
     * 初始化音频正在缓存View
     */
    private void initCachingAudioView() {
        View folderLayout = getLayoutInflater().inflate(R.layout.cache_caching_audio_folder, null);
        mAudioCachingView = folderLayout.findViewById(R.id.folder_layout);
        mAudioDownloadingCountText = (TextView) folderLayout.findViewById(R.id.folder_entry_count);
        mAudioDownloadingVideoTitle = (TextView) folderLayout.findViewById(R.id.folder_entry_introduce);
        mAudioDownloadingSpeedText = (TextView) folderLayout.findViewById(R.id.folder_entry_velocity);
        mAudioCheckImg = (ImageView) folderLayout.findViewById(R.id.folder_check_img);
        mAudioDownloadingFolder = (FrameLayout) folderLayout.findViewById(R.id.folder_caching);
        mAudioCachingFolderView = (ImageView) folderLayout.findViewById(R.id.folder_caching_img);
        mAudioCachingFolderView.setImageResource(R.drawable.common_video_folder_bg);
        mAudioDownloadingProgress = (CircleProgress) folderLayout.findViewById(R.id.folder_caching_progress);
        audioBottomLine = folderLayout.findViewById(R.id.header_audio_bottom_line);
        listView.addHeaderView(folderLayout);
        mAudioCachingView.setVisibility(View.GONE);
        folderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsEdit) {
                    if (isCachingSelected) {
                        isCachingSelected = false;
                        cachingFolderSelectedIsFromOnClick = false;
                        mAudioCheckImg.setImageResource(R.drawable.common_edit_on_no);
                    } else {
                        isCachingSelected = true;
                        cachingFolderSelectedIsFromOnClick = true;
                        mAudioCheckImg.setImageResource(R.drawable.common_edit_on);
                    }
                    getCheckedNum(checkedNum);
                } else {
                    IntentUtils.startCachingActivity(CacheAllActivity.this, IfengType.TYPE_AUDIO);
                }
            }
        });
    }

    private int checkedNum = 0;//列表选中的数量

    @Override
    public void getCheckedNum(int num) {//选中数量回调
        this.checkedNum = num;
        if (isCachingSelected) {
            int downloadingSize = CacheManager.getDownloadingList(!isSelectVideo).size();
            num += downloadingSize;
        }
        mDeleteBtn.setText(this.getString(R.string.common_delete) + "(" + num + ")");
    }

    /**
     * 设置正在缓存编辑逻辑
     */
    private void setCachingFolerEdit() {
        if (mIsEdit) {
            if (isSelectVideo) {
                mVideoCheckImg.setVisibility(View.VISIBLE);
            } else {
                mAudioCheckImg.setVisibility(View.VISIBLE);
            }
            if (isCachingSelected) {
                if (!cachingFolderSelectedIsFromClear || cachingFolderSelectedIsFromOnClick) {
                    if (isSelectVideo) {
                        mVideoCheckImg.setImageResource(R.drawable.common_edit_on);
                    } else {
                        mAudioCheckImg.setImageResource(R.drawable.common_edit_on);
                    }
                }
            } else {
                if (isSelectVideo) {
                    mVideoCheckImg.setImageResource(R.drawable.common_edit_on_no);
                } else {
                    mAudioCheckImg.setImageResource(R.drawable.common_edit_on_no);
                }
            }
        } else {
            if (isSelectVideo) {
                mVideoCheckImg.setVisibility(View.GONE);
            } else {
                mAudioCheckImg.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 刷新界面，包括Adapter和正在缓存View
     */
    public void refreshUI() {
        if (CacheManager.getDownloadingList(!isSelectVideo).size() == 0 && isCachingSelected) {//如果下载中的数量为0，并且选中了HeaderView，则把HeaderView置为未选中
            isCachingSelected = false;
            cachingFolderSelectedIsFromOnClick = false;
            cachingFolderSelectedIsFromClear = false;
        }
        getCheckedNum(adapter.getSelectedList().size());//每次下载完成重新计算选中的数量
        updateCachingView();
        if (adapter != null) {
            cacheVideoModels.clear();
            cacheVideoModels.addAll(CacheManager.getDownloadAllList(!isSelectVideo));
            adapter.setDatas(cacheVideoModels);
        }
        setEmptyView(isSelectVideo);
    }

    /**
     * 设置没有数据时显示的View
     */
    private void setEmptyView(boolean isVideo) {
        boolean isDownloadingEmpty = ListUtils.isEmpty(CacheManager.getDownloadingList(!isSelectVideo));//正在下载数据是否为空
        boolean isDownloadEmpty = ListUtils.isEmpty(CacheManager.getDownloadAllList(!isVideo));//下载完成的视频(音频)是否为空
        boolean isEmpty = isDownloadingEmpty && isDownloadEmpty;//是否正在下载、下载完成都为空
        mEmptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        emptyImg.setImageResource(isVideo ? R.drawable.cache_empty : R.drawable.cache_empty);
        emptyText.setText(isVideo ? R.string.cache_video_none : R.string.cache_audio_none);
        tvEdit.setVisibility(isEmpty ? View.GONE : View.VISIBLE);//没有数据，不显示编辑文字
        listView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
    }

    /**
     * 删除或清空选中的数据
     *
     * @param isClear 是否清空
     */
    public void deleteSelectedList(boolean isClear) throws Exception {
        List<Integer> ids = new ArrayList<>();
        if (isCachingSelected) {//如果正在缓存的选中
            for (CacheBaseModel info : CacheManager.getDownloadingList(!isSelectVideo)) {
                //统计
                DownloadRecord record = new DownloadRecord(info.getGuid(), info.getName(), "cancel", PageIdConstants.MY_CACHED);
                CustomerStatistics.sendDownload(record);
                ids.add(info.getId());
            }
        }

        for (CacheBaseModel cacheBaseModel : isClear ? adapter.getAllSelectedList() : adapter.getSelectedList()) {
            if (cacheBaseModel instanceof CacheVideoModel) {
                ids.add(cacheBaseModel.getId());
                AudioCacheManager.deleteAudio((CacheVideoModel) cacheBaseModel);
            }
        }
        CacheUtil.getDownloadQueue(this).changeStateByIds(ids, DownloadOrder.STATE_STOP);
        CacheVideoDao.deleteList(this, ids);

        if (isCachingSelected) {
            CacheManager.getDownloadingList(!isSelectVideo).clear();
        }
    }

    /**
     * 注册sd卡监听广播
     */
    private void registerSDcardBR() {
        mSDcardBroadReceiver = new SDcardReceiver(this, mSDcardCapacityProgress, mSDcardCapacityText);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addDataScheme(CacheManager.SDCARD_DATASCHEME);
        registerReceiver(mSDcardBroadReceiver, filter);
    }

    /**
     * 处理编辑模式
     */
    private void dealEditMode() {
        if (mIsEdit) {
            exitEditMode();
        } else {
            entryEditMode();
        }
    }

    /**
     * 进入编辑模式
     */
    void entryEditMode() {
        adapter.setInEditMode(true);
        mEditBottomView.setVisibility(View.VISIBLE);
        mSDcardInfoView.setVisibility(View.GONE);
        tvEdit.setText(getString(R.string.finish));
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_edit_text_color));
        mIsEdit = true;
        setCachingFolerEdit();
    }

    /**
     * 退出编辑模式
     */
    void exitEditMode() {
        adapter.setInEditMode(false);
        mEditBottomView.setVisibility(View.GONE);
        mSDcardInfoView.setVisibility(View.VISIBLE);
        tvEdit.setText(getString(R.string.edit));
        tvEdit.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
        mIsEdit = false;
        setCachingFolerEdit();
        resetCheckedState();
    }

    /**
     * 清空确认提示框
     */
    private void showClearConfirmDialog() {
        Resources res = this.getResources();
        dialog = AlertUtils.getInstance().showTwoBtnDialog(this,
                res.getString(R.string.cache_delete_video_confirm_msg),
                res.getString(R.string.common_popup_layout_clear),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PageActionTracker.clickMyList(0, ActionIdConstants.CLICK_MY_CLEAR, isSelectVideo);
                        new DeleteSelectedDownLoadAsyncTask().execute(true);//清空数据
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                },
                res.getString(R.string.common_cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                });
        if (dialog != null) {
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (isCachingSelected) {
                        if (cachingFolderSelectedIsFromOnClick) {//如果来自点击事件还保持选中状态
                            isCachingSelected = true;
                        } else if (cachingFolderSelectedIsFromClear) {//如果来自清空重置状态
                            isCachingSelected = false;
                        }
                    }
                    cachingFolderSelectedIsFromClear = false;
                }
            });
        }
    }

    /**
     * 重置选中状态
     */
    private void resetCheckedState() {
        if (isCachingSelected) {
            isCachingSelected = false;
            cachingFolderSelectedIsFromOnClick = false;//退出编辑模式，重置是否来自点击事件的选中状态
        }
        adapter.getSelectedList().clear();
        adapter.getAllSelectedList().clear();
        getCheckedNum(0);
        for (CacheBaseModel model : cacheVideoModels) {
            ((CacheVideoModel) model).setCheckState(0);
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * 获取适配器中需要的的数据
     */
    public List<CacheBaseModel> getAdapterData() {
        CacheManager.refreshDownloadVideos(this);
        cacheVideoModels.clear();
        cacheVideoModels.addAll(CacheManager.getDownloadAllList(!isSelectVideo));
        return cacheVideoModels;
    }

    /**
     * 异步删除下载完和正在下载的视频
     */
    private class DeleteSelectedDownLoadAsyncTask extends AsyncTask<Boolean, Void, Void> {
        private Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(CacheAllActivity.this, R.style.common_Theme_Dialog);
            dialog.setContentView(R.layout.cache_dialog_progress);
            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText(CacheAllActivity.this.getString(R.string.cache_wait_delete));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            try {
                if (params != null) {
                    deleteSelectedList(params[0]);
                }
            } catch (Exception e) {
                CacheManager.synchronizeDB(CacheAllActivity.this);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            exitEditMode();
            getCheckedNum(0);//设置删除按钮数量为0
            if (!CacheAllActivity.this.isFinishing()) {
                dialog.dismiss();
            }
            adapter.getSelectedList().clear();
            adapter.setDatas(getAdapterData());
            refreshUI();
            if (isCachingSelected) {
                isCachingSelected = false;
                cachingFolderSelectedIsFromClear = false;
                cachingFolderSelectedIsFromOnClick = false;
            }
        }
    }
}
