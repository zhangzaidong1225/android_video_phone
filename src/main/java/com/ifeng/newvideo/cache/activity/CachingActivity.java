package com.ifeng.newvideo.cache.activity;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.cache.NetDealListener;
import com.ifeng.newvideo.cache.NetDealManager;
import com.ifeng.newvideo.cache.SDcardReceiver;
import com.ifeng.newvideo.cache.adapter.CachingAdapter;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.DownloadRecord;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.mine.adapter.ICheckedNumber;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.exception.DownloadDBException;
import com.ifeng.video.core.exception.IllegalParamsException;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.CacheVideoDao;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 正在缓存界面
 */
public class CachingActivity extends BaseFragmentActivity implements OnClickListener, ICheckedNumber {
    private static final Logger logger = LoggerFactory.getLogger(CachingActivity.class);

    private TextView titleVideo;
    private TextView titleAudio;
    private TextView title_right_text;//编辑、完成
    private View mEditBottom;//底部删除、清空布局
    private TextView mBtnDelete;//删除按钮

    private ListView listView;
    private View mAllDownloadEmptyView;//没有数据V布局
    private TextView emptyText;
    private ImageView emptyImg;
    private View mSDcardCapacityInfoLayout;//底部SD卡信息布局
    private TextView mSDcardCapacityText;

    private CachingAdapter mAdapter;
    private BroadcastReceiver mSDcardReceiver;
    private ProgressBar mSDcardCapacityProgressBar;
    private NetDealManager netDealManager;
    private Dialog dialog;

    private ArrayList<CacheVideoModel> list = new ArrayList<>();
    private boolean mIsEdit = false;//true :列表处于编辑状态，准备进行删除或清空
    private boolean isStartingAll = false;
    private boolean isLockScreen = false;

    private boolean isSelectVideo = true;//选择的是否是视频

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cache_activity_caching);
        isSelectVideo = !IfengType.TYPE_AUDIO.equals(getIntent().getAction());
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initView();//要在注册SD卡广播之前
        initAdapter();
        updateEditorView(isSelectVideo);
        registerSDCardBR();
        netDealManager = new NetDealManager(this);
        isLockScreen = false;
    }

    @Override
    protected void onResume() {
        CacheManager.setCachingActivity(this);
        if (isLockScreen) {
            refreshUI();
        }
        isLockScreen = true;
        super.onResume();
    }

    @Override
    protected void onPause() {
        for (CacheVideoModel cacheVideoModel : CacheManager.mDownloadingList) {
            CacheManager.DownHandler handler = (CacheManager.DownHandler) CacheUtil.getDownloadQueue(this).getDownloadHandlerById(cacheVideoModel.getId());
            if (handler != null) {
                handler.setHolder(null);
            }
        }
        CacheManager.setCachingActivity(null);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mSDcardReceiver);
    }

    @Override
    public void getCheckedNum(int number) {
        mBtnDelete.setText(this.getString(R.string.common_delete) + "(" + number + ")");
    }

    /**
     * 注册SD卡广播接收器
     */
    private void registerSDCardBR() {
        mSDcardReceiver = new SDcardReceiver(this, mSDcardCapacityProgressBar, mSDcardCapacityText);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addDataScheme(CacheManager.SDCARD_DATASCHEME);
        registerReceiver(mSDcardReceiver, filter);
    }

    private void initView() {
        //标题部分
        findViewById(R.id.back).setOnClickListener(this);
        titleVideo = (TextView) findViewById(R.id.title_video);
        titleVideo.setText(R.string.setting_video);
        titleVideo.setOnClickListener(this);
        titleAudio = (TextView) findViewById(R.id.title_audio);
        titleAudio.setText(R.string.setting_FM);
        titleAudio.setOnClickListener(this);
        title_right_text = (TextView) findViewById(R.id.title_right_text);
        title_right_text.setOnClickListener(this);
        initTitleRightText(mIsEdit);
        //全部开始，全部暂停部分
        findViewById(R.id.caching_pause).setOnClickListener(this);
        findViewById(R.id.caching_start).setOnClickListener(this);
        //底部清空删除部分
        mEditBottom = findViewById(R.id.caching_editor_bottombar);
        findViewById(R.id.btn_clear).setOnClickListener(this);
        mBtnDelete = (TextView) findViewById(R.id.btn_delete);
        mBtnDelete.setOnClickListener(this);
        //底部SD卡信息布局
        mSDcardCapacityInfoLayout = findViewById(R.id.capacity_info_FL);
        mSDcardCapacityProgressBar = (ProgressBar) findViewById(R.id.capacity_progress);
        mSDcardCapacityText = (TextView) findViewById(R.id.capacity_tv);
        //ListView与empty布局
        mAllDownloadEmptyView = findViewById(R.id.caching_empty);
        emptyText = (TextView) findViewById(R.id.empty_text);
        emptyImg = (ImageView) findViewById(R.id.empty_image);
        listView = (ListView) findViewById(R.id.caching_listView);
        setTitleStyle();

        //默认报一次选中视频
        PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_TAB_VIDEO, null);
    }

    /**
     * 设置标题样式
     */
    private void setTitleStyle() {
        titleVideo.setTextColor(getResources().getColor(isSelectVideo ? R.color.white : R.color.cache_paused_text_color));
        titleVideo.setBackgroundResource(isSelectVideo ? R.color.cache_paused_text_color : R.drawable.common_mine_title_bar_left);
        titleAudio.setTextColor(getResources().getColor(isSelectVideo ? R.color.cache_paused_text_color : R.color.white));
        titleAudio.setBackgroundResource(isSelectVideo ? R.drawable.common_mine_title_bar_right : R.color.cache_paused_text_color);
    }

    /**
     * 初始化标题右侧文字：编辑、删除
     *
     * @param isEditMode 是否是编辑模式
     */
    private void initTitleRightText(boolean isEditMode) {
        if (!isEditMode) {
            title_right_text.setText(getString(R.string.edit));
            title_right_text.setTextColor(getResources().getColor(R.color.mine_watch_complete_text_color));
            title_right_text.setVisibility(View.VISIBLE);
        } else {
            title_right_text.setText(getString(R.string.finish));
            title_right_text.setTextColor(getResources().getColor(R.color.mine_watch_edit_text_color));
            title_right_text.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 设置listView adapter
     */
    private void initAdapter() {
        CacheManager.getDownloadVideos(this);
        mAdapter = new CachingAdapter(this);
        mAdapter.setCheckedNumber(this);
        listView.setAdapter(mAdapter);
        setAdapterData(CacheManager.getDownloadingList(!isSelectVideo), list);
        listView.setRecyclerListener(new AbsListView.RecyclerListener() {
            @Override
            public void onMovedToScrapHeap(View view) {
                CachingAdapter.DownViewHolder holder = (CachingAdapter.DownViewHolder) view.getTag();
                if (holder == null) {
                    return;
                }
                CacheManager.DownHandler handler = (CacheManager.DownHandler) CacheUtil
                        .getDownloadQueue(CachingActivity.this)
                        .getDownloadHandlerById(holder.cacheVideoModel.getId());
                if (handler != null) {
                    handler.setHolder(null);
                }
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    /**
     * 为适配器添加数据源
     */
    private void setAdapterData(List<CacheVideoModel> downloadingList, ArrayList<CacheVideoModel> list) {
        list.clear();
        list.addAll(downloadingList);
        mAdapter.setShowList(list);
        updateEditorView(isSelectVideo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back: // 返回
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_CACHING);
                onBackPressed();
                break;
            case R.id.title_video:
                if (mIsEdit || isSelectVideo) {
                    return;
                }
                isSelectVideo = true;
                setTitleStyle();
                setAdapterData(CacheManager.getDownloadingList(!isSelectVideo), list);
                PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_TAB_VIDEO, null);
                break;
            case R.id.title_audio:
                if (mIsEdit || !isSelectVideo) {
                    return;
                }
                isSelectVideo = false;
                setTitleStyle();
                setAdapterData(CacheManager.getDownloadingList(!isSelectVideo), list);
                PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_TAB_AUDIO, null);

                break;
            case R.id.title_right_text: // 编辑
                if (mIsEdit) {
                    PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_MY_EDIT_FINISH, isSelectVideo);
                } else {
                    PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_MY_EDIT, isSelectVideo);
                }
                handleEditMode();
                break;
            case R.id.btn_delete: // 删除
                if (mAdapter.getSelectedDownload().size() > 0) {
                    new DeleteSelectedAsyncTask().execute();
                } else {
                    ToastUtils.getInstance().showShortToast(R.string.toast_unchecked_any_video);
                }
                PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_MY_DELETE, isSelectVideo);
                break;
            case R.id.btn_clear: // 清空
                showClearConfirmDialog();
                break;
            case R.id.caching_start: // 点击全部开始
                if (!mIsEdit) {
                    dealStartAllDownload();
                    PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_CACHE_START_ALL, isSelectVideo);
                }
                break;
            case R.id.caching_pause: // 点击全部暂停
                if (!mIsEdit) {
                    dealPauseAllDownload();
                    PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_CACHE_PAUSE_ALL, isSelectVideo);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        resetCheckedState();
        finish();
    }

    /**
     * 点击全部暂停
     */
    private void dealPauseAllDownload() {
        if (CacheManager.isCaching(this, isSelectVideo ? IfengType.TYPE_VIDEO : IfengType.TYPE_AUDIO)) {
            try {
                CacheUtil.getDownloadQueue(this).changeStateByGroup(DownloadOrder.STATE_PAUSE, isSelectVideo ? IfengType.TYPE_VIDEO : IfengType.TYPE_AUDIO);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**
     * 点击全部开始
     */
    private void dealStartAllDownload() {
        if (CacheManager.isAllCaching(this, !isSelectVideo)) {
            logger.debug("没有停止的任务，直接退出");
            return;
        }
        if (isStartingAll) {
            logger.debug("开始中，直接退出");
            return;
        }
        isStartingAll = true;
        netDealManager.dealNetTypeWithSetting(new NetDealListener() {
            @Override
            public void onDealByState(int state) {
                if (state != NO_ACTION) {
                    try {
                        CacheUtil.getDownloadQueue(CachingActivity.this).changeStateByGroup(state, isSelectVideo ? IfengType.TYPE_VIDEO : IfengType.TYPE_AUDIO);
                    } catch (IllegalParamsException | DownloadDBException e) {
                        logger.error(e.toString(), e);
                    }
                }
                isStartingAll = false;
            }
        });

    }

    /**
     * 处理编辑模式
     */
    private void handleEditMode() {
        if (!mIsEdit) {
            entryEditorMode();
        } else {
            exitEditorMode();
        }
    }

    /**
     * 进入编辑模式
     */
    private void entryEditorMode() {
        mIsEdit = true;
        mAdapter.setInEditMode(true);
        mEditBottom.setVisibility(View.VISIBLE);
        mSDcardCapacityInfoLayout.setVisibility(View.GONE);
        initTitleRightText(mIsEdit);
    }

    /**
     * 退出编辑模式
     */
    public void exitEditorMode() {
        mIsEdit = false;
        mAdapter.setInEditMode(false);
        mEditBottom.setVisibility(View.GONE);
        mSDcardCapacityInfoLayout.setVisibility(View.VISIBLE);
        initTitleRightText(mIsEdit);
        resetCheckedState();
    }

    /**
     * 重置选中状态
     */
    private void resetCheckedState() {
        mAdapter.getSelectedDownload().clear();
        getCheckedNum(0);
        for (CacheVideoModel model : list) {
            model.setCheckState(0);
        }
        mAdapter.notifyDataSetChanged();
    }

    /**
     * 清空时弹出的提示框
     */
    private void showClearConfirmDialog() {
        Resources res = this.getResources();
        dialog = AlertUtils.getInstance().showTwoBtnDialog(this,
                res.getString(R.string.cache_delete_video_confirm_msg),
                res.getString(R.string.common_popup_layout_clear),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PageActionTracker.clickMyList(1, ActionIdConstants.CLICK_MY_CLEAR, isSelectVideo);
                        closeClearConfigDialog();
                        mAdapter.getSelectedDownload().clear();//清除之前选中的、添加全部用于清空
                        mAdapter.getSelectedDownload().addAll(CacheManager.getDownloadingList(!isSelectVideo));
                        new DeleteSelectedAsyncTask().execute();
                    }
                },
                res.getString(R.string.common_cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        closeClearConfigDialog();
                    }
                });
    }

    /**
     * 关闭提醒Dialog
     */
    private void closeClearConfigDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * 刷新界面
     */
    public void refreshUI() {
        updateEditorView(isSelectVideo);
        //if (mAdapter.isInEditMode()) {
        list.removeAll(CacheManager.getDownloadAllList(!isSelectVideo));//数据源移除下载完成的，保证页面刷新
        mAdapter.getSelectedDownload().removeAll(CacheManager.getDownloadAllList(!isSelectVideo));//选中的数据移除下载完成的，保证选中的数量正确
        //}
        mAdapter.notifyDataSetChanged();
        getCheckedNum(mAdapter.getSelectedDownload().size());//每次下载完成重新计算选中的数量
    }

    /**
     * 根据正在下载列表是否为空，更新界面
     *
     * @param isVideo 选中的是否为视频
     */
    private void updateEditorView(boolean isVideo) {
        boolean isDataEmpty = ListUtils.isEmpty(CacheManager.getDownloadingList(!isVideo));
        if (isDataEmpty) {
            exitEditorMode();
        }
        listView.setVisibility(isDataEmpty ? View.GONE : View.VISIBLE);
        mAllDownloadEmptyView.setVisibility(isDataEmpty ? View.VISIBLE : View.GONE);
        title_right_text.setVisibility(isDataEmpty ? View.GONE : View.VISIBLE);
        mSDcardCapacityInfoLayout.setVisibility(isDataEmpty ? View.GONE : View.VISIBLE);
        emptyImg.setImageResource(isVideo ? R.drawable.cache_empty : R.drawable.cache_empty);
        emptyText.setText(isVideo ? R.string.cache_video_none : R.string.cache_audio_none);
    }

    /**
     * 1、显示正在删除dialog
     * 2、暂停正在下载的任务
     * 3、删除数据库和相应文件
     * 4、从正在下载列表中移除选中的任务
     * 5、删除完成后，清空选中的视频
     */
    private class DeleteSelectedAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //CacheUtil.getDownloadQueue(cachingActivity).hold(true);
            mAdapter.setDeleting(true);
            dialog = new Dialog(CachingActivity.this, R.style.common_Theme_Dialog);
            dialog.setContentView(R.layout.cache_dialog_progress);
            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText(CachingActivity.this.getString(R.string.cache_wait_delete));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            List<Integer> ids = new ArrayList<>();
            for (CacheVideoModel cacheVideoModel : mAdapter.getSelectedDownload()) {
                DownloadRecord record = new DownloadRecord(cacheVideoModel.getGuid(), cacheVideoModel.getName(), "cancel", PageIdConstants.MY_CACHING);//统计
                CustomerStatistics.sendDownload(record);
                ids.add(cacheVideoModel.getId());
            }
            try {
                CacheUtil.getDownloadQueue(CachingActivity.this).changeStateByIds(ids, DownloadOrder.STATE_STOP);
                CacheVideoDao.deleteList(CachingActivity.this, ids);
            } catch (Exception e) {
                CacheManager.synchronizeDB(CachingActivity.this);
                logger.error("DeleteSelectedAsyncTask doInBackground failed \n", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            exitEditorMode();
            getCheckedNum(0);//设置删除按钮数量为0
            mAdapter.setDeleting(false);
            CacheManager.getDownloadingList(!isSelectVideo).removeAll(mAdapter.getSelectedDownload());
            mAdapter.getSelectedDownload().clear();
            CacheManager.refreshDownloadVideos(CachingActivity.this);
            if (CacheManager.mDownloadingList.size() <= 0) {
                finish();
            }
            setAdapterData(CacheManager.getDownloadingList(!isSelectVideo), list);
        }
    }
}
