package com.ifeng.newvideo.cache;

import android.content.Context;

/**
 * 用户设置中“缓存路径”改变回调
 * Created by cuihz on 2014/11/18.
 */
public interface StorageSettingChangedListener {
    void onStorageSettingChanged(Context context);
}
