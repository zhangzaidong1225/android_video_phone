package com.ifeng.newvideo.cache;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.dialogUI.DialogUtilsFor3G;
import com.ifeng.newvideo.dialogUI.DownloadDialogInActivity;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.download.DownloadInfo;
import com.ifeng.video.core.download.DownloadOrder;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.dao.CacheVideoDao;
import com.ifeng.video.dao.db.dao.VideoPlayDao;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 网络管理类
 * Created by cuihz on 2014/11/19.
 */
public class NetDealManager implements NetSettingChangedListener, DialogUtilsFor3G.DialogCallBackFor3G {
    private static final Logger logger = LoggerFactory.getLogger(NetDealManager.class);

    private Context context;
    public DialogUtilsFor3G dialogUtilsFor3G;
    private DialogReceiver dialogReceiver;
    private NetDealListener netDealListener;
    private AtomicInteger atomicInteger;
    private List<CacheVideoModel> newCacheVideoList;
    private List<DownloadInfo> newDownloadInfoList;
    private boolean fromActivity;
    private Dialog cacheDialog;

    public NetDealManager(Context context) {
        fromActivity = context instanceof Activity;
        this.context = context;
        dialogUtilsFor3G = new DialogUtilsFor3G();
        dialogReceiver = new DialogReceiver();
    }

    public NetDealManager() {
    }

    public void initDialogState() {
        dialogUtilsFor3G.reset();
    }


    public void dealNetTypeWithSetting(NetDealListener listener) {
        if (!fromActivity && DownloadDialogInActivity.downloadDialogInActivity != null) {
            DownloadDialogInActivity.downloadDialogInActivity.finish();
        }
        newCacheVideoList = new ArrayList<CacheVideoModel>();
        newDownloadInfoList = new ArrayList<DownloadInfo>();
        dialogUtilsFor3G.setDialogCallBack(this);
        this.netDealListener = listener;
        if (NetUtils.isNetAvailable(context)) {
            logger.debug("开始处理网络");
            if (NetUtils.isMobile(context)) {
                boolean isMobileOpen = SharePreUtils.getInstance().getCacheVideoState();
                if (isMobileOpen) {
                    logger.debug("允许运营商网络缓存 --- 开关开启");
                    // 其他运营商网络，弹出网络提示框————“亲，您现在使用的是运营商网络，继续缓存可能会产生流量费用，建议您改用wifi网络 ”
                    logger.debug("其他运营商网络");
                    if (fromActivity) {
                        if (IfengApplication.getInstance().getShowContinueCacheVideoDialog()) {
                            showContinueCacheVideoDialog();
                        } else {
                            ToastUtils.getInstance().showShortToast(context.getString(R.string.cache_alert_text));
                            onClickDialogBtn(DialogUtilsFor3G.FLAG_DIALOG_CONTINUE);
                        }
                    } else {
                        netDealListener.onDealByState(DownloadOrder.STATE_WAIT_DOWN);
                        netDealListener = null;
                    }
                } else {
                    logger.debug("允许运营商网络缓存 --- 开关关闭");
                    if (fromActivity) {
                        ToastUtils.getInstance().showShortToast(R.string.cache_added_download_only_wifi);
                        changeDownloadUrls(false);
                    } else {
                        netDealListener.onDealByState(DownloadOrder.STATE_WAIT_DOWN);
                        netDealListener = null;
                    }
                }
            } else {
                logger.debug("wifi网络");
                if (fromActivity) {
                    changeDownloadUrls(false);
                } else {
                    netDealListener.onDealByState(DownloadOrder.STATE_WAIT_DOWN);
                    netDealListener = null;
                }
            }
        } else {
            logger.debug("网络不可用");
            if (fromActivity) {
                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            } else {
                ToastUtils.getInstance().showLongToast(R.string.cache_curDownloadVideo_paused);
            }
            netDealListener.onDealByState(NetDealListener.NO_ACTION);
            netDealListener = null;
        }
    }

    /**
     * 展示是否继续缓存的Dialog
     */
    private void showContinueCacheVideoDialog() {
        cacheDialog = DialogUtilsFor3G.showContinueCacheVideoDialog(context,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        IfengApplication.getInstance().setShowContinueCacheVideoDialog(false);
                        if (cacheDialog != null && cacheDialog.isShowing()) {
                            cacheDialog.dismiss();
                        }
                        onClickDialogBtn(DialogUtilsFor3G.FLAG_DIALOG_CONTINUE);
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cacheDialog != null && cacheDialog.isShowing()) {
                            cacheDialog.dismiss();
                        }
                        netDealListener.onDealByState(NetDealListener.NO_ACTION);
                        netDealListener = null;
                    }
                });
        cacheDialog.setCancelable(false);
        cacheDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * 一个按钮“我知道了”提示框
     *
     * @param message
     */
    private void showOneButtonDialog(String message) {
        AlertUtils.getInstance().showOneBtnDialog(context, message, context.getString(R.string.cache_i_know), null);
    }


    @Override
    public void onClickDialogBtn(int flag) {
        switch (flag) {
            case DialogUtilsFor3G.FLAG_DIALOG_CANCEL:
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CANCEL_DOWNLOAD:
                netDealListener.onDealByState(DownloadOrder.STATE_PAUSE);
                netDealListener = null;
                break;
            case DialogUtilsFor3G.FLAG_DIALOG_CONTINUE:
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CONTINUE_DOWNLOAD:
                changeDownloadUrls(false);
                break;
            case DialogUtilsFor3G.FLAG_DIALOG_NO_MOBILE_OPEN:
                netDealListener.onDealByState(NetDealListener.NO_ACTION);
                netDealListener = null;
                break;
        }
    }

    /**
     * 根据状态替换下载url
     *
     * @param isVip
     */
    private void changeDownloadUrls(boolean isVip) {
        if (CacheManager.mDownloadingList == null || CacheManager.mDownloadingList.size() == 0) {
            netDealListener.onDealByState(DownloadOrder.STATE_WAIT_DOWN);
            netDealListener = null;
            return;
        }
        atomicInteger = new AtomicInteger();
        atomicInteger.set(CacheManager.mDownloadingList.size());
        for (CacheVideoModel cacheVideoModel : CacheManager.mDownloadingList) {
            if (isVip == isFreeUrl(cacheVideoModel.getDownUrl())) {
                isOver();
            } else {
                VideoPlayDao.getSingleVideoByGuidForDownload(cacheVideoModel.getGuid(), new PlayerInfoModel(),
                        new MyResponseListener(context, cacheVideoModel),
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                isOver();
                                logger.error("切换失败");
                            }
                        });
            }
        }
    }

    class MyResponseListener implements Response.Listener<PlayerInfoModel> {
        private final Context context;
        private CacheVideoModel cacheVideoModel;

        public MyResponseListener(Context context, CacheVideoModel cacheVideoModel) {
            this.context = context;
            this.cacheVideoModel = cacheVideoModel;
        }

        @Override
        public void onResponse(PlayerInfoModel playerInfoModel) {
            if (playerInfoModel == null) {
                isOver();
                return;
            }
            int stream = getStreamBySize(playerInfoModel, cacheVideoModel.getTotalSize());
            long createTime = cacheVideoModel.getCreateTime();
            //TODO　ｔｙｐｅ需要动态传入
            cacheVideoModel = CacheUtil.playInfoToCacheVideoModel(playerInfoModel,
                    cacheVideoModel.getState(), stream, IfengType.TYPE_VIDEO, cacheVideoModel.getPath());
            cacheVideoModel.setCreateTime(createTime);
            newCacheVideoList.add(cacheVideoModel);
            DownloadInfo downloadInfo = CacheUtil.cacheVideoModelToDownloadInfo(cacheVideoModel);
            newDownloadInfoList.add(downloadInfo);
            isOver();

        }
    }


    /**
     * 替换下载url是否已经结束
     */
    private void isOver() {
        int count = atomicInteger.decrementAndGet();
        if (count <= 0) {
            try {
                CacheUtil.getDownloadQueue(context).changeDownloadInfoByList(newDownloadInfoList);
                CacheVideoDao.saveList(context, newCacheVideoList);
                netDealListener.onDealByState(DownloadOrder.STATE_WAIT_DOWN);
                netDealListener = null;
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }

        }
    }


    /**
     * 是否是联通免流量url
     *
     * @param url
     * @return
     */
    private boolean isFreeUrl(String url) {
        return url.contains("cuff");
    }


    private int getStreamBySize(PlayerInfoModel playerInfoModel, long totalSize) {
        long size = totalSize / 1024;

        if (Math.abs(size - playerInfoModel.media_original_filesize) < 10) {
            return MediaConstants.STREAM_ORIGINAL;
        }
        if (Math.abs(size - playerInfoModel.media_supper_filesize) < 10) {
            return MediaConstants.STREAM_SUPPER;
        }
        if (Math.abs(size - playerInfoModel.media_high_filesize) < 10) {
            return MediaConstants.STREAM_HIGH;
        }
        if (Math.abs(size - playerInfoModel.media_middle_filesize) < 10) {
            return MediaConstants.STREAM_MID;
        } else {
            return MediaConstants.STREAM_LOW;
        }
    }


    /**
     * 用户在设置中改变3G设置的处理
     *
     * @param event
     */
    @Override
    public void onNetSettingSwitchChanged(Context context, int event) {
        switch (event) {
            case NetSettingChangedListener.SETTING_SWITCH_NET_OFF:// 我的设置中关闭了2G/3G网络缓存"
                if (NetUtils.isNetAvailable(context) && !NetUtils.isMobile(context)) {
                    return;
                }
//                try {
//                    CacheUtil.getDownloadQueue(context).changeStateByGroup(DownloadOrder.STATE_PAUSE);
//                } catch (IllegalParamsException e) {
//                    logger.error(e.toString(), e);
//                } catch (DownloadDBException e) {
//                    logger.error(e.toString(), e);
//                }

                CacheManager.stopCaching(context);
                if (CacheManager.isCaching(context, IfengType.TYPE_All)) {
                    ToastUtils.getInstance().showLongToast(R.string.cache_curDownloadVideo_paused);
                }
                break;
            case NetSettingChangedListener.SETTING_SWITCH_NET_ON:
                if (NetUtils.isNetAvailable(context) && !NetUtils.isMobile(context)) {
                    return;
                }
                CacheManager.startCaching(context);
                break;
        }
    }

    private void startActivity(String action) {
        Intent intent = new Intent(context, DownloadDialogInActivity.class);
        intent.putExtra(DownloadDialogInActivity.KEY_ACTION, action);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadDialogInActivity.ACTION_DIALOG_CANCEL);
        intentFilter.addAction(DownloadDialogInActivity.ACTION_DIALOG_CONTINUE);
        intentFilter.addAction(DownloadDialogInActivity.ACTION_DIALOG_NOACTION);
        intentFilter.addAction(DownloadDialogInActivity.ACTION_DIALOG_ORDER);
        context.getApplicationContext().registerReceiver(dialogReceiver, intentFilter);
    }

    class DialogReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DownloadDialogInActivity.ACTION_DIALOG_CANCEL)) {
                logger.debug("activity dialog 取消");
                netDealListener.onDealByState(DownloadOrder.STATE_PAUSE);
                netDealListener = null;
            }
            if (action.equals(DownloadDialogInActivity.ACTION_DIALOG_CONTINUE)) {
                logger.debug("activity dialog 继续");
                changeDownloadUrls(false);
            }
            context.unregisterReceiver(this);
        }
    }
}
