package com.ifeng.newvideo.member;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.member.adapter.DealRecordAdapter;
import com.ifeng.newvideo.member.bean.DealRecordData;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ll on 2017/7/14.
 */
public class DealRecordActivity extends BaseFragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView mListView;
    private DealRecordAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vip_deal_record_activity);
        initViews();
        mAdapter = new DealRecordAdapter(this);
        mListView.setAdapter(mAdapter);
        requestVipMarketPolicy();
    }

    private void initViews() {
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.vip_deal_record_title));
        findViewById(R.id.back).setOnClickListener(this);
        mListView = (ListView) findViewById(R.id.listView_deal_record);
        mListView.setOnItemClickListener(this);
    }

    private void requestVipMarketPolicy() {
        String url = String.format(DataInterface.VIP_DEAL_RECORD, User.getUid(), PhoneConfig.userKey);
        CommonDao.sendRequest(url, null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }

                        JSONObject json = null;
                        try {
                            json = JSONObject.parseObject(response.toString());
                            if (json == null) {
                                return;
                            }
                            List<DealRecordData> entityList = new ArrayList<DealRecordData>();
                            JSONArray jsonArray = json.getJSONArray("orderList");
                            for (Object aJsonArray : jsonArray) {
                                DealRecordData entity = new DealRecordData();
                                JSONObject jsonObject = (JSONObject) aJsonArray;
                                entity.setOrderName(jsonObject.getString("orderName"));
                                entity.setOrderPayMoney(jsonObject.getString("orderPayMoney"));
                                entity.setOrderDuration(jsonObject.getString("orderDuration"));
                                entity.setOrderEffectiveDate(jsonObject.getString("orderEffectiveDate"));
                                entity.setOrderExpireDate(jsonObject.getString("orderExpireDate"));
                                entity.setOrderPayDate(jsonObject.getString("orderPayDate"));
                                entity.setOrderId(jsonObject.getString("orderId"));
                                entity.setOrderPayType(jsonObject.getString("orderPayType"));
                                entityList.add(entity);
                            }

                            if (ListUtils.isEmpty(entityList)) {
                            } else {
                                mAdapter.setList(entityList);
                            }
                        } catch (Exception e) {
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
