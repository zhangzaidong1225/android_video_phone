package com.ifeng.newvideo.member.bean;

import java.io.Serializable;

/**
 * Created by ll on 2017/7/20.
 */
public class PointTaskBean implements Serializable {

    private static final long serialVersionUID = 6993120463974992663L;

    private boolean done;//是否完成
    private String times;//当日完成次数
    private int value;//当日积分数
    private String desc;//介绍

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
