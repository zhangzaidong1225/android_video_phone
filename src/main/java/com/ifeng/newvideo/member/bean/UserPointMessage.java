package com.ifeng.newvideo.member.bean;

import java.io.Serializable;

/**
 * Created by ll on 2017/7/24.
 */
public class UserPointMessage implements Serializable {
    private static final long serialVersionUID = -4828359222253611889L;
    private int code;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable{
        private static final long serialVersionUID = 6021259683831049966L;
        private int opcode;
        private int point;
        private String msg;
        private int days;

        public int getOpcode() {
            return opcode;
        }

        public void setOpcode(int opcode) {
            this.opcode = opcode;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public int getDays() {
            return days;
        }

        public void setDays(int days) {
            this.days = days;
        }
    }

}
