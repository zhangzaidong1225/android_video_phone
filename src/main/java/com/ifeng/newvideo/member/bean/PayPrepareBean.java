package com.ifeng.newvideo.member.bean;

import java.io.Serializable;

/**
 * Created by ll on 2017/7/13.
 */
public class PayPrepareBean implements Serializable {
    private static final long serialVersionUID = 2220633259561031640L;
    private String appid;
    private String partnerid;
    private String prepayid;
    private String pkg;
    private String noncestr;
    private String timestamp;
    private String sign;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "PayPrepareBean{" +
                "\nappid='" + appid + '\'' +
                ", \npartnerid='" + partnerid + '\'' +
                ", \nprepayid='" + prepayid + '\'' +
                ", \npkg='" + pkg + '\'' +
                ", \nnoncestr='" + noncestr + '\'' +
                ", \ntimestamp='" + timestamp + '\'' +
                ", \nsign='" + sign + '\'' +
                '}';
    }
}
