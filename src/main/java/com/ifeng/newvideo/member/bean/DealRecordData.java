package com.ifeng.newvideo.member.bean;

/**
 * 购买记录
 * Created by ll on 2017/7/14.
 */
public class DealRecordData {
    private String orderName;//订单名称
    private String orderDuration;//购买时长
    private String orderPayMoney;//订单金额
    private String orderEffectiveDate;//订单有效日期
    private String orderExpireDate;//订单失效日期
    private String orderEXP;//订单有效期
    private String orderPayDate;//订单交易日期
    private String orderId;//订单流水号
    private String orderPayType;//订单支付方式

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderDuration() {
        return orderDuration;
    }

    public void setOrderDuration(String orderDuration) {
        this.orderDuration = orderDuration;
    }

    public String getOrderPayMoney() {
        return orderPayMoney;
    }

    public void setOrderPayMoney(String orderPayMoney) {
        this.orderPayMoney = orderPayMoney;
    }

    public String getOrderEXP() {
        return orderEXP;
    }

    public void setOrderEXP(String orderEXP) {
        this.orderEXP = orderEXP;
    }

    public String getOrderPayDate() {
        return orderPayDate;
    }

    public void setOrderPayDate(String orderPayDate) {
        this.orderPayDate = orderPayDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderPayType() {
        return orderPayType;
    }

    public void setOrderPayType(String orderPayType) {
        this.orderPayType = orderPayType;
    }

    public String getOrderEffectiveDate() {
        return orderEffectiveDate;
    }

    public void setOrderEffectiveDate(String orderEffectiveDate) {
        this.orderEffectiveDate = orderEffectiveDate;
    }

    public String getOrderExpireDate() {
        return orderExpireDate;
    }

    public void setOrderExpireDate(String orderExpireDate) {
        this.orderExpireDate = orderExpireDate;
    }

    @Override
    public String toString() {
        return "DealRecordData{" +
                "orderName='" + orderName + '\'' +
                ", orderDuration='" + orderDuration + '\'' +
                ", orderPayMoney='" + orderPayMoney + '\'' +
                ", orderEffectiveDate='" + orderEffectiveDate + '\'' +
                ", orderExpireDate='" + orderExpireDate + '\'' +
                ", orderEXP='" + orderEXP + '\'' +
                ", orderPayDate='" + orderPayDate + '\'' +
                ", orderId='" + orderId + '\'' +
                ", orderPayType='" + orderPayType + '\'' +
                '}';
    }
}
