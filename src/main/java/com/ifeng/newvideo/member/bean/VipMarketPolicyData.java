package com.ifeng.newvideo.member.bean;

/**
 * Created by ll on 2017/7/10.
 */
public class VipMarketPolicyData {
    public static final String JSON_ARRAY = "marketingList";
    private String salesName;
    private String salesActivities;
    private String salesMoney;
    private String isRecommend;
    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getSalesActivities() {
        return salesActivities;
    }

    public void setSalesActivities(String salesActivities) {
        this.salesActivities = salesActivities;
    }

    public String getSalesMoney() {
        return salesMoney;
    }

    public void setSalesMoney(String salesMoney) {
        this.salesMoney = salesMoney;
    }

    public String getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(String isRecommend) {
        this.isRecommend = isRecommend;
    }

    @Override
    public String toString() {
        return "VipMarketPolicyData{" +
                "salesName='" + salesName + '\'' +
                ", salesActivities='" + salesActivities + '\'' +
                ", salesMoney='" + salesMoney + '\'' +
                ", isRecommend='" + isRecommend + '\'' +
                ", productId='" + productId + '\'' +
                '}';
    }
}
