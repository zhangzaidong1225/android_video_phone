package com.ifeng.newvideo.member;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.member.adapter.PointTaskAdapter;
import com.ifeng.newvideo.member.adapter.PointTaskNewAdapter;
import com.ifeng.newvideo.member.bean.PointTaskBean;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.widget.ListViewForScrollView;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ll on 2017/7/20.
 */
public class PointTaskActivity extends BaseFragmentActivity implements View.OnClickListener {
    private List<PointTaskBean> mDataList = new ArrayList<>();
    private ListViewForScrollView mPointListView;
    private PointTaskAdapter mAdapter;
    private List<PointTaskBean> newbieList = new ArrayList<>();
    private ListViewForScrollView lvPointViewNew;
    private PointTaskNewAdapter newBieAdapter;
    private String mTodayPoint;
    private TextView mTodayPointView;
    private LinearLayout mLoadingView;
    private RelativeLayout mTodayPointTitle;
    private TextView tvNormalTask;
    private TextView tvNewbieTask;
    private View noNet;
    private View errorView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.point_task_activity_layout);
        initView();
//        setVisibilityByData(!ListUtils.isEmpty(mDataList));
        if (NetUtils.isNetAvailable(this)) {
            requestNetData();
            loadingStatus();
        } else {
            noNetStatus();
        }
    }

    private void noNetStatus() {
        noNet.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
        mTodayPointTitle.setVisibility(View.GONE);
        tvNewbieTask.setVisibility(View.GONE);
        tvNormalTask.setVisibility(View.GONE);
    }

    private void loadingStatus() {
        mLoadingView.setVisibility(View.VISIBLE);
        mTodayPointTitle.setVisibility(View.GONE);
        noNet.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        tvNewbieTask.setVisibility(View.GONE);
        tvNormalTask.setVisibility(View.GONE);
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(R.string.point_task_title);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.tv_get_point).setOnClickListener(this);
        mPointListView = (ListViewForScrollView) findViewById(R.id.listView_point_task);
        mAdapter = new PointTaskAdapter(this);
        mPointListView.setAdapter(mAdapter);
        lvPointViewNew = (ListViewForScrollView) findViewById(R.id.lv_point_task_new);
        newBieAdapter = new PointTaskNewAdapter(this);
        lvPointViewNew.setAdapter(newBieAdapter);
        mTodayPointView = (TextView) findViewById(R.id.tv_today_point);
        mLoadingView = (LinearLayout) findViewById(R.id.ll_loading);
        mTodayPointTitle = (RelativeLayout) findViewById(R.id.rl_today_point_title);
        tvNormalTask = (TextView) findViewById(R.id.tv_normal_task);
        tvNewbieTask = (TextView) findViewById(R.id.tv_newbie_task);
        noNet = findViewById(R.id.no_net_layer);
        errorView = findViewById(R.id.load_fail_layout);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingStatus();
                requestNetData();
            }
        });

    }

    private void setVisibilityByData(boolean hasData) {
        if (hasData) {
            mLoadingView.setVisibility(View.GONE);
            mTodayPointTitle.setVisibility(View.VISIBLE);
            tvNewbieTask.setVisibility(View.VISIBLE);
            tvNormalTask.setVisibility(View.VISIBLE);
        } else {
            mLoadingView.setVisibility(View.VISIBLE);
            mTodayPointTitle.setVisibility(View.GONE);
        }
    }

    private List<String> dailyKeyList = new ArrayList<>();
    private List<String> newbieKeyList = new ArrayList<>();

    private void requestNetData() {
        String url = String.format(DataInterface.GET_POINT_TASK, new User(this).getUid(), PhoneConfig.mos, PhoneConfig.userKey, "", User.getIfengToken()) + "&is_vip=" + (User.isVip() ? "1" : "0");
        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        JSONObject jsonObject = JSON.parseObject(response.toString());
                        int code = jsonObject.getIntValue("code");
                        if (200 == code) {
                            try {

                                JSONObject data = jsonObject.getJSONObject("data");
                                JSONObject dailyList = data.getJSONObject("list");
                                JSONObject newbieOnceList = data.getJSONObject("once_list");
                                mTodayPoint = data.getString("total_point");
                                if (EmptyUtils.isNotEmpty(dailyList)) {
                                    Iterator<String> iterable = dailyList.keySet().iterator();
                                    while (iterable.hasNext()) {
                                        dailyKeyList.add(iterable.next());
                                    }
                                    for (int i = 0; i < dailyKeyList.size(); i++) {
                                        JSONObject temp = dailyList.getJSONObject(dailyKeyList.get(i));
                                        PointTaskBean bean = new PointTaskBean();
                                        bean.setDone(temp.getBoolean("done"));
                                        bean.setValue(temp.getIntValue("value"));
                                        bean.setTimes(temp.getString("times"));
                                        bean.setDesc(temp.getString("desc"));
                                        mDataList.add(bean);
                                    }
                                    mAdapter.setData(mDataList);
                                }

                                if (EmptyUtils.isNotEmpty(newbieOnceList)) {

                                    Iterator<String> newbieIterable = newbieOnceList.keySet().iterator();
                                    while (newbieIterable.hasNext()) {
                                        newbieKeyList.add(newbieIterable.next());
                                    }
                                    for (int i = 0; i < newbieKeyList.size(); i++) {
                                        JSONObject temp = newbieOnceList.getJSONObject(newbieKeyList.get(i));
                                        PointTaskBean bean = new PointTaskBean();
                                        bean.setDone(temp.getBoolean("done"));
                                        bean.setValue(temp.getIntValue("value"));
                                        bean.setTimes(temp.getString("times"));
                                        bean.setDesc(temp.getString("desc"));
                                        newbieList.add(bean);
                                    }
                                    newBieAdapter.setData(newbieList);
                                }

                                String str = "<font color='#F54343'>" + (TextUtils.isEmpty(mTodayPoint) ? "0" : mTodayPoint) + "</font>";
                                str = String.format(getString(R.string.point_task_today_got), str);
                                mTodayPointView.setText(Html.fromHtml(str));
                                setVisibilityByData(!ListUtils.isEmpty(mDataList));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorStatus();
                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    private void errorStatus() {
        errorView.setVisibility(View.VISIBLE);
        mTodayPointTitle.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
        noNet.setVisibility(View.GONE);
        tvNewbieTask.setVisibility(View.GONE);
        tvNormalTask.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.tv_get_point:
                PageActionTracker.clickBtn(ActionIdConstants.POINT_TASK_CLICK_GET_POINT, PageIdConstants.PAGE_MY);
                IntentUtils.startADActivity(this, null, DataInterface.HOW_TO_GET_POINT_H5, null, ADActivity.FUNCTION_VALUE_H5_STATIC, getString(R.string.point_task_how_got), null, null, null, null, null, null);
                break;
            default:
                break;
        }

    }
}
