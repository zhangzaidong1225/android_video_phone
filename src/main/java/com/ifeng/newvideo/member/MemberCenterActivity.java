package com.ifeng.newvideo.member;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BitmapUtils;
import com.ifeng.video.dao.db.constants.DataInterface;

/**
 * Created by ll on 2017/7/10.
 */
public class MemberCenterActivity extends BaseFragmentActivity implements View.OnClickListener {
    private TextView mUserName, mUserStatusDes, mTitle, mRenew;
    private NetworkImageView mUserHeader;
    private boolean isLogin = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vip_member_center_activity);
        isLogin = User.isLogin();
//        if (!isLogin || !User.isVip()) {
//            finish();
//        }
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadViewWithData();
    }

    private void initViews() {
        mTitle = (TextView) findViewById(R.id.title);
        mTitle.setText(getString(R.string.vip_mine_item_member_is_vip));

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.rl_deal).setOnClickListener(this);
        findViewById(R.id.rl_service_agreement).setOnClickListener(this);
        mUserHeader = (NetworkImageView) findViewById(R.id.img_user_header);
        mUserName = (TextView) findViewById(R.id.tv_username);
        mUserStatusDes = (TextView) findViewById(R.id.tv_vip_due);
        mRenew = (TextView) findViewById(R.id.tv_vip_renew);
        mRenew.setOnClickListener(this);
    }

    private void loadViewWithData() {
        VolleyHelper.getImageLoader().get(User.getUserIcon(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (null != response && null != response.getBitmap()) {
                    mUserHeader.setImageBitmap(BitmapUtils.makeRoundCorner(response.getBitmap()));
                } else {
                    mUserHeader.setImageResource(R.drawable.member_center_header_user_default_header);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                mUserHeader.setImageResource(R.drawable.member_center_header_user_default_header);
            }
        });
        mUserName.setText(User.getUserName());
        mUserStatusDes.setText(String.format(getString(R.string.vip_open_member_due_date), User.getVipdate()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_vip_renew:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VIP_CENTER_RENEW_VIP, PageIdConstants.VIP_USER_CENTER);
                IntentUtils.startOpenMemberActivity(this);
                break;
            case R.id.rl_deal:
                IntentUtils.startDealRecordActivity(this);
                break;
            case R.id.rl_service_agreement:
                IntentUtils.startADActivity(this, null, DataInterface.VIP_MEMBER_AGREEMENT, null, ADActivity.FUNCTION_VALUE_H5_STATIC, getString(R.string.vip_open_member_service_agreement), null, null, null, null, null, null);
                break;
            case R.id.back:
                finish();
                break;
        }
    }
}
