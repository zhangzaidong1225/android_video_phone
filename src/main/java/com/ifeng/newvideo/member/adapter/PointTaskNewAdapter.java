package com.ifeng.newvideo.member.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.member.bean.PointTaskBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ll on 2017/7/21.
 */
public class PointTaskNewAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<PointTaskBean> mList = new ArrayList<>();
    public PointTaskNewAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<PointTaskBean> list) {
        mList = list;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PointTaskBean getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.point_task_activity_lv_item_new_layout, null);
            viewHolder.taskAxis = (ImageView) convertView.findViewById(R.id.iv_task_axis);
            viewHolder.taskName = (TextView) convertView.findViewById(R.id.tv_task_name);
            viewHolder.taskPoint = (TextView) convertView.findViewById(R.id.tv_task_point);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            if (mList.get(position).isDone()) {
                viewHolder.taskAxis.setBackgroundResource(R.drawable.user_point_complete);
            } else {
                viewHolder.taskAxis.setBackgroundResource(R.drawable.user_point_uncomplete);
            }
        } else if (position == getCount() - 1) {
            if (mList.get(position).isDone()) {
                viewHolder.taskAxis.setBackgroundResource(R.drawable.user_center_point_task_axis_bottom_selected);
            } else {
                viewHolder.taskAxis.setBackgroundResource(R.drawable.user_center_point_task_axis_bottom_normal);
            }
        } else {
            if (mList.get(position).isDone()) {
                viewHolder.taskAxis.setBackgroundResource(R.drawable.user_center_point_task_axis_mid_selected);
            } else {
                viewHolder.taskAxis.setBackgroundResource(R.drawable.user_center_point_task_axis_mid_normal);
            }
        }
        viewHolder.taskPoint.setText("+"+mList.get(position).getValue());
        if (mList.get(position).isDone()) {
            viewHolder.taskName.setText(String.format(mContext.getString(R.string.point_task_completed), mList.get(position).getDesc()));
            viewHolder.taskName.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.tv_notice_text_color));
            viewHolder.taskPoint.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.tv_living_text_color));
        } else {
            viewHolder.taskName.setText(String.format(mContext.getString(R.string.point_task_yet), mList.get(position).getDesc()));
            viewHolder.taskName.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.tv_end_text_color));
            viewHolder.taskPoint.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.tv_end_text_color));
        }
        return convertView;
    }
    class ViewHolder {
        ImageView taskAxis;
        TextView taskName, taskPoint;
    }
}
