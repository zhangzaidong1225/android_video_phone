package com.ifeng.newvideo.member.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.member.bean.VipMarketPolicyData;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

/**
 * Created by ll on 2017/7/10.
 */
public class MemberMarketPolicyAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<VipMarketPolicyData> list = new ArrayList<>();
    int mSelectPosition = 0;

    public MemberMarketPolicyAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }
    public void setList(List<VipMarketPolicyData> list) {
        this.list = list;
        notifyDataSetChanged();
    }
    public void changeSelected(int position){
        if(position != mSelectPosition){
            mSelectPosition = position;
            notifyDataSetChanged();
        }
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public VipMarketPolicyData getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
        // return list.get(position).getProductId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.vip_open_member_maket_policy_item, null);
            viewHolder.policyName = (TextView)convertView.findViewById(R.id.tv_policy_name);
            viewHolder.policyDes = (TextView)convertView.findViewById(R.id.tv_market_policy_des);
            viewHolder.policyPrice = (TextView)convertView.findViewById(R.id.tv_market_policy_price);
            viewHolder.policyRcommend = (ImageView)convertView.findViewById(R.id.im_recommend);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.policyName.setText(list.get(position).getSalesName());
        viewHolder.policyDes.setText(list.get(position).getSalesActivities());
        viewHolder.policyPrice.setText(list.get(position).getSalesMoney());
        viewHolder.policyRcommend.setVisibility("1".equals(list.get(position).getIsRecommend()) ? View.VISIBLE : GONE);
        if (mSelectPosition == position) {
            convertView.setBackgroundResource(R.drawable.vip_open_member_market_item_bg_select);
        } else {
            convertView.setBackgroundResource(R.drawable.vip_open_member_market_item_bg_normal);
        }
        return convertView;
    }

    class ViewHolder {
        TextView policyName, policyDes,  policyPrice;
        ImageView policyRcommend;
    }

}
