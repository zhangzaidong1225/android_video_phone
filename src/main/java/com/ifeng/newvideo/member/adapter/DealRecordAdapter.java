package com.ifeng.newvideo.member.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.member.bean.DealRecordData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ll on 2017/7/14.
 */
public class DealRecordAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater layoutInflater;
    private List<DealRecordData> mList = new ArrayList<>();

    public DealRecordAdapter(Context context) {
        mContext = context;
        layoutInflater = LayoutInflater.from(mContext);
    }

    public void setList(List<DealRecordData> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public DealRecordData getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.vip_deal_record_item, null);
            viewHolder.orderName = (TextView) convertView.findViewById(R.id.tv_order_title);
            viewHolder.orderDuration = (TextView) convertView.findViewById(R.id.tv_order_duration);
            viewHolder.orderPrice = (TextView) convertView.findViewById(R.id.tv_order_price);
            viewHolder.orderEXP = (TextView) convertView.findViewById(R.id.tv_orderexp);
            viewHolder.orderDate = (TextView) convertView.findViewById(R.id.tv_orderdate);
            viewHolder.orderType = (TextView) convertView.findViewById(R.id.tv_ordertype);
            viewHolder.orderId = (TextView) convertView.findViewById(R.id.tv_orderid);
            viewHolder.dividerView = convertView.findViewById(R.id.order_record_divider);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        DealRecordData data = mList.get(position);
        viewHolder.orderName.setText(data.getOrderName());
        viewHolder.orderDuration.setText(data.getOrderDuration());
        viewHolder.orderPrice.setText(data.getOrderPayMoney());
        viewHolder.orderEXP.setText(String.format(mContext.getString(R.string.vip_member_record_order_exp_text), data.getOrderEffectiveDate(), data.getOrderExpireDate()));
        viewHolder.orderDate.setText(data.getOrderPayDate());
        String payType = "";
        if ("1".equalsIgnoreCase(data.getOrderPayType())) {
            payType = viewHolder.orderDuration.getContext().getString(R.string.vip_open_member_wx_pay);
        }
        viewHolder.orderType.setText(payType);
        viewHolder.orderId.setText(data.getOrderId());

        boolean isLastItem = position == mList.size() - 1;
        viewHolder.dividerView.setVisibility(isLastItem ? View.GONE : View.VISIBLE);
        return convertView;
    }

    static class ViewHolder {
        TextView orderName, orderPrice, orderDate, orderEXP, orderType, orderId, orderDuration;
        View dividerView;
    }
}
