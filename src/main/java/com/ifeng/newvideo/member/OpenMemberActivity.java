package com.ifeng.newvideo.member;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.member.adapter.MemberMarketPolicyAdapter;
import com.ifeng.newvideo.member.bean.PayPrepareBean;
import com.ifeng.newvideo.member.bean.VipMarketPolicyData;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.widget.ListViewForScrollView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.ArrayList;
import java.util.List;

import static com.ifeng.newvideo.wxapi.WXPayEntryActivity.APP_ID;

/**
 * 开通会员/续费
 * Created by ll on 2017/7/5.
 */
public class OpenMemberActivity extends BaseFragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private boolean isLogin = false;
    private User user;
    private List<VipMarketPolicyData> mVipMarketPolicyList = new ArrayList<>();
    private MemberMarketPolicyAdapter mAdapter;
    private ListViewForScrollView mPolicyListView;
    private TextView mPayNum, mPayCommit, mUserName, mLoginBtn, mUserStatusDes, mTitle;
    private NetworkImageView mUserHeader;
    private LinearLayout mMarketPolicy;
    private String mCurrentProductId;
    public static String mSelectedPrice;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vip_open_member_activity);
        isLogin = User.isLogin();
        user = new User(this);
        initViews();
        mAdapter = new MemberMarketPolicyAdapter(this);
        mPolicyListView.setAdapter(mAdapter);
        requestVipMarketPolicy();
        loadViewWithLoginStatus();
        loadViewWithVip();
        registerLoginBroadcast();
    }

    private void initViews() {
        mTitle = (TextView) findViewById(R.id.title);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.rl_service_agreement).setOnClickListener(this);
        mPolicyListView = (ListViewForScrollView) findViewById(R.id.listView_pay_policy);
        mPolicyListView.setOnItemClickListener(this);
        mPayNum = (TextView) findViewById(R.id.tv_pay_num);
        mPayCommit = (TextView) findViewById(R.id.tv_commit);
        mPayCommit.setOnClickListener(this);
        mUserHeader = (NetworkImageView) findViewById(R.id.img_header);
        mUserHeader.setDefaultImageResId(R.drawable.icon_unlogin_header);
        mUserHeader.setErrorImageResId(R.drawable.icon_unlogin_header);
        mUserName = (TextView) findViewById(R.id.tv_username);
        mLoginBtn = (TextView) findViewById(R.id.tv_login);
        mLoginBtn.setOnClickListener(this);
        mUserStatusDes = (TextView) findViewById(R.id.tv_login_info);
        mMarketPolicy = (LinearLayout) findViewById(R.id.ll_vip_market_policy);
    }

    private void loadViewWithVip() {
        if (User.isVip()) {
            mTitle.setText(getString(R.string.vip_open_member_renew));
            mUserStatusDes.setText(String.format(getString(R.string.vip_open_member_due_date), user.getVipdate()));
        } else {
            mTitle.setText(getString(R.string.vip_open_member_title));
        }

    }

    private void loadViewWithLoginStatus() {
        if (isLogin) {
            mUserHeader.setImageUrl(user.getUserIcon(), VolleyHelper.getImageLoader());
            mUserName.setText(user.getUserName());
            mUserName.setTextColor(IfengApplication.getInstance().getResources().getColor(R.color.open_member_login_btn_text_color));
            mUserStatusDes.setText(getString(R.string.vip_open_member_login_info_login_no_vip));
            mLoginBtn.setVisibility(View.GONE);
        } else {
            mUserName.setText(getString(R.string.vip_open_member_user_name_no_login));
            mUserStatusDes.setText(getString(R.string.vip_open_member_login_info_no_login));
            mLoginBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.tv_commit:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VIP_CENTER_CONFIRM_PAY, PageIdConstants.VIP_USER_PAY_FOR);
                if (isLogin) {
                    if (isWeiXinPaySupported()) {
                        final Dialog dialog = AlertUtils.getInstance().showSmallProgressDailog(this);
                        if (dialog != null) {
                            dialog.setCanceledOnTouchOutside(false);
                        }
                        String url = String.format(DataInterface.WEXIN_PAY_PREPAYMENT, User.getUid(), mCurrentProductId);
                        CommonDao.sendRequest(url, null,
                                new Response.Listener() {
                                    @Override
                                    public void onResponse(Object response) {
                                        if (response == null || TextUtils.isEmpty(response.toString())) {
                                            if (dialog != null) {
                                                dialog.dismiss();
                                            }
                                            return;
                                        }

                                        JSONObject jsonObject = JSON.parseObject(response.toString());
                                        int code = jsonObject.getIntValue("code");
                                        if (1 == code) {
                                            JSONObject data = jsonObject.getJSONObject("data");
                                            PayPrepareBean bean = new PayPrepareBean();
                                            bean.setAppid(data.getString("appid"));
                                            bean.setPartnerid(data.getString("partnerid"));
                                            bean.setPrepayid(data.getString("prepayid"));
                                            bean.setNoncestr(data.getString("noncestr"));
                                            bean.setPkg(data.getString("package"));
                                            bean.setTimestamp(data.getString("timestamp"));
                                            bean.setSign(data.getString("sign"));
                                            accessWeiXinPayBy(bean);
                                        } else {
                                            String msg = jsonObject.getString("msg");
                                            if (!TextUtils.isEmpty(msg)) {
                                                ToastUtils.getInstance().showShortToast(msg);
                                            }
                                        }
                                        if (dialog != null) {
                                            dialog.dismiss();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                    }
                                }, CommonDao.RESPONSE_TYPE_GET_JSON);

                    } else {
                        ToastUtils.getInstance().showShortToast(getString(R.string.vip_open_member_no_weixin_msg));
                    }
                } else {
                    IntentUtils.startLoginActivity(this);
                }
                break;
            case R.id.tv_login:
                IntentUtils.startLoginActivity(this);
                break;
            case R.id.rl_service_agreement:
                IntentUtils.startADActivity(this, null, DataInterface.VIP_MEMBER_AGREEMENT, null, ADActivity.FUNCTION_VALUE_H5_STATIC, getString(R.string.vip_open_member_service_agreement), null, null, null, null, null, null);
                break;
        }
    }

    private void requestVipMarketPolicy() {
        String url = String.format(DataInterface.VIP_MARKET_POLICY, PhoneConfig.userKey);
        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            mMarketPolicy.setVisibility(View.GONE);
                            return;
                        }

                        JSONObject json = null;
                        try {
                            json = JSONObject.parseObject(response.toString());
                            if (json == null) {
                                mMarketPolicy.setVisibility(View.GONE);
                                return;
                            }
                            List<VipMarketPolicyData> entityList = new ArrayList<VipMarketPolicyData>();
                            JSONArray jsonArray = json.getJSONArray("marketingList");

                            for (Object aJsonArray : jsonArray) {
                                VipMarketPolicyData entity = new VipMarketPolicyData();
                                JSONObject jsonObject = (JSONObject) aJsonArray;
                                entity.setProductId(jsonObject.getString("productId"));
                                entity.setSalesName(jsonObject.getString("salesName"));
                                entity.setSalesActivities(jsonObject.getString("salesActivities"));
                                entity.setSalesMoney(jsonObject.getString("salesMoney"));
                                entity.setIsRecommend(jsonObject.getString("isRecommend"));
                                entityList.add(entity);
                            }

                            if (ListUtils.isEmpty(entityList)) {
                                mMarketPolicy.setVisibility(View.GONE);
                            } else {
                                mMarketPolicy.setVisibility(View.VISIBLE);
                                mVipMarketPolicyList = entityList;
                                mAdapter.setList(mVipMarketPolicyList);
                                VipMarketPolicyData marketPolicyData = mVipMarketPolicyList.get(0);
                                mPayNum.setText(String.format(getString(R.string.vip_open_member_wx_pay_num), marketPolicyData.getSalesMoney()));
                                mCurrentProductId = marketPolicyData.getProductId();
                            }
                        } catch (Exception e) {
                            mMarketPolicy.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON, false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mPayNum.setText(String.format(getResources().getString(R.string.vip_open_member_wx_pay_num),
                mVipMarketPolicyList.get(position).getSalesMoney()));
        mCurrentProductId = mVipMarketPolicyList.get(position).getProductId();
        mSelectedPrice = mVipMarketPolicyList.get(position).getSalesMoney();
        mAdapter.changeSelected(position);
    }

    public static boolean accessWeiXinPayBy(PayPrepareBean bean) {

        if (bean == null || IfengApplication.getInstance() == null) {
            return false;
        }
        IWXAPI api = WXAPIFactory.createWXAPI(IfengApplication.getInstance(), APP_ID);
        if (api == null) {
            return false;
        }
        api.registerApp(APP_ID);

        PayReq request = new PayReq();
        request.appId = bean.getAppid();
        request.partnerId = bean.getPartnerid();
        request.prepayId = bean.getPrepayid();
        request.packageValue = bean.getPkg();
        request.nonceStr = bean.getNoncestr();
        request.timeStamp = bean.getTimestamp();
        request.sign = bean.getSign();
        // ToastUtils.getInstance().showLongToast(bean.toString());
        return api.sendReq(request);
    }


    public static boolean isWeiXinPaySupported() {

        if (IfengApplication.getInstance() == null) {
            return false;
        }
        IWXAPI api = WXAPIFactory.createWXAPI(IfengApplication.getInstance(), APP_ID);
        api.registerApp(APP_ID);

        boolean isSupported = api != null && api.getWXAppSupportAPI() >= com.tencent.mm.sdk.constants.Build.PAY_SUPPORTED_SDK_INT;
        return isSupported;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mLoginReceiver);
    }

    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING) {
                    isLogin = true;
                    ToastUtils.getInstance().showShortToast("登录成功");
                    if (User.isVip()) {
                        finish();
                    }
                } else if (loginState == IntentKey.UNLOGIN) {
                    isLogin = false;
                }
                loadViewWithLoginStatus();
                loadViewWithVip();
            }
        }
    };

}
