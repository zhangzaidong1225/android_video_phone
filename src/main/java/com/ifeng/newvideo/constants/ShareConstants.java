package com.ifeng.newvideo.constants;

/**
 * 分享相关常量
 * Created by fanshell on 2016/2/15.
 */
public class ShareConstants {

    /**
     * 同assets/ShareSDK.xml中的值
     */
    public static final String MOB_KEY = "27f71f2d184";

    public static final String SHARE_TITLE_HEAD_VIDEO = "#凤凰视频客户端视频推荐#";
    public static final String SHARE_TITLE_HEAD_LIVE = "#凤凰视频独家直播#";
    public static final String SHARE_TITLE_HEAD_TV_LIVE = "【直播】";
    public static final String SHARE_TITLE_HEAD_VR_LIVE = "【VR直播】";
    public static final String SHARE_TITLE_HEAD_VR_VIDEO = "【VR】";

    public static final String SHARE_TITLE_END = " (分享自@凤凰视频客户端)";

    /**
     * 分享相关
     */
    public static final String SHARE_TEXT = "share_text";
    public static final String SHARE_URL = "share_url";
    public static final String SHARE_IMG = "share_img";
    public static final String SHARE_LIVE = "share_is_live";
    public static final String SHARE_VR_LIVE = "share_is_vrlive";
    public static final String SHARE_VR_VIDEO = "share_is_vrvideo";
    public static final String SHARE_AD = "share_is_ad";
    public static final String SHARE_SIGN = "share_is_sign";
    public static final String SHARE_FM = "share_is_fm";
    /**
     * 分享统计相关
     */
    public static final String SHARE_ID = "share_id";
    public static final String SHARE_ECHID = "share_echid";
    public static final String SHARE_CHID = "share_is_chid";
    public static final String SHARE_WMID = "share_wmid";


}
