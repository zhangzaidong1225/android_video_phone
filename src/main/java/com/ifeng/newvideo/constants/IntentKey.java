package com.ifeng.newvideo.constants;

/**
 * 用于存放一些Intent中用到的Key值
 */
public class IntentKey {
    public static final String ANCHOR = "anchor";
    public static final String AUDIO_OR_VIDEO = "audio_or_video";
    public static final String VOD_GUID = "vod_guid";
    public static final String VOD_ECHID = "vod_echid";

    public static final String IS_CONVERT_SHAREINFO = "is_convert_shareinfo";
    /**
     * fromParamForVideoAd 视频跳转来源，用于广告请求参数，不清楚的话，填""，会转成news
     */
    public static final String FROM_PARAM_FOR_VIDEO_AD = "from_param_for_video_ad";
    /**
     * 视频跳转来源
     */
    public static final String FROM_PARAM_FOR_VIDEO = "from_param_for_video";
    /**
     * 电台FM 统计V tag
     */
    public static final String VOD_ID = "vod_id";
    public static final String LAYOUT_TYPE = "layout_type";
    public static final String E_CHID = "echid";
    public static final String CHID = "chid";
    public static final String TOPIC_TYPE = "topic_type";
    public static final String TOPIC_ID = "topic_id";
    public static final String CURRENT_PARM_INDEX = "current_parm_index";
    public static final String FM_DURATION = "fm_duration";//电台播放时长
    public static final String FM_FILE_SIZE = "fm_file_size";//电台文件大小

    /**
     * 应用下载app Url
     */
    public final static String STORE_APP_URL = "store_app_url";

    /**
     * 应用下载app 名称
     */
    public final static String STORE_APP_NAME = "store_app_name";

    /**
     * app启动次数
     */
    public final static String APP_START_TIME = "app_start_time";
    /**
     * 视频提示次数，用于播放界面中弹出 “您可以在设置中选择默认播放视频或音频” Toast
     */
    public final static String VIDEO_START_TIME = "video_start_time";
    /**
     * 看过视频的书签
     */
    public static final String HISTORY_BOOK_MARK = "history_book_mark";

    /**
     * 是否显示弹幕
     */
    public static final String IS_SHOW_DANMA = "is_show_danma";
    /**
     * 是否弹出发送弹幕框
     */
    public static final String IS_SHOW_EDIT_DANMU = "is_show_edit_danma";
    /**
     * 是否从看过跳转而来
     */
    public static final String IS_FROM_HISTORY = "is_from_history";

    /**
     * 是不是从缓存跳来的
     */
    public static final String IS_FROM_CACHE = "IS_FROM_CACHE";

    public static final String CHANNELID = "channelId";
    public static final String CHANNEL_TYPE = "channel_type";

    /**
     * 直播
     */
    public static final String LIVE_INFO_MODEL = "LiveInfoModel";

    public static final String LIVE_BY_PUSH_INDEX = "livePushIndex";

    public static final String LIVE_IFENG_URL = "liveIfengUrl";


    /**
     * 推送
     */
    public static final String UPDATECONFIG_NOCHECK = "UPDATECONFIG_NOCHECK";
    public static final String UPDATECONFIG_OBJ = "UPDATECONFIG_OBJ";
    public static final String PUSH_LIVE_CLASSIC = "PUSH_LIVE_CLASSIC";
    public static final String PUSH_TYPE_NOSUPPORTED = "PUSH_TYPE_NOSUPPORTED";
    public static final String ACTION_CLOSE_ACTIVITY = "com.ifeng.newvideo.activity.close";
    public static final String FLAG_PUSHTYPE_NOSUPPORTED_HINT = "FLAG_PUSHTYPE_NOSUPPORTED_HINT";
    /**
     * 是否来自push
     */
    public static final String IS_FROM_PUSH = "is_from_push";
    public static final String LIVE_SERVICE_TIME = "live_service_time";
    public static final String LIVE_GET_SERVICETIME = "live_is_get_time";
    /**
     * 用户账号信息
     */
    public static final String LOGIN_USERACCOUNT = "LOGIN_USERACCOUNT";

    /**
     * 推送开关
     */
    public static String PUSH_ON = "push_on";

    /**
     * page统计的来源页面标识
     */
    public static final String STATISTICS_REF_PAGE = "statistics_ref_page";
    /**
     * page统计的目标页面标识
     */
    public static final String STATISTICS_TAG_PAGE = "statistics_tag_page";


    /**
     * dlna *
     */
    public static final String LIST_TAG = "list_tag";
    public static final String DATA_INFO = "data_info";

    /**
     * 是否从启动页传来的，用于跳转到主页面后，再打开启动页广告
     */
    public static final String IS_FROM_SPLASH_ACTIVITY = "isFromSplashActivity";
    public static final String IS_FROM_ALARM = "isFromAlarm";
    /**
     * 从启动页传来的广告model
     */
    public static final String AD_MODEL_FROM_SPLASH_ACTIVITY = "ad_model_from_splash_activity";

    /**
     * 登录广播
     */
    public static final String LOGINBROADCAST = "loginCast";
    public static final String LOGINSTATE = "state";
    public static final int UNLOGIN = 0;
    public static final int LOGINING = 1;//已登陆

    /**
     * 评论跳转的登录广播
     */
    public static final String LOGIN_BROADCAST_FOR_COMMENT = "login_Cast";
    public static final String LOGIN_STATE = "login_state";
    /**
     * 未登录
     */
    public static final int LOGIN_FAIL = 0;
    /**
     * 已登录
     */
    public static final int LOGIN_SUCCESS = 1;
    /**
     * 登录默认状态，未知
     */
    public static final int LOGIN_NONE = 2;

    /**
     * 评论跳转的绑定广播
     */
    public static final String BUNDLE_BROADCAST_FOR_COMMENT = "bundle_broadcast_for_comment";
    public static final String BUNDLE_STATE_FOR_COMMENT = "bundle_state_for_comment";
    public static final int BUNDLE_YES = 1;//绑定成功
    public static final int BUNDLE_NO = 0;//绑定失败

    /**
     * 缓存 *
     */

    public static final String FOLDER_TYPE = "folder_type";//类型，区分是单条还是专题
    public static final String FOLDER_GUID = "folder_guid";
    public static final String PROGRAM_LIST = "program_list";
    public static final String CURRENT_PROGRAM = "current_program";
    public static final String PROGRAM_LIST_INDEX = "program_list_index";
    public static final String CACHE_FOLDER_MODEL = "cache_folder_model";


    /**
     * 通知UI广播
     */
    public static final String ACTION_NOTIFY_UI_CHANGE = "notify.ui.change";
    //请求数据完成时
    public static final String ACTION_INIT_CONTROLL_AUDIO_SERVICE = "init.control.with.audio.service";
    //联通流量数据超限
    public static final String ACTION_OVERFLOW_AUDIO_SERVICE = "overflow.with.audio.service";
    //切换运营商网络时
    public static final String ACTION_CHANGE_3GNET_SERVICE = "change.3gnet.service";
    //播放音频时
    public static final String ACTION_AUDIO_PREPARING = "action.audio.preparing";
    public static final String ACTION_AUDIO_PLAYING = "action.audio.playing";
    public static final String ACTION_AUDIO_PAUSE = "action.audio.pause";
    public static final String ACTION_AUDIO_STATE_ERROR = "action.audio.error";
    public static final String BUNDLE_EXTRAS = "bundle_extras";
    public static final String ONE_TO_ONE_BROADCAST = "onetoone.broadcast";

    /**
     * 凤凰快讯
     */
    public static final String IFENG_NEWS_ID = "ifeng.news.id";
    public static final String IFENG_NEWS_CONTENT = "ifeng.news.content";


    /**
     * 跳转到VRLiveActivity携带的数据的key
     */
    public static final String VR_LIVE_URL = "vr_live_url";
    public static final String VR_LIVE_ID = "vr_live_id";
    public static final String VR_LIVE_PROGRAM_TITLE = "vr_live_program_title";
    public static final String VR_LIVE_ECHID = "vr_live_echid";
    public static final String VR_LIVE_CHID = "vr_live_chid";
    public static final String VR_LIVE_IMAGE_URL = "vr_live_image_url";
    public static final String VR_LIVE_SHARE_URL = "vr_live_share_url";
    public static final String VR_LIVE_WE_MEDIA_ID = "vr_live_we_media_id";
    public static final String VR_LIVE_WE_MEDIA_NAME = "vr_live_we_media_name";
    public static final String VR_SEEKTO_POSITION = "vr_seekto_position";
    public static final String VR_VIDEO_LIST = "vr_video_list";
    public static final String VR_CURRENT_POSITION_IN_VIDEO_LIST = "vr_current_position_in_video_list";
    public static final String VR_VIDEO_IS_FROM_HISTORY = "vr_video_is_from_history";

    /**
     * 定位到自媒体的卫视分类位置
     */
    public static final String LOCATE_TO_TV = "locate_to_tv";

    /**
     * 电视台直播
     */
    public static final String TV_LIVE_INFO = "tv_live_info";
    public static final String TV_LIVE_ECHID = "tv_live_echid";
    public static final String TV_LIVE_CHID = "tv_live_chid";
    public static final String TV_LIVE_WEMEDIA_ID = "tv_live_wemedia_id";
    public static final String TV_LIVE_WEMEDIA_NAME = "tv_live_wemedia_name";
    public static final String TV_LIVE_CHANNEL_ID = "tv_live_channel_id";
    public static final String TV_LIVE_FROM_PUSH = "tv_live_from_push";

    /**
     * 自媒体主页自媒体ID
     */
    public static final String WE_MEIDA_ID = "we_meida_id";

    /**
     * WebView字段
     */
    public static final String WEB_VIEW_TITLE = "webview_title";
    public static final String WEB_VIEW_URL = "webview_url";

    /**
     * 缓存播放页
     */
    public static final String CACHE_VIDEO_MODELS = "cache_video_models";
    public static final String CACHE_VIDEO_MODEL = "cache_video_model";

    /**
     * 搜索关键字
     */
    public static final String SEARCH_KEYWORD = "search_keyword";

    /**
     * 为了保持首页和播放页的标题、图片一致
     * 用于分享、播放
     */
    public static final String HOME_VIDEO_TITLE = "home_video_title";
    public static final String HOME_VIDEO_IMG = "home_video_img";

    //fm传递数据
    public static final String AUDIO_PROGRAM_LIST = "audio_program_list";
    public static final String AUDIO_PAGE_NUM = "audio_page_num";
    public static final String AUDIO_PROGRAM_ID = "audio_programid";
    public static final String AUDIO_FM_TYPE = "audio_fm_type";
    public static final String AUDIO_FM_URl = "audio_fm_url";
    public static final String AUDIO_FM_IMAGE_URl = "audio_fm_image_url";
    public static final String AUDIO_FM_GUID = "audio_fm_guid";
    public static final String AUDIO_FM_NAME = "audio_fm_name";
    /**
     * 电台FM 统计V tag
     */
    public static final String FM_STATISTICS_V_TAG = "from_param_for_statistics_v_tag";

    /**
     * 消息字段
     */
    public static final String MSG_STATUS = "msg_have";

    public static final String IS_FROM_MINE = "is_from_mine";

}
