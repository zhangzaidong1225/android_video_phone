package com.ifeng.newvideo.constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 刷新页面计时常量类
 * Created by fengfan on 2015/8/5,11:24.
 */
public class PageRefreshConstants {

    private static final Logger logger = LoggerFactory.getLogger(PageRefreshConstants.class);

    /**
     * 常量标记，页面类型
     */
    public static final String PAGE_CATEGORY = "page_category";
    /**
     * 常量标记，页面id（第几个页面）
     */
    public static final String PAGE_ID = "page_id";
    /**
     * 常量标记，页面上次刷新的日期
     */
    public static final String PAGE_LAST_REFRESH_TIME = "page_last_refresh_time";

    public static final String SEPARATOR = "-";
    /**
     * 首页类型
     */
    public static final String CATEGORY_HOME = "home";

    /**
     * 记录类型
     */
    public static final String CATEGORY_DOC = "doc";

    /**
     * 直播类型
     */
    public static final String CATEGORY_LIVE = "live";
    /**
     * 需要刷新的时间间隔(单位：毫秒)
     */
    // public static final long REFRESH_DATA_INTERVAL = 1 * 60 * 1000; // 1m
    public static final long REFRESH_DATA_INTERVAL = 10 * 60 * 1000; // 10m

    public static final int HOMEPAGE_REFRESH_DATA_INTERVAL = 2 * 60 * 60 * 1000; // 2h
    // public static final int HOMEPAGE_REFRESH_DATA_INTERVAL = 60 * 1000; // 1m

}
