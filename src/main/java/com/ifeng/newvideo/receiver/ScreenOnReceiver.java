package com.ifeng.newvideo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 监听锁屏解锁状态的广播
 * Created by hu on 2014/10/19.
 */
public class ScreenOnReceiver extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(ScreenOnReceiver.class);

    private final IfengApplication app = IfengApplication.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {
        logger.debug("onReceive come in...");
        final String action = intent.getAction();
        if (Intent.ACTION_SCREEN_ON.equals(action)) {//开屏
            logger.debug("screen is on...");
            app.setIsScreenOff(false);
        } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {//锁屏
            logger.debug("screen is off...");
            app.setIsScreenOff(true);
            if (app.isTopApplication()) {
                CustomerStatistics.exitApp();
            }
            if (onScreenOffListener != null) {
                onScreenOffListener.onScreenOffCallBack();
            }
        } else if (Intent.ACTION_USER_PRESENT.equals(action)) {//解锁
            logger.debug("screen is unlock...");
            if (app.isTopApplication()) {
                CustomerStatistics.enterApp();
            }
        }
    }

    private OnScreenOffListener onScreenOffListener;

    public void setOnScreenOffListener(OnScreenOffListener onScreenOffListener) {
        this.onScreenOffListener = onScreenOffListener;
    }

    public interface OnScreenOffListener {
        void onScreenOffCallBack();
    }
}
