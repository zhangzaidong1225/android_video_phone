package com.ifeng.newvideo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.statistics.StatisticHBService;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.Util4act;
import com.ifeng.video.core.utils.DistributionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by hu on 2014/10/19.
 */
public class BootReceiver extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(BootReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            logger.debug("turn on phone!!!");
            /*启动后台监测联网日志服务 */
            IntentUtils.startService(context, new Intent(StatisticHBService.ACTION));
        }
        initPushService(context);
    }

    /**
     * 开启推送服务
     *
     * @param context
     */
    private void initPushService(Context context) {
        if (!Util4act.isServiceRun(context, "com.ifeng.ipush.client.service.PushService")) {
            if ("true".equals(DistributionInfo.isPushTest)) {
                Ipush.initDebug(context, 1, false);
            } else {
                Ipush.init(context, 1, false);
            }
        } else {
            logger.info("BootReceiver", "PushService 已经运行");
        }
    }
}
