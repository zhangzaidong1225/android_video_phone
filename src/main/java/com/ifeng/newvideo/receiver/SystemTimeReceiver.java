package com.ifeng.newvideo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.video.dao.db.model.LaunchAppModel;
import com.ifeng.newvideo.utils.TimeUtils;
import com.ifeng.video.core.net.RequestJson;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.dao.db.constants.DataInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cuihz on 2014/11/20.
 */
public class SystemTimeReceiver extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(SystemTimeReceiver.class);

    @Override
    public void onReceive(final Context context, Intent intent) {
        RequestJson<LaunchAppModel> myReq = new RequestJson<LaunchAppModel>(Request.Method.GET,
                DataInterface.getLaunchAppUrl(),
                LaunchAppModel.class,
                new SuccessListener(),
                new ErrorListener()
        );
        VolleyHelper.getRequestQueue().add(myReq);
    }

    private static class SuccessListener implements Response.Listener<LaunchAppModel> {

        public SuccessListener() {
        }

        @Override
        public void onResponse(LaunchAppModel response) {
            if (response != null) {
                TimeUtils.setTimeDiff(response.getSystemTime());
            }
        }
    }

    private static class ErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
        }
    }
}
