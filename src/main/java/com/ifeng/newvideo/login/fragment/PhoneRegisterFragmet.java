package com.ifeng.newvideo.login.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.activity.CheckPhoneNumberActivity;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.LoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 手机注册页面
 */
public class PhoneRegisterFragmet extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private static final Logger logger = LoggerFactory.getLogger(PhoneRegisterFragmet.class);

    //请求数据状态码
    private static final int SUCESSS_STATE = 1;//成功
    private static final int FAILED_STATE = 0;//失败

    private EditText edPhone, edPwd, edRestisterAcc;

    private ImageView ivRegisterEmailX, ivRegisterEmailCodeX, ivRegisterEmailAccX, ivRegisterEmailAcc, refreshBtn;
    private CheckBox cbRegisterEmail;
    private TextView btnRegisterEmail;
    private TextView tvArgmentEmail;
    private TextView tvArgmentEmail2;
    private InputMethodManager inputManager;
    private static final int RESULTCODE = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_register_phone, container, false);
        initView(root);
        setOnEditChangeListener();
        return root;
    }

    private void initView(View root) {
        ivRegisterEmailAcc = (ImageView) root.findViewById(R.id.login_iv_verify_code);
        ivRegisterEmailAcc.setOnClickListener(this);

        setAccredit(ivRegisterEmailAcc);
        refreshBtn = (ImageView) root.findViewById(R.id.login_iv_refresh_verify_code);
        refreshBtn.setOnClickListener(this);

        /** 手机注册 */
        edPhone = (EditText) root.findViewById(R.id.register_phone_edit);//手机号EditText
        ivRegisterEmailX = (ImageView) root.findViewById(R.id.register_email_clear);//右侧清除图标
        ivRegisterEmailX.setOnClickListener(this);

        edPwd = (EditText) root.findViewById(R.id.register_pwd_edit);//密码EditText
        ivRegisterEmailCodeX = (ImageView) root.findViewById(R.id.register_password_clear);//右侧清除图标
        ivRegisterEmailCodeX.setOnClickListener(this);

        edRestisterAcc = (EditText) root.findViewById(R.id.login_et_verify);//验证码EditText
        ivRegisterEmailAccX = (ImageView) root.findViewById(R.id.login_iv_verify_clear);//右侧清除图标
        ivRegisterEmailAccX.setOnClickListener(this);

        edPhone.setOnTouchListener(this);
        edPwd.setOnTouchListener(this);
        edRestisterAcc.setOnTouchListener(this);

        tvArgmentEmail = (TextView) root.findViewById(R.id.register_agreement);//同意协议文字
        tvArgmentEmail.setOnClickListener(this);
        tvArgmentEmail2 = (TextView) root.findViewById(R.id.register_agreement2);//同意协议文字
        tvArgmentEmail2.setOnClickListener(this);

        btnRegisterEmail = (TextView) root.findViewById(R.id.btn_email_register);//获取短信验证码
        btnRegisterEmail.setOnClickListener(this);

        cbRegisterEmail = (CheckBox) root.findViewById(R.id.cb_register_email);//checkBox
        cbRegisterEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_PROTOCOL, true, PageIdConstants.MY_LOGIN_REG);
                } else {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_PROTOCOL, false, PageIdConstants.MY_LOGIN_REG);
                }
            }
        });
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            switch (view.getId()) {
                case R.id.register_phone_edit:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_ACCOUNT_EDIT, PageIdConstants.MY_LOGIN_REG);
                    ((EditText) view).setCursorVisible(true);
                    break;
                case R.id.register_pwd_edit:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_PWD_EDIT, PageIdConstants.MY_LOGIN_REG);
                    ((EditText) view).setCursorVisible(true);
                    break;
                case R.id.login_et_verify:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_EDIT, PageIdConstants.MY_LOGIN_REG);
                    ((EditText) view).setCursorVisible(true);
                    break;
            }
        }
        return false;
    }

    private void setOnEditChangeListener() {
        edPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    ivRegisterEmailX.setVisibility(View.INVISIBLE);
                } else {
                    ivRegisterEmailX.setVisibility(View.VISIBLE);
                }
            }
        });

        edPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    ivRegisterEmailCodeX.setVisibility(View.INVISIBLE);
                } else {
                    ivRegisterEmailCodeX.setVisibility(View.VISIBLE);
                }
            }
        });

        edRestisterAcc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    ivRegisterEmailAccX.setVisibility(View.INVISIBLE);
                } else {
                    ivRegisterEmailAccX.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULTCODE) {
            setAccredit(ivRegisterEmailAcc);
            edRestisterAcc.setText("");
            popInputBox(edRestisterAcc);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_iv_verify_code:
                setAccredit(ivRegisterEmailAcc);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_REFRESH, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.login_iv_refresh_verify_code:
                setAccredit(ivRegisterEmailAcc);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_REFRESH, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.register_email_clear:
                edPhone.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_ACCOUNT_CLEAR, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.login_iv_verify_clear:
                edRestisterAcc.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_CLEAR, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.register_password_clear:
                edPwd.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_PWD_CLEAR, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.register_agreement:
                IntentUtils.startRegisterAgreementActivity(getActivity());
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_PROTOCOL_VIEW, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.register_agreement2:
                IntentUtils.startWebViewActivity(getActivity(), "https://id.ifeng.com/muser/protected?id=5",
                        getString(R.string.login_agree_title2));
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_PRIVACY_PROTOCOL_VIEW, PageIdConstants.MY_LOGIN_REG);
                break;

            case R.id.btn_email_register:
                if (!NetUtils.isNetAvailable(getActivity())) {
                    ToastUtils.getInstance().showShortToast(getActivity().getString(R.string.common_net_useless));
                    return;
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_GETSMS, PageIdConstants.MY_LOGIN_REG);
                if (cbRegisterEmail.isChecked()) {
                    String phoneNum = edPhone.getText().toString().trim();
                    if (TextUtils.isEmpty(phoneNum)) {
                        ToastUtils.getInstance().showShortToast(R.string.login_toast_input_phonenumber);
                        break;
                    }
                    if (StringUtils.isMobileNumberAll(phoneNum)) {
                        String eCode = edPwd.getText().toString().trim();
                        if (!TextUtils.isEmpty(eCode)) {
                            if (StringUtils.isPassWord(eCode)) {
                                String eAcc = edRestisterAcc.getText().toString().trim();
                                if (!TextUtils.isEmpty(eAcc)) {
                                    phoneCheck(phoneNum, eCode, eAcc);
                                } else {
                                    ToastUtils.getInstance().showShortToast(R.string.login_toast_input_verificationcode);
                                }

                            } else {
                                ToastUtils.getInstance().showShortToast(R.string.login_toast_pwdform_mistake);
                            }
                        } else {
                            ToastUtils.getInstance().showShortToast(R.string.login_toast_input_pwd);
                            return;
                        }
                    } else {
                        ToastUtils.getInstance().showShortToast(R.string.login_toast_phonenumber_mistake);
                    }
                    return;
                }
                ToastUtils.getInstance().showShortToast(R.string.login_toast_read_treaty);
                break;
        }
    }

    /**
     * 验证手机号是否已经注册，未注册，则进入验证码界面同时，获取验证码
     *
     * @param phonenum
     */
    private void phoneCheck(final String phonenum, final String password, final String auth) {
        LoginDao.requestPhoneCheck(phonenum, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int code = getStateCode(response);
                if (code == SUCESSS_STATE) {
                    if (getResult(response, "message").equals("手机号已存在")) {
                        ToastUtils.getInstance().showShortToast(R.string.login_phone_exist);
                    }
                } else if (code == FAILED_STATE) {
                    phoneGetAccreditNum(phonenum, password, auth);
                }
            }
        }, new PhoneCheckErrorListener());
    }

    private void phoneGetAccreditNum(final String phoneNum, final String password, final String auth) {
        Map<String, String> data = new HashMap<String, String>();
        data.put("mobile", phoneNum);
        data.put("auth", auth);
        data.put("pf", "2");
        data.put("comefrom", "19");
        LoginDao.requestPhoneAccreditNumber(data, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                logger.debug("response=={}", response);
                int code = getStateCode(response);
                if (code == SUCESSS_STATE) {
                    Intent intent = new Intent(getActivity(), CheckPhoneNumberActivity.class);
                    intent.putExtra("phoneNum", phoneNum);
                    intent.putExtra("password", password);
                    startActivityForResult(intent, 100);
                } else {
                    setAccredit(ivRegisterEmailAcc);
                    ToastUtils.getInstance().showShortToast(getResult(response, "message"));

                }
            }
        }, new GetAccreditErrorListener());
    }

    /**
     * 获取手机注册的验证码
     */
    private void setAccredit(final ImageView imageView) {
        LoginDao.requestAccredit(new GetAccreditBitmapSuccessListener(imageView), new GetAccreditBitmapErrorListener());
    }

    private int getStateCode(String jsonString) {
        int code = 2; // 不能用默认0，因为0,1，-1，都为状态码。
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            code = jsonObject.getInteger("code");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return code;
    }

    private String getResult(String jsonString, String msg) {
        String result = null;
        try {
            JSONObject object = JSON.parseObject(jsonString);
            result = object.getString(msg);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return result;
    }

    /**
     * 自动弹出软键盘
     */
    private void popInputBox(final EditText editText) {
        editText.setFocusable(true);
        editText.requestFocus();
        new Timer().schedule(new TimerTask() {
            public void run() {
                inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(editText, 0);
            }
        }, 500);
    }

    private static class PhoneCheckErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
        }
    }

    private static class GetAccreditErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
    }

    private static class GetAccreditBitmapSuccessListener implements Response.Listener<Bitmap> {
        private final ImageView imageView;

        public GetAccreditBitmapSuccessListener(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        public void onResponse(Bitmap response) {
            imageView.setImageBitmap(response);
        }
    }

    private static class GetAccreditBitmapErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
        }
    }
}
