package com.ifeng.newvideo.login;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import com.ifeng.video.dao.db.dao.ChannelDao;
import com.ifeng.video.dao.db.dao.SubscribeRelationDao;
import com.ifeng.video.dao.db.model.Channel;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 该Service用于把服务端返回的订阅状态数据保存到本地
 * Created by Administrator on 2016/8/10.
 */
public class HandleDataService extends IntentService {
    private static final Logger logger = LoggerFactory.getLogger(HandleDataService.class);

    public static final String KEY_SUBSCRIBE_RELATION_DATA = "date_key";
    public static final String KEY_USER_ID = "key_user_id";//当前用户di
    public static final String KEY_CHANNEL_3 = "date_key_channel3";
    public static final String KEY_CHANNEL_2 = "date_key_channel2";

    //public static final String SAVE_DATA_SUCCESS_ACTION = "com.ifeng.handle_data_success_action";//订阅关系成功保存本地，发出广播的Action
    public static final String LOGIN_SUCCESS_ACTION = "login_success_action";//登录成功Action
    public static final String LOGOUT_ACTION = "logout_action";//退出登录Action
    public static final String EXIT_APP_ACTION = "exit_app_action";//退出应用Action

    public HandleDataService() {
        super("HandleDataService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            String action = intent.getAction();
            Bundle bundle = intent.getExtras();
            if (LOGIN_SUCCESS_ACTION.equals(action)) {
                saveSubscribeRelationToDB(bundle);
            } else if (LOGOUT_ACTION.equals(action)) {
                deleteAllSubscribeRelation(bundle);
            } else if (EXIT_APP_ACTION.equals(action)) {
                deleteUnSubscribeRelationFromDB(bundle);
            } else {
                syncChannels2DB(bundle);
            }
        }
    }

    private void syncChannels2DB(Bundle bundle) {
        try {
            ArrayList<Channel.ChannelInfoBean> channel_3 = bundle.getParcelableArrayList(HandleDataService.KEY_CHANNEL_3);
            if (channel_3 != null) {
                ChannelDao.updateSortIdAndLevel(getApplicationContext().getApplicationContext(), channel_3);
            }
            ArrayList<Channel.ChannelInfoBean> channel_2 = bundle.getParcelableArrayList(HandleDataService.KEY_CHANNEL_2);
            if (channel_2 != null) {
                ChannelDao.updateSortIdAndLevelAndDelete(getApplicationContext().getApplicationContext(), channel_2, ChannelDao.CHANNEL_LV_1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.debug("HandleDataService onDestroy()");
    }

    /**
     * 把服务端订阅数据保存到本地
     *
     * @param bundle
     */
    private void saveSubscribeRelationToDB(Bundle bundle) {
        List<SubscribeRelationModel> relationModels = bundle.getParcelableArrayList(HandleDataService.KEY_SUBSCRIBE_RELATION_DATA);
        try {
            SubscribeRelationDao.getInstance(this).addSubscribeList(relationModels);//保存数据
            //sendBroadcast(new Intent(SAVE_DATA_SUCCESS_ACTION));
            logger.debug("add subscribe relation data success!");
        } catch (SQLException e) {
            logger.debug("add subscribe relation data fail !");
            e.printStackTrace();
        }
    }

    /**
     * 删除表中用户未订阅的自媒体数据
     *
     * @param bundle
     */
    private void deleteUnSubscribeRelationFromDB(Bundle bundle) {
        try {
            String userId = bundle.getString(HandleDataService.KEY_USER_ID);
            SubscribeRelationDao.getInstance(this).deleteUnSubscribeByUserId(userId);
            logger.debug("delete un_subscribe relation data success!");
        } catch (SQLException e) {
            logger.debug("delete un_subscribe relation data fail !");
            e.printStackTrace();
        }
    }

    /**
     * 删除表中用户订阅的自媒体数据
     *
     * @param bundle
     */
    private void deleteAllSubscribeRelation(Bundle bundle) {
        try {
            String userId = bundle.getString(HandleDataService.KEY_USER_ID);
            SubscribeRelationDao.getInstance(this).deleteAllSubscribeByUserId(userId);
            logger.debug("delete all subscribe relation data success!");
        } catch (SQLException e) {
            logger.debug("delete all subscribe relation data fail !");
            e.printStackTrace();
        }
    }
}
