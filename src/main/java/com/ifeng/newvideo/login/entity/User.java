package com.ifeng.newvideo.login.entity;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.RsaUtil;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.constants.SharePreConstants;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.LoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 用户登陆信息
 */
public class User {

    private static final Logger logger = LoggerFactory.getLogger(User.class);
    /**
     * 用户名
     */
    private String username;
    /**
     * 头像地址
     */
    private String iconUrl;
    /**
     * 凤凰token
     */
    private String ifengToken;
    /**
     * 用户唯一id
     */
    private String uid;
    /**
     * 平台
     */
    private String sns;
    /**
     * 凤凰用户名
     */
    private String ifengUsername;


    /**
     * 是否实名制
     * 0为已实名、1为未实名
     */
    private String realNameStatus;
    /**
     * 上下文对象
     */
    private static Context context = IfengApplication.getInstance();
    /**
     * 会员状态
     * 0-非VIP 1-VIP
     */
    private int vipStatus;
    /**
     * 会员截止日期
     * 非会员为空
     */
    private String vipDate;

    /**
     * 登录 服务器时间
     */
    private String timeStamp;

    /**
     * 签名
     *
     * @return
     */
    private String sign;

    public static String getSign() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(SIGN, null);
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static String getTimeStamp() {
        if (context == null) {
            return null;
        }
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(TIMESTAMP, null);
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    private static final String USERNAME = "username";
    private static final String ICONURL = "iconUrl";
    private static final String IFENGTOKEN = "ifengToken";
    private static final String UID = "uid";
    private static final String SNS = "sns";
    private static final String IFENGUSERNAME = "ifengUsername";
    private static final String REALNAMESTATUS = "realNameStatus";
    private static final String VIPSTATUS = "vipStatus";
    private static final String VIPDATE = "vipDate";
    private static final String TIMESTAMP = "timeStamp";
    private static final String SIGN = "sign";
    /**
     * 0为已实名
     */
    public static final String REALNAMESTATUS_BIND_YES = "0";
    /**
     * 1为未实名
     */
    public static final String REALNAMESTATUS_BIND_NO = "1";

    public User(Context context) {
        if (!(context instanceof Application)) {
            context = IfengApplication.getInstance();
        }
        User.context = context;
    }


    /**
     * 进行存储操作的时候用第一个构造，读取用第二个
     *
     * @param username   用户名
     * @param iconUrl    头像的url
     * @param ifengToken 凤凰的token
     * @param context    上下文对象
     * @param vipStatus  会员状态
     * @param vipDate    会员截止日期
     */
    public User(String username, String iconUrl, String ifengToken, Context context, String uid, String sns,
                String ifengUsername, String realNameStatus, int vipStatus, String vipDate, String timeStamp, String sign) {
        this.username = username;
        this.iconUrl = iconUrl;
        this.ifengToken = ifengToken;
        this.uid = uid;
        this.sns = sns;
        this.ifengUsername = ifengUsername;
        if (!(context instanceof Application)) {
            context = context.getApplicationContext();
        }
        User.context = context;
        this.realNameStatus = realNameStatus;
        this.vipStatus = vipStatus;
        this.vipDate = vipDate;
        this.timeStamp = timeStamp;
        this.sign = sign;
    }

    /**
     * 进行存储操作的时候用第一个构造，读取用第二个
     *
     * @param vipStatus 会员状态
     * @param vipDate   会员截止日期
     */
    public User(int vipStatus, String vipDate, String sign) {
        this.vipStatus = vipStatus;
        this.vipDate = vipDate;
        this.sign = sign;
    }

    // 存储用户信息
    public synchronized void storeUserInfo() {
        if (username != null) {
            SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            // 每次保存用户信息的时候都执行一次清除操作，清除之前的缓存避免发生数据混乱
            editor.clear().apply();

            editor.putString(USERNAME, username);
            editor.putString(ICONURL, iconUrl);
            editor.putString(IFENGTOKEN, ifengToken);
            editor.putString(UID, uid);
            editor.putString(SNS, sns);
            editor.putString(IFENGUSERNAME, ifengUsername);
            editor.putString(REALNAMESTATUS, realNameStatus);
            editor.putInt(VIPSTATUS, vipStatus);
            editor.putString(VIPDATE, vipDate);
            editor.putString(TIMESTAMP, timeStamp);
            editor.putString(SIGN, sign);
            editor.apply();
        }
    }

    /**
     * 清除登陆数据。
     * 每次保存用户信息的时候都执行一次清除操作，清除之前的缓存避免发生数据混乱
     */
    public static synchronized void clearUserInfo() {
        if (isLogin()) {
            SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear().commit();
        }
    }

    /**
     * 判断用户是否登录，登录了 则返回TRUE
     *
     * @return
     */
    public static boolean isLogin() {
        return !TextUtils.isEmpty(getIfengToken());
    }

    public static String getUserName() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(USERNAME, null);
    }

    public static String getUid() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(UID, "");
    }

    public static String getUserIcon() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(ICONURL, "");
    }

    public static String getIfengToken() {
        if (context == null) {
            context = IfengApplication.getInstance();
        }
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(IFENGTOKEN, "");
    }

    public String getSns() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(SNS, null);
    }

    public String getIfengUserName() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(IFENGUSERNAME, null);
    }

    public static String getRealNameStatus() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(REALNAMESTATUS, null);
    }

    public static void setRealNameStatus(String realNameStatus) {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(REALNAMESTATUS, realNameStatus);
        editor.commit();
    }

    public static void setUserIcon(String iconUrl) {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ICONURL, iconUrl);
        editor.commit();
    }

    public static String getVipdate() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getString(VIPDATE, null);
    }

    public static void setVipdate(String vipdate) {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(VIPDATE, vipdate);
        editor.commit();
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", ifengToken='" + ifengToken + '\'' +
                ", uid='" + uid + '\'' +
                ", sns='" + sns + '\'' +
                ", ifengUsername='" + ifengUsername + '\'' +
                ", realNameStatus='" + realNameStatus + '\'' +
                ", vipStatus=" + vipStatus +
                ", vipDate='" + vipDate + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    /**
     * 存储会员信息
     */
    public synchronized void storeMemberStatus() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(VIPSTATUS, vipStatus);
        editor.putString(VIPDATE, vipDate);
        editor.putString(SIGN, sign);
        editor.apply();
    }

    /**
     * 判断是否为会员
     */
    public static boolean isVip() {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        int status = preferences.getInt(VIPSTATUS, 0);
        String date = preferences.getString(VIPDATE, null);
        // 会员状态 && 校验sign
        if (1 == status) {
            if (!TextUtils.isEmpty(date)) {
                try {
                    String content = "guid=" + getUid() + "&VIPEXP=" + date + "&signkey=" + RsaUtil.SIGN_KEY;
                    boolean rsaResult = RsaUtil.doCheck(content, getSign(), RsaUtil.PUBLIC_RSA);
                    if (!rsaResult) {
                        PageActionTracker.clickBtn(ActionIdConstants.RSA_CHECK_FAILED, PageIdConstants.PAGE_MY);
                    }
                    return rsaResult;
                } catch (Exception e) {
                    logger.error("isVip error {} ", e);
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * 更新用户信息 主要更新Vip信息
     */
    public static void updateUserVipInfo(final Context context) {
        if (isLogin()) {
            String params = "&sid=" + User.getIfengToken() + "&deviceId=" + PhoneConfig.userKey + "&ts=" + getTimeStamp();
            LoginDao.requestServerCheckLogin(new Response.Listener<Object>() {
                @Override
                public void onResponse(Object response) {
                    if (response == null || TextUtils.isEmpty(response.toString())) {
                        ToastUtils.getInstance().showShortToast("获取用户信息失败,请重新登录");
                        return;
                    }
                    int code = 0;
                    String msgcode;
                    int vipStatus;
                    String vipDate = null;
                    String sign = "";
                    JSONObject jsonObject = null;
                    jsonObject = JSON.parseObject(response.toString());
                    code = jsonObject.getIntValue("code");
                    msgcode = jsonObject.getString("msgcode");

                    vipStatus = jsonObject.getIntValue("VIPStatus");
                    vipDate = jsonObject.getString("VIPEXP");
                    sign = jsonObject.getString("sign");
                    // 登录成功
                    if (LoginDao.LOGIN_SUCCESS.equals(msgcode)) {
                        new User(vipStatus, vipDate, sign).storeMemberStatus();
                    }
                    // 互踢模式，多端登录
                    else if (LoginDao.LOGIN_ON_ANOTHER_DEVICE.equals(msgcode)) {
                        IntentUtils.startLoginActivity(context);
                        ToastUtils.getInstance().showShortToast("请重新登录");
                        clearUserInfo(); //清空本地用户数据
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastUtils.getInstance().showShortToast("获取用户信息失败,请重新登录");
                }
            }, params);
        }
    }


    /**
     * 登录积分
     */
    public static void LoginPointAccount() {
        String url = String.format(DataInterface.POINT_TASK_LOGIN, User.getUid(), PhoneConfig.mos, PhoneConfig.userKey, User.getAuthForPoint(), User.getIfengToken());
        CommonDao.sendRequest(url, null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        JSONObject jsonObject = JSON.parseObject(response.toString());
                        int code = jsonObject.getIntValue("code");
                        if (200 == code) {
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);
    }

    /**
     * 【1】双方协定秘钥字符串 secret
     * 【2】str = guid+token+secret
     * 【3】md5_res = md5(str)
     * 【4】对md5_res进行截取：从第二个字符开始取，取16个字符，得到auth参数
     *
     * @return
     */
    public static String getAuthForPoint() {
        String secret = "qwE9q3m..Wk+2]w#!13m";
        String str = getUid() + getIfengToken() + secret;
        String md5Str = StringUtils.md5s(str);
        String result = md5Str.substring(1, 17);
        return result;
    }

    public static boolean getBooleanByName(String name,
                                           boolean default_value) {
        SharedPreferences preferences = IfengApplication.getInstance().getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getBoolean(name, default_value);
    }

    public static void setBooleanByName(String name,
                                        Boolean value) {
        SharedPreferences preferences = IfengApplication.getInstance().getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    /**
     * 设置积分开始记录时间
     */
    public static void setLongByName(String name, long serverTime) {
        SharedPreferences preferences = IfengApplication.getInstance().getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(name, serverTime);
        editor.commit();
    }

    /**
     * 获取积分开始记录时间
     */
    public static long getLongByName(String name, long default_value) {
        SharedPreferences preferences = IfengApplication.getInstance().getSharedPreferences(SharePreConstants.USERINFO, Context.MODE_PRIVATE);
        return preferences.getLong(name, default_value);
    }

}
