package com.ifeng.newvideo.login.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.LoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * 绑定手机页面
 * Created by jieyz on 2015/10/28.
 */
public class BindingPhoneActivity extends BaseFragmentActivity implements View.OnClickListener,View.OnTouchListener {
    private static final Logger logger = LoggerFactory.getLogger(BindingPhoneActivity.class);

    private EditText mPhoneNumber;  //要输入的电话号码
    private ImageView phone_number_clear;
    private EditText mValidatedCode; //要输入的图片验证码
    private ImageView validated_code_clear;
    private EditText mPhoneValidatedCode; //要输入的手机短信验证码
    private ImageView phone_validated_code_clear;
    private TextView mObtainPhoneCode;    //获取手机短信验证码
    private ImageView mAccredit;    //图片上显示的验证码
    private ImageView mRefreshAuthCode;    //刷新图片上显示的验证码
    private TextView mConfirmBundle;    //确定绑定按钮


    //请求数据状态码
    private static final int SUCESSS_STATE = 1;//成功
    private static final int FAILED_STATE = 0;//失败

    private boolean state = false;//获取短信验证码是否可以点击,false:可以点击,true:不可以点击
    private CountDown mCountDown;
    private static final int TOTALTIME = 59 * 1000;
    private static final int ONCETIME = 1000;

    private static final int MESSAGE = 100;//handler发送消息
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE:
                    mObtainPhoneCode.setText(R.string.login_register_renew_accred);
                    mObtainPhoneCode.setBackgroundResource(R.drawable.login_clickable_bg);
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_phone_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        findViewById(R.id.back).setOnClickListener(this);
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.bundle_phone_number));
        findViewByIds();
        setonClickListener();
        setAccredit(mAccredit);
    }

    private void findViewByIds() {
        mPhoneNumber = (EditText) findViewById(R.id.phone_number);
        mValidatedCode = (EditText) findViewById(R.id.validated_code);
        mPhoneValidatedCode = (EditText) findViewById(R.id.phone_validated_code);
        mPhoneNumber.setOnTouchListener(this);
        mValidatedCode.setOnTouchListener(this);
        mPhoneValidatedCode.setOnTouchListener(this);
        phone_number_clear = (ImageView) findViewById(R.id.phone_number_clear);
        phone_number_clear.setOnClickListener(this);
        validated_code_clear = (ImageView) findViewById(R.id.validated_code_clear);
        validated_code_clear.setOnClickListener(this);
        phone_validated_code_clear = (ImageView) findViewById(R.id.phone_validated_code_clear);
        phone_validated_code_clear.setOnClickListener(this);
        mObtainPhoneCode = (TextView) findViewById(R.id.obtain_validated_code);
        mObtainPhoneCode.setOnClickListener(this);
        mAccredit = (ImageView) findViewById(R.id.iv_accredit);
        mAccredit.setOnClickListener(this);
        mRefreshAuthCode = (ImageView) findViewById(R.id.refresh_auth_code);
        mRefreshAuthCode.setOnClickListener(this);
        mConfirmBundle = (TextView) findViewById(R.id.confirm_bundle);
        mConfirmBundle.setOnClickListener(this);
        mCountDown = new CountDown(TOTALTIME, ONCETIME);
        mCountDown.setTimeText(mObtainPhoneCode);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(motionEvent.getAction()==MotionEvent.ACTION_UP) {
            switch (view.getId()) {
                case R.id.phone_number:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_ACCOUNT_EDIT, PageIdConstants.MY_LOGIN_BINDING);
                    ((EditText) view).setCursorVisible(true);
                    break;
                case R.id.validated_code:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_PWD_EDIT, PageIdConstants.MY_LOGIN_BINDING);
                    ((EditText) view).setCursorVisible(true);
                    break;
                case R.id.phone_validated_code:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_EDIT, PageIdConstants.MY_LOGIN_BINDING);
                    ((EditText) view).setCursorVisible(true);
                    break;
            }
        }
        return false;
    }

    private void setonClickListener() {
        mPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setClearIconShowOrGone(phone_number_clear, !TextUtils.isEmpty(s.toString()));
            }
        });

        mPhoneValidatedCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setClearIconShowOrGone(phone_validated_code_clear, !TextUtils.isEmpty(s.toString()));
            }
        });

        mValidatedCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setClearIconShowOrGone(validated_code_clear, !TextUtils.isEmpty(s.toString()));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_LOGIN_BINDING);
                finish();
                break;
            case R.id.iv_accredit:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_REFRESH, PageIdConstants.MY_LOGIN_BINDING);
                setAccredit(mAccredit);
                break;
            case R.id.refresh_auth_code:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_REFRESH, PageIdConstants.MY_LOGIN_BINDING);
                setAccredit(mAccredit);
                break;
            case R.id.obtain_validated_code:
                String phoneNumber = mPhoneNumber.getText().toString().trim();
                String validatedCode = mValidatedCode.getText().toString().trim();
                if (validateString(phoneNumber, validatedCode)) {
                    if (!state) {
                        phoneGetAccreditNum(phoneNumber, validatedCode);
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_GETSMS, PageIdConstants.MY_LOGIN_BINDING);
                    }
                }
                break;
            case R.id.confirm_bundle:
                String phone = mPhoneNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.getInstance().showShortToast("请输入电话号码");
                    return;
                }
                String msgCode = mPhoneValidatedCode.getText().toString().trim();
                if (TextUtils.isEmpty(msgCode)) {
                    ToastUtils.getInstance().showShortToast("请输入短信验证码");
                    return;
                }
                if (!NetUtils.isNetAvailable(this)) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                bundlePhoneNumber(phone, msgCode);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_FINISH, PageIdConstants.MY_LOGIN_BINDING);
                break;
            case R.id.phone_number_clear:
                mPhoneNumber.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_ACCOUNT_CLEAR, PageIdConstants.MY_LOGIN_BINDING);
                break;
            case R.id.validated_code_clear:
                mValidatedCode.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_CLEAR, PageIdConstants.MY_LOGIN_BINDING);
                break;
            case R.id.phone_validated_code_clear:
                mPhoneValidatedCode.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_CLEAR, PageIdConstants.MY_LOGIN_BINDING);
                break;
        }
    }

    /**
     * 设置清除图标显示还是隐藏
     */
    private void setClearIconShowOrGone(ImageView imageView, boolean isShow) {
        imageView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 验证手机号码和图片验证码是否合法
     *
     * @param phoneNumber
     * @param validatedCode
     * @return
     */
    private boolean validateString(String phoneNumber, String validatedCode) {
        if (TextUtils.isEmpty(phoneNumber)) {
            ToastUtils.getInstance().showShortToast("请输入电话号码!");
            return false;
        }
        if (!phoneNumber.startsWith("1") || phoneNumber.length() != 11) {
            ToastUtils.getInstance().showShortToast("请输入正确的电话号码!");
            return false;
        }
        if (TextUtils.isEmpty(validatedCode)) {
            ToastUtils.getInstance().showShortToast("请输入验证码!");
            return false;
        }
        return true;
    }

    /**
     * 获取图片验证码
     */
    private void setAccredit(final ImageView imageView) {
        LoginDao.requestAccredit(new GetAccreditBitmapSuccessListener(imageView), new GetAccreditBitmapErrorListener());
    }

    /**
     * 获取手机验证码
     *
     * @param phoneNum
     * @param auth
     */
    private void phoneGetAccreditNum(final String phoneNum, final String auth) {
        LoginDao.requestPhoneBundleAccreditNumber(phoneNum, auth, User.getIfengToken(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                logger.debug("response=={}", response);
                int code = getStateCode(response);
                ToastUtils.getInstance().showShortToast(getResult(response, "message"));
                if (code == SUCESSS_STATE) {
                    mCountDown.start();
                    mObtainPhoneCode.setClickable(false);
                } else {//如果获取验证码失败，刷新验证码图标
                    setAccredit(mAccredit);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    logger.error(error.toString(), error);
                }
                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            }
        });
    }

    /**
     * 绑定手机号码
     *
     * @param phoneNum
     * @param cert
     */
    private void bundlePhoneNumber(final String phoneNum, final String cert) {
        LoginDao.bundlePhoneNumber(phoneNum, cert, User.getIfengToken(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                logger.debug("response=={}", response);
                int code = getStateCode(response);
                if (code == SUCESSS_STATE) {
                    User.setRealNameStatus("0");
                    ToastUtils.getInstance().showShortToast("绑定成功");

                    Intent bundleBroad = new Intent(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT);
                    bundleBroad.putExtra(IntentKey.BUNDLE_STATE_FOR_COMMENT, IntentKey.BUNDLE_YES);
                    LocalBroadcastManager.getInstance(BindingPhoneActivity.this).sendBroadcast(bundleBroad);

                    finish();
                } else {
                    User.setRealNameStatus("1");
                    ToastUtils.getInstance().showShortToast(getResult(response, "message"));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    logger.error(error.toString(), error);
                }
                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            }
        });
    }

    private String getResult(String jsonString, String msg) {
        String result = null;
        try {
            JSONObject object = JSON.parseObject(jsonString);
            result = object.getString(msg);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return result;
    }

    private int getStateCode(String jsonString) {
        int code = 2; // 不能用默认0，因为0,1，-1，都为状态码。
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            code = jsonObject.getInteger("code");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return code;
    }


    private static class GetAccreditBitmapSuccessListener implements Response.Listener<Bitmap> {
        private final ImageView imageView;

        public GetAccreditBitmapSuccessListener(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        public void onResponse(Bitmap response) {
            imageView.setImageBitmap(response);
        }
    }

    private static class GetAccreditBitmapErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCountDown.cancel();
    }

    /**
     * 倒计时
     */
    public class CountDown extends CountDownTimer {
        private TextView TimeStates;

        public CountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            state = true;
            mObtainPhoneCode.setBackgroundResource(R.drawable.login_unclickable_bg);
            String cTime = convertMMTime(millisUntilFinished);
            if (TimeStates != null) {
                TimeStates.setText(cTime);
            }
        }

        @Override
        public void onFinish() {
            state = false;
            mHandler.sendEmptyMessage(MESSAGE);
            mObtainPhoneCode.setClickable(true);
        }

        public void setTimeText(TextView tv) {
            TimeStates = tv;
        }

        public String convertMMTime(long time) {
            int totalSeconds = (int) (time / 1000);
            int seconds = totalSeconds % 60;
            return String.format(Locale.US, "%02d" + "秒后重试", seconds);
        }
    }
}
