package com.ifeng.newvideo.login.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.LoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * Created by xt on 2014/11/5.
 * <p/>
 * 注册短信验证码页面
 */
public class CheckPhoneNumberActivity extends BaseFragmentActivity implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(CheckPhoneNumberActivity.class);
    private ImageView ivBack;
    private TextView tvRegisterAccNext, tvArgmentPhone;
    private EditText edRegisterPhoneAccredit;
    private ImageView mAccreditClear;
    private String phoneNum;
    private String password;
    private static CountDown mCountDown;
    private static final int TOTALTIME = 59 * 1000;
    private static final int ONCETIME = 1000;
    private static boolean state = false;
    private static TextView tvRegetAccredit;
    private static final int RESULTCODE = 100;

    //请求数据状态码
    private static final int SUCESSS_STATE = 1;//成功
    private static final int FAILED_STATE = 0;//失败

    private static final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 100) {
                tvRegetAccredit.setText(R.string.login_register_renew_accred);
                tvRegetAccredit.setBackgroundResource(R.drawable.login_clickable_bg);
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_register_check_phone_number);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        Intent intent = getIntent();
        phoneNum = intent.getStringExtra("phoneNum");
        password = intent.getStringExtra("password");
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.login_register_verify_phone));
        tvArgmentPhone = (TextView) findViewById(R.id.tv_register_phone_num);
        tvArgmentPhone.setText(phoneNum);
        ivBack = (ImageView) findViewById(R.id.back);
        ivBack.setOnClickListener(this);
        tvRegisterAccNext = (TextView) findViewById(R.id.btn_next);
        tvRegisterAccNext.setOnClickListener(this);
        mAccreditClear = (ImageView) findViewById(R.id.register_phone_accredit_clear);
        mAccreditClear.setOnClickListener(this);
        tvRegetAccredit = (TextView) findViewById(R.id.tv_reget_accredit);
        tvRegetAccredit.setOnClickListener(this);
        edRegisterPhoneAccredit = (EditText) findViewById(R.id.ed_register_phone_accredit);
        edRegisterPhoneAccredit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable.toString())) {
                    mAccreditClear.setVisibility(View.GONE);
                } else {
                    mAccreditClear.setVisibility(View.VISIBLE);
                }
            }
        });
        mCountDown = new CountDown(TOTALTIME, ONCETIME);
        mCountDown.setTimeText(tvRegetAccredit);
        mCountDown.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_LOGIN_REG_VERIFY);
                finish();
                break;
            case R.id.btn_next:
                if (NetUtils.isNetAvailable(this)) {
                    String accreditNum = edRegisterPhoneAccredit.getText().toString().trim();
                    if (!TextUtils.isEmpty(accreditNum)) {
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_FINISH, PageIdConstants.MY_LOGIN_REG_VERIFY);
                        phoneRegister(phoneNum, password, accreditNum);
                    } else {
                        ToastUtils.getInstance().showShortToast("请输入验证码");
                    }
                } else {
                    ToastUtils.getInstance().showShortToast("无网络");
                }
                break;
            case R.id.tv_reget_accredit:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_REG_GETSMS, PageIdConstants.MY_LOGIN_REG_VERIFY);
                if (!state) {
                    setResult(RESULTCODE, new Intent("false"));
                    finish();
                }
                break;
            case R.id.register_phone_accredit_clear:
                edRegisterPhoneAccredit.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_CLEAR, PageIdConstants.MY_LOGIN_REG_VERIFY);
                break;
        }

    }

    private void phoneRegister(String username, String password, String sendcode) {
        LoginDao.requestPhoneRegister(username, password, sendcode, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int code = getStateCode(response);
                /* 注册成功保存数据，通知我的界面刷新UI */
                if (code == SUCESSS_STATE) {

                    ToastUtils.getInstance().showShortToast(R.string.login_register_sucess);
                    int vipStatus;
                    String vipExp = "";
                    String sign = "";
                    JSONObject object = JSONObject.parseObject(response);
                    vipStatus = object.getIntValue("VIPStatus");
                    vipExp = object.getString("VIPEXP");
                    sign = object.getString("sign");
                    new User(getDataContent(response, "uname"), null, getDataContent(response, "token"),
                            CheckPhoneNumberActivity.this, getDataContent(response, "guid"), null,
                            getDataContent(response, "uname"), getDataContent(response, "realNameStatus"), vipStatus,
                            vipExp,"", sign).storeUserInfo();

                    Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);
                    loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.LOGINING);
                    sendBroadcast(loginBroad);

                    Intent intent = new Intent(CheckPhoneNumberActivity.this, ActivityMainTab.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    finish();

                } else if (code == FAILED_STATE) {
                    if (getResult(response, "message").equals("验证码不正确")) {
                        ToastUtils.getInstance().showShortToast(R.string.login_authcode_error);
                    } else {
                        ToastUtils.getInstance().showShortToast(getResult(response, "message"));
                    }
                }
            }
        }, new PhoneRegisterErrorListener());
    }

    private int getStateCode(String jsonString) {
        int code = 2; // 不能用默认0，因为0,1，-1，都为状态码。
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            code = jsonObject.getInteger("code");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return code;
    }

    private String getResult(String jsonString, String msg) {
        String result = null;
        try {
            JSONObject object = JSON.parseObject(jsonString);
            result = object.getString(msg);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return result;
    }

    private String getDataContent(String jsonString, String content) {
        String result = null;
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            JSONObject jsonObject2 = jsonObject.getJSONObject("data");
            result = jsonObject2.getString(content);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return result;
    }

    public static class CountDown extends CountDownTimer {
        private TextView TimeStates;

        public CountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            state = true;
            String cTime = convertMMTime(millisUntilFinished);
            if (TimeStates != null) {
                TimeStates.setText(cTime);
                TimeStates.setBackgroundResource(R.drawable.login_unclickable_bg);
            }
        }

        @Override
        public void onFinish() {
            state = false;
            Message message = new Message();
            message.what = 100;
            mHandler.sendMessage(message);

            mCountDown = null;
        }

        public void setTimeText(TextView tv) {
            TimeStates = tv;
        }

        public String convertMMTime(long time) {
            int totalSeconds = (int) (time / 1000);
            int seconds = totalSeconds % 60;
            return String.format(Locale.US, "%02d" + "秒后重试", seconds);
        }
    }

    private static class PhoneRegisterErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
    }
}
