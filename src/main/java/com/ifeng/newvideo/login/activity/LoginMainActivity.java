package com.ifeng.newvideo.login.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.constants.ShareConstants;
import com.ifeng.newvideo.coustomshare.SharePlatformUtils;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.LoadDataTask;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLDecoderUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.LoginDao;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.mob.MobSDK;
import com.mob.tools.utils.UIHandler;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 登陆的入口页面
 */
public class LoginMainActivity extends BaseFragmentActivity implements OnClickListener, View.OnTouchListener, Handler.Callback,
        PlatformActionListener {
    private static final Logger logger = LoggerFactory.getLogger(LoginMainActivity.class);

    //网络请求状态码
    private static final int STATE_SUCESSS = 1;//成功
    private static final int STATE_FAILED = 0;//失败

    private static final int USERNAME_TYPE = 1;//用户名登陆类型
    private static final int EMAIL_TYPE = 2;//邮箱登陆类型
    private static final int PHONE_TYPE = 3;//手机登陆类型

    // 认证相关参数
    private static final int MSG_USERID_FOUND = 1;
    private static final int MSG_LOGIN = 2;
    private static final int MSG_AUTH_CANCEL = 3;
    private static final int MSG_AUTH_ERROR = 4;
    private static final int MSG_AUTH_COMPLETE = 5;
    private static final int MSG_NET_FAIL = 6;

    public static final int USER_LOGIN_REQUESTCODE = 101;

    //键盘弹出进行滚动相关
    private ScrollView scrollView;
    private View emptyViewBottom;
    private int scrollY;
    private int scrollViewBottom;

    private LinearLayout llQQLoginBtn, llSinaLoginBtn, llWechatLoginBtn;
    private ImageView ivLoginBack;
    private EditText etAccount, etPwd, etVerify;
    private RelativeLayout rlVerify;//验证码布局
    private ImageView ivWechatLogin;
    private ImageView ivRefreshVerifyCode, ivVerifyCode, ivAccountClear, ivPwdClear, login_iv_verify_clear;
    private TextView tvLogin, tvForgetPwd, tvRegister;

    private String strAccount, strPwd, strVerfy;
    private boolean isWechatClientExist;
    private Platform weChatPlat;
    private ArrayList<SubscribeRelationModel> relationModels = new ArrayList<>();//保存订阅关系list
    private boolean isFromMine;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_main);
        MobSDK.init(this, ShareConstants.MOB_KEY);
        checkPlatformExist();
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(false);
        initView();
    }

    private void initWechatView() {
        ivWechatLogin.setEnabled(isWechatClientExist ? true : false);
        llWechatLoginBtn.setEnabled(isWechatClientExist ? true : false);
    }

    private void initView() {
        /** TOP */
        ivLoginBack = (ImageView) findViewById(R.id.back);
        ivLoginBack.setOnClickListener(this);
        /** scrollView 滑动相关 */
        scrollView = (ScrollView) findViewById(R.id.login_scrollview);
        emptyViewBottom = findViewById(R.id.empty_view_bottom);
        scrollY = (int) this.getResources().getDimension(R.dimen.login_scroll_distance);
        scrollView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                scrollViewBottom = scrollView.getMeasuredHeight();
                return true;
            }
        });
        /** 选择登录界面的按钮 */
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.login_title));
        llQQLoginBtn = (LinearLayout) findViewById(R.id.login_qq);
        llQQLoginBtn.setOnClickListener(this);
        llSinaLoginBtn = (LinearLayout) findViewById(R.id.login_sina);
        llSinaLoginBtn.setOnClickListener(this);
        llWechatLoginBtn = (LinearLayout) findViewById(R.id.login_wechat);
        llWechatLoginBtn.setOnClickListener(this);
        rlVerify = (RelativeLayout) findViewById(R.id.login_rl_verify);
        login_iv_verify_clear = (ImageView) findViewById(R.id.login_iv_verify_clear);
        login_iv_verify_clear.setOnClickListener(this);
        ivWechatLogin = (ImageView) findViewById(R.id.login_iv_wechat);
        etAccount = (EditText) findViewById(R.id.login_et_account);
        etPwd = (EditText) findViewById(R.id.login_et_pwd);
        ivAccountClear = (ImageView) findViewById(R.id.login_account_clear);
        ivPwdClear = (ImageView) findViewById(R.id.login_pwd_clear);
        etVerify = (EditText) findViewById(R.id.login_et_verify);
        ivVerifyCode = (ImageView) findViewById(R.id.login_iv_verify_code);
        tvLogin = (TextView) findViewById(R.id.tv_login_btn);
        tvLogin.setEnabled(false);
        ivRefreshVerifyCode = (ImageView) findViewById(R.id.login_iv_refresh_verify_code);
        tvForgetPwd = (TextView) findViewById(R.id.login_iv_forget_pwd);
        tvRegister = (TextView) findViewById(R.id.title_right_text);
        tvRegister.setText(getString(R.string.login_register));
        tvRegister.setTextColor(getResources().getColor(R.color.subscribe_text_color));
        tvRegister.setVisibility(View.VISIBLE);
        //通过微信客户端是否存在确定图标显示
        initWechatView();
        setTextWatcher();
        //设置验证码
        setAccredit(ivVerifyCode);
        String userName = SharePreUtils.getInstance().getUserAccount();
        etAccount.setText(userName);

        ivVerifyCode.setOnClickListener(this);
        ivRefreshVerifyCode.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        tvForgetPwd.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        ivAccountClear.setOnClickListener(this);
        ivPwdClear.setOnClickListener(this);

        etAccount.setOnTouchListener(this);
        etPwd.setOnTouchListener(this);
        etVerify.setOnTouchListener(this);
        emptyViewBottom.setOnTouchListener(this);

        isFromMine = getIntent().getBooleanExtra(IntentKey.IS_FROM_MINE, false);
    }

    private void setTextWatcher() {
        etAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    ivAccountClear.setVisibility(View.INVISIBLE);
                    tvLogin.setEnabled(false);
                } else {
                    ivAccountClear.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(etPwd.getText().toString())) {
                        tvLogin.setEnabled(true);
                    }
                }
            }
        });
        etPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    tvLogin.setEnabled(false);
                    ivPwdClear.setVisibility(View.INVISIBLE);
                } else {
                    ivPwdClear.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(etAccount.getText().toString())) {
                        tvLogin.setEnabled(true);
                    }
                }
            }
        });

        etVerify.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    login_iv_verify_clear.setVisibility(View.INVISIBLE);
                } else {
                    login_iv_verify_clear.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void checkPlatformExist() {
        isWechatClientExist = SharePlatformUtils.hasWebChat();
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            // 用户信息找到的时候
            case MSG_USERID_FOUND:
                break;

            // 登录操作
            case MSG_LOGIN:
                break;

            // 授权取消的情况
            case MSG_AUTH_CANCEL:
                ToastUtils.getInstance().showShortToast(R.string.common_author_cancel);
                break;

            // 授权错误的情况
            case MSG_AUTH_ERROR:
                ToastUtils.getInstance().showShortToast(R.string.common_author_failed);
                break;

            // 授权完成
            case MSG_AUTH_COMPLETE:
                ToastUtils.getInstance().showShortToast(R.string.common_author_success);
                break;

            case MSG_NET_FAIL:
                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_wechat:
                if (!ClickUtils.isFastDoubleClick() && isWechatClientExist) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGIN_WX, PageIdConstants.MY_LOGIN);
                    authorize(new Wechat());
                }
                break;
            case R.id.login_iv_verify_clear:
                etVerify.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_CLEAR, PageIdConstants.MY_LOGIN);
                break;
            case R.id.login_qq:
                if (!ClickUtils.isFastDoubleClick()) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGIN_QQ, PageIdConstants.MY_LOGIN);
                    authorize(new QQ());
                }
                break;
            case R.id.login_sina:
                if (!ClickUtils.isFastDoubleClick()) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGIN_SINA, PageIdConstants.MY_LOGIN);
                    authorize(new SinaWeibo());
                }
                break;
            case R.id.login_account_clear:
                etAccount.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_ACCOUNT_CLEAR, PageIdConstants.MY_LOGIN);
                break;
            case R.id.login_pwd_clear:
                etPwd.setText("");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_PWD_CLEAR, PageIdConstants.MY_LOGIN);
                break;
            case R.id.login_iv_verify_code:
                setAccredit(ivVerifyCode);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_REFRESH, PageIdConstants.MY_LOGIN);
                break;
            case R.id.login_iv_refresh_verify_code:
                setAccredit(ivVerifyCode);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_REFRESH, PageIdConstants.MY_LOGIN);
                break;
            case R.id.tv_login_btn:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGIN_ENTER, PageIdConstants.MY_LOGIN);
                handleIfengLogin();
                break;
            case R.id.login_iv_forget_pwd:
                if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                    ToastUtils.getInstance().showShortToast(this.getString(R.string.common_net_useless));
                } else {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_PWD_FORGET, PageIdConstants.MY_LOGIN);
                    IntentUtils.startForgetPwdActivity(this);
                }
                break;
            case R.id.title_right_text:
                if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                    ToastUtils.getInstance().showShortToast(this.getString(R.string.common_net_useless));
                } else {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LOGIN_REG, PageIdConstants.MY_LOGIN);
                    Intent registerIntent = new Intent(this, RegisterActivity.class);
                    startActivity(registerIntent);
                }
                break;
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_LOGIN);
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void handleIfengLogin() {
        if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
            ToastUtils.getInstance().showShortToast(this.getString(R.string.common_net_useless));
            return;
        }
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }
        strAccount = etAccount.getText().toString().trim();
        strPwd = etPwd.getText().toString().trim();
        strVerfy = etVerify.getText().toString().trim();
        if (TextUtils.isEmpty(strAccount) && TextUtils.isEmpty(strPwd)) {
            ToastUtils.getInstance().showShortToast(R.string.login_toast_input_empty);
            return;
        }

        if (TextUtils.isEmpty(strAccount)) {
            ToastUtils.getInstance().showShortToast(R.string.login_toast_input_number);
            return;
        }
        if (TextUtils.isEmpty(strPwd)) {
            ToastUtils.getInstance().showShortToast(R.string.login_toast_input_pwd);
            return;
        }

        // 用正则判断复杂字符串可能耗时长，会阻塞主线程，所以用异步
        new LoadDataTask(new LoadDataTask.LoadingData() {
            boolean isMobileNumber;
            boolean isEmail;

            @Override
            public void loadDbAchieve() {
                isMobileNumber = StringUtils.isMobileNumberAll(strAccount);
                isEmail = StringUtils.isEmail(strAccount);
            }

            @Override
            public void result() {
                if (isMobileNumber || isEmail) {
                    ifengLogin(strAccount, strPwd, getLoginType(isMobileNumber, isEmail), strVerfy);
                } else {
                    ToastUtils.getInstance().showShortToast(R.string.login_toast_number_not_exist);
                }
            }
        }).executed();
    }

    @Override
    public void onComplete(Platform platform, int action, HashMap<String, Object> stringObjectHashMap) {
        if (action == Platform.ACTION_USER_INFOR) {
            String sns = translateUTF8(getSns(platform));
            if (!TextUtils.isEmpty(sns)) {
                String uid = translateUTF8(platform.getDb().getUserId());
                String userName = translateUTF8(getRegisterNameWhenInvalid());
                checkOtherBind(sns, uid, userName, platform);
            } else {
                platform.removeAccount(true);
                UIHandler.sendEmptyMessage(MSG_AUTH_ERROR, this);
            }
        }
    }

    @Override
    public void onError(Platform platform, int action, Throwable throwable) {
        if (action == Platform.ACTION_USER_INFOR) {
            UIHandler.sendEmptyMessage(MSG_AUTH_ERROR, this);
        }
        if (throwable != null) {
            logger.error(throwable.toString(), throwable);
        }
    }

    @Override
    public void onCancel(Platform platform, int action) {
        if (action == Platform.ACTION_USER_INFOR) {
            UIHandler.sendEmptyMessage(MSG_AUTH_CANCEL, this);
        }
    }

    /**
     * 判断是进行登录进行认证
     */
    private void authorize(Platform plat) {
        if (!NetUtils.isNetAvailable(this)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (plat.isClientValid()) {
            String userId = plat.getDb().getUserId();
            if (userId != null) {
                plat.getDb().removeAccount();
            }
        }
        plat.setPlatformActionListener(this);
        plat.SSOSetting(false);
        plat.showUser(null);
        CustomerStatistics.setStaticsIdAndTypeFromOutside(plat.getName());
    }

    private String translateUTF8(String string) {
        try {
            return URLEncoderUtils.encodeInUTF8(string);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据平台获取他相应的接口中的平台字段
     */
    private String getSns(Platform platform) {
        if (platform.getName().equals("SinaWeibo")) {
            return "sina";
        }
        if (platform.getName().equals("TencentWeibo")) {
            return "tweibo";
        }
        if (platform.getName().equals("QZone")) {
            return "qzone";
        }
        if (platform.getName().equals("QQ")) {
            return "qzone";
        }
        if (platform.getName().equals("Wechat")) {
            return "weixin";
        }
        return null;
    }

    private int getStateCode(String jsonString) {
        int code = 2; // 不能用默认0，因为0,1，-1，都为状态码。
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            code = jsonObject.getInteger("code");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return code;
    }

    private String getLoginToken(String jsonString) {
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            JSONObject jsonObject2 = jsonObject.getJSONObject("data");
            return jsonObject2.getString("token");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return null;
    }

    private String getIfengUserName(String jsonString) {
        try {
            return getDataContent(jsonString, "username");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return null;
    }

    private String getResult(String jsonString, String msg) {
        try {
            JSONObject object = JSON.parseObject(jsonString);
            return object.getString(msg);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return "";
    }

    /**
     * 用于用户注册时保证用户名唯一
     */
    private String getRegisterNameWhenInvalid() {
        return "ifengvideo" + (System.currentTimeMillis() + "").substring(6);
    }

    private String getRegisterNameWhenUsed(String userName) {
        return userName + (System.currentTimeMillis() + "").substring(6);
    }

    private void checkOtherBind(final String sns, final String uid, final String userName, final Platform platform) {
        LoginDao.requestCheckOtherBind(sns, uid, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int state = getStateCode(response);
                switch (state) {
                    // 绑定成功进行登录操作
                    case STATE_SUCESSS:
                        otherLogin(sns, uid, platform);
                        break;

                    // 绑定操作失败，进行第三方注册
                    case STATE_FAILED:
                        otherRegister(sns, uid, userName, platform);
                        break;
                    default:
                        break;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    logger.error(error.toString(), error);
                }
                UIHandler.sendEmptyMessage(MSG_NET_FAIL, LoginMainActivity.this);
            }
        });
    }

    /**
     * 第三方登录
     */
    private void otherLogin(String sns, final String uid, final Platform platform) {
        LoginDao.requestOtherLogin(sns, uid, PhoneConfig.userKey, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                logger.debug("---> in otherLogin response is {}", response);
                int state = getStateCode(response);
                switch (state) {
                    // 登录成功
                    case STATE_SUCESSS:
                        PlatformDb platformDb = platform.getDb();
                        // 因为QQ空间返回的头像小，因此做替换。
                        String userIcon = platformDb.getUserIcon();
                        // http://wiki.connect.qq.com/get_user_info QQ头像尺寸
                        String platformName = platform.getName();
                        if ((platformName.equals("QQ") || platformName.equals("QZone")) && !TextUtils.isEmpty(userIcon) && userIcon.endsWith("40")) {
                            userIcon = userIcon.substring(0, userIcon.length() - 2) + "100";
                        }
                        int vipStatus;
                        String vipExp = "";
                        String sign = "";
                        JSONObject object = JSONObject.parseObject(response);
                        vipStatus = object.getIntValue("VIPStatus");
                        vipExp = object.getString("VIPEXP");
                        sign = object.getString("sign");
                        User user = new User(platformDb.getUserName(), userIcon, getLoginToken(response),
                                LoginMainActivity.this, getDataContent(response, "guid"),
                                getSns(platform), getIfengUserName(response),
                                getDataContent(response, "realNameStatus"), vipStatus,
                                vipExp, getDataContent(response, "timestamp"), sign);
                        user.storeUserInfo();
                        // FIXME 此处发广播逻辑有重合
                        User.LoginPointAccount();
                        sendLoginStatusBroadcast(true);
                        closeSoftInput();
                        checkIfengUser(getLoginToken(response), getDataContent(response, "realNameStatus"));
//                        finish();
                        break;

                    case STATE_FAILED:
                        ToastUtils.getInstance().showShortToast(R.string.login_failed);
                        break;
                    default:
                        break;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    logger.error(error.toString(), error);
                }
                UIHandler.sendEmptyMessage(MSG_NET_FAIL, LoginMainActivity.this);
            }
        });
    }

    private void otherRegister(final String sns, final String uid, final String userName, final Platform platform) {
        LoginDao.requestOtherRegister(uid, sns, translateUTF8(userName), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int state = getStateCode(response);
                switch (state) {
                    case STATE_SUCESSS:
                        otherLogin(sns, uid, platform);
                        break;

                    case STATE_FAILED:
                        String dialogTitle = "";
                        String tempName = "";

                        // '1002' => '用户名已存在',
                        // '1005' => '用户名格式不正确',
                        // '6002' => '第三方账号已绑定',
                        String msgCode = getResult(response, "msgcode");
                        switch (msgCode) {
                            case "1002":
                                dialogTitle = getString(R.string.nick_name_dialog_title_already_used);
                                tempName = getRegisterNameWhenUsed(userName);
                                showChangeNickNameDialog(dialogTitle, tempName, true, sns, uid, userName, platform);
                                break;
                            case "1005":
                                dialogTitle = getString(R.string.nick_name_dialog_title_invalid);
                                tempName = getRegisterNameWhenInvalid();
                                showChangeNickNameDialog(dialogTitle, tempName, true, sns, uid, userName, platform);
                                break;
                            case "6002":
                            default:
                                otherLogin(sns, uid, platform);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    logger.error(error.toString(), error);
                }
                UIHandler.sendEmptyMessage(MSG_NET_FAIL, LoginMainActivity.this);
            }
        });
    }

    /**
     * 获取验证码
     *
     * @param imageView
     */

    private void setAccredit(final ImageView imageView) {
        LoginDao.requestAccredit(new GetAccreditBitmapSuccessListener(imageView), new GetAccreditBitmapErrorListener());
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            scrollView.smoothScrollTo(0, -scrollY);
        }
        return super.onKeyUp(keyCode, event);
    }

    float startY = 0;
    float moveY = 0;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
//        logger.debug("-----> in dispatchTouchEvent positionY is {}， scrollViewBottom is {}", startY, scrollViewBottom);
        if (ev.getY() > scrollViewBottom) {
            switch (ev.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    startY = ev.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    moveY = ev.getY();
                    scrollView.smoothScrollBy(0, -(int) (moveY - startY));
//                    logger.debug("in dispatchTouchEvent move distance is {}", (int) (moveY - startY));
                    startY = moveY;
                    break;
                default:
                    break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            switch (view.getId()) {
                case R.id.login_et_account:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_ACCOUNT_EDIT, PageIdConstants.MY_LOGIN);
                    ((EditText) view).setCursorVisible(true);
                    scrollView.smoothScrollTo(0, scrollY);
                    break;
                case R.id.login_et_pwd:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_PWD_EDIT, PageIdConstants.MY_LOGIN);
                    ((EditText) view).setCursorVisible(true);
                    scrollView.smoothScrollTo(0, scrollY);
                    break;
                case R.id.login_et_verify:
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_VERIFY_EDIT, PageIdConstants.MY_LOGIN);
                    ((EditText) view).setCursorVisible(true);
                    scrollView.smoothScrollTo(0, scrollY);
                    break;
                default:
                    break;
            }
        }
        return false;
    }

    private static class GetAccreditBitmapSuccessListener implements Response.Listener<Bitmap> {
        private final ImageView imageView;

        public GetAccreditBitmapSuccessListener(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        public void onResponse(Bitmap response) {
            imageView.setImageBitmap(response);
        }
    }

    private static class GetAccreditBitmapErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
        }
    }

    private void ifengLogin(final String userName, String password, int type, String authCode) {
        final String userAccount = etAccount.getText().toString();
        LoginDao.requestIfengLogin(userName, password, type, authCode, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int code = getStateCode(response);
                if (code == STATE_SUCESSS) {// 登陆成功的时候
                    String token = getDataContent(response, "token");
                    String realNameStatus = getDataContent(response, "realNameStatus");
                    String guid = getDataContent(response, "guid");
                    Log.d("login", guid);
                    SharePreUtils.getInstance().setUserAccount(userAccount);
                    closeSoftInput();
                    setResult(RESULT_OK);
                    checkIfengUser(token, realNameStatus);

                } else if (code == STATE_FAILED) {// 登陆失败的时候
                    String authcode = getDataContent(response, "authcode");
                    String errorMsg = getResult(response, "message");
                    setResult(RESULT_CANCELED);
                    if (errorMsg.equals("用户名不存在") || errorMsg.equals("手机号不存在") || errorMsg.equals("邮箱不存在")) {
                        ifengLoginHandler.sendEmptyMessage(MSG_LOGIN_IFENG_FAIL_ACCOUNT_NOT_EXIST);
                        return;
                    }
                    if (errorMsg.equals("操作过于频繁，请稍候重试")) {
                        ifengLoginHandler.sendEmptyMessage(MSG_LOGIN_IFENG_FAIL_OPERATE_TOO_MUCH);
                        return;
                    }
                    if (authcode != null && authcode.equals("true")) {
                        rlVerify.setVisibility(View.VISIBLE);
                        setAccredit(ivVerifyCode);
                        Message msg = new Message();
                        msg.what = MSG_LOGIN_IFENG_FAIL_AUTH_CODE_ERROR;
                        msg.obj = errorMsg;
                        ifengLoginHandler.sendMessage(msg);
                        return;
                    }

                    if (errorMsg.equals("用户名或密码不正确")) {
                        ifengLoginHandler.sendEmptyMessage(MSG_LOGIN_IFENG_FAIL_PWD_ERROR);
                    } else if (errorMsg.equals("尝试登录次数过多，请24小时之后再尝试")) {
                        ifengLoginHandler.sendEmptyMessage(MSG_LOGIN_IFENG_FAIL_TRY_TOO_MUCH);
                    } else {
                        Message msg = new Message();
                        msg.what = MSG_LOGIN_IFENG_FAIL_OTHER;
                        msg.obj = errorMsg;
                        ifengLoginHandler.sendMessage(msg);
                    }
                }
            }
        }, new IfengLoginErrorListener());
    }

    private static final int MSG_LOGIN_IFENG_FAIL_ACCOUNT_NOT_EXIST = 1;
    private static final int MSG_LOGIN_IFENG_FAIL_OPERATE_TOO_MUCH = 2;
    private static final int MSG_LOGIN_IFENG_FAIL_PWD_ERROR = 3;
    private static final int MSG_LOGIN_IFENG_FAIL_TRY_TOO_MUCH = 4;
    private static final int MSG_LOGIN_IFENG_FAIL_OTHER = 5;
    private static final int MSG_LOGIN_IFENG_FAIL_AUTH_CODE_ERROR = 6;
    private static final int MSG_LOGIN_IFENG_FAIL_ERROR = 7;
    static IfengLoginHandler ifengLoginHandler = new IfengLoginHandler();

    static final class IfengLoginHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_LOGIN_IFENG_FAIL_ACCOUNT_NOT_EXIST:
                    ToastUtils.getInstance().showShortToast(R.string.login_username_not_exist);
                    break;
                case MSG_LOGIN_IFENG_FAIL_OPERATE_TOO_MUCH:
                    ToastUtils.getInstance().showShortToast(R.string.login_operate_more);
                    break;
                case MSG_LOGIN_IFENG_FAIL_PWD_ERROR:
                    ToastUtils.getInstance().showShortToast(R.string.login_password_mistake);
                    break;
                case MSG_LOGIN_IFENG_FAIL_TRY_TOO_MUCH:
                    ToastUtils.getInstance().showShortToast("尝试登录次数过多，请24小时之后再尝试");
                    break;
                case MSG_LOGIN_IFENG_FAIL_OTHER:
                    ToastUtils.getInstance().showShortToast(msg.obj.toString());
                    break;
                case MSG_LOGIN_IFENG_FAIL_AUTH_CODE_ERROR:
                    ToastUtils.getInstance().showShortToast(msg.obj.toString());
                    break;
                case MSG_LOGIN_IFENG_FAIL_ERROR:
                    ToastUtils.getInstance().showShortToast("登录失败，请稍后重试");
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Ifeng登录的时候需要验证用户的登录方式，此方法用来验证用户的登录方式,1,2,3分别是用户名，邮箱，手机
     *
     * @param isMobileNumber
     * @param isEmail
     */
    private int getLoginType(boolean isMobileNumber, boolean isEmail) {
        if (isMobileNumber) {
            return PHONE_TYPE;
        }
        if (isEmail) {
            return EMAIL_TYPE;
        }
        return USERNAME_TYPE;
    }

    /**
     * Ifeng 登陆，获取用户信息（昵称，头像）
     */
    private void checkIfengUser(final String token, final String realNameStatus) {
        LoginDao.requestIfengUser(token, PhoneConfig.userKey,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        logger.debug("--- in checkIfengUser response is {} ", response);
                        int code = getStateCode(response);
                        if (code == STATE_SUCESSS) {
                            String nickname = getDataContent(response, "nickname");
                            String nicknameStatus = getDataContent(response, "nicknameStatus");
                            String imageUrl = getDataContent(response, "image");
                            String imageStatus = getDataContent(response, "imageStatus");
                            String username = getDataContent(response, "username");
                            String guid = getDataContent(response, "guid");

                            String tmpName;
                            if (!StringUtils.isBlank(nickname) && !nickname.equalsIgnoreCase("null")) {
                                tmpName = nickname;
                            } else if (!StringUtils.isBlank(username) && !username.equalsIgnoreCase("null")) {
                                tmpName = username;
                            } else {
                                tmpName = "ifengvideo" + guid;
                            }
                            logger.debug("checkIfengUser 昵称 tmpName {}, nickname {}, username {}", tmpName, nickname, username);

                            String noCommitStatus = "0";
                            if (noCommitStatus.equals(imageStatus)) {
                                if (!TextUtils.isEmpty(User.getUserIcon())) {
                                    requestUploadHeaderFileUrl(User.getUserIcon());
                                } else {
                                    requestUploadHeaderFileUrl(imageUrl);
                                }
                            }

                            if (!TextUtils.isEmpty(imageUrl)) {
                                imageUrl = getEscapeString(imageUrl);
                            }

                            JSONObject object = JSONObject.parseObject(response);
                            int vipStatus = object.getIntValue("VIPStatus");
                            String vipExp = object.getString("VIPEXP");
                            String sign = object.getString("sign");

                            User user = new User(tmpName, imageUrl, token, LoginMainActivity.this, guid,
                                    null, tmpName, realNameStatus, vipStatus, vipExp,
                                    getDataContent(response, "lastLoginTime"), sign);
                            user.storeUserInfo();

                            logger.debug("checkIfengUser user {}", user.toString());

                            User.LoginPointAccount();

                            // 昵称状态  0:未设置,类似新号，1:未审核 2:通过 3:未通过 8:正在审核
                            if (noCommitStatus.equals(nicknameStatus)) {
                                if (!TextUtils.isEmpty(User.getUserName())) {
                                    tmpName = User.getUserName();
                                }
                                if (!checkNickName(tmpName)) {
                                    tmpName = getRegisterNameWhenInvalid();
                                }
                                requestNickNameUrl(tmpName, false);

                            } else {
                                sendLoginStatusBroadcast(true);
                                finish();
                            }
                        }
                    }
                },
                new CheckIfengUserErrorListener());
    }

    private static class IfengLoginErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
            ifengLoginHandler.sendEmptyMessage(MSG_LOGIN_IFENG_FAIL_ERROR);
        }
    }

    private static class CheckIfengUserErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error(error.toString(), error);
            }
        }
    }

    /**
     * 获得登录返回结果的详细信息
     */
    private String getDataContent(String jsonString, String content) {
        String result = null;
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            JSONObject jsonObject2 = jsonObject.getJSONObject("data");
            result = jsonObject2.getString(content);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return result;
    }

    /**
     * Ifeng登陆查询用户信息的头像图片时做的相应的解码工作
     */
    private String getEscapeString(String escape) {
        try {
            return URLDecoderUtils.decodeInUTF8(escape);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return escape;
        }
    }

    @Override
    public void onBackPressed() {
        sendLoginStatusBroadcast(false);
        closeSoftInput();
        if (!toRequestNickName) {
            finish();
        }
    }

    /**
     * 发送用户是否登录广播
     *
     * @param isLoginSuccess 是否登录
     */
    private void sendLoginStatusBroadcast(boolean isLoginSuccess) {
        if (isLoginSuccess) {//发送登录成功广播
            Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);
            loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.LOGINING);
            sendBroadcast(loginBroad);

            Intent loginCommentBroad = new Intent(IntentKey.LOGIN_BROADCAST_FOR_COMMENT);
            loginCommentBroad.putExtra(IntentKey.LOGIN_STATE, IntentKey.LOGIN_SUCCESS);
            LocalBroadcastManager.getInstance(LoginMainActivity.this).sendBroadcast(loginCommentBroad);
        } else {//发送未登录广播
            Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);
            loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.UNLOGIN);
            sendBroadcast(loginBroad);
        }
    }

    /**
     * 关闭软键盘
     */
    private void closeSoftInput() {
        if (etAccount != null) {
            InputMethodManager imm = (InputMethodManager) LoginMainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etAccount.getWindowToken(), 0);
        }
    }

    private boolean checkNickName(String content) {
        if (!TextUtils.isEmpty(content)) {
            try {
                if (content.getBytes("GBK").length > 24) {
                    return false;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (!filterRegularName(content)) {
            return false;
        }
        return true;
    }

    private volatile boolean toRequestNickName = false;

    private void requestNickNameUrl(final String nickName, final boolean secondCheck) {
        if (!User.isLogin()) {
            return;
        }
        toRequestNickName = true;

        if (checkNickNameAndToast(nickName)) {
            return;
        }

        String url = String.format(DataInterface.UPLOAD_NICK_NAME, User.getIfengToken(), translateUTF8(nickName));
        CommonDao.sendRequest(url,
                null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        logger.debug("requestNickNameUrl  response {}", response.toString());
                        try {
                            org.json.JSONObject jsonObject = new org.json.JSONObject(response.toString());
                            if ("1".equals(jsonObject.getString("code"))) {
                                UserPointManager.addRewards(UserPointManager.PointType.addByChangeNickName);
                                toRequestNickName = false;
                                sendLoginStatusBroadcast(true);
                                finish();
                            } else {
                                if (isFromMine) {
                                    toRequestNickName = false;
                                    sendLoginStatusBroadcast(true);
                                    finish();
                                } else {
                                    toRequestNickName = true;
                                    //  '5001' => '昵称不存在',
                                    //  '5002' => '昵称已存在',
                                    //  '5003' => '昵称不正确',
                                    //  '5004' => '不能与被修改信息相同',
                                    String msgCode = jsonObject.getString("msgcode");
                                    String dialogTitle = "";
                                    String tempName = "";
                                    switch (msgCode) {
                                        case "5002":
                                            dialogTitle = getString(R.string.nick_name_dialog_title_already_used);
                                            tempName = getRegisterNameWhenUsed(nickName);
                                            break;
                                        case "5003":
                                            dialogTitle = getString(R.string.nick_name_dialog_title_invalid);
                                            tempName = getRegisterNameWhenInvalid();
                                            break;
                                        default:
                                            dialogTitle = getString(R.string.nick_name_dialog_title_modify);
                                            tempName = User.getUserName();
                                            break;
                                    }
                                    showChangeNickNameDialog(dialogTitle, tempName, false, "", "", "", null);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    private boolean checkNickNameAndToast(String nickName) {
        if (TextUtils.isEmpty(nickName)) {
            ToastUtils.getInstance().showShortToast("昵称不可以为空");
            return true;
        }

        if (!TextUtils.isEmpty(nickName)) {
            try {
                if (nickName.getBytes("GBK").length > 24) {
                    ToastUtils.getInstance().showShortToast("字数超出上限了哦");
                    return true;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (!filterRegularName(nickName)) {
            ToastUtils.getInstance().showShortToast("名字格式不对哦，请重新输入");
            return true;
        }
        return false;
    }

    private boolean filterRegularName(String content) {
        // String nickNameRegex = "^[a-z0-9\\-_\\x{4e00}-\\x{9fa5}]{2,24}$";
        String pattern = "^[a-z0-9\\-_\\x{4e00}-\\x{9fa5}]{2,24}$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(content);
        return m.matches();

    }

    public String subStr(String str, int subSLength) throws UnsupportedEncodingException {
        if (str == null)
            return "";
        else {
            int tempSubLength = subSLength;//截取字节数
            String subStr = str.substring(0, str.length() < subSLength ? str.length() : subSLength);//截取的子串
            int subStrByetsL = subStr.getBytes("GBK").length;//截取子串的字节长度
            //int subStrByetsL = subStr.getBytes().length;//截取子串的字节长度
            // 说明截取的字符串中包含有汉字
            while (subStrByetsL > tempSubLength) {
                int subSLengthTemp = --subSLength;
                subStr = str.substring(0, subSLengthTemp > str.length() ? str.length() : subSLengthTemp);
                subStrByetsL = subStr.getBytes("GBK").length;
                //subStrByetsL = subStr.getBytes().length;
            }
            return subStr;
        }
    }

    public boolean isChinese(char c) {

        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);

        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS

                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS

                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A

                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION

                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION

                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {

            return true;

        }

        return false;
    }

    private void requestUploadHeaderFileUrl(String filePath) {
        String url = String.format(DataInterface.UPLOAD_HEADER_IMAGE_URL, User.getIfengToken(), filePath);
        Log.d("file", "uploadFileUrl:" + url);

        CommonDao.sendRequest(url, null,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }

                        try {
                            org.json.JSONObject jsonObject = new org.json.JSONObject(response.toString());
                            if ("1".equals(jsonObject.getString("code"))) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);
    }

    private Dialog changeNickNameDialog;

    private void showChangeNickNameDialog(String dialogTitle, String nickName, final boolean isOtherRegister,
                                          final String sns, final String uid, final String userName, final Platform platform) {

        dismissChangeNickNameDialog();

        LayoutInflater inflater = LayoutInflater.from(LoginMainActivity.this);
        View layout = inflater.inflate(R.layout.custom_nick_name_dialog_layout, null);

        final TextView title = (TextView) layout.findViewById(R.id.tv_dialog_title);
        if (!TextUtils.isEmpty(dialogTitle)) {
            title.setText(dialogTitle);
        }

        final EditText editText = (EditText) layout.findViewById(R.id.et_nick_name);
        final String hintName = nickName;
        if (!TextUtils.isEmpty(hintName)) {
            editText.setHint(hintName);
        }

        final View close = layout.findViewById(R.id.nick_iv_close);
        close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissChangeNickNameDialog();
                logger.debug("close  isOtherRegister {} , sns {} , uid {}, platform {}",
                        isOtherRegister, sns, uid, platform);
                continueLoginWhenCancelNickDialog(isOtherRegister, sns, uid, platform);
            }
        });

        TextView confirmTv = (TextView) layout.findViewById(R.id.tv_nick_confirm);
        confirmTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String finalName = editText.getText().toString();
                if (TextUtils.isEmpty(finalName)) {
                    finalName = hintName;
                }

                logger.debug("confirm  isOtherRegister {} , sns {} , uid {}, platform {}",
                        isOtherRegister, sns, uid, platform);

                if (checkNickNameAndToast(finalName)) {
                    title.setText(getString(R.string.nick_name_dialog_title_invalid));
                    return;
                }
                if (isOtherRegister) {
                    otherRegister(sns, uid, finalName, platform);
                } else {
                    requestNickNameUrl(finalName, false);
                }

                dismissChangeNickNameDialog();
            }
        });

        changeNickNameDialog = new Dialog(this, R.style.shareDialogTheme);
        changeNickNameDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        changeNickNameDialog.setContentView(layout);
        changeNickNameDialog.setCanceledOnTouchOutside(false);
        changeNickNameDialog.setCancelable(true);
        changeNickNameDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                logger.debug("onCancel  isOtherRegister {} , sns {} , uid {}, platform {}",
                        isOtherRegister, sns, uid, platform);
                continueLoginWhenCancelNickDialog(isOtherRegister, sns, uid, platform);
            }
        });
        if (!this.isFinishing()) {
            changeNickNameDialog.show();
        }
    }

    private void continueLoginWhenCancelNickDialog(boolean isOtherRegister, String sns, String uid, Platform platform) {
        toRequestNickName = false;
        if (isOtherRegister) {
            otherRegister(sns, uid, getRegisterNameWhenInvalid(), platform);
        } else {
            sendLoginStatusBroadcast(true);
            finish();
        }
    }

    private void dismissChangeNickNameDialog() {
        if (null != changeNickNameDialog && changeNickNameDialog.isShowing()) {
            changeNickNameDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (changeNickNameDialog != null) {
            changeNickNameDialog.dismiss();
            changeNickNameDialog = null;
        }
    }
}
