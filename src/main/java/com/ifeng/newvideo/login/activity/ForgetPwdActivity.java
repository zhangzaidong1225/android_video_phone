package com.ifeng.newvideo.login.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.widget.BaseWebViewClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 忘记密码
 */
public class ForgetPwdActivity extends BaseFragmentActivity {

    private static final Logger logger = LoggerFactory.getLogger(ForgetPwdActivity.class);

    private WebView webView;
    private String title;
    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity_forget_pwd);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        if (getIntent() != null) {
            title = getIntent().getStringExtra(IntentKey.WEB_VIEW_TITLE);
            url = getIntent().getStringExtra(IntentKey.WEB_VIEW_URL);
        }
        enableExitWithSlip(false);
        initView();
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(title);
        webView = (WebView) findViewById(R.id.wv_forget);
        initWebView();
        findViewById(R.id.back).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_LOGIN_FORGET);
                        finish();
                    }
                });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        WebSettings settings = webView.getSettings();
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(true);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setAllowFileAccess(true);

        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(url);
    }

    private static final class MyWebViewClient extends BaseWebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            view.loadUrl("javascript:window.local_obj.showSource("
                    + "document.getElementsByTagName('html')[0].innerHTML);");
            super.onPageFinished(view, url);
        }

    }
}
