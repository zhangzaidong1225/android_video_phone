package com.ifeng.newvideo.login.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by xt on 2014/11/5.
 */
public class CheckEmailActivity extends BaseFragmentActivity implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(CheckEmailActivity.class);
    private ImageView ivBack;
    private String url = "";
    private TextView tvMailto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_register_check_mail);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(false);

        ((TextView) findViewById(R.id.title)).setText(getString(R.string.login_register_ifeng));
        ivBack = (ImageView) findViewById(R.id.back);
        ivBack.setOnClickListener(this);

        tvMailto = (TextView) findViewById(R.id.tv_mail_into);
        Intent intent = getIntent();
        url = intent.getStringExtra("url");

        SpannableString sp = new SpannableString("去邮箱验证");
        sp.setSpan(new URLSpan(url), 1, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvMailto.setText(sp);
        tvMailto.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }

    }

}
