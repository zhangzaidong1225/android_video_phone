package com.ifeng.newvideo.login.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.fragment.PhoneRegisterFragmet;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;

/**
 * 注册页面
 */
public class RegisterActivity extends BaseFragmentActivity implements RadioGroup.OnCheckedChangeListener {
    private RadioGroup registerGroup;
    private PhoneRegisterFragmet phoneRegisterFragmet;
    private ImageView ivBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_register_main);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        initView();
    }

    private void initView() {
        registerGroup = (RadioGroup) findViewById(R.id.register_grp);
        registerGroup.setOnCheckedChangeListener(this);

        ivBack = (ImageView) findViewById(R.id.back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.MY_LOGIN_REG);
                finish();
            }
        });

        ((TextView) findViewById(R.id.title)).setText(getString(R.string.login_register_title));

        phoneRegisterFragmet = new PhoneRegisterFragmet();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.llregister_choose, phoneRegisterFragmet);
        //提交修改
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.phone_register:
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.llregister_choose, phoneRegisterFragmet);
                transaction.commit();
                break;

        }
    }
}
