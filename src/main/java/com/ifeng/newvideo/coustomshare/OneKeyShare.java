package com.ifeng.newvideo.coustomshare;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.constants.ShareConstants;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.domains.ShareRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.FragmentLive;
import com.ifeng.newvideo.ui.UniversalChannelFragment;
import com.ifeng.newvideo.ui.VRChannelFragment;
import com.ifeng.newvideo.ui.ad.ADMorePopWindow;
import com.ifeng.newvideo.ui.adapter.ListAdapter4BigPictureChannel;
import com.ifeng.newvideo.ui.live.vr.VRBaseActivity;
import com.ifeng.newvideo.ui.mine.signin.ActiveInfo;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.dao.LoginDao;
import com.ifeng.video.dao.util.IfengImgUrlUtils;
import com.mob.MobSDK;
import com.mob.tools.utils.UIHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.mob.tools.utils.ResHelper.getStringRes;


/**
 * 分享入口
 * <p/>
 * Android 不同平台分享内容的详细说明
 * http://wiki.mob.com/%E4%B8%8D%E5%90%8C%E5%B9%B3%E5%8F%B0%E5%88%86%E4%BA%AB%E5%86%85%E5%AE%B9%E7%9A%84%E8%AF%A6%E7%BB%86%E8%AF%B4%E6%98%8E/
 * <p/>
 * Created by xt on 2015/1/7.
 */
public class OneKeyShare implements PlatformActionListener, Handler.Callback, SharePlatform {
    private static final Logger logger = LoggerFactory.getLogger(OneKeyShare.class);


    //网络请求状态码
    private static final int SUCCESS_STATE = 1;//成功
    private static final int FAILED_STATE = 0;//失败
    private static final String HTTP_V_IFENG_COM_APPS = "http://v.ifeng.com/apps/";
    private final Context context;

    private Platform.ShareParams shareParams;
    private Platform platform;
    private boolean isLand = false;
    private PopupWindow pop;
    private View clickView;
    public NotifyShareCallback notifyShareCallback;
    public static boolean isLandWebQQ = false;//是否为横屏WebQQ分享

    private static final int MSG_ACTION_CALLBACK = 2;
    private static final int MSG_DOWNLOAD = 4;
    private final HashMap<String, Object> shareParamsMap;
    private HashMap<String, Object> shareParamsTemp;
    private static final HashMap<String, String> hashMap = new HashMap<String, String>();
    private boolean isLive;
    private boolean isAD;
    private boolean isSignIn;
    private boolean isVRLive;
    private boolean isVRVideo;
    private boolean isFM;
    private int shareType = -100;

    /**
     * 默认video
     */
    public int getShareType() {
        return shareType == -100 ? Platform.SHARE_VIDEO : shareType;
    }

    public void setShareType(int shareType) {
        this.shareType = shareType;
    }

    ADMorePopWindow adMorePopWindow;

    public OneKeyShare(Context context) {
        try {
            MobSDK.init(context, ShareConstants.MOB_KEY);
        } catch (Exception e) {
            logger.error("OneKeyShare ShareSDK.initSDK error! {}", e);
        }
        this.context = context;
        shareParamsMap = new HashMap<String, Object>();
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }


    /**
     * title标题，在印象笔记、邮箱、信息、微信（包括好友、朋友圈和收藏）、
     * 易信（包括好友、朋友圈）、人人网和QQ空间使用，否则可以不提供
     */
    private void setTitle(String title) {
        shareParamsMap.put("title", title);
    }

    private String getTitle() {
        return (String) shareParamsMap.get("title");
    }

    /**
     * imageUrl是图片的网络路径，新浪微博、人人网、QQ空间和Linked-In支持此字段
     */
    private void setImageUrl(String imageUrl) {
        if (TextUtils.isEmpty(imageUrl))
            imageUrl = context.getString(R.string.share_default_image);
        shareParamsMap.put("imageUrl", imageUrl);
    }

    private String getImageUrl() {
        return (String) shareParamsMap.get("imageUrl");
    }

    /**
     * text是分享文本，所有平台都需要这个字段
     */
    private void setText(String text) {
        shareParamsMap.put("text", text);
    }

    private String getText() {
        return (String) shareParamsMap.get("text");
    }


    /**
     * text是分享文本，所有平台都需要这个字段
     */
    private void setCoustomText(String text) {
        shareParamsMap.put("CoustomText", text);
    }

    private String getCoustomText() {
        return (String) shareParamsMap.get("CoustomText");
    }

    /**
     * url在微信（包括好友、朋友圈收藏）和易信（包括好友和朋友圈）支付宝中使用，否则可以不提供
     */
    private void setUrl(String url) {
        shareParamsMap.put("url", url);
    }


    private String getUrl() {
        return handleShareUrlParams((String) shareParamsMap.get("url"));
    }

    /**
     * titleUrl是标题的网络链接，仅在人人网和QQ空间使用，否则可以不提供
     */
    private void setTitleUrl(String titleUrl) {
        shareParamsMap.put("titleUrl", titleUrl);
    }

    private String getTitleUrl() {
        return handleShareUrlParams((String) shareParamsMap.get("titleUrl"));
    }

    /**
     * 处理分享地址后的参数
     * FM音频分享平台名称规则
     * 分享页来源，在现有分享链接下增加分享平台属性  & _share_vapp_[SNS]
     */
    private String handleShareUrlParams(String shareUrl) {
        String url = replaceAudioSharePlat(shareUrl);
        url = addShareVappSNS(url);
        return url;
    }

    private String replaceAudioSharePlat(String url) {
        if (url.contains("AudioSharePlat")) {
            String sharePlat = "";
            if (platform != null) {
                if (platform.getName().equals("QZone")) {
                    sharePlat = "qzone";
                } else if (platform.getName().equals("Wechat")) {
                    sharePlat = "weixin";
                } else if (platform.getName().equals("SinaWeibo")) {
                    sharePlat = "sina";
                } else {
                    sharePlat = "FM";
                }
            }
            url = url.replace("AudioSharePlat", sharePlat);
        }
        return url;
    }

    /**
     * @param url 分享链接
     * @return 在现有分享链接下增加分享平台属性  _share_vapp_[SNS]
     */
    private String addShareVappSNS(String url) {
        if (TextUtils.isEmpty(url)) {
            return HTTP_V_IFENG_COM_APPS;
        }
        return new StringBuilder()
                .append(url)
                .append(url.contains("?") ? "&" : "?")
                .append("_share_vapp_")
                .append(getShareVappSNS(platform))
                .toString();
    }

    /**
     * 二次分享需求，仅点播
     * 在分享地址后加参数 t 和 p
     * V7.1.0添加
     *
     * @param videoTitle 视频标题
     * @param imgUrl     图片地址
     * @param shareUrl   分享地址
     * @return t = URLEncoderUtils.encodeInUTF8(videoTitle)       p = imgUrl
     */
    private String add_T_P(String videoTitle, String imgUrl, String shareUrl) {
        if (TextUtils.isEmpty(shareUrl)) {
            return HTTP_V_IFENG_COM_APPS;
        }
        if (true) { // TODO 740去掉t  p 参数
            return shareUrl;
        }
        try {
            String picUrl = isWechatOrWechatMoments()
                    ? IfengImgUrlUtils.getUrl(imgUrl, IfengImgUrlUtils.W300_H300)
                    : imgUrl;

            return new StringBuilder()
                    .append(shareUrl.trim())
                    .append(shareUrl.contains("?") ? "&" : "?") // 已有?的话，拼接&
                    .append("t=").append(URLEncoderUtils.encodeInUTF8(videoTitle))
                    .append("&p=").append(picUrl)
                    .toString();
        } catch (Exception e) {
            logger.error("shareVod param error");
            return shareUrl.trim();
        }
    }

    private boolean isWechatOrWechatMoments() {
        if (platform == null || TextUtils.isEmpty(platform.getName())) {
            return false;
        }
        return Wechat.NAME.equalsIgnoreCase(platform.getName())
                || WechatMoments.NAME.equalsIgnoreCase(platform.getName());
    }

    public void setMapPlatform(Map<String, Platform> platforms, String text) {
        if (platforms.size() <= 0) {
            return;
        }

        setCoustomText(text);
        for (String key : platforms.keySet()) {
            shareTo(platforms.get(key), false);
        }
    }

    /**
     * 设置是否为横屏 *
     */
    public void setIsLandScape(boolean isLandScape) {
        this.isLand = isLandScape;
    }


    private void callbackShow(boolean isShow) {
        //先报了分享的page.然后当前页重新统计时间
        //1.video,topic,fm播放页activity回调 
        //2.大图和直播 fragment回调
        //3.H5(AD)和Vr播放页 activity回调
        if (!(context instanceof Activity) || ((Activity) context).isFinishing()) {
            return;
        }
        if (notifyShareCallback == null) return;

        if (context instanceof NotifyShareCallback
                || notifyShareCallback instanceof VRChannelFragment
                || notifyShareCallback instanceof ListAdapter4BigPictureChannel
                || notifyShareCallback instanceof FragmentLive) {
            notifyShareCallback.notifyShareWindowIsShow(isShow);
        }
    }

    private void show() {
        if (!(context instanceof Activity) || ((Activity) context).isFinishing())
            return;
        //无用
        if (notifyShareCallback instanceof UniversalChannelFragment) {
            notifyShareCallback.notifyShareWindowIsShow(true);
        }
        callbackShow(true);

        if (isLand) {
            initHorizontalView(clickView);
        } else {
            initVerticalView(clickView);
        }
    }

    public PopupWindow getPop() {
        return pop;
    }

    private void initVerticalView(View clickView) {
        if (clickView == null)
            return;
        ShareVerticalPopWindow verticalPopWindow = new ShareVerticalPopWindow(context) {
            public boolean onTouchEvent(MotionEvent event) {
                return true;
            }
        };
        pop = new PopupWindow(verticalPopWindow, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT, false);
        pop.setBackgroundDrawable(new BitmapDrawable());
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
        verticalPopWindow.setPop(pop);
        verticalPopWindow.setOneKeyShare(this);
        int[] location = {0, 0};
        pop.showAtLocation(clickView, Gravity.BOTTOM, location[0], location[1]);
        final long enterTime = System.currentTimeMillis();
        pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //页面类别，分为： 1 播放页：video 2 频道列表页：ch 3 其他页：other
                int type = context instanceof ActivityMainTab ? 2 : 1;
                PageActionTracker.endPageShare(true, type, enterTime);
                callbackShow(false);
            }
        });
    }

    private void initHorizontalView(View clickView) {
        if (clickView == null)
            return;
        ShareHorizontalPopWindow horizontalPopWindow = new ShareHorizontalPopWindow(context) {
            public boolean onTouchEvent(MotionEvent event) {
                return true;
            }
        };
        pop = new PopupWindow(horizontalPopWindow, FrameLayout.LayoutParams.WRAP_CONTENT, DisplayUtils.convertDipToPixel(165), false);
        pop.setBackgroundDrawable(new BitmapDrawable());
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
        horizontalPopWindow.setPop(pop);
        horizontalPopWindow.setOneKeyShare(this);
        int[] location = {0, 0};
        pop.showAtLocation(clickView, Gravity.CENTER_VERTICAL | Gravity.LEFT, location[0] + clickView.getWidth() + 15, location[1]);
        final long enterTime = System.currentTimeMillis();
        pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //页面类别，分为： 1 播放页：video 2 频道列表页：ch 3 其他页：other
                int type = context instanceof ActivityMainTab ? 2 : 1;
                PageActionTracker.endPageShare(false, type, enterTime);
                callbackShow(false);
            }
        });
    }

    private ADMorePopWindow initADHorizontalView(String pageUrl, String title, ADMorePopWindow.RefreshCallBack refreshCallback) {
        adMorePopWindow = new ADMorePopWindow(context, refreshCallback) {
            public boolean onTouchEvent(MotionEvent event) {
                return true;
            }
        };
        //vr横屏，ad竖屏
        final boolean isFromVR = context instanceof VRBaseActivity;
        if (pop != null) {
            return adMorePopWindow;
        }
        pop = new PopupWindow(adMorePopWindow, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT, false);
        pop.setBackgroundDrawable(new BitmapDrawable());
        pop.setOutsideTouchable(false);
        pop.setFocusable(false);
        adMorePopWindow.setPop(pop);
        adMorePopWindow.setPageUrl(pageUrl);
        adMorePopWindow.setTitle(title);
        adMorePopWindow.setOneKeyShare(this);
        final long enterTime = System.currentTimeMillis();
        pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (isFromVR) {
                    PageActionTracker.endPageShare(false, 1, enterTime);
                } else {
                    PageActionTracker.endPageShare(true, 3, enterTime);
                }
                callbackShow(false);
            }
        });
        return adMorePopWindow;
    }

    public void showOrDismissADPopupWindow(View parentView) {
        if (pop == null)
            return;
        if (pop.isShowing()) {
            if (adMorePopWindow != null) {
                adMorePopWindow.exitAnim();
            }
            pop.dismiss();
        } else {
            callbackShow(true);
            pop.showAtLocation(parentView, Gravity.BOTTOM, 0, 0);
            if (adMorePopWindow != null) {
                adMorePopWindow.startAnim();
            }
        }
    }

    public ADMorePopWindow getADMorePopWindow() {
        return adMorePopWindow;
    }

    /**
     * 分享完成回调
     */
    @Override
    public void onComplete(Platform platform, int action, HashMap<String, Object> stringObjectHashMap) {
        Message msg = new Message();
        msg.what = MSG_ACTION_CALLBACK;
        switch (action) {
            case Platform.ACTION_AUTHORIZING: // 授权成功
                msg.arg1 = 1;
                break;

            case Platform.ACTION_SHARE:  // 分享成功
                msg.arg1 = 4;
                break;

        }
        msg.arg2 = action;
        msg.obj = platform;
        UIHandler.sendMessage(msg, this);
        if (!User.isLogin()) {
            if (getSnsName(platform) != null) {
                login(platform);
            }
        }
    }

    /**
     * 错误回调
     */
    @Override
    public void onError(Platform platform, int action, Throwable throwable) {
        throwable.printStackTrace();
        logger.error("in onError error{}", throwable.toString());
        Message msg = new Message();
        msg.what = MSG_ACTION_CALLBACK;
        switch (action) {

            // 授权错误
            case Platform.ACTION_AUTHORIZING:
                msg.arg1 = 3;
                break;

            // 分享错误
            case Platform.ACTION_SHARE:
                msg.arg1 = 6;
                break;
        }
        msg.arg2 = action;
        msg.obj = throwable;
        UIHandler.sendMessage(msg, this);
    }

    /**
     * 取消回调
     */
    @Override
    public void onCancel(Platform platform, int action) {
        Message msg = new Message();
        msg.what = MSG_ACTION_CALLBACK;
        switch (action) {
            // 授权取消
            case Platform.ACTION_AUTHORIZING:
                msg.arg1 = 3;
                break;

            // 分享取消
            case Platform.ACTION_SHARE:
                msg.arg1 = 5;
                break;
        }
        msg.arg2 = action;
        msg.obj = platform;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_DOWNLOAD: {
                String text = String.valueOf(msg.getData().get("text"));
                String downloadUrl = String.valueOf(msg.getData().get("downloadUrl"));
                boolean isInstall = msg.getData().getBoolean("isInstall");
                showDialog(text, downloadUrl, isInstall);
            }
            break;
            case MSG_ACTION_CALLBACK: {
                switch (msg.arg1) {
                    case 1: // 授权成功
                        ToastUtils.getInstance().showShortToast(R.string.common_author_success);
                        break;
                    case 2:  // 授权失败
                        String expName = msg.obj.getClass().getSimpleName();
                        if ("WechatClientNotExistException".equals(expName)
                                || "WechatTimelineNotSupportedException".equals(expName)
                                || "WechatFavoriteNotSupportedException".equals(expName)) {
                            int resId = getStringRes(context, "wechat_client_inavailable");
                            if (resId > 0) {
                                ToastUtils.getInstance().showShortToast(context.getString(resId));
                            }
                        } else if ("QQClientNotExistException".equals(expName)) {
                            int resId = getStringRes(context, "qq_client_inavailable");
                            if (resId > 0) {
                                ToastUtils.getInstance().showShortToast(context.getString(resId));
                            }
                        } else {
                            int resId = getStringRes(context, "share_failed");
                            if (resId > 0) {
                                ToastUtils.getInstance().showShortToast(context.getString(resId));
                            }
                        }
                        break;
                    case 3:// 授权取消
                        ToastUtils.getInstance().showShortToast(R.string.common_author_cancel);
                        break;
                    case 4:// 分享成功
                        ToastUtils.getInstance().showShortToast(R.string.share_completed);
                        sendShareStatistics(id, echid, chid, wmid, getPlatformId(), true, page);
                        SendSmartStatisticUtils.sendShareOperatorStatistics(context, type, keyword, title);
                        if (getShareType() == Platform.SHARE_VIDEO) {
                            UserPointManager.addRewards(UserPointManager.PointType.addByShareUrl);
                        }
                        break;
                    case 5:// 分享取消
                        sendShareStatistics(id, echid, chid, wmid, getPlatformId(), false, page);
                        ToastUtils.getInstance().showShortToast(R.string.share_cancel_share);
                        break;
                    case 6:// 分享失败
                        sendShareStatistics(id, echid, chid, wmid, getPlatformId(), false, page);
                        ToastUtils.getInstance().showShortToast(R.string.share_failed);
                        break;
                }
            }
            break;
        }
        return false;
    }

    @Override
    public void shareToPlatform(Platform platform, boolean isShowEditPage) {
        logger.debug("---- shareVodWithoutPopWindow platform is null ? {}", platform == null);
        if (platform == null) {
            return;
        }
        this.platform = platform;
        shareTo(platform, isShowEditPage);
    }


    /**
     * 分享
     */
    private void shareTo(Platform platform, boolean isShowEditPage) {
        if (isShowEditPage) {//如果进入编辑页
            Intent intent = new Intent();
            intent.setClass(context, EditPage.class);
            //统计相关数据
            intent.putExtra(ShareConstants.SHARE_ID, id);
            intent.putExtra(ShareConstants.SHARE_ECHID, echid);
            intent.putExtra(ShareConstants.SHARE_CHID, chid);
            intent.putExtra(ShareConstants.SHARE_WMID, wmid);
            //分享相关数据
            intent.putExtra(ShareConstants.SHARE_TEXT, getText());
            intent.putExtra(ShareConstants.SHARE_IMG, getImageUrl());
            intent.putExtra(ShareConstants.SHARE_URL, getUrl());
            intent.putExtra(ShareConstants.SHARE_LIVE, isLive);
            intent.putExtra(ShareConstants.SHARE_FM, isFM);
            intent.putExtra(ShareConstants.SHARE_VR_LIVE, isVRLive);
            intent.putExtra(ShareConstants.SHARE_VR_VIDEO, isVRVideo);
            intent.putExtra(ShareConstants.SHARE_AD, isAD);
            intent.putExtra(ShareConstants.SHARE_SIGN, isSignIn);
            context.startActivity(intent);
            CustomerStatistics.setStaticsIdAndTypeFromOutside(platform.getName());
        } else {//不进入编辑页
            if (platform == null) {
                return;
            }
            platform.SSOSetting(false);
            String name = platform.getName();
            CustomerStatistics.setStaticsIdAndTypeFromOutside(name);
            boolean isWechat = "WechatMoments".equals(name) || "Wechat".equals(name) || "WechatFavorite".equals(name);

            /** 一个客户端不能分享主要有两种原因，一种是没有客户端，另一种就是版本低 */
            // 微信
            if (isWechat && !platform.isClientValid()) {
                Message msg = Message.obtain();
                msg.what = MSG_DOWNLOAD;
                Bundle data = new Bundle();
                data.putString("text", "微信");
                data.putString("downloadUrl", context.getString(R.string.share_wachat_web_site));
                data.putBoolean("isInstall", false);
                msg.setData(data);
                UIHandler.sendMessage(msg, this);
                return;
            }

            /**
             * QQ空间分享
             * platform.isClientValid()检测不到QQ6.0及以上版本
             * 因而此处不做QQ客户端检测判断，未安装QQ则调用QQ空间网页版进行分享
             */
            boolean isQZone = QZone.NAME.equals(name);
            boolean isQQ = QQ.NAME.equals(name);
            if ((isQQ || isQZone) && !platform.isClientValid()) {
                Message msg = new Message();
                msg.what = MSG_DOWNLOAD;
                Bundle data = new Bundle();
                data.putString("text", "QQ");
                data.putString("downloadUrl", context.getString(R.string.share_qq_web_site));
                data.putBoolean("isInstall", false);
                msg.setData(data);
                UIHandler.sendMessage(msg, this);
                return;
            }

            boolean isAlipay = "Alipay".equals(name);
            if (isAlipay && !platform.isClientValid()) {
                Message msg = new Message();
                msg.what = MSG_DOWNLOAD;
                Bundle data = new Bundle();
                data.putString("text", "支付宝");
                data.putString("downloadUrl", context.getString(R.string.share_alipay_web_site));
                data.putBoolean("isInstall", false);
                msg.setData(data);
                UIHandler.sendMessage(msg, this);
                return;
            }

            if (notifyShareCallback instanceof UniversalChannelFragment) {
                notifyShareCallback.notifyShare(null, false);
            }

            if (SinaWeibo.NAME.equals(name)) {
                toSinaWeibo();
            } else if (Wechat.NAME.equals(name)) {
                toWechat();
            } else if (WechatMoments.NAME.equals(name)) {
                toWechatMoments();
            } else if (isQQ) {
                if (isLand && !platform.isClientValid()) {
                    isLandWebQQ = true;
                }
                toQQ();
            } else if (QZone.NAME.equals(name)) {
                toQzone();
            } else if (Alipay.NAME.equals(name)) {
                toAlipay();
            }
            platform.setPlatformActionListener(this);//设置分享回调
            shareParams = new Platform.ShareParams(shareParamsTemp);
            platform.share(shareParams);
        }
    }

    /************************************************************************************************/

    /**
     * 可以通过此方法，弹出PopWindow进行分享
     *
     * @param videoTitle          分享的title
     * @param imgUrl              分享图片url
     * @param shareUrl            分享地址.传接口中的mUrl
     *                            通过shareInfo获取地址:将guid ,videoUrl置空，先调用getShareUrl获取分享地址。
     * @param clickView           点击View
     * @param notifyShareCallback 回调
     * @param isLand              是否为横屏
     */
    public void shareTopic(String videoTitle, String imgUrl, String shareUrl, View clickView,
                           NotifyShareCallback notifyShareCallback, boolean isLand) {
        this.isLand = isLand;
        this.clickView = clickView;
        setTitle(videoTitle);
        setText(videoTitle);
        setImageUrl(imgUrl);
        this.notifyShareCallback = notifyShareCallback;
        String url;
        if (!TextUtils.isEmpty(shareUrl)) {
            url = shareUrl.trim();
        } else {
            url = HTTP_V_IFENG_COM_APPS;
        }
        setUrl(url);
        setTitleUrl(url);

        if (!isLand) {
            show();
        }
    }

    /**
     * 点播分享，弹出分享弹层后选择第三方
     */
    public void shareVodWithPopWindow(String videoTitle, String imgUrl, String shareUrl, View clickView,
                                      NotifyShareCallback notifyShareCallback, boolean isLand) {
        this.isLand = isLand;
        this.clickView = clickView;
        this.notifyShareCallback = notifyShareCallback;
        setVodParams(videoTitle, imgUrl, shareUrl);
        if (!isLand) {
            show();
        }
    }


    /**
     * VR点播分享，弹出分享弹层后选择第三方
     */
    public void shareVRVodWithPopWindow(boolean isVRVideo, String videoTitle, String imgUrl, String shareUrl,
                                        View clickView, NotifyShareCallback notifyShareCallback, boolean isLand) {
        this.isVRVideo = isVRVideo;
        this.isLand = isLand;
        this.clickView = clickView;
        this.notifyShareCallback = notifyShareCallback;
        setVodParams(videoTitle, imgUrl, shareUrl);
        if (!isLand) {
            show();
        }
    }

    /**
     * 点播底页 快捷分享，不需弹出分享弹层
     *
     * @param platform       平台，如new QQ()
     * @param videoTitle     视频标题
     * @param imgUrl         分享图片地址
     * @param shareUrl       分享地址
     * @param isShowEditPage 仅新浪微博传true, 其他传false
     */
    public void shareVodWithoutPopWindow(Platform platform, String videoTitle, String imgUrl, String shareUrl, boolean isShowEditPage) {
        setVodParams(videoTitle, imgUrl, shareUrl);
        shareTo(platform, isShowEditPage);
    }

    /**
     * 设置【点播】分享参数
     * 为减少重复代码、方便维护，抽取出的方法
     */
    private void setVodParams(String videoTitle, String imgUrl, String shareUrl) {
        setTitle(videoTitle);
        setText(videoTitle);
        setImageUrl(imgUrl);
        String url = add_T_P(videoTitle, imgUrl, shareUrl);
        setUrl(url);
        setTitleUrl(url);
    }

    /**
     * FM分享，弹出分享弹层后选择第三方
     */
    public void shareFMWithPopWindow(String videoTitle, String imgUrl, String shareUrl, View clickView,
                                     NotifyShareCallback notifyShareCallback, boolean isLand) {
        this.isLand = isLand;
        this.isFM = true;
        this.clickView = clickView;
        this.notifyShareCallback = notifyShareCallback;
        setTitle(videoTitle);
        setText(videoTitle);
        setImageUrl(imgUrl);
        setUrl(shareUrl);
        setTitleUrl(shareUrl);
        if (!isLand) {
            show();
        }
    }

    /**
     * 广告页分享
     */
    public void shareAd(String pageUrl, String title, String imageUrl, ADMorePopWindow.RefreshCallBack refreshCallback) {
        logger.debug("shareAd pageUrl is {} , title is {}", pageUrl, title);
        this.isAD = true;
        setTitle(title);
        setText(title);
        setImageUrl(imageUrl);
        setUrl(pageUrl);
        setTitleUrl(pageUrl);
        initADHorizontalView(pageUrl, title, refreshCallback);
    }

    /**
     * VR直播页分享
     */
    public void shareVRLive(String pageUrl, String title, String imageUrl, ADMorePopWindow.RefreshCallBack refreshCallback) {
        logger.debug("shareAd pageUrl is {} , title is {}", pageUrl, title);
        this.isVRLive = true;
        setTitle(title);
        setText(title);
        setImageUrl(imageUrl);
        setUrl(pageUrl);
        setTitleUrl(pageUrl);
        initADHorizontalView(pageUrl, title, refreshCallback);
    }

    /**
     * VR点播页分享
     */
    public void shareVRVideo(String pageUrl, String title, String imageUrl, ADMorePopWindow.RefreshCallBack refreshCallback) {
        logger.debug("shareAd pageUrl is {} , title is {}", pageUrl, title);
        this.isVRVideo = true;
        setTitle(title);
        setText(title);
        setImageUrl(imageUrl);
        setUrl(pageUrl);
        setTitleUrl(pageUrl);
        initADHorizontalView(pageUrl, title, refreshCallback);
    }

    /**
     * 直播分享
     *
     * @param liveUrl
     * @param programTitle
     * @param notifyShareCallback
     * @param channelName
     * @param imageUrl
     */
    public void shareLive(String liveUrl, View clickView, String programTitle,
                          NotifyShareCallback notifyShareCallback, String channelName, String imageUrl, boolean isLand) {
        logger.debug("----> shareLive imageUrl is {}", imageUrl);
        this.isLand = isLand;
        this.clickView = clickView;
        this.isLive = true;
        String url = liveUrl;
        this.notifyShareCallback = notifyShareCallback;
        if (TextUtils.isEmpty(liveUrl) || "".equals(liveUrl.trim())) {
            url = " " + HTTP_V_IFENG_COM_APPS + " ";
        }
        setText(programTitle);
        setTitle(programTitle);
        setTitleUrl(url);
        setImageUrl(imageUrl);
        setUrl(url);
        shareParamsMap.put("live", true);
        if (!isLand) {
            show();
        }
    }

    /**
     * H5分享
     */
    public void shareH5Live(String liveUrl, View clickView, String programTitle,
                            NotifyShareCallback notifyShareCallback, String desc, String imageUrl, boolean isLand) {
        logger.debug("----> shareLive imageUrl is {}", imageUrl);
        this.isLand = isLand;
        this.clickView = clickView;
        this.isLive = true;
        String url = liveUrl;
        this.notifyShareCallback = notifyShareCallback;
        if (TextUtils.isEmpty(liveUrl) || "".equals(liveUrl.trim())) {
            url = " " + HTTP_V_IFENG_COM_APPS + " ";
        }
        setText(desc);
        setTitle(programTitle);
        setTitleUrl(url);
        setImageUrl(imageUrl);
        setUrl(url);
        shareParamsMap.put("live", true);
        if (!isLand) {
            show();
        }
    }


    /**
     * 签到页面分享
     *
     * @param activityInfo
     */
    public void shareSignIn(ActiveInfo activityInfo) {
        isLand = false;
        isSignIn = true;
        if (!StringUtils.isBlank(activityInfo.introUrl) && !StringUtils.isBlank(activityInfo.shareContent)) {
//            setTitle(context.getString(R.string.sign_in_title));
            setTitle(activityInfo.shareContent);
            setTitleUrl(activityInfo.introUrl);
            setText(activityInfo.shareContent);
            setImageUrl(context.getString(R.string.share_default_image));
            setUrl(activityInfo.introUrl);
            shareParamsMap.put("sign", true);
            show();
        }
    }

    /**
     * 根据平台获取他相应的接口中的平台字段
     * weibo	新浪微博
     * weixin	微信好友
     * wxzone	微信朋友圈
     * qq	QQ好友
     * qzone	QQ空间
     * zfb	支付宝好友
     * zfbzone	支付宝朋友圈
     * h5	视频H5页面
     * newsapp	新闻客户端
     */
    private String getShareVappSNS(Platform platform) {
        if (platform == null || TextUtils.isEmpty(platform.getName())) {
            return "";
        }
        if (platform.getName().equals(SinaWeibo.NAME)) {
            return "weibo";
        }
        if (platform.getName().equals(Wechat.NAME)) {
            return "weixin";
        }
        if (platform.getName().equals(WechatMoments.NAME)) {
            return "wxzone";
        }
        if (platform.getName().equals(QQ.NAME)) {
            return "qq";
        }
        // h5	    视频H5页面
        // newsapp	新闻客户端
        if (platform.getName().equals(QZone.NAME)) {
            return "qzone";
        }
        if (platform.getName().equals(Alipay.NAME)) {
            return "zfb";
        }
        if (platform.getName().equals(Alipay.NAME)) {
            return "zfbzone";
        }
        return "";
    }

    private Dialog dialog;

    // 弹下载提示框
    private void showDialog(String client, final String downloadUrl, boolean isInstall) {
        String btnShare;
        String message;
        if (!isInstall) {
            message = "亲，您尚未安装" + client + "，请先安装";
            btnShare = "下载";
        } else {
            message = "亲，您安装的" + client + "版本较低，请先升级，谢谢。";
            btnShare = "升级";
        }

        dialog = AlertUtils.getInstance().showTwoBtnDialog(context, message, btnShare, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(downloadUrl));
                context.startActivity(intent);
            }
        }, "取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        cancel();
    }

    private void cancel() {
        if (isLand) {
            popDismiss();
        }
    }

    public void popDismiss() {
        if (pop != null) {
            pop.dismiss();
        }
    }

    /**
     * ****************************************** 此处为分享时自动登陆 start**********************************
     */
    private void login(Platform platform) {
        String sns = translateUTF8(getSnsName(platform));
        String uid = translateUTF8(platform.getDb().getUserId());
//        String url = String.format(DataInterface.CHECK_OTHER_BIND_URL, sns, uid);
        String userName = translateUTF8(getRegisterName());
        checkBind(sns, uid, userName, platform);
    }

    /**
     * 用于用户注册时保证用户名唯一
     */
    private String getRegisterName() {
        return "ifeng" + (System.currentTimeMillis() + "").substring(6);
    }

    private String translateUTF8(String string) {
        try {
            return URLEncoderUtils.encodeInUTF8(string);
        } catch (UnsupportedEncodingException e) {
            logger.error("translateUTF8 error ! {}", e.toString());
            e.printStackTrace();
        }
        return null;
    }

    private void checkBind(final String sns, final String uid, final String userName, final Platform platform) {
        LoginDao.requestCheckOtherBind(sns, uid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int state = getStateCode(response);
                        switch (state) {
                            case SUCCESS_STATE: // 绑定成功进行登录操作
                                otherLogin(sns, uid, platform);
                                break;
                            case FAILED_STATE: // 绑定操作失败，进行第三方注册
                                register(sns, uid, userName, platform);
                                break;
                        }
                    }
                },
                new LoginCheckBindErrorListener());
    }

    /**
     * 获取状态码
     */
    private int getStateCode(String jsonString) {
        int code = 2; // 不能用默认0，因为0,1，-1，都为状态码。
        try {
            return JSON.parseObject(jsonString).getInteger("code");
        } catch (Exception e) {
            logger.error(e.toString(), e);
            return code;
        }
    }

    /**
     * 第三方登录
     */
    private void otherLogin(final String sns, String uid, final Platform platform) {
        LoginDao.requestOtherLogin(sns, uid, PhoneConfig.userKey, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int state = getStateCode(response);
                switch (state) {
                    case SUCCESS_STATE:  // 登录成功
                        PlatformDb db = platform.getDb();
                        int vipStatus;
                        String vipExp = "";
                        String sign = "";
                        JSONObject object = JSONObject.parseObject(response);
                        vipStatus = object.getIntValue("VIPStatus");
                        vipExp = object.getString("VIPEXP");
                        sign = object.getString("sign");
                        User user = new User(db.getUserName(), db.getUserIcon(), getLoginToken(response), context,
                                getUid(response), getSnsName(platform), getIfengUserName(response), getUserNameStatus(response), vipStatus,
                                vipExp, getDataContent(response, "timestamp"), sign);
//                                db.getUserId(), getSnsName(platform), getIfengUserName(response), getUserNameStatus(response));
                        user.storeUserInfo();
                        Intent loginBroad = new Intent(IntentKey.LOGINBROADCAST);
                        loginBroad.putExtra(IntentKey.LOGINSTATE, IntentKey.LOGINING);
                        context.sendBroadcast(loginBroad);
                        break;
                    case FAILED_STATE:   // 登录失败
                        ToastUtils.getInstance().showShortToast(R.string.login_failed);
                        break;
                }
            }
        }, new OtherLoginErrorListener());
    }

    /**
     * 根据平台获取他相应的接口中的平台字段
     */
    private String getSnsName(Platform platform) {
        if (platform.getName().equals("SinaWeibo")) {
            return "sina";
        }
        // 和测试商量后注掉，取消Qzone的登录
        // 修复 http://redmine.staff.ifeng.com/issues/11273
        // if (platform.getName().equals("QZone")) {
        //     return "qzone";
        // }
        return null;
    }

    private String getLoginToken(String jsonString) {
        return getDataContent(jsonString, "token");
    }

    private String getUid(String jsonString) {
        return getDataContent(jsonString, "guid");
    }

    private String getDataContent(String jsonString, String key) {
        try {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            JSONObject jsonObject2 = jsonObject.getJSONObject("data");
            return jsonObject2.getString(key);
        } catch (Exception e) {
            logger.error("getLoginToken error! {}", e.toString());
            return null;
        }
    }

    private String getIfengUserName(String jsonString) {
        return getDataContent(jsonString, "username");
    }

    private String getUserNameStatus(String jsonString) {
        try {
            return JSON.parseObject(jsonString).getString("realNameStatus");
        } catch (Exception e) {
            logger.error(e.toString(), e);
            return null;
        }
    }

    private String getResult(String jsonString, String key) {
        try {
            return JSON.parseObject(jsonString).getString(key);
        } catch (Exception e) {
            logger.error("getResult error !{}", e.toString());
            return null;
        }
    }

    private void register(final String sns, final String uid, final String userName, final Platform platform) {
        LoginDao.requestOtherRegister(uid, sns, userName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int state = getStateCode(response);
                switch (state) {
                    case SUCCESS_STATE:
                        otherLogin(sns, uid, platform);
                        break;
                    case FAILED_STATE:
                        ToastUtils.getInstance().showShortToast(getResult(response, "message"));
                        break;
                }
            }
        }, new OtherRegisterErrorListener());
    }

    /**************************************************此处为分享时自动登陆 end******************************************/


    /**
     * FM单条分享地址
     * aid 单期节目ID	必填
     * pf	平台（iPhone，android）	必填
     * tvid	直播电台ID	可选
     * sid	专题ID	可选
     * s	分享目的平台（qzone weixin sina）除了这三个制定外 其余的s=FM	必填
     * v	客户端版本号 统一用6.4.3	必填
     * pid	所属专辑ID 	必填
     * <p/>
     * demo  http://diantai.ifeng.com/play.php?aid=1464390&s=qzone&pf=android&v=6.4.3&pid=1041
     */
    // TODO FM分享
    public String getFMSingleShareUrl(String aid, String programId) {

        if (aid != null && !aid.trim().equals("")) {
            //"AudioSharePlat"在后面会替换成相应的平台字符串，在这里用户还没有点击分享因此此时还不知道用户要分享到哪个平台,需要在后面替换
            String shareUrl = "http://diantai.ifeng.com/play.php?aid=" + aid + "&s=AudioSharePlat" + "&pf=android&v=6.4.3&pid=" + programId;
            if (!TextUtils.isEmpty(shareUrl)) {
                hashMap.put(aid, shareUrl);
            }
            return shareUrl;
        }
        return null;
    }

    /**
     * 用info取服务器上的视频地址
     */
    public void getShareUrl(String shareUrl) {
        if (shareUrl != null && !hashMap.containsKey(shareUrl)) {
            hashMap.put(shareUrl, shareUrl);
        }
    }

    /**
     * 新浪微博分享只要text字段
     * text 分享的文本
     */
    private void toSinaWeibo() {
        shareParamsTemp = new HashMap<String, Object>();
        shareParamsTemp.put("text", getCoustomText());
        shareParamsTemp.put("imageUrl", getImageUrl());
        // text中已包含url，所以设为空，不然微博里会显示2个网页链接
        shareParamsTemp.put("url", "");
    }

    /**
     * QQ分享
     * title  分享标题
     * tilteUrl 分享标题url
     * text  分享的文本
     * imageUrl 分享的网络图片
     */
    private void toQQ() {
        if (OneKeyShareContainer.oneKeyShare != null && OneKeyShareContainer.oneKeyShare.notifyShareCallback != null) {
            OneKeyShareContainer.oneKeyShare.notifyShareCallback.notifyPlayerPauseForShareWebQQ(true);
        }
        shareParamsTemp = new HashMap<String, Object>();
        if (isLive) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_TV_LIVE + " " + getTitle());
        } else if (isVRLive) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_LIVE + " " + getTitle());
        } else if (isVRVideo) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_VIDEO + " " + getTitle());
        } else {
            shareParamsTemp.put("title", getTitle());
        }
        shareParamsTemp.put("titleUrl", getTitleUrl());

        shareParamsTemp.put("url", getUrl());
        shareParamsTemp.put("imageUrl", getImageUrl());
        shareParamsTemp.put("text", "　");
    }

    /**
     * QQ空间分享
     * QQ空间支持分享文字和图文 参数说明 title：最多200个字符 text：最多600个字符
     * QQ空间分享时一定要携带title、text、titleUrl、site、siteUrl
     */
    private void toQzone() {
        shareParamsTemp = new HashMap<String, Object>();
        if (isLive) {
            shareParamsTemp.put("text", " ");
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_TV_LIVE + " " + getTitle());
        } else if (isVRLive) {
            shareParamsTemp.put("text", " ");
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_LIVE + " " + getTitle());
        } else if (isVRVideo) {
            shareParamsTemp.put("text", " ");
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_VIDEO + " " + getTitle());
        } else {
            shareParamsTemp.put("text", " ");
            shareParamsTemp.put("title", getTitle());
        }
        shareParamsTemp.put("titleUrl", getTitleUrl());
        shareParamsTemp.put("imageUrl", getImageUrl());
        shareParamsTemp.put("site", context.getString(R.string.app_name));
        shareParamsTemp.put("siteUrl", context.getString(R.string.app_site_url));
        logger.debug("--- in OneKeyShare text is {} , title is {}, titleUrl is {} , imageUrl is {} , site is {} , siteUrl is {}  , shareParamsMap.containsKey(live) is {}", shareParamsMap.get("text"), shareParamsMap.get("title"), shareParamsMap.get("titleUrl"), shareParamsMap.get("imageUrl"), context.getString(R.string.app_name), context.getString(R.string.app_site_url), shareParamsMap.containsKey("live"));
    }

    /**
     * 支付宝朋友分享
     */
    private void toAlipay() {
        shareParamsTemp = new HashMap<>();
        if (isLive) {
            //shareParamsTemp.put("text", ShareConstants.SHARE_TITLE_HEAD_LIVE + " " + getText());
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_TV_LIVE + " " + getTitle());
        } else if (isVRLive) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_LIVE + " " + getTitle());
        } else if (isVRVideo) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_VIDEO + " " + getTitle());
        } else {
//            shareParamsTemp.put("text", getText());
            shareParamsTemp.put("title", getTitle());
        }
        shareParamsTemp.put("imageUrl", getImageUrl());
        shareParamsTemp.put("url", getUrl());
        shareParamsTemp.put("shareType", Platform.SHARE_WEBPAGE);
        logger.debug("---- oneKeyShare toAlipay title is {} , text is {} , imageUrl is {} , url is  {} , ", shareParamsMap.get("title"), shareParamsMap.get("text"), shareParamsMap.get("imageUrl"), shareParamsMap.get("url"));
    }

    /**
     * 微信好友分享
     * title  分享的标题
     * url    分享的视频地址
     * text   分享的文本
     * imageUrl  分享的网络图片
     */
    private void toWechat() {
        shareParamsTemp = new HashMap<String, Object>();
        if (isLive) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_TV_LIVE + " " + getTitle());
        } else if (isVRLive) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_LIVE + " " + getTitle());
        } else if (isVRVideo) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_VIDEO + " " + getTitle());
        } else {
            shareParamsTemp.put("title", getTitle());
        }
        shareParamsTemp.put("url", getUrl());
        shareParamsTemp.put("imageUrl", getImageUrl());
        shareParamsTemp.put("shareType", getShareType());
    }

    /**
     * 微信朋友圈
     * title  分享的标题
     * url    分享的视频地址
     * text   分享的文本
     * imageUrl 分享的网路图片
     */
    private void toWechatMoments() {
        shareParamsTemp = new HashMap<String, Object>();
        if (isLive) {
//            shareParamsTemp.put("text", ShareConstants.SHARE_TITLE_HEAD_LIVE + " " + getText());
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_TV_LIVE + " " + getTitle());
        } else if (isVRLive) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_LIVE + " " + getTitle());
        } else if (isVRVideo) {
            shareParamsTemp.put("title", ShareConstants.SHARE_TITLE_HEAD_VR_VIDEO + " " + getTitle());
        } else {
//            shareParamsTemp.put("text", getText());
            shareParamsTemp.put("title", getTitle());
        }
        shareParamsTemp.put("url", getUrl());
        shareParamsTemp.put("imageUrl", getImageUrl());
        shareParamsTemp.put("shareType", getShareType());
    }

    private static class LoginCheckBindErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error("LoginCheckBindErrorListener error {}", error.toString());
            }
        }
    }

    private static class OtherLoginErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error("OtherLoginErrorListener error ! {}", error.toString());
            }
        }
    }

    private static class OtherRegisterErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error("OtherRegisterErrorListener error ! {}", error.toString());
            }
        }
    }

    private static class GetShareUrlErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error != null) {
                logger.error("GetShareUrlErrorListener error {}", error.toString());
            }
        }
    }

    /**
     * 发送分享统计
     *
     * @param id    视频的id
     * @param echid 编辑id
     * @param chid  媒资id
     * @param wmid  自媒体id
     * @param share 分享的平台名称
     * @param yn    分享成功yes/no
     * @param page  所在页面id
     */
    private void sendShareStatistics(String id, String echid, String chid, String wmid, String share, boolean yn, String page) {
        ShareRecord shareRecord = new ShareRecord(id, echid, chid, wmid, share, yn, page);
        CustomerStatistics.sendShareRecord(shareRecord);
    }

    private String id;//视频id
    private String echid;//编辑id
    private String chid;//媒资id
    private String wmid;//自媒体id
    private String page;//所在页面id

    /**
     * 初始化分享统计数据
     */
    public void initShareStatisticsData(String id, String echid, String chid, String wmid, String page) {
        this.id = id;
        this.echid = echid;
        this.chid = chid;
        this.wmid = wmid;
        this.page = page;
    }

    /**
     * 获取分享平台id
     */
    private String getPlatformId() {
        return TextUtils.isEmpty(CustomerStatistics.openId) ? "" : CustomerStatistics.openId;
    }

    private String type = "";
    private String keyword = "";
    private String title = "";

    /**
     * 初始化分享智能统计数据
     *
     * @param type    数据类型
     * @param keyword 频道名称、自媒体名称
     * @param title   视频标题
     */
    public void initSmartShareData(String type, String keyword, String title) {
        this.type = type;
        this.keyword = keyword;
        this.title = title;
    }
}
