package com.ifeng.newvideo.coustomshare;

import cn.sharesdk.framework.Platform;

/**
 * 分享到频道接口
 * Created by xt on 2015/1/7.
 */
public interface SharePlatform {
    /**
     * 分享到平台
     *
     * @param platform       平台
     * @param isShowEditPage 是否有编辑页面
     */
    void shareToPlatform(Platform platform, boolean isShowEditPage);
}
