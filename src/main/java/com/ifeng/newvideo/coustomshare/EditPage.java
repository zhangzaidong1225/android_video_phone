package com.ifeng.newvideo.coustomshare;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.ShareConstants;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.StatisticsConstants;
import com.ifeng.newvideo.statistics.domains.ShareRecord;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 分享编辑页面，参照shareSDK中的EditPage.java
 * Created by xt on 2015/1/7.
 */
public class EditPage extends BaseFragmentActivity implements View.OnClickListener, TextWatcher, PlatformActionListener {
    private static final Logger logger = LoggerFactory.getLogger(EditPage.class);

    // 分享内容最大字数
    private static final int MAX_TEXT_COUNT = 140;
    // share content editor
    private EditText etContent;
    // Words counter
    private TextView tvCounter;
    private View leftBtn, rightBtn;
    private LinearLayout sinaBtn;
    // 编辑页编辑文字状态
    private int editState;
    private static final int TOO_MORE = 1;
    private static final int RIGHT = 0;
    private static final int EMPTY = 2;

    private final Handler handler = new Handler();
    // 平台图片上的遮盖view
    private View coverView;
    private View sinaView;
    private Map<String, Platform> platforms;
    private Platform sinaPlatform;
    //分享统计相关数据
    private String id = "";//视频id
    private String echid = "";//编辑id
    private String chid = "";//媒资id
    private String wmid = "";//自媒体id

    boolean isLive;
    boolean isVRLive;
    boolean isVRVideo;
    boolean isAD;
    boolean isSignIn;
    boolean isFM;
    private static final String SPACE = " ";
    // 去掉t、p
    String tempUrl;

    public void toLand() {
        if (Build.VERSION.SDK_INT >= 9) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_editpage_activity);
        IfengApplication.getInstance().setEditShow(false);
        if (OneKeyShareContainer.oneKeyShare != null && OneKeyShareContainer.oneKeyShare.notifyShareCallback != null) {
            OneKeyShareContainer.oneKeyShare.notifyShareCallback.notifyShare(EditPage.this, true);
        }
        initViews();
        initListener();
        initData();

    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.share_below_in, R.anim.share_top_out);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void initViews() {
        leftBtn = findViewById(R.id.left_btn);
        rightBtn = findViewById(R.id.right_btn);
        etContent = (EditText) findViewById(R.id.shareContent);
        tvCounter = (TextView) findViewById(R.id.shareCount);
        sinaBtn = (LinearLayout) findViewById(R.id.sinaweibo_btn);
        sinaView = findViewById(R.id.sina_view);
    }

    private void initListener() {
        leftBtn.setOnClickListener(this);
        rightBtn.setOnClickListener(this);
        sinaBtn.setOnClickListener(this);
        etContent.addTextChangedListener(this);
    }

    private String shareUrl = "";

    private void initData() {
        onTextChanged(etContent.getText(), 0, etContent.length(), 0);
        platforms = new HashMap<>();
        sinaPlatform = ShareSDK.getPlatform(SinaWeibo.NAME);
        if (sinaPlatform != null && sinaPlatform.isAuthValid()) {
            sinaView.setVisibility(View.INVISIBLE);
            platforms.put(sinaPlatform.getName(), sinaPlatform);
        }
        Intent intent = getIntent();
        //分享统计相关
        id = intent.getStringExtra(ShareConstants.SHARE_ID);
        echid = intent.getStringExtra(ShareConstants.SHARE_ECHID);
        chid = intent.getStringExtra(ShareConstants.SHARE_CHID);
        wmid = intent.getStringExtra(ShareConstants.SHARE_WMID);
        //分享相关
        String text = intent.getStringExtra(ShareConstants.SHARE_TEXT);
        shareUrl = intent.getStringExtra(ShareConstants.SHARE_URL);

        isLive = intent.getBooleanExtra(ShareConstants.SHARE_LIVE, false);
        isVRLive = intent.getBooleanExtra(ShareConstants.SHARE_VR_LIVE, false);
        isVRVideo = intent.getBooleanExtra(ShareConstants.SHARE_VR_VIDEO, false);
        isAD = intent.getBooleanExtra(ShareConstants.SHARE_AD, false);
        isSignIn = intent.getBooleanExtra(ShareConstants.SHARE_SIGN, false);
        isFM = intent.getBooleanExtra(ShareConstants.SHARE_FM, false);

        tempUrl = shareUrl;
        tempUrl = TextUtils.isEmpty(tempUrl) ? "" : tempUrl;
        if (tempUrl.contains("?")) {//url中t、p参数不展示
            int index = tempUrl.indexOf("?");
            tempUrl = tempUrl.substring(0, index);
        }
        logger.debug("shareUrl temp  {}", tempUrl);

        if (isLive) {//【直播】 + 视频标题 + 视频url + (分享自@凤凰视频客户端)
            etContent.setText(ShareConstants.SHARE_TITLE_HEAD_TV_LIVE + SPACE + text + SPACE + tempUrl + ShareConstants.SHARE_TITLE_END);
        } else if (isVRLive) {//【VR直播】 + 视频标题 + 视频url + (分享自@凤凰视频客户端)
            etContent.setText(ShareConstants.SHARE_TITLE_HEAD_VR_LIVE + SPACE + text + SPACE + tempUrl + ShareConstants.SHARE_TITLE_END);
        } else if (isVRVideo) {//【VR】 + 视频标题 + 视频url + (分享自@凤凰视频客户端)
            etContent.setText(ShareConstants.SHARE_TITLE_HEAD_VR_VIDEO + SPACE + text + SPACE + tempUrl + ShareConstants.SHARE_TITLE_END);
        } else if (isAD) {
            etContent.setText(text + SPACE + tempUrl + ShareConstants.SHARE_TITLE_END);
        } else if (isSignIn) {
            etContent.setText("【" + text + "】" + ShareConstants.SHARE_TITLE_END);
        } else if (isFM) {
            //电台分享链接不截取,显示完整
            etContent.setText(text + SPACE + shareUrl + ShareConstants.SHARE_TITLE_END);
        } else {//视频标题 + 视频url + (分享自@凤凰视频客户端)
            etContent.setText(text + SPACE + tempUrl + ShareConstants.SHARE_TITLE_END);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.left_btn:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.SINA_SHARE);
                onBackPressed();
                break;
            case R.id.right_btn:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SINA_SEND, PageIdConstants.SINA_SHARE);
                publishShare();
                break;
            case R.id.sinaweibo_btn:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SINA_PLATFORM, PageIdConstants.SINA_SHARE);
                authorize(sinaPlatform, sinaView);
                break;
        }
    }

    private void publishShare() {
        if (editState != RIGHT || !NetUtils.isNetAvailable(this)) {
            alertShareDialog(NetUtils.isNetAvailable(this));
        } else {
            onClickShare();
        }
    }

    private void onClickShare() {
        boolean selected = false;
        //String text = etContent.getText().toString();
        if (sinaView.getVisibility() == View.INVISIBLE) {
            platforms.clear();
            if (sinaView.getVisibility() == View.INVISIBLE) {
                platforms.put(sinaPlatform.getName(), sinaPlatform);
            }

            selected = true;
        }
        if (selected) {
            if (OneKeyShareContainer.oneKeyShare != null) {
                // 替换为带t/p的分享地址
                shareUrl = etContent.getText().toString().replaceAll(tempUrl, shareUrl);
                logger.debug("shareUrl final  {}", shareUrl);
                OneKeyShareContainer.oneKeyShare.setMapPlatform(platforms, shareUrl);
                OneKeyShareContainer.oneKeyShare = null;
            }
            finish();
        } else {
            ToastUtils.getInstance().showShortToast(R.string.no_share_platform);
        }
    }

    private void alertShareDialog(boolean isNet) {
        if (editState == EMPTY) {
            AlertUtils.getInstance().showOneBtnDialog(this, "亲，请输入内容后再分享", this.getString(R.string.common_i_know), null);
        } else if (editState == TOO_MORE) {
            AlertUtils.getInstance().showOneBtnDialog(this, "亲，分享内容不能超过140字", this.getString(R.string.common_i_know), null);
        } else if (!isNet) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
        }
    }

    @Override
    public void onComplete(Platform platform, final int action, HashMap<String, Object> stringObjectHashMap) {
        if (action == Platform.ACTION_USER_INFOR) {//授权action
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ToastUtils.getInstance().showShortToast("授权成功");
                    if (coverView != null) {
                        coverView.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }

    @Override
    public void onError(Platform platform, final int action, Throwable throwable) {
        if (action == Platform.ACTION_USER_INFOR) {
            handler.post(new AuthErrorRunnable());
        }
    }

    @Override
    public void onCancel(Platform platform, final int action) {
        if (action == Platform.ACTION_USER_INFOR) {
            handler.post(new AuthCancelRunnable());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        int remain = MAX_TEXT_COUNT - calculateStringCounts(etContent.getText().toString());
        tvCounter.setText(String.valueOf(remain) + "/" + MAX_TEXT_COUNT);
        tvCounter.setTextColor(remain > 0 ? 0xffcfcfcf : 0xffff0000);
        if (remain == MAX_TEXT_COUNT) {
            editState = EMPTY;
        } else if (remain < 0) {
            editState = TOO_MORE;
            tvCounter.setText(String.format(getString(R.string.more_than), String.valueOf(-remain)));
        } else {
            editState = RIGHT;
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    public int calculateStringCounts(String str) {
        char[] tmpChar = str.toCharArray();
        float curCount = 0;
        String s1;
        for (char c : tmpChar) {
            s1 = String.valueOf(c);
            if (s1.getBytes().length == 1) {
                curCount += 0.5f;
            } else {
                ++curCount;
            }
        }
        return (int) curCount;
    }

    /**
     * 判断是进行登录进行认证
     */
    private void authorize(Platform plat, View v) {
        if (!NetUtils.isNetAvailable(this)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (plat.isAuthValid() && v.getVisibility() == View.VISIBLE) {
            v.setVisibility(View.INVISIBLE);
            return;
        } else if (plat.isAuthValid() && v.getVisibility() == View.INVISIBLE) {
            v.setVisibility(View.VISIBLE);
            return;
        }
        coverView = v;
        plat.setPlatformActionListener(this);
        changeNameCustomerStatistics(plat.getName());
        plat.SSOSetting(false);
        plat.showUser(null);
    }

    @Override
    public void finish() {
        super.finish();
//        overridePendingTransition(R.anim.common_push_bottom_in, R.anim.common_below_out);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        return super.onKeyUp(keyCode, event);
    }

    private void changeNameCustomerStatistics(String name) {
        if (name.equals("WechatMoments")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_WEIXIN;
        } else if (name.equals("Wechat")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_WEIXIN;
        } else if (name.equals("QQ")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_QQ;
        } else if (name.equals("SinaWeibo")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_SINAWEIBO;
        } else if (name.equals("QZone")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_QZONE;
        } else if (name.equals("Alipay")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_ALIPAY;
        }
        CustomerStatistics.inType = StatisticsConstants.APPSTART_TYPE_FROM_OUTSIDE;
    }

    private static class AuthErrorRunnable implements Runnable {
        @Override
        public void run() {
            ToastUtils.getInstance().showShortToast("授权出现错误，请重试，谢谢");
        }
    }

    private static class AuthCancelRunnable implements Runnable {
        @Override
        public void run() {
            ToastUtils.getInstance().showShortToast("授权取消");
        }
    }


    @Override
    public void onBackPressed() {
        sendShareStatistics(id, echid, chid, wmid, StatisticsConstants.APPSTART_TYPE_FROM_SINAWEIBO, false, PageIdConstants.SINA_SHARE);
        finish();
        ToastUtils.getInstance().showShortToast(R.string.share_cancel_share);
        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare = null;
        }
    }

    /**
     * 发送分享统计
     *
     * @param id    视频的id
     * @param echid 编辑id
     * @param chid  媒资id
     * @param wmid  自媒体id
     * @param share 分享的平台名称
     * @param yn    分享成功yes/no
     * @param page  所在页面id
     */
    private void sendShareStatistics(String id, String echid, String chid, String wmid, String share, boolean yn, String page) {
        ShareRecord shareRecord = new ShareRecord(id, echid, chid, wmid, share, yn, page);
        CustomerStatistics.sendShareRecord(shareRecord);
    }
}
