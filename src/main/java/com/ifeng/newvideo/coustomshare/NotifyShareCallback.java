package com.ifeng.newvideo.coustomshare;


/**
 * 接口：编辑界面展示后，会通知相关界面(如播放基页)进行逻辑处理
 */
public interface NotifyShareCallback {

    /**
     * 通知相关界面(如播放基页)进行逻辑处理
     *
     * @param page 编辑页面
     * @param show 编辑页面是否显示
     */
    void notifyShare(EditPage page, boolean show);
    /**
     * 如果分享到webQQ页，通知播放器暂停(webQQ不会走播放页面onPause方法)
     *
     * @param isWebQQ 是否是分享到webQQ
     */
    void notifyPlayerPauseForShareWebQQ(boolean isWebQQ);

    /**
     * 通知播放器页面竖屏分享弹窗是否展示
     * 分享弹窗展示：播放器不暂停播放
     * @param isShow
     */
    void notifyShareWindowIsShow(boolean isShow);
}
