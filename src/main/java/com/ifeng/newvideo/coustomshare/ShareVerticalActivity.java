package com.ifeng.newvideo.coustomshare;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 竖屏分享页面
 * Created by xt on 2015/1/6.
 */
public class ShareVerticalActivity extends BaseFragmentActivity implements View.OnClickListener, SharePlatform {

    private static final Logger logger = LoggerFactory.getLogger(ShareVerticalActivity.class);


    private LinearLayout sinaweiboBtn;
    private LinearLayout wechatBtn;
    private LinearLayout wechatmomentsBtn;
    private LinearLayout QQBtn;
    private LinearLayout QzonBtn;
    private LinearLayout aliPayBtn;
    private RelativeLayout shareParent;
    private Button btnCancel;
    private Platform platform;
    private static ViewGroup.LayoutParams imageParams;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_vertical);
        setAnimFlag(ANIM_FLAG_BOTTOM_TOP);
        initViews();
        initListener();
    }

    private void initViews() {
        sinaweiboBtn = (LinearLayout) findViewById(R.id.sinaweibo_btn);
        wechatBtn = (LinearLayout) findViewById(R.id.wecat_btn);
        QQBtn = (LinearLayout) findViewById(R.id.qq_btn);
        wechatmomentsBtn = (LinearLayout) findViewById(R.id.wechatmoments_btn);
        QzonBtn = (LinearLayout) findViewById(R.id.qqzone_btn);
        aliPayBtn = (LinearLayout) findViewById(R.id.alipay_btn);
        btnCancel = (Button) findViewById(R.id.cancel_Btn);
        shareParent = (RelativeLayout) findViewById(R.id.share_parent);
        // shareParent.startAnimation(AnimationUtils.loadAnimation(this,R.anim.common_popup_enter));
        LinearLayout shareContainer = (LinearLayout) findViewById(R.id.share_container);
        shareContainer.setOnTouchListener(new ShareContainerOnTouchListener());

        ImageView sinaImage = (ImageView) findViewById(R.id.image_sina);
        ImageView wechatImage = (ImageView) findViewById(R.id.image_wecat);
        ImageView qqImage = (ImageView) findViewById(R.id.image_qq);
        ImageView wechatMomentsImage = (ImageView) findViewById(R.id.image_wecatMoments);
        ImageView qqzoneImage = (ImageView) findViewById(R.id.image_qqzone);
        ImageView alipayImage = (ImageView) findViewById(R.id.image_alipay);

        if (SharePlatformUtils.hasWebChat()) {
            wechatImage.setImageResource(R.drawable.btn_share_white_wechat);
            wechatMomentsImage.setImageResource(R.drawable.btn_share_pyq);
        } else {
            wechatImage.setImageResource(R.drawable.btn_share_white_wechat_none);
            wechatMomentsImage.setImageResource(R.drawable.btn_share_white_pyq_none);
        }

        if (SharePlatformUtils.hasQQ()) {
            qqImage.setImageResource(R.drawable.btn_share_white_qq);
            qqzoneImage.setImageResource(R.drawable.btn_share_qzone);
        } else {
            qqImage.setImageResource(R.drawable.btn_share_white_qq_none);
            qqzoneImage.setImageResource(R.drawable.btn_share_qzone_none);
        }

        if (SharePlatformUtils.hasAlipay()) {
            alipayImage.setImageResource(R.drawable.btn_share_alipay);
        } else {
            alipayImage.setImageResource(R.drawable.btn_share_alipay_none);
        }

        ImageView null1 = (ImageView) findViewById(R.id.null1);
        ImageView null2 = (ImageView) findViewById(R.id.null2);
        if (imageParams == null) {
            imageParams = sinaImage.getLayoutParams();
            imageParams.width = (DisplayUtils.getWindowWidth() - DisplayUtils.convertDipToPixel(150)) / 4;
            imageParams.height = imageParams.width;
        }
        sinaImage.setLayoutParams(imageParams);
        wechatImage.setLayoutParams(imageParams);
        qqImage.setLayoutParams(imageParams);
        wechatMomentsImage.setLayoutParams(imageParams);
        qqzoneImage.setLayoutParams(imageParams);
        alipayImage.setLayoutParams(imageParams);
        null1.setLayoutParams(imageParams);
        null2.setLayoutParams(imageParams);
    }

    private void initListener() {
        sinaweiboBtn.setOnClickListener(this);
        wechatBtn.setOnClickListener(this);
        QQBtn.setOnClickListener(this);
        QzonBtn.setOnClickListener(this);
        wechatmomentsBtn.setOnClickListener(this);
        aliPayBtn.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        shareParent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sinaweibo_btn:
                platform = ShareSDK.getPlatform(SinaWeibo.NAME);
                shareToPlatform(platform, true);
                break;
            case R.id.wecat_btn:
                platform = ShareSDK.getPlatform(Wechat.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.qq_btn:
                platform = ShareSDK.getPlatform(QQ.NAME);
                shareToPlatform(platform, false);
                break;

            case R.id.wechatmoments_btn:
                platform = ShareSDK.getPlatform(WechatMoments.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.qqzone_btn:
                platform = ShareSDK.getPlatform(QZone.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.alipay_btn:
                platform = ShareSDK.getPlatform(Alipay.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.cancel_Btn:
            case R.id.share_parent:
                finish();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        finish();
    }

    @Override
    public void shareToPlatform(Platform platform, boolean isShowEditPage) {
        if (!NetUtils.isNetAvailable(this)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare.shareToPlatform(platform, isShowEditPage);
            if (!isShowEditPage && !(platform.getName().equals("QQ"))) {
                OneKeyShareContainer.oneKeyShare = null;
                finish();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        IfengApplication.isShareVertialActivityFinish = true;
    }

    private static class ShareContainerOnTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }


}
