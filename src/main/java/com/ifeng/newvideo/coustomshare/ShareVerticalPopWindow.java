package com.ifeng.newvideo.coustomshare;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;

import static com.ifeng.newvideo.coustomshare.SharePlatformUtils.hasAlipay;
import static com.ifeng.newvideo.coustomshare.SharePlatformUtils.hasQQ;
import static com.ifeng.newvideo.coustomshare.SharePlatformUtils.hasSinaWeibo;
import static com.ifeng.newvideo.coustomshare.SharePlatformUtils.hasWebChat;

/**
 * 分享横屏页面
 * Created by xt on 2015/1/7.
 */
public class ShareVerticalPopWindow extends LinearLayout implements View.OnClickListener, SharePlatform {

    private PopupWindow pop;
    private Platform platform;
    private OneKeyShare oneKeyShare;
    private Context context;

    private LinearLayout sinaweiboBtn;
    private LinearLayout wechatBtn;
    private LinearLayout wechatmomentsBtn;
    private LinearLayout QQBtn;
    private LinearLayout QzonBtn;
    private LinearLayout aliPayBtn;
    private RelativeLayout shareParent;
    private LinearLayout shareContainer;
    private Button btnCancel;
    private static ViewGroup.LayoutParams imageParams;

    private TextView sinaweibo;
    private TextView wechat;
    private TextView pyq;
    private TextView qq;
    private TextView qqZone;
    private TextView aLiPay;

    public ShareVerticalPopWindow(Context context) {
        super(context);
        this.context = context;
        initView();
        initListener();
    }

    public ShareVerticalPopWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPop(PopupWindow pop) {
        this.pop = pop;
    }

    public void setOneKeyShare(OneKeyShare oneKeyShare) {
        this.oneKeyShare = oneKeyShare;
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.share_vertical, null);
        sinaweiboBtn = (LinearLayout) view.findViewById(R.id.sinaweibo_btn);
        wechatBtn = (LinearLayout) view.findViewById(R.id.wecat_btn);
        QQBtn = (LinearLayout) view.findViewById(R.id.qq_btn);
        wechatmomentsBtn = (LinearLayout) view.findViewById(R.id.wechatmoments_btn);
        QzonBtn = (LinearLayout) view.findViewById(R.id.qqzone_btn);
        aliPayBtn = (LinearLayout) view.findViewById(R.id.alipay_btn);
        btnCancel = (Button) view.findViewById(R.id.cancel_Btn);
        shareParent = (RelativeLayout) view.findViewById(R.id.share_parent);
        shareContainer = (LinearLayout) view.findViewById(R.id.share_container);
        shareContainer.startAnimation(AnimationUtils.loadAnimation(context, R.anim.common_popup_enter));

        ImageView sinaImage = (ImageView) view.findViewById(R.id.image_sina);
        ImageView wechatImage = (ImageView) view.findViewById(R.id.image_wecat);
        ImageView qqImage = (ImageView) view.findViewById(R.id.image_qq);
        ImageView wechatMomentsImage = (ImageView) view.findViewById(R.id.image_wecatMoments);
        ImageView qqzoneImage = (ImageView) view.findViewById(R.id.image_qqzone);
        ImageView alipayImage = (ImageView) view.findViewById(R.id.image_alipay);

        sinaweibo = (TextView) view.findViewById(R.id.share_tv_sinaweibo);
        wechat = (TextView) view.findViewById(R.id.share_tv_wechat);
        pyq = (TextView) view.findViewById(R.id.share_tv_pyq);
        qq = (TextView) view.findViewById(R.id.share_tv_qq);
        qqZone = (TextView) view.findViewById(R.id.share_tv_qq_zone);
        aLiPay = (TextView) view.findViewById(R.id.share_tv_alipay);

        if (hasWebChat()) {
            wechatImage.setImageResource(R.drawable.btn_share_white_wechat);
            wechatMomentsImage.setImageResource(R.drawable.btn_share_pyq);
        } else {
            wechatImage.setImageResource(R.drawable.btn_share_white_wechat_none);
            wechatMomentsImage.setImageResource(R.drawable.btn_share_white_pyq_none);
            wechat.setTextColor(Color.parseColor("#BBBBBB"));
            pyq.setTextColor(Color.parseColor("#BBBBBB"));
        }

        if (hasQQ()) {
            qqImage.setImageResource(R.drawable.btn_share_white_qq);
            qqzoneImage.setImageResource(R.drawable.btn_share_qzone);
        } else {
            qqImage.setImageResource(R.drawable.btn_share_white_qq_none);
            qqzoneImage.setImageResource(R.drawable.btn_share_qzone_none);
            qq.setTextColor(Color.parseColor("#BBBBBB"));
            qqZone.setTextColor(Color.parseColor("#BBBBBB"));

        }

        if (hasAlipay()) {
            alipayImage.setImageResource(R.drawable.btn_share_alipay);
        } else {
            alipayImage.setImageResource(R.drawable.btn_share_alipay_none);
            aLiPay.setTextColor(Color.parseColor("#BBBBBB"));
        }

        if (hasSinaWeibo()) {
            sinaImage.setImageResource(R.drawable.btn_share_white_weibo);
        } else {
            sinaImage.setImageResource(R.drawable.btn_share_white_weibo_none);
            sinaweibo.setTextColor(Color.parseColor("#BBBBBB"));
        }

        ImageView null1 = (ImageView) view.findViewById(R.id.null1);
        ImageView null2 = (ImageView) view.findViewById(R.id.null2);
        if (imageParams == null) {
            imageParams = sinaImage.getLayoutParams();
            imageParams.width = (DisplayUtils.getWindowWidth() - DisplayUtils.convertDipToPixel(150)) / 4;
            imageParams.height = imageParams.width;
        }
        sinaImage.setLayoutParams(imageParams);
        wechatImage.setLayoutParams(imageParams);
        qqImage.setLayoutParams(imageParams);
        wechatMomentsImage.setLayoutParams(imageParams);
        qqzoneImage.setLayoutParams(imageParams);
        alipayImage.setLayoutParams(imageParams);
        null1.setLayoutParams(imageParams);
        null2.setLayoutParams(imageParams);

        addView(view);
    }

    private void initListener() {
        sinaweiboBtn.setOnClickListener(this);
        wechatBtn.setOnClickListener(this);
        QQBtn.setOnClickListener(this);
        QzonBtn.setOnClickListener(this);
        wechatmomentsBtn.setOnClickListener(this);
        aliPayBtn.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        shareParent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sinaweibo_btn:
                platform = ShareSDK.getPlatform(SinaWeibo.NAME);
                shareToPlatform(platform, true);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_SINA, PageIdConstants.SHARE_V);
                break;
            case R.id.wecat_btn:
                platform = ShareSDK.getPlatform(Wechat.NAME);
                shareToPlatform(platform, false);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WX, PageIdConstants.SHARE_V);
                break;
            case R.id.qq_btn:
                platform = ShareSDK.getPlatform(QQ.NAME);
                shareToPlatform(platform, false);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQ, PageIdConstants.SHARE_V);
                break;
            case R.id.wechatmoments_btn:
                platform = ShareSDK.getPlatform(WechatMoments.NAME);
                shareToPlatform(platform, false);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WXF, PageIdConstants.SHARE_V);
                break;
            case R.id.qqzone_btn:
                platform = ShareSDK.getPlatform(QZone.NAME);
                shareToPlatform(platform, false);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQZONE, PageIdConstants.SHARE_V);
                break;
            case R.id.alipay_btn:
                platform = ShareSDK.getPlatform(Alipay.NAME);
                shareToPlatform(platform, false);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_ALIPAY, PageIdConstants.SHARE_V);
                break;
            case R.id.cancel_Btn:
            case R.id.share_parent:
                exitAnimation();
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_CANCEL, PageIdConstants.SHARE_V);
                break;
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        dismiss();
    }

    private void dismiss() {
        if (pop != null && pop.isShowing()) {
            IfengApplication.isShareVertialActivityFinish = true;
            pop.dismiss();
        }
    }

    private void exitAnimation() {
        if (pop != null && pop.isShowing()) {
            IfengApplication.isShareVertialActivityFinish = true;
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.common_popup_exit);
            shareContainer.startAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    pop.dismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
    }

    @Override
    public void shareToPlatform(Platform platform, boolean isShowEditPage) {
        dismiss();
        if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare.shareToPlatform(platform, isShowEditPage);
            if (!isShowEditPage && !(platform.getName().equals("QQ"))) {
                OneKeyShareContainer.oneKeyShare = null;
            }
        }
    }

}
