package com.ifeng.newvideo.coustomshare;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;

/**
 * 分享横屏页面
 * Created by xt on 2015/1/7.
 */
public class ShareHorizontalPopWindow extends LinearLayout implements View.OnClickListener, SharePlatform {

    private PopupWindow pop;
    private OneKeyShare oneKeyShare;
    private Platform platform;
    private Context context;

    private static final int SINAWEIBO_ID = 1;
    private static final int WECAHAT_ID = 2;
    private static final int QQ_ID = 3;
    private static final int QZONE_ID = 4;
    private static final int WECHATMOMENTS_ID = 5;
    private static final int ALIPAY_ID = 6;

    public ShareHorizontalPopWindow(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public ShareHorizontalPopWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPop(PopupWindow pop) {
        this.pop = pop;
    }

    public void setOneKeyShare(OneKeyShare oneKeyShare) {
        this.oneKeyShare = oneKeyShare;
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.share_horizontal, null);
        ImageView ivQQ = (ImageView) view.findViewById(R.id.iv_qq);
        ImageView ivSina = (ImageView) view.findViewById(R.id.iv_sina);
        ImageView ivWechat = (ImageView) view.findViewById(R.id.iv_wechat);
        ImageView ivWechatMoment = (ImageView) view.findViewById(R.id.iv_wechat_moment);
        ImageView ivQzone = (ImageView) view.findViewById(R.id.iv_qzone);
        ImageView ivAlipay = (ImageView) view.findViewById(R.id.iv_alipay);
        ivQQ.setOnClickListener(this);
        ivQQ.setId(QQ_ID);
        ivSina.setOnClickListener(this);
        ivSina.setId(SINAWEIBO_ID);
        ivWechat.setOnClickListener(this);
        ivWechat.setId(WECAHAT_ID);
        ivWechatMoment.setOnClickListener(this);
        ivWechatMoment.setId(WECHATMOMENTS_ID);
        ivQzone.setOnClickListener(this);
        ivQzone.setId(QZONE_ID);
        ivAlipay.setOnClickListener(this);
        ivAlipay.setId(ALIPAY_ID);
        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        addView(view);
    }

    @Override
    public void onClick(View v) {
        if (pop != null && pop.isShowing()) {
            pop.dismiss();
        }
        boolean isShowEditPage = false;
        switch (v.getId()) {
            case SINAWEIBO_ID:
                platform = ShareSDK.getPlatform(SinaWeibo.NAME);
                isShowEditPage = true;
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_SINA, PageIdConstants.SHARE_H);
                break;
            case WECAHAT_ID:
                platform = ShareSDK.getPlatform(Wechat.NAME);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WX, PageIdConstants.SHARE_H);
                break;
            case QQ_ID:
                platform = ShareSDK.getPlatform(QQ.NAME);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQ, PageIdConstants.SHARE_H);
                break;
            case WECHATMOMENTS_ID:
                platform = ShareSDK.getPlatform(WechatMoments.NAME);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WXF, PageIdConstants.SHARE_H);
                break;
            case QZONE_ID:
                platform = ShareSDK.getPlatform(QZone.NAME);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQZONE, PageIdConstants.SHARE_H);
                break;
            case ALIPAY_ID:
                platform = ShareSDK.getPlatform(Alipay.NAME);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_ALIPAY, PageIdConstants.SHARE_H);
                break;
        }
        shareToPlatform(platform, isShowEditPage);
    }

    @Override
    public void shareToPlatform(Platform platform, boolean isShowEditPage) {
        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare.shareToPlatform(platform, isShowEditPage);
            if (!isShowEditPage && !(platform.getName()).equals("QQ")) {
                OneKeyShareContainer.oneKeyShare = null;
            }
        }
    }
}
