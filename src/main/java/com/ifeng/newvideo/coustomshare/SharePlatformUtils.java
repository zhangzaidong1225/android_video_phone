package com.ifeng.newvideo.coustomshare;

import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * Created by android-dev on 2016/11/24 0024.
 */
public class SharePlatformUtils {

    public static boolean hasWebChat() {
        Wechat wechat = new Wechat();
        return wechat.isClientValid();
    }

    public static boolean hasQQ() {
        QQ qq = new QQ();
        return qq.isClientValid();
    }

    public static boolean hasAlipay() {
        Alipay alipay = new Alipay();
        return alipay.isClientValid();
    }

    public static boolean hasSinaWeibo() {
        SinaWeibo sinaWeibo = new SinaWeibo();
        return sinaWeibo.isClientValid();
    }
}
