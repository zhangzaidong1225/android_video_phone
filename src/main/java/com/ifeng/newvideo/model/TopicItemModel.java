package com.ifeng.newvideo.model;

import com.ifeng.video.dao.db.model.CMPPSubTopicModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cuihz on 2014/11/28.
 */
public class TopicItemModel {

    private static final Logger logger = LoggerFactory.getLogger(TopicItemModel.class);

    //list列表最后一条，用于隐藏灰条
    private boolean isLastItem;
    private int type;
    private String subTitle;
    private CMPPSubTopicModel.SubTopicList.TopicDetail topicDetail1;
    private CMPPSubTopicModel.SubTopicList.TopicDetail topicDetail2;
    private int index1 = -1;
    private int index2 = -1;

    public boolean isLastItem() {
        return isLastItem;
    }

    public void setLastItem(boolean isLastItem) {
        this.isLastItem = isLastItem;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public CMPPSubTopicModel.SubTopicList.TopicDetail getTopicDetail1() {
        return topicDetail1;
    }

    public void setTopicDetail1(CMPPSubTopicModel.SubTopicList.TopicDetail topicDetail1) {
        this.topicDetail1 = topicDetail1;
    }

    public CMPPSubTopicModel.SubTopicList.TopicDetail getTopicDetail2() {
        return topicDetail2;
    }

    public void setTopicDetail2(CMPPSubTopicModel.SubTopicList.TopicDetail topicDetail2) {
        this.topicDetail2 = topicDetail2;
    }

    public int getIndex1() {
        return index1;
    }

    public void setIndex1(int index1) {
        this.index1 = index1;
    }

    public int getIndex2() {
        return index2;
    }

    public void setIndex2(int index2) {
        this.index2 = index2;
    }
}
