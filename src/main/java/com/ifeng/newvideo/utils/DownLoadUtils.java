package com.ifeng.newvideo.utils;

import android.content.Context;
import android.content.Intent;
import com.ifeng.newvideo.appstore.StoreDownLoadService;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.video.core.utils.URLDecoderUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by android-dev on 2016/10/25.
 */
public class DownLoadUtils {

    private static final Logger logger = LoggerFactory.getLogger(DownLoadUtils.class);

    /**
     * @param adId                 广告id
     * @param downloadUrl          下载url
     * @param downloadStartUrl     下载开始上报的url
     * @param downloadCompletedUrl 下载完成上报的url
     */
    public static void download(Context context, String adId, String downloadUrl,
                                ArrayList<String> downloadStartUrl, ArrayList<String> downloadCompletedUrl) {
        try {
            logger.debug("WebViewDownLoadListener------> onDownloadStart url is {}", downloadUrl);
            Intent intent = new Intent(context, StoreDownLoadService.class);
            intent.putExtra(IntentKey.STORE_APP_URL, downloadUrl);
            intent.putExtra(ADActivity.KEY_AD_ID, adId);
            intent.putStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_COMPLETED_URL, downloadCompletedUrl);
            intent.putStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_START_URL, downloadStartUrl);
            intent.putExtra(IntentKey.STORE_APP_NAME, getApkName(downloadUrl));
            context.startService(intent);
        } catch (Exception e) {
            logger.error("WebViewDownLoadListener------> onDownloadStart Exception ! ", e);
        }
    }

    private static String getApkName(String url) {
        // http://gdown.baidu.com/data/wisegame/2b3c934e4996ff11/fenghuangxinwen_198.apk?apkname=%e6%b5%8b%e8%af%95apk%e5%90%8d%e7%a7%b0
        String apkName = "download-temp-file";
        if (url.contains("apkname=")) {
            String[] urlSubstring = url.split("apkname=");
            if (urlSubstring.length >= 1) {
                try {
                    final String s = urlSubstring[1];
                    if (s.contains("&")) {
                        urlSubstring = s.split("&");
                        if (urlSubstring.length > 0) {
                            apkName = URLDecoderUtils.decodeInUTF8(urlSubstring[0]);
                        }
                    } else {
                        apkName = URLDecoderUtils.decodeInUTF8(urlSubstring[1]);
                    }
                } catch (UnsupportedEncodingException e) {
                    logger.error("download UnsupportedEncodingException error! {}", e);
                }
            }
        } else {
            String[] urlSubstring = url.split("/");
            if (urlSubstring.length >= 1) {
                apkName = urlSubstring[urlSubstring.length - 1];
                apkName = (apkName.split("\\?"))[0];
            }
        }
        return apkName;
    }
}
