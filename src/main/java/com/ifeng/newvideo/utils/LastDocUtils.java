package com.ifeng.newvideo.utils;

import com.ifeng.newvideo.IfengApplication;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.model.HistoryModel;

import java.util.List;

/**
 * 首页接口请求参数lastDoc
 * Created by android-dev on 2017/5/2 0002.
 */
public class LastDocUtils {

    /**
     * simid客户端无数据使用空字符串
     * guid播放记录中的取值
     * 上报四条数据，取播放记录最新四条记录
     * 无播放记录则为空字符串
     *
     * @return <simid,guid>|<simid,guid>|<simid,guid>|<simid,guid>
     * 例如：<,122-ffds-ssa-ss-ddaa>|<,122-ffds-ssa-ss-ddaa>
     */
    public static String getLastDoc() {
        try {
            List<HistoryModel> latestFourData = HistoryDAO.getInstance(IfengApplication.getInstance()).getLatestData(HistoryDAO.NUM_4);
            if (ListUtils.isEmpty(latestFourData)) {
                return "";
            }
            // 拼接  <simid,guid>|<simid,guid>|<simid,guid>|<simid,guid>
            StringBuilder lastDoc = new StringBuilder(HistoryDAO.NUM_4);
            for (HistoryModel historyModel : latestFourData) {
                lastDoc.append('<')
                        .append(historyModel.getSimId())
                        .append(',')
                        .append(historyModel.getProgramGuid())
                        .append(">|")
                ;
            }
            // 删掉最后一个 |
            if (lastDoc.toString().endsWith("|")) {
                lastDoc.deleteCharAt(lastDoc.length() - 1);
            }
            return lastDoc.toString().replaceAll("null", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
