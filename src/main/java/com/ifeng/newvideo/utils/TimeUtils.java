package com.ifeng.newvideo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;

/**
 * 工具类：服务器时间和本地时间 相关
 * Created by cuihz on 2014/11/20.
 */
public class TimeUtils {

    private static final Logger logger = LoggerFactory.getLogger(TimeUtils.class);

    public static void setTimeDiff(String serverTime) {
        logger.debug("serverTime === {}", serverTime);
        try {
            SharePreUtils.getInstance().setServerLocalTimeDiff(Long.parseLong(serverTime) - System.currentTimeMillis());
        } catch (Exception e) {
            logger.error("服务器时间错误 {}", e);
        }
    }

    public static long getRealTime(long localTime) {
        try {
            return localTime + SharePreUtils.getInstance().getServerLocalTimeDiff();
        } catch (Exception e) {
            logger.error("获取手机时间和服务器时间差值 出现异常 {}", e);
            return localTime;
        }
    }

    public static long getLocalTime(long serverTime) {
        return serverTime - SharePreUtils.getInstance().getServerLocalTimeDiff();
    }

    public static boolean isSameDay() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String currentDate = dateFormat.format(getRealTime(System.currentTimeMillis()));
        String serverDate = SharePreUtils.getInstance().getServerDate();
        return currentDate.equals(serverDate);
    }

}
