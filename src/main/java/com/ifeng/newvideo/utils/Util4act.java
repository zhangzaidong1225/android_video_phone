package com.ifeng.newvideo.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.SharePreConstants;
import com.ifeng.video.dao.db.dao.ADInfoDAO;
import com.ifeng.video.dao.db.model.PhoneModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @describle 跟业务相关的工具类
 * Created by antboyqi on 14-8-6.
 */
public class Util4act {

    private static final Logger logger = LoggerFactory.getLogger(Util4act.class);

    public static class Constants {
        public static final String SHOUYE_RECOMMEND = "精选";
        public static final String SHOUYE_ORIGIN = "新闻推荐";
        public static final String LiBOTAI_RECOMMEND = "联播台";
    }

    /**
     * 启动页封面故事广告请求参数
     *
     * @return
     */
    public static Map<String, String> buildUrlParams4CoverStoryAD() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("os", ADInfoDAO.OS_ANDROID);
        map.put("screen", PhoneConfig.getScreenForAd());
        map.put("gv", PhoneConfig.softversion);
        map.put("uid", PhoneConfig.UID);
        map.put("publishid", PhoneConfig.publishid);
        return map;
    }

    /**
     * 单列item的ImageView布局参数配置  16:9
     */
    public static ViewGroup.LayoutParams staticHomeLayoutImageViewRatio4Single(Context context, ViewGroup.LayoutParams layoutParams) {
        // 此写法只适用于目前所有页面的图片宽高一致的情况，不一致时需要调整。
        String keyWidth = "keySingleWidth";
        String keyHeight = "keySingleHeight";
        Object attribute = IfengApplication.getInstance().getAttribute(keyWidth);
        int width = attribute != null ? (int) attribute : 0;
        Object attribute1 = IfengApplication.getInstance().getAttribute(keyHeight);
        int height = attribute1 != null ? (int) attribute1 : 0;

        if (height != 0 && width != 0) {
            layoutParams.width = width;
            layoutParams.height = height;
        } else {
            final DisplayMetrics displayMetrics = DisplayUtils.getDisplayMetrics();
            layoutParams.width = (displayMetrics.widthPixels) * 374 / 1080;
            layoutParams.height = layoutParams.width * 9 / 16;
            IfengApplication.getInstance().setAttribute(keyWidth, layoutParams.width);
            IfengApplication.getInstance().setAttribute(keyHeight, layoutParams.height);
        }
        logger.debug("single layoutParams.keyWidth::{},,layoutParams.keyHeight::{}", layoutParams.width, layoutParams.height);
        return layoutParams;
    }

    /**
     * 2列item的ImageView布局参数配置  16:9
     */
    public static ViewGroup.LayoutParams staticHomeLayoutImageViewRatio4Double(Context context, ViewGroup.LayoutParams layoutParams) {
        // 此写法只适用于目前所有页面的图片宽高一致的情况，不一致时需要调整。
        String keyWidth = "keyDoubleWidth";
        String keyHeight = "keyDoubleHeight";
        Object attribute = IfengApplication.getInstance().getAttribute(keyWidth);
        int width = attribute != null ? (int) attribute : 0;
        Object attribute1 = IfengApplication.getInstance().getAttribute(keyHeight);
        int height = attribute1 != null ? (int) attribute1 : 0;

        if (height != 0 && width != 0) {
            layoutParams.width = width;
            layoutParams.height = height;
        } else {
            final DisplayMetrics displayMetrics = DisplayUtils.getDisplayMetrics();
            layoutParams.width = (displayMetrics.widthPixels - (DisplayUtils.convertDipToPixel(9) * 3)) / 2;
            layoutParams.height = layoutParams.width * 9 / 16;
            IfengApplication.getInstance().setAttribute(keyWidth, layoutParams.width);
            IfengApplication.getInstance().setAttribute(keyHeight, layoutParams.height);
        }
        logger.debug("double layoutParams.width::{},,layoutParams.height::{}", layoutParams.width, layoutParams.height);
        return layoutParams;
    }

    /**
     * 当两个版本号不相同时，显示用户欢迎页
     */
    public static boolean shouldShowGuideByVersion(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        String preVersionName = null;
        String curVersionName = null;
        try {
            curVersionName = PackageUtils.getAppVersion(context);

            preVersionName = preferences.getString("versionName", null);
            if (!TextUtils.isEmpty(curVersionName)) {
                preferences.edit().putString("versionName", curVersionName).commit();
            }

            if(TextUtils.isEmpty(curVersionName) || TextUtils.isEmpty(preVersionName)||!curVersionName.equals(preVersionName)){
                preferences.edit().putBoolean("isFirstOpenApp", true).commit();
                return true;
            }else{
                preferences.edit().putBoolean("isFirstOpenApp", false).commit();
                return false;
            }
        } catch (Exception e) {
            return true;
        }
    }

    public static String getAuthorityFromPermission(Context context, String permission) {
        if (permission == null) return null;
        List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(PackageManager.GET_PROVIDERS);
        if (packs != null) {
            for (PackageInfo pack : packs) {
                ProviderInfo[] providers = pack.providers;
                if (providers != null) {
                    for (ProviderInfo provider : providers) {
                        if (permission.equals(provider.readPermission)) return provider.authority;
                        if (permission.equals(provider.writePermission)) return provider.authority;
                    }
                }
            }
        }
        return null;
    }

    /**
     * 判断service是否运行
     *
     * @param context
     * @param serviceFullName service的全称（一般为包名+service类的名称 ）
     * @return
     */
    public static boolean isServiceRun(Context context, String serviceFullName) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> list = am.getRunningServices(200);
        for (ActivityManager.RunningServiceInfo info : list) {
            if (info.service.getClassName().equals(serviceFullName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取手机的相关信息 MCC ： 国际标识 Mobile Country Code 中国为460
     * <p/>
     * MNC ： 运营商标志 Mobile Network Code 中国移动为00，中国联通为01,中国电信为03
     * <p/>
     * CID ： 基站标识 cell id
     * <p/>
     * LOC ： 区域标识 location id
     *
     * @param context
     */
    public static PhoneModel getPhoneInfo(Context context) {
        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        PhoneModel phone = new PhoneModel();
        phone.setImei(tel.getDeviceId());
        phone.setImsi(tel.getSubscriberId());// 获取Imsi
        phone.setMtype(android.os.Build.MODEL);// 手机型号
        phone.setLang(Locale.getDefault().getLanguage()); // 设备语言
        phone.setSim(tel.getSimSerialNumber());// 获得sim卡号

        String operator = tel.getNetworkOperator();
        //TODO 基站信息，先注掉
//        if (operator != null && operator.length() >= 5) {
//            phone.setMcc(Integer.valueOf(operator.substring(0, 3)));
//            phone.setMnc(Integer.valueOf(operator.substring(3, 5)));
//            try {
//            /* 获取手机基站信息 ,此方法在电信定制机 会出现问题，因此加入*/
//                GsmCellLocation gsm = (GsmCellLocation) tel.getCellLocation();
//                phone.setLac(gsm.getLac());
//                phone.setCid(gsm.getCid());
//            } catch (Exception e) {
//                logger.error(e.toString(), e);
//            }
//        }
        logger.info("imei:" + phone.getImei() + ";   imsi:" + phone.getImsi() + " ;   mtype:" + phone.getMtype() + ";   lang:" + phone.getLang());
        return phone;
    }


    public static boolean isInForeground(Context context) {
        logger.debug("in isAppOnForeground() ");
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = context.getPackageName();
        List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
        if (tasksInfo.size() > 0) {
            String topApplication = tasksInfo.get(0).topActivity.getPackageName();
            logger.debug("top application is " + topApplication);
            if (packageName.equals(topApplication)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 获取省份字段提取，主要用于定向推送
     *
     * @param temp
     * @return
     */
    public static String getProvince(String temp) {
        if (temp.contains("市")) {
            return temp.substring(0, temp.indexOf("市"));
        } else if (temp.contains("省")) {
            return temp.substring(0, temp.indexOf("省"));
        } else if (temp.contains("香")) {
            return "香港";
        } else if (temp.contains("澳")) {
            return "澳门";
        }
        return temp;
    }

    public static boolean shouldShowDescribeView(String memberType) {
        return CheckIfengType.isTopicType(memberType) || CheckIfengType.isLiveType(memberType) || CheckIfengType.isAD(memberType) || CheckIfengType.isVRLive(memberType);
    }


}
