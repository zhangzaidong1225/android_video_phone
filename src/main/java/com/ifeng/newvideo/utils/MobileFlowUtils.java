package com.ifeng.newvideo.utils;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.MobileDao;
import com.ifeng.video.dao.db.model.MobileUserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MobileFlowUtils {

    private static final Logger logger = LoggerFactory.getLogger(MobileFlowUtils.class);

    private static volatile MobileFlowUtils mobileFlowUtils = null;

    public static final String MOBILE_FLOW_REMAIN_OVER_HALF = "0";
    public static final String MOBILE_FLOW_REMAIN_HALF = "1";
    public static final String MOBILE_FLOW_REMAIN_TEN = "2";
    public static final String MOBILE_FLOW_REMAIN_ZERO = "3";

    public static final int MOBIEL_ORDER_FAIL = 0;
    public static final int MOBILE_ORDER_SUCCESS = 1;

    private Context mContext;

    public MobileFlowUtils(Context context) {
        this.mContext = context;
    }

    public MobileFlowUtils() {
    }

    public static MobileFlowUtils getInstance(Context context) {
        if (mobileFlowUtils == null) {
            synchronized (MobileFlowUtils.class) {
                if (mobileFlowUtils == null) {
                    mobileFlowUtils = new MobileFlowUtils(context);
                }
            }
        }
        return mobileFlowUtils;
    }

    public void requestMobileFreeVideoCheckUrl() {
        MobileDao.requestMobileFreeVideoCheckUrl(PhoneConfig.userKey, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (response == null) {
                    logger.debug("mobileCheckUrl is null");
                    return;
                }
                com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(response.toString());
                String url = jsonObject.getString("resultUrl");
                logger.debug("mobileCheckUrl: {}", response.toString());
                SharePreUtils.getInstance().setMobileCheckUrl(url);
                requestMobilePcid();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("mobileCheckUrlError:{} ", error.getMessage());
            }
        });
    }

    public void requestMobilePcid() {
        MobileDao.requestMobielPcid(SharePreUtils.getInstance().getMobileCheckUrl(),
                MobileUserModel.class, new Response.Listener<MobileUserModel>() {
                    @Override
                    public void onResponse(MobileUserModel response) {
                        if (response == null) {
                            logger.debug("mobileUserModel is null");
                            return;
                        }
                        logger.debug("mobileUserModel: {}", response.toString());
                        String statusCode = response.getResultcode();
                        if ("0".equals(statusCode)) {
                            String pcid = response.getPcId();
                            SharePreUtils.getInstance().setMobilePcid(pcid);
                            requestMobileOrderStatus();
                        } else {
                            SharePreUtils.getInstance().setMobilePcid("");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("mobileUserModelError: {}", error.getMessage());
                    }
                });
    }

    public void requestMobileOrderStatus() {
        MobileDao.requestMobielOrderStatus(PhoneConfig.userKey,
                SharePreUtils.getInstance().getMobilePcid(),
                MobileUserModel.MobileOrder.class,
                new Response.Listener<MobileUserModel.MobileOrder>() {
                    @Override
                    public void onResponse(MobileUserModel.MobileOrder response) {
                        if (response == null) {
                            logger.debug("mobileOrder is null");
                            return;
                        }
                        logger.debug("mobileOrder:{}", response.toString());
                        int resultStatus = response.getResult();
                        SharePreUtils.getInstance().setMobileOrderStatus(resultStatus);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("mobileOrderError:{} ", error.getMessage());
                    }
                });
    }

    public void checkMobileFlow() {
        if (1 == SharePreUtils.getInstance().getMobileOrderStatus() && NetUtils.isMobile(mContext)) {
            requestMobileFlow();
        }
    }

    public void requestMobileFlow() {
        MobileDao.requestMobileRemain(PhoneConfig.userKey,
                SharePreUtils.getInstance().getMobilePcid(),
                MobileUserModel.MobileFlowRemain.class,
                new Response.Listener<MobileUserModel.MobileFlowRemain>() {
                    @Override
                    public void onResponse(MobileUserModel.MobileFlowRemain response) {
                        if (response == null) {
                            logger.debug("mobileFlow is null");
                            return;
                        }
                        MobileUserModel.MobileFlow mobileFlow = response.getResult();
                        logger.debug("mobileFlow:{}", response.toString());
                        SharePreUtils.getInstance().setMobileFlowRemain(mobileFlow.getNumber());
                        SharePreUtils.getInstance().setMobileFlowTotal(mobileFlow.getTotalNum());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.debug("mobileFlowError :{}", error.getMessage());
                    }
                });
    }

    public void mobileFlowAlert(String num, String totalNum) {
        switch (num) {
            case MOBILE_FLOW_REMAIN_ZERO:
                ToastUtils.getInstance().showShortToast(R.string.video_mobile_remain_flow);
                IfengApplication.mobileNetCanPlay = false;
                break;
            case MOBILE_FLOW_REMAIN_TEN:
                int currentNum = (int) (0.1 * Integer.valueOf(totalNum));
                String currentFlow = String.format(mContext.getResources().getString(R.string.video_mobile_flow_half), "10%", currentNum + "G");
                ToastUtils.getInstance().showShortToast(currentFlow);
                break;
            case MOBILE_FLOW_REMAIN_HALF:
                int halfNum = (int) (0.5 * Integer.valueOf(totalNum));
                String halfFlow = String.format(mContext.getResources().getString(R.string.video_mobile_flow_half), "50%", halfNum + "G");
                ToastUtils.getInstance().showShortToast(halfFlow);
                break;
            case MOBILE_FLOW_REMAIN_OVER_HALF:
                break;
            default:
                break;
        }
    }

    public void mobileToast() {
        if (MobileFlowUtils.MOBILE_ORDER_SUCCESS == SharePreUtils.getInstance().getMobileOrderStatus()) {
            if (!MobileFlowUtils.MOBILE_FLOW_REMAIN_ZERO.equals(SharePreUtils.getInstance().getMobileFlowRemain())) {
                if (IfengApplication.getInstance().mobileToastNum < 2) {
                    ToastUtils.getInstance().showShortToast(R.string.video_mobile_global_alert);
                    IfengApplication.getInstance().mobileToastNum += 1;
                }
            }
        } else {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }
    }
}
