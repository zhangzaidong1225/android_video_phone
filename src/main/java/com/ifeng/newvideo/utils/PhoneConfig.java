package com.ifeng.newvideo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.video.core.utils.DistributionInfo;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.SharePreConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.util.Locale;

import static ch.qos.logback.core.encoder.ByteArrayUtil.toHexString;

/**
 * 手机配置信息类
 */
public class PhoneConfig {
    private static final Logger logger = LoggerFactory.getLogger(PhoneConfig.class);

    /**
     * 系统版本
     */
    public static String mos;

    private final static String USER_AGENT = "IFENGVIDEO7_Platform_Android";

    private static final String STATISTIC_ID = "statisticId";

    /**
     * 视频客户端统计用
     */
    public static String ua_for_video;
    /**
     * 新闻客户端统计用
     */
    public static String ua_for_news;
    /**
     * 视频客户端 反馈系统用
     */
    public static String ua_for_feedback;
    /**
     * app版本号
     */
    public static String softversion;
    /**
     * 渠道号
     */
    public static String publishid = "";
    /**
     * md5 (IMEI or MAC)
     */
    public static String userKey;
    public static long logintime;
    /**
     * loginTime + random
     */
    public static String stasId;
    /**
     * IMEI or MAC
     */
    public static String UID;
    /**
     * 国际移动用户识别码IMSI
     */
    private static String IMSI;
    /**
     * 设备识别码
     */
    public static String IMEI;
    /**
     * 设备品牌
     */
    public static String BRAND;
    /**
     * 设备厂商
     */
    public static String MANUFACTURER;

    public static int screenWidth = 0;
    public static int screenHeight = 0;
    public static float screenDensity;
    public static int screenDensityDpi;
    /**
     * 是否为升级用户
     */
    private static String isUpdate;

    private static PhoneConfig instance = null;

    public static void init(Context context) {

        // 软件版本号！！！注意！！！顺序在publishId初始化之前
        softversion = getVersionName(context);

        // 发布渠道号
        DistributionInfo.getInstance(context);
        // TODO 发版必查
        // 1、手动打包，在这儿修改渠道号
        // 2、jenkins发布，注释掉就好
        // DistributionInfo.publish_id = "30011";
        // DistributionInfo.umeng_channel = "30011";

        publishid = PublishIdUtils.initPublishID(context);
        DistributionInfo.publish_id = publishid;

        // 系统版本号
        mos = "android_" + android.os.Build.VERSION.RELEASE;

        //设备品牌
        BRAND = Build.BRAND;
        //设备厂商
        MANUFACTURER = Build.MANUFACTURER;

        IMSI = getIMSI(context);

        IMEI = getIMEI(context);

        UID = getDeviceID(context);

        userKey = getUserKey(UID);

        ua_for_video = getUAForVideo();
        ua_for_news = getUAForNews();
        ua_for_feedback = getUAForFeedback();

        logintime = getLoginTime(context);

        stasId = getStatisticId(context);

        isUpdate = getIsUpdate();

        DisplayMetrics metric = getDisplayMetrics(context);
        screenWidth = metric.widthPixels;
        screenHeight = metric.heightPixels;
        screenDensity = metric.density;
        screenDensityDpi = metric.densityDpi;
    }

    private static DisplayMetrics getDisplayMetrics(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metric = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(metric);
        return metric;
    }

    public synchronized static PhoneConfig getInstance() {
        if (instance == null) {
            instance = new PhoneConfig();
        }
        return instance;
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static int convertDipToPx(Context context, int dip) {
        float density = context.getResources().getDisplayMetrics().density;
        logger.debug("PhoneConfig device density = {}" + density);
        return (int) (density * dip + 0.5);
    }

    /**
     * 设备mac地址
     */
    public static String getDeviceMac() {
        try {
            return DeviceUtils.getMacAddress();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取设备的语⾔言设置
     *
     * @return
     */
    public static String getPhoneLanguage() {
        return Locale.getDefault().getLanguage();
    }

    /**
     * 唯一区分用户;形式可能为第三方分配的uid,或者与mei项内容相同
     *
     * @param context
     * @return
     */
    public static String getUid(Context context) {


        return null;
    }

    public static String getStatisticId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, 0);
        if (preferences.contains(STATISTIC_ID)) {
            return preferences.getString(STATISTIC_ID, "");
        } else {
            String statisticId = "" + logintime + (int) (Math.random() * 8999 + 1000);
            preferences.edit().putString(STATISTIC_ID, statisticId).commit();
            return statisticId;
        }
    }


    private static TelephonyManager getTelephonyManager(Context context) {
        try {
            return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取IMSI
     */
    private static String getIMSI(Context context) {
        TelephonyManager telephonyManager = getTelephonyManager(context);
        try {
            if (telephonyManager != null) {
                return telephonyManager.getSubscriberId();
            }
        } catch (Exception e) {
            logger.error("getIMSI error ! {}", e.getMessage());
        }
        return "UnKnown";
    }

    /**
     * 获取IMEI
     */
    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = getTelephonyManager(context);
        try {
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
        } catch (Exception e) {
            logger.error("getIMEI error ! {}", e.getMessage());
        }
        return "";
    }

    /**
     * 获取SIM卡运营商名称,根据返回的运营商名称判断会出现问题，所以通过IMSI来进行判断 例如：ChinaMobile
     *
     * @param context
     * @return
     */
    private static String getServiceProviderName(Context context) {
        TelephonyManager telManager = getTelephonyManager(context);
        if (telManager == null) {
            return null;
        }
        String operator = telManager.getSimOperator();
        String simString = null;
        String isSIM = null;
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007")) {
                // 中国移动
                simString = "cmcc";
            } else if (operator.equals("46001")) {
                // 中国联通
                simString = "cucc";
            } else if (operator.equals("46003")) {
                // 中国电信
                simString = "ctcc";
            } else if (!operator.equals("")) {
                simString = "other";
            }

            if (simString != null) {
                isSIM = "&sp=" + simString;
            } else {
                if (isSIM == null) {
                    isSIM = "";
                }
            }
        }
        return isSIM;
    }

    /**
     * 获取SIM卡运营商对应的编号
     * 0-Unkown,1-移动,2-联通,3-电信
     *
     * @param context
     * @return
     */
    public static int getSimCode(Context context) {
        TelephonyManager telManager = getTelephonyManager(context);
        if (telManager == null) {
            return 0;
        }
        String operator = telManager.getSimOperator();
        int operatorCode = 0;
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007")) {
                // 中国移动
                operatorCode = 1;
            } else if (operator.equals("46001")) {
                // 中国联通
                operatorCode = 2;
            } else if (operator.equals("46003")) {
                // 中国电信
                operatorCode = 3;
            } else if (!operator.equals("")) {
                operatorCode = 0;
            }

        }
        return operatorCode;
    }

    public static String getSimName(Context context) {
        TelephonyManager telManager = getTelephonyManager(context);
        if (telManager == null) {
            return "Unknown";
        }
        String operator = telManager.getSimOperator();
        if (TextUtils.isEmpty(operator)) {
            return "Unknown";
        }
        if (telManager.getSimState() != TelephonyManager.SIM_STATE_ABSENT && telManager.getSimState() != TelephonyManager.SIM_STATE_UNKNOWN) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007")) {
                return "移动";// 中国移动
            }
            if (operator.equals("46001")) {
                return "联通";// 中国联通
            }
            if (operator.equals("46003")) {
                return "电信";// 中国电信
            }
        }
        return "Unknown";
    }

    /**
     * 是否为升级用户，是1，否0
     * <p>
     * 是升级用户的情况： 旧版本值不为空  && 当前版本值和旧版本值 不同
     */
    private static String getIsUpdate() {
        String lastVersionName = SharePreUtils.getInstance().getFirstInstallVersionName();
        logger.debug("curSoftVersion = {}, oldSoftVersion = {}", softversion, lastVersionName);

        //  旧版本值不为空 && 当前版本值和旧版本值 不同，为升级用户
        if (!TextUtils.isEmpty(lastVersionName) && !softversion.equals(lastVersionName)) {
            return "1";
        }
        // 否则，为非升级用户
        SharePreUtils.getInstance().setFirstInstallVersionName(softversion);
        return "0";
    }

    public static boolean isUpdateUser() {
        return "1".equals(getIsUpdate());
    }

    public static String getRe() {
        if (screenWidth > screenHeight) {
            return screenWidth + "*" + screenHeight;
        }
        return screenHeight + "*" + screenWidth;
    }

    public static String getCombineString(String netType, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("datatype=").append("videoapp").append('&')
                .append("mos=").append(mos).append('&')
                .append("softversion=").append(softversion).append('&')
                .append("publishid=").append(publishid).append('&')
                .append("userkey=").append(userKey).append('&')
                .append("ua=").append(ua_for_video).append('&')
                .append("net=").append(netType).append('&')
                .append("logintime=").append(getLoginTimeForStatistic()).append('&') // loginTime毫秒级发送数据时如果老数据是秒级别（即<=1000000000）则正常发送
                .append("uid=").append(stasId).append('&')
                .append("openudid=").append(UID).append('&') // openudid为ios值，android上报uid，和userkey对比校验
                .append("re=").append(getRe()).append('&')
                .append("isupdate=").append(isUpdate)
                .append(getServiceProviderName(context)).append('&')
                .append("userid=").append(new User(context).getUid()).append('&')
                .append("pid=").append(IMEI);
        return stringBuilder.toString();
    }

    public static String getCombineStringForNews(String netType, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("datatype=").append("newsapp").append('&')
                .append("mos=").append(mos).append('&')
                .append("softversion=").append("5.4.0").append('&')
                .append("publishid=").append("2024").append('&')
                .append("userkey=").append(userKey).append('&')
                .append("ua=").append(ua_for_news).append('&')
                .append("net=").append(netType).append('&')
                .append("logintime=").append(getLoginTimeForStatistic()).append('&') // loginTime毫秒级发送数据时如果老数据是秒级别（即<=1000000000）则正常发送
                .append("isupdate=").append(isUpdate).append('&')
                .append("md5=").append(getMD5ForNews(context)).append('&')
                .append("sha1=").append(getSHA1ForNews(context));
        return stringBuilder.toString();
    }

    public static String getCombineStringForNewsSDK(String netType, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("datatype=").append("news_sdk").append('&')
                .append("mos=").append(mos).append('&')
                .append("softversion=").append(softversion).append('&')
                .append("publishid=").append(IfengApplication.IFENG_NEWS_SDK_PUBLISH_ID).append('&')
                .append("userkey=").append(UID).append('&')
                .append("ua=").append(ua_for_news).append('&')
                .append("net=").append(netType).append('&')
                .append("logintime=").append(getLoginTimeForStatistic()).append('&')
                .append("isupdate=").append(isUpdate).append('&')
                .append("md5=").append(getMD5ForNews(context)).append('&')
                .append("sha1=").append(getSHA1ForNews(context)).append('&')
                .append("head=").append("http");
        return stringBuilder.toString();
    }


    public static String getCombineStringForAd(String netType, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("datatype=").append("videoapp").append('&')
                .append("mos=").append(mos).append('&')
                .append("userkey=").append(userKey);
        return stringBuilder.toString();
    }

    public static String getCombineStringForErr(String netType, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("datatype=").append("videoapp").append('&')
                .append("mos=").append(mos).append('&')
                .append("softv=").append(softversion).append('&')
                .append("publishid=").append(publishid).append('&')
                .append("userkey=").append(userKey).append('&')
                .append("ua=").append(ua_for_video).append('&')
                .append("net=").append(netType).append('&')
                .append("re=").append(getRe()).append('&')
                .append(getServiceProviderName(context));
        return stringBuilder.toString();
    }

    private static long getLoginTime(Context context) {
        long loginTime = SharePreUtils.getInstance().getLoginTimeForStatistics();
        if (loginTime < 0) {
            loginTime = System.currentTimeMillis();
            SharePreUtils.getInstance().setLoginTimeForStatistics(loginTime);
        }
        return loginTime;
    }

    private static String getVersionName(Context ctx) {
        try {
            PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            logger.error("getVersionName error !{}", e.toString(), e);
            return "";
        }
    }

    /**
     * IMEI or MAC address or ANDROID_ID
     */
    private static String getDeviceID(Context ctx) {
        String deviceId = getSharePreferences().getString(SharePreConstants.DEVICE_ID, null);
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = getIMEI(ctx);
        }
        if (TextUtils.isEmpty(deviceId) || deviceId.contains("000000000000000")) {
            deviceId = getDeviceMac();
        }
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = DeviceUtils.getAndroidID();
        }
        if (!TextUtils.isEmpty(deviceId)) {
            getSharePreferences().edit().putString(SharePreConstants.DEVICE_ID, deviceId).apply();
        }
        return deviceId;
    }

    private static SharedPreferences getSharePreferences() {
        return IfengApplication.getAppContext().getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * 获取MD5加密的userkey
     *
     * @return
     */
    private static String getUserKey(String deviceId) {
        try {
            return StringUtils.md5s(deviceId);
        } catch (NullPointerException e) {
            logger.error("getUserKey error !{}", e.toString(), e);
            return null;
        }
    }

    private static String getBrand() {
        String brand = Build.BRAND;
        if (TextUtils.isEmpty(brand)) {    //取不到 用一个默认值
            brand = "android";
        }
        return brand;
    }

    /**
     * 获取系统ua
     * 1、设备名称和品牌信息用","隔开
     * 2、空格转换为_, 均小写
     * 3、urlEncode
     */
    private static String getUAForVideo() {
        String brand = getBrand();
        String ua = (Build.MODEL + "," + brand).replaceAll(" ", "_").toLowerCase();
        try {
            ua = URLEncoderUtils.encodeInUTF8(ua);
        } catch (Exception e) {
            logger.error("getUAForVideo error ! {}", e);
        }
        return ua;
    }

    /**
     * 新闻统计使用 获取系统ua
     *
     * @return
     */
    private static String getUAForNews() {
        String ua = null;
        try {
            ua = URLEncoderUtils.encodeInUTF8(android.os.Build.MODEL.replaceAll(" ", "_"));
        } catch (Exception e) {
            ua = USER_AGENT;
        }
        return ua.toLowerCase(Locale.getDefault());
    }

    /**
     * 品牌,设备名称
     */
    private static String getUAForFeedback() {
        String brand = getBrand();
        String ua = (brand + "," + Build.MODEL).replaceAll(" ", "_").toLowerCase();
        try {
            ua = URLEncoderUtils.encodeInUTF8(ua);
        } catch (Exception e) {
            logger.error("getUAForVideo error ! {}", e);
        }
        return ua;
    }


    private static String getMD5ForNews(Context ctx) {
        if (null == ctx) {
            return "";
        }
        String md5 = "";
        try {
            PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(
                    ctx.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];
            MessageDigest md1 = MessageDigest.getInstance("MD5");
            md1.update(sign.toByteArray());
            byte[] digest = md1.digest();
            if (null != digest) {
                md5 = toHexString(digest);
            }
        } catch (Exception e) {
            // ignore it.
        }
        return md5;

    }

    private static String getSHA1ForNews(Context ctx) {
        if (null == ctx) {
            return "";
        }
        String sha1 = "";
        try {
            PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(
                    ctx.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];
            MessageDigest md1 = MessageDigest.getInstance("SHA1");
            md1.update(sign.toByteArray());
            byte[] digest = md1.digest();
            if (null != digest) {
                sha1 = toHexString(digest);
            }
        } catch (Exception e) {

        }
        return sha1;

    }

    /**
     * 获取ip地址
     *
     * @return
     */
    public static String getIpAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            logger.error("getIpAddress error !{}", e.toString(), e);
            return "";
        }
    }

    /**
     * screenWidth x screenHeight
     */
    public static String getScreenForAd() {
        return screenWidth + "x" + screenHeight;
    }

    /**
     * screenWidth * screenHeight
     */
    public static String getScreenForSmartStatistics() {
        return screenWidth + "*" + screenHeight;
    }


    public static String getLoginTimeForStatistic() {
        return ((logintime / 1000000000 > 10) ? logintime / 1000 : logintime) + "";
    }


    /**
     * 谷歌渠道包 要求：
     * 1.去掉所有自身升级
     * 2.去掉所有广告的下载，量不大，甚至可以考虑干脆去掉广告
     * 3.去掉评分
     * 4.去掉美女、时尚频道
     * 5.去掉退出安装新闻
     * 6.去掉我的中推荐h5区域 (在订阅下面)，因为里面可能会有游戏之类的下载
     * 7.CustomWebViewClient onReceivedSslError，弹dialog
     */
    public static boolean isGooglePlay() {
        return publishid.equals("30011");
    }

    /**
     * 金立，去掉我的中推荐h5区域 (在订阅下面)，因为里面可能会有游戏之类的下载
     */
    public static boolean isGionee() {
        return publishid.equals("41004");
    }

    /**
     * TODO
     * 设备类型：iphone|ipad|phone|pad 等
     *
     * @return
     */
    public static String getDeviceFamily() {
        return "androidphone";
    }

    private static final String vt = "5";

    /**
     * IMCP要求的适配版本号，为保持一致，非IMCP接口也采用，默认为5
     *
     * @return
     */
    public static String getVTForPush() {
        return vt;
    }

    /**
     * 屏幕尺寸
     *
     * @return
     */
    public static String getScreenSizeForPush() {
        StringBuilder screenSize = new StringBuilder();
        screenSize.append(screenWidth).append('x').append(screenHeight);
        return screenSize.toString();
    }
}
