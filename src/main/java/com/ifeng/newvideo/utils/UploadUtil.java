package com.ifeng.newvideo.utils;

import android.util.Log;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.video.core.utils.FileUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class UploadUtil {

    private static volatile UploadUtil uploadUtil = null;
    private static final String TAG = "uploadFile";
    private static final int TIME_OUT = 10 * 1000;   //超时时间
    private static final String CHARSET = "utf-8"; //设置编码

    public static UploadUtil getInstance() {
        if (uploadUtil == null) {
            synchronized (UploadUtil.class) {
                if (uploadUtil == null) {
                    uploadUtil = new UploadUtil();
                }
            }
        }
        return uploadUtil;
    }

    /**
     * android上传文件到服务器
     *
     * @param file       需要上传的文件
     * @param RequestURL 请求的rul
     * @return 返回响应的内容
     */
    public String uploadFile(File file, String RequestURL) {
        String result = null;
        String BOUNDARY = UUID.randomUUID().toString();  //边界标识   随机生成
        String PREFIX = "--";
        String LINE_END = "\r\n";
        String CONTENT_TYPE = "multipart/form-data";   //内容类型

        try {

            if (file != null) {
                /**
                 * 当文件不为空，把文件包装并且上传
                 */
                String blockId = shaHexForTransmission(file.getAbsolutePath());
                long size = FileUtils.getFileSize(file.getAbsolutePath());
                String fileId = blockId + "_1";
                String blockCount = "1";
                String appId = "ifengvideo";
                String blockIndex = "1";
                String subTime = String.valueOf(System.currentTimeMillis());
                String bizId = "android_avt_" + User.getUid() + "_" + subTime.substring(subTime.length() - 5, subTime.length());
                Log.d("uploadUtils", bizId + "--" + subTime);

                URL url = new URL(RequestURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(TIME_OUT);
                conn.setConnectTimeout(TIME_OUT);
                conn.setDoInput(true);  //允许输入流
                conn.setDoOutput(true); //允许输出流
                conn.setUseCaches(false);  //不允许使用缓存
                conn.setRequestMethod("POST");  //请求方式
                conn.setRequestProperty("Charset", CHARSET);  //设置编码
                conn.setRequestProperty("connection", "keep-alive");
                conn.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary=" + BOUNDARY);
                conn.connect();


                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

                String[] params = {"\"appId\"", "\"bizId\"", "\"blockIndex\"", "\"fileId\"", "\"blockCount\"", "\"blockId\""};
                String[] values = {appId, bizId, blockIndex, fileId, blockCount, blockId};
                //添加相应参数
                for (int i = 0; i < params.length; i++) {
                    //添加分割边界
                    StringBuffer sb = new StringBuffer();
                    sb.append(PREFIX);
                    sb.append(BOUNDARY);
                    sb.append(LINE_END);

                    sb.append("Content-Disposition: form-data; name=" + params[i] + LINE_END);
                    sb.append(LINE_END);
                    sb.append(values[i]);
                    sb.append(LINE_END);
                    dos.write(sb.toString().getBytes());
                }

                StringBuffer sb = new StringBuffer();
                sb.append(PREFIX);
                sb.append(BOUNDARY);
                sb.append(LINE_END);
                /**
                 * 这里重点注意：
                 * name里面的值为服务器端需要key   只有这个key 才可以得到对应的文件
                 * filename是文件的名字，包含后缀名的   比如:abc.png
                 */
                sb.append("Content-Disposition: form-data; name=\"blockContent\"; filename=\"" + blockId + "\"" + LINE_END);

//                sb.append("Content-Disposition: form-data; name=\"blockContent\"; blockContent=\"" + blockId + "\""+LINE_END);
                sb.append("Content-Type: application/octet-stream; charset=" + CHARSET + LINE_END);
//                sb.append("Content-Type: image/jpg"+LINE_END);
                sb.append(LINE_END);
                dos.write(sb.toString().getBytes());
                //读取文件的内容
                InputStream is = new FileInputStream(file);
                byte[] bytes = new byte[1024];
                int len = 0;
                while ((len = is.read(bytes)) != -1) {
                    dos.write(bytes, 0, len);
                }
                is.close();
                //写入文件二进制内容
                dos.write(LINE_END.getBytes());
                //写入end data
                byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINE_END).getBytes();
                dos.write(end_data);
                dos.flush();
                /**
                 * 获取响应码  200=成功
                 * 当响应成功，获取响应的流
                 */
                int res = conn.getResponseCode();
                Log.d("file", "code:" + res);

                if (res == 200) {
                    InputStream input = conn.getInputStream();
                    StringBuffer newSb = new StringBuffer();
                    int ss;
                    while ((ss = input.read()) != -1) {
                        newSb.append((char) ss);
                    }
                    result = newSb.toString();
                    Log.d("file", result + "");
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String shaHexForTransmission(String filePath) {
        InputStream in = null;
        String checksum = null;
        try {
            File file = new File(filePath);
            in = new FileInputStream(file);
            int fileLength = in.available();
            byte[] data = null;

            if (fileLength > 512 * 1024) {
                data = new byte[512 * 1024 + 8];
            } else {
                data = new byte[(int) fileLength + 8];
            }
            in.read(data, 0, data.length - 8);
            System.arraycopy(toBytes(fileLength), 0, data, data.length - 8, 8);
            checksum = shaHex(data);
        } catch (IOException e) {
            Log.e(e.getMessage(), e + "");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    //ignore exception
                }
            }
        }
        Log.d("file", checksum + "");
        return checksum;
    }

    private static byte[] toBytes(long l) {
        byte[] b = new byte[8];
        for (int i = 0; i < 8; i++) {
            b[i] = (byte) (l >>> (56 - (i * 8)));
        }
        return b;
    }

    /**
     * 对byte数组进行sha-1摘要运算
     *
     * @param toDigest
     * @return
     */
    public static String shaHex(byte[] toDigest) {
        if (toDigest == null) {
            return null;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] digested = digest.digest(toDigest);
            return bytesToHexString(digested);
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }
}
