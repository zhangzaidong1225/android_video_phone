package com.ifeng.newvideo.utils;

import com.android.volley.Response;
import com.ifeng.video.dao.db.dao.WeMediaDao;

/**
 * Created by ll on 2017/7/25.
 */
public class SubscribeWeMediaUtil {

    /**
     * 封装 订阅 接口
     * 为了添加积分功能
     * @param weMediaId 自媒体id
     * @param userId    用户id
     * @param operate   ADD_SUBSCRIBE、CANCEL_SUBSCRIBE，订阅、退订
     * @param t
     * @param cls
     * @param listener
     * @param errorListener
     */
    public static void subscribe(String weMediaId, String userId, int operate, String t, Class cls, Response.Listener listener, Response.ErrorListener errorListener) {
        if (WeMediaDao.ADD_SUBSCRIBE == operate) {
            UserPointManager.addRewards(UserPointManager.PointType.addBySubscribeWeMedia);
        }
        WeMediaDao.subscribe(weMediaId, userId, operate, t, cls, listener, errorListener);
    }
}
