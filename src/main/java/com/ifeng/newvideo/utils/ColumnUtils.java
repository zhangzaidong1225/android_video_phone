package com.ifeng.newvideo.utils;

import com.ifeng.video.dao.db.model.SubColumnVideoListInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pengcheng on 2015/11/27 027.
 */
public class ColumnUtils {

    private static final Logger logger = LoggerFactory.getLogger(ColumnUtils.class);

    /**
     * 转换成栏目标题
     *
     * @param name
     * @param columnName
     * @return
     */
    public static String convertColumnTitle(String name, String columnName) {
        String title = name;
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat stringFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
        SimpleDateFormat format = new SimpleDateFormat("MM.dd", Locale.US); // xcr
        // 去掉月份
//        title = title.replaceFirst(columnName + "：", "");
//        title = title.replaceFirst(columnName + ":", "");
        title = title.replaceFirst(columnName, "");

        Pattern pattern = Pattern.compile("\\d{4}\\-\\d{2}\\-\\d{2}");
        Pattern pattern2 = Pattern.compile("\\d{8}");
        Matcher matcher = pattern.matcher(title);
        Matcher matcher2 = pattern2.matcher(title);// 又是服务器容错！！
        String time = "";
        try {
            if (matcher.find()) {
                Date date = dataFormat.parse(matcher.group());
                time = format.format(date);
                title = title.replaceFirst(matcher.group(), "");
            } else if (matcher2.find()) {
                Date date = stringFormat.parse(matcher2.group());
                time = format.format(date);
                title = title.replaceFirst(matcher2.group(), "");
            } else {
                time = format.format(dataFormat.parse(time));
            }
            title = time + "  " + title.trim();
        } catch (Exception e) {
            logger.error("转化[" + title + "]栏目的日期出现错误！！");
        }
        return title;
    }

    /**
     * 转换栏目标题
     *
     * @param program
     * @return
     */
    public static String convertColumnTitle(SubColumnVideoListInfo.VideoItem program) {
        if (program == null) {
            return null;
        }
        String title = program.getName();
        String columnName = program.getColumnName();
        String publishTime = program.getSeVersion();
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat stringFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
        SimpleDateFormat format = new SimpleDateFormat("MM.dd", Locale.US); // xcr
        // 去掉月份
//        title = title.replaceFirst(columnName + "：", "");
//        title = title.replaceFirst(columnName + ":", "");
        title = title.replaceFirst(columnName, "");
        Pattern dataPattern = Pattern.compile("\\d{4}\\-\\d{2}\\-\\d{2}");
        Pattern stringPattern = Pattern.compile("\\d{8}");
        Matcher dataMatcher = dataPattern.matcher(title);
        Matcher stringMatcher = stringPattern.matcher(title);// 又是服务器容错！！
        String time = "";
        try {
            if (dataMatcher.find()) {
                Date date = dataFormat.parse(dataMatcher.group());
                time = format.format(date);
                title = title.replaceFirst(dataMatcher.group(), "");
            } else if (stringMatcher.find()) {
                Date date = stringFormat.parse(stringMatcher.group());
                time = format.format(date);
                title = title.replaceFirst(stringMatcher.group(), "");
            } else {
                time = publishTime.substring(0, 8);
                time = format.format(stringFormat.parse(time));
            }
            title = time + "  " + title.trim();
        } catch (Exception e) {
            logger.error("转化[" + title + "]栏目出现错误！！");
            return null;
        }
        return title;
    }
}
