package com.ifeng.newvideo.utils;

import android.graphics.Color;
import android.text.TextUtils;
import com.ifeng.newvideo.R;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 工具类：处理列表中标签显示
 * Created by Pengcheng on 2015/8/5
 */
public class TagUtils {

    private static final Logger logger = LoggerFactory.getLogger(TagUtils.class);

    public static int getTagTextColor(String memberType) {
        // 广告  签到（推广）
        if (CheckIfengType.isAD(memberType) || CheckIfengType.isSignIn(memberType)) {
            return Color.parseColor("#2a90d7"); // 深蓝色
        }
        // 其他
        return Color.parseColor("#f54343"); //红色
    }

    public static int getTagBackground(String memberType) {
        // 广告  签到（推广）
        if (CheckIfengType.isAD(memberType) || CheckIfengType.isSignIn(memberType)) {
            return R.drawable.home_item_tag_blue; // 深蓝色
        }
        // 其他
        return R.drawable.home_item_tag_red; //红色
    }

    /**
     * 根据接口数据中的tag和memberType返回不同的标签显示文字
     *
     * @param tag        接口数据中的tag值，如原创
     * @param memberType 接口数据中memberType值，如live
     * @param clickType  接口数据中clickType值，如adin
     */
    public static String getTagTextForList(String tag, String memberType, String clickType) {
        return getTagText(tag, memberType, clickType, false);
    }

    private static String getTagText(String tag, String memberType, String clickType, boolean isHeadFlow) {
        if (!TextUtils.isEmpty(tag) && tag.equals("隐藏")) {
            return "";
        }
        String EMPTY_CHAR = "";
        // String EMPTY_CHAR = "  ";
        if (!TextUtils.isEmpty(tag)) { // 接口返回的tag
            return EMPTY_CHAR + tag.trim() + EMPTY_CHAR;
        }
        if (TextUtils.isEmpty(memberType)) {
            return "";
        }
//        if(CheckIfengType.isAD(memberType)){
//            return EMPTY_CHAR + "推广" + EMPTY_CHAR;
//        }
        if (CheckIfengType.isLiveType(memberType)) {  // 直播
            return EMPTY_CHAR + "直播" + EMPTY_CHAR;
        }
        if (CheckIfengType.isTopicType(memberType)) {// 专题
            return EMPTY_CHAR + "专辑" + EMPTY_CHAR;
        }
        if (CheckIfengType.isVRLive(memberType) || CheckIfengType.isVRVideo(memberType)) {// VR
            return EMPTY_CHAR + "VR" + EMPTY_CHAR;
        }
        // if (CheckIfengType.isVideo(memberType) && isHeadFlow) {// 推荐
        //     return EMPTY_CHAR + "推荐" + EMPTY_CHAR;
        // }
        // if (CheckIfengType.isSignIn(memberType) ||
        //         CheckIfengType.isAdapp(memberType) ||
        //         CheckIfengType.isAdnew(memberType)) {// 签到 Adapp adnew
        //     return EMPTY_CHAR + "推广" + EMPTY_CHAR;
        // }

        if (TextUtils.isEmpty(clickType)) {
            return "";
        }
        // if (CheckIfengType.shouldTagBePromotion(clickType)) {// 推广
        //     return EMPTY_CHAR + "推广" + EMPTY_CHAR;
        // }
        if (CheckIfengType.shouldTagBeAdvert(clickType)) {// 广告
            return EMPTY_CHAR + "广告" + EMPTY_CHAR;
        }
        return ""; // 其他
    }
}
