package com.ifeng.newvideo.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class ScreenUtils {
    public static int getWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static boolean isInRight(int xWeight) {
        return (xWeight > getWidth() * 3 / 4);
    }

    public static boolean isInLeft(int xWeight) {
        return (xWeight < getWidth() / 4);
    }

    public static boolean isLand() {
        Configuration configuration = Resources.getSystem().getConfiguration();
        return configuration.orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    /**
     * 屏幕尺寸
     *
     * @return
     */
    public static String getScreenSize(Context ctx) {
        StringBuilder screenSize = new StringBuilder();
        try {
            if (ctx == null) {
                return "";
            }
            Display display = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            int screenHeight = metrics.heightPixels;

            screenSize.append(screenWidth).append('x').append(screenHeight);
        } catch (Exception e) {

        }
        return screenSize.toString();
    }

}
