package com.ifeng.newvideo.utils;

import android.text.TextUtils;
import android.util.Log;
import com.ifeng.newvideo.IfengApplication;
import com.xiaomi.mipush.sdk.MiPushClient;

import java.lang.reflect.Method;

/**
 * Created by ll on 2017/9/4.
 */
public class FilterUtil {
    /**
     * 判断当前手机是否为华为手机.
     */
    public static boolean isHuaWeiMobile() {
        String emuiVerion = "";
        Class<?>[] clsArray = new Class<?>[]{String.class};
        Object[] objArray = new Object[]{"ro.build.version.emui"};
        try {
            Class<?> SystemPropertiesClass = Class
                    .forName("android.os.SystemProperties");
            Method get = SystemPropertiesClass.getDeclaredMethod("get",
                    clsArray);
            String version = (String) get.invoke(SystemPropertiesClass,
                    objArray);
            if (!TextUtils.isEmpty(version)) {
                return true;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (LinkageError e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 判断当前手机是否为小米手机.
     */
    public static boolean isXiaoMiMobile() {

        if (IfengApplication.getInstance() == null) {
            return false;
        }
        return MiPushClient.shouldUseMIUIPush(IfengApplication.getInstance());
    }
    /**
     * 判断华为push是否有效
     *     针对bugly 527487
     * @return
     */
    public static boolean isHuaweiPushValid() {
        try {
            Class clazz = Class.forName("com.huawei.android.pushagent.utils.tools.PushServiceTools");
            Method[] methods = clazz.getDeclaredMethods();
            if (methods != null) {
                for (int i=0;i<methods.length;i++) {
                    String methodName = methods[i].getName();
                    if (TextUtils.isEmpty(methodName)) {
                        continue;
                    }
                    if (methodName.equals("broadcastOrStartSelfService")) {
                        Log.d("IfengVideo", "FilterUtils.isHuaweiPushValid return true");
                        return true;
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (LinkageError e) {
            e.printStackTrace();
        }catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("IfengNews", "FilterUtils.isHuaweiPushValid return false");
        return false;
    }
}
