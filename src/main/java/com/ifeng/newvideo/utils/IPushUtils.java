package com.ifeng.newvideo.utils;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.video.core.utils.DistributionInfo;
import com.ifeng.video.dao.db.dao.ConfigDao;
import com.ifeng.video.dao.db.dao.PSPDAO;
import com.ifeng.video.dao.db.dao.SPDAO;
import com.ifeng.video.dao.db.model.ConfigModel;
import com.ifeng.video.dao.db.model.LaunchAppModel;
import com.ifeng.video.dao.db.model.PSPModel;
import com.ifeng.video.dao.db.model.SPModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 推送tag工具类
 * <p>
 * 在后续各端迭代中，计划增加tag值，需求如下
 * 按地域 L:省  L:省_市 L:省_市_县 （覆盖策略）
 * 按厂商 D:apple D:厂商信息里面获取到的值 如没有 则不填写(android)
 * 按渠道 C:渠道号
 * 内容  SP:取searchPath 值后两个ID 例 100409_100417 (如searchPath为3个取后两个，如仅为两个就获取全部，
 * 适用于需要通过获取单条信息拿到播放地址的视频以及在列表中就有播放地址同时也包含searchPath值的数据)
 * <p>
 * 每次播放时入本地库，并累加次数。每次程序激活时，读取数据库，按累计次数倒序排列，当某ID累计次数达到或超过5次（默认）时，
 * 选取该ID值进行上报，并清空已有记录重新计算，推送打开数据不记录在此。当下次达到此标准时，在推送系统替换现有ID值。 （覆盖策略）
 * <p>
 * PSP：取推送数据的searchPath值后两个ID 例 100409_100417  (如searchPath为3个取后两个，如仅为两个就获取全部，
 * 适用于需要通过获取单条信息拿到播放地址的视频) 入本地库 并累加次数 每次程序激活时，读取数据库，按累计次数倒序进行排序，
 * 当某ID累计次数达到或超过5次（默认）时 ，上报该ID值，并清空纪录。当下次达到此标准时，在推送系统替换现有ID值（覆盖策略）
 * <p>
 * SP，PSP 累计次数标准由配置接口确定
 * <p>
 * Created by Pengcheng on 2016/4/22 022.
 */
public class IPushUtils {

    private static final Logger logger = LoggerFactory.getLogger(IPushUtils.class);

    /**
     * 初始化推送相关 这里初始化推送
     * initDebug测试平台
     * init线上平台
     */
    public static void initIPush() {
        if ("true".equals(DistributionInfo.isPushTest)) {
            Ipush.initDebug(IfengApplication.getAppContext(), 1, false);
            logger.info("----------------IPush.initDebug(this, 1, false);----------------");
        } else {
            Ipush.init(IfengApplication.getAppContext(), 1, false);
            logger.info("----------------IPush.init(this, 1, false);---------------------");
        }
    }

    /**
     * 设置IPush Tag
     * 地理位置标签以“L:”或者“JW:”开头前置是位置，后者是经纬度
     * 1、位置，用于定向推送
     * 2、SIM
     * 3、启动时间（启动接口中的时间），用于推送唤醒老用户
     *
     * @param shouldSendSP 只有在获取了配置接口之后才发送PSP  SP，其他情况都不发
     */
    public static void setIPushTag(boolean shouldSendSP) {
        String location = IPushUtils.getPushTagLocation();
        String day = getServerDate();
        String sim = SharePreUtils.getInstance().getSIM();
        String manufacturer = "D:" + Build.MANUFACTURER;
        String publishId = "C:" + PhoneConfig.publishid;
        // TODO 暂时写死7.0.0，为了方便编辑推送，等新PUSH SDK 上线后，可以改为获取版本号
        String ver = "VER:7.0.0";
        // String ver = "VER:" + PhoneConfig.softversion;
        List<String> list = new ArrayList<>();
        if (!TextUtils.isEmpty(location) && !location.equalsIgnoreCase("L:") && !location.equalsIgnoreCase("null|null|null")) {
            list.add(location);
        }
        if (!TextUtils.isEmpty(day)) {
            list.add(day);
        }
        if (!TextUtils.isEmpty(manufacturer)) {
            list.add(manufacturer);
        }
        if (!TextUtils.isEmpty(publishId)) {
            list.add(publishId);
        }
        if (!TextUtils.isEmpty(ver)) {
            list.add(ver);
        }
        if (shouldSendSP) {
            String sp = getSP(IfengApplication.getAppContext());
            String psp = getPSP(IfengApplication.getAppContext());
            if (!TextUtils.isEmpty(sp)) {
                list.add(sp);
            }
            if (!TextUtils.isEmpty(psp)) {
                list.add(psp);
            }
        }
        if (!TextUtils.isEmpty(sim) && !sim.equals("Unknown")) {
            list.add(sim);
        }

        if (!SharePreUtils.getInstance().hasReceivedNewsInstallPush()) {
            list.add(IfengApplication.IFENG_NEWS_SDK_PUBLISH_ID); // 凤凰新闻静默下载完成tag
        }

        String[] tags = list.toArray(new String[list.size()]);
        Ipush.setTags(IfengApplication.getAppContext(), tags);
        logger.debug("IPushTag -> {}", Arrays.toString(tags));
    }

    /**
     * 转换searchPath后，存入或更新数据库
     *
     * @param searchPath 接口中返回的searchPath
     * @param isFromPush 是否来自推送，存入对应数据表
     */
    public static void saveSearchPath(String searchPath, boolean isFromPush) {
        if (!TextUtils.isEmpty(searchPath)) {
            searchPath = handleSearchPath(searchPath);
            if (isFromPush) {
                PSPDAO.getInstance(IfengApplication.getInstance()).insertOrUpdate(new PSPModel(searchPath));
            } else {
                SPDAO.getInstance(IfengApplication.getInstance()).insertOrUpdate(new SPModel(searchPath));
            }
        }
    }

    public static void setPushTagNo(ConfigModel.Fun pushTagSP, ConfigModel.Fun pushTagPSP) {
        String loginTime = PhoneConfig.getLoginTimeForStatistic();
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        int pushTagSPNo = ConfigDao.DEFAULT_PUSH_TAG_SP_NO;
        if (pushTagSP != null) {
            pushTagSPNo = ConfigDao.getFunCode(pushTagSP, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, pushTagSPNo);
        }
        sharePreUtils.setPushTagSPNo(pushTagSPNo);

        int pushTagPSPNo = ConfigDao.DEFAULT_PUSH_TAG_SP_NO;
        if (pushTagPSP != null) {
            pushTagPSPNo = ConfigDao.getFunCode(pushTagPSP, PhoneConfig.publishid, PhoneConfig.softversion, loginTime, pushTagPSPNo);
        }
        sharePreUtils.setPushTagPSPNo(pushTagPSPNo);

        logger.debug("requestConfig onResponse  pushTagSPNo {}, pushTagPSPNo {}", pushTagSPNo, pushTagPSPNo);
    }

    /**
     * 返回后两个，并处理字符串中的"-", "_"
     *
     * @param searchPath 如100376-100409-100412_
     * @return 后两个100409_100412
     */
    private static String handleSearchPath(String searchPath) {
        if (TextUtils.isEmpty(searchPath)) {
            return "";
        }
        String str = searchPath;
        String last = str.substring(str.length() - 1, str.length());
        // 去掉最后一个字符 - _
        if ("-".equalsIgnoreCase(last) || "_".equalsIgnoreCase(last)) {
            str = str.substring(0, str.length() - 1);
        }
        str = str.replaceAll("-", "_");

        String[] a = str.split("_");
        if (a.length >= 3) {
            str = a[a.length - 2] + "_" + a[a.length - 1];
        }
        return str;
    }


    // L:北京L:北京_北京L:北京_北京_朝阳
    private static String getPushTagLocation() {
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String district = sharePreUtils.getDistrict();

        StringBuilder location = new StringBuilder();
        location.append("L:");
        if (!TextUtils.isEmpty(province)) {
            location.append(province);
            province = location.toString();
        }
        if (!TextUtils.isEmpty(city)) {
            location.append("_");
            location.append(city);
            city = location.toString();
        }
        if (!TextUtils.isEmpty(district)) {
            location.append("_");
            location.append(district);
            district = location.toString();
        }
        location = new StringBuilder();
        location.append(province).append('|').append(city).append('|').append(district);

        String result = location.toString();
        return result;
    }

    private static String getSP(Context context) {
        SPDAO spdao = SPDAO.getInstance(context);
        if (spdao == null) {
            return "";
        }
        SPModel spModel = spdao.getMaxSearchPath();
        if (spModel == null) {
            return "";
        }
        logger.debug("IPushTag sp {}", spModel.toString());
        int pushTagSPNo = SharePreUtils.getInstance().getPushTagSPNo();
        logger.debug("IPushTag spNo {}", pushTagSPNo);
        if (spModel.getCount() >= pushTagSPNo) {
            spdao.clear();
            return "SP:" + spModel.getSearchPath();
        }
        return "";
    }

    private static String getPSP(Context context) {
        PSPDAO pspdao = PSPDAO.getInstance(context);
        if (pspdao == null) {
            return "";
        }
        PSPModel pspModel = pspdao.getMaxSearchPath();
        if (pspModel == null) {
            return "";
        }
        logger.debug("IPushTag psp {}", pspModel.toString());
        int pushTagPSPNo = SharePreUtils.getInstance().getPushTagPSPNo();
        logger.debug("IPushTag pspNo {}", pushTagPSPNo);
        if (pspModel.getCount() >= pushTagPSPNo) {
            pspdao.clear();
            return "PSP:" + pspModel.getSearchPath();
        }
        return "";
    }

    private static String getServerDate() {
        Object attribute = IfengApplication.getInstance().getAttribute(LaunchAppModel.LAUNCH_APP_MODEL);
        if (attribute != null) {
            return ((LaunchAppModel) attribute).getDay();
        }
        return "";
    }
}
