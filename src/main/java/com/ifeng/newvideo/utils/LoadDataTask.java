package com.ifeng.newvideo.utils;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import com.ifeng.video.core.utils.BuildUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;

/**
 * Created by Vincent on 2014/10/17.
 */
public class LoadDataTask extends AsyncTask<Object, Integer, Integer> {

    private static final Logger logger = LoggerFactory.getLogger(LoadDataTask.class);

    private final LoadingData loadingData;

    public LoadDataTask(LoadingData loadingData) {
        this.loadingData = loadingData;
    }


    @Override
    protected Integer doInBackground(Object... params) {
        loadingData.loadDbAchieve();
        return null;
    }

    @Override
    protected void onPostExecute(Integer resultCode) {
        super.onPostExecute(resultCode);
        loadingData.result();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void executed() {
        if (BuildUtils.hasHoneycomb()) {
            executeOnExecutor(Executors.newCachedThreadPool());
        } else {
            execute();
        }
    }


    public interface LoadingData {

        void loadDbAchieve();

        void result();
    }


}
