package com.ifeng.newvideo.utils;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by ll on 2017/7/25.
 */
public class RsaUtil {

    /**
     * 签名算法
     */
    public static final String SIGN_ALGORITHMS = "SHA256WithRSA";
    public static final String PUBLIC_RSA = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCv+fFWrHyw2lOsHW9WsJw3/mWfmdPP9MKYbrjb2bPpwOBz+WIcsqCVSNN2UcCaMHXLqfZeOqF8fxT4vSTQqhugluWQIpEFR3DvY9lG1cvg5kgkQZOS4l6m4Bd3ddRSeXIrM9U77O/77PRNPkcPQCU3rWKtLXRp1HrCHp0qYxy93QIDAQAB";
    public static final String SIGN_KEY = "o5yCjP0T7tGTZ5hU";
    /**
     * RSA验签名检查
     * @param content 待签名数据
     * @param sign 签名值
     * @param publicKey 分配给开发商公钥
     * @return 布尔值
     */
    public static boolean doCheck(String content, String sign, String publicKey)
    {
        try
        {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] encodedKey = Base64.decode(publicKey);
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

            java.security.Signature signature = java.security.Signature
                    .getInstance(SIGN_ALGORITHMS);

            signature.initVerify(pubKey);
            signature.update(content.getBytes());

            boolean bverify = signature.verify(Base64.decode(sign));
            return bverify;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

}
