package com.ifeng.newvideo.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.ActivitySplash;
import com.ifeng.video.core.utils.DistributionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by antboyqi on 14-10-18.
 */
public class ShortCutUtil {

    private static final Logger logger = LoggerFactory.getLogger(ShortCutUtil.class);

    /*判断快捷方式*/
    public static boolean hasShortcut(Context ctx) {
        boolean result = false;
        String title = ctx.getString(R.string.app_name);
        final String uriStr;
        final String authority = Util4act.getAuthorityFromPermission(ctx, "com.android.launcher.permission.READ_SETTINGS");
        if (authority == null) {
            logger.warn("该手机的Launcher应用没有内容提供者!authority为空!");
            return false;
        }
        // android系统桌面的基本信息由一个launcher.db的Sqlite数据库管理，里面有三张表 其中一张表就是favorites。
        uriStr = "content://" + authority + "/favorites?notify=true";
        final Uri CONTENT_URI = Uri.parse(uriStr);
        Cursor cursor = null;
        try {
            cursor = ctx.getContentResolver().query(CONTENT_URI, null, "title=?", new String[]{title}, null);
            if (cursor != null && cursor.getCount() > 0) {
                result = true;
            }
            return result;
        } catch (Exception e) {// 有些手机系统没有这个表
            return false;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * 处理快捷方式
     * 1、如当前快捷方式版本与发布包中版本不一致，则创建
     * 2、创建后，保存当前版本号
     */
    public static void handleShortCut(Context ctx) {
        String curVersion = SharePreUtils.getInstance().getShortCutVersion();
        String distributeVersion = DistributionInfo.desk_shortcut_version;
        logger.debug("curVersion = {} , distributeVersion = {}", curVersion, distributeVersion);
        if (!curVersion.equals(distributeVersion)) {
//            if (hasShortcut(ctx)) {
            deleteShortCut(ctx);
//            }
            createShortCut(ctx);
            SharePreUtils.getInstance().setShortCutVersion(distributeVersion);
        }
    }


    /**
     * 创建删除快捷方式
     */
    private static void createShortCut(Context context) {
        Intent shortcutIntent = new Intent();
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        shortcutIntent.setClass(context, ActivitySplash.class);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(context, R.drawable.ic_launcher));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        addIntent.putExtra("duplicate", false);

        context.sendBroadcast(addIntent);
    }

    /**
     * 删除快捷方式
     */
    private static void deleteShortCut(Context context) {
        Intent shortcut = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getString(R.string.app_name));

        Intent intent = new Intent();
        intent.setClass(context, context.getClass());
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intent);

        context.sendBroadcast(shortcut);
    }

}
