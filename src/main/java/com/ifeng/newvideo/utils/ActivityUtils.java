package com.ifeng.newvideo.utils;


import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import com.ifeng.newvideo.IfengApplication;

import java.util.List;

public class ActivityUtils {

    /**
     * 判断当前应用程序处于前台还是后台
     */
    public static boolean isApplicationBroughtToBackground(final Context context) {
        if (null == context) {
            return false;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getClassName().equals(context.getClass().getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 进入系统设置页去勾选“显示通知”。
     */
    public static void openAndroidAppSet(Context context) {
        if (null == context) {
            return;
        }
        try {//打开手机里app通知设置页
            Intent intent = new Intent();
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", context.getPackageName());
            intent.putExtra("app_uid", context.getApplicationInfo().uid);
            context.startActivity(intent);
//            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } catch (Exception e) {//如果失败，打开app设置页
            Intent newIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", IfengApplication.getInstance().getPackageName(), null);
            newIntent.setData(uri);
            context.startActivity(newIntent);
//            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
    }
}
