package com.ifeng.newvideo.utils;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.media.AudioManager;
import android.provider.Settings;
import android.view.*;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnTouchListener;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.videoplayer.widget.skin.BrightView;
import com.ifeng.newvideo.videoplayer.widget.skin.SeekPopupView;
import com.ifeng.newvideo.videoplayer.widget.skin.VolumeView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 播放控手势控制
 *
 * @author dengjiaping
 */
public class GestureControl implements OnTouchListener {
    private Logger logger = LoggerFactory.getLogger(GestureControl.class);

    public Context mContext;
    /**
     * 播放控制层
     **/
    public View mPlayControllerView;


    private GestureDetector mGestureDetector;
    private AudioManager mAudioManager;

    /**
     * 是否允许触摸
     */
    public boolean touchable = false;

    private VolumeView mVolumeView;
    private BrightView mBrightView;
    private SeekPopupView mFastView;


    public void setVolumeView(VolumeView volumeView) {
        this.mVolumeView = volumeView;
    }

    public void setBrightView(BrightView brightView) {
        this.mBrightView = brightView;
    }

    public void setFastView(SeekPopupView seekPopupView) {
        this.mFastView = seekPopupView;
    }

    /**
     * @param mContext
     * @param mPlayControllerView 播放控制层
     */
    public GestureControl(Context mContext, View mPlayControllerView) {
        this.mContext = mContext;
        this.mPlayControllerView = mPlayControllerView;
        init();
    }

    private void init() {
        this.mAudioManager = (AudioManager) this.mContext.getSystemService(Service.AUDIO_SERVICE);
        this.mGestureDetector = new GestureDetector(mContext, mOnGestureListener);
        if (mPlayControllerView != null) {
            mPlayControllerView.setOnTouchListener(this);
        }


    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (mVolumeView != null && mVolumeView.isShown()) {
                    mVolumeView.setVisibility(View.GONE);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_VOLUME_DRAG, null, mVolumeView.getCurPage());
                }
                if (mBrightView != null && mBrightView.isShown()) {
                    mBrightView.setVisibility(View.GONE);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_BRIGHT_DRAG, null, mBrightView.getCurPage());
                }
                if (mFastView != null && mFastView.isShown()) {
                    mFastView.setVisibility(View.GONE);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_GESTURE_DRAG, null, mFastView.getCurPage());
                    mFastView.seekTo();
                }
            case MotionEvent.ACTION_CANCEL:
                if (mVolumeView != null && mVolumeView.isShown()) {
                    mVolumeView.setVisibility(View.GONE);
                }
                if (mBrightView != null && mBrightView.isShown()) {
                    mBrightView.setVisibility(View.GONE);
                }
                if (mFastView != null && mFastView.isShown()) {
                    mFastView.setVisibility(View.GONE);
                    mFastView.seekTo();
                }
                break;

            default:
                break;
        }

        if (!touchable) {
            return false;
        }

        return mGestureDetector.onTouchEvent(event);
    }


    private final OnGestureListener mOnGestureListener = new OnGestureListener() {

        private float mYLastMoveAtPos;
        private float mYMove;

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            mPlayControllerView.performClick();
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (e1 == null || e2 == null) {
                return false;
            }
            boolean volumeShow = mVolumeView != null && mVolumeView.isShown();
            boolean brightShow = mBrightView != null && mBrightView.isShown();
            boolean fastShow = mFastView != null && mFastView.isShown();
            logger.debug("onScroll.............");
            if (Math.abs(distanceX) > Math.abs(distanceY)) {
                if (!volumeShow && !brightShow) {
                    int seekGap = (int) ((e2.getX() - e1.getX()) * 100 / mPlayControllerView.getWidth());
                    if (mFastView != null) {
                        logger.debug("SeekPopupView:is {}", mFastView == null ? "null" : mFastView.toString());
                        mFastView.setPercent(seekGap, seekGap >= 0);
                    }


                }
                return true;
            }

            if (fastShow) {
                return true;
            }

            this.mYMove = e2.getY();
            boolean isAdd = this.mYLastMoveAtPos - this.mYMove > 0;

            int delta = (int) (this.mYLastMoveAtPos - this.mYMove) * 100 / ScreenUtils.getHeight();
            if (ScreenUtils.isInRight((int) e1.getX())) {
                //right
                int lastVolumePercent = getVolume();
                int volumePercent = lastVolumePercent + delta;
                volumePercent = (volumePercent > 100 ? 100 : (volumePercent < 0 ? 0 : volumePercent));

                //距离不足以引起音量变化不记录位置，方便后面累计
                if (lastVolumePercent != volumePercent) {
                    this.mYLastMoveAtPos = mYMove;
                    setVolume(volumePercent, isAdd);
                    mVolumeView.setPercent(volumePercent);
                } else if (volumePercent == 0 || volumePercent == 100) {
                    mVolumeView.setPercent(volumePercent);
                }
            } else {
                //left
                handleBright(delta);
            }


            return true;

        }


        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            mYLastMoveAtPos = e.getY();
            return true;
        }

    };

    //	------------------------音量控制----------------------
    public void setVolume(int percentage, boolean isAdd) {
        if (null == this.mAudioManager) {
            return;
        }
        if (percentage < 0) {
            percentage = 0;
        } else if (percentage >= 100) {
            percentage = 100;
        }

        int maxValue = this.mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volume = this.mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int value = (int) (percentage * maxValue * 1.0 / 100);
        if (isAdd && value <= volume) {//增加音量会出现减退的情况 百分百计算值会省略
            value = volume + 1;
        }
        this.mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, value, 0);
    }

    /**
     * 获取当前音量百分比(0-100)
     */
    public int getVolume() {
        if (null == this.mAudioManager) {
            return 0;
        }
        int volume = this.mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int maxValue = this.mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        return (int) (volume * 100 / maxValue + 0.5f);
    }

//	------------------------音量控制--end--------------------

//	------------------------亮度控制----------------------

    private int mCurrentBrightness = -1;

    /**
     * 设置亮度
     *
     * @param paramInt 取值0-255
     */
    private void setScreenBrightness(int paramInt) {
        this.mCurrentBrightness = paramInt;
        Window localWindow = ((Activity) mContext).getWindow();
        WindowManager.LayoutParams localLayoutParams = localWindow
                .getAttributes();
        float f = paramInt / 255.0F;
        localLayoutParams.screenBrightness = f;
        localWindow.setAttributes(localLayoutParams);
    }

    /**
     * 获取当前亮度(取值0-255)
     */
    private int getScreenBrightness() {
        if (this.mCurrentBrightness != -1) {
            return this.mCurrentBrightness;
        }
        int screenBrightness = 255;
        try {
            screenBrightness = Settings.System.getInt(
                    mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Exception localException) {

        }
        return screenBrightness;


    }


    private void handleBright(int delta) {
        int brightness = getScreenBrightness() * 100 / 255 + delta / 6;
        brightness = (brightness > 100 ? 100 : (brightness < 0 ? 0 : brightness));
        setScreenBrightness(brightness * 255 / 100);
        if (mBrightView != null) {
            mBrightView.setPercent(brightness);
        }

    }

}
