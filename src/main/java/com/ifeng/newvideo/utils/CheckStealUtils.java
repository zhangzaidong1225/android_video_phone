package com.ifeng.newvideo.utils;

import android.text.TextUtils;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * 为判断是否有劫持现象，给出两个测试接口来验证数据的正确
 * Created by android-dev on 2016/11/4 0004.
 */
public class CheckStealUtils {

    private static final Logger logger = LoggerFactory.getLogger(CheckStealUtils.class);

    /**
     * 获取数据接口
     */
    private static final String STEAL_URL_CHECK = "http://api.3g.ifeng.com/android_api?from=ifengvideo";

    /**
     * 上报数据接口
     */
    private static final String STEAL_URL_REPORT = "http://api.3g.ifeng.com/android_params_api?from=ifengvideo";


    public static void checkStealUrl() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                super.run();
                HttpURLConnection checkConnect = null;
                HttpURLConnection dataConnect = null;
                HttpURLConnection sendConnect = null;
                OutputStream sendOut = null;
                int responseCode = -1;
                try {
                    //request for url
                    URL checkUrl = new URL(STEAL_URL_CHECK);
                    checkConnect = (HttpURLConnection) checkUrl.openConnection();
                    checkConnect.setUseCaches(false);
                    checkConnect.connect();
                    InputStream checkIn = checkConnect.getInputStream();
                    responseCode = checkConnect.getResponseCode();
                    String result = new String(getBytesByInputStream(checkIn), "UTF-8");
                    checkConnect.disconnect();
                    checkConnect = null;
                    logger.debug("checkStealUrl data url={}", result);
                    if (responseCode != 200 || TextUtils.isEmpty(result)) {
                        return;
                    }

                    //request for data
                    URL dataUrl = new URL(result);
                    dataConnect = (HttpURLConnection) dataUrl.openConnection();
                    dataConnect.setUseCaches(false);
                    dataConnect.connect();
                    InputStream dataIn = dataConnect.getInputStream();
                    responseCode = dataConnect.getResponseCode();
                    String header = getResponseHeader(dataConnect);
                    String body = new String(getBytesByInputStream(dataIn), "UTF-8");
                    if (body != null && body.length() > 10000) {
                        body = body.substring(0, 10000);
                    }
                    dataConnect.disconnect();
                    dataConnect = null;
                    logger.debug("checkStealUrl body={} \nheader={}", body, header);
                    if (responseCode != 200 || TextUtils.isEmpty(header) || TextUtils.isEmpty(body)) {
                        return;
                    }

                    //send the data
                    URL sendUrl = new URL(STEAL_URL_REPORT);
                    sendConnect = (HttpURLConnection) sendUrl.openConnection();
                    sendConnect.setRequestMethod("POST");
                    sendConnect.setDoOutput(true);
                    sendConnect.setUseCaches(false);
                    sendConnect.connect();
                    sendOut = sendConnect.getOutputStream();
                    String sendData = "header=" + header + "&body=" + body;
                    sendOut.write(sendData.getBytes("UTF-8"));
                    sendOut.flush();
                    sendOut.close();
                    sendOut = null;
                    sendConnect.getInputStream();
                    sendConnect.disconnect();
                    sendConnect = null;

                } catch (Exception e) {
                    logger.error("checkStealUrl error! {}", e);
                } finally {
                    if (checkConnect != null) {
                        checkConnect.disconnect();
                    }
                    if (dataConnect != null) {
                        dataConnect.disconnect();
                    }
                    if (sendConnect != null) {
                        sendConnect.disconnect();
                    }
                }
            }
        };
        thread.start();

    }


    private static byte[] getBytesByInputStream(InputStream is) {
        byte[] bytes = null;
        BufferedInputStream bis = new BufferedInputStream(is);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(baos);
        byte[] buffer = new byte[1024 * 8];
        int length = 0;
        try {
            while ((length = bis.read(buffer)) > 0) {
                bos.write(buffer, 0, length);
            }
            bos.flush();
            bytes = baos.toByteArray();
        } catch (IOException e) {
            logger.error("getBytesByInputStream error! {}", e);
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return bytes;
    }

    private static String getResponseHeader(HttpURLConnection conn) {
        Map<String, List<String>> responseHeaderMap = conn.getHeaderFields();
        return responseHeaderMap.toString();
    }

}
