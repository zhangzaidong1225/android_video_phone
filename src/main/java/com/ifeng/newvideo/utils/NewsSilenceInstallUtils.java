package com.ifeng.newvideo.utils;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import com.ifeng.core.IfengEngine;
import com.ifeng.core.download.SilenceDownload;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.ActionRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 凤凰新闻静默下载、安装工具类
 * Created by android-dev on 2017/7/6 0006.
 */
public class NewsSilenceInstallUtils {

    private static final Logger logger = LoggerFactory.getLogger(NewsSilenceInstallUtils.class);

    /**
     * 凤凰新闻静默下载
     */
    public static void startDownloadIfNeeded() {
        try {
            if (shouldDownload()) {
                IfengEngine.getInstance().performUpload();
                logger.info("start download");
            }
        } catch (Throwable e) {
            logger.error("start download err ", e);
        }
    }

    /**
     * 安装apk
     */
    public static void installApkIfNeeded() {
        try {
            // if (!shouldNotify()) {
            //     return;
            // }
            logger.info("IfengNewsSilence install");
            CustomerStatistics.clickBtnRecord(new ActionRecord(
                    ActionIdConstants.NEWS_SILENCE_INSTALL,
                    ActionIdConstants.ACT_EMPTY, ""));

            IfengEngine.getInstance().installApk();

        } catch (Throwable e) {
            logger.error("IfengNewsSilence install err ", e);
        }
    }

    /**
     * 停止下载
     */
    public static void stopDownload() {
        try {
            if (isDownloadComplete()) {
                return;
            }
            logger.info("IfengNewsSilence stopDownload");
            IfengEngine.getInstance().stopDownload();
        } catch (Throwable e) {
            logger.error("IfengNewsSilence stopDownload err   ", e);
        }
    }

    /**
     * 下载完成的回调
     */
    public static void setDownloadCompleteCallback() {
        try {
            SilenceDownload.DownloadCompleteCallback completeCallback = new SilenceDownload.DownloadCompleteCallback() {
                @Override
                public void downloadComplete() {

                    CustomerStatistics.clickBtnRecord(new ActionRecord(
                            ActionIdConstants.NEWS_SILENCE_DOWNLOAD_COMPLETE,
                            ActionIdConstants.ACT_EMPTY, ""));

                    IPushUtils.setIPushTag(false);

                    logger.info("setDownloadCompleteCallback  downloaded");
                }
            };
            IfengEngine.getInstance().setDownloadCompleteCallback(completeCallback);
        } catch (Throwable e) {
            logger.error("setDownloadCompleteCallback  error !  {}", e);
        }
    }

    public static boolean isDownloadComplete() {
        boolean isDownloadComplete = false;
        try {
            isDownloadComplete = IfengEngine.getInstance().isComplete();
            logger.info("IfengNewsSilence push isDownloadComplete = {}", isDownloadComplete);
        } catch (Throwable e) {
            logger.error("IfengNewsSilence push isDownloadComplete err ", e);
        }
        return isDownloadComplete;
    }

    /**
     * 是否安装
     *
     * @return 未点击过安装新闻的推送 && 未安装过新闻 && 非审核 && 非特殊渠道号 && 已下载完新闻安装包
     */
    public static boolean shouldNotify() {
        return !SharePreUtils.getInstance().hasReceivedNewsInstallPush() &&
                !SharePreUtils.getInstance().hasInstalledNewsEver() &&
                !SharePreUtils.getInstance().isNewsSilenceInstallInReview() &&
                !SharePreUtils.getInstance().isSpecialChannelId() &&
                isDownloadComplete();
    }

    /**
     * 是否下载
     *
     * @return 未收到过安装推送 && 未安装过新闻 && 非审核 && 非特殊渠道号 && 未下载完
     */
    private static boolean shouldDownload() {
        return !SharePreUtils.getInstance().hasReceivedNewsInstallPush() &&
                !SharePreUtils.getInstance().hasInstalledNewsEver() &&
                !SharePreUtils.getInstance().isNewsSilenceInstallInReview() &&
                !SharePreUtils.getInstance().isSpecialChannelId() &&
                !isDownloadComplete();
    }

    /**
     * 是否拒绝过安装
     *
     * @return 调起过安装界面 && 应用列表中没有新闻app
     */
    private static boolean hasRejectInstall() {
        //TODO
        return false;
    }

    public static void checkHasInstalledNews() {
        try {
            new InstalledAppTask().execute();
        } catch (Throwable e) {
            logger.error("IfengNewsSilence checkHasInstalledNews err ", e);
        }
    }

    private static class InstalledAppTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            PackageManager packageManager = IfengApplication.getInstance().getPackageManager();
            List<PackageInfo> packageInfoList = packageManager.getInstalledPackages(0);
            if (packageInfoList == null || packageInfoList.isEmpty()) {
                return false;
            }

            List<String> newsPkgNames = new ArrayList<>(2);
            newsPkgNames.add("com.ifeng.news2");
            newsPkgNames.add("com.ifext.news");

            for (PackageInfo packageInfo : packageInfoList) {
                if (packageInfo != null && newsPkgNames.contains(packageInfo.packageName)) {
                    logger.info("IfengNewsSilence checkHasInstalledNews  true {}", packageInfo.packageName);
                    SharePreUtils.getInstance().setHasInstalledNews(true);
                    break;
                }
            }
            return false;
        }
    }

}
