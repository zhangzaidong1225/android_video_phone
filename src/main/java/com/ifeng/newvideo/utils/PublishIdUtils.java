package com.ifeng.newvideo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.text.TextUtils;
import com.ifeng.video.core.utils.DistributionInfo;
import com.ifeng.video.core.utils.FileUtils;
import com.ifeng.video.dao.db.constants.SharePreConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 发版渠道号工具类
 * Created by Pengcheng on 2015/3/24 024.
 */
class PublishIdUtils {

    private static final Logger logger = LoggerFactory.getLogger(PublishIdUtils.class);

    private static final String PUBLISH_ID_PREFERENCES_KEY = "pi";
    private static final String EXTERNAL_DIR = Environment.getExternalStorageDirectory() + "/ifengvideo/";
    private static final String PI_FILENAME = "pi.cfg";
    private static final String[] ERROR_PUBLIC_IDS = new String[]{"v.ifeng.com", "m.ifeng.com", "m.ifeng.com-video-top",
            "Ifeng-app", "QuickResponse", "Ifeng-video", "m.ifeng.com-other-top", "m.ifeng.com-top", "v.ifeng.com-top",
            "m.ifeng.com-test", "IfengApp", "appStore", "yingyonghui", "hiapk", "goapk", "mumayi", "2024", "tenxun", "gfan", "91sjzs",
            "360.cn", "google_market", "Nduo", "goapk.com", "Dcn", "eoe", "360sjzs", "nduo", "baiduapp", "sikai", "market-liqu",
            "market-baoping", "market-maopao", "market-tompad", "market-kaiqi", "market-baoruan", "market-3G", "market-lenovo",
            "023mi", "SAMSUNGSTORE", "ztestore", "meizu", "jinl", "oppo0802", "haierTV", "CoolpadYL", "duowei", "samsungS3", "asus",
            "tianyi", "hisense", "WO_market", "cmmm", "CQMC", "GZCM", "CQCT", "GDCM", "sjzs", "null", "000 1", "#", "v5.4test",
            "intel", "test"};

    /**
     * 初始化publishId，如果publishId错误则重置为当前渠道
     *
     * @param context
     * @return
     */
    public static String initPublishID(Context context) {
        String publishId = publishIdRead(context);
        for (String id : ERROR_PUBLIC_IDS) {
            if (id.equals(publishId)) {
                clearPublishID(context);
                publishId = publishIdRead(context);
                break;
            }
        }
        return publishId;
    }

    private static void clearPublishID(Context context) {
        File piFile = null;
        if (FileUtils.isExternalStorageMounted()) {
            piFile = new File(EXTERNAL_DIR, PI_FILENAME);
        }
        if (piFile != null && piFile.exists()) {
            piFile.delete();
        }
        piFile = new File(context.getFilesDir(), PI_FILENAME);
        if (piFile.exists()) {
            piFile.delete();
        }
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(PUBLISH_ID_PREFERENCES_KEY, "").commit();
    }

    /**
     * @param context
     * @param publishId
     * @return
     */
    private static File publishIdSave(Context context, String publishId) {
        File piFile = null;
        FileWriter writer = null;

        if (FileUtils.isExternalStorageMounted()) {
            File storageDir = new File(EXTERNAL_DIR);
            piFile = new File(EXTERNAL_DIR, PI_FILENAME);
            if (!storageDir.exists()) storageDir.mkdirs();
            if (!piFile.exists()) {
                try {
                    piFile.createNewFile();
                } catch (IOException e) {
                    logger.error("publishIdSave error! {}", e.getMessage());
                }
            }
        } else {
            try {
                context.openFileOutput(PI_FILENAME, Context.MODE_PRIVATE);
                piFile = new File(context.getFilesDir().getAbsolutePath(), PI_FILENAME);
            } catch (FileNotFoundException e) {
                logger.error("publishIdSave error! {}", e.getMessage());
            }
        }

        try {
            writer = new FileWriter(piFile);
            writer.write(publishId);
            writer.write("\r\n");
        } catch (IOException e) {
            logger.error("publishIdSave error! {}", e.getMessage());
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    logger.error("publishIdSave error! {}", e.getMessage());
                }
            }
        }
        return piFile;
    }

    private static String publishIdRead(Context context) {
        String ret = "";
        File piFile = null;
        SharedPreferences preferences = context.getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        ret = preferences.getString(PUBLISH_ID_PREFERENCES_KEY, "");
        if (TextUtils.isEmpty(ret)) {
            if (FileUtils.isExternalStorageMounted()) {
                piFile = new File(EXTERNAL_DIR, PI_FILENAME);
            } else {
                piFile = new File(context.getFilesDir(), PI_FILENAME);
            }
            if (!piFile.exists()) {
                piFile = publishIdSave(context, DistributionInfo.publish_id);
            }
            FileInputStream fis = null;
            InputStreamReader isr = null;
            BufferedReader bis = null;
            try {
                fis = new FileInputStream(piFile);
                isr = new InputStreamReader(fis);
                bis = new BufferedReader(isr);
                ret = bis.readLine();
                preferences.edit().putString(PUBLISH_ID_PREFERENCES_KEY, ret).commit();
            } catch (Exception e) {
                logger.error("publishIdRead error! {}", e);
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        logger.error("publishIdRead error! {}", e);
                    }
                }
            }
        }
        if (ret == null) {
            ret = "";
        }
        return ret;
    }
}
