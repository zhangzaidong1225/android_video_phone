package com.ifeng.newvideo.utils;

import android.text.TextUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.JsonUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.TabIconConfigDAO;
import com.ifeng.video.dao.db.model.TabIconConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 顶部导航图标、背景更新
 * Created by android-dev on 2017/1/10 0010.
 */
public class TabIconUpdateUtils {

    private static final Logger logger = LoggerFactory.getLogger(TabIconUpdateUtils.class);


    public static TabIconConfigModel getLocalData() {
        boolean tabIconDownloadSuccess = SharePreUtils.getInstance().isTabIconDownloadSuccess();
        logger.debug("getLocalData isTabIconDownloadSuccess = {}", tabIconDownloadSuccess);
        if (!tabIconDownloadSuccess) {
            return null;
        }

        TabIconConfigModel model = null;
        try {
            model = JsonUtils.parseObject(CommonDao.getCache(DataInterface.getTabIconConfigUrl()), TabIconConfigModel.class);
            logger.debug("getLocalData ~~ {}", model != null ? model.toString() : null);
            if (model == null) {
                return null;
            }

            TabIconConfigModel.TarbarBean tarbarBean = model.getTarbar();
            boolean isValidTime = tarbarBean != null && isValidTime(tarbarBean.getBeginTime(), tarbarBean.getEndTime());
            logger.debug("getLocalData isValidTime = {}", isValidTime);
            if (!isValidTime) {
                return null;
            }

        } catch (Exception e) {
            logger.error("getLocalData  {}", e);
        }

        return model;
    }

    /**
     * 判断是否需要更新  起止日期是否正确
     * 下载8张图片
     * 存 色值、起止时间
     */
    public static void updateTabBarIconConfigs() {
        TabIconConfigDAO.getTabIconConfig(
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        boolean isValidResponse = response != null && !TextUtils.isEmpty(response.toString())
                                && ((TabIconConfigModel) response).getTarbar() != null
                                && ((TabIconConfigModel) response).getTarbar().getIconArray() != null;
                        logger.debug("isValidResponse {}  TabIconConfigModel= {}", isValidResponse, isValidResponse ? response.toString() : null);
                        if (!isValidResponse) {
                            return;
                        }
                        TabIconConfigModel model = (TabIconConfigModel) response;
                        if (!isNeedUpdate(model)) {
                            logger.debug("isNeedUpdate false");
                            return;
                        }
                        TabIconConfigModel.TarbarBean tarbarBean = model.getTarbar();
                        if (tarbarBean == null || !isValidTime(tarbarBean.getBeginTime(), tarbarBean.getEndTime())) {
                            logger.debug("isValidTime false");
                            return;
                        }

                        downloadTabIcons(tarbarBean);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SharePreUtils.getInstance().setTabIconDownloadSuccess(false);
                        logger.error("updateTabBarIconConfigs onErrorResponse  {}", error);
                    }
                });
    }

    /**
     * 改同一个SP值，用的时候判断该值
     *
     * @param tarbarBean
     */
    private static void downloadTabIcons(TabIconConfigModel.TarbarBean tarbarBean) {
        try {
            TabIconConfigModel.TarbarBean.IconArrayBean iconArray = tarbarBean.getIconArray();
            List<String> urlList = new ArrayList<>();
            urlList.add(iconArray.getHomeIcon().getNormal3x());
            urlList.add(iconArray.getHomeIcon().getSelected3x());
            urlList.add(iconArray.getLiveIcon().getNormal3x());
            urlList.add(iconArray.getLiveIcon().getSelected3x());
            urlList.add(iconArray.getSubIcon().getNormal3x());
            urlList.add(iconArray.getSubIcon().getSelected3x());
            urlList.add(iconArray.getMyIcon().getNormal3x());
            urlList.add(iconArray.getMyIcon().getSelected3x());

            for (final String url : urlList) {
                VolleyHelper.getImageLoader().get(url, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        boolean isSuccess = response != null && response.getBitmap() != null;
                        SharePreUtils.getInstance().setTabIconDownloadSuccess(isSuccess);
                        logger.debug("tabIcon onResponse {}   {}", isSuccess, url);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SharePreUtils.getInstance().setTabIconDownloadSuccess(false);
                        logger.error("tabIcon onErrorResponse failed  {} , {}", url, error);
                    }
                });
            }
        } catch (Exception e) {
            logger.error("downloadTabIcons failed  {}", e);
            SharePreUtils.getInstance().setTabIconDownloadSuccess(false);
        }
    }

    private static boolean isValidTime(String beginTime, String endTime) {
        if (TextUtils.isEmpty(beginTime) || TextUtils.isEmpty(endTime)) {
            return false;
        }
        try {
            // 服务端校准后的时间
            Date curDate = DateUtils.hhmmssFormat_One.parse(DateUtils.hhmmssFormat_One.format(new Date(LiveUtils.getCurrentTime())));

            Date beginDate = DateUtils.hhmmssFormat_One.parse(beginTime);
            Date endDate = DateUtils.hhmmssFormat_One.parse(endTime);

            return curDate.after(beginDate) && curDate.before(endDate);
        } catch (ParseException e) {
            logger.error("isValidTime error  {}", e);
        }
        return false;
    }

    private static boolean isNeedUpdate(TabIconConfigModel model) {
        return model != null && "1".equals(model.getIsTabUpdate());
    }
}
