package com.ifeng.newvideo.utils;

import android.util.Log;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.video.upgrade.VersionUtils;
import com.ifeng.video.upgrade.grayupgrade.GrayDownload;
import com.ifeng.video.upgrade.grayupgrade.GrayUpgradeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 灰度升级
 * Created by android-dev on 2017/7/6 0006.
 */
public class GrayInstallUtils {

    private static final Logger logger = LoggerFactory.getLogger(GrayInstallUtils.class);

    /**
     * 凤凰新闻静默下载
     */
    public static void startDownloadIfNeeded() {
        Log.d("gray", "start download----");

        try {
            if (shouldDownload()) {
                GrayUpgradeUtils.getInstance().performUpgrade();
                logger.info("gray ----- start download");
                Log.d("gray", "start download ---0000");
            } else {
                Log.d("gray", "no need download----1");
            }

        } catch (Throwable e) {
            logger.error("gray ----- start download err ", e);
        }
    }

    /**
     * 安装apk
     */
    public static void installApkIfNeeded() {
        try {
            logger.info("gray ----- grayUpgrade install");
            GrayUpgradeUtils.getInstance().installApk();
        } catch (Throwable e) {
            logger.error("gray ----- grayUpgrade install err ", e);
        }
    }

    /**
     * 停止下载
     */
    public static void stopDownload() {
        try {
            logger.info("gray ----- grayUpgrade stopDownload");
            Log.d("gray", "grayUpgrade stopDownload");
            if (isDownloadComplete()) {
                return;
            }
            GrayUpgradeUtils.getInstance().stopDownload();
        } catch (Throwable e) {
            logger.error("gray ----- grayUpgrade stopDownload err   ", e);
            Log.d("gray", "grayUpgrade stopDownload err");

        }
    }

    /**
     * 下载完成的回调
     */
    public static void setDownloadCompleteCallback() {
        try {

            GrayDownload.DownloadCompleteCallback completeCallback = new GrayDownload.DownloadCompleteCallback() {
                @Override
                public void downloadComplete() {
                    logger.info("gray ----- setDownloadCompleteCallback  downloaded");
                    Log.d("gray", "setDownloadCompleteCallback  downloaded");
                }
            };
            GrayUpgradeUtils.getInstance().setDownloadCompleteCallback(completeCallback);
        } catch (Throwable e) {
            logger.error("gray ----- setDownloadCompleteCallback  error !  {}", e);
            Log.d("gray", "setDownloadCompleteCallback  error !  {}" + e.getMessage());
        }
    }

    private static boolean shouldDownload() {
        return VersionUtils.getIsCanGrayUpgrade(IfengApplication.getAppContext()) &&
                !isDownloadComplete();
    }

    public static boolean isDownloadComplete() {
        boolean isDownloadComplete = false;
        try {
            isDownloadComplete = GrayUpgradeUtils.getInstance().isComplete();
            logger.info("gray ----- grayUpgrade  isDownloadComplete = {}", isDownloadComplete);
        } catch (Throwable e) {
            logger.error("gray ----- grayUpgrade  isDownloadComplete err ", e);
        }
        return isDownloadComplete;
    }

}
