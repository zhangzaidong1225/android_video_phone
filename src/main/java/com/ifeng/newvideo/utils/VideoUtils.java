package com.ifeng.newvideo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 视频工具类，如从本地视频文件中获取音频文件等
 * Created by Pengcheng on 2014/11/7 007.
 */
public class VideoUtils {

    private static final Logger logger = LoggerFactory.getLogger(VideoUtils.class);

    /**
     * 通过视频文件获得音频文件，支持自定义文件名和后缀
     *
     * @param videoPath 视频的完整输出路径，包括文件名和后缀，如/mvn/sdcard/Android/data/com.ifeng.newvideo/files/audio/aaa.ifeng
     * @param audioPath 音频的完整输出路径，包括文件名和后缀，如/mvn/sdcard/Android/data/com.ifeng.newvideo/files/audio/aaa.ifeng
     * @return String，返回常量字符串success等
     */
    public native static String getAudioViaVideo(String videoPath, String audioPath);

    static {
        try {
            // 加载本地so库文件，注意顺序
            System.loadLibrary("avutil-52");
            System.loadLibrary("avcodec-55");
            System.loadLibrary("avformat-55");
            System.loadLibrary("swscale-2");
            System.loadLibrary("ffcopy");
        } catch (Exception e) {
            logger.error("loadLibrary error \n {}", e.toString());
        }
    }

}
