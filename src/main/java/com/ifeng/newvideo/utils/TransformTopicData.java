package com.ifeng.newvideo.utils;


import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.dao.ConfigDao;
import com.ifeng.video.dao.db.model.CMPPSubTopicModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.db.model.SubAudioFMListInfo;
import com.ifeng.video.dao.db.model.SubChannelInfoModel;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 其他bean类转PlayerInfoModel
 * Created by cuihz on 2014/9/12.
 */
public class TransformTopicData {

    /**
     * CMPP单条专题转PlayerInfoModel
     *
     * @param detail
     * @return
     */
    public static PlayerInfoModel topicDetailToPlayerInfoModel(CMPPSubTopicModel.SubTopicList.TopicDetail detail) {
        if (detail == null || detail.memberItem == null) {
            return null;
        }
        PlayerInfoModel playerInfoModel = new PlayerInfoModel();
        CMPPSubTopicModel.SubTopicList.TopicDetail.MemberItem memberItem = detail.memberItem;
//        playerInfoModel.setStage_url_photo(detail.thumbImageUrl);
        playerInfoModel.setGuid(memberItem.guid);
        playerInfoModel.setName(detail.title);
        playerInfoModel.setType(detail.memberType);
        playerInfoModel.setCreateDate(memberItem.createDate);
        playerInfoModel.setDuration(memberItem.duration);
        playerInfoModel.setCpName(memberItem.cpName);
        playerInfoModel.setSearchPath(memberItem.searchPath);
        //playerInfoModel.setStatisticID(memberItem.statisticID);
        if (memberItem.files != null && !memberItem.files.isEmpty()) {
            for (CMPPSubTopicModel.SubTopicList.TopicDetail.MemberItem.VideoFile videoFile : memberItem.files) {
                if (videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_TYPE_AUDIO)) {
                    playerInfoModel.audio_filesize = videoFile.filesize;
                    playerInfoModel.audiourl = videoFile.mediaUrl;
                } else if (videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_LOW)) {
                    playerInfoModel.media_low_filesize = videoFile.filesize;
                    playerInfoModel.media_url_low = videoFile.mediaUrl;
                } else if (videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_HIGH)) {
                    playerInfoModel.media_high_filesize = videoFile.filesize;
                    playerInfoModel.media_url_high = videoFile.mediaUrl;
                } else if (videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_MIDDLE)) {
                    playerInfoModel.media_url_middle = videoFile.mediaUrl;
                    playerInfoModel.media_middle_filesize = videoFile.filesize;
                } else if (videoFile.useType == Integer.parseInt(MediaConstants.POSTER_BIG)) {
                    playerInfoModel.poster_big_filesize = videoFile.filesize;
                    playerInfoModel.poster_url_big = videoFile.mediaUrl;
                } else if (videoFile.useType == Integer.parseInt(MediaConstants.POSTER_SMALL)) {
                    playerInfoModel.poster_small_filesize = videoFile.filesize;
                    playerInfoModel.poster_url_small = videoFile.mediaUrl;
                } else if (videoFile.useType == Integer.parseInt(MediaConstants.STAGE_PHOTO)) {
                    playerInfoModel.stage_photo_filesize = videoFile.filesize;
                    playerInfoModel.stage_url_photo = videoFile.mediaUrl;
                }
                // 特殊处理start ，加入配置接口中H265选项,-1取103 ；0取548；1取549
                else if (PlayerInfoModel.CLARITY_S == ConfigDao.DEFAULT_CLARITY_S_H264
                        && videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_SUPPER)) {
                    playerInfoModel.media_supper_filesize = videoFile.filesize;
                    playerInfoModel.media_url_supper = videoFile.mediaUrl;
                } else if (PlayerInfoModel.CLARITY_S == ConfigDao.DEFAULT_CLARITY_S_H265_0
                        && videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_SUPPER_H265_548)) {
                    playerInfoModel.media_supper_filesize = videoFile.filesize;
                    playerInfoModel.media_url_supper = videoFile.mediaUrl;
                } else if (PlayerInfoModel.CLARITY_S == ConfigDao.DEFAULT_CLARITY_S_H265_1
                        && videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_SUPPER_H265_549)) {
                    playerInfoModel.media_supper_filesize = videoFile.filesize;
                    playerInfoModel.media_url_supper = videoFile.mediaUrl;
                }
                // 特殊处理end
                else if (videoFile.useType == Integer.parseInt(MediaConstants.MEDIA_ORIGINAL)) {
                    playerInfoModel.media_original_filesize = videoFile.filesize;
                    playerInfoModel.media_url_original = videoFile.mediaUrl;
                }
            }
        }
        return playerInfoModel;
    }

    /**
     * 单条FM音频转PlayerInfoModel
     *
     * @param detail
     * @return
     */
    public static PlayerInfoModel audioFMToPlayerInfoModel(SubAudioFMListInfo.AudioData.AudioItem detail) {
        if (detail == null || ListUtils.isEmpty(detail.audiolist)) {
            return null;
        }
        PlayerInfoModel playerInfoModel = new PlayerInfoModel();
        playerInfoModel.audiourl = detail.audiolist.get(0).filePath;
        //播放或者下载地址,默认为高清码流
        playerInfoModel.media_url_high = detail.audiolist.get(0).filePath;
        //文件大小,默认为高清码流
        playerInfoModel.media_high_filesize = Integer.parseInt(detail.audiolist.get(0).getSize());
        playerInfoModel.setType(IfengType.TYPE_FM);
        playerInfoModel.stage_url_photo = detail.getImg370_370();
        playerInfoModel.poster_url_big = detail.getImg370_370();
        playerInfoModel.title = detail.getTitle();
        playerInfoModel.guid = detail.id;
        playerInfoModel.columnName = detail.programName;
        playerInfoModel.id = detail.programId;
        playerInfoModel.setDuration(Integer.parseInt(detail.getDuration()));
        playerInfoModel.setCreateDate(generateTime(Long.parseLong(detail.getCreateTime())));
        return playerInfoModel;
    }

    private static String generateTime(long position) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(new Date(position * 1000));
    }

    public static SubChannelInfoModel.MemberItem extendDataToMemberItem(CMPPSubTopicModel.ExtendData extendData) {
        SubChannelInfoModel.MemberItem memberItem = new SubChannelInfoModel.MemberItem();
        memberItem.setClickType(extendData.linkType);
        memberItem.setClickUrl(extendData.url);
        memberItem.setTitle(extendData.extTitle);
        return memberItem;
    }

}
