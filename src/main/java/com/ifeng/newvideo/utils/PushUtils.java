package com.ifeng.newvideo.utils;

import android.text.TextUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.push.DealClientIdOfPush;
import com.ifeng.newvideo.push.HuaWeiPushReceiver;
import com.ifeng.newvideo.push.impl.HWPushImpl;
import com.ifeng.newvideo.push.impl.IpushImpl;
import com.ifeng.newvideo.push.impl.MiPushImpl;
import com.ifeng.newvideo.push.impl.PushBridge;

/**
 * Created by ll on 2017/9/4.
 */
public class PushUtils {
    public static final String PUSH_RESOURCE = "push_resource";

    public static final String PUSH_MESSAGE_TYPE = "push_message_type";
    /**
     * 对于第三方推送，TYPE_MESSAGE_NOTIFY标识第三方SDK通知栏的消息；
     * TYPE_MESSAGE_PASS_THROUGH标识透传的消息。
     */
    public static final String TYPE_MESSAGE_NOTIFY = "message_push_type_notify";

    public static final String TYPE_MESSAGE_PASS_THROUGH = "message_push_type_through";

    /** 记录在本地的，使用华为推送还是ipush的参数名*/
    public final static String USE_HUAWEI_OR_IPUSH_FLAG = "use_huawei_or_ipush_flag";
    /** 使用华为push */
    public final static String USE_HUAWEI_PUSH = "0";
    /** 使用ipush */
    public final static String USE_IPUSH = "1";
    /** 关闭华为push和ipush */
    public final static String CLOSE_HUAWEIPUSH_IPUSH = "2";

    public static final int RESOURCE_IFENG = 1;
    public static final int RESOURCE_HUAWEI = 2;
    public static final int RESOURCE_XIAOMI = 3;

    private static PushBridge sPushBridge;

    /**
     * Bundle Extra
     */
    public static final String PUSH_EXTRA_BUNDLE = "extra.com.ifeng.video.push.bundle";

    public static final String WAKE_UP_MESSAGE_ACTION = "action.com.ifeng.video.wakeup.message";
    public static final String IPUSH_MESSAGE_ACTION = "action.com.ifeng.video.push.IPUSH_MESSAGE";
    public static final String ATTRIBUTE_ACTIVATED = "activated";

    public static final String KEY_PUSH_MSG = "Msg";

    static {

        if (IfengApplication.getInstance() != null) {

            if (FilterUtil.isXiaoMiMobile()) {  //小米手机启动小米push
                sPushBridge = new MiPushImpl();
            } else {
                if (FilterUtil.isHuaWeiMobile() && FilterUtil.isHuaweiPushValid()) {  //华为手机根据情况启动
                    String whichPush = "0";
                    switch (whichPush) {
                        case USE_HUAWEI_PUSH:
                            sPushBridge = new HWPushImpl();
                            SharePreUtils.setStringByName(IfengApplication.getInstance(), PushUtils.USE_HUAWEI_OR_IPUSH_FLAG, USE_HUAWEI_PUSH);
                            break;
                        case USE_IPUSH:
                        default:
                            sPushBridge = new IpushImpl();
                            SharePreUtils.setStringByName(IfengApplication.getInstance(), PushUtils.USE_HUAWEI_OR_IPUSH_FLAG, USE_IPUSH);
                            break;
                    }
                } else {  //其他情况启动ipush
                    sPushBridge = new IpushImpl();
                    SharePreUtils.setStringByName(IfengApplication.getInstance(), PushUtils.USE_HUAWEI_OR_IPUSH_FLAG, USE_IPUSH);
                }
            }
        }
    }

    public static void initPush() {
        if (IfengApplication.getInstance() == null || sPushBridge == null) {

            return;
        }
        // step 1. init.
        sPushBridge.initPush();

        if (sPushBridge == null) {
            return;
        }

        // step 2. open or close push.
        boolean isSysAppNotiSetOpened = SharePreUtils.getInstance().getPushMessageState();
        if (isSysAppNotiSetOpened) {
            sPushBridge.startOrResumePush();
        } else {
            sPushBridge.pausePush();
        }
    }

    /**
     * open and subscribe xiaomi's push receiver.
     */
    public static void openPush() {
        if (IfengApplication.getInstance() == null || sPushBridge == null) {
            return;
        }
        sPushBridge.startOrResumePush();

        if (FilterUtil.isHuaWeiMobile()) {
            new DealClientIdOfPush(HuaWeiPushReceiver.PLATFORM).reOpenPush(IfengApplication.getInstance());
        }
    }

    /**
     * close and unsubscribe xiaomi's push. when it sets, client will not
     * receive xiaomi's push message.
     */
    public static void closePush() {

        if (IfengApplication.getInstance() == null || sPushBridge == null) {
            return;
        }
        sPushBridge.pausePush();
    }

    public static boolean directOpenPush(String type, String nMessageType) {
        return !TextUtils.isEmpty(type) && TYPE_MESSAGE_NOTIFY.equals(nMessageType);
    }
}
