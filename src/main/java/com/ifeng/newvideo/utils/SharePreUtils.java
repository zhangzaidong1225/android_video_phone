package com.ifeng.newvideo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.constants.PageRefreshConstants;
import com.ifeng.newvideo.setting.value.SettingConfig;
import com.ifeng.newvideo.statistics.P2PStatisticRatioUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.constants.SharePreConstants;
import com.ifeng.video.dao.db.dao.ConfigDao;
import com.ifeng.video.dao.db.model.PhoneModel;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * SharePreference工具
 */
public class SharePreUtils {

    private final SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static volatile SharePreUtils sharePreUtils = null;
    private final static String UPLOAD_DATE = "upload_date";
    private final static String SERVER_TIME = "Server_Time";
    private final static String UNICOM_CANCEL_ORDER_DATE = "unicom_Cancel_Order_Date";
    private final static String Channel_DATA_LAST_TIMESTAMP = "channel_data_last_timestamp";
    private final static String CANCEL_INSTALL_NEWS_TIME = "cancel_install_news_time";
    static final String PREFERENCES_FIRST_INSTALL_VERSION_NAME = "install_softversion";
    static final String PREFERENCES_LAST_VERSION_CODE = "last_versioncode";
    private static final String SIG_FOR_NEWS = "sigForNews";

    private static final String TAB_ICONS_DOWNLOAD_IS_SUCCESS = "is_tab_icons_download_success";
    private static final String TAB_ICONS_TOP_DIVIDER_COLOR = "tab_icons_top_divider_color";
    private static final String TAB_ICONS_BACKGROUND_COLOR = "tab_icons_top_background_color";
    private static final String TAB_ICONS_BEGIN_TIME = "tab_icons_begin_time";
    private static final String TAB_ICONS_END_TIME = "tab_icons_end_time";
    private static Map<String, Integer> dingMap = new HashMap<>();
    private static Map<String, Boolean> visibleMap = new HashMap<>(); //频道是否可见


    private SharePreUtils() {
        sharedPreferences = IfengApplication.getInstance().getSharedPreferences(SharePreConstants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static SharePreUtils getInstance() {
        if (sharePreUtils == null) {
            synchronized (SharePreUtils.class) {
                if (sharePreUtils == null) {
                    sharePreUtils = new SharePreUtils();
                }
            }
        }
        return sharePreUtils;
    }

    /**
     * 设置服务器时间
     */
    public void setServerTime(long serverTime) {
        editor = sharedPreferences.edit();
        editor.putLong(SERVER_TIME, serverTime);
        editor.commit();
    }

    /**
     * 获取服务器时间
     */
    public long getServerTime() {
        return sharedPreferences.getLong(SERVER_TIME, System.currentTimeMillis());
    }

    /**
     * 设置上传数据时间(每天一次)
     */
    public void setUploadDate(long uploadDate) {
        editor = sharedPreferences.edit();
        editor.putLong(UPLOAD_DATE, uploadDate);
        editor.commit();
    }

    /**
     * 获取上传数据时间(每天一次)
     */
    public Long getUploadDate() {
        return sharedPreferences.getLong(UPLOAD_DATE, 0);
    }

    /**
     * 记录页面刷新的时间
     *
     * @param pageCategory    频道类型
     * @param pageId          页面的位置
     * @param lastRefreshTime 刷新的时间记录
     */
    public void setPageData(String pageCategory, int pageId, long lastRefreshTime) {
        editor = sharedPreferences.edit();
        String pageKey = pageCategory + PageRefreshConstants.SEPARATOR + pageId;
        editor.putLong(pageKey, lastRefreshTime);
        editor.commit();
    }

    /**
     * 获取页面刷新刷新时间
     *
     * @param pageCategory 频道类型
     * @param pageId       页面的位置
     * @return
     */
    public Long getPageData(String pageCategory, int pageId) {
        String pageKey = pageCategory + PageRefreshConstants.SEPARATOR + pageId;
        return sharedPreferences.getLong(pageKey, 0);
    }

    /**
     * 保存用户登录账号
     *
     * @param userName
     */
    public void setUserAccount(String userName) {
        editor = sharedPreferences.edit();
        editor.putString(IntentKey.LOGIN_USERACCOUNT, userName);
        editor.commit();
    }

    /**
     * 获取用户登录账号信息
     *
     * @return
     */
    public String getUserAccount() {
        return sharedPreferences.getString(IntentKey.LOGIN_USERACCOUNT, "");
    }

    /**
     * 设置运营商网络状态
     * true 为打开状态 false为关闭状态
     *
     * @param netState
     */
    public void setNetState(boolean netState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SettingConfig.WIFI_CONDITION, netState);
        editor.commit();
    }

    /**
     * 获得运营商网络状态
     * true 为打开状态 false为关闭状态
     *
     * @return
     */
    public boolean getNetState() {
        return sharedPreferences.getBoolean(SettingConfig.WIFI_CONDITION, true);
    }

    /**
     * 设置“允运营商网络缓存” 开关的状态
     *
     * @param cacheVideoState true 为打开状态 false为关闭状态
     */
    public void setCacheVideoState(boolean cacheVideoState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SettingConfig.DOWNLOAD_VIDEO_ONLY_WIFI, cacheVideoState);
        editor.commit();
    }

    /**
     * 获取“允运营商网络缓存” 开关的状态
     *
     * @return true 为打开状态 false为关闭状态
     */
    public boolean getCacheVideoState() {
        return sharedPreferences.getBoolean(SettingConfig.DOWNLOAD_VIDEO_ONLY_WIFI, false);
    }

    /**
     * 设置消息推送状态
     * true 为打开状态 false为关闭状态
     *
     * @param pushMessageState
     */
    public void setPushMessageState(boolean pushMessageState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SettingConfig.PUSH_ON, pushMessageState);
        editor.commit();
    }

    /**
     * 设置画中画状态
     * true 为打开状态 false为关闭状态
     *
     * @param mode
     */
    public void setPipModeState(boolean mode) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SettingConfig.PIP_ON, mode);
        editor.commit();
    }

    /**
     * 设置定时关闭音频
     *
     * @param netState
     */
    public void setAudioPause(boolean netState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SettingConfig.PAUSE_AUDIO, netState);
        editor.commit();
    }

    /**
     * 获得定时关闭音频
     */
    public boolean getAudioPause() {
        return sharedPreferences.getBoolean(SettingConfig.PAUSE_AUDIO, false);
    }

    /**
     * 获取消息推送状态
     * true 为打开状态 false为关闭状态
     */
    public boolean getPushMessageState() {
        return sharedPreferences.getBoolean(SettingConfig.PUSH_ON, true);
    }

    /**
     * 获取画中画状态
     * true 为打开状态 false为关闭状态
     */
    public boolean getPiPModeState() {
        return sharedPreferences.getBoolean(SettingConfig.PIP_ON, true);
    }

    /**
     * 设置DLNA的状态
     * true 为打开状态 false为关闭状态
     *
     * @param dlnaState
     */
    public void setDLNAState(boolean dlnaState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SettingConfig.DLNA_ON, dlnaState);
        editor.commit();
    }

    /**
     * 获取应用启动次数
     * true 为打开状态 false为关闭状态
     */
    public int getStartTime() {
        return sharedPreferences.getInt(IntentKey.APP_START_TIME, 0);
    }

    /**
     * 设置应用启动次数
     * true 为打开状态 false为关闭状态
     *
     * @param time
     */
    public void setStartTime(int time) {
        editor = sharedPreferences.edit();
        editor.putInt(IntentKey.APP_START_TIME, time);
        editor.commit();
    }

    /**
     * 获取DLNA的状态
     * true 为打开状态 false为关闭状态
     */
    public boolean getDLNAState() {
        return sharedPreferences.getBoolean(SettingConfig.DLNA_ON, true);
    }

    /**
     * 设置视频默认的清晰度
     *
     * @param currentStream 如：MediaConstants.STREAM_HIGH
     */
    public void setVodCurrentStream(int currentStream) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.VIDEO_DEFINITION, currentStream);
        editor.commit();
        Log.d("streamValue:", String.valueOf(currentStream));
    }

    /**
     * 获取点播默认播放清晰度
     */
    public int getVodCurrentStream() {
        return sharedPreferences.getInt(SharePreConstants.VIDEO_DEFINITION, ConfigDao.DEFAULT_BITRATE);
    }

    public void setLiveCurrentStream(int currentStream) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.LIVE_STREAM, currentStream);
        editor.commit();
    }

    /**
     * 直播默认播放清晰度
     */
    public int getLiveCurrentStream() {
        return sharedPreferences.getInt(SharePreConstants.LIVE_STREAM, MediaConstants.STREAM_AUTO);
    }

    /**
     * 设置首页指导页的显示状态
     *
     * @param Show
     */
    public void setHomeGuideGestureState(boolean Show) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.SHOULD_SHOW_HOME_GUID_GESTURE_HINT, Show);
        editor.commit();
    }

    /**
     * 获取首页指导页的显示状态
     *
     * @return
     */
    public boolean getHomeGuideGestureState() {
        return sharedPreferences.getBoolean(SharePreConstants.SHOULD_SHOW_HOME_GUID_GESTURE_HINT, false);
    }

    /**
     * 设置点播播放指导页的显示状态
     */
    public void setVodPlayGestureState(boolean Show) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.VIDEO_PLAY_GESTURE_HINT, Show);
        editor.commit();
    }

    /**
     * 获取点播播放指导页的显示状态
     */
    public boolean getVodPlayGestureState() {
        return sharedPreferences.getBoolean(SharePreConstants.VIDEO_PLAY_GESTURE_HINT, true);
    }

    /**
     * 设置直播播放指导页的显示状态
     */
    public void setLivePlayGestureState(boolean Show) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.LIVE_PLAY_GESTURE_HINT, Show);
        editor.commit();
    }

    /**
     * 获取直播播放指导页的显示状态
     */
    public boolean getLivePlayGestureState() {
        return sharedPreferences.getBoolean(SharePreConstants.LIVE_PLAY_GESTURE_HINT, true);
    }

    /**
     * 设置默认播放模式
     * true为音频 false为视频
     *
     * @param isAudioMode
     */
    public void setDefaultPlayMode(boolean isAudioMode) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.PLAY_AUDIO_TYPE, isAudioMode);
        editor.commit();
    }

    /**
     * 获取默认播放模式
     * true为音频 false为视频
     */
    public boolean getDefaultPlayMode() {
        // 6.14.0 设置去掉默认音视频
        return false;
//        return sharedPreferences.getBoolean(SharePreConstants.PLAY_AUDIO_TYPE, false);
    }

    /**
     * 设置App是否要更新的状态
     * true为有更新 false没有更新
     *
     * @param updateState
     */
    public void setHasUpdateState(boolean updateState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.UPDATE_AVALIABLE, updateState);
        editor.commit();
    }

    /**
     * 获取App是否更新的状态
     * true为有更新 false没有更新
     *
     * @return
     */
    public boolean getHasUpdateState() {
        return sharedPreferences.getBoolean(SharePreConstants.UPDATE_AVALIABLE, false);
    }

    /**
     * 设置缓存路径
     * true 为SD卡 false为手机内存
     *
     * @param cachePathState
     */
    public void setCachePathState(boolean cachePathState) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.IF_VIDEO_CACHE_SDCARD, cachePathState);
        editor.commit();
    }

    /**
     * 获取缓存路径
     * true 为SD卡 false为手机内存  默认设置为false，保证第一次无SD卡缓存时可显示手机内存
     *
     * @return
     */
    public boolean getCachePathState() {
        return sharedPreferences.getBoolean(SharePreConstants.IF_VIDEO_CACHE_SDCARD, false);
    }

    /**
     * 设置签到手机号码
     *
     * @param no
     */
    public void setSignInNo(String no) {
        editor = sharedPreferences.edit();
        editor.putString(SharePreConstants.SIGN_IN_MOBILE, no);
        editor.commit();
    }

    /**
     * 获取签到手机号码
     *
     * @return
     */
    public String getSignInNo() {
        return sharedPreferences.getString(SharePreConstants.SIGN_IN_MOBILE, "");
    }

    /**
     * 添加签到日
     *
     * @param month
     * @param day
     */
    public void addSignDay(String month, String day) {
        editor = sharedPreferences.edit();
        String beforemonth = sharedPreferences.getString(SharePreConstants.SIGN_MONTH, "");
        if (!beforemonth.equals(month)) {//如果月份不一样，或者还没有初始化，覆盖天数记录和月份
            editor.putString(SharePreConstants.SIGN_DAY, day + "日");
            editor.putString(SharePreConstants.SIGN_MONTH, month);
        } else {//叠加天数
            String beforeday = getSignDay();
            if (beforeday.contains(day + "日")) {//表明已经签到过
                return;
            } else {
                day = beforeday + day + "日";
                editor.putString(SharePreConstants.SIGN_DAY, day);
            }

        }
        editor.commit();
    }

    /**
     * 获取签到日
     *
     * @return
     */
    private String getSignDay() {
        return sharedPreferences.getString(SharePreConstants.SIGN_DAY, "");
    }

    /**
     * 返回日期数组
     *
     * @return
     */
    public String[] getSignDayArr() {
        ArrayList<String> arr = new ArrayList<String>();
        String data = getSignDay();
        return data.split("日");
    }

    /**
     * 判断当天是否签到，顺便检测一下是否到了下个月
     */
    public boolean hasSigned(String month, String day) {
        editor = sharedPreferences.edit();
        String beforemonth = sharedPreferences.getString(SharePreConstants.SIGN_MONTH, "");
        if (!beforemonth.equals(month)) {//如果月份不一样，或者还没有初始化，覆盖月份,初始化day
            editor.putString(SharePreConstants.SIGN_MONTH, month);
            editor.putString(SharePreConstants.SIGN_DAY, "");
            editor.commit();
        }
        String days = getSignDay();
        return days.contains(day);
    }

    /**
     * 获取首页频道更新
     */
    public String getChannelUpdate() {
        return sharedPreferences.getString(SharePreConstants.CHANNEL_UPDATE, null);
    }

    /**
     * 设置首页频道更新
     */
    public void setChannelUpdate(String channelUpdate) {
        editor = sharedPreferences.edit();
        editor.putString(SharePreConstants.CHANNEL_UPDATE, channelUpdate);
        editor.apply();
    }

    /**
     * 获取审核状态
     */
    public String getInreview() {
        return sharedPreferences.getString(SharePreConstants.INREVIEW, "");
    }

    /**
     * 设置审核状态
     */
    public void setInreview(String inreview) {
        editor = sharedPreferences.edit();
        editor.putString(SharePreConstants.INREVIEW, inreview);
        editor.commit();
    }

    /**
     * 获取快捷方式版本号
     */
    public String getShortCutVersion() {
        return sharedPreferences.getString(SharePreConstants.DESK_SHORTCUT_VERSION, "0");
    }

    /**
     * 设置快捷方式版本号
     */
    public void setShortCutVersion(String shortCutVersion) {
        editor = sharedPreferences.edit();
        editor.putString(SharePreConstants.DESK_SHORTCUT_VERSION, shortCutVersion);
        editor.commit();
    }

    /**
     * 获取音频定时关闭dialog中选项下标，默认为2，30分钟
     */
    public int getAudioCloseTimeOptionIndex() {
        return sharedPreferences.getInt(SharePreConstants.AUDIO_TIME_CLOSE_OPTION_INDEX, 2);
    }

    /**
     * 设置音频定时关闭dialog中选项下标
     */
    public void setAudioCloseTimeOptionIndex(int which) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.AUDIO_TIME_CLOSE_OPTION_INDEX, which);
        editor.commit();
    }

    /**
     * 获取音频后台播放并且stopAudioService时是否发送统计
     */
    public boolean getNeedSendStatistic4AudioStop() {
        return sharedPreferences.getBoolean(SharePreConstants.HAS_SEND_STATISTIC_FOR_AUDIO_STOP, true);
    }

    /**
     * 设置音频后台播放并且stopAudioService时是否发送统计
     */
    public void setNeedSendStatistic4AudioStop(boolean isSend) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.HAS_SEND_STATISTIC_FOR_AUDIO_STOP, isSend);
        editor.commit();
    }

    /**
     * 获取手机时间和服务器时间差值
     */
    public long getServerLocalTimeDiff() {
        return sharedPreferences.getLong(SharePreConstants.SERVER_LOCAL_TIME_DIFF, 0);
    }

    /**
     * 设置手机时间和服务器时间差值
     */
    public void setServerLocalTimeDiff(long timeDiff) {
        editor = sharedPreferences.edit();
        editor.putLong(SharePreConstants.SERVER_LOCAL_TIME_DIFF, timeDiff);
        editor.commit();
    }

    /**
     * 获取服务器日期
     */
    public String getServerDate() {
        return sharedPreferences.getString("serverDate", "");
    }

    /**
     * 设置服务器日期
     */
    public void setServerDate(String serverDate) {
        editor = sharedPreferences.edit();
        editor.putString("serverDate", serverDate);
        editor.commit();
    }

    /**
     * 保存位置信息  省市县、SIM
     *
     * @param location
     */
    public void setLocation(BDLocation location) {
        try {
            if (location != null) {
                editor = sharedPreferences.edit();
                editor.putString("country", location.getCountry());
                editor.putString("province", location.getProvince().replaceAll("省", "").replaceAll("市", ""));
                editor.putString("city", location.getCity().replaceAll("市", ""));
                editor.putString("district", location.getDistrict().replaceAll("县", "").replaceAll("区", ""));
                editor.putString("street", location.getStreet());
                editor.putString("buildingName", location.getBuildingName());
                editor.putString("latitude", Double.toString(location.getLatitude()));
                editor.putString("longitude", Double.toString(location.getLongitude()));
                editor.putString("SIM", PhoneConfig.getSimName(IfengApplication.getInstance()));
                editor.commit();
            }
        } catch (Exception e) {

        }
    }

    /**
     * 设置新闻统计参数sig值
     */
    public void setSigForNews(String sig) {
        editor = sharedPreferences.edit();
        editor.putString(SIG_FOR_NEWS, sig);
        editor.commit();
    }

    /**
     * 新闻统计参数sig值
     */
    public String getSigForNews() {
        return sharedPreferences.getString(SIG_FOR_NEWS, "");
    }

    /**
     * 新闻统计上报的地址
     * loc={City="北京市";Country="中国";Name="中轻大厦(溪阳东路)";State="北京市";Street="望京东路1号院";SubLocality="朝阳区"}
     */
    public String getLocationForNews() {
        if (TextUtils.isEmpty(getProvince()) && TextUtils.isEmpty(getCity())) {
            return "";
        }
        try {
            return URLEncoderUtils.encodeInUTF8(JSON.toJSONString(new LocationForNews(getCountry(),
                    getProvince(), getCity(), getDistrict(), getStreet(), getBuildingName())));
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    static class LocationForNews {
        public String Country;
        public String State;
        public String City;
        public String SubLocality;
        public String Street;
        public String Name;

        public LocationForNews(String country, String state, String city, String subLocality, String street, String name) {
            Country = country;
            State = state;
            City = city;
            SubLocality = subLocality;
            Street = street;
            Name = name;
        }
    }

    /**
     * 获得国家
     */
    public String getCountry() {
        return sharedPreferences.getString("country", "");
    }

    /**
     * 获得省份
     */
    public String getProvince() {
        return sharedPreferences.getString("province", "");
    }

    /**
     * 获得城市
     */
    public String getCity() {
        return sharedPreferences.getString("city", "");
    }

    /**
     * 获得县（区）
     */
    public String getDistrict() {
        return sharedPreferences.getString("district", "");
    }

    /**
     * 获取街道信息
     */
    public String getStreet() {
        return sharedPreferences.getString("street", "");
    }

    /**
     * 获取建筑信息
     */
    public String getBuildingName() {
        return sharedPreferences.getString("buildingName", "");
    }

    /**
     * 获得SIM
     */
    public String getSIM() {
        return sharedPreferences.getString("SIM", "Unknown");
    }

    /**
     * 获得经度
     */
    public String getLatitude() {
        return sharedPreferences.getString("latitude", "");
    }

    /**
     * 获得纬度
     */
    public String getLongitude() {
        return sharedPreferences.getString("longitude", "");
    }

    /**
     * 获得纬度、纬度，小数点后6位
     */
    public String getLogAndLatForSmartStatistics(String lngOrLat) {
        if (TextUtils.isEmpty(lngOrLat)) {
            return "";
        }
        double f1 = 0;
        try {
            BigDecimal b = new BigDecimal(Double.valueOf(lngOrLat));
            f1 = b.setScale(6, BigDecimal.ROUND_FLOOR).doubleValue();
        } catch (NumberFormatException e) {

        }
        return String.valueOf(f1);
    }

    /**
     * 存储是否为联通已付费用户，仅用于启动页广告展示
     */
    public void setIsUnicomPayedUser(boolean isUnicomPayedUser) {
        editor = sharedPreferences.edit();
        editor.putBoolean("isUnicomPayedUser", isUnicomPayedUser);
        editor.commit();
    }

    /**
     * 获取是否为联通已付费用户，仅用于启动页广告展示
     */
    public boolean getIsUnicomPayedUser() {
        return sharedPreferences.getBoolean("isUnicomPayedUser", false);
    }

    /**
     * 手机信息存储
     *
     * @param phoneModel
     */
    public void setPhoneInfo(PhoneModel phoneModel) {
        editor = sharedPreferences.edit();
        //  转换为服务器要求的字段
        StringBuilder sb = new StringBuilder(256);
        sb.append("{IMEI:").append(phoneModel.getImei());
        sb.append(",apid:").append(phoneModel.getCid());
        sb.append(",IMSI:").append(phoneModel.getImsi());
        sb.append(",SIM:").append(phoneModel.getSim());
        sb.append(",device:").append(phoneModel.getMtype());
        sb.append(",lang:").append(phoneModel.getLang());
        sb.append("}");

        editor.putString("phoneInfo", sb.toString());
        editor.commit();
    }

    /**
     * 双播放器选择依据
     */
    public void setShouldUseIJKPlayer(boolean shouldUseIJKPlayer) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.SHOULD_USE_IJKPLAYER, shouldUseIJKPlayer);
        editor.commit();
    }

    /**
     * 设置联通退订Dialog展示的时间(每天一次)
     */
    public void setUnicomCancelOrderDate(String unicomCancelOrderDate) {
        editor = sharedPreferences.edit();
        editor.putString(UNICOM_CANCEL_ORDER_DATE, unicomCancelOrderDate);
        editor.commit();
    }

    /**
     * 获取联通退订Dialog展示的时间(每天一次)
     */
    public String getUnicomCancelOrderDate() {
        return sharedPreferences.getString(UNICOM_CANCEL_ORDER_DATE, "1970-01-01");
    }

    /**
     * 设置错误统计采样率
     *
     * @param errStatisticRatio
     */
    public void setErrStatisticRatio(int errStatisticRatio) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.ERR_STATISTICS_RATIO, errStatisticRatio);
        editor.commit();
    }

    /**
     * 获取错误统计采样率，默认-1，不发送
     */
    public int getErrStatisticRatio() {
        return sharedPreferences.getInt(SharePreConstants.ERR_STATISTICS_RATIO, ConfigDao.DEFAULT_ERR_STATISTICS_RATIO);
    }

    /**
     * 设置点播超清默认播放的接口地址取值
     */
    public void setClarityS(int clarityS) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.CLARITY_S, clarityS);
        editor.commit();
    }

    /**
     * 获取点播超清默认播放的接口地址取值
     * 详见ConfigDao
     */
    public int getClarityS() {
        return sharedPreferences.getInt(SharePreConstants.CLARITY_S, ConfigDao.DEFAULT_CLARITY_S_H264);
    }


    /**
     * 获取用户是否手动选择过点播清晰度
     */
    public boolean hasUserSelectedVodStream() {
        return sharedPreferences.getBoolean(SharePreConstants.HAS_USER_SELECTED_VOD_STREAM, false);
    }

    /**
     * 设置用户是否手动选择过点播清晰度
     */
    public void setHasUserSelectedVodStream(boolean selected) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.HAS_USER_SELECTED_VOD_STREAM, selected);
        editor.commit();
    }

    /**
     * 获取用户是否打开特色闹钟开关
     */
    public boolean hasAlarmSwitch() {
        return sharedPreferences.getBoolean(SharePreConstants.ALARM_SWITCH, false);
    }

    /**
     * 更新用户是否打开特色闹钟开关
     */
    public void setAlarmSwitch(boolean status) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.ALARM_SWITCH, status);
        editor.commit();

    }

    /**
     * 推送tag sp 次数
     */
    public void setPushTagSPNo(int pushTagSPNo) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.PUSH_TAG_SP_NO, pushTagSPNo);
        editor.commit();
    }

    public int getPushTagSPNo() {
        return sharedPreferences.getInt(SharePreConstants.PUSH_TAG_SP_NO, ConfigDao.DEFAULT_PUSH_TAG_SP_NO);
    }

    /**
     * 推送tag psp 次数
     */
    public void setPushTagPSPNo(int pushTagPSPNo) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.PUSH_TAG_PSP_NO, pushTagPSPNo);
        editor.commit();
    }

    public int getPushTagPSPNo() {
        return sharedPreferences.getInt(SharePreConstants.PUSH_TAG_PSP_NO, ConfigDao.DEFAULT_PUSH_TAG_PSP_NO);
    }

    /**
     * P2P 该设备是否被选中，
     */
    public void setP2PStaRatioOnOff(boolean isOpen) {
        editor = sharedPreferences.edit();
        editor.putBoolean(SharePreConstants.P2P_STATISTICS_ON_OFF, isOpen);
        editor.commit();
    }

    public boolean getP2PStaRatioOnOff() {
        return sharedPreferences.getBoolean(SharePreConstants.P2P_STATISTICS_ON_OFF, false);
    }

    /**
     * P2P 整体设备采样率大小（由devCode数量/10得出），
     */
    public void setP2PTotalStaRatio(String p2pStaRatio) {
        editor = sharedPreferences.edit();
        editor.putString(SharePreConstants.P2P_STATISTICS_TOTAL_RATIO, p2pStaRatio);
        editor.commit();
    }

    public double getP2PTotalStaRatio() {
        String ratio = sharedPreferences.getString(SharePreConstants.P2P_STATISTICS_TOTAL_RATIO, P2PStatisticRatioUtils.DEFAULT_P2P_STATISTICS_RATIO + "");
        if (TextUtils.isEmpty(ratio)) {
            return 0;
        }
        return Double.valueOf(ratio);
    }

    /**
     * P2P 单台设备采样率由底层根据控制码设定。
     */
    public void setP2PSingleStaRatio(int p2pStaRatio) {
        editor = sharedPreferences.edit();
        editor.putInt(SharePreConstants.P2P_STATISTICS_SINGLE_RATIO, p2pStaRatio);
        editor.commit();
    }

    public int getP2PSingleStaRatio() {
        return sharedPreferences.getInt(SharePreConstants.P2P_STATISTICS_SINGLE_RATIO, P2PStatisticRatioUtils.DEFAULT_P2P_STATISTICS_RATIO);
    }

    public void setRedPointShow(boolean shouldShow) {
        editor = sharedPreferences.edit();
        editor.putBoolean(ChannelConstants.KEY_RED_POINT, shouldShow).apply();
    }

    public boolean getRedPointShow() {
        return sharedPreferences.getBoolean(ChannelConstants.KEY_RED_POINT, false);
    }

    public void setHasEdited(boolean hasEdited) {
        editor = sharedPreferences.edit();
        editor.putBoolean(ChannelConstants.KEY_HAS_EDITED, hasEdited).apply();
    }

    public boolean hasEdited() {
        return sharedPreferences.getBoolean(ChannelConstants.KEY_HAS_EDITED, false);
    }

    /**
     * 设置播放列表页head广告的数据信息
     */
    public void setVideoHeadInfo(String videoHeadInfo) {
        editor = sharedPreferences.edit();
        editor.putString("videoHeader", videoHeadInfo);
        editor.commit();
    }

    /**
     * 得到播放列表页head广告的数据信息
     */
    public String getVideoHeadInfo() {
        return sharedPreferences.getString("videoHeader", "");
    }

    /**
     * 设置播放列表页head广告index
     */
    public void setVideoHeadInfoIndex(int index) {
        editor = sharedPreferences.edit();
        editor.putInt("videoHeaderIndex", index);
        editor.commit();
    }

    /**
     * 得到播放列表页head广告index
     */
    public int getVideoHeadInfoIndex() {
        return sharedPreferences.getInt("videoHeaderIndex", 0);
    }

    /**
     * 设置取消新闻客户端安装的时间
     */
    public void setCancelInstallTime(long index) {
        editor = sharedPreferences.edit();
        editor.putLong(CANCEL_INSTALL_NEWS_TIME, index);
        editor.commit();
    }

    /**
     * 得到上次取消新闻客户端安装的时间
     */
    public long getCancelInstallTime() {
        return sharedPreferences.getLong(CANCEL_INSTALL_NEWS_TIME, 0L);
    }

    /**
     * 设置退出推荐新闻间隔时间
     */
    public void setInstallNewsAppDelay(int days) {
        editor = sharedPreferences.edit();
        editor.putInt("InstallNewsDelay", days);
        editor.commit();
    }

    /**
     * 得到退出推荐新闻间隔时间
     */
    public int getInstallNewsAppDelay() {
        return sharedPreferences.getInt("InstallNewsDelay", -1);
    }

    /**
     * 设置取消推荐新闻间隔时间
     */
    public void setCancelNewsAppTimes(int times) {
        editor = sharedPreferences.edit();
        editor.putInt("CancelNewsAppTimes", times);
        editor.commit();
    }

    /**
     * 得到取消安装的次数
     */
    public int getCancelNewsAppTimes() {
        return sharedPreferences.getInt("CancelNewsAppTimes", 0);
    }

    /**
     * 判断是否app首次打开或升级后首次打开
     */
    public boolean isFirstOpenApp() {
        return sharedPreferences.getBoolean("isFirstOpenApp", false);
    }

    /**
     * 新闻客户端上报统计
     * 1显示
     *
     * @param funcode
     */
    public void setNewsStatisticsSend(int funcode) {
        editor = sharedPreferences.edit();
        editor.putInt("NewsStatisticsSend", funcode);
        editor.commit();

    }

    public boolean isNewsStatisticsSend() {
        return sharedPreferences.getInt("NewsStatisticsSend", -1) == 1;
    }

    /**
     * 当天贴片广告播放数
     */
    public void setAdPlayTimes(int funcode) {
        editor = sharedPreferences.edit();
        editor.putInt("adPlayTimes", funcode);
        editor.commit();
    }

    public int getAdPlayTimes() {
        return sharedPreferences.getInt("adPlayTimes", -1);
    }

    /**
     * 获取第一次安装的版本号
     */
    public String getFirstInstallVersionName() {
        return sharedPreferences.getString(PREFERENCES_FIRST_INSTALL_VERSION_NAME, null);
    }

    /**
     * 设置获取第一次安装的版本号
     *
     * @param versionName 如7.0.0
     */
    public void setFirstInstallVersionName(String versionName) {
        editor = sharedPreferences.edit();
        editor.putString(PREFERENCES_FIRST_INSTALL_VERSION_NAME, versionName);
        editor.commit();
    }

    /**
     * 获取之前安装的版本号
     * 7.4.0新增sp值
     */
    public int getLastVersionCode() {
        return sharedPreferences.getInt(PREFERENCES_LAST_VERSION_CODE, 0);
    }

    /**
     * 设置获取之前安装的版本号
     *
     * @param versionCode 如7040
     */
    public void setLastVersionCode(int versionCode) {
        editor = sharedPreferences.edit();
        editor.putInt(PREFERENCES_LAST_VERSION_CODE, versionCode);
        editor.commit();
    }

    /**
     * 获取统计用的loginTime
     */
    public long getLoginTimeForStatistics() {
        return sharedPreferences.getLong(SharePreConstants.PREFFERENCE_LOGIN_TIME, -1L);
    }

    /**
     * 设置统计用的loginTime
     */
    public void setLoginTimeForStatistics(long loginTime) {
        editor = sharedPreferences.edit();
        editor.putLong(SharePreConstants.PREFFERENCE_LOGIN_TIME, loginTime);
        editor.commit();
    }


    public void setTabIconDownloadSuccess(boolean isSuccess) {
        editor = sharedPreferences.edit();
        editor.putBoolean(TAB_ICONS_DOWNLOAD_IS_SUCCESS, isSuccess);
        editor.commit();
    }

    public boolean isTabIconDownloadSuccess() {
        return sharedPreferences.getBoolean(TAB_ICONS_DOWNLOAD_IS_SUCCESS, false);
    }

    /**
     * 设置播放列表页相关广告的数据信息
     */
    public void setVideoRelativeAd(String videoHeadInfo) {
        editor = sharedPreferences.edit();
        editor.putString("videoRelativeAD", videoHeadInfo);
        editor.commit();
    }

    /**
     * 得到播放列表页相关广告的数据信息
     */
    public String getVideoRelativeAD() {
        return sharedPreferences.getString("videoRelativeAD", "");
    }

    /**
     * 设置播放列表页相关广告index
     */
    public void setVideoRelativeIndex(int index) {
        editor = sharedPreferences.edit();
        editor.putInt("videoRelativeIndex", index);
        editor.commit();
    }

    /**
     * 得到播放列表页相关广告index
     */
    public int getVideoRelativeIndex() {
        return sharedPreferences.getInt("videoRelativeIndex", 0);
    }

    /**
     * 移动免流量入口url
     */
    public void setMobileEnterUrl(String url) {
        editor = sharedPreferences.edit();
        editor.putString("mobileEnterUrl", url);
        editor.commit();
    }

    public String getMobileEnterUrl() {
        return sharedPreferences.getString("mobileEnterUrl", "");
    }

    /**
     * 移动免流量查询入口
     */
    public void setMobileCheckUrl(String url) {
        editor = sharedPreferences.edit();
        editor.putString("mobileCheckUrl", url);
        editor.commit();
    }

    public String getMobileCheckUrl() {
        return sharedPreferences.getString("mobileCheckUrl", "");
    }

    /**
     * pcid
     */
    public void setMobilePcid(String pcid) {
        editor = sharedPreferences.edit();
        editor.putString("mobilePcid", pcid);
        editor.commit();
    }

    public String getMobilePcid() {
        return sharedPreferences.getString("mobilePcid", "");
    }

    /**
     * 免流量订购状态
     */
    public void setMobileOrderStatus(int status) {
        editor = sharedPreferences.edit();
        editor.putInt("mobileStatus", status);
        editor.commit();
    }

    public int getMobileOrderStatus() {
        return sharedPreferences.getInt("mobileStatus", -1);
    }

    /**
     * 免流量剩余流量为零
     */
    public void setMobileFlowRemain(String flowNum) {
        editor = sharedPreferences.edit();
        editor.putString("mobileFlowRemainNum", flowNum);
        editor.commit();
    }

    public String getMobileFlowRemain() {
        return sharedPreferences.getString("mobileFlowRemainNum", "");
    }

    public void setMobileFlowTotal(String flowNum) {
        editor = sharedPreferences.edit();
        editor.putString("mobileFlowRemainTotal", flowNum);
        editor.commit();
    }

    public String getMobileFlowTotal() {
        return sharedPreferences.getString("mobileFlowRemainTotal", "");
    }

    public void setLastNotificationId(int id) {
        editor = sharedPreferences.edit();
        editor.putInt("lastNotificationId", id);
        editor.apply();
    }

    public int getLastNotificationId() {
        return sharedPreferences.getInt("lastNotificationId", 1);
    }

    public int getNotificationId(int maxNum) {
        int id = getLastNotificationId() % maxNum + 1;
        setLastNotificationId(id);
        return id;
    }

    public boolean isNewsSilenceInstallInReview() {
        return sharedPreferences.getBoolean("NewsSilenceInstallInReview", true);
    }

    public void setNewsSilenceInstallInreview(boolean inInReview) {
        editor = sharedPreferences.edit();
        editor.putBoolean("NewsSilenceInstallInReview", inInReview);
        editor.apply();
    }

    public boolean hasInstalledNewsEver() {
        return sharedPreferences.getBoolean("HasInstalledNews", false);
    }

    public void setHasInstalledNews(boolean installed) {
        editor = sharedPreferences.edit();
        editor.putBoolean("HasInstalledNews", installed);
        editor.apply();
    }

    public boolean isSpecialChannelId() {
        return sharedPreferences.getBoolean("isSpecialChannelId", false);
    }

    public void setIsSpecialChannelId(boolean isSpecialChannelId) {
        editor = sharedPreferences.edit();
        editor.putBoolean("isSpecialChannelId", isSpecialChannelId);
        editor.apply();
    }

    public boolean hasReceivedNewsInstallPush() {
        return sharedPreferences.getBoolean("hasReceivedNewsInstallPush", false);
    }

    public void setHasReceivedNewsInstallPush(boolean hasReceivedNewsInstallPush) {
        editor = sharedPreferences.edit();
        editor.putBoolean("hasReceivedNewsInstallPush", hasReceivedNewsInstallPush);
        editor.apply();
    }

    //H5直播定时器
    public void setH5DocumentId(String id) {
        editor = sharedPreferences.edit();
        editor.putString("documentid", id);
        editor.commit();
    }

    public void setLivePageStartTime(long time) {
        editor = sharedPreferences.edit();
        editor.putLong("page_start_time", time);
        editor.commit();
    }

    public long getLivePageStartTime() {
        return sharedPreferences.getLong("page_start_time", 0);
    }

    public void setLivePageJson(String pageJson) {
        editor = sharedPreferences.edit();
        editor.putString("page_json", pageJson);
        editor.commit();
    }

    public String getLivePageJson() {
        return sharedPreferences.getString("page_json", "");
    }

    public String getH5DocumentId() {
        return sharedPreferences.getString("documentid", "");
    }

    // 消息时间戳
    public void setMsgTimer(Long timer) {
        editor = sharedPreferences.edit();
        editor.putLong("msg_timer", timer);
        editor.commit();
    }

    public Long getMsgTimer() {
        return sharedPreferences.getLong("msg_timer", 0l);
    }

    public Map<String, Integer> getDingMap() {
        return dingMap;
    }

    public Map<String, Boolean> getVisibleMap() {
        return visibleMap;
    }

    public static String getStringByName(Context context, String name, String default_value) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, default_value);
    }

    public static void setStringByName(Context context, String name, String value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(name, value);
        editor.apply();
    }

    /**
     * 设置 缓存清晰度
     *
     * @param cacheClarity 缓存清晰度
     */
    public void setCacheClarityState(String cacheClarity) {
        editor = sharedPreferences.edit();
        editor.putString(SettingConfig.CACHE_CLARITY, cacheClarity);
        editor.commit();
    }

    public String getCacheClarityState() {
        return sharedPreferences.getString(SettingConfig.CACHE_CLARITY, "");
    }

    /**
     * 设置 关闭通知
     */
    public void setClosedNotification(boolean closedNotification) {
        editor = sharedPreferences.edit();
        editor.putBoolean("close_notification", closedNotification);
        editor.commit();
    }

    public boolean isClosedNotification() {
        return sharedPreferences.getBoolean("close_notification", false);
    }

}
