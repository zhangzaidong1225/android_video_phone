package com.ifeng.newvideo.utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.dao.ConfigDao;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.player.ChoosePlayerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fanshell on 2016/8/19.
 */
public class StreamUtils {
    private static Logger logger = LoggerFactory.getLogger(StreamUtils.class);
    private static final HashMap<Integer, String> STREAM_TYPE = new HashMap<>();
    private static final HashMap<String, Integer> STREAM_VALUE = new HashMap<>();
    private static final HashMap<String, String> STREAM_BYTE = new HashMap();
    private static final HashMap<String, String> VR_STREAM_BYTE = new HashMap();

    public static final String KEY_STREAM = "stream";

    private static final String VR_MP4_1M = "VRmp41M";
    private static final String VR_MP4_2M = "VRmp42M";
    private static final String VR_MP4_4M = "VRmp44M";

    private static final String VIDEO_SUPER_CLARITY = "mp41M";
    private static final String VIDEO_HD_CLARITY = "mp4500k";
    private static final String VIDEO_STANDARD_CLARITY = "mp4350k";
    private static final String SHARE_SUPER_CLARITY = "超清优先";
    private static final String SHARE_HD_CLARITY = "高清优先";
    private static final String SHARE_STANDARD_CLARITY = "标清优先";

    static {
        STREAM_TYPE.put(MediaConstants.STREAM_LOW, "流畅");
        STREAM_TYPE.put(MediaConstants.STREAM_MID, "标清");
        STREAM_TYPE.put(MediaConstants.STREAM_HIGH, "高清");
        STREAM_TYPE.put(MediaConstants.STREAM_SUPPER, "超清");
        STREAM_TYPE.put(MediaConstants.STREAM_ORIGINAL, "原画");
        STREAM_TYPE.put(MediaConstants.STREAM_AUTO, "自动");

        STREAM_VALUE.put("流畅", MediaConstants.STREAM_LOW);
        STREAM_VALUE.put("标清", MediaConstants.STREAM_MID);
        STREAM_VALUE.put("高清", MediaConstants.STREAM_HIGH);
        STREAM_VALUE.put("超清", MediaConstants.STREAM_SUPPER);
        STREAM_VALUE.put("原画", MediaConstants.STREAM_ORIGINAL);
        STREAM_VALUE.put("自动", MediaConstants.STREAM_AUTO);

        STREAM_BYTE.put("标清", "mp4350k");
        STREAM_BYTE.put("高清", "mp4500k");
        STREAM_BYTE.put("超清", "mp41M");
        STREAM_BYTE.put("H265超清", "h265350k");
        STREAM_BYTE.put("ts标清", "ts350k");
        STREAM_BYTE.put("ts高清", "ts500k");
        STREAM_BYTE.put("ts超清", "ts1M");

        VR_STREAM_BYTE.put("标清", VR_MP4_1M);
        VR_STREAM_BYTE.put("高清", VR_MP4_2M);
        VR_STREAM_BYTE.put("超清", VR_MP4_4M);
    }

    /**
     * 得真实的地址
     *
     * @param fileTypes
     * @return
     */
    public static String getMediaUrl(List<FileType> fileTypes) {
        if (ListUtils.isEmpty(fileTypes)) {
            return "";
        }
        FileType result = getValidateFileType(fileTypes);
        return (result == null || result.mediaUrl == null) ? "" : result.mediaUrl;
    }

    /**
     * 得到一个有效的地址
     *
     * @param fileTypes 文件的类型
     * @return 得到一个有效的地址
     */
    public static String getValidateStream(List<FileType> fileTypes) {
        FileType result = getValidateFileType(fileTypes);
        if (result == null) {
            return "标清";
        }
        if (VIDEO_STANDARD_CLARITY.equals(result.useType)) {
            return "标清";
        } else if ("mp4500k".equals(result.useType)) {
            return "高清";
        }
        return "超清";
    }

    /**
     * 得到视频下载地址
     * "mp4350k" : 标清
     * "mp4500k" : 高清
     * "mp41M" : 超清
     *
     * @param videoFiles
     * @return
     */
    public static FileType getMediaDownLoadFileType(List<FileType> videoFiles) {
        String cacheClarity = SharePreUtils.getInstance().getCacheClarityState();
        String clarity = VIDEO_HD_CLARITY;
        if (!TextUtils.isEmpty(cacheClarity)) {
            if (cacheClarity.contentEquals(SHARE_SUPER_CLARITY)) {
                clarity = VIDEO_SUPER_CLARITY;
            } else if (cacheClarity.contentEquals(SHARE_HD_CLARITY)) {
                clarity = VIDEO_HD_CLARITY;
            } else {
                clarity = VIDEO_STANDARD_CLARITY;
            }
        }

        for (FileType fileType : videoFiles) {
            if (TextUtils.isEmpty(fileType.useType) || TextUtils.isEmpty(fileType.mediaUrl)) {
                continue;
            }
            // 优先设置的清晰度
            if (clarity.equalsIgnoreCase(fileType.useType)) {
                return fileType;
            }
        }

        for (FileType fileType : videoFiles) {
            if (TextUtils.isEmpty(fileType.useType) || TextUtils.isEmpty(fileType.mediaUrl)) {
                continue;
            }
            // 高清
            if (VIDEO_HD_CLARITY.equalsIgnoreCase(fileType.useType)) {
                return fileType;
            }
        }

        for (FileType fileType : videoFiles) {
            if (TextUtils.isEmpty(fileType.useType) || TextUtils.isEmpty(fileType.mediaUrl)) {
                continue;
            }
            // 高清没有取标清
            if (VIDEO_STANDARD_CLARITY.equalsIgnoreCase(fileType.useType)) {
                return fileType;
            }
        }

        return new FileType();
    }

    /**
     * 得到音频下载地址
     *
     * @param videoFiles
     * @return
     */
    public static FileType getAudioDownLoadFileType(List<FileType> videoFiles) {
        for (FileType fileType : videoFiles) {
            if (TextUtils.isEmpty(fileType.useType) || TextUtils.isEmpty(fileType.mediaUrl)) {
                continue;
            }
            if ("mp3".equals(fileType.useType)) {
                return fileType;
            }
        }
        return new FileType();
    }

    /**
     * 得到流的类型
     *
     * @return
     */
    public static String getStreamType() {
        int streamType = SharePreUtils.getInstance().getVodCurrentStream();
        return STREAM_TYPE.get(streamType);
    }

    /**
     * 用于得到tv的播放地址
     *
     * @param tvLiveInfo
     * @return
     */
    public static String getMediaUrlForTV(TVLiveInfo tvLiveInfo) {
        if (tvLiveInfo == null) {
            return "";
        }
        String url = "";
        int streamType = SharePreUtils.getInstance().getLiveCurrentStream();
        switch (streamType) {
            case MediaConstants.STREAM_LOW:
                url = tvLiveInfo.getVideoL();
                break;
            case MediaConstants.STREAM_MID:
                url = tvLiveInfo.getVideoM();
                break;
            case MediaConstants.STREAM_HIGH:
                url = tvLiveInfo.getVideoH();
                break;
        }
        // 为空时，改为自动
        if (TextUtils.isEmpty(url)) {
            SharePreUtils.getInstance().setLiveCurrentStream(MediaConstants.STREAM_AUTO);
            url = tvLiveInfo.getVideo();
        }
        return url;
    }

    /**
     * 得到直播的播放类型
     *
     * @return
     */

    public static String getLiveStreamType() {
        int streamType = SharePreUtils.getInstance().getLiveCurrentStream();
        return STREAM_TYPE.get(streamType);
    }

    public static int getStreamValue(String stream) {
        return STREAM_VALUE.get(stream);
    }

    public static String getMediaUrlForPic(List<ChannelBean.VideoFilesBean> beans) {
        if (EmptyUtils.isEmpty(beans)) {
            return "";
        }
        List<FileType> videoFiles = new ArrayList<>();
        for (ChannelBean.VideoFilesBean bean : beans) {
            FileType type = new FileType();
            type.useType = bean.getUseType();
            type.mediaUrl = bean.getMediaUrl();
            type.filesize = bean.getFilesize();
            type.spliteTime = bean.getSpliteTime();
            videoFiles.add(type);
        }
        return getMediaUrl(videoFiles);
    }

    /**
     * 当前是超清 && 自有播放器 && 配置接口中取H265超清 ---> 播放h265350k地址
     * 否则 ---> 原码流type
     */
    @NonNull
    private static String handleStreamSuper(String stream) {
        boolean isSuper = !TextUtils.isEmpty(stream) && stream.equals("超清");
        if (!isSuper) {
            return stream;
        }

        boolean useIJKPlayer = ChoosePlayerUtils.useIJKPlayer(IfengApplication.getAppContext());
        if (useIJKPlayer) {
            return stream;
        }

        int clarityS = SharePreUtils.getInstance().getClarityS();
        boolean isSuperH265 = ConfigDao.DEFAULT_CLARITY_S_H265_1 == clarityS;
        if (isSuperH265) {
            return "H265超清";
        }

        return stream;
    }


    public static List<String> getAllStream(List<FileType> fileTypes) {
        List<String> allStreamResult = new ArrayList<>();

        if (fileTypes == null || fileTypes.isEmpty()) {
            allStreamResult.add("超清");
            allStreamResult.add("高清");
            allStreamResult.add("标清");
            logger.debug("getAllStream:return all");
            return allStreamResult;
        }

        if (isValidateStream(VIDEO_SUPER_CLARITY, fileTypes)) {
            allStreamResult.add("超清");
        }
        if (isValidateStream(VIDEO_HD_CLARITY, fileTypes)) {
            allStreamResult.add("高清");
        }
        if (isValidateStream(VIDEO_STANDARD_CLARITY, fileTypes)) {
            allStreamResult.add("标清");
        }
        return allStreamResult;
    }

    private static boolean isValidateStream(String type, List<FileType> fileTypes) {
        for (FileType item : fileTypes) {
            if (EmptyUtils.isNotEmpty(type) && item != null
                    && type.equals(item.useType) && !TextUtils.isEmpty(item.mediaUrl)) {
                return true;
            }
        }
        return false;
    }


    private static FileType getValidateFileType(List<FileType> fileTypes) {
        if (ListUtils.isEmpty(fileTypes)) {
            return null;
        }
        int streamType = SharePreUtils.getInstance().getVodCurrentStream();
        int original = streamType;

        FileType result = null;
        boolean isFound = false;
        while (true) {
            String stream = STREAM_TYPE.get(streamType);
            stream = handleStreamSuper(stream);
            String streamByte = STREAM_BYTE.get(stream);
            int size = fileTypes.size();
            for (int i = 0; i < size; i++) {
                result = fileTypes.get(i);
                if (TextUtils.isEmpty(streamByte) || TextUtils.isEmpty(result.useType) || TextUtils.isEmpty(result.mediaUrl)) {
                    continue;
                }
                if (streamByte.equalsIgnoreCase(result.useType.toLowerCase().trim())) {
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
            streamType = (streamType + 1) % (MediaConstants.STREAM_AUTO);
            if (streamType == original) {
                logger.error("no type matches");
                return new FileType();
            }
        }
        logger.debug("streamValue:{},url:{}", result.useType, result.mediaUrl);
        return result;
    }

    public static String getMp3Url(List<FileType> fileTypes) {
        if (ListUtils.isEmpty(fileTypes)) {
            return "";
        }
        for (FileType result : fileTypes) {
            if ("mp3".equalsIgnoreCase(result.useType)) {
                return result.mediaUrl;
            }
        }
        return "";
    }


    /**
     * 得到VR全部码流
     *
     * @param fileTypes 文件类型
     */
    public static List<String> getVRAllStream(List<FileType> fileTypes) {
        List<String> allStreamResult = new ArrayList<>();
        if (isValidateStream(VR_MP4_4M, fileTypes)) {
            allStreamResult.add("超清");
        } else if (isValidateStream(VIDEO_SUPER_CLARITY, fileTypes)) {
            allStreamResult.add("超清");
        }

        if (isValidateStream(VR_MP4_2M, fileTypes)) {
            allStreamResult.add("高清");
        } else if (isValidateStream(VIDEO_HD_CLARITY, fileTypes)) {
            allStreamResult.add("高清");
        }

        if (isValidateStream(VR_MP4_1M, fileTypes)) {
            allStreamResult.add("标清");
        } else if (isValidateStream(VIDEO_STANDARD_CLARITY, fileTypes)) {
            allStreamResult.add("标清");
        }
        return allStreamResult;
    }

    /**
     * 获取VR码流地址
     */
    public static String getVRStreamUrl(List<FileType> fileTypes) {
        FileType result = getVRValidateFileType(fileTypes);
        return (result == null || result.mediaUrl == null) ? "" : result.mediaUrl;
    }

    /**
     * 获取VR码流类型
     */
    public static String getVRStreamType(List<FileType> fileTypes) {
        FileType result = getVRValidateFileType(fileTypes);
        if (result == null) {
            int streamType = SharePreUtils.getInstance().getVodCurrentStream();
            return STREAM_TYPE.get(streamType);
        }
        if (VR_MP4_1M.equals(result.useType) || VIDEO_STANDARD_CLARITY.equals(result.useType)) {
            return "标清";
        } else if (VR_MP4_2M.equals(result.useType) || VIDEO_HD_CLARITY.equals(result.useType)) {
            return "高清";
        }
        return "超清";
    }


    private static FileType getVRValidateFileType(List<FileType> fileTypes) {
        int streamType = SharePreUtils.getInstance().getVodCurrentStream();
        int original = streamType;

        FileType result = null;
        boolean isFound = false;
        while (true) {
            String stream = STREAM_TYPE.get(streamType);
            stream = handleStreamSuper(stream);
            String vrStreamByte = VR_STREAM_BYTE.get(stream);
            String streamByte = STREAM_BYTE.get(stream);
            for (FileType fileType : fileTypes) {
                result = fileType;
                if (TextUtils.isEmpty(vrStreamByte) || TextUtils.isEmpty(result.useType) || TextUtils.isEmpty(result.mediaUrl)) {
                    continue;
                }
                if (vrStreamByte.equalsIgnoreCase(result.useType.toLowerCase().trim())
                        || streamByte.equalsIgnoreCase(result.useType.toLowerCase().trim())) {
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
            streamType = (streamType + 1) % (MediaConstants.STREAM_AUTO);
            if (streamType == original) {
                logger.error("vr  no type matches");
                return new FileType();
            }
        }
        logger.debug("vr streamValue:{},url:{}", result.useType, result.mediaUrl);
        return result;
    }
}
