package com.ifeng.newvideo.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.member.bean.UserPointMessage;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;

/**
 * Created by ll on 2017/7/21.
 * 用户积分管理
 */
public class UserPointManager {
    public final static String SCROE_TIME_KEY = "scroeTimeKey"; //获取积分时间
    public final static String SCORE_OPEN_AD_KEY = "addByOpenAD";//广告获取积分
    public final static String SCORE_SIGN_IN_KEY = "addBySignIn";//签到获取积分
    public final static String SCORE_OPEN_APP_KEY = "addByOpenApp";//开启客户端获取积分
    public final static String SCORE_OPEN_VIDEO_KEY = "addByOpenVedio";//打开视频获取积分
    public final static String SCORE_COMMENT_KEY = "addByComment";//评论视频获取积分
    public final static String SCORE_SHARE_URL_KEY = "addByShareUrl";//分享获取积分
    public final static String SCORE_SUBSCRIBE_WEMEDIA_KEY = "SubscribeWeMedia";//订阅自媒体
    public final static String SCORE_CHANGE_NICK_NAME = "changeNickName";//修改昵称
    public final static String HAS_SIGN_IN_KEY = "HasSignIn";//签到获取积分

    public enum PointType {
        //开启客户端
        addByOpenApp(1, "开启客户端"),

        addByOpenVideo(2, "观看视频"),

        //分享积分
        addByShareUrl(3, "分享视频"),

        //评论积分
        addByComment(4, "发表评论"),

        addByOpenAD(5, "欣赏广告"),

        //订阅自媒体积分
        addBySubscribeWeMedia(6, "订阅自媒体"),

        //签到
        addBySignIn(7, "签到"),

        //修改昵称
        addByChangeNickName(8, "修改昵称");

        private int mParam;
        private String mDescription;

        PointType(int param, String description) {
            mParam = param;
            mDescription = description;
        }

        public int getParam() {
            return mParam;
        }

        public String getDescription() {
            return mDescription;
        }
    }

    public static void addRewards(final PointType type) {
        boolean isAdded = false;
        if (type == PointType.addByOpenAD) {
            isAdded = User.getBooleanByName(SCORE_OPEN_AD_KEY, false);
        } else if (type == PointType.addBySignIn) {
            isAdded = User.getBooleanByName(SCORE_SIGN_IN_KEY, false);
        } else if (type == PointType.addByComment) {
            isAdded = User.getBooleanByName(SCORE_COMMENT_KEY, false);
        } else if (type == PointType.addByOpenVideo) {
            isAdded = User.getBooleanByName(SCORE_OPEN_VIDEO_KEY, false);
        } else if (type == PointType.addByOpenApp) {
            isAdded = User.getBooleanByName(SCORE_OPEN_APP_KEY, false);
        } else if (type == PointType.addByShareUrl) {
            isAdded = User.getBooleanByName(SCORE_SHARE_URL_KEY, false);
        } else if (type == PointType.addBySubscribeWeMedia) {
            isAdded = User.getBooleanByName(SCORE_SUBSCRIBE_WEMEDIA_KEY, false);
        } else if (type == PointType.addByChangeNickName) {
            isAdded = User.getBooleanByName(SCORE_CHANGE_NICK_NAME, false);
        }

        if (!isAdded && User.isLogin()) {
            sendUserPoint(
                    new PointPostCallback<UserPointMessage>() {
                        @Override
                        public void onSuccess(UserPointMessage result) {
                            ToastUtils.getInstance().showSignToast(type.getDescription(), String.valueOf(result.getData().getPoint()));
                        }

                        @Override
                        public void onFail(UserPointMessage result) {
                            if (type == PointType.addByOpenAD) {
                                User.setBooleanByName(SCORE_OPEN_AD_KEY, true);
                            } else if (type == PointType.addBySignIn) {
                                User.setBooleanByName(SCORE_SIGN_IN_KEY, true);
                            } else if (type == PointType.addByOpenApp) {
                                User.setBooleanByName(SCORE_OPEN_APP_KEY, true);
                            } else if (type == PointType.addByOpenVideo) {
                                User.setBooleanByName(SCORE_OPEN_VIDEO_KEY, true);
                            } else if (type == PointType.addBySubscribeWeMedia) {
                                User.setBooleanByName(SCORE_SUBSCRIBE_WEMEDIA_KEY, true);
                            } else if (type == PointType.addByShareUrl) {
                                User.setBooleanByName(SCORE_SHARE_URL_KEY, true);
                            } else if (type == PointType.addByComment) {
                                User.setBooleanByName(SCORE_COMMENT_KEY, true);
                            } else if (type == PointType.addByChangeNickName) {
                                User.setBooleanByName(SCORE_CHANGE_NICK_NAME, true);
                            }
                        }

                    }, type);
        }
    }


    public static void addRewardsForSignIn(final Context context, final PointType type, final PointPostCallback<String> callback) {
        boolean isAdded = false;
        if (type == PointType.addByOpenAD) {
            isAdded = User.getBooleanByName(SCORE_OPEN_AD_KEY, false);
        } else if (type == PointType.addBySignIn) {
            isAdded = User.getBooleanByName(SCORE_SIGN_IN_KEY, false);
        } else if (type == PointType.addByComment) {
            isAdded = User.getBooleanByName(SCORE_COMMENT_KEY, false);
        } else if (type == PointType.addByOpenVideo) {
            isAdded = User.getBooleanByName(SCORE_OPEN_VIDEO_KEY, false);
        } else if (type == PointType.addByOpenApp) {
            isAdded = User.getBooleanByName(SCORE_OPEN_APP_KEY, false);
        } else if (type == PointType.addByShareUrl) {
            isAdded = User.getBooleanByName(SCORE_SHARE_URL_KEY, false);
        } else if (type == PointType.addBySubscribeWeMedia) {
            isAdded = User.getBooleanByName(SCORE_SUBSCRIBE_WEMEDIA_KEY, false);
        }

        if (!isAdded && User.isLogin()) {
            sendUserPoint(
                    new PointPostCallback<UserPointMessage>() {
                        @Override
                        public void onSuccess(UserPointMessage result) {
                            callback.onSuccess("1");
                            if (type.getParam() == 7) {
                                if (1 == result.getData().getDays()) {
                                    ToastUtils.getInstance().showSignToast(type.getDescription(), String.valueOf(result.getData().getPoint()));
                                } else if (result.getData().getDays() > 1) {
                                    ToastUtils.getInstance().showSignToast(String.format(IfengApplication.getInstance().getString(R.string.user_center_sign_days_toast), result.getData().getDays()), String.valueOf(result.getData().getPoint()));
                                }
                            } else {
                                ToastUtils.getInstance().showSignToast(type.getDescription(), String.valueOf(result.getData().getPoint()));
                            }
                        }

                        @Override
                        public void onFail(UserPointMessage result) {
                            if (type == PointType.addByOpenAD) {
                                User.setBooleanByName(SCORE_OPEN_AD_KEY, true);
                            } else if (type == PointType.addBySignIn) {
                                User.setBooleanByName(SCORE_SIGN_IN_KEY, true);
                            } else if (type == PointType.addByOpenApp) {
                                User.setBooleanByName(SCORE_OPEN_APP_KEY, true);
                            } else if (type == PointType.addByOpenVideo) {
                                User.setBooleanByName(SCORE_OPEN_VIDEO_KEY, true);
                            } else if (type == PointType.addBySubscribeWeMedia) {
                                User.setBooleanByName(SCORE_SUBSCRIBE_WEMEDIA_KEY, true);
                            } else if (type == PointType.addByShareUrl) {
                                User.setBooleanByName(SCORE_SHARE_URL_KEY, true);
                            } else if (type == PointType.addByComment) {
                                User.setBooleanByName(SCORE_COMMENT_KEY, true);
                            }
                        }

                    }, type);
        }
    }

    public interface PointPostCallback<T> {
        public void onSuccess(T result);

        public void onFail(T result);
    }

    private static void sendUserPoint(final PointPostCallback<UserPointMessage> callback, PointType type) {
        String url = String.format(DataInterface.POINT_TASK_ADD_POINT, User.getUid(), PhoneConfig.mos, PhoneConfig.userKey, "", User.getIfengToken()) + "&type=" + type.getParam() + "&is_vip=" + (User.isVip() ? "1" : "0");
        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        JSONObject jsonObject = JSON.parseObject(response.toString());
                        int code = jsonObject.getIntValue("code");
                        UserPointMessage result = new UserPointMessage();
                        UserPointMessage.Data data = result.new Data();
                        if (200 == code) {
                            JSONObject object = jsonObject.getJSONObject("data");
                            data.setOpcode(object.getIntValue("opcode"));
                            data.setPoint(object.getIntValue("point"));
                            data.setDays(object.getIntValue("days"));
                            data.setMsg(object.getString("msg"));
                            result.setData(data);
                            callback.onSuccess(result);
                        } else if (202 == code) {
                            callback.onFail(result);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);

    }


    public static void resetUserPointStatusForTime() {
        long currentTime = User.getLongByName(SCROE_TIME_KEY, -1L);
        if (currentTime == -1L || !DateUtils.isToday(String.valueOf(currentTime))) {
            User.setBooleanByName(SCORE_SUBSCRIBE_WEMEDIA_KEY, false);
            User.setBooleanByName(SCORE_SHARE_URL_KEY, false);
            User.setBooleanByName(SCORE_COMMENT_KEY, false);
            User.setBooleanByName(SCORE_OPEN_VIDEO_KEY, false);
            User.setBooleanByName(SCORE_OPEN_AD_KEY, false);
            User.setBooleanByName(SCORE_SIGN_IN_KEY, false);
            User.setBooleanByName(SCORE_OPEN_APP_KEY, false);
            User.setBooleanByName(SCORE_CHANGE_NICK_NAME, false);

            User.setLongByName(SCROE_TIME_KEY, System.currentTimeMillis());
        }
    }

    public static void resetUserPointStatusForVip() {
        User.setBooleanByName(SCORE_SUBSCRIBE_WEMEDIA_KEY, false);
        User.setBooleanByName(SCORE_SHARE_URL_KEY, false);
        User.setBooleanByName(SCORE_COMMENT_KEY, false);
        User.setBooleanByName(SCORE_OPEN_VIDEO_KEY, false);
        User.setBooleanByName(SCORE_OPEN_AD_KEY, false);
        User.setBooleanByName(SCORE_SIGN_IN_KEY, false);
        User.setBooleanByName(SCORE_OPEN_APP_KEY, false);
        User.setBooleanByName(SCORE_CHANGE_NICK_NAME, false);
    }
}
