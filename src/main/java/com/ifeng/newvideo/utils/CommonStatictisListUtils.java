package com.ifeng.newvideo.utils;

import android.text.TextUtils;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.domains.ADInfoRecord;
import com.ifeng.newvideo.statistics.domains.ActionLiveRecord;
import com.ifeng.newvideo.statistics.domains.AdVideoRecord;
import com.ifeng.newvideo.statistics.domains.LiveExposeRecord;
import com.ifeng.newvideo.statistics.domains.PageInfoRecord;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ClientInfoDAO;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.SearchResultModel;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.ifeng.video.dao.db.dao.CommonDao.RESPONSE_TYPE_GET_JSON;

/**
 * 统计发送
 *
 * @author zhangzd
 */
public class CommonStatictisListUtils {

    private static final Logger logger = LoggerFactory.getLogger(CommonStatictisListUtils.class);

    private static volatile CommonStatictisListUtils commonStatictisListUtils = null;

    // 优酷统计
    public static final String YK_CLIENT_ID = "848df8351f5efc9f";
    public static final String YK_EXPOSURE = "exposure";
    public static final String YK_PLAY = "play";
    public static final String YK_NEXT = "next";
    public static final String YK_OPEN_YOULU_H5 = "openyouku_h5";
    public static final String YK_OPEN_YOULU_APP = "openyouku_app";
    public static final String YK_LIKE = "like";
    public static final String YK_FAVOURITE = "favourite";
    public static final String YK_SUBSCIRBE = "subscribe";
    public static final String YK_HOMEPAGE_FEEDS = "homepage_feeds";
    public static final String YK_FEED_DETAIL = "feed_detail";
    public static final String YK_NULL = "null";


    public static final String PARAM_SEPARATOR = "_";
    public static final String PARAM_ZERO = "0";
    public static final String PARAM_ONE = "1";

    //view 的排列顺序 1-- recyclerView  2-- headerView 3-- normao  4--- slide
    public static final int RECYVLERVIEW_TYPE = 0; //0_0 焦点图
    public static final int HEADERVIEW_TYPE = 1; //1_0
    public static final int PULLTOREFRESHVIEW_TYPE = 2; // 2_0
    public static final int SLIDE_TYPE = 3; //position_0

    // 列表类型
    public static final int wellChosen = 20;
    public static final int ifengTv = 21;
    public static final int normal = 22;
    public static final int fm = 23;
    public static final int lianbo = 24;
    public static final int vr = 25;

    private static final int SLIDE = 9;
    private int slideNum = 0;

    public static List<PageInfoRecord> mPageInfoList = new ArrayList<>(100);// 所有的曝光列表
    public static List<PageInfoRecord> wellChosenFocusList = new ArrayList<>(20);//精选
    public static List<PageInfoRecord> ifengTvFocusList = new ArrayList<>(20);// 卫视
    public static List<PageInfoRecord> normalFocusList = new ArrayList<>(20);//大图频道
    public static List<PageInfoRecord> fmFocusList = new ArrayList<>(20);// FM
    public static List<PageInfoRecord> lianboFocusList = new ArrayList<>(20);//连播
    public static List<PageInfoRecord> vRFocusList = new ArrayList<>(20);//VR
    public static List<ADInfoRecord.ADInfo> adInfoList = new ArrayList<>(); //adinfo
    public static List<LiveExposeRecord> liveLocalExposeRecords = new ArrayList<>(20); //H5 直播
    public static List<PageInfoRecord> videoDetailFocusList = new ArrayList<>(20); //播放底页视频列表 (专题)


    public static CommonStatictisListUtils getInstance() {
        if (commonStatictisListUtils == null) {
            synchronized (CommonStatictisListUtils.class) {
                if (commonStatictisListUtils == null) {
                    commonStatictisListUtils = new CommonStatictisListUtils();
                }
            }
        }
        return commonStatictisListUtils;
    }

    public PageInfoRecord creatorPageInfoRecord(String id, String rnum, String reftype, String channelId, String simid, String rToken, String payload) {
        PageInfoRecord.PInfo pInfo = new PageInfoRecord.PInfo(id, rnum, reftype, channelId);
        PageInfoRecord pageInfoRecord = null;
        pageInfoRecord = new PageInfoRecord(pInfo, simid, rToken, payload);
        return pageInfoRecord;
    }

    public PageInfoRecord creatorPageInfoRecord(String id, String rnum, String reftype, String channelId) {
        PageInfoRecord.PInfo pInfo = new PageInfoRecord.PInfo(id, rnum, reftype, channelId);
        PageInfoRecord pageInfoRecord = null;
        pageInfoRecord = new PageInfoRecord(pInfo);
        return pageInfoRecord;
    }

    /**
     * 头部是recyclerView 的卫视频道
     * 包括： slide 类型
     *
     * @param id
     * @param position
     * @param reftype
     * @param channelId
     * @param viewType
     * @param simid
     * @param rToken
     * @param payload
     */
    public void addRecyclerViewFocusList(String id, int position, String reftype, String channelId, int viewType, String simid, String rToken, String payload) {
        if (viewType == RECYVLERVIEW_TYPE) {
            String focusPosition = PARAM_ZERO + PARAM_SEPARATOR + position;
            PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
            changeIfengTvFocusList(pageInfoRecord);
        }

        if (viewType == SLIDE_TYPE) {
            String focusPosition = slideNum + PARAM_SEPARATOR + position;
            PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
            changeWellChosenFocusList(pageInfoRecord);
        }
    }

    // 焦点图
    public void addHeaderViewFocusList(String id, int position, String reftype, String channelId, int count, int viewType, int formType, String simid, String rToken, String payload) {
        if (viewType == RECYVLERVIEW_TYPE) {
            if (position < count) {
                String focusPosition = PARAM_ZERO + PARAM_SEPARATOR + position;
                PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
                changeCommonList(formType, pageInfoRecord);
            }
        }
        if (viewType == HEADERVIEW_TYPE) {
            if (position < count) {
                String focusPosition = PARAM_ONE + PARAM_SEPARATOR + position;
                PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
                changeIfengTvFocusList(pageInfoRecord);
            }
        }
    }

    //针对FM
    public void addHeaderViewFocusList(String id, int position, String reftype, String channelId, int count, int viewType, int formType) {
        if (viewType == RECYVLERVIEW_TYPE) {
            if (position < count) {
                String focusPosition = PARAM_ZERO + PARAM_SEPARATOR + position;
                PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
                changeCommonList(formType, pageInfoRecord);
            }
        }
        if (viewType == HEADERVIEW_TYPE) {
            if (position < count) {
                String focusPosition = PARAM_ONE + PARAM_SEPARATOR + position;
                PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
                changeIfengTvFocusList(pageInfoRecord);
            }
        }
    }

    /**
     * 信息流pageinfo 曝光
     *
     * @param id        item 的ID
     * @param position  位置信息
     * @param reftype   item 类型
     * @param channelId 上一级的channelId
     * @param type      item.getViewType 类型 针对slide类型
     * @param viewType  view 的排列顺序
     * @param formType  // 源自那个频道
     */
    public void addPullRefreshViewFocusList(String id, int position, String reftype, String channelId, int type, int viewType, int formType) {
        String focusPosition = null;
        PageInfoRecord pageInfoRecord = null;
        int firstNum = 0;
        switch (viewType) {
            case RECYVLERVIEW_TYPE:
                focusPosition = position + PARAM_SEPARATOR + PARAM_ZERO;
                pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
                changeCommonList(formType, pageInfoRecord);
                break;
            case HEADERVIEW_TYPE:
                if (type != SLIDE) {
                    firstNum = position + 1;
                    focusPosition = firstNum + PARAM_SEPARATOR + PARAM_ZERO;
                    pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
                    changeCommonList(formType, pageInfoRecord);
                } else {
                    slideNum = position + 1;
                }
                break;
            case PULLTOREFRESHVIEW_TYPE:
                firstNum = position + 2;
                focusPosition = firstNum + PARAM_SEPARATOR + PARAM_ZERO;
                pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
                changeIfengTvFocusList(pageInfoRecord);
                break;
            default:
                break;
        }
    }

    /**
     * 针对精选和推荐频道的信息流曝光
     *
     * @param id
     * @param position
     * @param reftype
     * @param channelId
     * @param type
     * @param viewType
     * @param formType
     */
    public void addPullRefreshViewFocusList(String id, int position, String reftype, String channelId, int type, int viewType, int formType, String simid, String rToken, String payload) {
        String focusPosition = null;
        PageInfoRecord pageInfoRecord = null;
        int firstNum = 0;
        switch (viewType) {
            case RECYVLERVIEW_TYPE:
                focusPosition = position + PARAM_SEPARATOR + PARAM_ZERO;
                pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
                changeCommonList(formType, pageInfoRecord);
                break;
            case HEADERVIEW_TYPE:
                if (type != SLIDE) {
                    firstNum = position + 1;
                    focusPosition = firstNum + PARAM_SEPARATOR + PARAM_ZERO;
                    pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
                    changeCommonList(formType, pageInfoRecord);
                } else {
                    slideNum = position + 1;
                }
                break;
            case PULLTOREFRESHVIEW_TYPE:
                firstNum = position + 2;
                focusPosition = firstNum + PARAM_SEPARATOR + PARAM_ZERO;
                pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId, simid, rToken, payload);
                changeIfengTvFocusList(pageInfoRecord);
                break;
            default:
                break;
        }
    }

    // 大图频道
    public void addPullRefreshViewNormalFocusList(BaseFragment baseFragment, String id, int position, String reftype, String channelId, int type, int viewType, int formType) {
        String focusPosition = position + PARAM_SEPARATOR + PARAM_ZERO;
        PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
        changeNormalFragmentFocusList(baseFragment, pageInfoRecord);
    }

    //播放底页
    public void addVideoDetailFocusList(BaseFragmentActivity baseFragment, String id, int position, String reftype, String channelId, int type, int viewType, int formType) {
        String focusPosition = position + PARAM_SEPARATOR + PARAM_ZERO;
        PageInfoRecord pageInfoRecord = creatorPageInfoRecord(id, focusPosition, reftype, channelId);
        changeVideoDetailFocusList(baseFragment, pageInfoRecord);
    }

    public void changeCommonList(int formType, PageInfoRecord pageInfoRecord) {
        switch (formType) {
            case wellChosen:
                changeWellChosenFocusList(pageInfoRecord);
                break;
            case fm:
                changeFMFocusList(pageInfoRecord);
                break;
            case lianbo:
                changeLianBoFocusList(pageInfoRecord);
                break;
            case normal:
                changeNormalFocusList(pageInfoRecord);
                break;
            case vr:
                changeVRFocusList(pageInfoRecord);
                break;
            default:
                break;
        }
    }

    public void changeWellChosenFocusList(PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            wellChosenFocusList.add(pageInfoRecord);
            clearFocusList(wellChosen);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void changeIfengTvFocusList(PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            ifengTvFocusList.add(pageInfoRecord);
            clearFocusList(ifengTv);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void changeNormalFocusList(PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            normalFocusList.add(pageInfoRecord);
            clearFocusList(normal);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void changeNormalFragmentFocusList(BaseFragment baseFragment, PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            if (baseFragment.mFocusList.size() >= 20) {
                sendStatictisList(baseFragment.mFocusList);
                baseFragment.mFocusList.clear();
            }
            baseFragment.mFocusList.add(pageInfoRecord);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void changeVideoDetailFocusList(BaseFragmentActivity baseFragment, PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            if (videoDetailFocusList.size() >= 10) {
                Log.d("detailFocusList", "202020202020202202002");
                sendStatictisList(videoDetailFocusList);
                videoDetailFocusList.clear();
            }
            videoDetailFocusList.add(pageInfoRecord);
            mPageInfoList.add(pageInfoRecord);
        }
    }


    public void changeFMFocusList(PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            fmFocusList.add(pageInfoRecord);
            clearFocusList(fm);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void changeLianBoFocusList(PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            lianboFocusList.add(pageInfoRecord);
            clearFocusList(lianbo);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void changeVRFocusList(PageInfoRecord pageInfoRecord) {
        if (!mPageInfoList.contains(pageInfoRecord)) {
            vRFocusList.add(pageInfoRecord);
            clearFocusList(vr);
            mPageInfoList.add(pageInfoRecord);
        }
    }

    public void clearFocusList(int type) {
        switch (type) {
            case wellChosen:
                if (wellChosenFocusList.size() >= 20) {
                    sendStatictisList(wellChosenFocusList);
                    wellChosenFocusList.clear();
                }
                break;
            case ifengTv:
                if (ifengTvFocusList.size() >= 20) {
                    sendStatictisList(ifengTvFocusList);
                    ifengTvFocusList.clear();
                }
                break;
            case normal:
                if (normalFocusList.size() >= 20) {
                    sendStatictisList(normalFocusList);
                    normalFocusList.clear();
                }
                break;
            case fm:
                if (fmFocusList.size() >= 20) {
                    sendStatictisList(fmFocusList);
                    fmFocusList.clear();
                }
                break;
            case lianbo:
                if (lianboFocusList.size() >= 20) {
                    sendStatictisList(lianboFocusList);
                    lianboFocusList.clear();
                }
                break;
            case vr:
                if (vRFocusList.size() >= 20) {
                    sendStatictisList(vRFocusList);
                    vRFocusList.clear();
                }
                break;
            default:
                break;
        }
    }

    public void sendStatictisList(List list) {
        PageActionTracker.focusPageItem(list);
    }

    public void sendDestoryList() {
        if (!ListUtils.isEmpty(wellChosenFocusList)) {
            sendStatictisList(wellChosenFocusList);
        }
        if (!ListUtils.isEmpty(ifengTvFocusList)) {
            sendStatictisList(ifengTvFocusList);
        }
        if (!ListUtils.isEmpty(fmFocusList)) {
            sendStatictisList(fmFocusList);
        }
        if (!ListUtils.isEmpty(lianboFocusList)) {
            sendStatictisList(lianboFocusList);
        }
        if (!ListUtils.isEmpty(vRFocusList)) {
            sendStatictisList(vRFocusList);
        }
        if (!ListUtils.isEmpty(liveLocalExposeRecords)) {
            sendLivePageInfo(liveLocalExposeRecords);
        }
        if (!ListUtils.isEmpty(videoDetailFocusList)) {
            sendStatictisList(videoDetailFocusList);
        }

    }

    public void sendADInfo(String adPositionId, String id, String channelId) {
        ADInfoRecord.ADInfo adInfo = new ADInfoRecord.ADInfo(adPositionId, id, channelId);
        ADInfoRecord adInfoRecord = null;
        if (!adInfoList.contains(adInfo)) {
            adInfoRecord = new ADInfoRecord(adInfo, "yes");
            adInfoList.add(adInfo);
        } else {
            adInfoRecord = new ADInfoRecord(adInfo, "no");
        }
        PageActionTracker.focusADItem(adInfoRecord);
    }

    public void sendAdVideo(AdVideoRecord adVideoRecord) {
        PageActionTracker.sendAdVideo(adVideoRecord);
    }


    public void sendLivePageInfo(List<LiveExposeRecord> liveExposeRecords) {
        PageActionTracker.focusPageItemLive(liveExposeRecords);
        liveLocalExposeRecords.clear();
    }

    public void sendActionLive(ActionLiveRecord actionLiveRecord) {
        PageActionTracker.focusActionLive(actionLiveRecord);
    }

    public void sendPageLive(PageLiveRecord pageLiveRecord) {
        PageActionTracker.focusPageLive(pageLiveRecord);
    }


    public void sendADEmptyExpose(String pid) {
        String url = "";

        if (!TextUtils.isEmpty(pid)) {
            url = DataInterface.getAdEmptyExpose(pid, PhoneConfig.softversion);
        } else {
            url = DataInterface.getAdEmptyExpose("", PhoneConfig.softversion);
        }
        Log.d("sendADEmptyExpose", url + "");

        CommonDao.sendRequest(url, null, new Response.Listener<Object>() {
            @Override
            public void onResponse(Object response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, CommonDao.RESPONSE_TYPE_GET_JSON);

    }

    /**
     * 发送优酷统计
     *
     * @param videoItem 播放底页
     */
    public void sendYoukuConstatic(VideoItem videoItem, String actionStr, String referpage) {
        String yvId = "";
        if (videoItem != null && !TextUtils.isEmpty(videoItem.yvId)) {
            yvId = videoItem.yvId;
        }
        sendYvid(yvId, actionStr, YK_FEED_DETAIL, YK_HOMEPAGE_FEEDS);
    }

    public void sendYoukuConstatic(String yvid, String actionStr, String referpage) {
        sendYvid(yvid, actionStr, YK_FEED_DETAIL, YK_HOMEPAGE_FEEDS);
    }

    /**
     * 信息流
     */
    public void sendHomeFeedYoukuConstatic(HomePageBeanBase homePageBean, String actionStr) {
        if (homePageBean == null || null == homePageBean.getMemberItem() || TextUtils.isEmpty(homePageBean.getMemberItem().getYvId())) {
            return;
        }
        sendYvid(homePageBean.getMemberItem().getYvId(), actionStr, YK_HOMEPAGE_FEEDS, YK_NULL);
    }

    /**
     * 自媒体
     *
     * @param memberItemEntity
     */
    public void sendWeMediaYoukuConstatic(WeMediaInfoList.InfoListEntity.BodyListEntity.MemberItemEntity memberItemEntity, String actionStr, String referpage) {
        if (memberItemEntity == null || TextUtils.isEmpty(memberItemEntity.getYvId())) {
            return;
        }
        sendYvid(memberItemEntity.getYvId(), actionStr, YK_HOMEPAGE_FEEDS, YK_NULL);
    }

    /**
     * 搜索结果
     */
    public void sendSearchResultYoukuConstatic(SearchResultModel resultModel, String actionStr, String referpage) {
        if (resultModel == null || TextUtils.isEmpty(resultModel.getYvId())) {
            return;
        }
        sendYvid(resultModel.getYvId(), actionStr, YK_HOMEPAGE_FEEDS, YK_NULL);
    }


    /**
     * 优酷统计
     *
     * @param yvid      yvid 非空 发送统计
     * @param actionStr type 事件类型
     * @param pagename  pagename
     * @param referpage referpage 类型
     */
    private void sendYvid(String yvid, String actionStr, String pagename, String referpage) {

        if (!TextUtils.isEmpty(yvid)) {
            String url = String.format(DataInterface.YOUKU_STAT_URL, YK_CLIENT_ID, actionStr, yvid, "", pagename,
                    referpage, System.currentTimeMillis(), PhoneConfig.UID, ClientInfoDAO.OS_TYPE_ANDROID, PhoneConfig.BRAND);

            CommonDao.sendRequest(url,
                    null,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("youku", response + "");
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("youku", error.toString() + "");
                        }
                    },
                    RESPONSE_TYPE_GET_JSON);
        }
    }


}
