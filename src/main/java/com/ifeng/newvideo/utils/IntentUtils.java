package com.ifeng.newvideo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.activity.CacheAllActivity;
import com.ifeng.newvideo.cache.activity.CachingActivity;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.HandleDataService;
import com.ifeng.newvideo.login.activity.ForgetPwdActivity;
import com.ifeng.newvideo.login.activity.LoginMainActivity;
import com.ifeng.newvideo.member.DealRecordActivity;
import com.ifeng.newvideo.member.MemberCenterActivity;
import com.ifeng.newvideo.member.OpenMemberActivity;
import com.ifeng.newvideo.member.PointTaskActivity;
import com.ifeng.newvideo.search.activity.SearchWeMediaActivity;
import com.ifeng.newvideo.setting.activity.AboutActivity;
import com.ifeng.newvideo.setting.activity.CachePathActivity;
import com.ifeng.newvideo.setting.activity.IfengNewsActivity;
import com.ifeng.newvideo.setting.activity.SettingActivity;
import com.ifeng.newvideo.setting.activity.SettingInfoActivity;
import com.ifeng.newvideo.setting.activity.UserFeedbackActivity;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ActivityFmMore;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.live.ActivityLive;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.ui.live.TVLiveActivity;
import com.ifeng.newvideo.ui.live.TVLiveListActivity;
import com.ifeng.newvideo.ui.live.vr.VRLiveActivity;
import com.ifeng.newvideo.ui.live.vr.VRVideoActivity;
import com.ifeng.newvideo.ui.mine.activity.MySubscriptionActivity;
import com.ifeng.newvideo.ui.mine.favorites.ActivityFavorites;
import com.ifeng.newvideo.ui.mine.history.ActivityHistory;
import com.ifeng.newvideo.ui.mine.msg.ActivityMsg;
import com.ifeng.newvideo.ui.mine.signin.SignInActivity;
import com.ifeng.newvideo.ui.subscribe.AllSubscribeActivity;
import com.ifeng.newvideo.ui.subscribe.AllWeMediaActivity;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.newvideo.videoplayer.activity.ActivityAudioFMPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityCacheVideoPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.ADInfoModel;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.db.model.HomePageBeanBase;
import com.ifeng.video.dao.db.model.LiveInfoModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 全局跳转工具类
 * Created by xt on 2014/9/1.
 */
public class IntentUtils {
    private static final Logger logger = LoggerFactory.getLogger(IntentUtils.class);

    public static final String H5_LIVE_URL = "http://izhibo.ifeng.com/live.html?liveid=";


    /**
     * 启动主页面
     *
     * @param context
     */
    public static void startMainTabActivity(Context context) {
        context.startActivity(new Intent(context, ActivityMainTab.class));
    }

    /**
     * 从启动页点击广告跳转到首页，然后分发到广告页
     *
     * @param adInfoModel 广告model
     */
    public static void startADActivityFromSplash(Context context, ADInfoModel adInfoModel) {
        Intent intent = new Intent(context, ActivityMainTab.class);
        intent.putExtra(IntentKey.IS_FROM_SPLASH_ACTIVITY, true);
        intent.putExtra(IntentKey.AD_MODEL_FROM_SPLASH_ACTIVITY, adInfoModel);
        context.startActivity(intent);
    }


    private static String shareTitle = null;
    private static String shareImg = null;

    /**
     * 增加精选传送的分享数据
     *
     * @param intent
     */
    public static void addShareData(Intent intent) {
        if (!TextUtils.isEmpty(shareTitle) && !TextUtils.isEmpty(shareImg)) {
            intent.putExtra(IntentKey.HOME_VIDEO_TITLE, shareTitle);
            intent.putExtra(IntentKey.HOME_VIDEO_IMG, shareImg);
            shareTitle = null;
            shareImg = null;
        }
    }

    public static void setShareData(String title, String img) {
        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(img)) {
            shareTitle = title;
            shareImg = img;
        }
    }

    public static void addHomePageShareData(BaseFragment fragment, HomePageBeanBase bean) {
        //所有频道支持首页传分享数据；去掉判断//fragment instanceof WellChosenFragment||fragment instanceof LianboChannelFragment

        String shareTitle = bean.getTitle();
        String shareImg = "";
        List<HomePageBeanBase.ImageBean> imageList = bean.getImageList();
        if (!ListUtils.isEmpty(imageList)) {
            shareImg = imageList.get(0).getImage();
        } else if (!TextUtils.isEmpty(bean.getImage())) {
            shareImg = bean.getImage();
        }

        if (CheckIfengType.isAD(bean.getMemberType())) {
            shareImg = bean.getImage();
        }
        setShareData(shareTitle, shareImg);

    }

    /**
     * 启动专题播放页面
     *
     * @param guid                聚焦id，即聚焦到某一个视频
     * @param topicId             大专题id
     * @param echid               频道id，只为统计用
     * @param topicType           专题类型
     * @param isFromHistory       是否由看过页面跳转
     * @param bookMark            看过视频的书签
     * @param fromParamForVideoAd 视频跳转来源，用于广告请求参数，不清楚的话，填""，会转成news
     */
    public static void toTopicDetailActivity(Context context, String guid, String topicId, String echid, String topicType,
                                             boolean isFromHistory, long bookMark, String fromParamForVideoAd) {
        Intent intent = new Intent(context, ActivityTopicPlayer.class);
        intent.putExtra(IntentKey.TOPIC_ID, topicId);
        intent.putExtra(IntentKey.TOPIC_TYPE, topicType);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        intent.putExtra(IntentKey.FROM_PARAM_FOR_VIDEO_AD, fromParamForVideoAd);
        //判断是否由看过页面跳转至专题播放页
        if (isFromHistory) {
            intent.putExtra(IntentKey.HISTORY_BOOK_MARK, bookMark);
        }
        addShareData(intent);
        context.startActivity(intent);
    }

    /**
     * 启动专题详情页。从push打开
     *
     * @param type 专题类型，焦点，联播，cmpp,imcp
     * @param id   cmpp为guid imcp为id
     */
    public static void toTopicFromPush(Context context, String type, String id) {
        Intent intent = new Intent(context, ActivityTopicPlayer.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(IntentKey.IS_FROM_PUSH, true);
        intent.putExtra(IntentKey.TOPIC_ID, id);
        intent.putExtra(IntentKey.TOPIC_TYPE, type);
        intent.putExtra(IntentKey.STATISTICS_REF_PAGE, "push");
        intent.putExtra(IntentKey.STATISTICS_TAG_PAGE, "");
        context.startActivity(intent);
    }


    /**
     * 启动点播播放页面
     *
     * @param guid                聚焦id
     * @param echid               频道id，只为统计用
     * @param needAnchor          是否需要锚定
     * @param isFromHistory       是否由看过页面跳转
     * @param bookMark            看过视频的书签
     * @param fromParamForVideoAd 视频跳转来源，用于广告请求参数，不清楚的话，填""，会转成news
     */
    public static void toVodDetailActivity(Context context, String guid, String echid, Boolean needAnchor,
                                           boolean isFromHistory, long bookMark, String fromParamForVideoAd) {
        Intent intent = new Intent(context, ActivityVideoPlayerDetail.class);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        intent.putExtra(IntentKey.ANCHOR, needAnchor);
        intent.putExtra(IntentKey.FROM_PARAM_FOR_VIDEO_AD, fromParamForVideoAd);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.putExtra(IntentKey.IS_FROM_HISTORY, isFromHistory);
        intent.putExtra(IntentKey.HISTORY_BOOK_MARK, bookMark);
        addShareData(intent);
        ((Activity) context).startActivityForResult(intent, 100);
        ((Activity) context).overridePendingTransition(R.anim.common_slide_left_in, R.anim.common_slide_left_out);
    }

    //添加弹幕是否显示标识
    public static void toVodDetailActivity(Context context, String guid, String echid, Boolean needAnchor,
                                           boolean isFromHistory, long bookMark, String fromParamForVideoAd, boolean isEditDanma) {
        Intent intent = new Intent(context, ActivityVideoPlayerDetail.class);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        intent.putExtra(IntentKey.ANCHOR, needAnchor);
        intent.putExtra(IntentKey.FROM_PARAM_FOR_VIDEO_AD, fromParamForVideoAd);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.putExtra(IntentKey.IS_FROM_HISTORY, isFromHistory);
        intent.putExtra(IntentKey.HISTORY_BOOK_MARK, bookMark);
        intent.putExtra(IntentKey.IS_SHOW_EDIT_DANMU, isEditDanma);
        addShareData(intent);
        ((Activity) context).startActivityForResult(intent, 100);
        ((Activity) context).overridePendingTransition(R.anim.common_slide_left_in, R.anim.common_slide_left_out);
    }


    /**
     * 从缓存跳到点播播放页面
     * 比普通跳转多传一个 IS_FROM_CACHE   true
     *
     * @param guid                聚焦id
     * @param echid               频道id，只为统计用
     * @param isColumn            视频类型是否为栏目
     * @param columnName          栏目名称
     * @param isFromHistory       是否由看过页面跳转
     * @param bookMark            看过视频的书签
     * @param fromParamForVideoAd 视频跳转来源，用于广告请求参数，不清楚的话，填""，会转成news
     */
    public static void toVodDetailActivityFromCache(Context context, String guid, String echid, Boolean isColumn, String columnName,
                                                    boolean isFromHistory, long bookMark, String fromParamForVideoAd) {
        Intent intent = new Intent(context, ActivityVideoPlayerDetail.class);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        if (isColumn) {
            intent.putExtra(IntentKey.LAYOUT_TYPE, IfengType.LayoutType.column);
        } else {
            intent.putExtra(IntentKey.LAYOUT_TYPE, IfengType.LayoutType.vod);
        }
        intent.putExtra(IntentKey.FROM_PARAM_FOR_VIDEO_AD, fromParamForVideoAd);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.putExtra(IntentKey.IS_FROM_CACHE, true);
        //判断是否由看过页面跳转至播放页
        if (isFromHistory) {
            intent.putExtra(IntentKey.HISTORY_BOOK_MARK, bookMark);
        }
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.common_slide_left_in, R.anim.common_slide_left_out);
    }

    /**
     * 从直播频道跳直播详情页面
     *
     * @param model
     */
    public static void toLiveDetailActivity(Context context, LiveInfoModel model, String echid) {
        Intent intent = new Intent(context, ActivityLive.class);
        intent.putExtra(IntentKey.LIVE_BY_PUSH_INDEX, false);
        intent.putExtra(IntentKey.LIVE_INFO_MODEL, model);
        intent.putExtra(IntentKey.E_CHID, echid);
        context.startActivity(intent);
    }

    public static void toLiveByPush(Context context, String pushSId) {
        Intent intent = new Intent(context, ActivityLive.class);
        intent.putExtra(IntentKey.LIVE_BY_PUSH_INDEX, true);
        intent.putExtra(IntentKey.LIVE_IFENG_URL, pushSId);
        intent.putExtra(IntentKey.IS_FROM_PUSH, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void toLiveDetailActivityWithIndexCMCC(Context context, String pushId, String countId, String echid, String imageUrl) {
        Intent intent = new Intent(context, ActivityLive.class);
        intent.putExtra(IntentKey.LIVE_BY_PUSH_INDEX, false);
        LiveInfoModel model = new LiveInfoModel();
        model.setNodeId(pushId);
        model.setContId(countId);
        model.setBkgURL(imageUrl);
        intent.putExtra(IntentKey.LIVE_INFO_MODEL, model);
        intent.putExtra(IntentKey.E_CHID, echid);
        context.startActivity(intent);
    }

    /**
     * 跳到直播详情页面
     *
     * @param channelIdOrTitle 可以分别通过title 和 channelID来启动， id优先级 > title
     * @param echid            用于统计
     */
    public static void toLiveDetailActivityWithIndex(Context context, String channelIdOrTitle, String echid) {
        Intent intent = new Intent(context, ActivityLive.class);
        intent.putExtra(IntentKey.LIVE_BY_PUSH_INDEX, true);
        intent.putExtra(IntentKey.LIVE_IFENG_URL, LiveUtils.transLiveId(channelIdOrTitle));
        intent.putExtra(IntentKey.E_CHID, echid);
        context.startActivity(intent);
    }


    /**
     * 跳转至凤凰快讯页面
     *
     * @param id      推送的快讯的ID
     * @param content 推送的快讯的内容
     */
    public static void startIfengNewsFromPush(Context context, String id, String content) {
        Intent intent = new Intent(context, IfengNewsActivity.class);
        intent.putExtra(IntentKey.IFENG_NEWS_ID, id);
        intent.putExtra(IntentKey.IFENG_NEWS_CONTENT, content);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 跳到搜索页面
     */
    public static void toSearchActivity(Context context) {
        Intent intent = new Intent(context, com.ifeng.newvideo.search.activity.SearchActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转至正在缓存页面
     *
     * @param type 页面类型，如 video / fm
     */
    public static void startCachingActivity(Context context, String type) {
        Intent intent = new Intent(context, CachingActivity.class);
        intent.setAction(type);
        context.startActivity(intent);
    }


    /**
     * 推送拉起下载
     *
     * @param guid
     * @param itemId
     * @param topicId
     * @param type
     */
    public static void startCacheAllActivityFromPush(Context context, String guid, String itemId, String topicId, String type) {
        Intent intent = new Intent(context, CacheAllActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentKey.IS_FROM_PUSH, true);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        intent.putExtra(IntentKey.VOD_ID, itemId);
        intent.putExtra(IntentKey.TOPIC_ID, topicId);
        intent.putExtra(IntentKey.TOPIC_TYPE, type);
        context.startActivity(intent);
    }

    /**
     * 跳转至缓存页面
     */
    public static void startCacheAllActivity(Context context) {
        Intent intent = new Intent(context, CacheAllActivity.class);
        context.startActivity(intent);
    }

    ///**
    // * 跳转至缓存专题类文件夹页面
    // */
    //public static void startCacheFolderActivity(Context context, int folderId, String resourceType) {
    //    Intent intent = new Intent(context, CacheFolderActivity.class);
    //    intent.putExtra(IntentKey.FOLDER_ID, folderId);
    //    intent.putExtra(IntentKey.FOLDER_RESOURCE_TYPE, resourceType);
    //    context.startActivity(intent);
    //}

    ///**
    // * 跳转至缓存文件夹音频更多页面
    // */
    //public static void startCacheFolderMoreAudioActivity(Context context, List<? extends PlayerInfoModel> list, String type, String guid, String programId) {
    //    Intent intent = new Intent(context, CacheMoreAudioActivity.class);
    //    Bundle bundle = new Bundle();
    //    PlayerInfoModel[] programs = new PlayerInfoModel[list.size()];
    //    bundle.putParcelableArray(IntentKey.PROGRAM_LIST, list.toArray(programs));
    //    intent.putExtras(bundle);
    //    intent.putExtra(IntentKey.FOLDER_TYPE, type);
    //    intent.putExtra(IntentKey.FOLDER_GUID, guid);
    //    intent.putExtra(IntentKey.FOLDER_PROGRAMID, programId);
    //    context.startActivity(intent);
    //}

    ///**
    // * 跳转至缓存文件夹视频更多页面
    // */
    //public static void startCacheFolderMoreVideoActivity(Context context, String type, String guid) {
    //    Intent intent = new Intent(context, CacheMoreVideoActivity.class);
    //    intent.putExtra(IntentKey.FOLDER_GUID, guid);
    //    intent.putExtra(IntentKey.FOLDER_TYPE, type);
    //    context.startActivity(intent);
    //}

    ///**
    // * 跳转至缓存文件夹更多视频页面 重载
    // */
    //public static void startCacheFolderMoreVideoActivity(Context context, String type, String guid, CacheFolderModel model) {
    //    Intent intent = new Intent(context, CacheMoreVideoActivity.class);
    //    intent.putExtra(IntentKey.FOLDER_GUID, guid);
    //    intent.putExtra(IntentKey.FOLDER_TYPE, type);
    //    intent.putExtra(IntentKey.CACHE_FOLDER_MODEL, model);
    //    context.startActivity(intent);
    //}

    /**
     * 跳转至观看过的页面
     */
    public static void ActivityHistory(Context context) {
        Intent intent = new Intent(context, ActivityHistory.class);
        context.startActivity(intent);
    }

    /**
     * 跳转至收藏页面
     */
    public static void startActivityFavorites(Context context) {
        Intent intent = new Intent(context, ActivityFavorites.class);
        context.startActivity(intent);
    }

    public static void startActivityMsg(Context context, boolean status) {
        Intent intent = new Intent(context, ActivityMsg.class);
        intent.putExtra(IntentKey.MSG_STATUS, status);
        context.startActivity(intent);
    }

    /**
     * 系统无线设置页
     */
    public static void startSystemWifiSetActivity(Context context) {
//        context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
        context.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
    }


    /**
     * 离线数据转换
     *
     * @param list        播放列表 如：
     * @param index       正要播放视频的位置 如：第3个 ：2list<PlayerInfoModel>
     * @param layoutType  播放视频的type 如：离线 ：offline
     * @param watchedTime 播放视频的观看时间 如：离线 ：offline
     * @return
     */
    public static Bundle getPlayerData(List<? extends PlayerInfoModel> list, IfengType.LayoutType layoutType, int index, Long watchedTime) {
        Bundle bundle = new Bundle();
        PlayerInfoModel[] programs = new PlayerInfoModel[list.size()];
        bundle.putParcelableArray(IntentKey.PROGRAM_LIST, list.toArray(programs));
        bundle.putInt(IntentKey.PROGRAM_LIST_INDEX, index);
        bundle.putSerializable(IntentKey.LAYOUT_TYPE, layoutType);
        if (watchedTime != null) {
            bundle.putLong(IntentKey.HISTORY_BOOK_MARK, watchedTime);
        }
        return bundle;
    }

    public static final String ACTION_SPLASHGUIDE2MAINTAB = "guild.to.maintab";
    public static final String ACTION_SPLASHGUIDESELF = "guild.self";

    /**
     * 用action启动页面
     *
     * @param action action
     * @param clazz  要跳到的页面
     */
    public static void launchActByAction(Context context, String action, Class clazz) {
        Intent intent = new Intent(context, clazz);
        intent.setAction(action);
        context.startActivity(intent);
    }

    /**
     * 添加QQ群，并根据是否有异常返回添加成功与否
     * <p/>
     * 群号：凤凰视频精品粉丝群(384451676), key 例如： Ox8qFysHAUtC9phyLYSuxEmjkaaH21ws
     * 调用 joinQQGroup(Ox8qFysHAUtC9phyLYSuxEmjkaaH21ws)
     * 即可发起手Q客户端申请加群 凤凰视频 精品粉丝群(384451676)
     * http://qun.qq.com/join.html
     *
     * @param key 由官网生成的key
     * @return 返回true表示呼起手Q成功，返回false表示呼起失败
     */
    public static boolean joinQQGroup(Context context, String key) {
        Intent intent = new Intent();
        intent.setData(Uri.parse("mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3D" + key));
        // 此Flag可根据具体产品需要自定义，如设置，则在加群界面按返回，返回手Q主界面，不设置，按返回会返回到呼起产品界面
        try {
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            // 未安装手Q或安装的版本不支持
            return false;
        }
    }

    /**
     * 从推送跳转到播放页面
     *
     * @param guid  视频guid
     * @param id    视频id??? // FIXME
     * @param echid 用于统计上报
     */
    public static void toVodVideoActivityFromPush(Context context, String guid, String id, String echid) {
        Intent intent = new Intent(context, ActivityVideoPlayerDetail.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentKey.IS_FROM_PUSH, true);
        intent.putExtra(IntentKey.LAYOUT_TYPE, IfengType.LayoutType.vod);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        intent.putExtra(IntentKey.VOD_ID, id);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.putExtra(IntentKey.STATISTICS_REF_PAGE, PageIdConstants.REF_PUSH);
        intent.putExtra(IntentKey.STATISTICS_TAG_PAGE, "");
        context.startActivity(intent);
    }

    /**
     * 跳转到设置页面
     */
    public static void startSettingActivity(Context context) {
        Intent intent = new Intent(context, SettingActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到缓存路径选择页面
     */
    public static void startCachePathActivity(Context context) {
        Intent intent = new Intent(context, CachePathActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳到登录页面
     */
    public static void startLoginActivity(Context context) {
        startLoginActivity(context, false);
    }

    /**
     *
     * @param isFromMine true 来自我的页面，昵称冲突弹出对话框
     */
    public static void startLoginActivity(Context context, boolean isFromMine) {
        Intent intent = new Intent(context, LoginMainActivity.class);
        intent.putExtra(IntentKey.IS_FROM_MINE, isFromMine);
        context.startActivity(intent);
    }

    /**
     * 启动应用推荐界面
     */
    @Deprecated
    public static void startADExchangeActivity(Context context) {
//        Intent intent = new Intent(context, ADChangeActivity.class);
//        context.startActivity(intent);
    }

    /**
     * 跳转广告页,带下载监测和下完监测链接
     *
     * @param context               context
     * @param adId                  无线广告id，无线广告统计上报需要字段，无可填null
     * @param clickUrl              广告地址
     * @param shareUrl              分享地址，除广告地址要加城市等参数外，一般和点击地址相同，无填null
     * @param function              根据产品需求定制的广告页面功能，如：ADActivity.FUNCTION_VALUE_AD , 无填null,
     * @param title                 广告标题
     * @param shareTitle            分享标题，因为有些要求分享和显示的标题不一样，如果title一致填null，不一致填相应值
     * @param imageUrl              广告图片
     * @param echid                 来源id
     * @param chid                  统计用 取接口中的searchPath
     * @param downloadCompletedUrls 广告下载完成上报地址，无填null
     * @param asyncDownloadUrls     广告异步下载地址，无填null
     */
    public static void startADActivity(Context context, String adId, String clickUrl, String shareUrl, String function,
                                       String title, String shareTitle, String imageUrl, String echid, String chid,
                                       ArrayList<String> downloadCompletedUrls, ArrayList<String> asyncDownloadUrls) {
        logger.debug("--->in startADActivity clickUrl is {} , title is {} , imageUrl is {}", clickUrl, title, imageUrl);

        if (TextUtils.isEmpty(clickUrl)) {
            return;
        }

        Intent intent = new Intent(context, ADActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ADActivity.KEY_AD_FLAG, true);
        intent.putExtra(ADActivity.KEY_AD_CLICK_URL, clickUrl);
        intent.putExtra(ADActivity.KEY_AD_SHARE_URL, shareUrl);
        intent.putExtra(ADActivity.KEY_FUNCTION, function);
        intent.putExtra(ADActivity.KEY_AD_TITLE, title);
        intent.putExtra(ADActivity.KEY_AD_SHARE_TITLE, shareTitle);
        intent.putExtra(ADActivity.KEY_AD_IMAGE, imageUrl);
        intent.putExtra(ADActivity.KEY_AD_ID, adId);
        intent.putExtra(ADActivity.KEY_ECHID, echid);
        intent.putExtra(ADActivity.KEY_CHID, chid);
        intent.putStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_COMPLETED_URL, downloadCompletedUrls);
        intent.putStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_START_URL, asyncDownloadUrls);
        context.startActivity(intent);
    }


    /**
     * 只用在移动免流量页面
     *
     * @see #startADActivity(Context, String, String, String, String, String, String, String, String, String, ArrayList, ArrayList)
     */
    public static void startADWebActivity(Context context, String adId, String clickUrl, String shareUrl, String function,
                                          String title, String imageUrl, String echid, String chid,
                                          ArrayList<String> downloadCompletedUrls, ArrayList<String> asyncDownloadUrls) {
        logger.debug("--->in startADActivity clickUrl is {} , title is {} , imageUrl is {}", clickUrl, title, imageUrl);
        if (TextUtils.isEmpty(clickUrl)) {
            return;
        }
        Intent intent = new Intent(context, ADActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ADActivity.KEY_AD_FLAG, true);
        intent.putExtra(ADActivity.KEY_AD_CLICK_URL, clickUrl);
        intent.putExtra(ADActivity.KEY_AD_SHARE_URL, shareUrl);
        intent.putExtra(ADActivity.KEY_FUNCTION, function);
        intent.putExtra(ADActivity.KEY_AD_TITLE, title);
        intent.putExtra(ADActivity.KEY_AD_IMAGE, imageUrl);
        intent.putExtra(ADActivity.KEY_AD_ID, adId);
        intent.putExtra(ADActivity.KEY_ECHID, echid);
        intent.putExtra(ADActivity.KEY_CHID, chid);
        intent.putStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_COMPLETED_URL, downloadCompletedUrls);
        intent.putStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_START_URL, asyncDownloadUrls);
        context.startActivity(intent);
//        ((Activity) context).startActivityForResult(intent, 210);
    }

    /**
     * 跳转关于页
     */
    public static void startAboutActivity(Context context) {
        context.startActivity(new Intent(context, AboutActivity.class));
    }

    /**
     * 跳转设置里信息
     */
    public static void startSetInfoActivity(Context context) {
        context.startActivity(new Intent(context, SettingInfoActivity.class));
    }


    /**
     * 跳转吐槽页
     */
    public static void startUserFeedbackActivity(Context context) {
        context.startActivity(new Intent(context, UserFeedbackActivity.class));
    }

    /**
     * 为创建ActivityVideoPlayDetail，清除栈
     */
    public static void toVodVideoActivityForInapp(Context context, String guid, String id, String echid) {
        Intent intent = new Intent(context, ActivityVideoPlayerDetail.class);
        intent.putExtra(IntentKey.LAYOUT_TYPE, IfengType.LayoutType.vod);
        intent.putExtra(IntentKey.VOD_GUID, guid);
        intent.putExtra(IntentKey.VOD_ID, id);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(IntentKey.STATISTICS_REF_PAGE, PageIdConstants.REF_OUTSIDE);
        intent.putExtra(IntentKey.STATISTICS_TAG_PAGE, "");
        context.startActivity(intent);
    }

    /**
     * 跳转签到页
     */
    public static void startSignInActivity(Context context) {
        context.startActivity(new Intent(context, SignInActivity.class));
    }

    /**
     * 跳转签到活动详情界面，（同H5界面）
     *
     * @param clickUrl 跳转地址
     * @param title    标题
     */
    public static void startSignInActiveInfoActivity(Context context, String clickUrl, String title, String imageUrl) {
        logger.debug("--->in startSignInActiveInfoActivity clickUrl is {} , title is {} , imageUrl is {} ", clickUrl, title, imageUrl);
        startADActivity(context, null, clickUrl, null, ADActivity.FUNCTION_VALUE_H5_STATIC, title, null, imageUrl, "", "", null, null);
    }

    /**
     * 跳转文明上网界面
     */
    public static void startCivilizationInternetActivity(Context context) {
        String url = "http://v.ifeng.com/special/app_protocol/baseline.shtml";
        startADActivity(context, null, url, url, ADActivity.FUNCTION_VALUE_H5_STATIC, context.getString(R.string.civilization_internet_title), null, null, "", "", null, null);
    }

    /**
     * 跳转注册协议界面
     */
    public static void startRegisterAgreementActivity(Context context) {
        String url = "http://id.ifeng.com/muser/agreement?id=5";
        String title = context.getString(R.string.login_agree_title);
        startWebViewActivity(context, url, title);
    }

    /**
     * 跳转到忘记密码页面
     */
    public static void startForgetPwdActivity(Context context) {
        String url = DataInterface.FORGET_PASSWORD;
        String title = context.getString(R.string.login_find_pwd);
        startWebViewActivity(context, url, title);
    }

    /**
     * 跳转到WebView页面
     */
    public static void startWebViewActivity(Context context, String url, String title) {
        Intent intent = new Intent(context, ForgetPwdActivity.class);
        intent.putExtra(IntentKey.WEB_VIEW_URL, url);
        intent.putExtra(IntentKey.WEB_VIEW_TITLE, title);
        context.startActivity(intent);
    }

    /**
     * 通过schema调起凤凰新闻客户端，并跳转到约定页面
     *
     * @param appScheme 接口中获取的appSchema
     * @param channelId
     */
    public static void startIfengNewsApp(Context context, String appScheme, String channelId) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_JUMP_NEWS_APP, ActionIdConstants.ACT_YES,
                String.format(PageIdConstants.HOME_CHANNEL, channelId));

        startAppByScheme(context, appScheme);
    }

    public static void startAppByScheme(Context context, String appScheme) {
        if (context == null || TextUtils.isEmpty(appScheme)) {
            return;
        }
        try {
            Intent intentToNewsClient = new Intent();
            intentToNewsClient.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intentToNewsClient.setAction(Intent.ACTION_VIEW);
            intentToNewsClient.addCategory(Intent.CATEGORY_DEFAULT);
            intentToNewsClient.setData(Uri.parse(appScheme));
            context.startActivity(intentToNewsClient);
        } catch (Exception exception) {
            logger.error("start ifengNews exception : {}", exception);
        }
    }

    public static void startBrowser(Context context, String url) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception exception) {
            logger.error("startBrowser exception : {}", exception);
        }
    }

    /**
     * 5.0 以上要求explicit intent
     */
    public static void startService(Context context, Intent intent) {
        intent.setPackage(IfengApplication.PACKAGE_NAME);
        context.startService(intent);
    }

    /**
     * 5.0 以上要求explicit intent
     */
    public static void stopService(Context context, Intent intent) {
        intent.setPackage(IfengApplication.PACKAGE_NAME);
        context.stopService(intent);
    }

    /**
     * 跳转到VR播放页,默认不是推送页面启动
     */
    public static void startVRLiveActivity(Context context, String vrId, String vrLiveUrl, String vrProgramTitle, String echid,
                                           String chid, String weMediaId, String weMediaName, String imageUrl, String shareUrl) {
        startVRLiveActivity(context, vrId, vrLiveUrl, vrProgramTitle, echid, chid, weMediaId, weMediaName, imageUrl, false, shareUrl);
    }

    /**
     * 跳转到VR播放页，推送
     */
    public static void startVRLiveActivity(Context context, String vrId, String vrLiveUrl, String vrProgramTitle, String echid,
                                           String chid, String weMediaId, String weMediaName, String imageUrl, boolean isFromPush, String shareUrl) {
        Intent intent = new Intent(context, VRLiveActivity.class);
        intent.putExtra(IntentKey.VR_LIVE_ID, vrId);
        intent.putExtra(IntentKey.VR_LIVE_URL, vrLiveUrl);
        intent.putExtra(IntentKey.VR_LIVE_PROGRAM_TITLE, vrProgramTitle);
        intent.putExtra(IntentKey.VR_LIVE_ECHID, echid);
        intent.putExtra(IntentKey.VR_LIVE_CHID, chid);
        intent.putExtra(IntentKey.VR_LIVE_WE_MEDIA_ID, weMediaId);
        intent.putExtra(IntentKey.VR_LIVE_WE_MEDIA_NAME, weMediaName);
        intent.putExtra(IntentKey.IS_FROM_PUSH, isFromPush);
        intent.putExtra(IntentKey.VR_LIVE_IMAGE_URL, imageUrl);
        intent.putExtra(IntentKey.VR_LIVE_SHARE_URL, shareUrl);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 跳转到VR播放页，推送
     *
     * @param position      当前视频在视频列表中的索引位置
     * @param seekTo        定位到播放位置
     * @param videoList     视频列表，存放的的视频的guid
     * @param isFromHistory 是否从看过或推送跳转而来
     */
    public static void startVRVideoActivity(Context context, int position, String echid, String chid, long seekTo, List<String> videoList, boolean isFromHistory) {
        Intent intent = new Intent(context, VRVideoActivity.class);
        intent.putExtra(IntentKey.VR_CURRENT_POSITION_IN_VIDEO_LIST, position);
        intent.putExtra(IntentKey.VR_LIVE_ECHID, echid);
        intent.putExtra(IntentKey.VR_LIVE_CHID, chid);
        intent.putExtra(IntentKey.VR_SEEKTO_POSITION, seekTo);
        intent.putExtra(IntentKey.VR_VIDEO_IS_FROM_HISTORY, isFromHistory);
        intent.putStringArrayListExtra(IntentKey.VR_VIDEO_LIST, (ArrayList<String>) videoList);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ((Activity) context).startActivityForResult(intent, 100);
    }

    /**
     * 跳转到直播列表页
     */
    public static void startTVLiveListActivity(Context context) {
        Intent intent = new Intent(context, TVLiveListActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到全部订阅页面
     */
    public static void startAllSubscribeActivity(Context context) {
        Intent intent = new Intent(context, AllSubscribeActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到全部自媒体页面
     */
    public static void startAllMediaActivity(Context context) {
        Intent intent = new Intent(context, AllWeMediaActivity.class);
        context.startActivity(intent);
    }


    /**
     * 跳转到自媒体主页
     */
    public static void startWeMediaHomePageActivity(Context context, String weMediaID, String echid) {
        Bundle bundle = new Bundle();
        bundle.putString(IntentKey.WE_MEIDA_ID, weMediaID);
        bundle.putString(IntentKey.E_CHID, echid);
        Intent intent = new Intent(context, WeMediaHomePageActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    /**
     * 跳转到搜索自媒体页
     */
    public static void startSearchWeMediaActivity(Context context, String keyword) {
        Bundle bundle = new Bundle();
        bundle.putString(IntentKey.SEARCH_KEYWORD, keyword);
        Intent intent = new Intent(context, SearchWeMediaActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    /**
     * 跳转到我的订阅页面
     */
    public static void startMySubscriptionActivity(Context context) {
        Intent intent = new Intent(context, MySubscriptionActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到电视台直播页面
     *
     * @param channelId   频道id  没有填"";
     * @param echid       统计,没有就填"";
     * @param chid        统计,没有就填"";
     * @param weMediaId   自媒体Id
     * @param weMediaName 自媒体名称
     * @param isFromPush  是否来自推送
     */
    public static void startActivityTVLive(Context context, String channelId, String echid, String chid,
                                           String weMediaId, String weMediaName, boolean isFromPush) {
        Intent intent = new Intent(context, TVLiveActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString(IntentKey.TV_LIVE_CHANNEL_ID, channelId);
        bundle.putString(IntentKey.TV_LIVE_ECHID, echid);
        bundle.putString(IntentKey.TV_LIVE_CHID, chid);
        bundle.putString(IntentKey.TV_LIVE_WEMEDIA_ID, weMediaId);
        bundle.putString(IntentKey.TV_LIVE_WEMEDIA_NAME, weMediaName);
        bundle.putBoolean(IntentKey.TV_LIVE_FROM_PUSH, isFromPush);
        intent.putExtras(bundle);
        addShareData(intent);
        context.startActivity(intent);
    }

    /**
     * 开启任务
     */
    public static void startTask(Context context, String action, Bundle bundle) {
        Intent intent = new Intent(context, HandleDataService.class);
        intent.setAction(action);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        context.startService(intent);//启动服务
    }

    public static void startTaskForChannels(Context context, Bundle bundle) {
        if (bundle != null) {
            Intent intent = new Intent(context, HandleDataService.class);
            intent.putExtras(bundle);
            context.startService(intent);
        }
    }

    /**
     * 跳转到缓存播放器页面
     *
     * @param model  当前实体
     * @param models 缓存实体类集合
     */
    public static void startActivityCacheVideoPlayer(Context context, CacheVideoModel model, ArrayList<CacheVideoModel> models) {
        Intent intent = new Intent(context, ActivityCacheVideoPlayer.class);
        intent.putExtra(IntentKey.CACHE_VIDEO_MODEL, model);
        intent.putExtra(IntentKey.CACHE_VIDEO_MODELS, models);
        intent.putExtra(IntentKey.IS_FROM_CACHE, true);
        context.startActivity(intent);
    }

    public static void startFMListActivity(Context context, String nodeName, String nodeId) {
        Intent intent = new Intent(context, ActivityFmMore.class);
        intent.putExtra("id", nodeId);
        intent.putExtra("name", nodeName);
        context.startActivity(intent);
    }

    /**
     * 启动电台播放页面
     * 目前跳转场景：收藏，看过，轮播图，列表
     *
     * @param guid            节目id
     * @param programId       专辑id
     * @param title           节目标题
     * @param type            节目类型，如脱口秀，有声小说
     * @param audioUrl        播放地址
     * @param imageUrl        节目图片
     * @param curPlayPosition 当前播放位置，新开始的传0，续播传当前值
     * @param fmStatisticVTag FM 统计V tag
     * @param echid           来源页id，用于统计上报
     * @param duration        时长
     * @param fileSize        文件大小
     */
    public static void toAudioFMActivity(Context context, String guid, String programId, String title, String type, String audioUrl,
                                         String imageUrl, long curPlayPosition, String fmStatisticVTag, String echid, int duration, int fileSize) {
        Intent intent = new Intent(context, ActivityAudioFMPlayer.class);
        intent.putExtra(IntentKey.AUDIO_PROGRAM_ID, programId);
        intent.putExtra(IntentKey.AUDIO_FM_TYPE, type);
        intent.putExtra(IntentKey.AUDIO_FM_URl, audioUrl);
        intent.putExtra(IntentKey.AUDIO_FM_GUID, guid);
        intent.putExtra(IntentKey.HISTORY_BOOK_MARK, curPlayPosition);//播放位置
        intent.putExtra(IntentKey.AUDIO_FM_NAME, title);
        intent.putExtra(IntentKey.AUDIO_FM_IMAGE_URl, imageUrl);
        intent.putExtra(IntentKey.FM_STATISTICS_V_TAG, fmStatisticVTag);
        intent.putExtra(IntentKey.E_CHID, echid);
        intent.putExtra(IntentKey.FM_DURATION, duration);
        intent.putExtra(IntentKey.FM_FILE_SIZE, fileSize);
        ((Activity) context).startActivityForResult(intent, 100);
    }

    /**
     * 启动电台播放页面
     * 目前跳转场景：FM更多
     *
     * @param programId       专辑id
     * @param fmStatisticVTag FM 统计V Tag
     * @param echid           来源页id，用于统计上报
     */
    public static void toAudioFMActivity(Context context, String guid, String programId, String type, String fmStatisticVTag, String echid) {
        Intent intent = new Intent(context, ActivityAudioFMPlayer.class);
        intent.putExtra(IntentKey.AUDIO_PROGRAM_ID, programId);
        intent.putExtra(IntentKey.AUDIO_FM_GUID, guid);
        intent.putExtra(IntentKey.AUDIO_FM_TYPE, type);
        intent.putExtra(IntentKey.FM_STATISTICS_V_TAG, fmStatisticVTag);
        intent.putExtra(IntentKey.E_CHID, echid);
        context.startActivity(intent);
    }

    /**
     * 跳转到开通会员页面
     */
    public static void startOpenMemberActivity(Context context) {
        Intent intent = new Intent(context, OpenMemberActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到凤凰视频会员页面
     */
    public static void startMemberCenterActivity(Context context) {
        Intent intent = new Intent(context, MemberCenterActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到会员交易记录页面
     */
    public static void startDealRecordActivity(Context context) {
        Intent intent = new Intent(context, DealRecordActivity.class);
        context.startActivity(intent);
    }

    /**
     * 跳转到积分任务页面
     */
    public static void startPointTaskActivity(Context context) {
        Intent intent = new Intent(context, PointTaskActivity.class);
        context.startActivity(intent);
    }


}
