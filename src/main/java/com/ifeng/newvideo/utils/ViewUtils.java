package com.ifeng.newvideo.utils;

import android.view.View;
import android.view.ViewGroup;
import com.ifeng.newvideo.R;

/**
 * Created by guolc on 2016/7/27.
 */
public class ViewUtils {


    public static void hideChildrenView(ViewGroup parent) {
        if (parent != null && parent.getChildCount() > 0) {
            for (int i = 0; i < parent.getChildCount(); i++) {
                parent.getChildAt(i).setVisibility(View.GONE);
            }
        }
    }
    public static void showChildrenView(ViewGroup parent){
        if(parent!=null&&parent.getChildCount()>0){
            for(int i=0;i<parent.getChildCount();i++){
                parent.getChildAt(i).setVisibility(View.VISIBLE);
            }
        }
    }


}
