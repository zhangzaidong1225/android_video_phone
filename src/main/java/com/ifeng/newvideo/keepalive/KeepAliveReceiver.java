package com.ifeng.newvideo.keepAlive;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.utils.FilterUtil;
import com.ifeng.newvideo.utils.PushUtils;

public class KeepAliveReceiver extends BroadcastReceiver {

    public final static String TAG = "keepalive";
    public static final String ACTION_PUSH_KEEPALIVE = "com.ifeng.ipush.intent.NOTIFICATION_KEEPALIVE";  //保活用

    @Override
    public void onReceive(Context context, Intent intent) {

        if (ACTION_PUSH_KEEPALIVE.equals(intent.getAction())) {
            Log.d(TAG, "keep alive push message received");
            if (!isIpushServicesRunning()) {
                initPush();
            }
        }
    }

    /**
     * 只针对ipush
     */
    private void initPush() {
         if (!(FilterUtil.isHuaWeiMobile() && FilterUtil.isHuaweiPushValid())) {
            Log.d(TAG, "LocalAliveService init ipush");
            PushUtils.initPush();
         }
    }


    private boolean isIpushServicesRunning() {
        try {
            ActivityManager manager = (ActivityManager) IfengApplication.getInstance().getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                String servName = service.service.getClassName();
                if (TextUtils.isEmpty(servName)) {
                    continue;
                }
                //过滤华为的和系统的服务
                if (servName.startsWith("com.huawei")
                        || servName.startsWith("com.samsung")
                        || servName.startsWith("com.xiaomi")
                        || servName.startsWith("com.meizu")
                        || servName.startsWith("com.zte")
                        || servName.startsWith("com.android")
                        || servName.startsWith("com.google")) {
                    continue;
                }
                if (IfengApplication.getInstance().getApplicationContext().getPackageName().equals(service.process) && servName.contains("ipush")) {
                    return true;
                } else {
                    continue;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
