package com.ifeng.newvideo.dialogUI;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.activity.BindingPhoneActivity;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.UniversalChannelFragment;
import com.ifeng.newvideo.ui.WellChosenFragment;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLDecoderUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.dao.LoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class DanmakuCommentFragment extends DialogFragment {

    private static final Logger logger = LoggerFactory.getLogger(DanmakuCommentFragment.class);
    private View mLayout;
    private EditText commentEditText;
    private Button send_comment_button;
    private TextView send_comment_number_indicate;
    private String currentComments;
    private String comment_id;

    private long enterTime;
    private int lastSelection;
    private Dialog bindPhoneDialog;

    public String lastComment = "";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void autoFocus() {
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                logger.debug("the comment pop show");
                InputMethodManager inManager = (InputMethodManager) commentEditText.getContext().getSystemService(INPUT_METHOD_SERVICE);
                inManager.showSoftInput(commentEditText, 0);
            }
        }, 500);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerLoginBundleBroadcast();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mLayout = inflater.inflate(R.layout.video_danmu_comment_layout, container);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        commentEditText = (EditText) mLayout.findViewById(R.id.send_comment_edit);
        //发布按钮
        initCommentButton(mLayout);
        send_comment_number_indicate = (TextView) mLayout.findViewById(R.id.send_comment_number_indicate);
        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                send_comment_button.setEnabled(!TextUtils.isEmpty(s.toString()));

                if (s.toString().length() > 20) {
                    send_comment_number_indicate.setTextColor(0xffff0000);
                    send_comment_number_indicate.setText(String.format(getString(R.string.more_than),
                            String.valueOf(s.toString().length() - 20)));
                } else {
                    send_comment_number_indicate.setTextColor(0xffbdc0c3);
                    send_comment_number_indicate.setText(s.toString().length() + "/" + 20);
                }
            }
        });
        //文明评论
        TextView start_civilization = (TextView) mLayout.findViewById(R.id.start_civilization);
        start_civilization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startCivilizationInternetActivity(v.getContext());
            }
        });
        //点击阴影消失
        mLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int top = mLayout.findViewById(R.id.send_comment).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top) {
                        dismissAllowingStateLoss();
                    }
                }
                return true;
            }
        });

        return mLayout;
    }

    private void initCommentButton(View view) {
        send_comment_button = (Button) view.findViewById(R.id.send_comment_button);
        send_comment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ClickUtils.isFastDoubleClick() || getActivity() == null) {
                    return;
                }
                if (!NetUtils.isNetAvailable(getActivity())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }

                if (!User.isLogin()) {
                    ToastUtils.getInstance().showShortToast("亲，登录后可发布评论");
                    IntentUtils.startLoginActivity(getActivity());
                    return;
                } else {
                    String params = "&sid=" + User.getIfengToken() + "&deviceId=" + PhoneConfig.userKey + "&ts=" + User.getTimeStamp();
                    LoginDao.requestServerCheckLogin(new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            if (response == null || TextUtils.isEmpty(response.toString())) {
                                ToastUtils.getInstance().showShortToast("弹幕发送失败，请重新发送");
                                return;
                            }
                            String message = null;
                            String messageCode = null;

                            try {
                                JSONObject jsonObject = null;
                                jsonObject = JSON.parseObject(response.toString());
                                message = URLDecoderUtils.decodeInUTF8(jsonObject.getString("message"));
                                messageCode = jsonObject.getString("msgcode");

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            if (LoginDao.LOGIN_SUCCESS.equals(messageCode)) {
                                if (!TextUtils.isEmpty(User.getRealNameStatus()) && User.REALNAMESTATUS_BIND_NO.equals(User.getRealNameStatus())) {
                                    showBindPhoneDialog();
                                    return;
                                }
                                sendComment();
                            } else if (LoginDao.LOGIN_ON_ANOTHER_DEVICE.equals(messageCode)) {
                                IntentUtils.startLoginActivity(getActivity());
                                ToastUtils.getInstance().showShortToast("亲，登录后可发布弹幕");
                                User.clearUserInfo(); //清空本地用户数据
                            } else {
                                if (TextUtils.isEmpty(message)) {
                                    message = "弹幕发送失败，请重新发送";
                                }
                                ToastUtils.getInstance().showShortToast(message);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }, params);
                }
            }
        });
    }

    @Override
    public void onResume() {
        enterTime = System.currentTimeMillis();
        if (!TextUtils.isEmpty(currentComments)) {
            commentEditText.setText(currentComments);
        } else {
            commentEditText.setText(lastComment);
            if (!TextUtils.isEmpty(lastComment)) {
                commentEditText.setSelection(lastSelection);
            }
        }

        if (commentEditText.getText().toString().length() > 20) {
            send_comment_number_indicate.setTextColor(0xffff0000);
            send_comment_number_indicate.setText(String.format(getString(R.string.more_than),
                    String.valueOf(commentEditText.getText().toString().length() - 20)));
        } else {
            send_comment_number_indicate.setTextColor(0xffbdc0c3);
            send_comment_number_indicate.setText(commentEditText.getText().toString().length() + "/" + 20);
        }
        if (isShowInputMethod) {
            autoFocus();
        }

        super.onResume();
    }


    private void sendComment() {
        if (commentEditText.getText().toString().length() > 20) {
            ToastUtils.getInstance().showShortToast("亲，字数超过20了哦");
            return;
        }

        String params = null;
        final Activity hostActivity = getActivity();

        try {

            if (null == mVideoItem || TextUtils.isEmpty(mVideoItem.guid)) {
                dismissAllowingStateLoss();
                ToastUtils.getInstance().showShortToast("弹幕发表失败，请重新发送");
                return;
            }
            // article_id, title, url, content、offset color fontsize
            // guid, title url, content, offset color fontsize
            String guid = mVideoItem.guid;
            String encodeUrl = URLEncoderUtils.encodeInUTF8(mVideoItem.mUrl);
            String encodeGuid = URLEncoderUtils.encodeInUTF8(guid);
            String encodeTitle = URLEncoderUtils.encodeInUTF8(TextUtils.isEmpty(mVideoItem.title) ? "" : mVideoItem.title.trim());
            String encodeContent = URLEncoderUtils.encodeInUTF8(commentEditText.getText().toString());
            String offset = String.valueOf(mVideoPlayer.getCurrentPosition() / 1000);
            String color = "0xFFFFFF";
            String fontsize = "14";

            params = "?article_id=" + encodeGuid +
                    "&title=" + encodeTitle +
                    "&url=" + encodeUrl +
                    "&content=" + encodeContent +
                    "&offset=" + offset +
                    "&color=" + color +
                    "&fontsize=" + fontsize +
                    "&sid=" + User.getIfengToken();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (null != hostActivity) {
            InputMethodManager imm = (InputMethodManager) hostActivity.getSystemService(INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
            }
        }

        dismissAllowingStateLoss();

        ToastUtils.getInstance().showShortToast("正在发布...");
        String url = DataInterface.POST_VIDEO_DANMU_URL + params;


        CommentDao.postVideoDanmuInfo(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (null == response || TextUtils.isEmpty(response.toString())) {
                    ToastUtils.getInstance().showShortToast("弹幕发表失败，请重新发送");
                    return;
                }
                JSONObject jsonObject = null;
                try {
                    jsonObject = JSON.parseObject(response.toString());
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    ToastUtils.getInstance().showShortToast("弹幕发表失败，请重新发送");
                    return;
                }
                Log.d("danmu", response.toString() + "");
                if (null != jsonObject) {
                    switch (jsonObject.getInteger("code")) {
                        case 1:
                            ToastUtils.getInstance().showShortToast("弹幕发表成功");
                            if (hostActivity instanceof ActivityVideoPlayerDetail) {
                                ActivityVideoPlayerDetail activity = (ActivityVideoPlayerDetail) hostActivity;
                                activity.updateLocalDanmu(commentEditText.getText().toString());
                            } else if (hostActivity instanceof ActivityTopicPlayer) {
                                ActivityTopicPlayer activity = (ActivityTopicPlayer) hostActivity;
                                activity.updateLocalDanmu(commentEditText.getText().toString());
                            } else {
                                if (channelBaseFragment != null) {
                                    channelBaseFragment.updateLocalDanmu(commentEditText.getText().toString());
                                } else if (wellFragment != null) {
                                    wellFragment.updateLocalDanmu(commentEditText.getText().toString());
                                }
                            }
                            lastComment = "";
                            PageActionTracker.clickBtn(ActionIdConstants.DANMA_SEND_REQUEST, true, "");
                            break;
                        case 100:
                        case 101:
                        case 500:
                            ToastUtils.getInstance().showShortToast("弹幕发表失败，请重新发送");
                            PageActionTracker.clickBtn(ActionIdConstants.DANMA_SEND_REQUEST, false, "");
                            break;
                        case 102:
                            break;
                        case 103:
                            ToastUtils.getInstance().showShortToast("发送弹幕太频繁啦！");
                            PageActionTracker.clickBtn(ActionIdConstants.DANMA_SEND_REQUEST, false, "");
                            break;
                        case 104:
                            ToastUtils.getInstance().showShortToast(jsonObject.get("msg").toString());
                            PageActionTracker.clickBtn(ActionIdConstants.DANMA_SEND_REQUEST, false, "");
                            break;
                        default:
                            break;
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                PageActionTracker.clickBtn(ActionIdConstants.DANMA_SEND_REQUEST, false, "");
                ToastUtils.getInstance().showShortToast("弹幕发表失败，请重新发送");
            }
        }, params);

    }

    @Override
    public void onPause() {
        super.onPause();
        recordComment();
        PageActionTracker.endPageComment(enterTime);
    }

    private void recordComment() {
        currentComments = "";
        lastComment = commentEditText.getText().toString();
        lastSelection = commentEditText.getSelectionEnd();
    }


    private void showBindPhoneDialog() {
        dismissBindPhoneDialog();
        bindPhoneDialog = AlertUtils.getInstance().showTwoBtnDialog(getActivity(),
                "为了您的个人信息安全和合法权益，请绑定手机后发布评论",
                "取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissBindPhoneDialog();
                    }
                },
                "绑定手机", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissBindPhoneDialog();
                        Intent intent = new Intent(getActivity(), BindingPhoneActivity.class);
                        startActivity(intent);
                    }
                });
    }

    private void dismissBindPhoneDialog() {
        if (bindPhoneDialog != null && bindPhoneDialog.isShowing()) {
            bindPhoneDialog.dismiss();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        lastComment = commentEditText.getText().toString();
        lastSelection = commentEditText.getSelectionEnd();
        dismissBindPhoneDialog();
        super.onDismiss(dialog);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {
            logger.error("DanmakuCommentFragment show error ! ", e);
        }
    }

    @Override
    public void onDestroyView() {
        if (getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mLoginBundleReceiver);
        }
        super.onDestroyView();
    }

    private void registerLoginBundleBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGIN_BROADCAST_FOR_COMMENT);
        filter.addAction(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mLoginBundleReceiver, filter);
    }

    private final BroadcastReceiver mLoginBundleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || TextUtils.isEmpty(intent.getAction())) {
                return;
            }
            if (intent.getAction().equals(IntentKey.LOGIN_BROADCAST_FOR_COMMENT)) {
                int loginState = intent.getIntExtra(IntentKey.LOGIN_STATE, IntentKey.LOGIN_NONE);
                if (loginState == IntentKey.LOGIN_SUCCESS) {
                    //没有绑定手机号码的话就去绑定页面
                    if (!TextUtils.isEmpty(User.getRealNameStatus()) && User.REALNAMESTATUS_BIND_NO.equals(User.getRealNameStatus())) {
//                        showBindPhoneDialog();
                    } else if (User.REALNAMESTATUS_BIND_YES.equals(User.getRealNameStatus())) {
//                        sendComment();
                    }
                } else if (loginState == IntentKey.LOGIN_FAIL) {
                    ToastUtils.getInstance().showShortToast("登录失败，请重试");
                }
            } else if (intent.getAction().equals(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT)) {
                int bundleState = intent.getIntExtra(IntentKey.BUNDLE_STATE_FOR_COMMENT, IntentKey.LOGIN_NONE);
                if (bundleState == IntentKey.BUNDLE_YES) {
//                    sendComment();
                } else if (bundleState == IntentKey.BUNDLE_NO) {
                    ToastUtils.getInstance().showShortToast("绑定失败，请重试");
                }
            }
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private VideoItem mVideoItem;
    private boolean isShowInputMethod;
    private NormalVideoHelper mVideoPlayer;
    private UniversalChannelFragment channelBaseFragment;
    private WellChosenFragment wellFragment;

    public void setCommends(String comments, String comment_id) {
        this.currentComments = comments;
        this.comment_id = comment_id;
    }

    public void setVideoItem(VideoItem model) {
        this.mVideoItem = model;
    }

    public void setNormalVideoPlayer(NormalVideoHelper normalVideoPlayer) {
        this.mVideoPlayer = normalVideoPlayer;
    }

    public void setFragment(UniversalChannelFragment fragment) {
        this.channelBaseFragment = fragment;
    }

    public void setFragment(WellChosenFragment fragment) {
        this.wellFragment = fragment;
    }

    public void setShowInputMethod(boolean isShowInputMethod) {
        this.isShowInputMethod = isShowInputMethod;
    }


}
