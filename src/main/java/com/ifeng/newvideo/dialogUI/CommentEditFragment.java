package com.ifeng.newvideo.dialogUI;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.activity.BindingPhoneActivity;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLDecoderUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.dao.LoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * 评论编辑页面
 */
public class CommentEditFragment extends DialogFragment {
    private static final Logger logger = LoggerFactory.getLogger(CommentEditFragment.class);
    private View mLayout;
    private EditText commentEditText;
    private Button send_comment_button;
    private TextView send_comment_number_indicate;
    private String currentComments;
    private String comment_id;

    public String lastComment = "";
    private boolean isShowInputMethod;
    private long enterTime;

    /**
     * 绑定手机Dialog
     */
    private Dialog bindPhoneDialog;
    private int lastSelection;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void autoFocus() {
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                logger.debug("the comment pop show");
                InputMethodManager inManager = (InputMethodManager) commentEditText.getContext().getSystemService(INPUT_METHOD_SERVICE);
                inManager.showSoftInput(commentEditText, 0);
            }
        }, 500);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerLoginBundleBroadcast();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        enterTime = System.currentTimeMillis();
        logger.debug("onResume");
        if (!TextUtils.isEmpty(currentComments)) {
            commentEditText.setText(currentComments);
        } else {
            commentEditText.setText(lastComment);
            if (!TextUtils.isEmpty(lastComment)) {
                commentEditText.setSelection(lastSelection);
            }
        }

        if (commentEditText.getText().toString().length() > 300) {
            send_comment_number_indicate.setTextColor(0xffff0000);
            send_comment_number_indicate.setText(String.format(getString(R.string.more_than),
                    String.valueOf(commentEditText.getText().toString().length() - 300)));
        } else {
            send_comment_number_indicate.setTextColor(0xffbdc0c3);
            send_comment_number_indicate.setText(commentEditText.getText().toString().length() + "/" + 300);
        }
        if (isShowInputMethod) {
            autoFocus();
        }

        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        logger.debug("onPause");
        recordComment();
        PageActionTracker.endPageComment(enterTime);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        logger.debug("onCreateView");
        mLayout = inflater.inflate(R.layout.video_comment_send_layout, container);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        commentEditText = (EditText) mLayout.findViewById(R.id.send_comment_edit);
        //发布按钮
        initCommentButton(mLayout);
        send_comment_number_indicate = (TextView) mLayout.findViewById(R.id.send_comment_number_indicate);
        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                send_comment_button.setEnabled(!TextUtils.isEmpty(s.toString()));

                if (s.toString().length() > 300) {
                    send_comment_number_indicate.setTextColor(0xffff0000);
                    send_comment_number_indicate.setText(String.format(getString(R.string.more_than),
                            String.valueOf(s.toString().length() - 300)));
                } else {
                    send_comment_number_indicate.setTextColor(0xffbdc0c3);
                    send_comment_number_indicate.setText(s.toString().length() + "/" + 300);
                }
            }
        });
        //文明评论
        TextView start_civilization = (TextView) mLayout.findViewById(R.id.start_civilization);
        // final int mLayoutBottom = mLayout.getBottom();
        start_civilization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //解决bug6599:进入退出文明上网页面，出现黑框
                // if (mLayout.getBottom() == mLayoutBottom) {
                IntentUtils.startCivilizationInternetActivity(v.getContext());
//                } else {
//                    hideSystemKeyBoard(getActivity(), v);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            IntentUtils.startCivilizationInternetActivity((BaseFragmentActivity) mActivityCallBack);
//                        }
//                    }, 500);
//
//                }

            }
        });
        //点击阴影消失
        mLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int top = mLayout.findViewById(R.id.send_comment).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top) {
                        dismissAllowingStateLoss();
                    }
                }
                return true;
            }
        });
        return mLayout;
    }

    private static void hideSystemKeyBoard(Context context, View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onDestroyView() {
        if (getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mLoginBundleReceiver);
        }
        super.onDestroyView();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {
            logger.error("CommitDialogFragment show error ! ", e);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        lastComment = commentEditText.getText().toString();
        lastSelection = commentEditText.getSelectionEnd();
        dismissBindPhoneDialog();
        super.onDismiss(dialog);
        if (getActivity() instanceof ActivityVideoPlayerDetail) {
            ActivityVideoPlayerDetail instance = (ActivityVideoPlayerDetail) getActivity();
            instance.autoNextProgramme();
        }
    }

    /**
     * 初始化发布按钮
     */
    private void initCommentButton(View view) {
        //操作网络请求
        send_comment_button = (Button) view.findViewById(R.id.send_comment_button);
        send_comment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClickUtils.isFastDoubleClick() || getActivity() == null) {
                    return;
                }
                if (!NetUtils.isNetAvailable(getActivity())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (!User.isLogin()) {
                    ToastUtils.getInstance().showShortToast("亲，登录后可发布评论");
                    IntentUtils.startLoginActivity(getActivity());
                    return;
                } else {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_SEND, PageIdConstants.PLAY_VIDEO_V);
                    // 接口
                    String params = "&sid=" + User.getIfengToken() + "&deviceId=" + PhoneConfig.userKey + "&ts=" + User.getTimeStamp();
                    LoginDao.requestServerCheckLogin(new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            if (response == null || TextUtils.isEmpty(response.toString())) {
                                ToastUtils.getInstance().showShortToast(R.string.comment_send_error);
                                return;
                            }
                            String message = null;
                            String messageCode = null;
                            int vipStatus = 0;
                            String vipDate = null;
                            String sign = "";
                            try {
                                JSONObject jsonObject = null;
                                jsonObject = JSON.parseObject(response.toString());
                                message = URLDecoderUtils.decodeInUTF8(jsonObject.getString("message"));
                                messageCode = jsonObject.getString("msgcode");
                                vipStatus = jsonObject.getIntValue("VIPStatus");
                                vipDate = jsonObject.getString("VIPEXP");
                                sign = jsonObject.getString("sign");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            //等于“0”表示操作成功，已登录
                            if (LoginDao.LOGIN_SUCCESS.equals(messageCode)) {
                                //没有绑定手机号码的话就去绑定页面
                                if (!TextUtils.isEmpty(User.getRealNameStatus()) && User.REALNAMESTATUS_BIND_NO.equals(User.getRealNameStatus())) {
                                    showBindPhoneDialog();
                                    return;
                                }
                                sendComment();
                                new User(vipStatus, vipDate, sign).storeMemberStatus();
                            }
                            // 已下线
                            else if (LoginDao.LOGIN_ON_ANOTHER_DEVICE.equals(messageCode)) {
                                IntentUtils.startLoginActivity(getActivity());
                                ToastUtils.getInstance().showShortToast("亲，登录后可发布评论");
                                User.clearUserInfo(); //清空本地用户数据
                            } else {
                                if (TextUtils.isEmpty(message)) {
                                    message = getString(R.string.comment_send_error);
                                }
                                ToastUtils.getInstance().showShortToast(message);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ToastUtils.getInstance().showShortToast(R.string.comment_send_error);
                        }
                    }, params);
                }

            }
        });
    }

    private void recordComment() {
        currentComments = "";
        lastComment = commentEditText.getText().toString();
        lastSelection = commentEditText.getSelectionEnd();
    }

    private void sendComment() {
        if (commentEditText.getText().toString().length() > 300) {
            ToastUtils.getInstance().showShortToast("亲，您已经超出字数了");
            return;
        }
        final Activity hostActivity = getActivity();

        if (hostActivity == null) {
            return;
        }

        String params = null;
        try {
            String guid = null;
            String encodeGuid = null;
            String encodeName = null;
            String encodeContent = null;
            String key = "";

            if (hostActivity instanceof ActivityTopicPlayer) {
                if (TextUtils.isEmpty(topicShareUrl)) {
                    dismissAllowingStateLoss();
                    ToastUtils.getInstance().showShortToast("评论发表失败，请重新发送");
                    return;
                }

                guid = topicShareUrl;
                encodeGuid = URLEncoderUtils.encodeInUTF8(guid);
                encodeName = URLEncoderUtils.encodeInUTF8(TextUtils.isEmpty(homeTitle) ? "" : homeTitle.trim());
                encodeContent = URLEncoderUtils.encodeInUTF8(commentEditText.getText().toString());
                key = StringUtils.md5s("Ifeng888" + encodeName + encodeGuid).substring(2, 8);
            } else {

                if (mVideoItem == null || TextUtils.isEmpty(mVideoItem.guid)) {
                    dismissAllowingStateLoss();
                    ToastUtils.getInstance().showShortToast("评论发表失败，请重新发送");
                    return;
                }

                guid = mVideoItem.guid;
                encodeGuid = URLEncoderUtils.encodeInUTF8(guid);
                encodeName = URLEncoderUtils.encodeInUTF8(TextUtils.isEmpty(mVideoItem.name) ? "" : mVideoItem.name.trim());
                encodeContent = URLEncoderUtils.encodeInUTF8(commentEditText.getText().toString());
                key = StringUtils.md5s("Ifeng888" + encodeName + encodeGuid).substring(2, 8);
            }


            User user = new User(hostActivity);
            String nickName = "";
            String userName = user.getIfengUserName();
            if (!TextUtils.isEmpty(userName)) {
                nickName = URLEncoderUtils.encodeInUTF8(userName);
            }

            String uid = User.getUid();
            if (uid == null) {
                uid = "";
            }
            String commentVerify = "";
            Bundle arguments = getArguments();
            if (arguments != null) {
                commentVerify = String.valueOf(arguments.getCharSequence(IntentKey.E_CHID));
            }
            if (TextUtils.isEmpty(commentVerify)) {
                commentVerify = "";
            }

            String ext2 = JSON.toJSONString(new Ext2(uid, nickName, commentVerify));

            params = "?docUrl=" + encodeGuid +
                    "&docName=" + encodeName +
                    "&content=" + encodeContent +
                    "&skey=" + key +
                    "&channelId=50" +
                    "&rt=client_v" +
                    "&quoteId=" + comment_id +
                    "&ext2=" + ext2 +
                    "&sid=" + User.getIfengToken();
        } catch (UnsupportedEncodingException e) {
            logger.debug("sendComment error!! {}", e);
        }

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);

        }

        dismissAllowingStateLoss();
        ToastUtils.getInstance().showShortToast("正在发布...");

        CommentDao.publishComment(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if (response == null || TextUtils.isEmpty(response.toString())) {
                    ToastUtils.getInstance().showShortToast(R.string.comment_send_error);
                    return;
                }
                JSONObject jsonObject = null;
                try {
                    jsonObject = JSON.parseObject(response.toString());
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    ToastUtils.getInstance().showShortToast(R.string.comment_send_error);
                    return;
                }

                if (jsonObject != null && jsonObject.getInteger("code") == 1) {
                    lastComment = "";
                    ToastUtils.getInstance().showShortToast(R.string.comment_send_success);
                    Log.d("addLocalData", "addLocalData:" + (getActivity() instanceof ActivityVideoPlayerDetail));
                    Log.d("addLocalData", "addLocalData:" + getActivity());
                    //更新adapter
                    if (hostActivity instanceof ActivityVideoPlayerDetail) {
                        ActivityVideoPlayerDetail act = (ActivityVideoPlayerDetail) hostActivity;
                        act.updateLocalData(commentEditText.getText().toString());
                        act.sendCommentAction();
                    }

                    if (hostActivity instanceof ActivityTopicPlayer) {
                        ActivityTopicPlayer topicPlayer = (ActivityTopicPlayer) hostActivity;
                        topicPlayer.updateLocalData(commentEditText.getText().toString());
                        topicPlayer.sendCommentAction();
                    }

                    UserPointManager.addRewards(UserPointManager.PointType.addByComment);

                } else {
                    String msg = getResult(response.toString(), "message");
                    if (TextUtils.isEmpty(msg)) {
                        msg = getString(R.string.comment_send_error);
                    }
                    ToastUtils.getInstance().showShortToast(msg);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtils.getInstance().showShortToast(R.string.comment_send_error);
            }
        }, params);
    }

    /**
     * 发布评论需要的参数
     */
    static class Ext2 {

        // 视频id
        public String guid;
        // 用户昵称
        public String nickname;
        // 来源
        public String from = "sj";
        // 固定值
        public String type = "doc";
        // 客户端频道标识 形如 100409-0之类 为每个频道的编辑分类
        public String comment_verify;

        /**
         * @param guid          用户id
         * @param nickName      用户昵称
         * @param commentVerify 客户端频道标识 （即echid）,形如 100409-0之类 为每个频道的编辑分类
         */
        public Ext2(String guid, String nickName, String commentVerify) {
            this.guid = guid;
            this.nickname = nickName;
            this.comment_verify = commentVerify;
        }

    }


    private String getResult(String jsonString, String msg) {
        String result = null;
        try {
            JSONObject object = JSON.parseObject(jsonString);
            result = object.getString(msg);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return result;
    }

    public void setCommends(String comments, String comment_id) {
        this.currentComments = comments;
        this.comment_id = comment_id;
    }


    /**
     * 登陆和绑定广播接收器
     */
    private final BroadcastReceiver mLoginBundleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || TextUtils.isEmpty(intent.getAction())) {
                return;
            }
            if (intent.getAction().equals(IntentKey.LOGIN_BROADCAST_FOR_COMMENT)) {
                int loginState = intent.getIntExtra(IntentKey.LOGIN_STATE, IntentKey.LOGIN_NONE);
                if (loginState == IntentKey.LOGIN_SUCCESS) {
                    //没有绑定手机号码的话就去绑定页面
                    if (!TextUtils.isEmpty(User.getRealNameStatus()) && User.REALNAMESTATUS_BIND_NO.equals(User.getRealNameStatus())) {
                        showBindPhoneDialog();
                    } else if (User.REALNAMESTATUS_BIND_YES.equals(User.getRealNameStatus())) {
                        sendComment();
                    }
                } else if (loginState == IntentKey.LOGIN_FAIL) {
                    ToastUtils.getInstance().showShortToast("登录失败，请重试");
                }
            } else if (intent.getAction().equals(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT)) {
                int bundleState = intent.getIntExtra(IntentKey.BUNDLE_STATE_FOR_COMMENT, IntentKey.LOGIN_NONE);
                if (bundleState == IntentKey.BUNDLE_YES) {
                    sendComment();
                } else if (bundleState == IntentKey.BUNDLE_NO) {
                    ToastUtils.getInstance().showShortToast("绑定失败，请重试");
                }
            }
        }
    };

    private void showBindPhoneDialog() {
        dismissBindPhoneDialog();
        bindPhoneDialog = AlertUtils.getInstance().showTwoBtnDialog(getActivity(),
                "为了您的个人信息安全和合法权益，请绑定手机后发布评论",
                "取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissBindPhoneDialog();
                    }
                },
                "绑定手机", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissBindPhoneDialog();
                        Intent intent = new Intent(getActivity(), BindingPhoneActivity.class);
                        startActivity(intent);
                    }
                });
    }

    private void dismissBindPhoneDialog() {
        if (bindPhoneDialog != null && bindPhoneDialog.isShowing()) {
            bindPhoneDialog.dismiss();
        }
    }

    private void registerLoginBundleBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGIN_BROADCAST_FOR_COMMENT);
        filter.addAction(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mLoginBundleReceiver, filter);
    }


    private VideoItem mVideoItem;
    private String topicShareUrl, homeTitle;

    public void setVideoItem(VideoItem model) {
        this.mVideoItem = model;
    }

    public void setTopicId(String topicShareUrl, String homeTitle) {
        this.topicShareUrl = topicShareUrl;
        this.homeTitle = homeTitle;
    }


    public void setShowInputMethod(boolean isShowInputMethod) {
        this.isShowInputMethod = isShowInputMethod;
    }


}
