
package com.ifeng.newvideo.dialogUI;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MobileNetAlert extends BaseFragmentActivity {
    private static final Logger logger = LoggerFactory.getLogger(MobileNetAlert.class);
    private DialogUtilsFor3G m3GDialogUtils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout rootLay = new RelativeLayout(this);
        rootLay.setBackgroundResource(R.color.transparent);
        rootLay.setBackgroundColor(getResources().getColor(R.color.transparent));
        this.setContentView(rootLay);
        String action = getIntent().getAction();
        if (m3GDialogUtils == null) {
            finish();
        }
        if (ActionIdConstants.AUDIO_NET_3G.equalsIgnoreCase(action)) {
            audioChange3GDialog();
        } else {
            finish();
        }

    }

    private void audioChange3GDialog() {
        if (m3GDialogUtils != null) {
            logger.debug("audioChange3GDialog");
            m3GDialogUtils.show3GNetAlertDialog(this);
            if (m3GDialogUtils.m3GNetAlertDialog != null) {
                m3GDialogUtils.m3GNetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
