package com.ifeng.newvideo.dialogUI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @describe dialog工具
 * Created by antboyqi on 14-7-21.
 */
public class DialogUtil {
    private static final Logger logger = LoggerFactory.getLogger(DialogUtil.class);

    /*
    * @param  由theme 定制dialog的title等值
    *
    * */
    public static Dialog setupDialog(int themeResId, Activity activity, OnDialogListener listener) {
        if (activity == null)
            throw new IllegalArgumentException("create dialog,the attached activity is null");
        if (themeResId < 0x01000000) {
            throw new IllegalArgumentException("create dialog,the themeResId is not resource id");
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        Resources.Theme theme = activity.getResources().newTheme();
        theme.applyStyle(R.style.dialogTheme, true);
        TypedArray typedArray = theme.obtainStyledAttributes(R.styleable.common_dialog_styleable);

        int len = typedArray.getIndexCount();
        String str;
        for (int x = 0; x < len; x++) {
            switch (typedArray.getIndex(x)) {
                case R.styleable.common_dialog_styleable_dialogMessage:
                    if (!TextUtils.isEmpty(str = typedArray.getString(x))) builder.setMessage(str);
                    break;
                case R.styleable.common_dialog_styleable_dialogTitle:
                    if (!TextUtils.isEmpty(str = typedArray.getString(x))) builder.setTitle(str);
                    break;
                case R.styleable.common_dialog_styleable_dialogIcon:
                    break;
                case R.styleable.common_dialog_styleable_negativeButtonText:
                    if (!TextUtils.isEmpty(str = typedArray.getString(x))) builder.setNegativeButton(str, listener);
                    break;
                case R.styleable.common_dialog_styleable_positiveButtonText:
                    if (!TextUtils.isEmpty(str = typedArray.getString(x))) builder.setPositiveButton(str, listener);
                    break;
                case R.styleable.common_dialog_styleable_neutralButtonText:
                    if (!TextUtils.isEmpty(str = typedArray.getString(x))) builder.setNeutralButton(str, listener);
                    break;
                case R.styleable.common_dialog_styleable_cancelable:
                    builder.setCancelable(typedArray.getBoolean(x, true));
                    break;
            }
        }
        builder.setOnKeyListener(listener);
        return builder.create();
    }

    /*
  * @param  title message,textPositiveBtn,textNegativeBtn,textNeutralBtn
  * */
    public static Dialog setupDialog(Activity activity, int imgRes, String title, String message, String textPositiveBtn, String textNegativeBtn, String textNeutralBtn, boolean isCancelAble, OnDialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if (imgRes > 0) builder.setIcon(imgRes);
        if (!TextUtils.isEmpty(title)) builder.setTitle(title);
        if (!TextUtils.isEmpty(message)) builder.setMessage(message);
        if (!TextUtils.isEmpty(textNegativeBtn)) builder.setNegativeButton(textNegativeBtn, listener);
        if (!TextUtils.isEmpty(textPositiveBtn)) builder.setPositiveButton(textPositiveBtn, listener);
        if (!TextUtils.isEmpty(textNeutralBtn)) builder.setNeutralButton(textNeutralBtn, listener);
        builder.setCancelable(isCancelAble);
        builder.setOnKeyListener(listener);
        return builder.create();
    }


    /**
     * 两个按钮Dialog
     *
     * @param context
     * @param leftOnClickListener  取消按钮事件
     * @param rightOnClickListener 下载按钮事件
     */
    public static Dialog showDownloadAppNewsDialog(Context context, View.OnClickListener leftOnClickListener,
                                                   View.OnClickListener rightOnClickListener) {
        try {
            if (context == null) {
                return null;
            }
            LayoutInflater inflater = LayoutInflater.from(context);
            View layout = inflater.inflate(R.layout.dialog_notice_install_news_layout, null);
            View closeButton = layout.findViewById(R.id.close_btn);
            View leftButton = layout.findViewById(R.id.left_btn);
            leftButton.setOnClickListener(leftOnClickListener);
            closeButton.setOnClickListener(leftOnClickListener);
            View rightButton = layout.findViewById(R.id.right_btn);
            rightButton.setOnClickListener(rightOnClickListener);

            Dialog dialog = new Dialog(context, R.style.transparentDialog);
            Window window = dialog.getWindow();
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = (int) (DisplayUtils.getWindowWidth() * 0.71);
            window.setAttributes(lp);
            dialog.setContentView(layout, lp);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return dialog;
        } catch (Exception e) {
            logger.error("showDownloadAppNewsDialog error! {}", e);
            return null;
        }
    }

    public interface OnDialogListener extends DialogInterface.OnClickListener, Dialog.OnKeyListener {
    }
}
