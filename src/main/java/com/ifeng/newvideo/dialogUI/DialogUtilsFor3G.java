package com.ifeng.newvideo.dialogUI;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.AlertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 专为播放，缓存的3G提示Dialog类
 * Created by yuelong on 2014/10/27.
 */
public class DialogUtilsFor3G {

    private static final Logger logger = LoggerFactory.getLogger(DialogUtilsFor3G.class);
    private DialogCallBackFor3G mDialogCallBack;

    public interface DialogCallBackFor3G {
        void onClickDialogBtn(int flag);
    }

    public static final int FLAG_UNICOM_DIALOG_CANCEL = 10;
    public static final int FLAG_UNICOM_DIALOG_CONTINUE = 11;
    public static final int FLAG_UNICOM_DIALOG_ORDER = 12;
    public static final int FLAG_UNICOM_DIALOG_RETRY = 13;
    public static final int FLAG_UNICOM_DIALOG_CONTINUE_DOWNLOAD = 14;
    public static final int FLAG_UNICOM_DIALOG_CANCEL_DOWNLOAD = 15;
    public static final int FLAG_DIALOG_CANCEL = 16;
    public static final int FLAG_DIALOG_CONTINUE = 17;
    public static final int FLAG_DIALOG_NO_MOBILE_OPEN = 18;
    public static final int FLAG_DIALOG_WAP_SETTING = 19;
    public boolean mBusinessVideoHasShow = false;
    private boolean mIsNoMobileNetOpenDialogShown = false;
    public boolean m3GNetDialogHasShow = false;
    public Dialog businessVideoDialog;
    public Dialog mCurMobileWapDialog = null;

    public void reset() {
        mBusinessVideoHasShow = false;
        mIsNoMobileNetOpenDialogShown = false;
        m3GNetDialogHasShow = false;
    }

    public void setDialogCallBack(DialogCallBackFor3G mDialogCallBack) {
        this.mDialogCallBack = mDialogCallBack;
    }

    public Dialog m3GNetAlertDialog;

    public boolean show3GNetAlertDialog(Context context) {
        if (m3GNetDialogHasShow) {
            return false;
        }
        if (m3GNetAlertDialog != null && m3GNetAlertDialog.isShowing()) {
            m3GNetAlertDialog.dismiss();
        }
        String content = context.getString(R.string.play_alert_text);
        String btnLabelCancel = context.getString(R.string.btn_cancel);
        String btnLabelOk = context.getString(R.string.continue_play);
        m3GNetAlertDialog = AlertUtils.getInstance().showTwoBtnDialog(context, content, btnLabelOk, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m3GNetDialogHasShow = true;
                m3GNetAlertDialog.dismiss();
                if (mDialogCallBack != null) {
                    mDialogCallBack.onClickDialogBtn(FLAG_DIALOG_CONTINUE);
                }
            }
        }, btnLabelCancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m3GNetDialogHasShow = false;
                m3GNetAlertDialog.dismiss();
                if (mDialogCallBack != null) {
                    mDialogCallBack.onClickDialogBtn(FLAG_DIALOG_CANCEL);
                }
            }
        });
        m3GNetDialogHasShow = true;
        m3GNetAlertDialog.setCancelable(false);
        m3GNetAlertDialog.setCanceledOnTouchOutside(false);
        return true;
    }

    private Dialog cacheNotWifiAlertDialog;

    public boolean showNotUnicomDownloadDialog(Context context) {
        if (m3GNetDialogHasShow) {
            return false;
        }
        if (cacheNotWifiAlertDialog != null && cacheNotWifiAlertDialog.isShowing()) {
            return true;
        }
        String content = context.getString(R.string.cache_alert_text);
        String btnLabelCancel = context.getString(R.string.btn_cancel);
        String btnLabelOk = context.getString(R.string.continue_down);
        cacheNotWifiAlertDialog = AlertUtils.getInstance().showTwoBtnDialog(context, content, btnLabelOk, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m3GNetDialogHasShow = true;
                if (cacheNotWifiAlertDialog != null && cacheNotWifiAlertDialog.isShowing()) {
                    cacheNotWifiAlertDialog.dismiss();
                }
                if (mDialogCallBack != null) {
                    mDialogCallBack.onClickDialogBtn(FLAG_DIALOG_CONTINUE);
                }
            }
        }, btnLabelCancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m3GNetDialogHasShow = false;
                if (cacheNotWifiAlertDialog != null && cacheNotWifiAlertDialog.isShowing()) {
                    cacheNotWifiAlertDialog.dismiss();
                }
                if (mDialogCallBack != null) {
                    mDialogCallBack.onClickDialogBtn(FLAG_DIALOG_CANCEL);
                }
            }
        });
        cacheNotWifiAlertDialog.setCancelable(false);
        cacheNotWifiAlertDialog.setCanceledOnTouchOutside(false);
        return true;
    }

    public Dialog mNoMobileOpenDialog;

    public void showNoMobileOpenDialog(Context context) {
        if (!mIsNoMobileNetOpenDialogShown) {
            mIsNoMobileNetOpenDialogShown = true;
            String content = context.getString(R.string.dialog_download_wifi_only);
            String btnLabelOK = context.getString(R.string.btn_iknow);
            mNoMobileOpenDialog = AlertUtils.getInstance().showOneBtnDialog(context, content, btnLabelOK, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIsNoMobileNetOpenDialogShown = false;
                    if (mNoMobileOpenDialog != null && mNoMobileOpenDialog.isShowing()) {
                        mNoMobileOpenDialog.dismiss();
                    }
                    if (mDialogCallBack != null) {
                        mDialogCallBack.onClickDialogBtn(FLAG_DIALOG_NO_MOBILE_OPEN);
                    }
                }
            });
            mNoMobileOpenDialog.setCancelable(false);
            mNoMobileOpenDialog.setCanceledOnTouchOutside(false);
        }
    }

    public static Dialog showCacheVideo4OperatorDialog(Context context, final View.OnClickListener upBtnOnClickListener, final View.OnClickListener downBtnOnClickListener) {
        return AlertUtils.getInstance().showTwoBtnDialog(context, "运营商网络缓存可能会导致超额流量，确认开启？",
                "开启", upBtnOnClickListener, "取消", downBtnOnClickListener);
    }

    public static Dialog showContinueCacheVideoDialog(Context context, final View.OnClickListener upBtnOnClickListener, final View.OnClickListener downBtnOnClickListener) {
        return AlertUtils.getInstance().showTwoBtnDialog(context, "正在使用运营商网络缓存，可能产生超额流量费",
                "继续缓存", upBtnOnClickListener, "取消", downBtnOnClickListener);
    }

}
