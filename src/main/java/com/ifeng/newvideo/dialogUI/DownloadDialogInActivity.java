package com.ifeng.newvideo.dialogUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.ifeng.newvideo.ui.basic.BaseActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 缓存的对话框，activity形式，用于后台切网
 * Created by cuihz on 2014/11/27.
 */
public class DownloadDialogInActivity extends BaseActivity {

    private static final Logger logger = LoggerFactory.getLogger(DownloadDialogInActivity.class);

    public static DownloadDialogInActivity downloadDialogInActivity;
    DialogUtilsFor3G dialogUtilsFor3G;

    public static final String KEY_ACTION = "kay_action";
    public static final String ACTION_WAP = "action_wap";
    public static final String ACTION_UNVIP = "action_unvip";
    public static final String ACTION_NOTUNICOM = "action_notunicom";
    public static final String ACTION_NO_MOBILE_OPEN = "action_no_mobile_open";


    public static final String ACTION_DIALOG_CANCEL = "action_dialog_cancel";
    public static final String ACTION_DIALOG_CONTINUE = "action_dialog_continue";
    public static final String ACTION_DIALOG_ORDER = "action_dialog_order";
    public static final String ACTION_DIALOG_NOACTION = "action_dialog_noaction";

    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        if (downloadDialogInActivity != null) {
            downloadDialogInActivity.finish();
        }
        downloadDialogInActivity = this;
        dialogUtilsFor3G = new DialogUtilsFor3G();
        dialogUtilsFor3G.setDialogCallBack(new DialogUtilsFor3G.DialogCallBackFor3G() {
            @Override
            public void onClickDialogBtn(int flag) {
                Intent intent;
                switch (flag) {
                    case DialogUtilsFor3G.FLAG_DIALOG_CANCEL:
                    case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CANCEL_DOWNLOAD:
                        logger.debug("activity dialog call back : cancel");
                        intent = new Intent(ACTION_DIALOG_CANCEL);
                        sendBroadcast(intent);
                        DownloadDialogInActivity.this.finish();
                        break;
                    case DialogUtilsFor3G.FLAG_DIALOG_CONTINUE:
                    case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CONTINUE_DOWNLOAD:
                        logger.debug("activity dialog call back : continue");
                        intent = new Intent(ACTION_DIALOG_CONTINUE);
                        sendBroadcast(intent);
                        DownloadDialogInActivity.this.finish();
                        break;
                    case DialogUtilsFor3G.FLAG_DIALOG_NO_MOBILE_OPEN:
                        logger.debug("activity dialog call back : no_mobile_open");
                        intent = new Intent(ACTION_DIALOG_NOACTION);
                        sendBroadcast(intent);
                        DownloadDialogInActivity.this.finish();
                        break;
                    case DialogUtilsFor3G.FLAG_DIALOG_WAP_SETTING:
                        logger.debug("activity dialog call back : setting");
                        intent = new Intent(ACTION_DIALOG_NOACTION);
                        sendBroadcast(intent);
                        DownloadDialogInActivity.this.finish();
                        break;
                }
            }
        });

        String action = getIntent().getStringExtra(KEY_ACTION);
        logger.debug(action);
        if (action.equals(ACTION_NO_MOBILE_OPEN)) {
            dialogUtilsFor3G.showNoMobileOpenDialog(this);
        }
        if (action.equals(ACTION_NOTUNICOM)) {
            dialogUtilsFor3G.showNotUnicomDownloadDialog(this);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
