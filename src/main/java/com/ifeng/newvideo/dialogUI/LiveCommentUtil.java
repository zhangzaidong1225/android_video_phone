package com.ifeng.newvideo.dialogUI;


import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.live.weblive.JsBridge;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LiveCommentUtil extends DialogFragment {
    private static final Logger logger = LoggerFactory.getLogger(LiveCommentUtil.class);

    private View mLayout;
    private EditText commentEditText;
    private Button send_comment_button;
    private TextView send_comment_number_indicate;
    private boolean isShowInputMethod;
    private long enterTime;
    private String currentComments;

    public String lastComment = "";

    private JsBridge mJsBridge;

    private int lastSelection;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void autoFocus() {
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                logger.debug("the comment pop show");
                InputMethodManager inManager = (InputMethodManager) commentEditText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inManager.showSoftInput(commentEditText, 0);
            }
        }, 500);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerLoginBundleBroadcast();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.h5_comment_send_layout, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        commentEditText = (EditText) mLayout.findViewById(R.id.send_comment_edit_h5);
        commentEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(40)});

        send_comment_button = (Button) mLayout.findViewById(R.id.send_comment_button_h5);
        send_comment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ClickUtils.isFastDoubleClick() || getActivity() == null) {
                    return;
                }
                if (!NetUtils.isNetAvailable(getActivity())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (!User.isLogin()) {
                    ToastUtils.getInstance().showShortToast("亲，登录后可发布评论");
                    IntentUtils.startLoginActivity(getActivity());
                    return;
                } else {
                    //TODO 发表评论
                    submitComment();
                }
            }
        });

        send_comment_number_indicate = (TextView) mLayout.findViewById(R.id.send_comment_number_indicate);
        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                send_comment_button.setEnabled(!TextUtils.isEmpty(s.toString()));

                if (s.toString().length() > 40) {
                    send_comment_number_indicate.setTextColor(0xffff0000);
                    send_comment_number_indicate.setText(String.format(getString(R.string.more_than),
                            String.valueOf(s.toString().length() - 40)));
                } else {
                    send_comment_number_indicate.setTextColor(0xffbdc0c3);
                    send_comment_number_indicate.setText(s.toString().length() + "/" + 40);
                }
            }
        });
        TextView start_civilization = (TextView) mLayout.findViewById(R.id.start_civilization);
        start_civilization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtils.startCivilizationInternetActivity(view.getContext());
            }
        });
        //点击阴影消失
        mLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int top = mLayout.findViewById(R.id.send_comment).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top) {
                        dismissAllowingStateLoss();
                    }
                }
                return true;
            }
        });


        return mLayout;
    }

    private void registerLoginBundleBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGIN_BROADCAST_FOR_COMMENT);
        filter.addAction(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mLoginBundleReceiver, filter);
    }

    /**
     * 登陆和绑定广播接收器
     */
    private final BroadcastReceiver mLoginBundleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || TextUtils.isEmpty(intent.getAction())) {
                return;
            }
            if (intent.getAction().equals(IntentKey.LOGIN_BROADCAST_FOR_COMMENT)) {
                int loginState = intent.getIntExtra(IntentKey.LOGIN_STATE, IntentKey.LOGIN_NONE);
                if (loginState == IntentKey.LOGIN_SUCCESS) {
                    submitComment();
                    //没有绑定手机号码的话就去绑定页面
//                    if (!TextUtils.isEmpty(User.getRealNameStatus()) && User.REALNAMESTATUS_BIND_NO.equals(User.getRealNameStatus())) {
//                        showBindPhoneDialog();
//                    } else if (User.REALNAMESTATUS_BIND_YES.equals(User.getRealNameStatus())) {
//                        sendComment();
//                    }
                } else if (loginState == IntentKey.LOGIN_FAIL) {
                    ToastUtils.getInstance().showShortToast("登录失败，请重试");
                }
            } else if (intent.getAction().equals(IntentKey.BUNDLE_BROADCAST_FOR_COMMENT)) {
                int bundleState = intent.getIntExtra(IntentKey.BUNDLE_STATE_FOR_COMMENT, IntentKey.LOGIN_NONE);
                if (bundleState == IntentKey.BUNDLE_YES) {
                    submitComment();
                } else if (bundleState == IntentKey.BUNDLE_NO) {
                    ToastUtils.getInstance().showShortToast("绑定失败，请重试");
                }
            }
        }
    };

    public void setJSBridge(JsBridge jsBridge) {
        this.mJsBridge = jsBridge;
    }

    public void setCommends(String comments) {
        this.currentComments = comments;
    }

    @Override
    public void onResume() {
        enterTime = System.currentTimeMillis();
        logger.debug("onResume");
        if (!TextUtils.isEmpty(currentComments)) {
            commentEditText.setText(currentComments);
        } else {
            commentEditText.setText(lastComment);
            if (!TextUtils.isEmpty(lastComment)) {
                commentEditText.setSelection(lastSelection);
            }
        }

        if (commentEditText.getText().toString().length() > 40) {
            send_comment_number_indicate.setTextColor(0xffff0000);
            send_comment_number_indicate.setText(String.format(getString(R.string.more_than),
                    String.valueOf(commentEditText.getText().toString().length() - 40)));
        } else {
            send_comment_number_indicate.setTextColor(0xffbdc0c3);
            send_comment_number_indicate.setText(commentEditText.getText().toString().length() + "/" + 40);
        }
        if (isShowInputMethod) {
            autoFocus();
        }

        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        logger.debug("onPause");
        recordComment();
        PageActionTracker.endPageComment(enterTime);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {
            logger.error("CommitDialogFragment show error ! ", e);
        }
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        lastComment = commentEditText.getText().toString();
        lastSelection = commentEditText.getSelectionEnd();

    }

    @Override
    public void onDestroy() {
        if (getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mLoginBundleReceiver);
        }
        super.onDestroy();

    }

    private void recordComment() {
        currentComments = "";
        lastComment = commentEditText.getText().toString();
        lastSelection = commentEditText.getSelectionEnd();
    }

    public void setShowInputMethod(boolean isShowInputMethod) {
        this.isShowInputMethod = isShowInputMethod;

    }


    /**
     * 发表评论
     */
    private void submitComment() {
        doSubmitCommentModule(commentEditText.getText().toString());
        commentEditText.setText("");
        dismiss();
    }

    private void doSubmitCommentModule(String comment) {
        if (!NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(R.string.comment_send_error);
        } else {
            sendComment(comment);
        }
    }

    private void sendComment(String comment) {
        mJsBridge.performInputCallBack(getInputResultJson(comment));
        commentEditText.setText("");
        dismiss();
        isShowInputMethod = false;
    }


    private String getInputResultJson(String comment) {
        JSONObject root = new JSONObject();
        try {
            if (TextUtils.isEmpty(comment)) {
                root.put("text", "");
            } else {
                root.put("text", comment);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root.toString();
    }


}
