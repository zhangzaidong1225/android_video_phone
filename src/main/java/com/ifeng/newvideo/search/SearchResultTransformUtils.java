package com.ifeng.newvideo.search;

import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.HomePageModel;
import com.ifeng.video.dao.db.model.SearchResultListModel;
import com.ifeng.video.dao.db.model.SearchResultModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索model转换工具类
 * Created by cuihz on 2015/1/5.
 */
public class SearchResultTransformUtils {
    private static final Logger logger = LoggerFactory.getLogger(SearchResultTransformUtils.class);

    public static SearchResultModel resultListItemToSearchResultItem(SearchResultListModel.ItemsEntity listItem) {
        SearchResultModel resultItem = new SearchResultModel();
        String guid = listItem.getDocumentid();
        if (guid.startsWith("v_")) {
            guid = guid.substring(2);
        }
        resultItem.setGuid(guid);
        resultItem.setCreateDate(listItem.getDate());
        resultItem.setImgUrl(listItem.getPosterurl());
        resultItem.setTitle(listItem.getTitle());
        resultItem.setType(IfengType.TYPE_VIDEO);
        resultItem.setDuration(listItem.getDuration());
        resultItem.setMediaid(listItem.getMediaid());
        resultItem.setMedianame(listItem.getMedianame());
        resultItem.setPlayTime(listItem.getPlayTime());
        resultItem.setMediaHeadPic(listItem.getMediaHeadPic());
        resultItem.setYvId(listItem.getYvId());
        return resultItem;
    }

    public static SearchResultModel recommendItemToSearchResultItem(HomePageModel.BodyRecommend.RecommendList.GroupList groupList) {
        SearchResultModel resultItem = new SearchResultModel();
        if (groupList == null) {
            return null;
        }
        String memberType = groupList.getMemberType();
        if (CheckIfengType.isVideo(memberType)) {
            HomePageModel.MemberItem memberItem = groupList.getMemberItem();
            if (memberItem == null) {
                return null;
            }
            resultItem.setCreateDate(memberItem.getCreateDate());
            resultItem.setGuid(memberItem.getGuid());
            if (groupList.getPlayTime() != null) {
                resultItem.setDuration("播放：" + StringUtils.changePlayTimes(String.valueOf(groupList.getPlayTime())));
            }
        } else if (CheckIfengType.isTopicType(memberType)) {
            resultItem.setCreateDate(groupList.getAbstractDesc());
            resultItem.setGuid(groupList.getMemberId());
        } else if (CheckIfengType.isColumn(memberType)) {
            resultItem.setGuid(groupList.getMemberId());
        } else {
            return null;
        }
        resultItem.setType(memberType);
        resultItem.setTitle(groupList.getTitle());
        resultItem.setImgUrl(groupList.getImage());
        return resultItem;
    }

    /**
     * 搜索结果实体类转换
     */
    public static List<SearchResultModel> resultListItemToSearchResultItem(List<SearchResultListModel.ItemsEntity> list) {
        List<SearchResultModel> tempList = new ArrayList<>();
        for (SearchResultListModel.ItemsEntity item : list) {
            tempList.add(resultListItemToSearchResultItem(item));
        }
        return tempList;
    }

    /*public static SearchResultItem recommendItemToSearchResultItem(Context context, SubChannelInfoModel.BodyList.VideoList videoItem){
        SearchResultItem resultItem = new SearchResultItem();
        if(CheckIfengType.isVideo(videoItem.getMemberType())){
            SubChannelInfoModel.MemberItem memberItem = videoItem.getMemberItem();
            if(memberItem == null){
                return null;
            }
            resultItem.setCreateDate(memberItem.getCreateDate());
            resultItem.setGuid(memberItem.getGuid());
            if(videoItem.getPlayTime() != null){
                resultItem.setDuration(context.getResources().getString(R.string.video_play) + StringUtils.changePlayTimes(String.valueOf(videoItem.getPlayTime())));
            }
        }
        else if(CheckIfengType.isTopicType(videoItem.getMemberType())){
            resultItem.setCreateDate(videoItem.getAbstractDesc());
            resultItem.setGuid(videoItem.getMemberId());
        }
        else{
            return null;
        }
        resultItem.setType(videoItem.getMemberType());
        resultItem.setTitle(videoItem.getTitle());
        resultItem.setImgUrl(videoItem.getImage());
        return resultItem;
    }*/
}
