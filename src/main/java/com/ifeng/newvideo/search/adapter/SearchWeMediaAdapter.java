package com.ifeng.newvideo.search.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.subscribe.WeMediaList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索自媒体适配器
 * Created by Administrator on 2016/7/21.
 */
public class SearchWeMediaAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SearchWeMediaAdapter.class);

    public static final int SIZE_THREE = 3;//展示三个
    public static final int SIZE_MAX = Integer.MAX_VALUE;//不限制展示数量
    private int size = SIZE_MAX;

    private List<WeMediaList.WeMediaListEntity> list = new ArrayList<>();

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public int getCount() {
        return list.size() >= size ? size : list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        OnClick onClick;
        if (view == null) {
            holder = new ViewHolder();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_search_wemedia_item_layout, viewGroup, false);
            holder.initView(view);
            view.setTag(holder);
            view.setTag(R.id.view_parent, onClick);
        } else {
            holder = (ViewHolder) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        holder.tv_subscribe.setOnClickListener(onClick);
        holder.itemView.setOnClickListener(onClick);
        WeMediaList.WeMediaListEntity entity = list.get(i);
        holder.img_media_cover.setImageUrl(entity.getHeadPic(), VolleyHelper.getImageLoader());
        holder.img_media_cover.setDefaultImageResId(R.drawable.icon_login_default_header);
        holder.img_media_cover.setErrorImageResId(R.drawable.icon_login_default_header);
        holder.tv_media_name.setText(entity.getName());
        boolean flag = TextUtils.isEmpty(entity.getFollowNo()) || "0".equals(entity.getFollowNo());//如果订阅人数为空或者为0
        holder.tv_subscribe_num.setVisibility(flag ? View.INVISIBLE : View.VISIBLE);
        holder.tv_subscribe_num.setText(StringUtils.changeNumberMoreThen10000(entity.getFollowNo()) + "人订阅");
        if (list.get(i).getFollowed() == 1) {//已订阅
            holder.tv_subscribe.setText(R.string.subscribed);
            holder.tv_subscribe.setTextColor(viewGroup.getContext().getResources().getColor(R.color.subscribed_text_color));
            holder.tv_subscribe.setBackgroundResource(R.drawable.btn_subscribe_shape_gray_border);
        } else {//未订阅  添加
            holder.tv_subscribe.setText(R.string.subscribe);
            holder.tv_subscribe.setTextColor(viewGroup.getContext().getResources().getColor(R.color.add_subscribe_text_color));
            holder.tv_subscribe.setBackgroundResource(R.drawable.btn_subscribe_shape_border);
        }
        return view;
    }

    public void setList(List<WeMediaList.WeMediaListEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSize(int size) {
        this.size = size;
    }

    static class ViewHolder {
        View itemView;
        NetworkImageView img_media_cover;//自媒体封面
        TextView tv_media_name;//自媒体名称
        TextView tv_subscribe_num;//订阅数量
        TextView tv_subscribe;//订阅按钮

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            img_media_cover = (NetworkImageView) view.findViewById(R.id.img_media_cover);
            tv_media_name = (TextView) view.findViewById(R.id.tv_media_name);
            tv_subscribe_num = (TextView) view.findViewById(R.id.tv_subscribe_num);
            tv_subscribe = (TextView) view.findViewById(R.id.tv_subscribe);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (clickListener == null) {
                return;
            }
            switch (view.getId()) {
                case R.id.view_parent:
                    clickListener.onItemClickListener(list.get(position), position);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_WEMEDIA + position, PageIdConstants.SEARCH_RESULT);
                    break;
                case R.id.tv_subscribe:
                    clickListener.onSubscribeClickListener(list.get(position));
                    break;
            }
        }
    }

    private ClickListener clickListener;

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClickListener(WeMediaList.WeMediaListEntity entity, int position);

        void onSubscribeClickListener(WeMediaList.WeMediaListEntity entity);
    }
}
