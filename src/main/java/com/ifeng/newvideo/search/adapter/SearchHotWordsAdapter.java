package com.ifeng.newvideo.search.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.video.dao.db.model.SearchHotWordsModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * 历史搜索、推荐热词适配器
 * Created by wangqing1 on 2015/1/6.
 */
public class SearchHotWordsAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SearchHotWordsAdapter.class);

    public static final int SIZE_FOUR = 4;//展示4个
    public static final int SIZE_SIX = 6;//展示6个
    public static final int SIZE_MAX = Integer.MAX_VALUE;//不限制显示数量
    private int size = SIZE_MAX;

    private final LayoutInflater inflater;
    private List<SearchHotWordsModel.SearchDefaultItem> list = new ArrayList<>();

    public SearchHotWordsAdapter(Context con, int size) {
        this.size = size;
        inflater = LayoutInflater.from(con);
    }

    public void setData(List<SearchHotWordsModel.SearchDefaultItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size() >= size ? size : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.search_hot_words_item, parent, false);
            holder.tv_word = (TextView) convertView.findViewById(R.id.tv_search_hot_word);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (!TextUtils.isEmpty(list.get(position).getWord())) {
            holder.tv_word.setText(list.get(position).getWord());
        }
        holder.tv_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onKeywordClick(list.get(position).getWord(), position);
                    if(size==SIZE_FOUR) {
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_HIS + "_" + position, PageIdConstants.PAGE_SEARCH);
                    }else if(size==SIZE_SIX) {
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_REC + "_" + position, PageIdConstants.PAGE_SEARCH);
                    }
                }
            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView tv_word;
    }

    private ClickListener clickListener;

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onKeywordClick(String keyword ,int pos);
    }
}
