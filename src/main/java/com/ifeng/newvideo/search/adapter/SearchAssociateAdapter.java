package com.ifeng.newvideo.search.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.dao.db.model.SearchAssociateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * 搜索联想词adapter
 * Created by wangqing1 on 2015/1/6.
 */
public class SearchAssociateAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SearchAssociateAdapter.class);

    private static final int MAX_SIZE = 10;//最多展示10条数据
    private final LayoutInflater inflater;
    private List<SearchAssociateModel> mList = new ArrayList<SearchAssociateModel>();
    private String selectedWord;

    public SearchAssociateAdapter(Context con) {
        inflater = LayoutInflater.from(con);
    }

    public void setData(ArrayList<SearchAssociateModel> arr) {
        if (arr == null) {
            return;
        }
        mList.clear();
        mList.addAll(arr);
        notifyDataSetChanged();
    }

    public void setKeyWord(String keyWord) {
        this.selectedWord = keyWord;
    }


    @Override
    public int getCount() {
        return mList.size() > MAX_SIZE ? MAX_SIZE : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.search_associate_item, null);
            holder.textView = (TextView) convertView.findViewById(R.id.tv_search_Associate_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String name = "";
        if (position >= 0 && position < mList.size()) {
            name = mList.get(position).getName();
        }

        SpannableStringBuilder builder = new SpannableStringBuilder(name);
        ForegroundColorSpan unSelectedSpanBefore = new ForegroundColorSpan(Color.parseColor("#262626"));
        ForegroundColorSpan selectedSpan = new ForegroundColorSpan(Color.RED);
        ForegroundColorSpan unSelectedSpanAfter = new ForegroundColorSpan(Color.parseColor("#262626"));
        if (TextUtils.isEmpty(selectedWord)) {
            builder.setSpan(unSelectedSpanBefore, 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            String tmpStr = selectedWord.replaceAll("\\s*", "");//去掉空格
            int selectedStrPos = name.indexOf(tmpStr);

            if (selectedStrPos != -1) {
//                builder.setSpan(unSelectedSpanBefore, 0, selectedStrPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                try {
                    builder.setSpan(selectedSpan, selectedStrPos, selectedStrPos + tmpStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    builder.setSpan(unSelectedSpanAfter, selectedStrPos + tmpStr.length(), name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        holder.textView.setText(builder);
        return convertView;
    }

    static class ViewHolder {
        TextView textView;
    }
}
