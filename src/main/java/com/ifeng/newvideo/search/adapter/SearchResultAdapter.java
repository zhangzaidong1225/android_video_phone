package com.ifeng.newvideo.search.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.SearchResultModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索结果适配器
 * Created by Administrator on 2016/9/27.
 */
public class SearchResultAdapter extends BaseAdapter {

    private volatile boolean isEmpty = false;//是否无数据
    private List<SearchResultModel> list = new ArrayList<>();
    private String keyword;
    //是否有自媒体
    private boolean isHeaderView;

    @Override
    public int getCount() {
        return isEmpty ? 1 : list.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        if (isEmpty || list.isEmpty()) {
            return null;
        }
        return list.get(i);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (isEmpty) {
            return getEmptyView(viewGroup);
        }

        ViewHolderVideo videoHolder;
        OnClick onClick;
        if (view == null || !(view.getTag() instanceof ViewHolderVideo)) {
            videoHolder = new ViewHolderVideo();
            onClick = new OnClick();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_listview_mix_text_picture, viewGroup, false);
            videoHolder.initView(view);
            view.setTag(R.id.view_parent, onClick);
            view.setTag(videoHolder);
        } else {
            videoHolder = (ViewHolderVideo) view.getTag();
            onClick = (OnClick) view.getTag(R.id.view_parent);
        }
        onClick.setPosition(i);
        videoHolder.itemView.setOnClickListener(onClick);

        SearchResultModel item = list.get(i);
        videoHolder.tvVideoTitle.setText(item.getTitle());

        String playTime = item.getPlayTime();
        boolean isShowPlayTime = !TextUtils.isEmpty(playTime);
        videoHolder.tvVideoWatchTime.setVisibility(isShowPlayTime ? View.VISIBLE : View.GONE);
        videoHolder.tvVideoWatchTime.setText(StringUtils.changePlayTimes(playTime));

        try {
            int duration = Integer.parseInt(item.getDuration());
            videoHolder.tvVideoTime.setText(StringUtils.changeDuration(duration));
        } catch (NumberFormatException e) {
            videoHolder.tvVideoTime.setText(item.getDuration());
        }

        videoHolder.videoCover.setDefaultImageResId(R.drawable.common_default_bg);
        videoHolder.videoCover.setErrorImageResId(R.drawable.common_default_bg);
        videoHolder.videoCover.setImageUrl(item.getImgUrl(), VolleyHelper.getImageLoader());

        boolean isWeMediaInfoEmpty = TextUtils.isEmpty(item.getMediaid()) || TextUtils.isEmpty(item.getMedianame());
//        videoHolder.avatar.setVisibility(isWeMediaInfoEmpty ? View.GONE : View.VISIBLE);
//        videoHolder.avatarMask.setVisibility(isWeMediaInfoEmpty ? View.GONE : View.VISIBLE);
        videoHolder.author.setVisibility(isWeMediaInfoEmpty ? View.GONE : View.VISIBLE);
//        videoHolder.avatar.setDefaultImageResId(R.drawable.icon_login_default_header);
//        videoHolder.avatar.setErrorImageResId(R.drawable.icon_login_default_header);
//        videoHolder.avatar.setImageUrl(item.getMediaHeadPic(), VolleyHelper.getImageLoader());
        videoHolder.author.setText(item.getMedianame());
        CommonStatictisListUtils.getInstance().sendSearchResultYoukuConstatic(item, CommonStatictisListUtils.YK_EXPOSURE, CommonStatictisListUtils.YK_HOMEPAGE_FEEDS);
        return view;
    }

    /**
     * 得到EmptyView
     */
    private View getEmptyView(ViewGroup root) {
        View view = LayoutInflater.from(root.getContext()).inflate(R.layout.search_result_empty_view, root, false);
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        }
        return view;
    }

    /**
     * 显示EmptyView
     */
    public synchronized void showEmptyView() {
        isEmpty = true;
        notifyDataSetChanged();
    }

    public void setList(List<SearchResultModel> list) {
        isEmpty = false;
        this.list = list;
        notifyDataSetChanged();
    }

    public void setKeyWord(String keyword) {
        this.keyword = keyword;
    }

    public void isHeaderView(boolean isHeaderView) {
        this.isHeaderView = isHeaderView;
    }

    static class ViewHolderVideo {
        View itemView;
        TextView tvVideoTitle;//视频标题
        TextView tvVideoType;//类型：逻辑思维
        TextView tvVideoWatchTime;//观看次数
        NetworkImageView videoCover;//视频封面
        //        NetworkImageView avatar;//自媒体头像
//        ImageView avatarMask;//自媒体头像圆形遮罩
        TextView tvVideoTime;//视频时长
        TextView author;//自媒体名称
        View divider;//底部一条线

        private void initView(View view) {
            itemView = view.findViewById(R.id.view_parent);
            tvVideoTitle = (TextView) view.findViewById(R.id.tv_left_title);
            tvVideoType = (TextView) view.findViewById(R.id.tv_category_label);
            tvVideoWatchTime = (TextView) view.findViewById(R.id.tv_play_times);
            videoCover = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
            tvVideoTime = (TextView) view.findViewById(R.id.tv_duration);
            author = (TextView) view.findViewById(R.id.tv_author);
//            avatar = (NetworkImageView) view.findViewById(R.id.niv_left_emoji);
//            avatarMask = (ImageView) view.findViewById(R.id.niv_left_avatar_mask);
            divider = view.findViewById(R.id.divider);

            author.setVisibility(View.VISIBLE);
//            avatar.setVisibility(View.VISIBLE);
//            avatarMask.setVisibility(View.VISIBLE);
        }
    }

    class OnClick implements View.OnClickListener {
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {

            String guid = list.get(position).getGuid();
            if (!TextUtils.isEmpty(guid)) {
                IntentUtils.toVodDetailActivity(view.getContext(), guid, "", false, false, 0, "");
            }
            int addPosition  = position + 1;;
            String addStr;
            if (isHeaderView) {
                addStr = "1_" + addPosition;
            } else {
                addStr = "0_" + addPosition;
            }

            PageActionTracker.clickSearchItem(addStr, keyword, "", "");
        }
    }
}
