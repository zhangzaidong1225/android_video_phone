package com.ifeng.newvideo.search;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 管理搜索结果的筛选
 * Created by cuihz on 2015/1/7.
 */
class SearchResultSelectManager {

    private static final Logger logger = LoggerFactory.getLogger(SearchResultSelectManager.class);

    private static final int KEY_ORDERBY = 0;
    private static final int KEY_CATEGORY = 1;
    private static final int KEY_DURATION = 2;
    private static final int KEY_PUBLIC_TIME = 3;

    private static int[] exCategoryButtons;
    private static int[] exDurationButtons;
    private static int[] exPublicTimeButtons;
    private static int select_black;
    private static int select_grey;
    private static int select_red;
    private static String[] categoryKeys;
    private static String[] durationKeys;
    private static String[] publicTimeKeys;
    private static final Map<Integer, Integer> selectedList = new HashMap<Integer, Integer>();
    private static View selectView;
    private static TextView similarButton;
    private static TextView publicTimeButton;

    //private static SearchResultFragment resultFragment ;
    private static SelectButtonOnCLick selectButtonOnCLick;

    private static void init(Context context) {

        categoryKeys = context.getResources().getStringArray(R.array.search_result_select_category_key);
        durationKeys = context.getResources().getStringArray(R.array.search_result_select_duration_key);
        publicTimeKeys = context.getResources().getStringArray(R.array.search_result_select_public_time_key);
        exCategoryButtons = new int[categoryKeys.length];
        exDurationButtons = new int[durationKeys.length];
        exPublicTimeButtons = new int[publicTimeKeys.length];
        initExButtonIds(context, exCategoryButtons, R.array.search_result_select_category_buttons);
        initExButtonIds(context, exDurationButtons, R.array.search_result_select_duration_buttons);
        initExButtonIds(context, exPublicTimeButtons, R.array.search_result_select_public_time_buttons);
        select_black = context.getResources().getColor(R.color.search_result_select_black);
        select_grey = context.getResources().getColor(R.color.search_result_select_gray);
        select_red = context.getResources().getColor(R.color.search_result_select_red);
    }

    private static void initExButtonIds(Context context, int[] ids, int arrayId) {
        TypedArray array = context.getResources().obtainTypedArray(arrayId);
        for (int i = 0; i < ids.length; i++) {
            ids[i] = array.getResourceId(i, 0);
        }
    }

    //public static void setSelectView(SearchResultFragment fragment, View view) {
        //if (categoryKeys == null) {
        //    init(fragment.getActivity());
        //}
        //if (selectedList.isEmpty()) {
        //    selectedList.put(KEY_ORDERBY, R.id.result_top_similar_button);
        //    selectedList.put(KEY_CATEGORY, R.id.search_result_select_category_no);
        //    selectedList.put(KEY_DURATION, R.id.search_result_select_duration_no);
        //    selectedList.put(KEY_PUBLIC_TIME, R.id.search_result_select_public_time_no);
        //}
        //selectButtonOnCLick = new SelectButtonOnCLick();
        //resultFragment = fragment;
        //selectView = view;
        //initView();
        //refreshView();
    //}

    public static void clear() {
        selectedList.clear();
        selectedList.put(KEY_ORDERBY, R.id.result_top_similar_button);
        selectedList.put(KEY_CATEGORY, R.id.search_result_select_category_no);
        selectedList.put(KEY_DURATION, R.id.search_result_select_duration_no);
        selectedList.put(KEY_PUBLIC_TIME, R.id.search_result_select_public_time_no);
        refreshView();
    }

    private static void initView() {
        similarButton = (TextView) selectView.findViewById(R.id.result_top_similar_button);
        publicTimeButton = (TextView) selectView.findViewById(R.id.result_top_public_time_button);
        similarButton.setOnClickListener(selectButtonOnCLick);
        publicTimeButton.setOnClickListener(selectButtonOnCLick);
        addExButtonsClickListener();
    }


    private static void addExButtonsClickListener() {
        addExButtonClickListener(exCategoryButtons);
        addExButtonClickListener(exDurationButtons);
        addExButtonClickListener(exPublicTimeButtons);
    }

    private static void addExButtonClickListener(int[] ids) {
        for (int id : ids) {
            selectView.findViewById(id).setOnClickListener(selectButtonOnCLick);
        }
    }

    private static void refreshView() {
        similarButton.setTextColor(select_black);
        publicTimeButton.setTextColor(select_black);
        initExButtonColor(exCategoryButtons);
        initExButtonColor(exDurationButtons);
        initExButtonColor(exPublicTimeButtons);
        setSelectedColor();
    }

    private static void setSelectedColor() {
        Collection<Integer> selectedIds = selectedList.values();
        for (int id : selectedIds) {
            ((TextView) selectView.findViewById(id)).setTextColor(select_red);
        }
    }


    private static void initExButtonColor(int[] ids) {
        for (int id : ids) {
            ((TextView) selectView.findViewById(id)).setTextColor(select_grey);
        }
    }


    private static void changeExSelected(int key, int selectedId) {
        if (selectedList.containsKey(key)) {
            int lastId = selectedList.get(key);
            TextView lastView = (TextView) selectView.findViewById(lastId);
            if (lastId > 0) {
                lastView.setTextColor(select_grey);
            }
        }
        selectedList.put(key, selectedId);
        ((TextView) selectView.findViewById(selectedId)).setTextColor(select_red);
    }

    private static void setCategoryParam(int id) {
        int index = indexOfIntArray(exCategoryButtons, id);
        if (index > -1) {
            SearchParamsManager.put(SearchParamsManager.KEY_CATEGORY, categoryKeys[index]);
        } else {
            logger.error("no catagory button " + id);
        }
    }

    private static void setDurationParam(int id) {
        int index = indexOfIntArray(exDurationButtons, id);
        if (index > -1) {
            String key = durationKeys[index];
            if (TextUtils.isEmpty(key)) {
                SearchParamsManager.put(SearchParamsManager.KEY_DURATION_START, "");
                SearchParamsManager.put(SearchParamsManager.KEY_DURATION_END, "");
            } else {
                String[] keys = key.split(":");
                SearchParamsManager.put(SearchParamsManager.KEY_DURATION_START, keys[0]);
                if (keys.length == 1) {
                    SearchParamsManager.put(SearchParamsManager.KEY_DURATION_END, "");
                } else {
                    SearchParamsManager.put(SearchParamsManager.KEY_DURATION_END, keys[1]);
                }
            }
        } else {
            logger.error("no duration button " + id);
        }
    }


    private static void sePublicTimeParam(int id) {
        int index = indexOfIntArray(exPublicTimeButtons, id);
        if (index > -1) {
            SearchParamsManager.put(SearchParamsManager.KEY_PUBLIC_TIME, publicTimeKeys[index]);
        } else {
            logger.error("no public time button " + id);
        }
    }

    private static int indexOfIntArray(int[] array, int id) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == id) {
                return i;
            }
        }
        return -1;
    }


    private static class SelectButtonOnCLick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            //if (!NetUtils.isNetAvailable(resultFragment.getActivity())){
            //    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            //    return;
            //}
            int id = v.getId();
            switch (id) {
                case R.id.result_top_similar_button:
                    similarButton.setTextColor(select_red);
                    publicTimeButton.setTextColor(select_black);
                    selectedList.put(KEY_ORDERBY, id);
                    SearchParamsManager.put(SearchParamsManager.KEY_ORDERBY, SearchParamsManager.ORDERBY_SIMILAR + "");
                    break;
                case R.id.result_top_public_time_button:
                    similarButton.setTextColor(select_black);
                    publicTimeButton.setTextColor(select_red);
                    selectedList.put(KEY_ORDERBY, id);
                    SearchParamsManager.put(SearchParamsManager.KEY_ORDERBY, SearchParamsManager.ORDERBY_PUBLIC_TIME + "");
                    break;
                case R.id.search_result_select_category_no:
                case R.id.search_result_select_category_documentary:
                case R.id.search_result_select_category_news:
                case R.id.search_result_select_category_mil:
                case R.id.search_result_select_category_ent:
                case R.id.search_result_select_category_fashion:
                case R.id.search_result_select_category_history:
                case R.id.search_result_select_category_gongkaike:
                case R.id.search_result_select_category_program:
                case R.id.search_result_select_category_vblog:
                    setCategoryParam(id);
                    changeExSelected(KEY_CATEGORY, id);
                    break;
                case R.id.search_result_select_duration_no:
                case R.id.search_result_select_duration_ten:
                case R.id.search_result_select_duration_thirty:
                case R.id.search_result_select_duration_sixty:
                case R.id.search_result_select_duration_sixty_up:
                    setDurationParam(id);
                    changeExSelected(KEY_DURATION, id);
                    break;
                case R.id.search_result_select_public_time_no:
                case R.id.search_result_select_public_time_day:
                case R.id.search_result_select_public_time_week:
                case R.id.search_result_select_public_time_month:
                    sePublicTimeParam(id);
                    changeExSelected(KEY_PUBLIC_TIME, id);
                    break;
            }
            //resultFragment.search(SearchResultFragment.ACTION_SELECT);
        }
    }

}
