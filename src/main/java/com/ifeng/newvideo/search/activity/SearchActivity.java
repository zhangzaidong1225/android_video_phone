package com.ifeng.newvideo.search.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSONArray;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.search.adapter.SearchAssociateAdapter;
import com.ifeng.newvideo.search.adapter.SearchHotWordsAdapter;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.widget.UnScrollGridView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.dao.SearchDao;
import com.ifeng.video.dao.db.dao.SearchHistoryDao;
import com.ifeng.video.dao.db.model.SearchAssociateModel;
import com.ifeng.video.dao.db.model.SearchHistoryModel;
import com.ifeng.video.dao.db.model.SearchHotWordsModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 搜索界面
 * Created by Vincent on 2015/1/4.
 */
public class SearchActivity extends BaseFragmentActivity implements View.OnClickListener,
        SearchResultFragment.OnRecommendItemClick, SearchHotWordsAdapter.ClickListener {
    private static final Logger logger = LoggerFactory.getLogger(SearchActivity.class);

    private EditText searchEdit;//搜索输入框
    private ImageView editClearImg;//清除EditText中内容的小图标
    private TextView rightText;//搜索框右侧搜索、取消Text

    private View searchResultLayout;//展示搜索结果
    private View historyRecommendLayout;//历史搜索和热词推荐布局
    private View historyLayout;//历史搜索布局
    private ListView associateListView;//联想词ListView
    private View loading;//加载
    private View error;//失败
    private View noNet;//无网
    private View partingLine;//历史搜索与推荐搜索之间分割线
    private View searchLine;//搜索框底部一条横线

    private UnScrollGridView historyGridView;//历史搜索GridView
    private UnScrollGridView recommendGridView;//推荐的GridView

    private SearchHotWordsAdapter historyAdapter;//搜索历史适配器
    private SearchHotWordsAdapter recommendAdapter;//推荐适配器
    private SearchAssociateAdapter associateAdapter;//联想词适配器

    private List<SearchHistoryModel> historyLocalList = new ArrayList<>();//数据库中历史记录数据
    private List<SearchHotWordsModel.SearchDefaultItem> historyList = new ArrayList<>();//搜索记录List
    private List<SearchHotWordsModel.SearchDefaultItem> recommendList = new ArrayList<>();//推荐List
    private List<SearchAssociateModel> associateList = new ArrayList<>();//联想词List

    private SearchResultFragment searchResultFragment;

    private boolean isRequestAssociateWords = true;//是否请求联想词数据

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setAnimFlag(ANIM_FLAG_BOTTOM_TOP);
        enableExitWithSlip(false);
        initView();
        initAdapter();
        requestData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSoftInputMode();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (searchResultFragment != null && !searchResultFragment.isVisible()) {
            PageActionTracker.endPageSearch();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_clear:
                searchEdit.setText("");
                getFocusable(searchEdit);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_CLEAR, PageIdConstants.PAGE_SEARCH);
                break;
            case R.id.tv_cancel:
                rightTextClickLogic(rightText.getText().toString());
                break;
            case R.id.icon_clear_history://清空历史记录
                showClearWarnDialog();
                break;
            case R.id.load_fail_layout:
            case R.id.no_net_layer:
                requestData();
                break;
        }
    }

    /**
     * 滑动界面改变焦点（隐藏软键盘）
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getY() > searchEdit.getHeight() + DisplayUtils.getStatusBarHeight()) {
            //getFocusable(searchResultLayout);
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 搜索结果页面推荐热词Item点击回调
     */
    @Override
    public void onRecommendItemClick(String hotWord) {
        searchLine.setVisibility(View.GONE);
        isRequestAssociateWords = false;
        searchEdit.setText(hotWord);
        rightText.setText(R.string.cancel);//设为取消
        saveKeywordToDB(hotWord);
    }

    /**
     * 搜索结果为空
     */
    @Override
    public void onSearchResultEmpty() {
        searchLine.setVisibility(View.VISIBLE);
    }

    /**
     * 当历史搜索、推荐热词中Item点击回调
     */
    @Override
    public void onKeywordClick(String keyword, int pos) {
        isRequestAssociateWords = false;
        searchEdit.setText(keyword);
        getSearchResult(keyword);
    }

    private void initView() {
        searchLine = findViewById(R.id.search_line);
        partingLine = findViewById(R.id.parting_line);
        loading = findViewById(R.id.loading_layout);
        ((TextView) loading.findViewById(R.id.loading_text)).setText(R.string.common_onload);
        error = findViewById(R.id.load_fail_layout);
        error.setOnClickListener(this);
        noNet = findViewById(R.id.no_net_layer);
        noNet.setOnClickListener(this);

        editClearImg = (ImageView) findViewById(R.id.iv_clear);
        editClearImg.setOnClickListener(this);
        rightText = (TextView) findViewById(R.id.tv_cancel);
        rightText.setText(R.string.search_cancel);
        rightText.setOnClickListener(this);
        findViewById(R.id.icon_clear_history).setOnClickListener(this);

        searchResultLayout = findViewById(R.id.rl_search_result_fragment);
        historyRecommendLayout = findViewById(R.id.history_recommend_layout);
        historyLayout = findViewById(R.id.ll_history);
        historyGridView = (UnScrollGridView) findViewById(R.id.grid_history);
        recommendGridView = (UnScrollGridView) findViewById(R.id.grid_recommend);

        associateListView = (ListView) findViewById(R.id.list_associate);

        searchEdit = (EditText) findViewById(R.id.search_edit);
        searchEdit.setOnFocusChangeListener(new FocusChangeListener());
        searchEdit.addTextChangedListener(new MyTextWatcher());
        searchEdit.setOnEditorActionListener(new EditorActionListener());
    }

    /**
     * 设置软键盘模式
     */
    private void setSoftInputMode() {
        if (NetUtils.isNetAvailable(SearchActivity.this)) {//有网、自动弹出软键盘
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } else {//无网、不自动弹出软键盘
            getFocusable(searchResultLayout);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    /**
     * 点击搜索、取消逻辑
     */
    private void rightTextClickLogic(String text) {
        if (text.equals(this.getString(R.string.search_cancel))) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_CANCEL, PageIdConstants.PAGE_SEARCH);
            VolleyHelper.getRequestQueue().cancelAll(SearchDao.TAG_SEARCHING);
            getFocusable(searchResultLayout);
            finish();
        } else if (text.equals(this.getString(R.string.search_text))) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH, PageIdConstants.PAGE_SEARCH);
            getSearchResult(getKeyword());
        }
    }

    /**
     * 获取数据
     */
    public void requestData() {
        setStatusLayout(LOADING);
        getHotWords();
        getHistoryWords();
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        historyAdapter = new SearchHotWordsAdapter(SearchActivity.this, SearchHotWordsAdapter.SIZE_FOUR);
        historyAdapter.setClickListener(this);
        historyGridView.setAdapter(historyAdapter);

        recommendAdapter = new SearchHotWordsAdapter(SearchActivity.this, SearchHotWordsAdapter.SIZE_SIX);
        recommendAdapter.setClickListener(this);
        recommendGridView.setAdapter(recommendAdapter);

        associateAdapter = new SearchAssociateAdapter(SearchActivity.this);
        associateListView.setAdapter(associateAdapter);
        associateListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onKeywordClick(associateList.get(i).getName(), i);
            }
        });
    }

    /**
     * 获取历史条目
     */
    private void getHistoryWords() {
        historyLocalList.clear();
        try {
            historyLocalList.addAll(SearchHistoryDao.getInstance(this).getAll());
            if (historyLocalList.size() > 0) {//存在搜索记录
                historyLayout.setVisibility(View.VISIBLE);
                partingLine.setVisibility(View.VISIBLE);
                Collections.sort(historyLocalList);
                historyList.clear();//先清空再获取，防止出现重复情况
                for (SearchHistoryModel anArr : historyLocalList) {
                    SearchHotWordsModel.SearchDefaultItem item = new SearchHotWordsModel.SearchDefaultItem();
                    item.setWord(anArr.getName());
                    historyList.add(item);
                }
                historyAdapter.setData(historyList);
            } else {//不存在搜索记录
                historyLayout.setVisibility(View.GONE);
                partingLine.setVisibility(View.GONE);
            }
        } catch (SQLException e) {
            historyLayout.setVisibility(View.GONE);
            partingLine.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    /**
     * 获取热门搜索词
     */
    private void getHotWords() {
        recommendList.clear();
        SearchDao.getSearchRecommendWords(SearchHotWordsModel.class,
                new Response.Listener<SearchHotWordsModel>() {
                    @Override
                    public void onResponse(SearchHotWordsModel response) {
                        if (response == null || ListUtils.isEmpty(response.getWordlist())) {
                            setStatusLayout(ERROR);
                        } else {
                            setStatusLayout(RECOMMEND_HISTORY);
                            recommendList.addAll(response.getWordlist());
                            recommendAdapter.setData(recommendList);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error instanceof NetworkError) {
                            setStatusLayout(NO_NET);
                        } else {
                            setStatusLayout(ERROR);
                        }
                    }
                }, false);
    }

    /**
     * 获取联想条目
     *
     * @param keyword 联想关键字
     */
    private void getAssociate(final String keyword) {
        if (TextUtils.isEmpty(keyword) || !NetUtils.isNetAvailable(SearchActivity.this)) {//当搜索框内容为空、无网时，即不再请求此接口数据。
            return;
        }

        SearchDao.getSearchAssociateWords(keyword,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (TextUtils.isEmpty(getKeyword()) || response == null || ListUtils.isEmpty(response)) {
                            return;
                        }
                        associateList.clear();
                        for (Object object : response) {
                            if (object != null && !TextUtils.isEmpty(object.toString())) {
                                associateList.add(new SearchAssociateModel(object.toString()));
                            }
                        }
                        if (ListUtils.isEmpty(associateList)) {
                            setStatusLayout(ERROR);
                        } else {
                            setStatusLayout(ASSOCIATE);
                            associateAdapter.setKeyWord(keyword);
                            associateAdapter.setData((ArrayList<SearchAssociateModel>) associateList);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, false);
    }

    /**
     * 搜索
     *
     * @param content 关键字
     */
    private void getSearchResult(final String content) {
        if (TextUtils.isEmpty(content)) {
            ToastUtils.getInstance().showShortToast(R.string.search_no_keyword);
            return;
        }
        getFocusable(searchResultLayout);
        saveKeywordToDB(content);
        openSearchResultFragment(content);
    }

    /**
     * 保存搜索词到本地
     */
    private void saveKeywordToDB(final String content) {
        new Thread(new Runnable() {
            @Override
            public void run() {//保存搜索记录到数据库
                try {
                    SearchHistoryDao.getInstance(SearchActivity.this)
                            .insertOrUpdate(new SearchHistoryModel(System.currentTimeMillis(), content));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * 删除搜索记录
     */
    private void deleteHistoryData() {
        historyLayout.setVisibility(View.GONE);
        partingLine.setVisibility(View.GONE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    SearchHistoryDao.getInstance(SearchActivity.this).deleteAll(historyLocalList);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private Dialog clearWarnDialog;

    /**
     * 显示清空历史记录Dialog
     */
    private void showClearWarnDialog() {
        Resources res = this.getResources();
        clearWarnDialog = AlertUtils.getInstance().showTwoBtnDialog(SearchActivity.this,
                res.getString(R.string.search_clear_history_dialog_title),
                res.getString(R.string.cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (clearWarnDialog != null && clearWarnDialog.isShowing()) {
                            clearWarnDialog.dismiss();
                        }
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_HIS_CLEAR, false, PageIdConstants.PAGE_SEARCH);
                    }
                },
                res.getString(R.string.ok),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (clearWarnDialog != null && clearWarnDialog.isShowing()) {
                            clearWarnDialog.dismiss();
                        }
                        deleteHistoryData();
                        PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_HIS_CLEAR, true, PageIdConstants.PAGE_SEARCH);
                    }
                });
    }

    /**
     * 显示、隐藏键盘
     *
     * @param isShow 是否弹出软键盘
     */
    private void toggleSystemKeyBoard(View v, boolean isShow) {
        InputMethodManager imm = (InputMethodManager) SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (isShow) {
            imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
        } else {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    /**
     * 打开搜索结果Fragment
     */
    private void openSearchResultFragment(String keyword) {
        PageActionTracker.endPageSearch();
        if (NetUtils.isNetAvailable(SearchActivity.this) && !ListUtils.isEmpty(recommendList)) {
            setStatusLayout(RECOMMEND_HISTORY);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (searchResultFragment != null) {//如果已经add了Fragment，先移除掉再添加
            transaction.remove(searchResultFragment);
        }
        searchResultFragment = SearchResultFragment.newInstance(keyword);
        transaction.setCustomAnimations(R.anim.common_slide_left_in, R.anim.common_slide_right_out);
        transaction.add(R.id.rl_search_result_fragment, searchResultFragment).commit();
        searchLine.setVisibility(View.GONE);
        historyRecommendLayout.setVisibility(View.GONE);
    }

    /**
     * 关闭搜索结果Fragment
     */
    private void hideSearchResultFragment() {
        if (searchResultFragment != null) {
            PageActionTracker.enterPage();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.common_slide_left_in, R.anim.common_slide_right_out);
            transaction.remove(searchResultFragment).commit();
            searchLine.setVisibility(View.VISIBLE);
            historyRecommendLayout.setVisibility(View.VISIBLE);
        }
        //getHotWords();//看需求是否要获取热词
        getHistoryWords();
        getAssociate(getKeyword());
    }

    /**
     * 获取焦点
     */
    private void getFocusable(View v) {
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        v.requestFocus();
    }

    /**
     * 获取关键词
     */
    private String getKeyword() {
        return searchEdit == null ? "" : searchEdit.getText().toString().trim();
    }

    /**
     * 根据聚焦来设置显隐
     */
    private void setTextByFocus(boolean hasFocus) {
        boolean isEmpty = TextUtils.isEmpty(searchEdit.getText().toString());
        //获取焦点、输入框内容不为空、没有点击搜索结果页中推荐搜索Item，显示搜索，否则显示取消
        rightText.setText(hasFocus && !isEmpty ? R.string.search_text : R.string.search_cancel);
    }

    /**
     * 搜索框监听器
     */
    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            CommonDao.cancel(DataInterface.getSearchAssociateWords(charSequence.toString()));
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(final Editable editable) {
            boolean empty = TextUtils.isEmpty(editable.toString());
            rightText.setText(empty ? R.string.search_cancel : R.string.search_text);
            editClearImg.setVisibility(empty ? View.GONE : View.VISIBLE);
            if (!empty) {//如果搜索框有数据，搜索联想词
                if (isRequestAssociateWords) {
                    getAssociate(editable.toString().trim());
                }
                isRequestAssociateWords = true;
            } else {
                if (!ListUtils.isEmpty(recommendList)) {//获取热词成功、展示热词
                    setStatusLayout(RECOMMEND_HISTORY);//显示历史、热词布局
                } else {//获取热词失败，重新获取热词
                    requestData();
                }
            }
        }
    }

    /**
     * 当搜索框焦点改变时
     */
    class FocusChangeListener implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            setTextByFocus(hasFocus);
            if (hasFocus) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_EDIT, PageIdConstants.PAGE_SEARCH);
                hideSearchResultFragment();
            }
            toggleSystemKeyBoard(view, hasFocus);
        }
    }

    /**
     * 软键盘按键监听
     **/
    class EditorActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {//判断是否是“搜索键”键
                getSearchResult(getKeyword());
                return true;
            }
            return false;
        }
    }

    public static final int LOADING = 1;//加载
    public static final int ERROR = 3;//错误(接口请求失败，数据解析错误)
    public static final int NO_NET = 4;//无网
    public static final int ASSOCIATE = 5;//联想词
    public static final int RECOMMEND_HISTORY = 6;//热词推荐和历史搜索布局

    private void setStatusLayout(int status) {
        switch (status) {
            case LOADING:
                loading.setVisibility(View.VISIBLE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                associateListView.setVisibility(View.GONE);
                historyRecommendLayout.setVisibility(View.GONE);
                break;
            case ERROR:
                loading.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                noNet.setVisibility(View.GONE);
                associateListView.setVisibility(View.GONE);
                historyRecommendLayout.setVisibility(View.GONE);
                break;
            case NO_NET:
                loading.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.VISIBLE);
                associateListView.setVisibility(View.GONE);
                historyRecommendLayout.setVisibility(View.GONE);
                break;
            case ASSOCIATE:
                loading.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                associateListView.setVisibility(View.VISIBLE);
                historyRecommendLayout.setVisibility(View.GONE);
                break;
            case RECOMMEND_HISTORY:
                loading.setVisibility(View.GONE);
                error.setVisibility(View.GONE);
                noNet.setVisibility(View.GONE);
                associateListView.setVisibility(View.GONE);
                historyRecommendLayout.setVisibility(View.VISIBLE);
                break;
        }
    }
}
