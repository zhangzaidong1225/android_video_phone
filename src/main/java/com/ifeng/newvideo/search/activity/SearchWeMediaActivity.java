package com.ifeng.newvideo.search.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.adapter.WeMediaListAdapter;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshGridView;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import com.ifeng.video.dao.db.model.subscribe.WeMediaList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索自媒体结果页
 * Created by Administrator on 2016/9/28.
 */
public class SearchWeMediaActivity extends BaseFragmentActivity implements View.OnClickListener, WeMediaListAdapter.ClickListener {
    private static final Logger logger = LoggerFactory.getLogger(SearchWeMediaActivity.class);

    private View loading_layout;//Loading
    private View no_net_layer;//无网
    private View load_fail_layout;//数据加载失败
    private PullToRefreshGridView gridView;

    private WeMediaListAdapter adapter;

    private List<WeMediaList.WeMediaListEntity> list = new ArrayList<>();
    private WeMediaList.WeMediaListEntity entity;

    private int currentPage = 1;//当前页码
    private String positionId = "";
    private String keyword;
    private boolean isClickSubscribeBtn = false;//是否点击了订阅按钮

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_we_media_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(true);
        registerLoginBroadcast();
        keyword = getIntent().getStringExtra(IntentKey.SEARCH_KEYWORD);
        initView();
        showStatusLayout(LOADING);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isClickSubscribeBtn) {
            return;
        }
        refreshData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mLoginReceiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.SEARCH_WEMEDIA);
                finish();
                break;
            case R.id.no_net_layer:
            case R.id.load_fail_layout:
                showStatusLayout(LOADING);
                refreshData();
                break;
        }
    }

    private void refreshData() {
        currentPage = 1;
        positionId = "";
        searchWeMedia(keyword);
    }

    @Override
    public void onSubscribeClickListener(WeMediaList.WeMediaListEntity entity) {
        if (!NetUtils.isNetAvailable(SearchWeMediaActivity.this)) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        this.entity = entity;
        if (User.isLogin()) {
            int state = entity.getFollowed();
            state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            subscribe(entity, getUserId(), state);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(SearchWeMediaActivity.this);
        }
    }

    @Override
    public void onItemClickListener(WeMediaList.WeMediaListEntity entity) {
        IntentUtils.startWeMediaHomePageActivity(SearchWeMediaActivity.this, entity.getWeMediaID() + "", "");
    }

    /**
     * 注册登录广播
     */
    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING && entity != null) {
                    if (isClickSubscribeBtn) {
                        subscribe(entity, getUserId(), SubscribeRelationModel.SUBSCRIBE);
                    } else {
                        refreshData();
                    }
                    isClickSubscribeBtn = false;
                } else {
                    isClickSubscribeBtn = false;
                    refreshData();
                }
            }
        }
    };

    private void initView() {
        findViewById(R.id.back).setOnClickListener(this);
        ((TextView) findViewById(R.id.title)).setText(R.string.we_media_related);
        loading_layout = findViewById(R.id.loading_layout);
        no_net_layer = findViewById(R.id.no_net_layer);
        no_net_layer.setOnClickListener(this);
        load_fail_layout = findViewById(R.id.load_fail_layout);
        load_fail_layout.setOnClickListener(this);
        gridView = (PullToRefreshGridView) findViewById(R.id.gridView);
        gridView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        gridView.getRefreshableView().setNumColumns(3);//展示3列
        gridView.setShowIndicator(false);
        gridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                searchWeMedia(keyword);
            }
        });
        adapter = new WeMediaListAdapter();
        adapter.setClickListener(this);
        adapter.setSize(WeMediaListAdapter.SIZE_MAX);
        gridView.getRefreshableView().setAdapter(adapter);
    }

    /**
     * 获取搜索相关自媒体数据
     *
     * @param keyword 关键字
     */
    private void searchWeMedia(String keyword) {
        WeMediaDao.searchWeMediaList(keyword,
                getUserId(),
                DataInterface.PAGESIZE_18,
                positionId,
                System.currentTimeMillis() + "",
                WeMediaList.class,
                new Response.Listener<WeMediaList>() {
                    @Override
                    public void onResponse(WeMediaList response) {
                        gridView.onRefreshComplete();
                        handleResponseEvent(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        gridView.onRefreshComplete();
                        handleErrorEvent(error);
                    }
                }, false);
    }

    /**
     * 处理响应事件
     */
    private void handleResponseEvent(WeMediaList response) {
        if (response != null && !ListUtils.isEmpty(response.getWeMediaList())) {
            if (currentPage == 1) {
                list.clear();
            }
            List<WeMediaList.WeMediaListEntity> handleList = handleData(response.getWeMediaList());
            if (handleList.size() > 0) {
                list.addAll(handleList);
            } else {
                if (currentPage > 1) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                }
            }
            showStatusLayout(NORMAL);
            adapter.setList(list);
            currentPage++;
        } else {
            if (currentPage == 1) {
                showStatusLayout(ERROR);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 处理错误响应
     */
    private void handleErrorEvent(VolleyError error) {
        if (currentPage == 1) {
            if (error != null && error instanceof NetworkError) {
                showStatusLayout(NO_NET);
            } else {
                showStatusLayout(ERROR);
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 数据容错、去重处理
     */
    private List<WeMediaList.WeMediaListEntity> handleData(List<WeMediaList.WeMediaListEntity> weMediaList) {
        if (weMediaList.size() > 0) {
            positionId = weMediaList.get(weMediaList.size() - 1).getWeMediaID();
        }
        List<WeMediaList.WeMediaListEntity> tempList = new ArrayList<>();
        for (WeMediaList.WeMediaListEntity entity : weMediaList) {
            if (TextUtils.isEmpty(entity.getName())
                    || TextUtils.isEmpty(entity.getWeMediaID())
                    || list.contains(entity)) {//容错、去重
                continue;
            }
            tempList.add(entity);
        }
        return tempList;
    }

    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final WeMediaList.WeMediaListEntity entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(entity.getWeMediaID(), uid, type, System.currentTimeMillis() + "", SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        //refreshData();
                        int followNo = 0;
                        if (TextUtils.isDigitsOnly(entity.getFollowNo())) {
                            followNo = Integer.parseInt(entity.getFollowNo());
                        }
                        boolean isSubscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        entity.setFollowed(type);
                        entity.setFollowNo(String.valueOf(isSubscribe ? (++followNo) : (--followNo)));
                        adapter.notifyDataSetChanged();
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, isSubscribe, entity.getName());
                        ToastUtils.getInstance().showShortToast(isSubscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    private void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(SearchWeMediaActivity.this, action, title, "");
    }

    private static final int LOADING = 0;//加载中
    private static final int NORMAL = 1;//正常
    private static final int ERROR = 2;//数据加载失败
    private static final int NO_NET = 3;//无网

    /**
     * 显示状态布局
     */
    private void showStatusLayout(int status) {
        switch (status) {
            case LOADING:
                loading_layout.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                load_fail_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NORMAL:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
                load_fail_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case ERROR:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
                load_fail_layout.setVisibility(View.VISIBLE);
                no_net_layer.setVisibility(View.GONE);
                break;
            case NO_NET:
                loading_layout.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
                load_fail_layout.setVisibility(View.GONE);
                no_net_layer.setVisibility(View.VISIBLE);
                break;
        }
    }
}
