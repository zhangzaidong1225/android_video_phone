package com.ifeng.newvideo.search.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.*;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.search.SearchResultTransformUtils;
import com.ifeng.newvideo.search.adapter.SearchHotWordsAdapter;
import com.ifeng.newvideo.search.adapter.SearchResultAdapter;
import com.ifeng.newvideo.search.header.SearchWeMediaHeader;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.smart.SmartStatistic;
import com.ifeng.newvideo.statistics.smart.domains.SmartSearch;
import com.ifeng.newvideo.ui.basic.BaseFragment;
import com.ifeng.newvideo.ui.subscribe.interfaced.RequestData;
import com.ifeng.newvideo.widget.UIStatusLayout;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshListView;
import com.ifeng.video.core.exception.ResponseEmptyException;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.SearchDao;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.SearchHotWordsModel;
import com.ifeng.video.dao.db.model.SearchResultListModel;
import com.ifeng.video.dao.db.model.SearchResultModel;
import com.ifeng.video.dao.db.model.subscribe.WeMediaList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 搜索结果Fragment
 * Created by Administrator on 2016/9/27.
 */
public class SearchResultFragment extends BaseFragment implements RequestData {
    private static final Logger logger = LoggerFactory.getLogger(SearchResultFragment.class);
    public static final String SEARCH_KEYWORD = "searchKeyword";

    private UIStatusLayout uiStatusLayout;
    private SearchWeMediaHeader weMediaHeader;
    private PullToRefreshListView listView;
    private LinearLayout emptyLayout;//搜索结果为空布局
    private View divider;//分割线
    private View searchRecommend;//搜索推荐文字
    private GridView gridView;//搜索推荐GridView

    private SearchResultAdapter adapter;//搜素相关结果适配器
    private SearchHotWordsAdapter recommendAdapter;//推荐适配器

    private List<SearchHotWordsModel.SearchDefaultItem> recommendList = new ArrayList<>();//推荐List
    private List<SearchResultModel> searchResultList = new ArrayList<>();//搜索结果List

    private String unKeyword;//搜索关键字：未encode
    private String keyword;//搜索关键字：encode
    private int currentPage = 1;//当前页码

    private boolean isGetVideoDataFail = true;//获取相关视频是否失败
    private boolean isFirstCome = true;//是否是第一次进入执行OnResume()方法，如果是第一次不请求相关自媒体数据

    private OnRecommendItemClick itemClick;

    public static SearchResultFragment newInstance(String keyword) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SEARCH_KEYWORD, keyword);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof SearchActivity) {
            this.itemClick = (OnRecommendItemClick) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            unKeyword = getArguments().getString(SEARCH_KEYWORD);
            keyword = unKeyword;
            try {
                keyword = URLEncoderUtils.encodeInUTF8(keyword);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                keyword = "";
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_result_fragment, container, false);
        initView(view);
        initRecommendAdapter();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        weMediaHeader.setKeyword(keyword);
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);//加载...
        search(keyword);//获取搜索结果
        getHotWords();//获取推荐搜索词
    }

   private long enterTime;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
                | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);//不弹出键盘
        if (User.isLogin()) {//如果用户登录，onResume()时刷新自媒体数据,否则不刷新
            getSearchWeMediaList(keyword);//获取相关自媒体
        }
        enterTime=System.currentTimeMillis();
        isFirstCome = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(emptyLayout!=null&&emptyLayout.isShown()) {
            PageActionTracker.endPageSearchEmpty(enterTime);
        }else{
            PageActionTracker.endPageSearchResult(enterTime);
        }
    }

    @Override
    protected void requestNet() {
    }

    /**
     * 整体无网、加载失败点击重新请求数据
     */
    @Override
    public void requestData() {
        emptyLayout.setVisibility(View.GONE);
        uiStatusLayout.setVisibility(View.VISIBLE);
        uiStatusLayout.setStatus(UIStatusLayout.LOADING);//加载...
        currentPage = 1;
        search(keyword);
    }

    private void initView(View view) {
        emptyLayout = (LinearLayout) view.findViewById(R.id.empty_layout);
        divider = view.findViewById(R.id.view_separate);
        emptyLayout.setVisibility(View.GONE);//开始Gone掉
        searchRecommend = emptyLayout.findViewById(R.id.search_recommend);
        gridView = (GridView) emptyLayout.findViewById(R.id.grid_recommend);

        weMediaHeader = new SearchWeMediaHeader(getActivity());
        weMediaHeader.setRefreshWeMediaList(new SearchWeMediaHeader.RefreshWeMediaList() {
            @Override
            public void refreshWeMediaList() {//刷新自媒体数据
                getSearchWeMediaList(keyword);
            }
        });
        uiStatusLayout = (UIStatusLayout) view.findViewById(R.id.ui_status_layout);
        uiStatusLayout.setVisibility(View.VISIBLE);
        uiStatusLayout.setLoadingText(R.string.common_onload);
        uiStatusLayout.setRequest(this);

        listView = uiStatusLayout.getListView();
        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        listView.setShowIndicator(false);
        listView.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_NEVER);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                search(keyword);
            }
        });
        adapter = new SearchResultAdapter();
        listView.getRefreshableView().addHeaderView(weMediaHeader);//添加HeaderView
        listView.getRefreshableView().setFooterDividersEnabled(true);
        listView.getRefreshableView().setAdapter(adapter);
    }

    /**
     * 初始化热词推荐适配器
     */
    private void initRecommendAdapter() {
        recommendAdapter = new SearchHotWordsAdapter(getActivity(), SearchHotWordsAdapter.SIZE_SIX);
        gridView.setAdapter(recommendAdapter);
        recommendAdapter.setClickListener(new SearchHotWordsAdapter.ClickListener() {
            @Override
            public void onKeywordClick(String key, int pos) {
                unKeyword = key;
                try {
                    keyword = URLEncoderUtils.encodeInUTF8(key);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    keyword = "";
                }
                requestData();//在当前页请求数据
                if (itemClick != null) {
                    itemClick.onRecommendItemClick(key);
                }
            }
        });
    }

    /**
     * 获取热门搜索词
     */
    private void getHotWords() {
        recommendList.clear();
        SearchDao.getSearchRecommendWords(SearchHotWordsModel.class,
                new Response.Listener<SearchHotWordsModel>() {
                    @Override
                    public void onResponse(SearchHotWordsModel response) {
                        if (response == null || ListUtils.isEmpty(response.getWordlist())) {
                            emptyLayout.setGravity(Gravity.CENTER);
                            searchRecommend.setVisibility(View.GONE);
                            gridView.setVisibility(View.GONE);
                            divider.setVisibility(View.GONE);
                        } else {
                            searchRecommend.setVisibility(View.VISIBLE);
                            gridView.setVisibility(View.VISIBLE);
                            divider.setVisibility(View.VISIBLE);
                            recommendList = response.getWordlist();
                            recommendAdapter.setData(recommendList);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        emptyLayout.setGravity(Gravity.CENTER);
                        searchRecommend.setVisibility(View.GONE);
                        gridView.setVisibility(View.GONE);
                        divider.setVisibility(View.GONE);
                    }
                }, false);
    }

    /**
     * 获取搜索相关自媒体数据
     *
     * @param keyword 关键字
     */
    private void getSearchWeMediaList(String keyword) {
        boolean searchWeMedia = !isFirstCome//不是第一次进入
                && !isGetVideoDataFail//相关视频数据请求成功
                && NetUtils.isNetAvailable(getActivity());//有网
        if (!searchWeMedia) {
            return;
        }
        WeMediaDao.searchWeMediaList(keyword,
                getUserId(),
                DataInterface.PAGESIZE_18,
                "",
                System.currentTimeMillis() + "",
                WeMediaList.class,
                new Response.Listener<WeMediaList>() {
                    @Override
                    public void onResponse(WeMediaList response) {
                        if (response != null && !ListUtils.isEmpty(response.getWeMediaList())) {
                            showLayout(handleWeMediaData(response.getWeMediaList()));
                        } else {
                            showLayout(null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showLayout(null);
                        uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
                    }
                }, false);
    }

    /**
     * 展示布局
     *
     * @param weMediaListList 搜索自媒体数据
     */
    private void showLayout(List<WeMediaList.WeMediaListEntity> weMediaListList) {
        boolean weMediaEmpty = ListUtils.isEmpty(weMediaListList);
        boolean videoEmpty = ListUtils.isEmpty(searchResultList);
        if (!weMediaEmpty && !videoEmpty) {//自媒体数据不为空，视频结果不为空：展示自媒体数据、展示视频结果
            uiStatusLayout.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            weMediaHeader.updateHeaderView(weMediaListList);
            adapter.isHeaderView(true);
        } else if (!weMediaEmpty && videoEmpty) {//自媒体数据不为空，视频结果为空：展示自媒体数据、展示视频结果为空布局
            uiStatusLayout.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            weMediaHeader.updateHeaderView(weMediaListList);
            adapter.showEmptyView();
        } else if (weMediaEmpty && !videoEmpty) {//自媒体数据为空，视频结果不为空：隐藏自媒体布局，展示视频结果
            uiStatusLayout.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
            uiStatusLayout.setStatus(UIStatusLayout.NORMAL);
            weMediaHeader.hideWeMediaView();
            adapter.isHeaderView(false);
        } else {//自媒体数据为空，视频结果为空：隐藏搜索结果布局，展示推荐搜索布局
            uiStatusLayout.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
            if (itemClick != null) {//搜索结果为空
                itemClick.onSearchResultEmpty();
            }
        }
        logger.debug("自媒体数据为空={},搜索结果空={}", weMediaEmpty, videoEmpty);
    }

    /**
     * 搜索
     *
     * @param keyword 关键字
     */
    private void search(final String keyword) {
        if (currentPage == 1) {
            sendSearchStatistics(unKeyword);
        }
        HashMap<String, String> param = new HashMap<>();
        param.put("q", keyword);//查询关键字，使用utf-8编码
        param.put("n", DataInterface.PAGESIZE_20);//返回结果条数，默认22
        param.put("p", currentPage + "");//页码
        param.put("category", "");//频道类型过滤条件
        param.put("dualf", "");//视频时长过滤条件，视频开始时间，单位为分钟
        param.put("dualt", "");//视频时长过滤条件，视频结束时间，单位为分钟
        param.put("pubtime", "");//发布时间条件,d:一天内，w:一周内，m:一月内
        param.put("s", "");//返回结果排序，1：相关度，0：按时间，默认最新最相关排序
        param.put("callback", "");//回调函数名称
        param.put("protocol", "1.0.0");//协议版本

        SearchDao.getSearchResult(SearchResultListModel.class, param,
                new Response.Listener<SearchResultListModel>() {
                    @Override
                    public void onResponse(SearchResultListModel response) {
                        listView.onRefreshComplete();
                        handleResponseEvent(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("search() error={}", error.toString());
                        listView.onRefreshComplete();
                        handleErrorEvent(error);
                    }
                }, false);

        if (currentPage > 1) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.SEARCH_RESULT);
        }
    }

    /**
     * 处理响应事件
     */
    private void handleResponseEvent(SearchResultListModel response) {
        isGetVideoDataFail = false;
        if (response != null && !ListUtils.isEmpty(response.getItems())) {
            List<SearchResultModel> temp = handleVideoData(response);
            if (temp.size() > 0) {
                searchResultList.addAll(temp);
            } else {
                if (currentPage > 1) {
                    ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
                }
            }
            if (currentPage == 1) {
                getSearchWeMediaList(keyword);//搜索相关自媒体
            }
            currentPage++;
            adapter.setKeyWord(unKeyword);
            adapter.setList(searchResultList);
        } else {
            if (currentPage == 1) {
                getSearchWeMediaList(keyword);//搜索相关自媒体
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 处理错误事件
     */
    private void handleErrorEvent(VolleyError error) {
        if (currentPage == 1) {
            if (error != null && error instanceof NetworkError) {
                isGetVideoDataFail = true;
                uiStatusLayout.setStatus(UIStatusLayout.NO_NET);//无网
            } else if (error != null && error instanceof ResponseEmptyException) {// 当结果为"[]"时，返回ResponseEmptyException，所以单独处理该Error
                isGetVideoDataFail = false;
                getSearchWeMediaList(keyword);//搜索相关自媒体
            } else {
                isGetVideoDataFail = true;
                uiStatusLayout.setStatus(UIStatusLayout.ERROR);//搜索结果失败，整体失败
            }
        } else {
            if (error != null && error instanceof NetworkError) {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            } else {
                ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
            }
        }
    }

    /**
     * 数据处理
     */
    private List<SearchResultModel> handleVideoData(SearchResultListModel response) {
        List<SearchResultModel> tempList = new ArrayList<>();
        List<SearchResultModel> transformList = SearchResultTransformUtils.resultListItemToSearchResultItem(response.getItems());
        for (SearchResultModel item : transformList) {
            boolean b = item == null
                    || TextUtils.isEmpty(item.getGuid())//容错、去重处理
                    || searchResultList.contains(item);
            if (b) {
                continue;
            }
            tempList.add(item);
        }
        return tempList;
    }

    /**
     * 对服务端返回数据进行过滤，去重处理
     *
     * @param list 服务端返回的数据
     */
    private List<WeMediaList.WeMediaListEntity> handleWeMediaData(List<WeMediaList.WeMediaListEntity> list) {
        List<WeMediaList.WeMediaListEntity> weMediaList = new ArrayList<>();
        for (WeMediaList.WeMediaListEntity entity : list) {//数据过滤、去重
            if (TextUtils.isEmpty(entity.getWeMediaID()) || TextUtils.isEmpty(entity.getName())) {
                continue;
            }
            weMediaList.add(entity);
        }
        return weMediaList;
    }

    /**
     * 获取当前用户id
     */
    private String getUserId() {
        return TextUtils.isEmpty(new User(getActivity()).getUid()) ? "" : new User(getActivity()).getUid();
    }

    /**
     * 发送搜索统计
     *
     * @param keyword 搜索关键字
     */
    private void sendSearchStatistics(String keyword) {
        SmartSearch smartSearch = new SmartSearch(getActivity(), keyword);
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.SEARCH_URL, smartSearch);
    }

    /**
     * 搜索推荐词Item点击回调
     */
    public interface OnRecommendItemClick {
        /**
         * 推荐热词点击回调方法
         */
        void onRecommendItemClick(String hotWord);

        /**
         * 搜索结果为空回调方法
         */
        void onSearchResultEmpty();
    }
}
