package com.ifeng.newvideo.search.header;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.R;

/**
 * 没有搜索结果的Header
 * Created by Administrator on 2016/9/27.
 */
public class SearchNoResultHeader extends RelativeLayout {

    public SearchNoResultHeader(Context context) {
        this(context, null);
    }

    public SearchNoResultHeader(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchNoResultHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.search_no_result, this, true);
    }
}
