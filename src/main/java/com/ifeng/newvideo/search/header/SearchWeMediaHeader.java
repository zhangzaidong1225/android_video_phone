package com.ifeng.newvideo.search.header;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.search.adapter.SearchWeMediaAdapter;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.newvideo.widget.ListViewForScrollView;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;
import com.ifeng.video.dao.db.model.subscribe.WeMediaList;

import java.util.List;

/**
 * 搜索相关自媒体Header
 * Created by Administrator on 2016/9/27.
 */
public class SearchWeMediaHeader extends RelativeLayout implements SearchWeMediaAdapter.ClickListener {

    private Context context;
    private ListViewForScrollView listView;//自媒体listView
    private View weMediaAll;//全部自媒体
    private View weMediaLayout;//自媒体布局

    private String keyword;
    private SearchWeMediaAdapter adapter;
    private WeMediaList.WeMediaListEntity entity;

    private boolean isClickSubscribeBtn = false;////是否点击了订阅按钮

    public SearchWeMediaHeader(Context context) {
        this(context, null);
    }

    public SearchWeMediaHeader(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchWeMediaHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.search_wemedia_header, this, true);
        initView(view);
        initAdapter();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        registerLoginBroadcast();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        context.unregisterReceiver(mLoginReceiver);
    }

    @Override
    public void onItemClickListener(WeMediaList.WeMediaListEntity entity, int position) {
        IntentUtils.startWeMediaHomePageActivity(context, entity.getWeMediaID() + "", "");

        int add = ++position;
        String zeroPosition = "0_" + add;
        PageActionTracker.clickSearchItem(zeroPosition, "", "");
    }

    @Override
    public void onSubscribeClickListener(WeMediaList.WeMediaListEntity entity) {
        if (!NetUtils.isNetAvailable(context)) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }
        this.entity = entity;
        if (User.isLogin()) {
            int state = entity.getFollowed();
            state = state == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            subscribe(entity, new User(context).getUid(), state);
            PageActionTracker.clickWeMeidaSub(state == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.PAGE_SEARCH);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(context);
        }
    }

    /**
     * 初始化View
     */
    private void initView(View view) {
        weMediaAll = view.findViewById(R.id.tv_wemedia_all);
        weMediaAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SEARCH_WEMEDIA_ALL, PageIdConstants.SEARCH_RESULT);
                IntentUtils.startSearchWeMediaActivity(context, keyword);

                PageActionTracker.clickSearchItem("", "", "");
            }
        });
        weMediaLayout = view.findViewById(R.id.wemedia_layout);
        listView = (ListViewForScrollView) view.findViewById(R.id.listView);
    }

    /**
     * 初始化适配器
     */
    private void initAdapter() {
        adapter = new SearchWeMediaAdapter();
        adapter.setClickListener(this);
        adapter.setSize(SearchWeMediaAdapter.SIZE_THREE);
        listView.setAdapter(adapter);
    }

    /**
     * 更新HeaderView
     */
    public void updateHeaderView(List<WeMediaList.WeMediaListEntity> list) {
        weMediaLayout.setVisibility(VISIBLE);
        weMediaAll.setVisibility(list.size() < 3 ? GONE : VISIBLE);
        adapter.setList(list);
    }

    /**
     * 隐藏自媒体布局
     */
    public void hideWeMediaView() {
        weMediaLayout.setVisibility(GONE);
    }

    /**
     * 设置查询关键字
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * 订阅
     *
     * @param entity 自媒体实体类
     * @param uid    用户id
     * @param type   0：退订，1：订阅
     */
    private void subscribe(final WeMediaList.WeMediaListEntity entity, String uid, final int type) {
        SubscribeWeMediaUtil.subscribe(entity.getWeMediaID(), uid, type, System.currentTimeMillis() + "", SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(type);
                            return;
                        }
                        int followNo = 0;
                        if (TextUtils.isDigitsOnly(entity.getFollowNo())) {
                            followNo = Integer.parseInt(entity.getFollowNo());
                        }
                        boolean isSubscribe = type == SubscribeRelationModel.SUBSCRIBE;
                        entity.setFollowed(type);
                        entity.setFollowNo(String.valueOf(isSubscribe ? (++followNo) : (--followNo)));
                        adapter.notifyDataSetChanged();
                        sendSubscribeStatistics(entity.getWeMediaID(), ColumnRecord.TYPE_WM, isSubscribe, entity.getName());

                        if (refreshWeMediaList != null) {
                            refreshWeMediaList.refreshWeMediaList();
                        }
                        ToastUtils.getInstance().showShortToast(isSubscribe ? R.string.toast_subscribe_success : R.string.toast_cancel_subscribe_success);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(type);
                    }
                });
    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：wm
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    private void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(getContext(), action, title, "");
    }

    /**
     * 注册登录广播
     */
    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        context.registerReceiver(mLoginReceiver, filter);
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING && isClickSubscribeBtn && entity != null) {
                    subscribe(entity, new User(context).getUid(), SubscribeRelationModel.SUBSCRIBE);
                    isClickSubscribeBtn = false;
                } else {
                    isClickSubscribeBtn = false;
                }
            }
        }
    };

    /**
     * 通过登录广播接收器订阅成功后刷新自媒体数据
     */
    public interface RefreshWeMediaList {
        void refreshWeMediaList();
    }

    private RefreshWeMediaList refreshWeMediaList;

    public void setRefreshWeMediaList(RefreshWeMediaList refreshWeMediaList) {
        this.refreshWeMediaList = refreshWeMediaList;
    }
}
