package com.ifeng.newvideo.search;

import android.text.TextUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * 搜索参数管理类
 * Created by cuihz on 2015/1/5.
 */
class SearchParamsManager {

    private static final Logger logger = LoggerFactory.getLogger(SearchParamsManager.class);
    public static final String KEY_QUERY_CONTENT = "q";
    public static final String KEY_ORDERBY = "s";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_DURATION_END = "dualt";
    public static final String KEY_DURATION_START = "dualf";
    public static final String KEY_PUBLIC_TIME = "pubtime";
    public static final String KEY_PAGE = "p";

    public static final int ORDERBY_SIMILAR = 1;
    public static final int ORDERBY_PUBLIC_TIME = 0;

    private static final Map<String, String> searchParams = new HashMap<String, String>();

    public static Map<String, String> getSearchParams() {
        return searchParams;
    }

    public static void put(String key, String value) {
        String encodeValue;
        if (value == null) {
            encodeValue = "";
        } else {
            try {
                encodeValue = URLEncoderUtils.encodeInUTF8(value);
            } catch (UnsupportedEncodingException e) {
                encodeValue = "";
                logger.error("search param:" + key + " value:" + value + " is error\n", e);
            }
        }
        searchParams.put(key, encodeValue);
    }

    public static int getCurPage() {
        String page = searchParams.get(KEY_PAGE);
        try {
            if (!TextUtils.isEmpty(page)) {
                return Integer.parseInt(page);
            } else {
                return 1;
            }
        } catch (Exception e) {
            logger.error("get cur page faild, str is " + page);
            return 1;
        }
    }

    public static void clearAll() {
        searchParams.clear();
    }

    public static void remove(String key) {
        searchParams.remove(key);
    }

}
