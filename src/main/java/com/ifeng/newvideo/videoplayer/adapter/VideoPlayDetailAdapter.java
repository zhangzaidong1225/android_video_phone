package com.ifeng.newvideo.videoplayer.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.fragment.CommentClickPopupWindow;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.model.CommentInfoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 点播详情页Adapter
 * 包含评论等
 * Created by fanshell on 2016/7/27.
 */
public class VideoPlayDetailAdapter extends BaseAdapter implements View.OnClickListener,
        CommentClickPopupWindow.CommentClickPopCallback {

    private CommentClickPopupWindow mPopuWindow;
    private CommentInfoModel mCommentModel;
    private List<CommentInfoModel.Newest> mComments;
    private int mCurrentPosition = -1;
    private BaseFragmentActivity mContext;

    private List<Integer> mPraisePosition;
    private VideoItem mVideoItem;
    private volatile boolean isError;
    private volatile boolean isEmpty;

    public VideoPlayDetailAdapter(BaseFragmentActivity context) {
        this.mContext = context;
        mPopuWindow = new CommentClickPopupWindow(mContext, this);
        this.mComments = new ArrayList<>();
        this.mPraisePosition = new ArrayList<>();
        isError = false;
        isEmpty = false;
    }


    public void addData(CommentInfoModel commentInfoModel) {
        isError = false;
        isEmpty = false;
        this.mComments.addAll(commentInfoModel.getComments().getNewest());
        notifyDataSetChanged();
    }

    /**
     * 添加一个本地数据
     */
    public void addLocalData(CommentInfoModel.Newest item) {
        if (isError || isEmpty) {
            isEmpty = false;
            isEmpty = false;
            this.mComments.clear();
        }
        this.mComments.add(0, item);
        //更新赞位置
        if (!mPraisePosition.isEmpty()) {
            int len = mPraisePosition.size();
            for (int i = 0; i < len; i++) {
                int value = mPraisePosition.get(i) + 1;
                mPraisePosition.set(i, value);
            }
        }

        notifyDataSetChanged();
    }

    public void setData(CommentInfoModel commentInfoModel) {
        mCommentModel = commentInfoModel;
        isError = false;
        isEmpty = false;
        mCurrentPosition = -1;
        mPraisePosition.clear();
        this.mComments.clear();
        this.mComments.addAll(commentInfoModel.getComments().getNewest());
        notifyDataSetChanged();
    }

    public CommentInfoModel getData() {
        return mCommentModel;
    }

    @Override
    public int getCount() {
        if (isEmpty || isError) {
            return 1;
        }
        return mComments.size();

    }


    @Override
    public CommentInfoModel.Newest getItem(int position) {
        if (isError || isEmpty || mComments.isEmpty()) {
            return null;
        }
        return mComments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (isEmpty) {
            return getEmptyView(parent);
        } else if (isError) {
            return getErrorView(parent);
        }
        ViewHolder viewHolder;
        //convertView.getTag() instanceof ViewHolder
        if (convertView == null || !(convertView.getTag() instanceof ViewHolder)) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_comment_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
            viewHolder.intView();
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CommentInfoModel.Newest model = mComments.get(position);
        viewHolder.update(position, model);

        return convertView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_comment_content:
                mCurrentPosition = (int) view.getTag();
                if (mPopuWindow.isShowing()) {
                    return;
                }
                mPopuWindow.show(hasPraise(), view);
                break;
            case R.id.comment_iv_talk_rl:
                mCurrentPosition = (int) view.getTag();
                onClickReply();
                break;
            case R.id.comment_iv_praise_rl:
                mCurrentPosition = (int) view.getTag();
                onClickAgree();
                break;
            case R.id.iv_vip_sign:
                if (User.isVip()) {
                    IntentUtils.startMemberCenterActivity(mContext);
                } else {
                    IntentUtils.startOpenMemberActivity(mContext);
                }
                break;
        }
    }

    //点赞
    @Override
    public void onClickAgree() {
        if (hasPraise()) {
            ToastUtils.getInstance().showShortToast("亲，您已经赞过了");
            return;
        }
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_PRAISE, PageIdConstants.PLAY_VIDEO_V);
        updatePraise();
        sendPraiseRequest();
    }

    public boolean hasPraise() {
        return mPraisePosition.contains(mCurrentPosition);
    }

    private void updatePraise() {
        mPraisePosition.add(mCurrentPosition);
        try {
            long tem = Long.parseLong(mComments.get(mCurrentPosition).getUptimes()) + 1;
            mComments.get(mCurrentPosition).setUptimes(String.valueOf(tem));
        } catch (Exception e) {

        }
        notifyDataSetChanged();
    }

    private void sendPraiseRequest() {
        if (mComments == null || mCurrentPosition >= mComments.size() || mVideoItem == null || TextUtils.isEmpty(mVideoItem.guid)) {
            return;
        }
        String params = "?docUrl=" + mVideoItem.guid + "&cmtId=" + mComments.get(mCurrentPosition).getComment_id() + "&rt=sj";
        CommentDao.voteComment(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }, params);
    }

    @Override
    public void onClickReply() {
        if (mComments == null || mCurrentPosition >= mComments.size()) {
            return;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("//@")
                .append(mComments.get(mCurrentPosition).getUname())
                .append(":")
                .append(mComments.get(mCurrentPosition).getComment_contents());

        String commit_id = mComments.get(mCurrentPosition).getComment_id();
        if (mContext instanceof ActivityVideoPlayerDetail) {
            ActivityVideoPlayerDetail detail = (ActivityVideoPlayerDetail) mContext;
            detail.showEditCommentWindow(builder.toString(), commit_id);
        }

        if (mContext instanceof ActivityTopicPlayer) {
            ActivityTopicPlayer topic = (ActivityTopicPlayer) mContext;
            topic.showEditCommentWindow(builder.toString(), commit_id);
        }

        PageActionTracker.clickBtn(ActionIdConstants.CLICK_REPLY, PageIdConstants.PLAY_VIDEO_V);
    }

    @Override
    public void onClickCopy() {
        if (mComments == null || mCurrentPosition >= mComments.size()) {
            return;
        }
        String comment_contents = mComments.get(mCurrentPosition).getComment_contents();
        if (!TextUtils.isEmpty(comment_contents)) {
            ClipboardManager myClipboard = (ClipboardManager) IfengApplication.getInstance()
                    .getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("commentContent", comment_contents);
            myClipboard.setPrimaryClip(clipData);
            ToastUtils.getInstance().showShortToast(R.string.copied_to_clipboard);
        }
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_COPY, PageIdConstants.PLAY_VIDEO_V);
    }


    private class ViewHolder {
        private NetworkImageView mUserIconView;
        private TextView mUserNameView;
        private TextView mCommentView;
        private TextView mTimeView;
        private TextView mPraiseCount;
        private View convertView;
        private ImageView mPraiseView;
        private ImageView mTalkView;
        private ImageView mVipSign;
        private View divider;

        public ViewHolder(View convertView) {
            this.convertView = convertView;
        }

        public void intView() {
            mUserIconView = (NetworkImageView) convertView.findViewById(R.id.tv_comment_user_head_icon);
            mUserNameView = (TextView) convertView.findViewById(R.id.tv_comment_username);
            divider = convertView.findViewById(R.id.comment_bottom_divider);

            mCommentView = (TextView) convertView.findViewById(R.id.tv_comment_content);
            mTimeView = (TextView) convertView.findViewById(R.id.tv_comment_publish_time);
            mPraiseCount = (TextView) convertView.findViewById(R.id.comment_tv_praise_count);
            mPraiseView = (ImageView) convertView.findViewById(R.id.comment_iv_praise);
            mTalkView = (ImageView) convertView.findViewById(R.id.comment_iv_talk);
            mVipSign = (ImageView) convertView.findViewById(R.id.iv_vip_sign);
            mVipSign.setOnClickListener(VideoPlayDetailAdapter.this);

            mCommentView.setOnClickListener(VideoPlayDetailAdapter.this);
            ((View) mTalkView.getParent()).setOnClickListener(VideoPlayDetailAdapter.this);
            ((View) mPraiseView.getParent()).setOnClickListener(VideoPlayDetailAdapter.this);
        }


        public void update(int position, CommentInfoModel.Newest model) {
            mCommentView.setTag(position);
            ((View) mPraiseView.getParent()).setTag(position);
            ((View) mTalkView.getParent()).setTag(position);
            if (model == null) {
                return;
            }
            setText(mUserNameView, model.getUname());
            setText(mCommentView, model.getComment_contents());
            setText(mTimeView, DateUtils.getCommentTime(model.getComment_date()));
            setText(mPraiseCount, model.getUptimes());

            boolean isLastItem = position == mComments.size() - 1;
            divider.setVisibility(isLastItem ? View.GONE : View.VISIBLE);

            mUserIconView.setImageUrl(model.getFaceurl(), VolleyHelper.getImageLoader());
            mUserIconView.setDefaultImageResId(R.drawable.avatar_default);
            mUserIconView.setErrorImageResId(R.drawable.avatar_default);
            if (mPraisePosition.contains(position)) {
                mPraiseView.setImageResource(R.drawable.comment_red_praise);
            } else {
                mPraiseView.setImageResource(R.drawable.comment_default_praise);
            }

            boolean isVip = model.getUser_role() != null && model.getUser_role().getVideo() != null
                    && 1 == model.getUser_role().getVideo().getVip();
            mVipSign.setVisibility(isVip ? View.VISIBLE : View.GONE);
        }
    }


    private void setText(TextView view, String text) {
        view.setText(TextUtils.isEmpty(text) ? "" : text);
    }


    public void setVideoItem(VideoItem item) {
        this.mVideoItem = item;
    }


    public synchronized void showEmptyView() {

        isEmpty = true;
        isError = false;
        notifyDataSetChanged();
    }

    public synchronized void showErrorView() {
        isEmpty = false;
        isError = true;
        notifyDataSetChanged();
    }


    /**
     * 返回加载数据空的View
     */
    private View getEmptyView(ViewGroup parent) {
        return inflaterView(parent, R.layout.video_detail_comment_empty);
    }


    /**
     * 返回加载数据失败的View
     */
    private View getErrorView(ViewGroup parent) {
        View view = inflaterView(parent, R.layout.common_load_data_fail);
        ImageView icon = (ImageView) view.findViewById(R.id.common_load_icon);
        TextView textView = (TextView) view.findViewById(R.id.empty_tv);
        icon.setImageResource(R.drawable.icon_common_load_fail);
        textView.setText(R.string.common_load_data_error);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView icon = (ImageView) view.findViewById(R.id.common_load_icon);
                TextView textView = (TextView) view.findViewById(R.id.empty_tv);
                icon.setImageResource(R.drawable.icon_common_loading);
                textView.setText(R.string.common_onloading);
                if (mListener != null) {
                    mListener.onLoadFailedClick();
                }
            }
        });

        return view;
    }

    /**
     * 加载一个View并加载其布局参数，但不挂载到root
     **/
    private View inflaterView(ViewGroup root, @LayoutRes int layoutId) {
        View view = LayoutInflater.from(root.getContext()).inflate(layoutId, root, false);
        ViewParent parent = view.getParent();
        if (root != null && parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        }
        return view;
    }

    private OnLoadFailedClick mListener;

    public void setOnLoadFailClick(OnLoadFailedClick listener) {
        this.mListener = listener;

    }

    public interface OnLoadFailedClick {
        void onLoadFailedClick();

    }

    public boolean isShowEmptyView() {
        return isEmpty;
    }

}
