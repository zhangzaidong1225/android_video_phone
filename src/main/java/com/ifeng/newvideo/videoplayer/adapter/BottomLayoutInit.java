package com.ifeng.newvideo.videoplayer.adapter;

import android.app.Activity;
import android.view.*;
import android.widget.LinearLayout;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayer;
import com.ifeng.newvideo.videoplayer.widget.BottomLayout;
import com.ifeng.video.core.utils.DisplayUtils;

/**
 * 播放基页底部操作栏View的初始化
 * 操作栏有：分享、收藏、DLNA、缓存等
 */
public class BottomLayoutInit {

    public static BottomLayout bottomLayout;

    private static final int NORMAL = 16;
    private static final int HEIGHT_OR_WIDTH = 9;
    private static final int SCREEN_WIDTH = 500;

    public static GestureDetector create(Activity activity, ViewGroup viewGroup, View.OnClickListener bottomOnClickListener) {
        bottomLayout = (BottomLayout) LayoutInflater.from(activity).inflate(R.layout.video_detail_bottom_operation,
                viewGroup, false);

        if (activity instanceof ActivityVideoPlayer) {
            ((ActivityVideoPlayer) activity).mBottomCollect = bottomLayout.bottom_collect;
        }
        viewGroup.addView(bottomLayout);
        bottomLayout.setVisibility(View.GONE);
        bottomLayout.setOnBottomItemClickListener(bottomOnClickListener);
        LinearLayout btn_layout = (LinearLayout) bottomLayout.findViewById(R.id.btn_layout);
        int windowWidth = DisplayUtils.getWindowWidth();
        if (windowWidth <= SCREEN_WIDTH) {
            for (int i = 0; i < btn_layout.getChildCount(); i++) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btn_layout.getChildAt(i).getLayoutParams();
                params.setMargins(0, 0, 0, 0);
            }
        } else {
            for (int i = 0; i < btn_layout.getChildCount(); i++) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btn_layout.getChildAt(i).getLayoutParams();
                params.setMargins(0, 0, DisplayUtils.convertDipToPixel(10), 0);
            }
        }
        PopBottomViewGestureListener popBottomViewGestureListener = new PopBottomViewGestureListener(activity, bottomLayout);
        return new GestureDetector(activity, popBottomViewGestureListener);
    }

    /**
     * 底部操作栏出现和隐藏事件
     */
    public static class PopBottomViewGestureListener extends GestureDetector.SimpleOnGestureListener {

        private final Activity activity;
        private final com.ifeng.newvideo.videoplayer.widget.BottomLayout bottomLayout;

        public PopBottomViewGestureListener(Activity activity, com.ifeng.newvideo.videoplayer.widget.BottomLayout bottomLayout) {
            this.activity = activity;
            this.bottomLayout = bottomLayout;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            if (e1 == null || e2 == null) {
                return false;
            }
            float mOldY = e1.getY();
            int y = (int) e2.getRawY();

            int videoHeight = DisplayUtils.getWindowWidth() * HEIGHT_OR_WIDTH / NORMAL;
            boolean vertical = Math.abs(distanceY) > Math.abs(distanceX);
            if (vertical && mOldY > videoHeight) {
                if (mOldY - y > 0) {
                    if (bottomLayout.isShown()) {
                        bottomLayout.hideBottomLayout();
                    }
                } else {
                    if (bottomLayout.isHiden()) {
                        bottomLayout.showBottomLayout();
                    }
                }
            }
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }
    }

}
