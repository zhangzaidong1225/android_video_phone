package com.ifeng.newvideo.videoplayer.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.exception.IllegalParamsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 栏目选集列表适配器
 */
public class AudioFMAdapter extends AbstractAsyncAdapter<VideoItem> {

    private static final Logger logger = LoggerFactory.getLogger(AudioFMAdapter.class);

    private AudioFMAdapter(FragmentBase fragment) throws IllegalParamsException {
        super(fragment.getActivity());
    }

    public AudioFMAdapter(FragmentBase fragment, List<VideoItem> response) throws IllegalParamsException {
        this(fragment);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView title;
        VideoItem program = getItem(position);
        if (program == null || inflater == null) {
            return null;
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.video_column_episode_list_item, null);
            title = (TextView) convertView.findViewById(R.id.column_played_title);
            convertView.setTag(title);
        } else {
            title = (TextView) convertView.getTag();
        }
        title.setText(TextUtils.isEmpty(program.title) ? "暂无标题" : program.title);
        if (position == selectedPos) {
            title.setTextColor(context.getResources().getColor(R.color.video_column_selected_textColor));
        } else {
            title.setTextColor(context.getResources().getColor(R.color.common_info_list_title));
        }
        return convertView;
    }

}
