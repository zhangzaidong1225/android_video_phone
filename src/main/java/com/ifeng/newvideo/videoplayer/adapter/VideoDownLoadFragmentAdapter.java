package com.ifeng.newvideo.videoplayer.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.exception.IllegalParamsException;
import com.ifeng.video.dao.db.constants.IfengType;

import java.util.List;

/**
 * 缓存fragment中ListView的Adapter
 */
public class VideoDownLoadFragmentAdapter extends AbstractAsyncAdapter<String> {


    public int[] mViewStateList;
    public String type; //初始化adapter的类型，如 Audio 、Video
    public static final int NODOWNLOAD = 0;
    public static final int PREPAREDOWNLOAD = 1;
    public static final int DOWNLOADED = 2;

    public VideoDownLoadFragmentAdapter(Context context, String type) throws IllegalParamsException {
        super(context);
        this.type = type;
    }

    @Override
    public void setList(List<String> list) throws IllegalParamsException {
        super.setList(list);
        mViewStateList = new int[list.size()];
        for (int i = 0; i < mViewStateList.length; i++) {
            mViewStateList[i] = 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView title;
        ImageView titleIcon;
        String program = list.get(position);
        if (inflater == null) {
            return null;
        }
        if (type.equals(IfengType.TYPE_AUDIO)) {
            convertView = inflater.inflate(R.layout.audio_fragment_download_list_item, null);
        } else {
            convertView = inflater.inflate(R.layout.video_fragment_download_list_item, null);
        }
        RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.down_load_bg);
        title = (TextView) convertView.findViewById(R.id.column_played_title);
        titleIcon = (ImageView) convertView.findViewById(R.id.down_icon);
        title.setText(program);
        ImageView lineTop = (ImageView) convertView.findViewById(R.id.item_line_top);
        ImageView lineBottom = (ImageView) convertView.findViewById(R.id.item_line_bottom);
        if (position == selectedPos || mViewStateList[position] == PREPAREDOWNLOAD) {
            title.setTextColor(context.getResources().getColor(R.color.common_info_list_title));
            titleIcon.setSelected(true);
            titleIcon.setVisibility(View.VISIBLE);
            lineTop.setBackgroundColor(context.getResources().getColor(R.color.video_down_load_info_item_p));
            lineBottom.setBackgroundColor(context.getResources().getColor(R.color.video_down_load_info_item_p));
            layout.setBackgroundResource(R.color.video_down_load_childfragment_bg_p);
        } else if (mViewStateList[position] == DOWNLOADED) {
            title.setTextColor(context.getResources().getColor(R.color.common_info_list_title_enable));
            titleIcon.setSelected(false);
            titleIcon.setVisibility(View.VISIBLE);
            layout.setBackgroundResource(R.color.video_down_load_childfragment_bg_e);
            lineTop.setBackgroundColor(context.getResources().getColor(R.color.video_down_load_info_item_e));
            lineBottom.setBackgroundColor(context.getResources().getColor(R.color.video_down_load_info_item_e));
        } else {
            title.setTextColor(context.getResources().getColor(R.color.common_info_list_title));
            titleIcon.setSelected(false);
            titleIcon.setVisibility(View.GONE);
            layout.setBackgroundResource(R.color.video_down_load_childfragment_bg_n);
            lineTop.setBackgroundColor(context.getResources().getColor(R.color.video_down_load_info_item_n));
            lineBottom.setBackgroundColor(context.getResources().getColor(R.color.video_down_load_info_item_n));
        }
        ImageView headr_line = (ImageView) convertView.findViewById(R.id.headr_line);
        if (position == 0) {
            headr_line.setVisibility(View.VISIBLE);
        } else {
            headr_line.setVisibility(View.GONE);
        }
        return convertView;
    }

}
