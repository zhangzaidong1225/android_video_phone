package com.ifeng.newvideo.videoplayer.adapter;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.bean.TopicPlayerItem;
import com.ifeng.newvideo.videoplayer.bean.UITopicData;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.model.MainAdInfoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 专题详情页Adapter
 */
public class TopicPlayDetailAdapter extends BaseAdapter {
    private static final int TITLE_TYPE = 0;
    private static final int VIDEO_TYPE = 1;
    private static final int AD_TYPE = 2;

    private List<UITopicData> mTopicItems;

    private MainAdInfoModel mainAdInfoModel;
    private int mAdvertPosition = -1;
    private ActivityTopicPlayer mActivity;
    private String mItemId;

    public TopicPlayDetailAdapter(ActivityTopicPlayer activity, String itemId) {
        this.mActivity = activity;
        this.mItemId = itemId;
        this.mTopicItems = new ArrayList<>();
    }

    public void setData(List<UITopicData> list) {
        this.mTopicItems.clear();
        this.mTopicItems.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (ListUtils.isEmpty(mTopicItems)) {
            return 0;
        }
        return mTopicItems.size();
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        if (mTopicItems.get(position).isTitle) {
            return TITLE_TYPE;
        }
        if (mTopicItems.get(position).isVideo) {
            return VIDEO_TYPE;
        }
        if (mTopicItems.get(position).isAd) {
            return AD_TYPE;
        }
        return VIDEO_TYPE;
    }

    @Override
    public Object getItem(int position) {
        if (ListUtils.isEmpty(mTopicItems)) {
            return null;
        }
        return mTopicItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TitleViewHolder titleViewHolder;
        TopicViewHolder topicViewHolder;
        int type = getItemViewType(position);
        switch (type) {
            case TITLE_TYPE:
                if (convertView == null || !(convertView.getTag() instanceof TitleViewHolder)) {
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.topic_play_item_title, parent, false);
                    titleViewHolder = new TitleViewHolder(convertView);
                    titleViewHolder.intView();
                    convertView.setTag(titleViewHolder);
                } else {
                    titleViewHolder = (TitleViewHolder) convertView.getTag();
                }

                String title = mTopicItems.get(position).topicName;
                if (!"".equals(title)) {
                    titleViewHolder.update(title);
                } else {
                    titleViewHolder.hideView();
                }
                break;

            case VIDEO_TYPE:
                if (convertView == null || !(convertView.getTag() instanceof TopicViewHolder)) {
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listview_mix_text_picture, parent, false);
                    topicViewHolder = new TopicViewHolder(convertView);
                    topicViewHolder.intView();
                    convertView.setTag(topicViewHolder);
                } else {
                    topicViewHolder = (TopicViewHolder) convertView.getTag();
                }
                if (mAdvertPosition != -1 && position > mAdvertPosition) {
                    position = position - 1;
                }
                topicViewHolder.update(mTopicItems.get(position));
                CommonStatictisListUtils.getInstance().addVideoDetailFocusList(mActivity, mTopicItems.get(position).item.itemId, position, PageIdConstants.REFTYPE_EDITOR, mItemId,
                        0, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.normal);
                break;
            case AD_TYPE:
                //large
                final TopicPlayerItem material = mTopicItems.get(position).item;
                TextView titleView;
                TextView tag;
                if (null != material && TextUtils.isEmpty(material.getImageURL())) {
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad_empty_layout, parent, false);
                    Log.d("topic", "Ad is Empty");
                    AdvertExposureDao.addIfengAdvExposureStatistics(material.getAdId(), null, material.getAdAction().getPvurl());
                    break;
                }

                if ("large".equals(material.getAdConditions().getShowType().trim())) {
                    //大图版
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kk_live, parent, false);
                    tag = (TextView) convertView.findViewById(R.id.tv_ad_banner_label);
                    titleView = (TextView) convertView.findViewById(R.id.tv_kk_title);
                    TextView online_count = (TextView) convertView.findViewById(R.id.tv_online_count);
                    NetworkImageView image = (NetworkImageView) convertView.findViewById(R.id.iv_right_picture_0);
                    setAdvertTag(material, tag);
                    titleView.setText(material.getText());
                    online_count.setText(material.getKkrand());
                    setImageUrl(image, material.getImageURL(), R.drawable.bg_default_ad_banner_big);
                } else {
                    // 其他版式
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_mix_text_pic, parent, false);

                    titleView = (TextView) convertView.findViewById(R.id.tv_left_title);
                    TextView adDesc = (TextView) convertView.findViewById(R.id.tv_ad_desc);
                    tag = (TextView) convertView.findViewById(R.id.tv_ad_tag);
                    TextView download = (TextView) convertView.findViewById(R.id.tv_ad_download);
                    NetworkImageView right_picture = (NetworkImageView) convertView.findViewById(R.id.niv_right_picture);

                    titleView.setText(material.getText());
                    setImageUrl(right_picture, material.getImageURL(), R.drawable.bg_default_mid);

                    //设置download的显示
                    download.setVisibility(View.GONE);
                    final MainAdInfoModel.AdMaterial.AdConditions conditions = material.getAdConditions();
                    String showType = conditions != null ? conditions.getShowType() : "";
                    if (ChannelBaseAdapter.AD_TYPE_APP.equalsIgnoreCase(showType)) {
                        download.setVisibility(View.VISIBLE);
                        ((View) (download.getParent())).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (material != null && material.getAdAction() != null) {
                                    MainAdInfoModel.AdMaterial.AdAction adAction = material.getAdAction();
                                    AdvertExposureDao.sendAdvertClickReq(material.getAdId(), adAction.getAsync_click());

                                    List<String> downloadCompleteUrl = new ArrayList<>();
                                    if (!ListUtils.isEmpty(material.getAdAction().getAsync_downloadCompletedurl())) {
                                        downloadCompleteUrl.addAll(material.getAdAction().getAsync_downloadCompletedurl());
                                    }
                                    if (!ListUtils.isEmpty(material.getAdAction().getDownloadCompletedurl())) {
                                        downloadCompleteUrl.addAll(material.getAdAction().getDownloadCompletedurl());
                                    }
                                    DownLoadUtils.download(IfengApplication.getAppContext(), material.getAdId(), adAction.getLoadingurl(),
                                            (ArrayList<String>) adAction.getAsync_download(), (ArrayList<String>) downloadCompleteUrl);
                                }
                            }
                        });
                    }
                    //描述的修改
                    String desc = material.getVdescription();
                    if (!TextUtils.isEmpty(desc)) {
                        adDesc.setText(desc);
                    }
                    //广告标签
                    setAdvertTag(material, tag);
                }
                //expose
                AdvertExposureDao.addIfengAdvExposureForChannel(material.getAdId(), material.getAdPositionId(),
                        material.getAdAction().getPvurl(), material.getAdAction().getAdpvurl());
                ADRecord.addAdShow(material.getAdId(), ADRecord.AdRecordModel.ADTYPE_INFO);
                break;
            default:
                break;
        }
        return convertView;
    }

    /**
     * 广告标签
     *
     * @param material
     * @param tag
     */
    private void setAdvertTag(TopicPlayerItem material, TextView tag) {
        tag.setVisibility(View.GONE);
        MainAdInfoModel.AdMaterial.Icon icon = material.getIcon();
        if (icon != null && icon.getShowIcon() == 1) {
            String text = icon.getText();

            if (TextUtils.isEmpty(text)) {
                text = "广告";
            }
            tag.setText(text);
            tag.setTextColor(Color.parseColor("#2a90d7"));
            tag.setBackgroundResource(R.drawable.home_item_tag_blue);
            tag.setVisibility(View.VISIBLE);
        }
    }


    private static class TopicViewHolder {
        private NetworkImageView mAvatarView;
        private NetworkImageView mPicView;
        private TextView mTitleView;
        private TextView mAuthorNameView;
        private TextView mPlayCountView;
        private TextView mPlayDuration;
        private View convertView;

        public TopicViewHolder(View convertView) {
            this.convertView = convertView;
        }

        public void intView() {
            mAvatarView = (NetworkImageView) convertView.findViewById(R.id.niv_left_emoji);
            mPicView = (NetworkImageView) convertView.findViewById(R.id.niv_right_picture);
            mTitleView = (TextView) convertView.findViewById(R.id.tv_left_title);
            mAuthorNameView = (TextView) convertView.findViewById(R.id.tv_author);
            mPlayCountView = (TextView) convertView.findViewById(R.id.tv_play_times);
            mPlayDuration = (TextView) convertView.findViewById(R.id.tv_duration);
        }

        public void update(UITopicData data) {
            if (data == null || data.item == null) {
                return;
            }
            mTitleView.setText(data.item.name);
            setImageUrl(mPicView, data.item.image, R.drawable.bg_default_mid);
            WeMedia media = data.item.weMedia;
            if (media != null) {
                mAvatarView.setVisibility(View.GONE);
//                setImageUrl(mAvatarView, media.headPic, R.drawable.avatar_default);
                mAuthorNameView.setText(media.name);
                mAuthorNameView.setVisibility(View.VISIBLE);
            } else {
                mAvatarView.setVisibility(View.GONE);
                mAuthorNameView.setText("");
                mAuthorNameView.setVisibility(View.GONE);
            }
            mPlayCountView.setText(StringUtils.changePlayTimes(data.item.playTime));
            mPlayDuration.setText(StringUtils.changeDuration(data.item.duration));
            if (data.isHighlight) {
                mTitleView.setTextColor(Color.RED);
            } else {
                mTitleView.setTextColor(Color.parseColor("#262626"));
            }
        }

        private void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
            imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
            imageView.setDefaultImageResId(defaultImgResId);
            imageView.setErrorImageResId(defaultImgResId);
        }
    }


    private static class TitleViewHolder {

        private TextView mTopicTitle;

        private View convertView;

        private View mRootView;

        public TitleViewHolder(View convertView) {
            this.convertView = convertView;
        }

        public void intView() {
            mTopicTitle = (TextView) convertView.findViewById(R.id.topic_play_title);
            mRootView = convertView.findViewById(R.id.topic_play_title_container);
        }

        public void update(String title) {
            mRootView.setVisibility(View.VISIBLE);
            mTopicTitle.setText(title);
        }

        public void hideView() {
            mRootView.setVisibility(View.GONE);
        }
    }


    public void changeHighlight(VideoItem videoItem) {
        if (videoItem == null || TextUtils.isEmpty(videoItem.itemId)) {
            return;
        }
        UITopicData tem;
        for (UITopicData mTopicItem : mTopicItems) {
            tem = mTopicItem;
            if (tem.item == null || !videoItem.itemId.equals(tem.item.itemId)) {
                tem.isHighlight = false;
            } else {
                tem.isHighlight = true;
            }
        }
//        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }


    /**
     * 设置广告
     */
    public void setAdInformation(MainAdInfoModel adInformation) {
        this.mainAdInfoModel = adInformation;
        try {
            this.mAdvertPosition = Integer.parseInt(adInformation.getAdMaterials().get(0).getAdConditions().getIndex());
            notifyDataSetChanged();
        } catch (Exception e) {
            this.mAdvertPosition = -1;
        }
    }


    private void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
    }


}
