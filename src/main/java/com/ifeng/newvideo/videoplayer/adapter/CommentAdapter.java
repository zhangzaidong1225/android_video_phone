package com.ifeng.newvideo.videoplayer.adapter;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.videoplayer.fragment.CommentClickPopupWindow;
import com.ifeng.newvideo.videoplayer.fragment.CommentFragment;
import com.ifeng.newvideo.videoplayer.interfaced.CommentDataCallBack;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.*;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.model.CommentInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * 评论列表适配器
 */
public class CommentAdapter extends BaseAdapter implements View.OnClickListener {


    private static final Logger logger = LoggerFactory.getLogger(CommentAdapter.class);
    private final Context context;
    private final CommentFragment commentFragment;
    private LayoutInflater inflater;
    List<CommentInfoModel.Newest> newests;
    public List<Integer> voteds = new ArrayList<>();
    public CommentClickPopupWindow commentClickPopupWindow;

    public CommentAdapter(Context context, CommentFragment commentFragment) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.commentFragment = commentFragment;
        //this.commentClickPopupWindow = new CommentClickPopupWindow(context, this);
    }

    @Override
    public int getCount() {
        return newests == null ? 0 : newests.size();
    }

    @Override
    public Object getItem(int position) {
        return newests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.video_comment_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.user_head_img = (NetworkImageView) convertView.findViewById(R.id.tv_comment_user_head_icon);
            viewHolder.comment_contents = (EditText) convertView.findViewById(R.id.tv_comment_content);
            viewHolder.user_name = (TextView) convertView.findViewById(R.id.tv_comment_username);
            viewHolder.time = (TextView) convertView.findViewById(R.id.tv_comment_publish_time);
            viewHolder.count = (TextView) convertView.findViewById(R.id.comment_tv_praise_count);
            viewHolder.vote_img = (ImageView) convertView.findViewById(R.id.comment_iv_praise);
            viewHolder.vote_img_rl = convertView.findViewById(R.id.comment_iv_praise_rl);
            viewHolder.reply_img = (ImageView) convertView.findViewById(R.id.comment_iv_talk);
            viewHolder.reply_img_rl = convertView.findViewById(R.id.comment_iv_talk_rl);
            viewHolder.vip_sign = (ImageView) convertView.findViewById(R.id.iv_vip_sign);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CommentInfoModel.Newest currentNewest = newests.get(position);
        String faceUrl = currentNewest.getFaceurl();
        if (TextUtils.isEmpty(faceUrl)) {
            faceUrl = currentNewest.getUserFace();
        }
        logger.debug("the position == {} and the url == {}", position, faceUrl);
        viewHolder.user_head_img.setImageUrl(faceUrl, VolleyHelper.getImageLoader());
        viewHolder.user_head_img.setDefaultImageResId(R.drawable.avatar_default);
        viewHolder.user_head_img.setErrorImageResId(R.drawable.avatar_default);
        viewHolder.comment_contents.setText(currentNewest.getComment_contents());
        viewHolder.user_name.setText(currentNewest.getUname());
        if (voteds.contains(position)) {
            viewHolder.vote_img.setImageResource(R.drawable.comment_red_praise);
        } else {
            viewHolder.vote_img.setImageResource(R.drawable.comment_default_praise);
        }
        viewHolder.vote_img_rl.setTag(position);
        viewHolder.reply_img_rl.setTag(position);
        viewHolder.comment_contents.setTag(position);
        viewHolder.comment_contents.setOnClickListener(this);
        viewHolder.vote_img_rl.setOnClickListener(this);
        viewHolder.reply_img_rl.setOnClickListener(this);
        if (currentNewest.getComment_date() != null) {
            String timeStr = DateUtils.getCommentTime(currentNewest.getComment_date());
            logger.debug("the comment adapter time = {}", timeStr);
            viewHolder.time.setText(timeStr);
        }
        viewHolder.count.setText(currentNewest.getUptimes());
        if (currentNewest.getUser_role() != null && currentNewest.getUser_role().getVideo() != null&&1 == currentNewest.getUser_role().getVideo().getVip()) {
            viewHolder.vip_sign.setVisibility(View.VISIBLE);
            } else {
            viewHolder.vip_sign.setVisibility(View.GONE);
            }
        return convertView;
    }


    @Override
    public void onClick(View v) {
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }
        if (!NetUtils.isNetAvailable(context)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        int position = (int) v.getTag();
        if (TextUtils.isEmpty(newests.get(position).getComment_id())) {
            return;
        }
        switch (v.getId()) {
            case R.id.tv_comment_content://内容
                logger.debug("内容");
                if (commentClickPopupWindow != null) {
                    if (voteds.contains(position)) {
                        //commentClickPopupWindow.show(true, v, position);
                    } else {
                        //commentClickPopupWindow.show(false, v, position);
                    }
                }
                break;
            case R.id.comment_iv_praise_rl://点赞
                vote(position);
                break;
            case R.id.comment_iv_talk_rl://回复
                reply(position);
                break;
            default:
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void copy(int position) {
        String comment_contents = newests.get(position).getComment_contents();
        if (context != null && !TextUtils.isEmpty(comment_contents)) {
            ClipboardManager myClipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            if (BuildUtils.hasHoneycomb()) {
                ClipData clipData = ClipData.newPlainText("commentContent", comment_contents);
                myClipboard.setPrimaryClip(clipData);
            }
            ToastUtils.getInstance().showShortToast(R.string.copied_to_clipboard);
        }
    }

    private void reply(int position) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_REPLY, PageIdConstants.PLAY_VIDEO_V);
        ((CommentDataCallBack) commentFragment.getActivity()).callShowEditCommentWindow("//@" + newests.get(position).getUname() + ":" + newests.get(position).getComment_contents());
    }

//    @Override
//    public void onClickAgree(int position) {
//        vote(position);
//    }
//
//    @Override
//    public void onClickReply(int position) {
//        reply(position);
//    }
//
//    @Override
//    public void onClickCopy(int position) {
//        copy(position);
//    }

    private static class ViewHolder {
        NetworkImageView user_head_img;
        EditText comment_contents;
        TextView user_name;
        TextView time;
        TextView count;
        ImageView vote_img;
        ImageView reply_img;
        View vote_img_rl;
        View reply_img_rl;
        ImageView vip_sign;
    }

    public void setData(List<CommentInfoModel.Newest> newests) {
        this.newests = newests;
    }

    /**
     * 点赞
     *
     * @param position 评论position
     */
    private void vote(int position) {
        if (voteds.contains(position)) {
            ToastUtils.getInstance().showShortToast("亲，您已经赞过了");
            return;
        }
        String oldTimes = newests.get(position).getUptimes();
        int upTimes = 0;
        if (!TextUtils.isEmpty(oldTimes)) {
            upTimes = Integer.parseInt(newests.get(position).getUptimes()) + 1;
        }
        newests.get(position).setUptimes(upTimes + "");
        voteds.add(position);
        notifyDataSetChanged();
        String guid = "";
        try {
            guid = URLEncoderUtils.encodeInUTF8(commentFragment.getGuid());
        } catch (UnsupportedEncodingException e) {
            guid = commentFragment.getGuid();
        }
        String params = "?guid=" + guid + "&cmtId=" + newests.get(position).getComment_id() + "&rt=sj";

        CommentDao.voteComment(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                logger.debug("the vote response is == {}", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.debug("the vote error");
            }
        }, params);

        PageActionTracker.clickBtn(ActionIdConstants.CLICK_PRAISE, PageIdConstants.PLAY_VIDEO_V);
    }

}
