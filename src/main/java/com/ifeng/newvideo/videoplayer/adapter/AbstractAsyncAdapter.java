package com.ifeng.newvideo.videoplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import com.ifeng.video.core.exception.IllegalParamsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * adapter基础类
 * 继承后重写getView和setList
 */
public abstract class AbstractAsyncAdapter<T> extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(AbstractAsyncAdapter.class);

    protected List<T> list;

    protected Context context;

    protected LayoutInflater inflater;

    /**
     * 聚焦的位置
     */
    int selectedPos;


    protected AbstractAsyncAdapter(Context context) throws IllegalParamsException {
        super();
        list = new ArrayList<T>();
        this.context = context;
        if (context != null) {
            inflater = LayoutInflater.from(context);
        } else {
            throw new IllegalParamsException("context is null");
        }
    }

    private AbstractAsyncAdapter(Context context, int listItemResId) throws IllegalParamsException {
        super();
        list = new ArrayList<T>();
        this.context = context;
        if (context != null) {
            inflater = LayoutInflater.from(context);
        } else {
            throw new IllegalParamsException("context is null");
        }
    }

    protected AbstractAsyncAdapter() {
    }

    public void setSelected(int position) {
        selectedPos = position;
        notifyDataSetChanged();
    }

    public boolean isSelectedPos(int pos) {
        return selectedPos >= 0 && selectedPos == pos;
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    /**
     * 重置选定的ItemView
     */
    public void resetSelected() {
        selectedPos = -1;
    }

    @Override
    public int getCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    public T getItem(int arg0) {
        return (arg0 < 0 || arg0 >= getCount()) ? null : list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    public void setList(List<T> list) throws IllegalParamsException {
        if (this.list != null) {
            this.list.clear();
            this.list = new ArrayList<T>(list.size());
            this.list.addAll(list);
            notifyDataSetChanged();
        } else {
            throw new IllegalParamsException("list is null");
        }
    }

    public void addList(List<T> list) {
        if (this.list == null) {
            this.list = list;
        } else {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void addItem(T item) {
        if (this.list == null) {
            this.list = new ArrayList<T>();
        }
        this.list.add(item);

    }

    public List<T> getList() {
        return list;
    }

    public void removeItem(int position) {
        this.list.remove(position);
    }

    public void clearList() {
        this.list.clear();
    }

    public Context getContext() {
        return context;
    }

}
