package com.ifeng.newvideo.videoplayer.base;


import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * 作为fragment的父类，处理一些公用的逻辑
 */
public class FragmentBase extends Fragment implements OnClickListener {

    //是否是第一次注册网络改变的广播接收者 Action :ConnectivityManager.CONNECTIVITY_ACTION
    protected boolean isFirstRegisterNetworkChangeReceiver;
    private DisplayMetrics dm;
    protected View progressView;
    protected View emptyView_RL;
    protected ImageView empty_img;
    protected TextView empty_tv;
    protected IfengApplication app;
    protected SharePreUtils mSharePreUtils;

    public FragmentBase() {
        super();
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        dm = activity.getResources().getDisplayMetrics();
        app = IfengApplication.getInstance();
        mSharePreUtils = SharePreUtils.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getName());
    }

    public void refreshData(){}

    protected DisplayMetrics getMetrics() {
        return this.dm;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

    }

    public View getBaseView(Context context) {
        return null;
    }

    public IfengApplication getApp() {
        return app;
    }

    /**
     * 返回当前activity是否为横屏
     *
     * @return
     */
    public boolean isLandScape() {
        try {
            return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        } catch (Exception e) {
            return false;
        }
    }

}
