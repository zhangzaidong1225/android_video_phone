package com.ifeng.newvideo.videoplayer.base;

/**
 * Created by yuelong on 2014/9/12.
 */
public interface ControllerToVideoPlayerListener {
    // 切换大小屏播放器的回调函数
    void onFullScreenClick();

    //订阅按钮回调

    void onSurbseClick();

    //DLNA按钮回调
    void onDlnaClick();

    //每个清晰按钮回调
    void onSelectItemClick(int definition);

    //清晰按钮回调
    void onSelectClick();

    //锁屏按钮回调
    void onClockClick();

    //controller显示回调
    void onControllerShow();

    //controller隐藏回调
    void onControllerHide();

    //切音频回调
    void onSwitchAVMode();

    //切上下条视频频回调
    void onSwitchProgram(boolean nextOrPre);
}
