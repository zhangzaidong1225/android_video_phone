package com.ifeng.newvideo.videoplayer.base;

import android.text.TextUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;

/**
 * 专题类型的数据转换逻辑
 */
public class TransformTopicType {

    /**
     * 转换为专题详情需要的type
     *
     * @param srcType
     * @return
     */
    public static String transformType4TopicDetail(String srcType) {
        if (!TextUtils.isEmpty(srcType)) {
            if (CheckIfengType.isCmppTopic(srcType)) {
                srcType = IfengType.TYPE_TOPIC_CMPPTOPIC;
            } else if (CheckIfengType.isLianBo(srcType)) {
                srcType = IfengType.TYPE_TOPIC_AFFILIATE;
            } else if (CheckIfengType.isFocus(srcType)) {
                srcType = IfengType.TYPE_TOPIC_IFENGFOCUS;
            }
        }
        return srcType;
    }

    /**
     * 转换为专题广告参数所需要的type
     *
     * @param srcType
     * @return
     */
    public static String transformType4TopicAd(String srcType) {
        if (!TextUtils.isEmpty(srcType)) {
            if (CheckIfengType.isCmppTopic(srcType)) {
                srcType = IfengType.TYPE_TOPIC_AD_CMPPTOPIC;
            } else if (CheckIfengType.isLianBo(srcType)) {
                srcType = IfengType.TYPE_TOPIC_LIANBO;
            } else if (CheckIfengType.isFocus(srcType)) {
                srcType = IfengType.TYPE_TOPIC_FOCUS;
            }
        }
        return srcType;
    }

}
