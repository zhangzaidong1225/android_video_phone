package com.ifeng.newvideo.videoplayer.base;


public class VideoPlayerConfig {

    public static final String SOURCE = "videoapp";

    public static final String IJKPLAYER_VERSION = "ijk_v0.0.1";

    public static final String IFENGPLAYER_VERSION = "ifeng_v0.6.0";

}
