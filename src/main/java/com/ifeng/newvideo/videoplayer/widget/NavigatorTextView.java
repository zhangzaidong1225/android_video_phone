package com.ifeng.newvideo.videoplayer.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.interfaced.NavigatorTextViewOnSelectedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 播放页导航条自定义Textview控件
 */
public class NavigatorTextView extends LinearLayout implements NavigatorTextViewOnSelectedListener {

    private static final Logger logger = LoggerFactory.getLogger(NavigatorTextView.class);

    private Context ctx;
    private OnIndicatorChangeListener indicatorListener;
    private View navigatorCurr;//字体导航底部的颜色条
    private TextView textContent;//导航Tabs名称

    public NavigatorTextView(Context context) {
        super(context);
        initViews(context);
    }

    public NavigatorTextView(Context context, String resId) {
        super(context);
        initViews(context);
        this.setContentText(resId);
    }

    private NavigatorTextView(Context context, String resId, int layout_id) {
        super(context);
        this.ctx = context;
        LayoutInflater.from(context).inflate(layout_id, this);
        navigatorCurr = this.findViewById(R.id.narrow);
        textContent = (TextView) this.findViewById(R.id.related_text);
        this.setContentText(resId);
    }

    private NavigatorTextView(Context context, AttributeSet attrs, String resId) {
        super(context, attrs);
        initViews(context);
        this.setContentText(resId);
    }

    private void initViews(Context context) {
        this.ctx = context;
        LayoutInflater.from(context).inflate(R.layout.video_navigator_item_lay_text, this);
        navigatorCurr = this.findViewById(R.id.narrow);
        textContent = (TextView) this.findViewById(R.id.related_text);
    }

    /**
     * 设置tabs字体内容的名称
     *
     * @param resId
     */
    private void setContentText(String resId) {
        textContent.setText(resId);
    }

    /**
     * 设置tabs字体内容的名称和字体大小
     *
     * @param resId
     * @param w     宽
     * @param h     高
     */
    public void setContentText(String resId, int w, int h) {
        textContent.setLayoutParams(new FrameLayout.LayoutParams(w, h));
        textContent.setText(resId);
    }

    public void setIndicatorListener(OnIndicatorChangeListener listener) {
        this.indicatorListener = listener;
    }

    public OnIndicatorChangeListener getIndicatorListener() {
        return this.indicatorListener;
    }

    public interface OnIndicatorChangeListener {
        void onIndicatorChange(int viewIndex);
    }

    @Override
    public void onSelected(boolean selected) {
        navigatorCurr.setVisibility(View.INVISIBLE);
        if (selected) {
            textContent.setTextColor(ctx.getResources().getColor(R.color.video_column_selected_textColor));
        } else {
            textContent.setTextColor(ctx.getResources().getColor(R.color.common_info_list_title_tab));
        }
        textContent.setContentDescription(selected ? "1" : "0");
    }

}
