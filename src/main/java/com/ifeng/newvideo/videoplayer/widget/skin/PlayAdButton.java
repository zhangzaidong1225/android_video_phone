package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.widget.CustomNetworkImageView;
import com.ifeng.video.core.net.VolleyHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PlayAdButton extends BaseView implements UIObserver, View.OnClickListener {
    private Logger logger = LoggerFactory.getLogger(PlayAdButton.class);

    private ImageView mPlayView, mPlayViewLand;
    private CustomNetworkImageView iv_ad, iv_ad_land;
    private TextView tv_use, tv_use_land;
    private View viewContrainer;
    private View protraitView;
    private View landView;


    public PlayAdButton(Context context) {
        super(context);
    }

    public PlayAdButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlayAdButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void initView() {
        this.setBackgroundColor(Color.TRANSPARENT);
//        LayoutInflater.from(mContext)
//                .inflate(R.layout.big_pic_ad_video_play, this);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        viewContrainer = inflater.inflate(R.layout.big_pic_ad_video_play, this, true);
        protraitView = viewContrainer.findViewById(R.id.ll_porit);
        landView = viewContrainer.findViewById(R.id.land_view);
        landView.setVisibility(View.GONE);

        protraitView.findViewById(R.id.btn_play_pause_ad).setOnClickListener(this);
        protraitView.findViewById(R.id.iv_ad).setOnClickListener(this);
        protraitView.findViewById(R.id.tv_ad_left).setOnClickListener(this);

        landView.findViewById(R.id.btn_play_pause_ad).setOnClickListener(this);
        landView.findViewById(R.id.iv_ad).setOnClickListener(this);
        landView.findViewById(R.id.tv_ad_left).setOnClickListener(this);

        mPlayView = (ImageView) findViewById(R.id.btn_play_pause_ad);
        iv_ad = (CustomNetworkImageView) findViewById(R.id.iv_ad);
        tv_use = (TextView) findViewById(R.id.tv_ad_left);

        mPlayViewLand  = (ImageView) landView.findViewById(R.id.btn_play_pause_ad);
        mPlayViewLand.setOnClickListener(this);
        iv_ad_land = (CustomNetworkImageView) landView.findViewById(R.id.iv_ad);
        iv_ad_land.setOnClickListener(this);
        tv_use_land = (TextView) landView.findViewById(R.id.tv_ad_left);
        tv_use_land.setOnClickListener(this);


        this.setVisibility(View.GONE);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_play_pause_ad:
            case R.id.ll_ad_playad:
                if (mPlayerControl != null && mPlayerControl.isPlaying()) {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, false, getCurPage());
                    mPlayerControl.pause();
                } else if (mPlayerControl != null) {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, true, getCurPage());

                    if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                        if (null != mOnPlayAndPreplayClickListener) {
                            mOnPlayAndPreplayClickListener.prePlayClick();
                        }
                    }
                    if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED) {
                        if (null != mOnPlayAndPreplayClickListener) {
                            mOnPlayAndPreplayClickListener.onPlayClick();
                        }
                    }

                    mPlayerControl.start();
                }
                break;
            case R.id.iv_ad:
            case R.id.tv_ad_left:
                if (null != mOnPlayAndPreplayClickListener) {
                    mOnPlayAndPreplayClickListener.onClickStopUrl();
                }
                IntentUtils.startADActivity(mContext, "", mUIPlayContext.adItem.getAdStopUrl(), "", ADActivity.FUNCTION_VALUE_AD, mUIPlayContext.adItem.getAdTitle(), null, "",
                        "", "", null, null);
                break;
            default:
                break;
        }

    }

    public void showPlayButton() {
        if (mPlayerControl != null) {
            if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT) {
                if (mPlayerControl.isPlaying()) {
                    mPlayView.setImageResource(R.drawable.btn_pause);
                }
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    mPlayView.setImageResource(R.drawable.btn_refresh);
                }
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED) {
                    mPlayView.setImageResource(R.drawable.btn_pause_ad);
                }
            } else {
                if (mPlayerControl.isPlaying()) {
                    mPlayViewLand.setImageResource(R.drawable.btn_pause);
                }
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    mPlayViewLand.setImageResource(R.drawable.btn_refresh_land);
                }
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED) {
                    mPlayViewLand.setImageResource(R.drawable.btn_pause_land);
                }
            }
        }
        updateText();
        this.setVisibility(View.VISIBLE);

    }

    public void hiddenPlayButton() {
        this.setVisibility(View.GONE);
    }

    public void updateText() {
        if (mUIPlayContext != null) {
            if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT) {
                tv_use.setText(mUIPlayContext.adItem.getAdStopText());
                if (!TextUtils.isEmpty(mUIPlayContext.adItem.getAdStopImageUrl())) {
                    iv_ad.setImageUrl(mUIPlayContext.adItem.getAdStopImageUrl(), VolleyHelper.getImageLoader());
                    iv_ad.setDefaultImageResId(R.drawable.bg_default_pic);
                    iv_ad.setErrorImageResId(R.drawable.bg_default_pic);
                }
            } else {
                tv_use_land.setText(mUIPlayContext.adItem.getAdStopText());
                if (!TextUtils.isEmpty(mUIPlayContext.adItem.getAdStopImageUrl())) {
                    iv_ad_land.setImageUrl(mUIPlayContext.adItem.getAdStopImageUrl(), VolleyHelper.getImageLoader());
                    iv_ad_land.setDefaultImageResId(R.drawable.bg_default_pic);
                    iv_ad_land.setErrorImageResId(R.drawable.bg_default_pic);
                }
            }
        }
    }

    public boolean adViewIsEmpty() {

        if (TextUtils.isEmpty(mUIPlayContext.adItem.getAdStopText()) || TextUtils.isEmpty(mUIPlayContext.image)) {
            return true;
        }
        return false;
    }


    private onPlayAndPreplayClickListener mOnPlayAndPreplayClickListener;

    public void setOnPlayAndPrePlayClickListener(onPlayAndPreplayClickListener listener) {
        this.mOnPlayAndPreplayClickListener = listener;
    }

    public interface onPlayAndPreplayClickListener {

        void onPlayClick();

        void prePlayClick();

        void onClickStopUrl();
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT) {
            landView.setVisibility(View.GONE);
            protraitView.setVisibility(View.VISIBLE);

            if (status == IPlayer.PlayerState.STATE_PAUSED) {
                mPlayView.setImageResource(R.drawable.btn_pause_ad);
                mUIPlayContext.status = IPlayer.PlayerState.STATE_PAUSED;
            }
            if (status == IPlayer.PlayerState.STATE_PLAYING) {
                mPlayView.setImageResource(R.drawable.btn_pause);
                mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYING;
            }
            if (status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED;
                mPlayView.setImageResource(R.drawable.btn_refresh);
            }
            if (status == IPlayer.PlayerState.STATE_IDLE) {
                mUIPlayContext.status = IPlayer.PlayerState.STATE_IDLE;
            }
        } else {
            landView.setVisibility(View.VISIBLE);
            protraitView.setVisibility(View.GONE);

            if (status == IPlayer.PlayerState.STATE_PAUSED) {
                mPlayViewLand.setImageResource(R.drawable.btn_pause_land);
                mUIPlayContext.status = IPlayer.PlayerState.STATE_PAUSED;
            }
            if (status == IPlayer.PlayerState.STATE_PLAYING) {
                mPlayViewLand.setImageResource(R.drawable.btn_pause);
                mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYING;
            }
            if (status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED;
                mPlayViewLand.setImageResource(R.drawable.btn_refresh_land);
            }
            if (status == IPlayer.PlayerState.STATE_IDLE) {
                mUIPlayContext.status = IPlayer.PlayerState.STATE_IDLE;
            }
        }
    }

}
