package com.ifeng.newvideo.videoplayer.widget.skin;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IntDef;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.utils.GestureControl;
import com.ifeng.newvideo.utils.GrayInstallUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.MobileFlowUtils;
import com.ifeng.newvideo.utils.NewsSilenceInstallUtils;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.widget.AdvertView;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.BaseControllerView;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.CacheControllerView;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.DefaultControllerView;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.LiveControllerView;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by fanshell on 2016/7/25.
 */
public class VideoSkin extends BaseSkin implements UIObserver, View.OnClickListener {
    private Logger logger = LoggerFactory.getLogger(VideoSkin.class);
    /**
     * 点播
     */
    public static final int SKIN_TYPE_VOD = 1;
    /**
     * 专题
     */
    public static final int SKIN_TYPE_TOPIC = 2;
    /**
     * 电视
     */
    public static final int SKIN_TYPE_TV = 3;

    /**
     * 本地
     */
    public static final int SKIN_TYPE_LOCAL = 4;
    /**
     * 大图
     */
    public static final int SKIN_TYPE_PIC = 5;

    /**
     * fm
     */
    public static final int SKIN_TYPE_FM = 6;
    public static final int SKIN_TYPE_SPECIAL = 7;
    public static final int SKIN_TYPE_EXIT = 8;

    /**
     * 会员去广告前三次展示文字
     */
    public static int VIP_MEMBER_NO_AD_NOTICE_TIMES = 1;

    @IntDef({SKIN_TYPE_VOD, SKIN_TYPE_TOPIC, SKIN_TYPE_TV, SKIN_TYPE_LOCAL, SKIN_TYPE_PIC, SKIN_TYPE_FM, SKIN_TYPE_SPECIAL, SKIN_TYPE_EXIT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface SkinType {
    }

    private int portraitHeight = 0;
    private int portraitWidth = 0;
    private boolean videoMargin = false;

    private BaseControllerView mControllerView;
    private BufferView mBufferView;
    private StreamView mStreamView;
    private LoadView mLoadView;
    private ImageView newLoadingView;

    private ErrorView mErrorView;
    private PlayButton mPlayView;
    private AdvertView mAdvertView;
    private TitleView mTitleView;
    private PlayAdButton playAdView;
    private BigPicAdView bigPicAdView;
    private DanmuView danmuView;
    private DanmuEditView danmuEditView;


    private GestureGuideView mGestureGuideView;

    private NoNetWorkView mNoNetWorkView;
    private MobileView mMobileView;
    private NetworkConnectionReceiver mNetReceiver;
    private INetWorkListener mNetWorkListener;
    private GestureControl mGestureControl;

    private OnNetWorkChangeListener mNetChangeListener;
    private VipNoticeView mVipNoticeView;


    private static final int STATE_HIDE = 0x12;
    private static final long HIDE_DURATION = 3 * DateUtils.SECOND_IN_MILLIS;
    private static final int VIP_NOTICE_HIDE = 0x02;

    private Handler mHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                case STATE_HIDE:
                    hideControllerView();
                    break;
                case VIP_NOTICE_HIDE:
                    if (mVipNoticeView.getVisibility() == VISIBLE) {
                        mVipNoticeView.setVisibility(GONE);
                    }
                    break;
                default:
                    break;
            }

        }
    };

    private boolean isShowErrorView = false;

    public void setVideoMargin(boolean videoMargin) {
        this.videoMargin = videoMargin;
    }

    public VideoSkin(Context context) {
        super(context);
    }

    public VideoSkin(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoSkin(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public VideoSkin(Context context, int skinType) {
        super(context);
        setSkinType(skinType);
    }

    @Override
    public void initView() {
        super.initView();
        if (mNetReceiver == null) {
            mNetReceiver = new NetworkConnectionReceiver(mContext);
            mNetWorkListener = new NetWorkListener();
        }
    }

    @Override
    protected void initPlayer() {
        this.mPlayerControl.addUIObserver(this);
    }


    private AudioContentView mAudioView;

    @Override
    protected void buildComponentView() {
        if (mSkinType != SKIN_TYPE_LOCAL) {
            changeLayoutParams(mUIPlayContext.widthRadio, mUIPlayContext.heightRadio);
            changeLayoutParams();
        }
        this.setOnClickListener(this);

        mAudioView = new AudioContentView(mContext);
        addCenterView(mAudioView);
        addSkinComponent();
        initGestureView();
        initCommonView();
        if (mPlayerControl != null) {
            hideLoadingView();
            attachPlayController(mPlayerControl);
            return;
        }

        if (!NetUtils.isNetAvailable(mContext) && (mSkinType != SKIN_TYPE_LOCAL)) {
            showNoNetWork();
        } else if (NetUtils.isMobile(mContext) && !IfengApplication.mobileNetCanPlay && (mSkinType != SKIN_TYPE_LOCAL)) {
//            showMobileAlert();
            showMobileView();
        } else {
            showLoadingView();
        }
    }

    /**
     * 加载公共的View
     */
    private void initCommonView() {
        /**
         * titleView
         */
        mTitleView = new TitleView(mContext);
        if (mSkinType == SKIN_TYPE_LOCAL) {
            mTitleView.setCacheSkin(true);
        }

        mNoNetWorkView = new NoNetWorkView(mContext);
        mNoNetWorkView.setNoNetWorkClick(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNetChangeListener != null) {
                    mNetChangeListener.onNoNetWorkClick();
                }
            }
        });

        mLoadView = new LoadView(mContext);

        //mobileView
        if (!IfengApplication.mobileNetCanPlay) {
            mMobileView = new MobileView(mContext);
            addCenterView(mMobileView);
            mMobileView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getId() == R.id.video_mobile_to_order) {
                        IntentUtils.startADWebActivity(mContext, null, SharePreUtils.getInstance().getMobileEnterUrl(), null, null, "任我看", "", "", "", null, null);
                    } else {
                        IfengApplication.mobileNetCanPlay = true;
                        if (mNetChangeListener != null) {
                            ToastUtils.getInstance().showShortToast("您正在使用运营商网络");
                            mNetChangeListener.onMobileClick();
                        }
                    }
                }
            });

        }
        mPlayView = new PlayButton(mContext);
        mAdvertView = new AdvertView(mContext);
        mErrorView = new ErrorView(mContext);
        mErrorView.findViewById(R.id.video_error_retry).setOnClickListener(mRetryClickListener);
        mErrorView.findViewById(R.id.video_error_refresh).setOnClickListener(mRetryClickListener);
        mGestureGuideView = new GestureGuideView(mContext);


        addTopView(mTitleView);
        addCenterView(mNoNetWorkView);
        addCenterView(mLoadView);
        addCenterView(mPlayView);
        addCenterView(mAdvertView);
        addCenterView(mErrorView);
        addCenterView(mGestureGuideView);
        mVipNoticeView = new VipNoticeView(mContext);
        mVipNoticeView.setVisibility(GONE);
        addTopAndCenterView(mVipNoticeView);
    }

    public void setVipNoticeViewVisibility() {
        if (VIP_MEMBER_NO_AD_NOTICE_TIMES <= 3) {
            mVipNoticeView.setVisibility(VISIBLE);
            VIP_MEMBER_NO_AD_NOTICE_TIMES++;
            mHandler.removeMessages(VIP_NOTICE_HIDE);
            mHandler.removeMessages(VIP_NOTICE_HIDE);
            mHandler.sendEmptyMessageDelayed(VIP_NOTICE_HIDE, HIDE_DURATION);
        } else {
            mVipNoticeView.setVisibility(GONE);
        }
    }

    /**
     * 加载特定的View
     */
    private void addSkinComponent() {
        switch (mSkinType) {
            case SKIN_TYPE_VOD:
                mControllerView = new DefaultControllerView(mContext);
                addBottomView(mControllerView);
                mControllerView.setVisibility(View.GONE);
                mStreamView = new StreamView(mContext);
                addRightView(mStreamView);
                mControllerView.setStreamView(mStreamView);
                mBufferView = new BufferView(mContext);
                addCenterView(mBufferView);
                danmuView = new DanmuView(mContext);
                addTopView(danmuView);
                danmuEditView = new DanmuEditView(mContext);
                addRightView(danmuEditView);

                break;
            case SKIN_TYPE_TOPIC:
                mControllerView = new DefaultControllerView(mContext);
                addBottomView(mControllerView);
                mControllerView.setVisibility(View.GONE);
                mStreamView = new StreamView(mContext);
                addRightView(mStreamView);
                mControllerView.setStreamView(mStreamView);
                mBufferView = new BufferView(mContext);
                addCenterView(mBufferView);
                danmuView = new DanmuView(mContext);
                addTopView(danmuView);
                danmuEditView = new DanmuEditView(mContext);
                addRightView(danmuEditView);

                break;
            case SKIN_TYPE_TV:
                mControllerView = new LiveControllerView(mContext);
                addBottomView(mControllerView);
                mControllerView.setVisibility(View.GONE);
                mStreamView = new StreamView(mContext);
                addRightView(mStreamView);
                mControllerView.setStreamView(mStreamView);
                mBufferView = new BufferView(mContext);
                addCenterView(mBufferView);
                break;
            case SKIN_TYPE_LOCAL:
                mControllerView = new CacheControllerView(mContext);
                addBottomView(mControllerView);
                break;
            case SKIN_TYPE_PIC:
                mControllerView = new DefaultControllerView(mContext);
                addBottomView(mControllerView);
                mControllerView.setVisibility(View.GONE);
                mStreamView = new StreamView(mContext);
                addRightView(mStreamView);
                mControllerView.setStreamView(mStreamView);
                mBufferView = new BufferView(mContext);
                addCenterView(mBufferView);
                playAdView = new PlayAdButton(mContext);
                addCenterView(playAdView);
                bigPicAdView = new BigPicAdView(mContext);
                addBottomView(bigPicAdView);
                danmuView = new DanmuView(mContext);
                addTopView(danmuView);
                danmuEditView = new DanmuEditView(mContext);
                addRightView(danmuEditView);

                break;
            case SKIN_TYPE_FM:
                mControllerView = new DefaultControllerView(mContext);
                addBottomView(mControllerView);
                mControllerView.setVisibility(View.GONE);
                mBufferView = new BufferView(mContext);
                addCenterView(mBufferView);

                break;
            default:

                break;
        }
        if (mControllerView != null) {
            SeekBarView seekBarView = mControllerView.getSeekBar();
            if (seekBarView != null) {
                seekBarView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        mHandler.removeMessages(STATE_HIDE);
                        mHandler.removeMessages(STATE_HIDE);
                        if (null != danmuView) {
                            danmuView.hideView();
                            danmuView.hideDanmakuView();
                            danmuView.removeMSG();
                            danmuView.removeMSG();
                        }
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        hideAdBigView();
                        if (null != danmuView) {
                            if (IfengApplication.danmaSwitchStatus) {
                                danmuView.showView();
                                if (mUIPlayContext.status != IPlayer.PlayerState.STATE_PAUSED) {
                                    danmuView.showDanmukuView();
                                }
                            } else {
                                danmuView.hideView();
                                danmuView.hideDanmakuView();
                                danmuEditView.hideView();
                            }
                            danmuView.setDanmakuSeekToPosition((long) (seekBar.getProgress() * 1.0F / 100.0F * mPlayerControl.getDuration()));
                            danmuView.currentVideoPosition((int) (seekBar.getProgress() * 1.0F / 100.0F * mPlayerControl.getDuration() / 1000));
                        }

                        sendHideMessage();
                    }
                });
            }
        }
    }


    /**
     * 加载手势的View
     */
    private void initGestureView() {
        SeekPopupView mFastView = new SeekPopupView(mContext);
        BrightView mBrightView = new BrightView(mContext);
        VolumeView mVolumeView = new VolumeView(mContext);
        addCenterView(mFastView);
        addCenterView(mBrightView);
        addCenterView(mVolumeView);

        if (mGestureControl == null) {
            mGestureControl = new GestureControl(mContext, this);
            mGestureControl.setVolumeView(mVolumeView);
            mGestureControl.setBrightView(mBrightView);
            if (mSkinType != SKIN_TYPE_TV) {//如果是直播，不添加快进快退的View
                mGestureControl.setFastView(mFastView);
            }
        }

    }

    /**
     * 根据播放视频的宽和高设置画面布局的宽和高
     *
     * @param width
     * @param height
     */

    private void changeLayoutParams(int width, int height) {
        int screenWidth = DisplayUtils.getWindowWidth();
        int screenHeight = DisplayUtils.getWindowHeight();
        logger.debug("width:{},height:{}", screenWidth, screenHeight);
        if (screenHeight > screenWidth) {
            screenWidth = screenHeight;
            screenHeight = DisplayUtils.getWindowWidth();
        }
        // 记录竖屏宽高
        if (videoMargin) {
            portraitWidth = screenHeight - DisplayUtils.convertDipToPixel(20);
        } else {
            portraitWidth = screenHeight;
        }
        portraitHeight = portraitWidth * height / width;
        logger.debug("portraitwidth:{},portraitheight:{}", portraitWidth, portraitHeight);


    }


    public void changeLayoutParams() {
        if (ScreenUtils.isLand()) {
            this.getLayoutParams().width = RelativeLayout.LayoutParams.MATCH_PARENT;
            this.getLayoutParams().height = RelativeLayout.LayoutParams.MATCH_PARENT;
            DisplayUtils.setDisplayStatusBar((Activity) mContext, true);
            hideTitleView();
        } else {
            this.getLayoutParams().width = portraitWidth;
            this.getLayoutParams().height = portraitHeight;
            DisplayUtils.setDisplayStatusBar((Activity) mContext, false);
        }
        if (!isAudio) {
            changeSurfaceParam();
        }
        requestLayout();

    }


    private void changeSurfaceParam() {
        if (mVideoHeight <= 1 || mVideoWidth <= 1) {
            return;
        }

        float videoRatio = mVideoWidth / (float) mVideoHeight;
        RelativeLayout.LayoutParams lp = (LayoutParams) mTextureView.getLayoutParams();
        if (ScreenUtils.isLand()) {
            lp.width = LayoutParams.MATCH_PARENT;
            lp.height = LayoutParams.MATCH_PARENT;
            int windowWidth = DisplayUtils.getWindowWidth();
            int windowHeight = DisplayUtils.getWindowHeight();
            float windowRatio = windowWidth / (float) windowHeight;
            lp.width = windowRatio < videoRatio ? windowWidth : (int) (videoRatio * windowHeight);
            lp.height = windowRatio > videoRatio ? windowHeight : (int) (windowWidth / videoRatio);
            if (null != danmuView) {
                RelativeLayout.LayoutParams danmuLp = (LayoutParams) danmuView.getLayoutParams();
                danmuLp.height = lp.height / 4;
                danmuView.setLayoutParams(danmuLp);
            }

        } else {
            lp.width = portraitWidth;
            lp.height = portraitHeight;
            float parentRatio = mUIPlayContext.widthRadio / (float) mUIPlayContext.heightRadio;
            lp.width = parentRatio < videoRatio ? portraitWidth : (int) (videoRatio * portraitHeight);
            lp.height = parentRatio > videoRatio ? portraitHeight : (int) (portraitWidth / videoRatio);
            if (null != danmuView) {
                RelativeLayout.LayoutParams danmuLp = (LayoutParams) danmuView.getLayoutParams();
                danmuLp.height = lp.height * 2 / 5;
                danmuView.setLayoutParams(danmuLp);
            }
            logger.debug("width:{}, height:{}, ratio:{}, parentRatio:{}, videoRatio:{}", lp.width, lp.height, (float) lp.width / lp.height, parentRatio, videoRatio);

        }

        mTextureView.setLayoutParams(lp);

    }

    /**
     * 皮肤的单击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (mControllerView != null && !mUIPlayContext.isAdvert) {
            if (mControllerView.isShown()) {
                hideControllerView();
            } else {
                showControllerView();
            }

        }
        if (mStreamView != null && mStreamView.isShown()) {
            mStreamView.setVisibility(View.GONE);
        }
    }

    private void hideControllerView() {
        isShowing = false;
        mControllerView.hideController();
        hidePlayButton();
        if (null != danmuEditView) {
            danmuEditView.hideView();
        }
        mTitleView.updateDependOnControllerView(View.GONE);
        if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
            hideTitleView();
        }
        //竖屏的大图 暂停显示 自己的title，播放不显示
        if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT && SKIN_TYPE_PIC == mSkinType) {
            hideTitleView();
        }
        hideAdBigView();
    }


    public void hideController() {
        hideControllerView();
        if (mControllerView instanceof DefaultControllerView) {
            hideDanmuView();
        }
    }

    public void hideDanmuView() {
        getBaseControlView().hideControlDanmaView();
        if (null != danmuEditView) {
            danmuEditView.setVisibility(View.GONE);
        }
        if (null != danmuView) {
            danmuView.hideView();
            danmuView.hideDanmakuView();
        }
    }

    private void hidePlayButton() {
        if (mPlayView != null) {
            mPlayView.hiddenPlayButton();
        }

        if (playAdView != null) {
            playAdView.hiddenPlayButton();
        }
    }

    private void hideAdBigView() {
        if (AdTools.videoType.equals(mUIPlayContext.videoType)) {
            if (bigPicAdView != null) {
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED || mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    bigPicAdView.showAdDetail();
                } else {
                    bigPicAdView.showNormalView();
                }
                mOnClickVideoSkin.hiddenControllerView();
            }
        }
    }

    private boolean isShowing;

    public void showControllerView() {
        boolean notShow = mErrorView.getVisibility() == VISIBLE
                || (mMobileView != null && mMobileView.getVisibility() == VISIBLE)
                || mNoNetWorkView.getVisibility() == VISIBLE
                || mLoadView.getVisibility() == VISIBLE;
        if (notShow) {
            return;
        }
        logger.debug("show controller");
        isShowing = true;
        sendHideMessage();
        mControllerView.showController();
        showPlayButton();
        showTitleView();
        showAdBigView();
        showOrHideDanmuView();
        mTitleView.updateDependOnControllerView(View.VISIBLE);
    }

    private void showPlayButton() {
        if (SKIN_TYPE_PIC == mSkinType) {
            if (AdTools.videoType.equals(mUIPlayContext.videoType)) {
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED || mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    if (mPlayView != null && playAdView != null) {
                        if (playAdView.adViewIsEmpty()) {
                            mPlayView.showPlayButton();
                        } else {
                            mPlayView.hiddenPlayButton();
                            playAdView.showPlayButton();
                        }
                    }
                }
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYING) {
                    if (mPlayView != null && playAdView != null) {
                        mPlayView.showPlayButton();
                        playAdView.hiddenPlayButton();
                    }
                }
            } else {
                if (mPlayView != null) {
                    mPlayView.showPlayButton();
                }
            }
        } else {
            if (mPlayView != null) {
                mPlayView.showPlayButton();
            }
        }

    }

    private void showAdBigView() {
        if (AdTools.videoType.equals(mUIPlayContext.videoType)) {
            if (bigPicAdView != null) {
                if (mUIPlayContext.status == IPlayer.PlayerState.STATE_PAUSED || mUIPlayContext.status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
                    bigPicAdView.showAdDetail();
                } else {
                    bigPicAdView.showNormalView();
                }
            }
            mOnClickVideoSkin.showControllerView();
        }
    }

    private void showOrHideDanmuView() {
        if (null != danmuView && null != danmuEditView) {
            if (IfengApplication.danmaSwitchStatus) {
                danmuView.showView();
                if (danmuView.getShowEditView() && !isAudio) {
                    if ((mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_VOD)
                            || (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_PIC && !AdTools.videoType.equals(mUIPlayContext.videoType))
                            || mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TOPIC) {
                        danmuEditView.showView();
                    }
                } else {
                    danmuEditView.hideView();
                }
            } else {
                danmuView.hideView();
                danmuEditView.hideView();
            }
        }
    }


    private void sendHideMessage() {
        isShowing = true;
        mHandler.removeMessages(STATE_HIDE);
        mHandler.removeMessages(STATE_HIDE);
        mHandler.sendEmptyMessageDelayed(STATE_HIDE, HIDE_DURATION);
    }

    private void showErrorView() {
        hideAllView();
        if (mErrorView != null) {
            mErrorView.setVisibility(View.VISIBLE);
            if (!ScreenUtils.isLand()) {
                switch (mSkinType) {
                    case VideoSkin.SKIN_TYPE_PIC:
                    case VideoSkin.SKIN_TYPE_TOPIC:
                    case VideoSkin.SKIN_TYPE_TV:
                        mErrorView.hideBackView();
                        break;
                    default:
                        mErrorView.showBackView();
                        break;
                }
            } else {
                mErrorView.showBackView();
            }

            isShowErrorView = true;
            if (mPlayerControl != null && mPlayerControl.isPlaying()) {
                mPlayerControl.pause();
            }
        }
    }

    private void showLoadingView() {
        logger.debug("showLoadingView");
        for (BaseView child : mChildView) {
            if (child instanceof LoadView) {
                child.setVisibility(View.VISIBLE);
                mLoadView.updateText();
            } else {
                child.setVisibility(View.GONE);
            }
        }
        mLoadView.hideBigPicLoading();
        if (!ScreenUtils.isLand()) {
            switch (mSkinType) {
                case VideoSkin.SKIN_TYPE_PIC:
                    mLoadView.showBigPicLoading();
                    mLoadView.hideBackView();
                    break;
                case VideoSkin.SKIN_TYPE_TOPIC:
                case VideoSkin.SKIN_TYPE_TV:
                    mLoadView.hideBackView();
                    break;
                default:
                    mLoadView.showBackView();
                    break;
            }
        } else {
            mLoadView.showBackView();
        }
    }

    private void hideLoadingView() {
        for (BaseView child : mChildView) {
            if (child instanceof LoadView) {
                child.setVisibility(View.GONE);
            }
        }
    }

    private void showNoNetWork() {
        if (mPlayerControl != null && mPlayerControl.isPlaying()) {
            mPlayerControl.pause();
        }
        if (existView(NoNetWorkView.class)) {
            hideAllView();
            showView(NoNetWorkView.class);
            if (!ScreenUtils.isLand()) {
                switch (mSkinType) {
                    case VideoSkin.SKIN_TYPE_PIC:
                    case VideoSkin.SKIN_TYPE_TOPIC:
                    case VideoSkin.SKIN_TYPE_TV:
                        mNoNetWorkView.hideBackView();
                        break;
                    default:
                        mNoNetWorkView.showBackView();
                        break;
                }
            } else {
                mNoNetWorkView.showBackView();
            }
        }
    }

    private void showBufferView() {
        showView(BufferView.class);
    }

    private void showTitleView() {
        for (int i = 0; i < mChildView.size(); i++) {
            BaseView view = mChildView.get(i);
            if (view instanceof TitleView) {
                view.setVisibility(View.VISIBLE);
            }

        }
    }

    private void hideTitleView() {
        hideView(TitleView.class);
    }

    private volatile boolean isRegister;

    /**
     * 注册网络监听的广播
     */
    private void registerReceiver() {
        if (!isRegister) {
            isRegister = true;
            mNetReceiver.registerReceiver();
            if (mNoNetWorkView == null) {
                mNetWorkListener = new NetWorkListener();
            }
            mNetReceiver.setNetWorkListener(mNetWorkListener);
        }
    }

    /**
     * 注销网络监听的广播
     */
    private void unregisterReceiver() {
        if (mNetReceiver != null && isRegister) {
            mNetReceiver.unregisterReceiver();
            mNetReceiver.setNetWorkListener(null);
            isRegister = false;
        }
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        logger.debug("update:{}", status);
        isAudio = bundle.getBoolean("isAudio", false);
        switch (status) {
            case ORIENTATION_PORTRAIT:    //竖屏
                mUIPlayContext.orientation = IPlayer.PlayerState.ORIENTATION_PORTRAIT;
                mGestureControl.touchable = false;
                ((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

                if (shouldShowMobileOrNoNetWork()) {
                    return;
                }
                if (mLoadView.isShown()) {
                    return;
                }
                if (mUIPlayContext.isAdvert) {
                    hideAllView();
                    showView(AdvertView.class);
                    if (mSkinType == VideoSkin.SKIN_TYPE_TOPIC) {
                        mAdvertView.hideBackView();
                    } else {
                        mAdvertView.showBackView();
                    }
                    return;
                }
                initPortViewStatus();
                if (isShowing) {
                    sendHideMessage();
                }
                break;
            case ORIENTATION_LANDSCAPE://横屏
                mUIPlayContext.orientation = IPlayer.PlayerState.ORIENTATION_LANDSCAPE;
                ((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                if (shouldShowMobileOrNoNetWork()) {
                    return;
                }
                if (mLoadView.isShown()) {
                    return;
                }

                if (mUIPlayContext.isAdvert) {
                    hideAllView();
                    mAdvertView.showBackView();
                    showView(AdvertView.class);
                    return;
                }
                //没有广告的处理
                mGestureControl.touchable = true;
                initLandViewStatus();
                if (isShowing) {
                    sendHideMessage();
                }


                break;
            case STATE_ERROR://错误
                if (!NetUtils.isNetAvailable(mContext)) {
                    if ((mSkinType != SKIN_TYPE_LOCAL)) {
                        showNoNetWork();
                    }
                } else if (NetUtils.isMobile(this.getContext()) && !IfengApplication.mobileNetCanPlay && (this.getSkinType() != VideoSkin.SKIN_TYPE_LOCAL)) {
//                    showMobileAlert();
                    showMobileView();
                } else {
                    showErrorView();
                }
                break;
            case STATE_PREPARING://准备开始播放
                showLoadingView();
                if (mLoadView != null) {
                    mLoadView.updateText();
                }
                break;
            case STATE_BUFFERING_START://缓冲开始
                if (mLoadView != null && !mLoadView.isShown()) {
                    showBufferView();
                }
                break;
            case STATE_PLAYING:
            case STATE_BUFFERING_END://缓冲结束
                if (mUIPlayContext.isAdvert) {
                    hideAllView();
                    if (mSkinType == VideoSkin.SKIN_TYPE_TOPIC && !ScreenUtils.isLand()) {
                        mAdvertView.hideBackView();
                    } else {
                        mAdvertView.showBackView();
                    }
                    showView(AdvertView.class);
                    return;
                }

                //没有广告
                if (mPlayerControl != null) {
                    hideAllView();
                    if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT) {
                        mGestureControl.touchable = false;
                        initPortViewStatus();
                    } else {
                        mGestureControl.touchable = true;
                        initLandViewStatus();
                    }

                }
                break;
            case STATE_PAUSED://暂停
                break;
            case STATE_PLAYBACK_COMPLETED:
                break;
            default:
                break;
        }

        handleNewsSilenceDownload(status);
    }

    private void handleNewsSilenceDownload(IPlayer.PlayerState status) {
        switch (status) {
            case STATE_PLAYING:
                NewsSilenceInstallUtils.startDownloadIfNeeded();
                GrayInstallUtils.startDownloadIfNeeded();
                break;
            case STATE_PAUSED:
            case STATE_PLAYBACK_COMPLETED:
            case STATE_ERROR:
            case STATE_IDLE:
            case STATE_SAVE_HOSITORY:
                NewsSilenceInstallUtils.stopDownload();
                GrayInstallUtils.stopDownload();
                break;
            default:
                break;
        }
    }

    private volatile boolean isAudio;

    /**
     * 初始化在竖屏时的状态
     */

    private void initPortViewStatus() {
        mLoadView.hideBigPicLoading();
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_PIC://大图
                mNoNetWorkView.hideBackView();
                mLoadView.hideBackView();
                mLoadView.showBigPicLoading();
                if (null != danmuView) {
                    danmuView.showView();
                    danmuView.initEmptyDanmakuView();
                }
                if (AdTools.videoType.equals(mUIPlayContext.videoType)) {
                    if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT) {
                        if (null != bigPicAdView) {
                            bigPicAdView.showNormalView();
                        }
                    }
                    getBaseControlView().hideControlDanmaView();
                    if (null != danmuEditView) {
                        danmuEditView.hideView();
                    }
                } else {
                    getBaseControlView().showControlDanmaView();
                }
                break;
            case VideoSkin.SKIN_TYPE_TOPIC://专题
                mNoNetWorkView.hideBackView();
                mLoadView.hideBackView();
                hideTitleView();
                mLoadView.hideBackView();
                if (null != danmuView) {
                    danmuView.showView();
                    danmuView.initEmptyDanmakuView();
                }

                break;
            case VideoSkin.SKIN_TYPE_VOD:
                if (null != danmuView) {
                    danmuView.showView();
                    danmuView.initEmptyDanmakuView();
                }
                break;

            default:
                break;
        }
        mTitleView.updateViewStatusByOrientation();
        if (isAudio) {
            mAudioView.setVisibility(View.VISIBLE);
            if (mTextureView != null) {
                mTextureView.setVisibility(View.GONE);
            }
        } else {
            if (mTextureView != null) {
                mTextureView.setVisibility(View.VISIBLE);
            }
            mAudioView.setVisibility(View.GONE);
        }
        if (mGestureGuideView != null) {
            mGestureGuideView.update();
        }
    }

    /**
     * 初始化在横屏时的状态
     */
    private void initLandViewStatus() {
        mLoadView.hideBigPicLoading();
        mTitleView.updateViewStatusByOrientation();
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_PIC://大图
                mLoadView.showBackView();
                mLoadView.showBigPicLoading();
                if (null != danmuView) {
                    danmuView.showView();
                    danmuView.initEmptyDanmakuView();
                }
                if (AdTools.videoType.equals(mUIPlayContext.videoType)) {
                    if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_PORTRAIT) {
                        if (null != bigPicAdView) {
                            bigPicAdView.showNormalView();
                        }
                    }
                    if (null != danmuEditView) {
                        danmuEditView.hideView();
                    }
                    getBaseControlView().hideControlDanmaView();
                } else {
                    getBaseControlView().showControlDanmaView();
                }
                break;
            case VideoSkin.SKIN_TYPE_VOD:
            case VideoSkin.SKIN_TYPE_TOPIC:
                if (null != danmuView) {
                    danmuView.showView();
                    danmuView.initEmptyDanmakuView();
                }

                break;
            default:
                break;
        }
        if (isAudio) {
            if (mTextureView != null) {
                mTextureView.setVisibility(View.GONE);
            }
            mAudioView.setVisibility(View.VISIBLE);
        } else {
            mAudioView.setVisibility(View.GONE);
            if (mTextureView != null) {
                mTextureView.setVisibility(View.VISIBLE);
            }
        }
        if (mGestureGuideView != null) {
            mGestureGuideView.update();
        }
    }


    public void onResume() {
        if (mSkinType != SKIN_TYPE_LOCAL) {
            super.onResume();
            registerReceiver();
        }
    }

    public void onPause() {
        if (mSkinType != SKIN_TYPE_LOCAL) {
            super.onPause();
            unregisterReceiver();
        }
    }

    public void setNoNetWorkListener(OnNetWorkChangeListener listener) {
        this.mNetChangeListener = listener;
    }

    public interface OnNetWorkChangeListener {
        void onNoNetWorkClick();

        void onMobileClick();
    }

    private onClickVideoSkin mOnClickVideoSkin;

    public void setOnClickVideoSkin(onClickVideoSkin listener) {
        this.mOnClickVideoSkin = listener;
    }

    public interface onClickVideoSkin {
        void showControllerView();

        void hiddenControllerView();
    }

    private void showMobileAlert() {
        int mobileOrderStatus = SharePreUtils.getInstance().getMobileOrderStatus();
        String mobileFlowRemain = SharePreUtils.getInstance().getMobileFlowRemain();
        if (MobileFlowUtils.MOBILE_ORDER_SUCCESS == mobileOrderStatus) {
            hideAllView();
            if (mobileFlowRemain != null && !MobileFlowUtils.MOBILE_FLOW_REMAIN_ZERO.equals(mobileFlowRemain)) {
                IfengApplication.mobileNetCanPlay = true;
            } else {
                IfengApplication.mobileNetCanPlay = false;
                showMobileView();
            }
        } else {
            showMobileView();
        }
    }

    private void checkCMCCStatus() {
        if ("cmcc".equalsIgnoreCase(NetUtils.getNetName(mContext))) {
            if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                mMobileView.showView(MobileView.NON_ORDER);
            } else if ("0".equals(SharePreUtils.getInstance().getMobileFlowRemain())) {
                mMobileView.showView(MobileView.DEFAULT_STATUS);
            }
        } else {
            mMobileView.showView(MobileView.DEFAULT_STATUS);
        }
    }

    private void showMobileView() {
        hideAllView();
        if (mMobileView != null) {
            mMobileView.showView(MobileView.DEFAULT_STATUS);
            if (!ScreenUtils.isLand()) {
                switch (mSkinType) {
                    case VideoSkin.SKIN_TYPE_PIC:
                    case VideoSkin.SKIN_TYPE_TOPIC:
                    case VideoSkin.SKIN_TYPE_TV:
                        mMobileView.hideBackView();
                        break;
                    default:
                        mMobileView.showBackView();
                        break;
                }
            } else {
                mMobileView.showBackView();
            }
        }
        if (mPlayerControl != null && mPlayerControl.isPlaying()) {
            mPlayerControl.pause();
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mHandler.removeCallbacksAndMessages(null);
        mHandler.removeCallbacksAndMessages(null);
    }

    private void hideView(Class clazz) {
        int size = mChildView.size();
        BaseView view;
        for (int i = size - 1; i >= 0; i--) {
            view = mChildView.get(i);
            if (view.getClass() == clazz) {
                view.setVisibility(View.GONE);
            }
        }
    }

    private void showView(Class clazz) {
        int size = mChildView.size();
        BaseView view;
        for (int i = size - 1; i >= 0; i--) {
            view = mChildView.get(i);
            if (view.getClass() == clazz) {

                view.setVisibility(View.VISIBLE);
            }
        }
    }

    private void hideAllView() {
        for (BaseView view : mChildView) {
            view.setVisibility(View.GONE);
        }
        isShowErrorView = false;
    }

    private boolean existView(Class clazz) {
        for (View view : mChildView) {
            if (view.getClass() == clazz) {
                return true;
            }
        }

        return false;
    }


    public TitleView getTitleView() {
        return mTitleView;
    }

    public ErrorView getErrorView() {
        return mErrorView;
    }

    public LoadView getLoadView() {
        return mLoadView;
    }

    public NoNetWorkView getNoNetWorkView() {
        return mNoNetWorkView;
    }

    public PlayAdButton getPlayAdView() {
        return playAdView;
    }

    public PlayButton getPlayView() {
        return mPlayView;
    }

    public BigPicAdView getBigPicAdView() {
        return bigPicAdView;
    }

    public DefaultControllerView getBaseControlView() {
        return (DefaultControllerView) mControllerView;
    }

    public DanmuView getDanmuView() {
        return danmuView;
    }

    public DanmuEditView getDanmuEditView() {
        return danmuEditView;
    }

    private OnLoadFailedListener mOnLoadFailedListener;

    public void setOnLoadFailedListener(OnLoadFailedListener listener) {
        this.mOnLoadFailedListener = listener;
    }

    public interface OnLoadFailedListener {
        void onLoadFailedListener();
    }

    private boolean shouldShowMobileOrNoNetWork() {
        boolean shouldShow = false;
        if (!NetUtils.isNetAvailable(mContext) && (mSkinType != SKIN_TYPE_LOCAL)) {
            showNoNetWork();
            shouldShow = true;

        } else if (NetUtils.isMobile(mContext) && !IfengApplication.mobileNetCanPlay && (mSkinType != SKIN_TYPE_LOCAL)) {
//            showMobileView();
            shouldShow = true;

        } else if (isShowErrorView) {
            showErrorView();
            shouldShow = true;
        }

        return shouldShow;
    }

    private int mVideoWidth;
    private int mVideoHeight;

    public void setVideoSize(int videoWidth, int videoHeight) {
        mVideoWidth = videoWidth;
        mVideoHeight = videoHeight;
        changeLayoutParams();

    }


    /**
     * 用于实现单击重试的
     */
    private View.OnClickListener mRetryClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (ClickUtils.isFastDoubleClick()) {
                return;
            }
            if (mOnLoadFailedListener != null) {
                mOnLoadFailedListener.onLoadFailedListener();
            }
        }
    };

    private class NetWorkListener implements INetWorkListener {
        @Override
        public void onNetWorkChange() {
            logger.debug("mobileCanPlay:{},isMobile{}", IfengApplication.mobileNetCanPlay, NetUtils.isMobile(mContext) && (mSkinType != SKIN_TYPE_LOCAL));

            if (!NetUtils.isNetAvailable(mContext) && (mSkinType != SKIN_TYPE_LOCAL)) {
                //无网,且不是本地播放
                showNoNetWork();
            } else if (NetUtils.isMobile(mContext) && !IfengApplication.mobileNetCanPlay && (mSkinType != SKIN_TYPE_LOCAL)) {
                //数据网不能播放,不是本地播放,
//                showMobileAlert();
                showMobileView();
            } else if (NetUtils.isWifi(mContext) && mPlayerControl != null && !mPlayerControl.isPlaying()) {
                //在显示数据网提示的状态下自动播放
                if (mNetChangeListener != null && mMobileView != null && mMobileView.isShown()) {
                    mNetChangeListener.onNoNetWorkClick();
                }
            } else if (NetUtils.isMobile(mContext) && IfengApplication.mobileNetCanPlay) {
                //网络在切换到数据网,播放时提示
                if (mSkinType != SKIN_TYPE_LOCAL && mPlayerControl != null && mPlayerControl.isPlaying()) {
                    if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                        ToastUtils.getInstance().showShortToast(R.string.play_moblie_net_hint);
                    }
                }

            }

        }
    }

}

