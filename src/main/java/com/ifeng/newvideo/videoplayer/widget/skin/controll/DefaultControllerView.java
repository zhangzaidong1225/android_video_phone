package com.ifeng.newvideo.videoplayer.widget.skin.controll;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.widget.skin.SeekBarView;
import com.ifeng.video.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fanshell on 2016/8/8.
 */
public class DefaultControllerView extends BaseControllerView implements UIObserver {

    private TextView mTotalTimeTextView;
    private TextView mCurrentTimeTextView;
    private ImageView controlDanmuView;
    private View mFullScreenView;
    private SeekBarView mSeekBar;
    private TextView mStreamText;
    private static final int MSG_UPDATE_PROGRESS = 105;
    private boolean isShow = true;
    private Logger logger = LoggerFactory.getLogger(DefaultControllerView.class);

    private Handler mUpdatePressHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PROGRESS:
                    updateTimeView();
                    this.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, 1000);
                    break;
            }

        }
    };

    public DefaultControllerView(Context context) {
        super(context);
    }

    public DefaultControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefaultControllerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void appendChildView() {
        mTotalTimeTextView = (TextView) findViewById(R.id.control_total_time);
        mCurrentTimeTextView = (TextView) findViewById(R.id.control_current_time);
        mFullScreenView = findViewById(R.id.control_full);
        mStreamText = (TextView) findViewById(R.id.control_current_stream);
        mSeekBar = (SeekBarView) findViewById(R.id.control_seekBar);
        controlDanmuView = (ImageView) findViewById(R.id.control_show_danmu);
        controlDanmuView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShow) {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DANMA_CLOSED, null, getCurPage());
                    if (ScreenUtils.isLand()) {
                        controlDanmuView.setImageDrawable(getResources().getDrawable(R.drawable.video_danma_closed_land));
                    } else {
                        controlDanmuView.setImageDrawable(getResources().getDrawable(R.drawable.video_danma_closed));
                    }

                } else {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DANMA_OPEN, null, getCurPage());
                    if (ScreenUtils.isLand()) {
                        controlDanmuView.setImageDrawable(getResources().getDrawable(R.drawable.video_danma_normal_land));
                    } else {
                        controlDanmuView.setImageDrawable(getResources().getDrawable(R.drawable.video_danma_normal));
                    }
                }
                if (mShowListener != null) {
                    mShowListener.showDanmuView(!isShow);
                }
                isShow = !isShow;

            }
        });
        mFullScreenView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configuration configuration = mContext.getResources().getConfiguration();
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_FULL_SCREEN, null, getCurPage());
                if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                } else {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
                }
            }
        });
        mStreamText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mStreamView != null) {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DEF, null, getCurPage());
                    mStreamView.showView();
                }
            }
        });
        mChildView.add(mSeekBar);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }

    private OnShowOrHideDanmuView mShowListener;

    public void setShowListener(OnShowOrHideDanmuView listener) {
        this.mShowListener = listener;
    }

    public interface OnShowOrHideDanmuView {
        void showDanmuView(boolean isShow);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mPlayerControl != null) {
            this.mPlayerControl.removeUIObserver(this);
        }
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.default_video_controller;
    }

    @Override
    public void showController() {
        updateTimeView();
        setFullViewStatus();
        setStreamViewStatus(ScreenUtils.isLand(), mIsAudio);

        this.setVisibility(View.VISIBLE);
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
        mUpdatePressHandler.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, 200);
    }

    private void setFullViewStatus() {
        if (ScreenUtils.isLand()) {
            if (!AdTools.videoType.equals(mUIPlayContext.videoType)) {
                this.mFullScreenView.setVisibility(View.GONE);
            } else {
                this.mFullScreenView.setVisibility(View.VISIBLE);
            }
            if (isShow) {
                controlDanmuView.setImageResource(R.drawable.video_danma_normal_land);
            } else {
                controlDanmuView.setImageResource(R.drawable.video_danma_closed_land);
            }
        } else {
            this.mFullScreenView.setVisibility(View.VISIBLE);
            if (isShow) {
                controlDanmuView.setImageResource(R.drawable.video_danma_normal);
            } else {
                controlDanmuView.setImageResource(R.drawable.video_danma_closed);
            }
        }
    }

    private void setStreamViewStatus(boolean isLandScape, boolean isAudio) {
        if (isLandScape) {
            if (!AdTools.videoType.equals(mUIPlayContext.videoType)) {
                mStreamText.setVisibility(isAudio ? GONE : VISIBLE);
                mStreamText.setText(mUIPlayContext.streamType);
                mStreamText.setTextColor(Color.parseColor("#ffffff"));
                mStreamText.setEnabled(true);
            } else {
                mStreamText.setVisibility(View.GONE);
            }
        } else {
            mStreamText.setVisibility(GONE);
            mStreamText.setEnabled(false);
            mStreamText.setTextColor(Color.parseColor("#aaaaaa"));
            mStreamText.setText(getResources().getString(R.string.common_video_high));
        }
    }

    @Override
    public void hideController() {
        this.setVisibility(View.GONE);
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);

    }

    @Override
    public SeekBarView getSeekBar() {
        return mSeekBar;
    }

    public ImageView getControlDanmuView() {
        return controlDanmuView;
    }

    public void setShowControlView(boolean isShowView) {
        if (isShowView) {
            controlDanmuView.setImageDrawable(getResources().getDrawable(R.drawable.video_danma_normal));
        } else {
            controlDanmuView.setImageDrawable(getResources().getDrawable(R.drawable.video_danma_closed));
        }
        this.isShow = isShowView;
    }


    public void hideControlDanmaView() {
        controlDanmuView.setVisibility(View.GONE);
    }

    public void showControlDanmaView() {
        controlDanmuView.setVisibility(View.VISIBLE);
    }


    private void updateTimeView() {
        // logger.debug("duration:{}");
        if (mPlayerControl != null) {
            // logger.debug("duration:{}", mPlayerControl.getDuration());
            // logger.debug("CurrentPosition:{}", mPlayerControl.getCurrentPosition());
            final long duration = mPlayerControl.getDuration();
            final long current_position = mPlayerControl.getCurrentPosition();
            if (duration >= 0) {
                mTotalTimeTextView.setText(StringUtils.formatDuration(duration));
            }
            if (current_position >= 0) {
                mCurrentTimeTextView.setText(StringUtils.formatDuration(current_position));
            }
        }
        mSeekBar.updateSeekBarProgress();
    }

    private boolean mIsAudio = false;

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        mIsAudio = bundle.getBoolean("isAudio", false);
        if (mIsAudio || AdTools.videoType.equals(mUIPlayContext.videoType)) {
            hideControlDanmaView();
        } else {
            showControlDanmaView();
        }

        switch (status) {
            case STREAM_CHANGE:
                mUIPlayContext.streamType = bundle.getString(StreamUtils.KEY_STREAM);
                this.mStreamText.setText(mUIPlayContext.streamType);
                break;
            case ORIENTATION_LANDSCAPE:
                mUIPlayContext.orientation = IPlayer.PlayerState.ORIENTATION_LANDSCAPE;
                setStreamViewStatus(true, mIsAudio);
                setFullViewStatus();
                break;
            case ORIENTATION_PORTRAIT:
                mUIPlayContext.orientation = IPlayer.PlayerState.ORIENTATION_PORTRAIT;
                setStreamViewStatus(false, mIsAudio);
                setFullViewStatus();
                break;

        }

    }


}
