package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 快进view
 * Created by fanshell on 2016/8/9.
 */
public class SeekPopupView extends BaseView {
    private Logger logger = LoggerFactory.getLogger(SeekPopupView.class);
    private int maxWidth;
    private View mPercentView;
    private TextView mFastTimeView;
    private TextView mTotalTimeView;
    private ImageView mFastIconView;
    private long position = -1;

    public SeekPopupView(Context context) {
        super(context);
    }

    public SeekPopupView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SeekPopupView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_fast, this, true);
       this.setVisibility(View.GONE);

        mPercentView = findViewById(R.id.fast_percent);
        mFastTimeView = (TextView) findViewById(R.id.gesture_fast_time);
        mTotalTimeView = (TextView) findViewById(R.id.gesture_total_time);
        mFastIconView = (ImageView) findViewById(R.id.gesture_fast_icon);
    }

    @Override
    protected void initPlayer() {

    }

    private int mCurrentPercent;

    public void setPercent(int percent, boolean isFast) {
        if (position == -1) {
            position = mPlayerControl.getCurrentPosition();
        }
        if (maxWidth == 0) {
            maxWidth = findViewById(R.id.fast_full).getWidth();
        }

        ViewGroup.LayoutParams lp = mPercentView.getLayoutParams();

        if (mPlayerControl.getDuration() == 0) {
            return;
        }
        int currentPercent = (int) (position * 100 / mPlayerControl.getDuration());
        percent = currentPercent + percent;
        if (percent <= 0) {
            percent = 0;
        }
        if (percent >= 100) {
            percent = 100;
        }

        mCurrentPercent = percent;
        lp.width = maxWidth * (percent) / 100;
        mPercentView.setLayoutParams(lp);
        updateTimeView(percent);
        if (isFast) {
            mFastIconView.setImageResource(R.drawable.common_gesture_fast_forward);
        } else {
            mFastIconView.setImageResource(R.drawable.common_gesture_fast_back);
        }
        this.setVisibility(View.VISIBLE);
        logger.debug("setPercent");

    }


    private void updateTimeView(int percent) {
        if (mPlayerControl != null) {
            mTotalTimeView.setText(StringUtils.formatDuration(mPlayerControl.getDuration()));
            long time = mPlayerControl.getDuration() * percent / 100;
            mFastTimeView.setText(StringUtils.formatDuration(time));
        }
    }

    public void seekTo() {
        if (mPlayerControl != null) {
            mPlayerControl.seekTo(mCurrentPercent * mPlayerControl.getDuration() / 100);
        }
        position = -1;

    }
}
