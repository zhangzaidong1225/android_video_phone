package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.SeekBar;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController;

import static com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController.PIP_VIDEO_PLAYER_TYPE_IFENG;
import static com.ifeng.newvideo.videoplayer.player.playController.FloatVideoPlayController.PIP_VIDEO_PLAYER_TYPE_IJK;

/**
 * Created by ll on 2017/10/16.
 */
public class PipSeekBar extends BaseView implements UIObserver{
    private SeekBar mSeekBarView;
    private static int IJK_TYPE_PLAYER_MSG = 10001;
    public PipSeekBar(Context context) {
        super(context);
    }

    public PipSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PipSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        inflate(mContext, R.layout.pip_video_player_seekbar, this);
        mSeekBarView = (SeekBar) this.getChildAt(0);
        mSeekBarView.setMax(100);
        mSeekBarView.setProgress(0);
        mSeekBarView.setEnabled(false);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mPlayerControl != null) {
            mPlayerControl.removeUIObserver(this);
        }
    }

    public void updateSeekBarProgress() {
        if (mPlayerControl != null) {
            long currentTime = mPlayerControl.getCurrentPosition();
            int pos = 0;
            long duration = 0;
            // 获取视频总长
            duration = mPlayerControl.getDuration();
            // 计算比例
            if (duration != 0) {
                pos = (int) ((currentTime * 100) / duration);
            }
            if (pos >= mSeekBarView.getProgress()) {
                mSeekBarView.setProgress(pos);
            } else if (pos == 0) {
                mSeekBarView.setProgress(pos);
            }
        }
    }
    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        switch (status) {
            case PIP_STATE_PLAYING:
            case STATE_PLAYING:
                if (mPlayerControl instanceof FloatVideoPlayController) {
                    if ( mPlayerControl.getPlayerType() == PIP_VIDEO_PLAYER_TYPE_IJK) {
                        updateProgressHandler.sendEmptyMessage(IJK_TYPE_PLAYER_MSG);
                    } else if ( mPlayerControl.getPlayerType() == PIP_VIDEO_PLAYER_TYPE_IFENG) {
                        updateSeekBarProgress();
                    }

                }
                break;
            case STATE_PLAYBACK_COMPLETED:
                if (mPlayerControl.getPlayerType() == PIP_VIDEO_PLAYER_TYPE_IJK) {
                    updateProgressHandler.removeCallbacksAndMessages(null);
                    mSeekBarView.setProgress(0);
                }
                break;

        }
    }



    private Handler updateProgressHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == IJK_TYPE_PLAYER_MSG) {
                updateSeekBarProgress();
                sendEmptyMessageDelayed(IJK_TYPE_PLAYER_MSG, 1000);
            }
        }
    };
}
