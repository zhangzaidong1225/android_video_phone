package com.ifeng.newvideo.videoplayer.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.member.OpenMemberActivity;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.player.VideoAdvertManager;
import com.ifeng.newvideo.videoplayer.widget.skin.BaseView;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.VideoAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 播放底页 播放器上前贴广告view
 * Created by fanshell on 2016/7/26.
 */
public class AdvertView extends BaseView implements UIObserver, View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(AdvertView.class);


    private View mBackView;
    private ImageView mVoiceView;
    private View mVoiceViewLayout;
    private View mDetailView;
    private View mFullScreen;
    private TextView mCountDownText;
    private View mCountDownLayout;
    private RelativeLayout mSkipADLayout;
    private AudioManager mAudioManager;
    private int mCurVolume;
    public static int mAdTotalLength = 10;
    private static boolean mIsAdPause = false;
    private static boolean mIsMute = false;//是否是静音
    private static boolean mIsADMute = false;//是否是广告静音
    // 片头没有倒计时、了解详情、不可点击跳转到广告页
    boolean mIsSponsorHead;

    public AdvertView(Context context) {
        super(context);
    }

    public AdvertView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AdvertView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void initView() {
        this.setBackgroundColor(Color.TRANSPARENT);
        LayoutInflater.from(mContext).inflate(R.layout.default_video_ad, this, true);
        mBackView = findViewById(R.id.video_ad_back);
        mVoiceView = (ImageView) findViewById(R.id.video_ad_mute);
        mVoiceViewLayout = findViewById(R.id.video_ad_mute_rl);
        mDetailView = findViewById(R.id.video_ad_detail);
        mFullScreen = findViewById(R.id.video_ad_fullscreen);
        mCountDownText = (TextView) findViewById(R.id.ad_count_down);
        mCountDownLayout = findViewById(R.id.ad_countdown_layout);
//        mSkipADLayout = (RelativeLayout)findViewById(R.id.rl_skip_ad);

        this.setOnClickListener(this);
        mBackView.setOnClickListener(this);
        mCountDownLayout.setOnClickListener(this);
//        mSkipADLayout.setOnClickListener(this);

        mVoiceViewLayout.setOnClickListener(this);
        mDetailView.setOnClickListener(this);
        mFullScreen.setOnClickListener(this);
        this.setVisibility(View.GONE);

        mAudioManager = (AudioManager) mContext.getSystemService(mContext.AUDIO_SERVICE);
        updateVolumeStatus();
    }

    private Runnable mCountDownRunnable = new Runnable() {
        @Override
        public void run() {
            if (getHandler() == null || mIsAdPause) {
                return;
            }
            mAdTotalLength--;
            setCountDown();

            // 手动调音量后，变更UI
            updateVolumeStatus();
        }
    };

    private void setCountDown() {
        if (mAdTotalLength > 0) {
            mCountDownText.setText(mAdTotalLength + (mUIPlayContext.canSkipAd() ? "s 跳过" : "s 会员免广告"));
        } else {
            mCountDownLayout.setVisibility(GONE);
        }

        mCountDownLayout.setVisibility(mIsSponsorHead ? GONE : VISIBLE);
        mCountDownText.setBackgroundResource(mUIPlayContext.canSkipAd() ?
                R.drawable.ad_player_skip_bg_shape_long : R.drawable.ad_player_skip_bg_shape_short);

        getHandler().postDelayed(mCountDownRunnable, 1000);
    }

    @Override
    public void onClick(View v) {
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.video_ad_back:
                clickBack();
                break;
            case R.id.ad_countdown_layout:
                clickCountDown();
                break;
            case R.id.video_ad_mute_rl:
                clickMute();
                break;
            case R.id.video_ad_detail:
                clickDetail();
                break;
            case R.id.video_ad_fullscreen:
                clickFullScreen();
                break;
//            case R.id.rl_skip_ad:
//                Intent intent = new Intent();
//                intent.setClass(mContext, OpenMemberActivity.class);
//                mContext.startActivity(intent);
//                break;
            default:
                clickView();
                break;
        }
    }

    private void clickCountDown() {
            if (mUIPlayContext.canSkipAd()) {
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_AD_SKIP, null, getCurPage());
                mPlayerControl.skipAdvert();
            } else {
            Intent intent = new Intent();
            intent.setClass(mContext, OpenMemberActivity.class);
            mContext.startActivity(intent);
        }
    }

    private void clickView() {
        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_AD, null, getCurPage());
        clickToAdActivity(mUIPlayContext.videoAdModel);
    }

    private void clickFullScreen() {
        Configuration configuration = mContext.getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
            PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_FULL_SCREEN, false, getCurPage());
        } else {
            mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
            PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_FULL_SCREEN, true, getCurPage());
        }
    }

    private void clickDetail() {
        logger.debug("video_ad_image_layout");
        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_AD_DETAIL, null, getCurPage());
        clickToAdActivity(mUIPlayContext.videoAdModel);
    }

    private void clickMute() {
        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_MUTE, !mIsMute, getCurPage());
        if (mIsMute) {
            //切成非静音
            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            if (mCurVolume == 0) {
                mCurVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 25 / 100;
            }
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurVolume, 0);
        } else {
            //切成静音
            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            mCurVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        }
        updateVolumeStatus();
    }

    private void clickBack() {
        if (ScreenUtils.isLand()) {
            mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
        } else {
            if (mContext instanceof Activity) {
                ((Activity) mContext).finish();
            }
        }

        PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_BACK, null, getCurPage());
    }

    private void clickToAdActivity(VideoAdInfoModel adInfoModel) {
        if (adInfoModel == null) {
            return;
        }
        String adType = "";
        if (!IfengType.TYPE_BROWSER.equalsIgnoreCase(adInfoModel.getType())) {
            adType = IfengType.TYPE_WEBFULL;
        }
        String clickUrl = adInfoModel.getClick();
        String echid = "";
        String chid = "";

        String adUnitId = adInfoModel.getId();
        ADJumpType.jump(adUnitId, adType, null, null, clickUrl, clickUrl, null, null,
                getContext(), null, echid, chid, null);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        logger.debug("status: {}", status);
        if (mUIPlayContext == null) {
            return;
        }
        if (!mUIPlayContext.isAdvert) {
            mUIPlayContext.adVideoTotalLength = 0;
            return;
        }

        mIsSponsorHead = VideoAdvertManager.isSponsorHead(mUIPlayContext.videoAdModel);
        mCountDownLayout.setVisibility(mIsSponsorHead ? GONE : VISIBLE);
        mDetailView.setVisibility(mIsSponsorHead ? GONE : VISIBLE);
        this.setClickable(!mIsSponsorHead);

        //更新倒计时
        updateCountDown(status);
    }


    private void updateCountDown(IPlayer.PlayerState status) {

        switch (status) {
            case STATE_PREPARING:
                break;
            case STATE_BUFFERING_START:
            case STATE_PAUSED:
                mIsAdPause = true;
                break;
            case STATE_BUFFERING_END:
                if (mIsAdPause && !mIsSponsorHead) {
                    mIsAdPause = false;
                    getHandler().removeCallbacks(mCountDownRunnable);
                    setCountDown();
                }
                break;
            case STATE_PLAYING:
                if (mIsSponsorHead) {
                    mAdTotalLength = 0;
                    return;
                }
                if (mUIPlayContext.adVideoTotalLength > 0) {
                    if (mAdTotalLength == 0) {
                        mAdTotalLength = mUIPlayContext.adVideoTotalLength;
                    }
                    mIsAdPause = false;
                    getHandler().removeCallbacks(mCountDownRunnable);
                    setCountDown();
                }
                break;
            case STATE_PLAYBACK_COMPLETED:
                if (mIsSponsorHead) {
                    resetVolumeStatus();
                }
            case STATE_ERROR:
                if (!mIsSponsorHead) {
                    mCountDownLayout.setVisibility(GONE);
                    getHandler().removeCallbacks(mCountDownRunnable);
                    //出错了重新播。
                    int currentAdDuration = Integer.parseInt(mUIPlayContext.videoAdModel.getLength());
                    mUIPlayContext.adVideoTotalLength = mUIPlayContext.adVideoTotalLength - currentAdDuration;
                    mAdTotalLength = mUIPlayContext.adVideoTotalLength;
                }
                break;
            case STATE_IDLE:
                mCountDownLayout.setVisibility(GONE);
                getHandler().removeCallbacks(mCountDownRunnable);
                break;
            default:
                break;
        }
    }

    private void updateVolumeStatus() {
        mCurVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mIsMute = mCurVolume == 0;

        if (mIsMute) {
            mVoiceView.setImageResource(R.drawable.common_gesture_volume_mute);
        } else {
            mVoiceView.setImageResource(R.drawable.common_gesture_volume_nomal);
        }
    }

    private void resetVolumeStatus() {
        if (mIsMute && mIsADMute) {
            //切换回非静音
            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurVolume, 0);
            mIsMute = false;
            mIsADMute = false;
            updateVolumeStatus();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        resetVolumeStatus();
    }

    public void hideBackView() {
        mBackView.setVisibility(View.GONE);
    }

    public void showBackView() {
        mBackView.setVisibility(View.VISIBLE);
    }

}
