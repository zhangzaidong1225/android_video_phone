package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ifeng.newvideo.R;

/**
 * Created by fanshell on 2016/8/9.
 */
public class BrightView extends BaseView {
    private int maxWidth;
    private View mPercentView;

    public BrightView(Context context) {
        super(context);
    }

    public BrightView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BrightView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_bright, this, true);
        this.setVisibility(View.GONE);
        mPercentView = findViewById(R.id.bright_percent);
    }

    @Override
    protected void initPlayer() {

    }

    public void setPercent(int percent) {
        maxWidth = findViewById(R.id.bright_full).getWidth();
        ViewGroup.LayoutParams lp = mPercentView.getLayoutParams();
        lp.width = maxWidth * percent / 100;
        mPercentView.setLayoutParams(lp);
        this.setVisibility(View.VISIBLE);
    }
}
