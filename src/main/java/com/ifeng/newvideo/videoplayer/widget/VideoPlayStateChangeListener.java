package com.ifeng.newvideo.videoplayer.widget;

import com.ifeng.video.player.PlayState;
import com.ifeng.video.player.StateListener;

/**
 * Created by fanshell on 2016/7/26.
 */
public class VideoPlayStateChangeListener implements StateListener {

    private PlayState mCurrentPlayState;

    @Override
    public PlayState getCurrState() {
        return mCurrentPlayState;
    }

    @Override
    public void onStateChange(PlayState state) {
        mCurrentPlayState = state;
        switch (state) {
            case STATE_IDLE://播放器处于空闲状态
                break;
            case STATE_PREPARING://准备加载
                break;
            case STATE_PREPARED://加载完成
                break;
            case STATE_ERROR://播放器错误
                break;
            case STATE_DISPLAY_FRAME://显示每帧
                break;
            case STATE_PLAYING://开始播放
                break;
            case STATE_PAUSED://暂停播放
                break;
            case STATE_PLAYBACK_COMPLETED://播放完成
                break;
            case STATE_SUSPEND://播放器处于挂起状态
                break;
            case STATE_RESUME://恢复播放
                break;
            case STATE_SUSPEND_UNSUPPORTED://播放器不支持格式
                break;
            case STATE_STOPED://停止播放
                break;
            case STATE_BUFFERING_START://卡顿时开始缓冲
                break;
            case STATE_BUFFERING_END://卡顿时结束缓冲
                break;
            case STATE_PLAY_PRE:
                break;
            case STATE_PLAY_NEXT:
                break;
            case STATE_POSITIONCHANGED://直播改变位置
                break;

        }

    }
}
