package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import com.ifeng.newvideo.R;

/**
 * Created by fanshell on 2016/7/25.
 */
public class BufferView extends BaseView {

    private static final String TAG = "BufferView";

    public BufferView(Context context) {
        super(context);
    }

    public BufferView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BufferView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext)
                .inflate(R.layout.default_video_buffer, this, true);
        this.setBackgroundColor(Color.TRANSPARENT);
        this.setVisibility(View.GONE);
    }

    @Override
    protected void initPlayer() {

    }
}
