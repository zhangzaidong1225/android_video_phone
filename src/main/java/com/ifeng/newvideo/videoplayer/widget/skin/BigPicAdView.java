package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.widget.CustomNetworkImageView;
import com.ifeng.video.core.net.VolleyHelper;

public class BigPicAdView extends BaseView implements UIObserver, View.OnClickListener {

    private RelativeLayout ll_ad_use;
    private LinearLayout ll_ad_know;
    private TextView tv_ad_know, tv_ad_use;
    private CustomNetworkImageView iv_ad;

    public BigPicAdView(Context context) {
        super(context);
    }

    public BigPicAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BigPicAdView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_big_picture_video_ad, this, true);
        ll_ad_use = (RelativeLayout) findViewById(R.id.ll_ad_use);
        iv_ad = (CustomNetworkImageView) findViewById(R.id.iv_ad);
        ll_ad_know = (LinearLayout) findViewById(R.id.ll_ad_know);
        tv_ad_know = (TextView) findViewById(R.id.tv_video_ad_detail);
        tv_ad_use = (TextView) findViewById(R.id.tv_ad_left);

        ll_ad_use.setOnClickListener(this);
        tv_ad_know.setOnClickListener(this);
        this.setOnClickListener(this);
//        this.setVisibility(View.GONE);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ll_ad_use) {
            if (mClickViewListener != null) {
                mClickViewListener.onClickAdToKnow();
            }
            IntentUtils.startADActivity(mContext, "", mUIPlayContext.adItem.getAdStopUrl(), "", ADActivity.FUNCTION_VALUE_AD, mUIPlayContext.adItem.getAdTitle(), null, "",
                    "", "", null, null);
        }
        if (view.getId() == R.id.tv_video_ad_detail) {
            if (mClickViewListener != null) {
                mClickViewListener.onClickAdToKnow();
            }

            IntentUtils.startADActivity(mContext, "", mUIPlayContext.adItem.getAdClickUrl(), "", ADActivity.FUNCTION_VALUE_AD, mUIPlayContext.adItem.getAdTitle(), null, "",
                    "", "", null, null);
        }
    }

    public OnClickAdView mClickViewListener;

    public void setOnClickAdView(OnClickAdView listener) {
        this.mClickViewListener = listener;
    }

    public interface OnClickAdView {
        //        void onClickAdToUse();
        void onClickAdToKnow();
    }

    public void showNormalView() {
        this.setVisibility(View.VISIBLE);
        updateText();
        ll_ad_use.setVisibility(View.VISIBLE);
        ll_ad_know.setVisibility(View.VISIBLE);
    }


    public void updateText() {
        if (mUIPlayContext != null) {
            tv_ad_use.setText(mUIPlayContext.adItem.getAdStopText());
            if (!TextUtils.isEmpty(mUIPlayContext.adItem.getAdStopImageUrl())) {
                iv_ad.setImageUrl(mUIPlayContext.adItem.getAdStopImageUrl(), VolleyHelper.getImageLoader());
                iv_ad.setDefaultImageResId(R.drawable.bg_default_pic);
                iv_ad.setErrorImageResId(R.drawable.bg_default_pic);
            }
        }
    }


    public void showAdDetail() {
//        this.setVisibility(View.GONE);
        ll_ad_use.setVisibility(View.GONE);
        ll_ad_know.setVisibility(View.VISIBLE);
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (AdTools.videoType.equals(mUIPlayContext.videoType)) {
            showNormalView();
        } else {
            this.setVisibility(View.GONE);
        }
    }
}
