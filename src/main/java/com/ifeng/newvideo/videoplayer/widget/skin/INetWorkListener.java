package com.ifeng.newvideo.videoplayer.widget.skin;

/**
 * Created by fanshell on 2016/7/26.
 */
public interface INetWorkListener {
    void onNetWorkChange();
}
