package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;

/**
 * Created by ll on 2017/7/17.
 */
public class VipNoticeView extends RelativeLayout {

    public VipNoticeView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.vip_notice_view, this, true);
        ((TextView)findViewById(R.id.tv_text)).setText(context.getString(R.string.vip_member_notice_text));
    }

    public VipNoticeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VipNoticeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
