package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.SeekBar;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;

/**
 * Created by fanshell on 2016/8/4.
 */
public class SeekBarView extends BaseView implements UIObserver {

    private SeekBar mSeekBarView;

    private SeekBar.OnSeekBarChangeListener mChangeListener;

    private int mSeekBarProgress = 0;

    public SeekBarView(Context context) {
        super(context);
    }

    public SeekBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SeekBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        inflate(mContext, R.layout.default_video_seekbar, this);
        mSeekBarView = (SeekBar) this.getChildAt(0);
        mSeekBarView.setMax(100);
        mSeekBarView.setProgress(0);
        mSeekBarView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mChangeListener != null) {
                    mChangeListener.onProgressChanged(seekBar, progress, fromUser);
                }
                mSeekBarProgress = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (mChangeListener != null) {
                    mChangeListener.onStartTrackingTouch(seekBar);
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //更新播放的进度
                if (mPlayerControl != null) {
                    int newPosition = (int) (mSeekBarProgress / 100.0F * mPlayerControl.getDuration());
                    mPlayerControl.seekTo(newPosition + getRoundNum(mSeekBarProgress * 0.01f * mPlayerControl.getDuration()));
                    seekBar.setProgress(mSeekBarProgress);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PROGRESS_DRAG, null, getCurPage());
                }
                if (mChangeListener != null) {
                    mChangeListener.onStopTrackingTouch(seekBar);
                }
            }
        });
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        updateSeekBarProgress();
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mPlayerControl != null) {
            mPlayerControl.removeUIObserver(this);
        }
    }


    public void updateSeekBarProgress() {
        if (mPlayerControl != null) {
            long currentTime = mPlayerControl.getCurrentPosition();
            int pos = 0;
            long duration = 0;
            // 获取视频总长
            duration = mPlayerControl.getDuration();
            // 计算比例
            if (duration != 0) {
                pos = (int) ((currentTime * 100) / duration);
            }
            if (pos >= mSeekBarProgress) {
                mSeekBarView.setProgress(pos);
            } else if (pos == 0) {
                mSeekBarView.setProgress(pos);
            }
        }
    }

    //四舍五入，解决出现1000.5毫秒的时候，返回1001，尽量返回较大的
    public int getRoundNum(float num)
    {
        long temp = (long)num;
        float a = num - temp;
        if (2 * a >= 1) {
            return 1;
        } else {
            return 0;
        }
    }


    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener listener) {
        this.mChangeListener = listener;
    }
}
