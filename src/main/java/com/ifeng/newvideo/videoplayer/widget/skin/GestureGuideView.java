package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;

/**
 * Created by fanshell on 2016/7/25.
 */
public class GestureGuideView extends BaseView {


    private View progressGuideView;

    public GestureGuideView(Context context) {
        super(context);
    }

    public GestureGuideView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GestureGuideView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {

        LayoutInflater.from(mContext).inflate(R.layout.player_gesture_guide_layout, this, true);

        progressGuideView = findViewById(R.id.player_gesture_guide_progress);
        this.setVisibility(VISIBLE);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TV) {
                    SharePreUtils.getInstance().setLivePlayGestureState(false);
                } else {
                    SharePreUtils.getInstance().setVodPlayGestureState(false);
                }
                GestureGuideView.this.setVisibility(GONE);
            }
        });
    }

    public void update() {
        // 只有横屏显示
        if (mUIPlayContext == null || mUIPlayContext.isAdvert || !ScreenUtils.isLand()) {
            this.setVisibility(GONE);
            return;
        }
        if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TV) { // 直播没有手势进度提示
            if (SharePreUtils.getInstance().getLivePlayGestureState()) {
                this.setVisibility(VISIBLE);
                progressGuideView.setVisibility(GONE);
            } else {
                this.setVisibility(GONE);
            }
        } else { // 非直播 有手势进度提示
            if (SharePreUtils.getInstance().getVodPlayGestureState()) {
                this.setVisibility(VISIBLE);
                progressGuideView.setVisibility(VISIBLE);
            } else {
                this.setVisibility(GONE);
            }
        }
    }


    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        super.attachUIContext(uiPlayContext);
    }

    @Override
    protected void initPlayer() {
    }

}
