package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.IfengSurfaceView;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/7/25.
 */
public abstract class BaseSkin extends BaseViewGroup {
    protected RelativeLayout mVideoContainerView;
    protected IfengSurfaceView mSurfaceView;
    protected TextureView mTextureView;
    protected List<BaseView> mChildView = new ArrayList<>();
    private OrientationSensorUtils mSensorUtils;
    private volatile boolean isBuildFinish;
    private ChangeOrientationHandler mChangeOrientationHandler;

    public BaseSkin(Context context) {
        this(context, null);
    }

    public BaseSkin(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseSkin(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void initView() {
        addVideoContainerView();
        if (mChangeOrientationHandler == null) {
            mChangeOrientationHandler = new ChangeOrientationHandler(this);
        }
        if (mSensorUtils == null) {
            mSensorUtils = new OrientationSensorUtils(mChangeOrientationHandler);
        }
    }


    public void addVideoView(TextureView videoView) {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
        this.mTextureView = videoView;
        mVideoContainerView.addView(videoView, 0, lp);

    }
    public void removeVideoView(TextureView videoView){
        mVideoContainerView.removeView(videoView);
    }

    private void addVideoContainerView() {
        if (mVideoContainerView == null) {
            mVideoContainerView = new RelativeLayout(mContext);
            LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            mVideoContainerView.setBackgroundColor(Color.BLACK);
            mVideoContainerView.setContentDescription("videoContainer");
            this.addView(mVideoContainerView, 0, lp);
        }

    }

    protected void addCenterView(View view) {
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
        this.addView(view, lp);
        addChildView(view);

    }

    protected void addTopAndCenterView(View view) {
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP, TRUE);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL, TRUE);
        this.addView(view, lp);
        addChildView(view);

    }


    protected void addTopView(View view) {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP, TRUE);
        this.addView(view, lp);
        addChildView(view);

    }

    protected void addRightView(View view) {
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT | RelativeLayout.CENTER_VERTICAL);
        this.addView(view, lp);
        addChildView(view);
    }

    protected void addBottomView(View view) {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, TRUE);
        this.addView(view, lp);
        addChildView(view);
    }

    protected void addTopAndRightView(View view) {
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP, TRUE);
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, TRUE);
        this.addView(view, lp);
        addChildView(view);

    }

    public void buildSkin(UIPlayContext playContext) {
        this.mUIPlayContext = playContext;
        this.mSkinType = this.mUIPlayContext.skinType;
        if (!isBuildFinish) {
            synchronized (this) {
                detachComponentView();
                buildComponentView();
                attachUIContext(playContext);
                isBuildFinish = true;
            }

        }
    }


    protected void addChildView(View view) {
        if ((!mChildView.contains(view)) && view instanceof BaseView) {
            synchronized (mChildView) {
                mChildView.add((BaseView) view);
            }
        }
    }

    public void onResume() {
        if (mSensorUtils != null && mSkinType != VideoSkin.SKIN_TYPE_PIC) {
            mSensorUtils.onResume();
        }
    }

    public void onPause() {
        if (mSensorUtils != null && mUIPlayContext.skinType != VideoSkin.SKIN_TYPE_PIC) {
            mSensorUtils.onPause();
        }
    }

    public void registerPlayController(IPlayController playerControl) {
        attachPlayController(playerControl);
    }

    @Override
    public void onAttachUIPlayControl(IPlayController playerControl) {
        attachChildPlayController(playerControl);
    }

    private void attachChildPlayController(IPlayController playerControl) {
        for (BaseView baseView : mChildView) {
            baseView.attachPlayController(playerControl);
        }
    }


    @Override
    public void onAttachUIPlayContext(UIPlayContext playContext) {
        attachChildPlayContext(playContext);
    }


    private void attachChildPlayContext(UIPlayContext playContext) {
        for (BaseView baseView : mChildView) {
            baseView.attachUIContext(playContext);
        }
    }

    protected abstract void buildComponentView();

    protected void detachComponentView() {
        if (mChildView != null && mChildView.size() > 0) {
            synchronized (mChildView) {
                for (int i = mChildView.size() - 1; i >= 0; i--) {
                    BaseView childView = mChildView.get(i);
                    removeView(childView);
                }
                mChildView.clear();
            }
        }
    }


    private static class ChangeOrientationHandler extends Handler {
        WeakReference<BaseSkin> weakReference;

        ChangeOrientationHandler(BaseSkin baseSkin) {
            weakReference = new WeakReference<>(baseSkin);
        }

        @Override
        public void handleMessage(Message msg) {
            BaseSkin baseSkin = weakReference.get();
            if (baseSkin == null) {
                return;
            }
            if (baseSkin.mPlayerControl == null || msg.getData() == null) {
                return;
            }
            Bundle bundle = msg.getData();
            IPlayer.PlayerState state = (IPlayer.PlayerState) bundle.getSerializable(OrientationSensorUtils.KEY_ORIENTATION);
            switch (state) {
                case ORIENTATION_PORTRAIT://竖屏
                    baseSkin.mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                    break;
                case ORIENTATION_LANDSCAPE://横屏
                    baseSkin.mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
                    break;
            }

            super.handleMessage(msg);
        }
    }


    public @VideoSkin.SkinType int getSkinType() {
        return mSkinType;
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mChangeOrientationHandler != null) {
            mChangeOrientationHandler.removeCallbacksAndMessages(null);
        }
    }
}
