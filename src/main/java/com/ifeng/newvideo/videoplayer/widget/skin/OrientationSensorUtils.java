package com.ifeng.newvideo.videoplayer.widget.skin;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import com.ifeng.newvideo.IfengApplication;


/**
 * 播放控手势控制
 *
 * @author dengjiaping
 */
@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class OrientationSensorUtils {

    public static final String KEY_ORIENTATION = "orientation";
    private OrientationSensorListener mOrientationSensorListener;
    private SensorManager sm;
    private Sensor sensor;
    private Handler handler;
    private volatile boolean isRegister;

    public OrientationSensorUtils(Handler handler) {
        this.handler = handler;
        isRegister = false;
        init();
    }

    public void init() {
        sm = (SensorManager) IfengApplication.getInstance().getSystemService(Context.SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mOrientationSensorListener = new OrientationSensorListener(handler);
    }

    public void onResume() {
        if (!isRegister) {
            sm.registerListener(mOrientationSensorListener, sensor, SensorManager.SENSOR_DELAY_UI);
            isRegister = true;
        }
    }

    public void onPause() {
        if (isRegister) {
            sm.unregisterListener(mOrientationSensorListener);
            isRegister = false;
        }
    }


}
