package com.ifeng.newvideo.videoplayer.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsSeekBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeekBarVer extends AbsSeekBar {
    private static final Logger logger = LoggerFactory.getLogger(SeekBarVer.class);
    private Drawable mThumb;
    private int height;
    private float mScale;

    public interface OnSeekBarChangeListenerVer {

        void onProgressChanged(SeekBarVer VerticalSeekBar, int progress, boolean fromUser);

        void onStartTrackingTouch(SeekBarVer VerticalSeekBar);

        void onStopTrackingTouch(SeekBarVer VerticalSeekBar);

    }

    private OnSeekBarChangeListenerVer mOnSeekBarChangeListener;

    public SeekBarVer(Context context) {
        this(context, null);
    }

    public SeekBarVer(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.seekBarStyle);
    }

    public SeekBarVer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListenerVer l) {
        mOnSeekBarChangeListener = l;
    }

    private void onStartTrackingTouch() {
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onStartTrackingTouch(this);
        }
    }

    private void onStopTrackingTouch() {
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onStopTrackingTouch(this);
        }
    }

    private void onProgressRefresh(float scale, boolean fromUser) {
        Drawable thumb = mThumb;
        if (thumb != null) {
            setThumbPos(getHeight(), thumb, scale, Integer.MIN_VALUE);
            invalidate();
        }
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onProgressChanged(this, getProgress(), fromUser);
        }
    }

    @Override
    public synchronized void setProgress(int progress) {
        super.setProgress(progress);
        updateThumb();
    }

    //bug #1343  在全屏播放页，音量条不能调整音量大小（适配问题/Google Nexus5/5.0.1）
    private void updateThumb() {
        onSizeChanged(getWidth(), getHeight(), 0, 0);
    }

    private void setThumbPos(int w, Drawable thumb, float scale, int gap) {
        int available = w - getPaddingLeft() - getPaddingRight();
        int thumbWidth = thumb.getIntrinsicWidth();
        int thumbHeight = thumb.getIntrinsicHeight();
        available -= thumbWidth;
        available += getThumbOffset() * 2;
        int thumbPos = (int) (scale * available);
        int topBound, bottomBound;
        if (gap == Integer.MIN_VALUE) {
            Rect oldBounds = thumb.getBounds();
            topBound = oldBounds.top;
            bottomBound = oldBounds.bottom;
        } else {
            topBound = gap;
            bottomBound = gap + thumbHeight;
        }
        logger.debug("thumb.setBounds（） : left = " + thumbPos + "  top :" + topBound + "  light :" + (thumbPos + thumbWidth) + "  bottom " + bottomBound);
        thumb.setBounds(thumbPos, topBound, thumbPos + thumbWidth, bottomBound);
    }

    protected void onDraw(Canvas c) {
        c.rotate(-90);
        // c.translate(0, -width);
        c.translate(-height, 0);
        super.onDraw(c);
    }

    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // width = 200;
        // height = 120;
        height = MeasureSpec.getSize(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        this.setMeasuredDimension(width, height);

    }

    @Override
    public void setThumb(Drawable thumb) {
        mThumb = thumb;
        super.setThumb(thumb);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(h, w, oldw, oldh);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setPressed(true);
                onStartTrackingTouch();
                trackTouchEvent(event);
                break;

            case MotionEvent.ACTION_MOVE:
                trackTouchEvent(event);
                onProgressRefresh(mScale, false);
                attemptClaimDrag();
                break;

            case MotionEvent.ACTION_UP:
                trackTouchEvent(event);
                onStopTrackingTouch();
                setPressed(false);
                break;

            case MotionEvent.ACTION_CANCEL:
                onStopTrackingTouch();
                setPressed(false);
                break;
        }
        return true;
    }

    private void trackTouchEvent(MotionEvent event) {
        final int Height = getHeight();
        final int available = Height - getPaddingBottom() - getPaddingTop();
        int Y = (int) event.getY();
        float scale;
        if (Y > Height - getPaddingBottom()) {
            scale = 0.0f;
        } else if (Y < getPaddingTop()) {
            scale = 1.0f;
        } else {
            scale = (float) 1 - (float) (Y) / (float) available;
        }
        mScale = scale;
        final int max = getMax();
        float progress = scale * max;
        logger.debug("trackTouchEvent -- setProgress : " + (int) progress);
        setProgress((int) progress);

    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

}
