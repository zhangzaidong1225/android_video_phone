package com.ifeng.newvideo.videoplayer.widget.skin;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.video.core.utils.GravityUtils;

import static com.ifeng.newvideo.videoplayer.widget.skin.OrientationSensorUtils.KEY_ORIENTATION;

public class OrientationSensorListener implements SensorEventListener {

    private Handler rotateHandler;

    public OrientationSensorListener(Handler handler) {
        rotateHandler = handler;
    }

    private float x;
    private float y;
    private float z;

    public void onSensorChanged(SensorEvent event) {

        if (!GravityUtils.isSystemGravityOpened(IfengApplication.getInstance())) {
            return;
        }

        x = event.values[0];
        y = event.values[1];
        z = event.values[2];

        //接收到重力变为竖着的时候做的一些操作
        if (Math.abs(x) <= 3.0f && y >= 7.0f && Math.abs(z) <= 5) {

            if (!ScreenUtils.isLand()) {// 当前是竖屏了不用再发了
                return;
            }
            Message msg = Message.obtain();
            Bundle bundle = new Bundle();
            bundle.putSerializable(KEY_ORIENTATION, IPlayer.PlayerState.ORIENTATION_PORTRAIT);
            msg.setData(bundle);
            rotateHandler.sendMessage(msg);

        }
        //接收到重力变为横着的时候做的一些操作
        if (Math.abs(x) >= 7.0f && Math.abs(y) <= 4f && Math.abs(z) <= 6.0) {
            if (ScreenUtils.isLand()) {
                return;
            }
            Message msg = Message.obtain();
            Bundle bundle = new Bundle();
            bundle.putSerializable(KEY_ORIENTATION, IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
            msg.setData(bundle);
            rotateHandler.sendMessage(msg);

        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


}
