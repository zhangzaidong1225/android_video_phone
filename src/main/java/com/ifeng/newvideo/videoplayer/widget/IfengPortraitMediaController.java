package com.ifeng.newvideo.videoplayer.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import com.ifeng.newvideo.R;
import com.ifeng.video.player.IfengMediaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yuelong on 2014/9/11.
 */
public class IfengPortraitMediaController extends IfengMediaController {

    private static final Logger logger = LoggerFactory.getLogger(IfengPortraitMediaController.class);

    public IfengPortraitMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IfengPortraitMediaController(Context context) {
        super(context);
        DisplayMetrics disp = mContext.getResources().getDisplayMetrics();
        mWindowWidth = disp.widthPixels;
        mWindowHeight = mWindowWidth * 9 / 16;
    }

    @Override
    protected View makeControllerView() {
        return ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mediacontroller_portrait, this);
    }

    @Override
    protected void initControllerView(View v) {
        super.initControllerView(v);
        mProgressBar = (SeekBar) v.findViewById(R.id.mediacontroller_seekbar);
        if (mProgressBar != null && mProgressBar instanceof SeekBar) {
            SeekBar seeker = (SeekBar) mProgressBar;
            // 索尼的机器不知道为何在横竖平切换的时候Thumb显示不出来了。 为此加上下面代码。
            Drawable thumb = mContext.getResources().getDrawable(R.drawable.common_seek_bar_indicator);
            thumb.setBounds(0, 0, thumb.getIntrinsicWidth(), thumb.getIntrinsicHeight());
            seeker.setThumb(thumb);
            seeker.setThumbOffset(thumb.getIntrinsicWidth() / 2);
        }
        mFullScreenButton = (RelativeLayout) v.findViewById(R.id.mediacontroller_fullScreen);
        if (mFullScreenButton != null) {
            mFullScreenButton.setOnClickListener(mScaleVideoListener);
        }

    }

    private final OnClickListener mScaleVideoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onFullScreenClick();
            }
        }
    };

    @Override
    protected void setWindow() {
       /* int[] location = new int[2];
        mAnchor.getLocationOnScreen(location);
        Rect anchorRect = new Rect(location[0], location[1], location[0] + mAnchor.getWidth(), location[1] + mAnchor.getHeight());
        mWindow.setAnimationStyle(mAnimStyle);
        mWindow.setWidth(mWindowWidth);
        mWindow.setHeight(mWindowHeight / 4);
        logger.debug("setWindow====={}", anchorRect.toString());
        mWindow.showAtLocation(mAnchor, Gravity.NO_GRAVITY, anchorRect.left, mWindowHeight * 3 / 4 + DisplayUtils.getStatusBarHeight(mContext));*/
        int[] location = new int[2];
        mAnchor.getLocationOnScreen(location);
        mWindow.setAnimationStyle(mAnimStyle);
        mWindow.setWidth(mWindowWidth);
        mWindow.setHeight(mWindowHeight / 4);
        mWindow.showAsDropDown(mAnchor, 0, -mWindowHeight / 4);
    }


    @Override
    public void updatePausePlayView() {
        if (mPlayer == null || mRoot == null || mPauseOrPlayButton == null) {
            return;
        }
        if (mPlayer.isPlaying()) {
            logger.debug("mPlayer.isPlaying()---->true");
            mPauseOrPlayButton.setImageResource(R.drawable.video_pause_btn_selector);
        } else {
            logger.debug("mPlayer.isPlaying()---->false");
            mPauseOrPlayButton.setImageResource(R.drawable.video_play_btn_selector);
        }
    }

}
