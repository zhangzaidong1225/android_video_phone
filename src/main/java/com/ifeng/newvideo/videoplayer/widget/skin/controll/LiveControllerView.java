package com.ifeng.newvideo.videoplayer.widget.skin.controll;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.widget.skin.SeekBarView;

/**
 * 直播底页控制器
 * Created by fanshell on 2016/8/8.
 */
public class LiveControllerView extends BaseControllerView implements UIObserver {

    private View mFullScreenView;
    private TextView mStreamText;

    public LiveControllerView(Context context) {
        super(context);
    }

    public LiveControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LiveControllerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void appendChildView() {
        mFullScreenView = findViewById(R.id.control_full);
        mFullScreenView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configuration configuration = mContext.getResources().getConfiguration();
                if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_FULL_SCREEN, null, PageIdConstants.PLAY_LIVE_H);
                } else {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_FULL_SCREEN, null, PageIdConstants.PLAY_LIVE_V);
                }
            }
        });

        mStreamText = (TextView) findViewById(R.id.control_current_stream);
        mStreamText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mStreamView != null) {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DEF, null, PageIdConstants.PLAY_LIVE_H);
                    mStreamView.showView();
                }
            }
        });
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.default_video_controller_tv;
    }

    @Override
    public void showController() {
        setFullViewStatus();
        setStreamViewStatus(ScreenUtils.isLand(), mIsAudio);
        this.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideController() {
        this.setVisibility(View.GONE);
    }

    @Override
    public SeekBarView getSeekBar() {
        return null;
    }

    private void setFullViewStatus() {
        if (ScreenUtils.isLand()) {
            this.mFullScreenView.setVisibility(View.GONE);
        } else {
            this.mFullScreenView.setVisibility(View.VISIBLE);
        }
    }

    private void setStreamViewStatus(boolean isLandScape, boolean isAudio) {
        if (isLandScape) {
            mStreamText.setVisibility(isAudio ? GONE : VISIBLE);
            mStreamText.setText(mUIPlayContext.streamType);
            mStreamText.setTextColor(Color.parseColor("#ffffff"));
            mStreamText.setEnabled(true);
        } else {
            mStreamText.setVisibility(GONE);
            mStreamText.setEnabled(false);
            mStreamText.setTextColor(Color.parseColor("#aaaaaa"));
            mStreamText.setText(getResources().getString(R.string.common_video_high));
        }
    }

    private boolean mIsAudio = false;

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        mIsAudio = bundle.getBoolean("isAudio", false);
        switch (status) {
            case STREAM_CHANGE:
                mUIPlayContext.streamType = bundle.getString(StreamUtils.KEY_STREAM);
                this.mStreamText.setText(mUIPlayContext.streamType);
                break;
            case ORIENTATION_LANDSCAPE:
                mUIPlayContext.orientation = IPlayer.PlayerState.ORIENTATION_LANDSCAPE;
                setFullViewStatus();
                setStreamViewStatus(true, mIsAudio);
                break;
            case ORIENTATION_PORTRAIT:
                mUIPlayContext.orientation = IPlayer.PlayerState.ORIENTATION_PORTRAIT;
                setFullViewStatus();
                setStreamViewStatus(false, mIsAudio);
                break;
            default:
                break;
        }
    }
}
