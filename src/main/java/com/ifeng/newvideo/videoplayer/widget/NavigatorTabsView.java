package com.ifeng.newvideo.videoplayer.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.interfaced.NavigatorTabsAdapter;
import com.ifeng.newvideo.videoplayer.interfaced.NavigatorTextViewOnSelectedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * 播放页面中导航条控件
 */
public class NavigatorTabsView extends LinearLayout implements ViewPager.OnPageChangeListener {

    private static final Logger logger = LoggerFactory.getLogger(NavigatorTabsView.class);

    public interface OnNavigatorTabChangeListener {
        void onNavigatorTabSelected(int position);
    }

    private final Context mContext;

    private ViewPager mPager;

    private NavigatorTabsAdapter mAdapter;

    private final ArrayList<View> mTabs = new ArrayList<View>();
    private Drawable mDividerDrawable;

    private int mDividerColor = 0xFF636363;
    private int mDividerMarginTop = 12;
    private int mDividerMarginBottom = 21;

    public NavigatorTabsView(Context context) {
        this(context, null);
    }

    public NavigatorTabsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigatorTabsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);

        this.mContext = context;

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ViewPagerExtensions, defStyle, 0);

        mDividerColor = a.getColor(R.styleable.ViewPagerExtensions_dividerColor, mDividerColor);

        mDividerMarginTop = a.getDimensionPixelSize(R.styleable.ViewPagerExtensions_dividerMarginTop, mDividerMarginTop);
        mDividerMarginBottom = a.getDimensionPixelSize(R.styleable.ViewPagerExtensions_dividerMarginBottom, mDividerMarginBottom);

        mDividerDrawable = a.getDrawable(R.styleable.ViewPagerExtensions_dividerDrawable);

        a.recycle();

        this.setOrientation(LinearLayout.HORIZONTAL);
    }

    /**
     * Sets the data behind this NavigatorTabsView.
     *
     * @param adapter The {@link com.ifeng.newvideo.videoplayer.interfaced.NavigatorTabsAdapter} which is responsible for maintaining
     *                the data backing this FixedTabsView and for producing a view
     *                to represent an item in that data set.
     */
    public void setAdapter(NavigatorTabsAdapter adapter) {
        this.mAdapter = adapter;

        if (mPager != null && mAdapter != null)
            initTabs();
    }

    /**
     * Binds the {@link android.support.v4.view.ViewPager} to this View
     */
    public void setViewPager(ViewPager pager) {
        this.mPager = pager;
        mPager.setOnPageChangeListener(this);

        if (mPager != null && mAdapter != null)
            initTabs();
    }

    public ViewPager getViewPager() {
        return mPager;
    }

    /**
     * Initialize and add all tabs to the layout
     */
    private void initTabs() {

        removeAllViews();
        mTabs.clear();

        if (mAdapter == null)
            return;

        for (int i = 0; i < mPager.getAdapter().getCount(); i++) {

            final int index = i;

            View tab = mAdapter.getView(i);
            if (tab == null) {
                continue;
            }
            // add if judgement by hgl
            if (tab.getLayoutParams() == null) {
                LayoutParams params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f);
                tab.setLayoutParams(params);
            }
            this.addView(tab);

            mTabs.add(tab);
            // if you don't want a seprator view, invoke setNeedSeparator(false);
            if (needSeparator && (i != mPager.getAdapter().getCount() - 1)) {
                this.addView(getSeparator());
            }

            tab.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPager.getCurrentItem() == index && navigatorTabListener != null) {
                        navigatorTabListener.onNavigatorTabSelected(index);
                        return;
                    }
                    mPager.setCurrentItem(index);
                }
            });

        }
        selectTab(mPager.getCurrentItem());
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public final void onPageSelected(int position) {
        selectTab(position);
        if (navigatorTabListener != null) {
            navigatorTabListener.onNavigatorTabSelected(position);
        }
    }

    private OnNavigatorTabChangeListener navigatorTabListener;

    public void setNavigatorTabChangeListener(OnNavigatorTabChangeListener listener) {
        this.navigatorTabListener = listener;
    }

    /**
     * Creates and returns a new Separator View
     *
     * @return
     */
    private View getSeparator() {
        View v = LayoutInflater.from(mContext).inflate(R.layout.video_navigator_divider, null);
        v.setVisibility(View.GONE);
        return v;
    }

    /**
     * Runs through all tabs and sets if they are currently selected.
     *
     * @param position The position of the currently selected tab.
     */
    private void selectTab(int position) {
        for (int i = 0, pos = 0; i < getChildCount(); i++) {

            View childView = this.getChildAt(i);
            boolean temp = i % 2 == 0;
            boolean isSelected = i == position;
            if (needSeparator && temp) {
                if (childView instanceof NavigatorTextViewOnSelectedListener) {
                    ((NavigatorTextViewOnSelectedListener) childView).onSelected(pos == position);
                }
                pos++;
            } else {
                if (mAdapter != null) {
                    mAdapter.changeViewState(position, this);
                }
                if (childView instanceof NavigatorTextViewOnSelectedListener) {
                    ((NavigatorTextViewOnSelectedListener) childView).onSelected(isSelected);
                }
            }
            childView.setSelected(isSelected);
        }
    }

    public void childViewPerformClick(int childIndex) {
        int realChildIndex = needSeparator ? childIndex * 2 : childIndex;
        if (this.getChildAt(realChildIndex) == null) {
            return;
        }
        if (needSeparator) {
            this.getChildAt(childIndex * 2).performClick();
        } else {
            this.getChildAt(childIndex).performClick();
        }
    }

    private boolean needSeparator = true;

    public void setNeedSeparator(boolean b) {
        this.needSeparator = b;
    }

    public boolean getNeedSeparator() {
        return this.needSeparator;
    }

    /**
     * 此方法只是为了修复Bug：16339而存在，无其他用处 此Bug跟音视频和网络都无关系。
     * 问题表象：当在直播横屏的时候切换其他频道，切换完成后点击分享至RenRen网，
     * 跳到分享编辑页后点击发布。在没有授权过的情况下会跳到RenRen授权页，在授权页点击返回，回到播放页并切回竖屏
     * 此时FixTab和播放的视频都是正确的，只有直播节目列表仍然是切换前的，没有对应上。
     * 奇怪的是mPager.getCurrentItem()返回的值还是切换后的正确的Index。 所以为了解决此问题，写了下面的方法。
     */
    public void checkoutIndexInTabsAndPager() {
        int size = 0;

        if (mPager != null && mPager.getAdapter() != null) {
            size = mPager.getAdapter().getCount();
        }

        if (size > 0) {

            mPager.getAdapter().notifyDataSetChanged();
            mPager.setOnPageChangeListener(null);
            mPager.post(new Runnable() {

                @Override
                public void run() {
                    final int curIndex = mPager.getCurrentItem();

                    int size = mPager.getAdapter().getCount();
                    mPager.requestFocus();
                    mPager.setCurrentItem((curIndex + 1) % size);
                    mPager.setOnPageChangeListener(NavigatorTabsView.this);
                    mPager.forceLayout();

                    mPager.post(new Runnable() {

                        @Override
                        public void run() {
                            mPager.setCurrentItem(curIndex);
                        }
                    });
                }
            });
        }
    }

}
