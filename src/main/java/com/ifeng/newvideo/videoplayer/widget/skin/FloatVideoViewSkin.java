package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;

/**
 * Created by ll on 2017/9/26.
 */
public class FloatVideoViewSkin extends BaseSkin implements UIObserver, View.OnClickListener {
    private LoadView mLoadView;

    private ErrorView mErrorView;

    private NoNetWorkView mNoNetWorkView;
    private PipMobileView mMobileView;
    private RelativeLayout mPipCloseView;
    private PipSeekBar mSeekBar;
    private NetworkConnectionReceiver mNetReceiver;
    private INetWorkListener mNetWorkListener;
    private VideoSkin.OnNetWorkChangeListener mNetChangeListener;
    private VideoSkin.OnLoadFailedListener mOnLoadFailedListener;
    private boolean isShowErrorView = false;
    private int portraitHeight = 0;
    private int portraitWidth = 0;
    public void setOnLoadFailedListener(VideoSkin.OnLoadFailedListener listener) {
        this.mOnLoadFailedListener = listener;
    }
    public FloatVideoViewSkin(Context context) {
        super(context);
    }

    public FloatVideoViewSkin(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatVideoViewSkin(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        super.initView();
        if (mNetReceiver == null) {
            mNetReceiver = new NetworkConnectionReceiver(mContext);
            mNetWorkListener = new NetWorkListener();
            registerReceiver();
        }
    }

    private volatile boolean isRegister;
    /**
     * 注册网络监听的广播
     */
    private void registerReceiver() {
        if (!isRegister) {
            isRegister = true;
            mNetReceiver.registerReceiver();
            if (mNoNetWorkView == null) {
                mNetWorkListener = new NetWorkListener();
            }
            mNetReceiver.setNetWorkListener(mNetWorkListener);
        }
    }

    /**
     * 注销网络监听的广播
     */
    private void unregisterReceiver() {
        if (mNetReceiver != null && isRegister) {
            mNetReceiver.unregisterReceiver();
            mNetReceiver.setNetWorkListener(null);
            isRegister = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    @Override
    protected void buildComponentView() {
        changeLayoutParams(16, 9);
        initCommonView();
        if (mPlayerControl != null) {
            hideLoadingView();
            attachPlayController(mPlayerControl);
            return;
        }
        if (!NetUtils.isNetAvailable(mContext)) {
            showNoNetWork();
        } else if (NetUtils.isMobile(mContext) && !IfengApplication.mobileNetCanPlay) {
            showMobileView();
        } else {
            mLoadView.showBigPicLoading();
        }
    }

    @Override
    public void buildSkin(UIPlayContext playContext) {
        detachComponentView();
        buildComponentView();
    }

    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        this.mUIPlayContext = uiPlayContext;
        this.mSkinType = this.mUIPlayContext.skinType;
        super.attachUIContext(uiPlayContext);
    }

    private void hideLoadingView() {
        for (BaseView child : mChildView) {
            if (child instanceof LoadView) {
                child.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void initPlayer() {
        this.mPlayerControl.addUIObserver(this);
    }

    private void initCommonView() {

        mNoNetWorkView = new NoNetWorkView(mContext);
        mNoNetWorkView.setNoNetWorkClick(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.PIP_CLICK_PLAY_RETRY, PageActionTracker.getPageName(false, getContext()));
                if (mNetChangeListener != null) {
                    mNetChangeListener.onNoNetWorkClick();
                }

            }
        });

        mLoadView = new LoadView(mContext);

        //mobileView
        if (!IfengApplication.mobileNetCanPlay) {
            mMobileView = new PipMobileView(mContext);
            addCenterView(mMobileView);
            mMobileView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getId() == R.id.video_mobile_to_order) {
                        IntentUtils.startADWebActivity(mContext, null, SharePreUtils.getInstance().getMobileEnterUrl(), null, null, "任我看", "", "", "", null, null);
                    } else {
                        IfengApplication.mobileNetCanPlay = true;
                        if (mNetChangeListener != null) {
                            ToastUtils.getInstance().showShortToast("您正在使用运营商网络");
                            mNetChangeListener.onMobileClick();
                        }
                    }
                }
            });

        }
        mErrorView = new ErrorView(mContext);
        mErrorView.findViewById(R.id.video_error_retry).setOnClickListener(mRetryClickListener);
        mErrorView.findViewById(R.id.video_error_refresh).setOnClickListener(mRetryClickListener);

        mPipCloseView = new RelativeLayout(mContext);
        ImageView close = new ImageView(mContext);
        close.setImageResource(R.drawable.pip_video_player_close_icon);
        mPipCloseView.setPadding(10,10,10,10);
        mPipCloseView.addView(close);
        mPipCloseView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeViews();
                PageActionTracker.clickBtn(ActionIdConstants.PIP_CLICK_TO_CLOSE, PageActionTracker.getPageName(false, getContext()));
            }
        });

        mSeekBar = new PipSeekBar(mContext);

        addCenterView(mNoNetWorkView);
        addCenterView(mLoadView);
        addCenterView(mErrorView);
        addTopAndRightView(mPipCloseView);
        addBottomView(mSeekBar);
    }

    public void removeViews() {
        ViewGroup group = (ViewGroup)getParent();
        if (EmptyUtils.isNotEmpty(group)) {
            mPlayerControl.stop();
            group.clearAnimation();
            group.setVisibility(GONE);
            ActivityMainTab.setPiPMode(false);
        }
    }

    /**
     * 用于实现单击重试的
     */
    private View.OnClickListener mRetryClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (ClickUtils.isFastDoubleClick()) {
                return;
            }
            PageActionTracker.clickBtn(ActionIdConstants.PIP_CLICK_PLAY_RETRY, PageActionTracker.getPageName(false, getContext()));
            if (mOnLoadFailedListener != null) {
                mOnLoadFailedListener.onLoadFailedListener();
            }
        }
    };

    private int mVideoWidth;
    private int mVideoHeight;

    public void setVideoSize(int videoWidth, int videoHeight) {
        mVideoWidth = videoWidth;
        mVideoHeight = videoHeight;
        changeLayoutParams(mUIPlayContext.widthRadio, mUIPlayContext.heightRadio);

    }

    private void changeLayoutParams(int width, int height) {
        int screenWidth = DisplayUtils.getWindowWidth();
        int screenHeight = DisplayUtils.getWindowHeight();
        if (screenHeight > screenWidth) {
            screenHeight = DisplayUtils.getWindowWidth();
        }
        portraitWidth = DisplayUtils.convertDipToPixel(200);
        portraitHeight = DisplayUtils.convertDipToPixel(200) * height / width;
        this.getLayoutParams().width = DisplayUtils.convertDipToPixel(200);
        this.getLayoutParams().height = DisplayUtils.convertDipToPixel(200) * height / width;
        changeSurfaceParam();
        requestLayout();
    }

    private void changeSurfaceParam() {
        if (mVideoHeight <= 1 || mVideoWidth <= 1) {
            return;
        }

        float videoRatio = mVideoWidth / (float) mVideoHeight;
        RelativeLayout.LayoutParams lp = (LayoutParams) mTextureView.getLayoutParams();
        lp.width = portraitWidth;
        lp.height = portraitHeight;
        float parentRatio = mUIPlayContext.widthRadio / (float) mUIPlayContext.heightRadio;
        lp.width = parentRatio < videoRatio ? portraitWidth : (int) (videoRatio * portraitHeight);
        lp.height = parentRatio > videoRatio ? portraitHeight : (int) (portraitWidth / videoRatio);

        mTextureView.setLayoutParams(lp);

    }

    public void setNoNetWorkListener(VideoSkin.OnNetWorkChangeListener listener) {
        this.mNetChangeListener = listener;
    }

    private boolean isMobileNetForPlaying = false;
    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        isMobileNetForPlaying = false;
        switch (status) {
            case STATE_ERROR://错误
                if (!NetUtils.isNetAvailable(mContext)) {
                        showNoNetWork();
                } else if (NetUtils.isMobile(this.getContext()) && !IfengApplication.mobileNetCanPlay) {
                    showMobileView();
                } else {
                    showErrorView();
                }
                break;
            case STATE_PREPARING://准备开始播放
                showLoadingView();
                if (mLoadView != null) {
                    mLoadView.updateText();
                }
                break;
            case STATE_BUFFERING_START://缓冲开始
                break;
            case STATE_PLAYING:
            case PIP_STATE_PLAYING:
            case STATE_BUFFERING_END://缓冲结束
                if (mPlayerControl != null) {
                    hideAllView();
                }
                break;
        }
    }

    private void showLoadingView() {
        for (BaseView child : mChildView) {
            if (child instanceof LoadView) {
                child.setVisibility(View.VISIBLE);
                mLoadView.updateText();
            } else {
                child.setVisibility(View.GONE);
            }
        }
        mLoadView.hideBigPicLoading();
        mLoadView.showBigPicLoading();
    }

    private void showErrorView() {
        hideAllView();
        if (mErrorView != null) {
            mErrorView.setVisibility(View.VISIBLE);
            if (!ScreenUtils.isLand()) {
                switch (mSkinType) {
                    case VideoSkin.SKIN_TYPE_PIC:
                    case VideoSkin.SKIN_TYPE_TOPIC:
                    case VideoSkin.SKIN_TYPE_TV:
                        mErrorView.hideBackView();
                        break;
                    default:
                        mErrorView.showBackView();
                        break;
                }
            } else {
                mErrorView.showBackView();
            }

            isShowErrorView = true;
            if (mPlayerControl != null && mPlayerControl.isPlaying()) {
                mPlayerControl.pause();
            }
        }
    }
    private void showNoNetWork() {
        if (mPlayerControl != null && (isMobileNetForPlaying || mPlayerControl.isPlaying())) {
            mPlayerControl.pause();
        }
        if (existView(NoNetWorkView.class)) {
            hideAllView();
            showView(NoNetWorkView.class);
            mNoNetWorkView.hideBackView();
        }
    }

    private void showMobileView() {
        if (mPlayerControl != null && isMobileNetForPlaying) {
            mPlayerControl.pause();
        }
        hideAllView();
        if (mMobileView != null) {
            mMobileView.showView(MobileView.DEFAULT_STATUS);
        }
    }

    private void showView(Class clazz) {
        int size = mChildView.size();
        BaseView view;
        for (int i = size - 1; i >= 0; i--) {
            view = mChildView.get(i);
            if (view.getClass() == clazz) {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean existView(Class clazz) {
        for (View view : mChildView) {
            if (view.getClass() == clazz) {
                return true;
            }
        }
        return false;
    }

    private void hideAllView() {
        for (BaseView view : mChildView) {
            if (view instanceof PipSeekBar) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
        isShowErrorView = false;
    }

    @Override
    public void onClick(View v) {
    }

    private class NetWorkListener implements INetWorkListener {
        @Override
        public void onNetWorkChange() {
            if (null != mPlayerControl) {
                if (!NetUtils.isNetAvailable(mContext)) {
                    showNoNetWork();
                } else if (NetUtils.isMobile(mContext) && !IfengApplication.mobileNetCanPlay) {
                    showMobileView();
                } else if (NetUtils.isWifi(mContext) && mPlayerControl != null && !mPlayerControl.isPlaying()) {
                    //在显示数据网提示的状态下自动播放
                    if (mNetChangeListener != null && mMobileView != null && mMobileView.isShown()) {
                        mNetChangeListener.onNoNetWorkClick();
                    }
                } else if (NetUtils.isMobile(mContext) && IfengApplication.mobileNetCanPlay) {
                    //网络在切换到数据网,播放时提示
                    if (mPlayerControl != null && mPlayerControl.isPlaying()) {
                        if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                            ToastUtils.getInstance().showShortToast(R.string.play_moblie_net_hint);
                        }
                    }

                }
            }
        }
    }

}
