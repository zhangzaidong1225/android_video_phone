package com.ifeng.newvideo.videoplayer.widget.skin.controll;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/7/25.
 */
public abstract class BaseControllerView extends BaseViewGroup {
    protected List<BaseView> mChildView;
    protected StreamView mStreamView;

    public BaseControllerView(Context context) {
        super(context, null);
    }

    public BaseControllerView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public BaseControllerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onAttachUIPlayControl(IPlayController playerControl) {
        if (mChildView != null && !mChildView.isEmpty()) {
            for (BaseView baseView : mChildView) {
                baseView.attachPlayController(playerControl);
            }
        }
    }

    @Override
    public void onAttachUIPlayContext(UIPlayContext playContext) {
        if (mChildView != null && !mChildView.isEmpty()) {
            for (BaseView baseView : mChildView) {
                baseView.attachUIContext(playContext);
            }
        }
    }


    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(getLayoutId(), this);
        mChildView = new ArrayList<BaseView>();
        appendChildView();
    }

    protected abstract void appendChildView();


    protected abstract int getLayoutId();

    public abstract void showController();

    public abstract void hideController();


    public void setStreamView(StreamView streamView) {
        this.mStreamView = streamView;
    }


    public abstract SeekBarView getSeekBar();


}
