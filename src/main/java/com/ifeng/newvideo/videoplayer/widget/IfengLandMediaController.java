package com.ifeng.newvideo.videoplayer.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.player.IfengMediaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 点播横屏操作栏
 * Created by yuelong on 2014/9/11.
 */
public class IfengLandMediaController extends IfengMediaController {

    private static final Logger logger = LoggerFactory.getLogger(IfengLandMediaController.class);
    private boolean isNextBtnEnable = true;


    public IfengLandMediaController(Context context) {
        super(context);
        DisplayMetrics disp = mContext.getResources().getDisplayMetrics();
        mWindowWidth = disp.widthPixels;
        mWindowHeight = disp.heightPixels;

        //音频模式下有可能是在后台新建出来的
        if (mWindowWidth < mWindowHeight) {
            int temp;
            temp = mWindowWidth;
            mWindowWidth = mWindowHeight;
            mWindowHeight = temp;
        }
    }

    @Override
    protected View makeControllerView() {
        return ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mediacontroller_land, this);
    }

    private final OnClickListener mDlNaVideoListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onDlnaClick();
            }
        }

    };

    private final OnClickListener mSelectStreamVideoListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onSelectClick();
            }
        }
    };

    @Override
    protected void initControllerView(View v) {
        super.initControllerView(v);
        mProgressBar = (SeekBar) v.findViewById(R.id.mediacontroller_seekbar);
        if (mProgressBar != null && mProgressBar instanceof SeekBar) {
            SeekBar seeker = (SeekBar) mProgressBar;
            Drawable thumb = mContext.getResources().getDrawable(R.drawable.common_seek_bar_indicator);
            thumb.setBounds(0, 0, thumb.getIntrinsicWidth(), thumb.getIntrinsicHeight());
            seeker.setThumb(thumb);
            seeker.setThumbOffset(thumb.getIntrinsicWidth() / 2);
        }
        mFullScreenButton = (RelativeLayout) v.findViewById(R.id.mediacontroller_fullScreen);
        if (mFullScreenButton != null) {
            mFullScreenButton.setOnClickListener(mScaleVideoListener);
        }

        mDlnaButton = (ImageView) v.findViewById(R.id.mediacontroller_dlna);
        if (mDlnaButton != null) {
            mDlnaButton.setOnClickListener(mDlNaVideoListener);
        }
        mDlnaDivider = v.findViewById(R.id.mediacontroller_dlna_divider);

        mSelectStream = (TextView) v.findViewById(R.id.mediacontroller_selected);
        mSelectStreamLine = v.findViewById(R.id.mediacontroller_selected_line);
        if (mSelectStream != null) {
            mSelectStream.setOnClickListener(mSelectStreamVideoListener);
        }
        mNetVideoBtn = (ImageView) v.findViewById(R.id.landscape_controller_next_video);
        if (!isNextBtnEnable) {
            mNetVideoBtn.setClickable(false);
            mNetVideoBtn.setImageResource(R.drawable.video_next_video_button_no_selected);
        }
        if (mNetVideoBtn != null) {
            mNetVideoBtn.setOnClickListener(mLandProgramClick);
        }

        View mediaControllerLandLayout = v.findViewById(R.id.mediacontroller_land_layout);
        mediaControllerLandLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IfengLandMediaController.this.isShowing()) {
                    IfengLandMediaController.this.hide();
                }
            }
        });

/*        mPreVideoBtn = (ImageView) v.findViewById(R.id.landscape_controller_pre_video);
        if (mPreVideoBtn != null) {
            mPreVideoBtn.setOnClickListener(mLandProgramClick);
        }*/
    }

    public void setNextBtnEnable(boolean enable) {
        isNextBtnEnable = enable;
    }

    private final OnClickListener mScaleVideoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                getControllerToVideoPlayerListener().onFullScreenClick();
            }
        }
    };

    private final OnClickListener mLandProgramClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (getControllerToVideoPlayerListener() != null) {
                switch (v.getId()) {
                    case R.id.landscape_controller_next_video:
                        getControllerToVideoPlayerListener().onSwitchProgram(true);
                        break;

/*                    case R.id.landscape_controller_pre_video:
                        getControllerToVideoPlayerListener().onSwitchProgram(false);
                        break;*/
                    default:
                        break;
                }
            }
        }
    };

    @Override
    protected void setWindow() {
        int[] anchorPos = new int[2];
        mAnchor.getRootView().getLocationOnScreen(anchorPos);
        setLayoutParams(new LayoutParams(mAnchor.getRootView().getWidth(), mAnchor.getRootView().getHeight()));
        Rect anchorRect = new Rect(anchorPos[0], anchorPos[1], anchorPos[0] + mAnchor.getRootView().getWidth(), anchorPos[1] + mAnchor.getRootView().getHeight());
        setLayoutParams(new LayoutParams(mAnchor.getRootView().getWidth(), mAnchor.getRootView().getHeight()));
        mRoot.getLayoutParams().height = mAnchor.getRootView().getHeight() / 4;
        mRoot.requestLayout();
        mWindow.setWidth(mWindowWidth);
        mWindow.setHeight(mWindowHeight / 4);
        logger.debug("setWindow====={}===={}", anchorRect.toString(), mWindowHeight);
        mWindow.showAtLocation(mAnchor.getRootView(), Gravity.NO_GRAVITY, 0, (mWindowHeight * 3 / 4));
    }

    public void updatePausePlayView() {
        if (mPlayer == null || mRoot == null || mPauseOrPlayButton == null) {
            return;
        }
        if (mPlayer.isPlaying()) {
            logger.debug("mPlayer.isPlaying()---->true");
            mPauseOrPlayButton.setImageResource(R.drawable.video_land_pause_btn_selector);
        } else {
            logger.debug("mPlayer.isPlaying()---->false");
            mPauseOrPlayButton.setImageResource(R.drawable.video_land_play_selector);
        }
    }

}
