package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.BatteryManager;
import android.util.AttributeSet;
import android.view.View;
import com.ifeng.video.core.utils.DisplayUtils;

public class BatteryView extends View {

    private static final int COLOR_HEAD = Color.WHITE; // 电池头颜色
    private static final int COLOR_BORDER = Color.WHITE; // 电池边框颜色

    private static final int COLOR_NORMAL = Color.WHITE; // 正常电量
    private static final int COLOR_CHARGING = Color.parseColor("#44db5e"); // 充电中颜色
    private static final int COLOR_LOW = Color.parseColor("#fe3824"); // 低电量

    private static final double POWER_LOW_VALUE = 0.1; // 低电量值0~1  10%

    private int mWidth = DisplayUtils.convertDipToPixel(25);    //总宽
    private int mHeight = DisplayUtils.convertDipToPixel(10);   //总高

    private int mHeadWidth = DisplayUtils.convertDipToPixel(2.3f); // 电池头宽
    private int mHeadHeight = DisplayUtils.convertDipToPixel(3); // 电池头高

    private int mMargin = DisplayUtils.convertDipToPixel(1);    //电池内芯与边框的距离

    private int mBorder = DisplayUtils.convertDipToPixel(1);     //电池外框的宽度
    private float mBorderRadius = DisplayUtils.convertDipToPixel(1.5f); // 电池边框圆角


    private float mCurPowerValue; // 当前电量值 0~1

    private boolean mIsCharging;    //是否在充电

    private Paint mHeadPaint;
    private Paint mBorderPaint;
    private Paint mBodyPaint;

    private RectF mHeadRect; // 电池头
    private RectF mBorderRect; // 电池边框
    private Rect mBodyRect = new Rect(); // 电池芯


    public BatteryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public BatteryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public BatteryView(Context context) {
        super(context);
        initView();
    }

    private void initView() {
        mBorderRect = new RectF(mBorder, mBorder, mWidth - mHeadWidth - mBorder, mHeight - mBorder);

        mHeadRect = new RectF(mBorderRect.right + mBorder, (mHeight - mHeadHeight) / 2,
                mBorderRect.right + mHeadWidth, (mHeight + mHeadHeight) / 2);

        initPaints();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //画外框
        canvas.drawRoundRect(mBorderRect, mBorderRadius, mBorderRadius, mBorderPaint);
        //画电池头
        canvas.drawRoundRect(mHeadRect, mBorderRadius, mBorderRadius, mHeadPaint);
        //画电池芯
        setBodyPaint();
        setBodyRect();
        canvas.drawRect(mBodyRect, mBodyPaint);
    }

    private void setBodyRect() {
        int width = (int) (mCurPowerValue * (mBorderRect.width() - mMargin * 2));
        int left = (int) (mBorderRect.left + mMargin);
        int right = left + width;
        int top = (int) (mBorderRect.top + mMargin);
        int bottom = (int) (mBorderRect.bottom - mMargin);
        mBodyRect.set(left, top, right, bottom);
    }

    private void initPaints() {
        mHeadPaint = new Paint();
        mHeadPaint.setAntiAlias(true);
        mHeadPaint.setStyle(Paint.Style.FILL);  //实心
        mHeadPaint.setColor(COLOR_HEAD);
        mHeadPaint.setStrokeWidth(mBorder);

        mBorderPaint = new Paint();
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setStyle(Paint.Style.STROKE);    //设置空心矩形
        mBorderPaint.setStrokeWidth(mBorder);          //设置边框宽度
        mBorderPaint.setColor(COLOR_BORDER);

        mBodyPaint = new Paint();
        mHeadPaint.setAntiAlias(true);
        mHeadPaint.setStyle(Paint.Style.FILL);
    }

    private void setBodyPaint() {
        if (mIsCharging) {
            mBodyPaint.setColor(COLOR_CHARGING);
        } else {
            if (mCurPowerValue < POWER_LOW_VALUE) {
                mBodyPaint.setColor(COLOR_LOW);
            } else {
                mBodyPaint.setColor(COLOR_NORMAL);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(mWidth, mHeight);
    }

    private void setPower(float power) {
        mCurPowerValue = power;
        invalidate();
    }

    private BroadcastReceiver mPowerConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            mIsCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;

            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            setPower(((float) level) / scale);
        }
    };

    @Override
    protected void onAttachedToWindow() {
        getContext().registerReceiver(mPowerConnectionReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        getContext().unregisterReceiver(mPowerConnectionReceiver);
        super.onDetachedFromWindow();
    }
}
