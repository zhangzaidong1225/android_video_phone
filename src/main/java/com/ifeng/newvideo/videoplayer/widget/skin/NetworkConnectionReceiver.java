package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

/**
 * Created by fanshell on 2016/8/8.
 */
public class NetworkConnectionReceiver extends BroadcastReceiver {
    private Context mContext;
    private INetWorkListener netWorkChangeListener;

    public NetworkConnectionReceiver(Context context) {
        this.mContext = context;
    }

    public void setNetWorkListener(INetWorkListener netWorkChangeListener) {
        this.netWorkChangeListener = netWorkChangeListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (netWorkChangeListener != null) {
            this.netWorkChangeListener.onNetWorkChange();
        }
    }

    /**
     * 取消网络监听广播
     */
    public void unregisterReceiver() {
        try {
            mContext.unregisterReceiver(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 注册网络监听广播
     */
    public void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.registerReceiver(this, filter);
    }
}
