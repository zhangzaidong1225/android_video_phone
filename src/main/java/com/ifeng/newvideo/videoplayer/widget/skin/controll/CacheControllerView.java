package com.ifeng.newvideo.videoplayer.widget.skin.controll;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.widget.skin.SeekBarView;
import com.ifeng.video.core.utils.StringUtils;

/**
 * 缓存播放器控制栏
 * Created by Administrator on 2016/8/28.
 */
public class CacheControllerView extends BaseControllerView {
    private TextView mTotalTimeTextView;
    private TextView mCurrentTimeTextView;
    private SeekBarView mSeekBar;

    private static final int MSG_UPDATE_PROGRESS = 105;


    private Handler mUpdatePressHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PROGRESS:
                    updateTimeView();
                    this.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, 1000);
                    break;
            }

        }
    };

    public CacheControllerView(Context context) {
        super(context);
    }

    public CacheControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CacheControllerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void appendChildView() {
        mTotalTimeTextView = (TextView) findViewById(R.id.control_total_time);
        mCurrentTimeTextView = (TextView) findViewById(R.id.control_current_time);
        mSeekBar = (SeekBarView) findViewById(R.id.control_seekBar);
        mChildView.add(mSeekBar);
    }

    @Override
    protected void initPlayer() {

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cache_video_controller;
    }

    @Override
    public void showController() {
        updateTimeView();
        this.setVisibility(View.VISIBLE);
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
        mUpdatePressHandler.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, 200);
    }

    @Override
    public void hideController() {
        this.setVisibility(View.GONE);
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
        mUpdatePressHandler.removeMessages(MSG_UPDATE_PROGRESS);
    }

    @Override
    public SeekBarView getSeekBar() {
        return mSeekBar;
    }

    private void updateTimeView() {
        if (mPlayerControl != null) {

            final long duration=mPlayerControl.getDuration();
            final long current_position=mPlayerControl.getCurrentPosition();
            if(duration>=0){
                mTotalTimeTextView.setText(StringUtils.formatDuration(duration));
            }
            if(current_position>=0){
                mCurrentTimeTextView.setText(StringUtils.formatDuration(current_position));
            }
        }
        mSeekBar.updateSeekBarProgress();
    }
}
