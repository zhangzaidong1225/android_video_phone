package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.video.core.utils.DisplayUtils;


public class DanmuEditView extends BaseView implements View.OnClickListener, UIObserver {

    private ImageView edit;
//    private boolean showEditView;

    public DanmuEditView(Context context) {
        super(context);
    }

    public DanmuEditView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DanmuEditView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_video_edit) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_DANMA_SEND, PageIdConstants.PLAY_VIDEO_V);
            if (null != editListener) {
                editListener.onClickEditButton();
            }
        }
    }


    public void setEditLandImage() {
        edit.setImageResource(R.drawable.video_danmu_input_land);
    }

    private boolean mIsAudio = false;

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        mIsAudio = bundle.getBoolean("isAudio", false);
        if (mIsAudio) {
            hideView();
        }
//        Log.d("danmu", "--status---" + status);

        switch (status) {
            case ORIENTATION_LANDSCAPE:
                edit.setImageResource(R.drawable.video_danmu_input_land);
                LayoutParams danmuEditViewlp = (LayoutParams) edit.getLayoutParams();
                danmuEditViewlp.height = DisplayUtils.convertDipToPixel(48);
                danmuEditViewlp.width = DisplayUtils.convertDipToPixel(48);
                danmuEditViewlp.setMargins(0, 0, DisplayUtils.convertDipToPixel(12), 0);
                edit.setLayoutParams(danmuEditViewlp);
                break;
            case ORIENTATION_PORTRAIT:
                edit.setImageResource(R.drawable.video_danma_input);
                LayoutParams lp = (LayoutParams) edit.getLayoutParams();
                lp.height = DisplayUtils.convertDipToPixel(32);
                lp.width = DisplayUtils.convertDipToPixel(32);
                lp.setMargins(0, 0, DisplayUtils.convertDipToPixel(12), 0);
                edit.setLayoutParams(lp);
                break;
            case STATE_IDLE:
            case STATE_PREPARING:
            case STATE_BUFFERING_START:
                hideView();
                break;
            case STATE_PLAYING:
            case STATE_BUFFERING_END:
                showView();
                break;
        }

    }

    private OnClickEditButton editListener;

    public void setEditListener(OnClickEditButton listener) {
        this.editListener = listener;
    }

    public interface OnClickEditButton {
        void onClickEditButton();
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext)
                .inflate(R.layout.default_video_danmu_edit_layout, this);
        edit = (ImageView) findViewById(R.id.btn_video_edit);
        edit.setOnClickListener(this);
        this.onClick(this);
        this.setVisibility(View.GONE);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            mPlayerControl.addUIObserver(this);
        }
    }

    public void showView() {
        this.setVisibility(View.VISIBLE);
//        showEditView = true;
    }

    public void hideView() {
        this.setVisibility(View.GONE);
    }

    public void hideView(boolean hideEdit) {
        this.setVisibility(View.GONE);
//        showEditView = hideEdit;
    }
}
