package com.ifeng.newvideo.videoplayer.widget.skin;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.video.core.utils.ListUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by fanshell on 2016/7/26.
 */
public class TitleView extends BaseView implements View.OnClickListener, UIObserver {
    private static final String TAG = "TitleView";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private TextView mTitleText;
    private TextView mTimeView;
    private View mTitleShare;
    private View mLiveProgram;//直播节目单
    private View mVideoAudioChange;//音视频切换
    private ImageView mVideoAudioChangeIcon;//音视频切换

    private View mBackView;
    private View mBatteryTimeLayout;

    private boolean isCacheSkin = false;//是否是缓存播放器


    public TitleView(Context context) {
        super(context);
    }

    public TitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_title, this, true);
        mBackView = findViewById(R.id.video_title_back);

        mTimeView = (TextView) findViewById(R.id.system_time);
        mBatteryTimeLayout = findViewById(R.id.battery_time_layout);

        mBackView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_BACK, null, getCurPage());
                if (isCacheSkin) {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                } else {
                    if (ScreenUtils.isLand()) {
                        mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                    } else {
                        if (mContext instanceof Activity) {
                            ((Activity) mContext).setResult(Activity.RESULT_OK, null);
                            ((Activity) mContext).finish();
                        }
                    }
                }
            }
        });
        mTitleText = (TextView) findViewById(R.id.video_title_text);
        mTitleShare = findViewById(R.id.video_title_share);
        mTitleShare.setOnClickListener(this);
        mLiveProgram = findViewById(R.id.live_program);
        mLiveProgram.setOnClickListener(this);
        mVideoAudioChange = findViewById(R.id.video_audio);
        mVideoAudioChange.setOnClickListener(this);
        mVideoAudioChangeIcon = (ImageView) mVideoAudioChange.findViewById(R.id.video_audio_change);
        setVisibility(GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_title_share:
                if (null != onShareProgramLClickListener) {
                    onShareProgramLClickListener.onShareClick();
                }
                break;
            case R.id.live_program:
                if (null != onShareProgramLClickListener) {
                    onShareProgramLClickListener.onLiveProgramClick();
                }
                break;
            case R.id.video_audio:
                if (null != onVideoAudioChangeListener) {
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_A_V_SWITCH, !isAudio, getCurPage());
                    onVideoAudioChangeListener.onVideoAudioChange();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void initPlayer() {
        if (this.mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }


    private void showTitleText(int visibility) {
        mTitleText.setVisibility(visibility);
        if (!TextUtils.isEmpty(mUIPlayContext.title)) {
            mTitleText.setText(mUIPlayContext.title);
        }
    }


    private OnVideoAudioChangeListener onVideoAudioChangeListener;

    public void updateDependOnControllerView(int visibility) {
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_TOPIC:
            case VideoSkin.SKIN_TYPE_VOD:
                mTitleShare.setVisibility(View.GONE);
                mLiveProgram.setVisibility(View.GONE);
                mVideoAudioChange.setVisibility(visibility);
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    showTitleText(visibility);
                }
                break;
            case VideoSkin.SKIN_TYPE_LOCAL:
                mTitleShare.setVisibility(View.GONE);
                mLiveProgram.setVisibility(View.GONE);
                showOrHideVideoAudioChangeView(visibility == VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_TV:
                mVideoAudioChange.setVisibility(visibility);
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    //横屏
                    mTitleShare.setVisibility(View.GONE);
                    mLiveProgram.setVisibility(View.GONE);
                    showTitleText(View.VISIBLE);
                } else {
                    mTitleShare.setVisibility(visibility);
                    mLiveProgram.setVisibility(View.GONE);
                    mBackView.setVisibility(View.VISIBLE);
                }
                break;
            default:
                mLiveProgram.setVisibility(View.GONE);
                mVideoAudioChange.setVisibility(View.GONE);
                mTitleShare.setVisibility(View.GONE);
                break;
        }

    }

    private boolean isAudio;

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (mSkinType == VideoSkin.SKIN_TYPE_LOCAL) {
            showOrHideVideoAudioChangeView(true);
        }
        if (bundle == null) {
            bundle = new Bundle();
        }
        isAudio = bundle.getBoolean("isAudio", false);
        if (isAudio) {
            mVideoAudioChangeIcon.setImageResource(R.drawable.common_icon_video);
            mVideoAudioChange.setClickable(true);
        } else {
            mVideoAudioChangeIcon.setImageResource(R.drawable.common_icon_audio);
            disableChangeAudioIcon(status);
        }
    }

    /**
     * 音频置灰图标
     *
     * @param status
     */
    private void disableChangeAudioIcon(IPlayer.PlayerState status) {
        if (status == IPlayer.PlayerState.STATE_PLAYING) {
            String path = "";
            switch (mSkinType) {
                case VideoSkin.SKIN_TYPE_TOPIC:
                case VideoSkin.SKIN_TYPE_VOD:
                    if (mUIPlayContext.videoItem != null) {
                        path = StreamUtils.getMp3Url(mUIPlayContext.videoItem.videoFiles);
                    }
                    if (TextUtils.isEmpty(path)) {
                        mVideoAudioChange.setClickable(false);
                        mVideoAudioChangeIcon.setImageResource(R.drawable.icon_audio_disable);
                    } else {
                        mVideoAudioChange.setClickable(true);
                        mVideoAudioChangeIcon.setImageResource(R.drawable.common_icon_audio);
                    }
                    break;
                case VideoSkin.SKIN_TYPE_TV:
                    if (mUIPlayContext.tvLiveInfo != null) {
                        path = mUIPlayContext.tvLiveInfo.getAudio();
                    }
                    if (TextUtils.isEmpty(path)) {
                        mVideoAudioChange.setClickable(false);
                        mVideoAudioChangeIcon.setImageResource(R.drawable.icon_audio_disable);
                    } else {
                        mVideoAudioChange.setClickable(true);
                        mVideoAudioChangeIcon.setImageResource(R.drawable.common_icon_audio);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 是否显示音视频切换View
     * 如果是缓存FM，是没有视频播放地址的，所以隐藏掉音视频切换View
     *
     * @param isShow 是否显示
     */
    private void showOrHideVideoAudioChangeView(boolean isShow) {
        boolean hasVideoUrl = false;//是否有视频播放地址
        if (mUIPlayContext != null && mUIPlayContext.videoItem != null && !ListUtils.isEmpty(mUIPlayContext.videoItem.videoFiles)) {
            String url = StreamUtils.getMediaUrl(mUIPlayContext.videoItem.videoFiles);
            hasVideoUrl = !TextUtils.isEmpty(url);
        }
        mVideoAudioChange.setVisibility(isShow && hasVideoUrl ? VISIBLE : GONE);
    }


    public interface OnVideoAudioChangeListener {
        /**
         * 点击音视频切换
         */
        void onVideoAudioChange();
    }

    public void setOnVideoAudioChangeListener(OnVideoAudioChangeListener onVideoAudioChangeListener) {
        this.onVideoAudioChangeListener = onVideoAudioChangeListener;
    }

    private OnShareProgramLClickListener onShareProgramLClickListener;

    public void setOnShareProgramLClickListener(OnShareProgramLClickListener onShareProgramLClickListener) {
        this.onShareProgramLClickListener = onShareProgramLClickListener;
    }

    public interface OnShareProgramLClickListener {
        /**
         * 点击分享图标
         */
        void onShareClick();

        /**
         * 点击直播页面节目单图标
         */
        void onLiveProgramClick();

    }

    public void setCacheSkin(boolean cacheSkin) {
        isCacheSkin = cacheSkin;
    }


    public void updateViewStatusByOrientation() {
        if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
            initLandViewStatus();
        } else {
            initPortViewStatus();
        }
    }

    /**
     * 初始化在横屏时的状态
     */
    private void initLandViewStatus() {
        this.showTitleShadow(true);
        showBatteryAndSetTime();

        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_PIC://大图
                this.mBackView.setVisibility(View.VISIBLE);
                showTitleText(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_TOPIC://专题
                this.mBackView.setVisibility(View.VISIBLE);
                showTitleText(View.VISIBLE);
                this.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_VOD://点播
                this.mBackView.setVisibility(View.VISIBLE);
                showTitleText(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_TV://直播
                this.mBackView.setVisibility(View.VISIBLE);
                this.mTitleShare.setVisibility(View.GONE);
                this.mLiveProgram.setVisibility(View.GONE);
                showTitleText(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_LOCAL://缓存
                this.mBackView.setVisibility(View.VISIBLE);
                showTitleText(View.VISIBLE);
                this.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_FM:
                showTitleText(View.VISIBLE);
                mBackView.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    /**
     * 初始化在竖屏时的状态
     */
    private void initPortViewStatus() {
        this.showTitleShadow(false);
        hideBatteryAndTime();
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_PIC://大图
                this.mBackView.setVisibility(View.GONE);
                this.mTitleText.setVisibility(View.GONE);
                this.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_TOPIC://专题
                this.mBackView.setVisibility(View.GONE);
                this.mTitleText.setVisibility(View.GONE);
                this.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_VOD://点播
                this.mBackView.setVisibility(View.VISIBLE);
                this.mTitleText.setVisibility(View.GONE);
                this.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_TV://直播
                this.mBackView.setVisibility(View.VISIBLE);
                mTitleText.setVisibility(View.GONE);
                mLiveProgram.setVisibility(View.GONE);
                if (mVideoAudioChange.getVisibility() == View.VISIBLE) {
                    mLiveProgram.setVisibility(View.GONE);
                    mTitleShare.setVisibility(View.VISIBLE);
                }
                this.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_FM:
                mTitleText.setVisibility(View.GONE);
                mBackView.setVisibility(View.VISIBLE);
                this.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void showTitleShadow(boolean isLandscape) {
        if (isLandscape) {
            this.setBackgroundResource(R.drawable.shadow_player_top);
        } else {
            this.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void showBatteryAndSetTime() {
        mBatteryTimeLayout.setVisibility(VISIBLE);
        mTimeView.setText(DATE_FORMAT.format(new Date()));
    }

    private void hideBatteryAndTime() {
        mBatteryTimeLayout.setVisibility(GONE);
    }

}
