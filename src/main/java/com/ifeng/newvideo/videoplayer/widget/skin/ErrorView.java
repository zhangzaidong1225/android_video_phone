package com.ifeng.newvideo.videoplayer.widget.skin;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;

/**
 * Created by fanshell on 2016/7/25.
 */
public class ErrorView extends BaseView implements UIObserver {

    private static final String TAG = "ErrorView";
    private View mBackView;
    private TextView mRetryTextView;
    private View mErrorRetryView;
    private ImageView mChangeView;
    private View mChangeIconContainer;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public ErrorView(Context context) {
        super(context);
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_error, this, true);
        mBackView = findViewById(R.id.video_error_back);
        mBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_LOCAL) {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                    return;
                }
                if (ScreenUtils.isLand()) {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                } else {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                }
            }
        });
        mRetryTextView = (TextView) findViewById(R.id.video_error_retry_text);
        mChangeIconContainer = findViewById(R.id.video_audio_change_container);
        mChangeView = (ImageView) findViewById(R.id.video_error_change);
        mErrorRetryView = findViewById(R.id.video_error_retry);
        this.setVisibility(View.GONE);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            mPlayerControl.addUIObserver(this);
        }
    }


    public void showBackView() {
        mBackView.setVisibility(VISIBLE);
    }

    public void hideBackView() {
        mBackView.setVisibility(GONE);
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        boolean isAudio = bundle.getBoolean("isAudio", false);
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_TOPIC:
            case VideoSkin.SKIN_TYPE_VOD:
            case VideoSkin.SKIN_TYPE_TV:
                mChangeIconContainer.setVisibility(View.VISIBLE);
                mErrorRetryView.setVisibility(View.GONE);
                if (isAudio) {
                    mRetryTextView.setText(getResources().getString(R.string.load_audio_fail_retry));
                    mChangeView.setImageResource(R.drawable.icon_error_change_video);
                } else {
                    mRetryTextView.setText(getResources().getString(R.string.load_video_fail_retry));
                    mChangeView.setImageResource(R.drawable.icon_error_change_audio);
                }
                break;
            case VideoSkin.SKIN_TYPE_LOCAL:
                mChangeIconContainer.setVisibility(View.GONE);
                if (isAudio) {
                    mRetryTextView.setText(getResources().getString(R.string.load_audio_fail));
                } else {
                    mRetryTextView.setText(getResources().getString(R.string.load_video_fail));
                }
                mErrorRetryView.setVisibility(View.VISIBLE);
                break;
            case VideoSkin.SKIN_TYPE_FM:
                mChangeIconContainer.setVisibility(View.GONE);
                mErrorRetryView.setVisibility(View.VISIBLE);
                mRetryTextView.setText(getResources().getString(R.string.load_audio_fail));
                break;
            case VideoSkin.SKIN_TYPE_PIC:
                mChangeIconContainer.setVisibility(View.GONE);
                mRetryTextView.setVisibility(View.VISIBLE);
                mRetryTextView.setText(getResources().getString(R.string.load_video_fail));
                mErrorRetryView.setVisibility(View.VISIBLE);
                break;
        }

    }


}
