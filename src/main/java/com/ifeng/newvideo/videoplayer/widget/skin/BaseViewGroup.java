package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;

/**
 * Created by fanshell on 2016/7/25.
 */
public abstract class BaseViewGroup extends BaseView {

    public BaseViewGroup(Context context) {
        this(context, null);
    }

    public BaseViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    public void attachPlayController(IPlayController playerControl) {
        onAttachUIPlayControl(playerControl);
        super.attachPlayController(playerControl);
    }

    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        super.attachUIContext(uiPlayContext);
        onAttachUIPlayContext(uiPlayContext);
    }

    public abstract void onAttachUIPlayControl(IPlayController playerControl);

    public abstract void onAttachUIPlayContext(UIPlayContext playContext);


}
