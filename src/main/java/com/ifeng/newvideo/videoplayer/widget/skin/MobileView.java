package com.ifeng.newvideo.videoplayer.widget.skin;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;

/**
 * 移动网络提示
 * Created by fanshell on 2016/7/25.
 */
public class MobileView extends BaseView implements View.OnClickListener {

    public static final int DEFAULT_STATUS = 501;
    public static final int NON_ORDER = 502;

    private View defaultLayout, nonOrderLayout,mobileLayout;

    public MobileView(Context context) {
        super(context);
    }

    public MobileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MobileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mobileLayout = inflater.inflate(R.layout.default_video_mobile, this, true);
        defaultLayout =  mobileLayout.findViewById(R.id.common_video_mobile);
        nonOrderLayout = mobileLayout.findViewById(R.id.common_video_mobile_nonorder_layer);

        this.setVisibility(View.GONE);
        defaultLayout.findViewById(R.id.video_mobile_title_back).setOnClickListener(this);
        nonOrderLayout.findViewById(R.id.video_mobile_title_back).setOnClickListener(this);
    }


    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        super.attachUIContext(uiPlayContext);

    }

    @Override
    protected void initPlayer() {

    }

    public void setOnClickListener(View.OnClickListener listener) {
        findViewById(R.id.video_mobile_continue).setOnClickListener(listener);
        findViewById(R.id.video_mobile_nonorder_continue).setOnClickListener(listener);
        findViewById(R.id.video_mobile_to_order).setOnClickListener(listener);
    }

    public void showBackView() {
        this.findViewById(R.id.video_mobile_title_back).setVisibility(VISIBLE);
    }

    public void hideBackView() {
        this.findViewById(R.id.video_mobile_title_back).setVisibility(GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_mobile_title_back:
                if (ScreenUtils.isLand()) {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                } else {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                }
                break;
            default:
                break;
        }
    }


    public void showView(int status) {
        this.setVisibility(VISIBLE);
        switch (status) {
            case DEFAULT_STATUS:
                defaultLayout.setVisibility(VISIBLE);
                nonOrderLayout.setVisibility(GONE);
                break;
            case NON_ORDER:
                nonOrderLayout.setVisibility(VISIBLE);
                defaultLayout.setVisibility(GONE);
                break;
            default:
                break;
        }
    }

}
