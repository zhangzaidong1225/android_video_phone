package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ifeng.newvideo.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fanshell on 2016/8/9.
 */
public class VolumeView extends BaseView {
    private int maxWidth;
    private View mPercentView;
    private ImageView mOperationBg;
    private Logger logger = LoggerFactory.getLogger(VolumeView.class);

    public VolumeView(Context context) {
        super(context);
    }

    public VolumeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VolumeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_volumn, this, true);
        this.setVisibility(View.GONE);
        mPercentView = findViewById(R.id.operation_percent);
        mOperationBg = (ImageView) findViewById(R.id.operation_bg);

    }

    @Override
    protected void initPlayer() {

    }

    public void setPercent(int percent) {
        logger.debug("{}",this);
        if (percent <= 0) {
            mOperationBg.setImageResource(R.drawable.common_gesture_volume_mute);
        } else {
            mOperationBg.setImageResource(R.drawable.common_gesture_volume_nomal);
        }

        if (maxWidth == 0) {
            maxWidth = findViewById(R.id.operation_full).getMeasuredWidth();
        }
        ViewGroup.LayoutParams lp = mPercentView.getLayoutParams();
        lp.width = maxWidth * percent / 100;
        mPercentView.setLayoutParams(lp);
        this.setVisibility(View.VISIBLE);
    }
}
