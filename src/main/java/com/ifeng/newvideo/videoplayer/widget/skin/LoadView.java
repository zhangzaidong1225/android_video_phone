package com.ifeng.newvideo.videoplayer.widget.skin;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.widget.CustomNetworkImageView;
import com.ifeng.video.core.net.VolleyHelper;

/**
 * Created by fanshell on 2016/7/25.
 */
public class LoadView extends BaseView {


    private View mBackView;
    private TextView mTitleText;
    private View mBigPicLoadingView;
    private TextView mBigPicTitleText;
    private CustomNetworkImageView mBigPicImage;
    private ImageView mAnimLoading;

    public LoadView(Context context) {
        super(context);
    }

    public LoadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_loading, this, true);
        this.setVisibility(View.VISIBLE);
        mTitleText = (TextView) findViewById(R.id.loading_title_text);
        mBackView = findViewById(R.id.video_load_title_back);
        mBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_LOCAL) {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                    return;
                }
                if (ScreenUtils.isLand()) {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                } else {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                }
            }
        });

        mBigPicLoadingView = findViewById(R.id.default_big_pic_loading);
        mBigPicTitleText = (TextView) findViewById(R.id.tv_pic_title);
        mBigPicImage = (CustomNetworkImageView) findViewById(R.id.fm_item_img);
        mAnimLoading = (ImageView) findViewById(R.id.anim_loading);
    }


    public void updateText() {
        if (mUIPlayContext != null && !TextUtils.isEmpty(mUIPlayContext.title)) {
            mTitleText.setText(mUIPlayContext.title);
            mBigPicTitleText.setText(mUIPlayContext.title);
            if (!TextUtils.isEmpty(mUIPlayContext.image)) {
                mBigPicImage.setImageUrl(mUIPlayContext.image, VolleyHelper.getImageLoader());
                mBigPicImage.setDefaultImageResId(R.drawable.bg_default_pic);
                mBigPicImage.setErrorImageResId(R.drawable.bg_default_pic);
            }
        }
    }


    public void showBackView() {
        mBackView.setVisibility(VISIBLE);
    }

    public void hideBackView() {
        mBackView.setVisibility(GONE);
    }

    public void showBigPicLoading() {
        updateText();
        mBigPicLoadingView.setVisibility(VISIBLE);
        showLoading();

    }

    public void hideBigPicLoading() {
        mBigPicLoadingView.setVisibility(GONE);
    }

    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        super.attachUIContext(uiPlayContext);

    }

    @Override
    protected void initPlayer() {

    }
    public void showLoading() {
        AnimationDrawable animationDrawable;
        animationDrawable = (AnimationDrawable) mAnimLoading.getDrawable();
        animationDrawable.start();
    }


}
