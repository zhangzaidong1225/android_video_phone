package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.FragmentHomePage;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;

/**
 * Created by fanshell on 2016/7/25.
 */
public abstract class BaseView extends RelativeLayout {

    protected Context mContext;
    protected UIPlayContext mUIPlayContext;
    protected IPlayController mPlayerControl;

    protected @VideoSkin.SkinType int mSkinType;

    public BaseView(Context context) {
        this(context, null);
    }

    public BaseView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }
    public abstract void initView();


    public void attachPlayController(IPlayController playerControl) {
        //删除之前的监听
        if (mPlayerControl != null && this instanceof UIObserver) {
            mPlayerControl.removeUIObserver((UIObserver) this);
        }
        this.mPlayerControl = playerControl;
        initPlayer();
    }


    public void attachUIContext(UIPlayContext uiPlayContext) {
        this.mUIPlayContext = uiPlayContext;
        this.mSkinType = mUIPlayContext.skinType;
    }

    protected abstract void initPlayer();


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mPlayerControl != null && this instanceof UIObserver) {
            mPlayerControl.removeUIObserver((UIObserver) this);
        }
    }


    protected void setSkinType(int skinType) {
        this.mSkinType = skinType;
    }

    public String getCurPage() {
        String page = "";
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_TOPIC:
                page= PageIdConstants.PLAY_TOPIC_V;
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    page= PageIdConstants.PLAY_TOPIC_H;
                }
                break;
            case VideoSkin.SKIN_TYPE_VOD:
                page= PageIdConstants.PLAY_VIDEO_V;
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    page= PageIdConstants.PLAY_VIDEO_H;
                }
                break;
            case VideoSkin.SKIN_TYPE_TV:
                page= PageIdConstants.PLAY_LIVE_V;
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    page= PageIdConstants.PLAY_LIVE_H;
                }
                break;
            case VideoSkin.SKIN_TYPE_PIC:
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    page= PageIdConstants.PLAY_VIDEO_H;
                } else if(!TextUtils.isEmpty(FragmentHomePage.getCurChannelId())){
                    page=String.format(PageIdConstants.HOME_CHANNEL,FragmentHomePage.getCurChannelId());
                }else {
                    page = PageIdConstants.PAGE_HOME;
                }
                break;
            case VideoSkin.SKIN_TYPE_FM:
                page= PageIdConstants.PLAY_FM_V;
                if (mUIPlayContext.orientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                    page= PageIdConstants.PLAY_FM_H;
                }
                break;
            case VideoSkin.SKIN_TYPE_LOCAL:
                page= PageIdConstants.PLAY_CACHE;
                break;
        }
        return page;
    }

}
