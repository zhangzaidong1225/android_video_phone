package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.ifeng.newvideo.R;


/**
 * 画中画移动网络提醒
 * Created by ll on 2017/10/31.
 */
public class PipMobileView extends BaseView implements View.OnClickListener {

    public static final int DEFAULT_STATUS = 501;
    public static final int NON_ORDER = 502;

    private View nonOrderLayout,mobileLayout;
    private LinearLayout defaultLayout;

    public PipMobileView(Context context) {
        super(context);
    }

    public PipMobileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PipMobileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mobileLayout = inflater.inflate(R.layout.pip_mobile_view, this, true);
        defaultLayout =  (LinearLayout) findViewById(R.id.common_video_mobile);
        nonOrderLayout = mobileLayout.findViewById(R.id.common_video_mobile_nonorder_layer);

        this.setVisibility(View.GONE);
    }


    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        super.attachUIContext(uiPlayContext);

    }

    @Override
    protected void initPlayer() {

    }

    public void setOnClickListener(View.OnClickListener listener) {
        findViewById(R.id.video_mobile_continue).setOnClickListener(listener);
        findViewById(R.id.video_mobile_nonorder_continue).setOnClickListener(listener);
        findViewById(R.id.video_mobile_to_order).setOnClickListener(listener);
    }

    @Override
    public void onClick(View view) {
    }


    public void showView(int status) {
        this.setVisibility(VISIBLE);
        switch (status) {
            case DEFAULT_STATUS:
                defaultLayout.setVisibility(VISIBLE);
                nonOrderLayout.setVisibility(GONE);
                break;
            case NON_ORDER:
                nonOrderLayout.setVisibility(VISIBLE);
                defaultLayout.setVisibility(GONE);
                break;
            default:
                break;
        }
    }

}
