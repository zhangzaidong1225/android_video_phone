package com.ifeng.newvideo.videoplayer.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 底部操作栏的控件
 */
public class BottomLayout extends RelativeLayout {

    private static final Logger logger = LoggerFactory.getLogger(BottomLayout.class);

    private ImageView bottom_back;
    public ImageView bottom_play_audio;
    private ImageView bottom_share;
    public ImageView bottom_collect;
    public ImageView bottom_subscribe;
    public ImageView bottom_dlna;
    private RelativeLayout layout;
    private Animation popInAnim;
    private Animation popOutAnim;
    private boolean isHiden = false;
    public RelativeLayout bottom_comment_input;
    public View bottom_play_audio_rl;
    public View bottom_share_rl;
    public View bottom_collect_rl;

    public BottomLayout(Context context) {
        super(context);
    }

    public BottomLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        layout = (RelativeLayout) findViewById(R.id.video_detail_bottom_layout);
        bottom_comment_input = (RelativeLayout) findViewById(R.id.bottom_comment_input);
        bottom_back = (ImageView) findViewById(R.id.bottom_back_iv);
        bottom_play_audio = (ImageView) findViewById(R.id.bottom_play_download_iv);
        bottom_play_audio_rl = findViewById(R.id.bottom_play_download_rl);
        bottom_share = (ImageView) findViewById(R.id.bottom_share_iv);
        bottom_share_rl = findViewById(R.id.bottom_share_rl);
        bottom_collect = (ImageView) findViewById(R.id.bottom_collect_iv);
        bottom_collect_rl = findViewById(R.id.bottom_collect_rl);
        bottom_subscribe = (ImageView) findViewById(R.id.bottom_subscribe_iv);
        bottom_dlna = (ImageView) findViewById(R.id.bottom_dlna_iv);
    }

    public BottomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 布局进出动画
        popInAnim = AnimationUtils.loadAnimation(context, R.anim.play_page_bottom_layout_apper);
        popOutAnim = AnimationUtils.loadAnimation(context, R.anim.play_page_bottom_layout_hide);

    }

    public boolean isHiden() {
        return isHiden;
    }

    /**
     * 布局由下往上显示
     */
    public void showBottomLayout() {
        if (mIsShowBottomLayout != null && !mIsShowBottomLayout.isShowBottomLayout()) {
            return;
        }
        layout.startAnimation(popInAnim);
        layout.setVisibility(View.VISIBLE);
        isHiden = false;
    }

    private IsShowBottomLayout mIsShowBottomLayout;

    public IsShowBottomLayout getIsShowBottomLayout() {
        return mIsShowBottomLayout;
    }

    public void setIsShowBottomLayout(IsShowBottomLayout mIsShowBottomLayout) {
        this.mIsShowBottomLayout = mIsShowBottomLayout;
    }

    public interface IsShowBottomLayout {
        boolean isShowBottomLayout();
    }

    /**
     * 布局由上往下隐藏
     */
    public void hideBottomLayout() {
        layout.startAnimation(popOutAnim);
        layout.setVisibility(View.GONE);
        isHiden = true;
    }

    /**
     * 组件监听
     */
    public void setOnBottomItemClickListener(OnClickListener l) {
        bottom_back.setOnClickListener(l);
        bottom_play_audio_rl.setOnClickListener(l);
        bottom_share_rl.setOnClickListener(l);
        bottom_collect_rl.setOnClickListener(l);
        bottom_subscribe.setOnClickListener(l);
        bottom_dlna.setOnClickListener(l);
        bottom_comment_input.setOnClickListener(l);
    }

    /**
     * 设置订阅按钮是不显示
     * 保留老代码，怕以后改需求
     */
    public void hideShowSubscribe() {
        bottom_subscribe.setVisibility(View.INVISIBLE);
    }

}
