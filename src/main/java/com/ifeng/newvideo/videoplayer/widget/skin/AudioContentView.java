package com.ifeng.newvideo.videoplayer.widget.skin;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BitmapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fanshell on 2016/11/21.
 */
public class AudioContentView extends BaseView implements UIObserver {
    private Logger logger = LoggerFactory.getLogger(AudioContentView.class);
    private ImageView mBackGroundView;
    private ImageView mThumbView;
    private boolean mIsLive;

    public AudioContentView(Context context) {
        super(context);
    }

    public AudioContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AudioContentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_audio_content, this);
        mThumbView = (ImageView) this.findViewById(R.id.video_audio_thumb);
        mBackGroundView = (ImageView) this.findViewById(R.id.video_audio_bg);
    }

    @Override
    protected void initPlayer() {
        this.mPlayerControl.addUIObserver(this);
    }

    private String mImagePath = "";
    private volatile boolean isFailed;

    private ObjectAnimator mRotationAnimation;

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        //check audio
        if (bundle == null) {
            return;
        }
        boolean isAudio = bundle.getBoolean("isAudio", false);
        if (!isAudio) {
            return;
        }
        // set animation
        if (status == IPlayer.PlayerState.STATE_PREPARING) {
            mThumbView.setRotation(0);
        } else if (status == IPlayer.PlayerState.STATE_PAUSED) {
            //pause animation
            if (mRotationAnimation != null && mRotationAnimation.isRunning()) {
                mRotationAnimation.cancel();
            }

        } else if (status == IPlayer.PlayerState.STATE_PLAYING) {
            //start animation
            if (mRotationAnimation == null) {
                float currentRotation = mThumbView.getRotation();
                mRotationAnimation = ObjectAnimator.ofFloat(mThumbView, "rotation", currentRotation, 360 + currentRotation);
                mRotationAnimation.setInterpolator(new LinearInterpolator());
                mRotationAnimation.setDuration(20000);
                mRotationAnimation.setRepeatCount(-1);
                mRotationAnimation.start();
            } else if (!mRotationAnimation.isRunning()) {
                mRotationAnimation.start();
            }

        }
        //check path
        String path = getAudioImgPath();
        if (TextUtils.isEmpty(path)) {
            mThumbView.setImageResource(R.drawable.common_audio_foreground);
            mBackGroundView.setImageResource(R.drawable.common_audio_image_bg);
            return;
        }

        //load image
        if (!path.equalsIgnoreCase(mImagePath)) {
            loadImage();
        } else {
            if (isFailed) {
                loadImage();
            }
        }
    }

    private String getAudioImgPath() {
        if (mUIPlayContext == null) {
            return "";
        }
        mIsLive = mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TV;

        // 非直播
        if (!mIsLive && mUIPlayContext.videoItem != null) {
            return mUIPlayContext.videoItem.image;
        }
        // 直播
        if (mIsLive && mUIPlayContext.tvLiveInfo != null) {
            return mUIPlayContext.tvLiveInfo.getImg490_490();
        }
        return "";
    }

    private void loadImage() {
        VolleyHelper.getImageLoader().get(getAudioImgPath(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                isFailed = false;
                mImagePath = getAudioImgPath();
                if (response.getBitmap() == null) {
                    mThumbView.setImageResource(R.drawable.common_audio_foreground);
                    mBackGroundView.setImageResource(R.drawable.common_audio_image_bg);
                    return;
                }
                mThumbView.setImageBitmap(BitmapUtils.makeRoundCorner(response.getBitmap()));
                // 直播用一张固定的背景图，不用当前在播图片
                if (!mIsLive) {
                    mBackGroundView.setImageBitmap(BitmapUtils.gsBlurFilter(response.getBitmap()));
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                isFailed = true;
                mThumbView.setImageResource(R.drawable.common_audio_foreground);
                mBackGroundView.setImageResource(R.drawable.common_audio_image_bg);
            }
        });
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.clearAnimation();
        if (mRotationAnimation != null) {
            mRotationAnimation.cancel();
        }
    }
}

