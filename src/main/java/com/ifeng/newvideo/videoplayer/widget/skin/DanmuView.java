package com.ifeng.newvideo.videoplayer.widget.skin;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.bean.VideoDanmuItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import master.flame.danmaku.controller.DrawHandler;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.DanmakuTimer;
import master.flame.danmaku.danmaku.model.IDisplayer;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.SpannedCacheStuffer;
import master.flame.danmaku.ui.widget.DanmakuView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public class DanmuView extends BaseView implements UIObserver {

    private DanmakuContext danmakuContext;
    private DanmakuView danmakuView;
    private DanmakuParser mParser;

    private List<VideoDanmuItem> data = new ArrayList<>();
    private ConcurrentLinkedQueue<VideoDanmuItem> mQueue = null;

    private final int WHAT_DISPLAY_SINGLE_DANMAKU = 0x01;
    private final int MAX_DANMAKU_LINES = 3;
    private final int BASE_TIME = 400;
    private final int BASE_TIME_ADD = 100;
    private int currentVideoTime = 0; //当前播放位置
    //private int preVideoTime = -1; // 前一个视频的 offset
    private long showTime = 0L; // 显示时间
    //private static boolean showDanmaku = false;
    private boolean showEditView = true;

    public DanmuView(Context context) {
        super(context);
    }

    public DanmuView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DanmuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private Handler mDanmukuHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (WHAT_DISPLAY_SINGLE_DANMAKU == msg.what) {
                mDanmukuHandler.removeMessages(WHAT_DISPLAY_SINGLE_DANMAKU);
                displayDanmuku();
            }
        }
    };

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_danmu_layout, this, true);
        danmakuView = (DanmakuView) findViewById(R.id.video_danmu_view);
        mParser = new DanmakuParser();

        initDanmukuView();
        this.setVisibility(View.VISIBLE);
    }

    public void initDanmaView() {
        danmakuView = (DanmakuView) findViewById(R.id.video_danmu_view);
        initDanmukuView();
    }

    public void initDanmukuView() {
        mQueue = new ConcurrentLinkedQueue<>();

        Map<Integer, Integer> maxLinesPair = new HashMap<>();
        // 设置最大显示行数
        maxLinesPair.put(BaseDanmaku.TYPE_SCROLL_RL, MAX_DANMAKU_LINES);
        // 设置是否禁止重叠
        Map<Integer, Boolean> overlappingEnablePair = new HashMap<>();
        overlappingEnablePair.put(BaseDanmaku.TYPE_SCROLL_RL, true);
        overlappingEnablePair.put(BaseDanmaku.TYPE_FIX_TOP, true);

        danmakuContext = DanmakuContext.create();
        danmakuContext.setDanmakuStyle(IDisplayer.DANMAKU_STYLE_STROKEN, 8) //描边的厚度
                .setDuplicateMergingEnabled(false)
                .setScrollSpeedFactor(1.2f) //弹幕的速度。注意！此值越小，速度越快！值越大，速度越慢。// by phil
                .setScaleTextSize(1.2f)  //缩放的值
//                .setCacheStuffer(new BackgroundCacheStuffer(), null)
                .setMaximumLines(maxLinesPair)
                .preventOverlapping(overlappingEnablePair);
        onStart();
        danmakuView.enableDanmakuDrawingCache(true);
        danmakuView.showFPS(false);
        danmakuView.prepare(mParser, danmakuContext);
    }

    /**
     * 绘制背景(自定义弹幕样式)
     */
    private static class BackgroundCacheStuffer extends SpannedCacheStuffer {
        // 通过扩展SimpleTextCacheStuffer或SpannedCacheStuffer个性化你的弹幕样式
        final Paint paint = new Paint();

        @Override
        public void measure(BaseDanmaku danmaku, TextPaint paint, boolean fromWorkerThread) {
            danmaku.padding = 10;  // 在背景绘制模式下增加padding
            super.measure(danmaku, paint, fromWorkerThread);
        }

        @Override
        public void drawBackground(BaseDanmaku danmaku, Canvas canvas, float left, float top) {

            paint.setColor(0xFFF54343);
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            RectF rect = new RectF(left + 2, top + 2, left + danmaku.paintWidth - 2, top + danmaku.paintHeight - 2);
            canvas.drawRoundRect(rect, 20, 20, paint);
//            canvas.drawRect(left + 2, top + 2, left + danmaku.paintWidth - 2, top + danmaku.paintHeight - 2, paint);
        }

        @Override
        public void drawStroke(BaseDanmaku danmaku, String lineText, Canvas canvas, float left, float top, Paint paint) {
            // 禁用描边绘制
        }
    }

    public void addDataSource(List<VideoDanmuItem> list) {

        if (!ListUtils.isEmpty(list)) {
            if (null != mQueue && null != data) {
                mQueue.clear();
                data.clear();
                data.addAll(list);
                //针对播放页处理多余数据
                for (VideoDanmuItem item : list) {
                    if (str2int(item.getOffset()) >= currentVideoTime) {
                        mQueue.add(item);
                    }
                }
            }
        } else {
            if (null != mQueue && null != data) {
                mQueue.clear();
                data.clear();
            }
        }

        if (IfengApplication.danmaSwitchStatus) {
            resumeDanmaku();
        }
    }

    private void displayDanmuku() {
        boolean pause = false;
        if (null != danmakuView) {
            pause = danmakuView.isPaused();
        }

        if (null != mQueue && !mQueue.isEmpty() && !pause) {
            VideoDanmuItem item = mQueue.poll();
            if (item.isFromUser()) {
                addDanmaku(item.getContent(), item.getColor(), item.getFontSize(), item.getOffset(), true);
            } else {
                addDanmaku(item.getContent(), item.getColor(), item.getFontSize(), item.getOffset(), false);
            }
        }
        mDanmukuHandler.sendEmptyMessageDelayed(WHAT_DISPLAY_SINGLE_DANMAKU, (int) (Math.random() * BASE_TIME) + BASE_TIME_ADD);
    }

    public void addDanmaku(String content, String color, String fontSize, String time, boolean withBorder) {
        BaseDanmaku danmaku = danmakuContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL, danmakuContext);
        int tmp = str2int(time);
        if (null == danmaku || null == danmakuView || tmp < currentVideoTime) {
            return;
        }
        int realTime = str2int(time) * 1000;

        if (danmakuView.getCurrentTime() > realTime) {
            showTime = danmakuView.getCurrentTime() + 200;
        } else {
            showTime = danmakuView.getCurrentTime() + 200 + (realTime - currentVideoTime * 1000);
        }

        danmaku.text = content;
        danmaku.priority = 0;
        danmaku.padding = 5;
        danmaku.textSize = DisplayUtils.convertDipToPixel(16);
        danmaku.textColor = getRandomColor();
        danmaku.setTime(showTime);

        if (withBorder) {
            danmaku.borderColor = Color.RED;
        }

        try {
            danmakuView.addDanmaku(danmaku);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("danmu:exception", e.toString());
        }

        //Log.d("danmu", currentVideoTime + "--" + realTime + "--" + danmakuView.getCurrentTime() + "--" + showTime + "--" + content + "--" + mPlayerControl.getCurrentPosition());

    }

    //在点播页使用数据过滤
    public void currentVideoPosition(int videoTime) {
        addData(videoTime);
        this.currentVideoTime = videoTime;
    }

    public void setDanmakuSeekToPosition(long time) {
        if (null != danmakuView) {
            danmakuView.seekTo(time);
        }
    }

    public void addData(int timer) {

        if (timer <= currentVideoTime) {
            // 方案二
            if (null != danmakuView) {
                danmakuView.removeAllDanmakus(true);
                danmakuView.clear();
                mQueue.clear();
                if (!ListUtils.isEmpty(data)) {
                    for (VideoDanmuItem item : data) {
                        if (str2int(item.getOffset()) >= timer) {
                            mQueue.add(item);
                        }
                    }
                }

                resumeDanmaku();
            }
        } else {
            if (null != danmakuView) {
                danmakuView.removeAllDanmakus(true);
                danmakuView.clear();
                mQueue.clear();
                if (!ListUtils.isEmpty(data)) {
                    for (VideoDanmuItem item : data) {
                        if (str2int(item.getOffset()) >= timer) {
                            mQueue.add(item);
                        }
                    }
                }
                resumeDanmaku();
            }
        }
    }

    public void removeMSG() {
        mDanmukuHandler.removeMessages(WHAT_DISPLAY_SINGLE_DANMAKU);
    }


    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;

        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes);
    }

    public void sendTextMessage(String content) {
        if (!TextUtils.isEmpty(content)) {
            addDanmaku(content, true);
        }
    }

    public void addDanmaku(String content, boolean withBorder) {
        BaseDanmaku danmaku = danmakuContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL, danmakuContext);
        if (danmaku == null || danmakuView == null) {
            return;
        }

        danmaku.text = content;
        danmaku.priority = 1;
        danmaku.padding = 3;
        danmaku.textSize = DisplayUtils.convertDipToPixel(16);
        danmaku.textColor = Color.WHITE;
        long timer = danmakuView.getCurrentTime() + 1200;
        danmaku.setTime(timer);
        if (withBorder) {
            danmaku.borderColor = Color.RED;
        }
        VideoDanmuItem item = new VideoDanmuItem();
        item.setContent(content);
        item.setOffset(String.valueOf((int) (timer / 1000)));
        item.setFromUser(true);
        if (!ListUtils.isEmpty(data)) {
            for (int i = 0; i < data.size(); i++) {
                if ((int) (timer / 1000) > (str2int(data.get(i).getOffset()))) {
                    data.add(i + 1, item);
                    break;
                }
            }
        } else {
            data.add(item);
        }

        try {
            danmakuView.addDanmaku(danmaku);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.d("danmu", currentVideoTime + "--" + danmakuView.getCurrentTime() + "--" + timer + "--" + content + "--" + mPlayerControl.getCurrentPosition());

    }

    /**
     * 从一系列颜色中随机选择一种颜色
     *
     * @return
     */
    private int getRandomColor() {
//        int[] colors = {Color.RED, Color.YELLOW, Color.BLUE, Color.GREEN, Color.CYAN, Color.BLACK, Color.DKGRAY};
        int[] colors = {Color.RED, Color.YELLOW, Color.WHITE};
        int i = ((int) (Math.random() * 10)) % colors.length;
        return colors[i];
    }

    public DanmakuView getDanmakuView() {
        return danmakuView;
    }


    public void showView() {
        this.setVisibility(View.VISIBLE);
    }

    public void initEmptyDanmakuView() {
        if (null == danmakuView) {
            initDanmaView();
        }
    }

    public void showDanmukuView() {
        if (null != danmakuView) {
            danmakuView.show();
        }
    }

    public void setShowEditView(boolean isEditShow) {
        this.showEditView = isEditShow;
    }

    public boolean getShowEditView() {
        return showEditView;
    }

    public void hideView() {
        this.setVisibility(View.GONE);
    }

    public void hideDanmakuView() {
        if (null != danmakuView) {
            danmakuView.hide();
        }
    }


    public void onStart() {
        if (danmakuView != null) {
            danmakuView.setCallback(new DrawHandler.Callback() {
                @Override
                public void prepared() {
                    danmakuView.start();
                }

                @Override
                public void updateTimer(DanmakuTimer timer) {

                }

                @Override
                public void danmakuShown(BaseDanmaku danmaku) {
                }

                @Override
                public void drawingFinished() {

                }
            });
        }
    }

    public void onResume() {
        if (danmakuView != null && danmakuView.isPrepared()) {
            danmakuView.resume();
            resumeDanmaku();
        }
    }

    public void resumeDanmaku() {
        if (null != mQueue && !mQueue.isEmpty()) {
            mDanmukuHandler.sendEmptyMessageDelayed(WHAT_DISPLAY_SINGLE_DANMAKU, (int) (Math.random() * BASE_TIME) + BASE_TIME_ADD);
        }
    }


    public void onPause() {
        if (danmakuView != null && danmakuView.isPrepared()) {
            danmakuView.pause();
        }
    }

    public void onDestory() {
        if (danmakuView != null) {
            danmakuView.release();
            danmakuView = null;
        }
        showTime = 0L;
    }

    public void clearDanmaku() {
        if (mQueue != null && !mQueue.isEmpty()) {
            mQueue.clear();
        }

        if (data != null && !ListUtils.isEmpty(data)) {
            data.clear();
        }

        currentVideoTime = 0;
        if (null != danmakuView) {
            danmakuView.removeAllDanmakus(true);
            danmakuView.clearDanmakusOnScreen();
            danmakuView.clear();
        }

    }


    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            mPlayerControl.addUIObserver(this);
        }
    }

    private boolean isAudio;

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        isAudio = bundle.getBoolean("isAudio", false);
        if (isAudio) {
            hideView();
            if (null != danmakuView) {
                danmakuView.setVisibility(View.GONE);
            }
        } else {
            if (IfengApplication.danmaSwitchStatus) {
                showView();
                showDanmukuView();
            }
        }

        switch (status) {
            case STATE_PAUSED:
                if (null != danmakuView) {
                    danmakuView.pause();
                    mDanmukuHandler.removeMessages(WHAT_DISPLAY_SINGLE_DANMAKU);
                    mDanmukuHandler.removeMessages(WHAT_DISPLAY_SINGLE_DANMAKU);
                }
                break;
            case STATE_PREPARING:
                if (null != danmakuView) {
                    danmakuView.hide();
                    danmakuView.pause();
                }

                break;
            case STATE_PLAYING:
                if (null != danmakuView && IfengApplication.danmaSwitchStatus && mUIPlayContext.status != IPlayer.PlayerState.STATE_PAUSED) {
                    danmakuView.show();
                    danmakuView.resume();
                    resumeDanmaku();
                }
                break;
            case STATE_BUFFERING_START:
                if (null != danmakuView) {
                    danmakuView.hide();
                    danmakuView.pause();
                }
                break;
            case STATE_BUFFERING_END:
                if (null != danmakuView && IfengApplication.danmaSwitchStatus && mUIPlayContext.status != IPlayer.PlayerState.STATE_PAUSED) {
                    danmakuView.show();
                    danmakuView.resume();
                    resumeDanmaku();
                }
                break;
            case STATE_SEEK_END:
                //ijkplayer 没有这个状态
                break;
            case STATE_ERROR:

                if (null != danmakuView) {
                    danmakuView.hide();
                    danmakuView.pause();
                    mDanmukuHandler.removeMessages(WHAT_DISPLAY_SINGLE_DANMAKU);
                    mDanmukuHandler.removeMessages(WHAT_DISPLAY_SINGLE_DANMAKU);
                }
                break;
        }
    }

    private int str2int(String offset) {
        int timer = 0;
        if (!TextUtils.isEmpty(offset)) {
            try {
                timer = Integer.parseInt(offset);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return timer;
        }
        return timer;
    }
}
