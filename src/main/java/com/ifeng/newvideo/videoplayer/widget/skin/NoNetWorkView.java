package com.ifeng.newvideo.videoplayer.widget.skin;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.videoplayer.player.IPlayer;

/**
 * Created by fanshell on 2016/7/25.
 */
public class NoNetWorkView extends BaseView {

    private LinearLayout mRetryView;
    private View mBackView;
    private View.OnClickListener mOnClickListener;

    public NoNetWorkView(Context context) {
        super(context);
    }

    public NoNetWorkView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoNetWorkView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_no_network, this, true);
        this.setVisibility(View.GONE);

        mRetryView = (LinearLayout) findViewById(R.id.ll_video_no_network_retry);
        mBackView = findViewById(R.id.video_title_back);
        mBackView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ScreenUtils.isLand()) {
                    mPlayerControl.setOrientation(IPlayer.PlayerState.ORIENTATION_PORTRAIT);
                } else {
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                    }
                }
            }
        });
    }

    @Override
    protected void initPlayer() {

    }

    public void showBackView() {
        mBackView.setVisibility(VISIBLE);
    }

    public void hideBackView() {
        mBackView.setVisibility(GONE);
    }

    public void setNoNetWorkClick(View.OnClickListener l) {
        this.mOnClickListener = l;
        mRetryView.setOnClickListener(mOnClickListener);
    }


}
