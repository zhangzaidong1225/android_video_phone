package com.ifeng.newvideo.videoplayer.widget.skin;

import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.bean.BigAdItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.VideoAdInfoModel;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/7/26.
 * 播放器皮肤相关的数据
 */
public class UIPlayContext {
    public UIPlayContext() {

    }

    public int skinType = VideoSkin.SKIN_TYPE_VOD;
    public IPlayer.PlayerState orientation = IPlayer.PlayerState.ORIENTATION_PORTRAIT;
    public String streamType = "高清";
    public String videoType = "video";
    public IPlayer.PlayerState status = IPlayer.PlayerState.STATE_IDLE;
    public String title = "";

    public int widthRadio = 16;
    public int heightRadio = 9;

    public VideoItem videoItem;
    /**
     * 大图admodel
     */
    public BigAdItem adItem;

    //所有视频的播放列表
    public List<VideoItem> VideoItemList = new ArrayList<>();

    public TVLiveInfo tvLiveInfo;
    //大图
    public List<ChannelBean.VideoFilesBean> videoFilesBeanList;

    /**
     * channel  进入视频列表页  可用于统计、获取广告数据等
     */
    public String channelId = "";

    /**
     * 是否为相关，用于控制统计V中的echid上报
     * 点播播放页，播放相关列表视频和收藏时，上报v和store中的echid应该为空
     */
    public boolean isRelate = false;

    /**
     * 广告贴片请求参数，首次打开为1，联播为0
     */
    public int mOpenFirst = 1;
    public int vodPlayCount;
    public int topicPlayCount;
    /**
     * 广告贴片请求参数
     */
    public String mFromForAd;
    /**
     * 广告总长度
     */
    public int adVideoTotalLength;

    // 是否专题
    public boolean isTopic = false;

    public boolean isAdvert;

    /**
     * 是否来自缓存，用于统计
     */
    public boolean isFromCache;
    /**
     * 是否来自推送
     */
    public boolean isFromPush;

    public String image;

    /**
     * 广告前贴model
     */
    public VideoAdInfoModel videoAdModel;

    /**
     * 流切换时,是否上报V
     */
    public boolean isChangeStream;

    /**
     * 大图在模竖屏切换时,是否上报V
     */
    public boolean isBigReportVideo;

    /**
     * 1、剧集的不允许跳过
     * 2、每个自然日，用户观看一定数量后允许跳过前贴广告
     */
    public boolean canSkipAd() {
        return !videoItem.isColumn() && SharePreUtils.getInstance().getAdPlayTimes() <= 0;
    }
}
