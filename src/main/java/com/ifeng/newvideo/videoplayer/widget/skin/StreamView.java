package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/7/25.
 */
public class StreamView extends BaseView implements View.OnClickListener, UIObserver {

    private Logger logger = LoggerFactory.getLogger(StreamView.class);
    private TextView mAutoTextView;
    private TextView mSuperTextView;
    private TextView mHighTextView;
    private TextView mStandardTextView;
    private TextView mLowTextView;
    private List<String> mStreams;
    private List<View> mStreamView;

    private int mCurrentPosition;


    /**
     * 用于卡顿
     */
    private int bufferCount;
    private long millions;
    private long BUFFER_MILLIONS_DISTANCE = 3 * DateUtils.SECOND_IN_MILLIS;
    private int BUFFER_TIMES = 3;

    public StreamView(Context context) {
        super(context);
    }

    public StreamView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StreamView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.default_video_stream, this, true);
        this.setVisibility(View.GONE);
        mAutoTextView = (TextView) findViewById(R.id.video_stream_auto);
        mSuperTextView = (TextView) findViewById(R.id.video_stream_supper);
        mHighTextView = (TextView) findViewById(R.id.video_stream_high);
        mStandardTextView = (TextView) findViewById(R.id.video_stream_standard);
        mLowTextView = (TextView) findViewById(R.id.video_stream_low);

        mAutoTextView.setOnClickListener(this);
        mSuperTextView.setOnClickListener(this);
        mHighTextView.setOnClickListener(this);
        mStandardTextView.setOnClickListener(this);
        mLowTextView.setOnClickListener(this);

    }


    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mPlayerControl != null) {
            this.mPlayerControl.removeUIObserver(this);
        }

    }

    @Override
    public void onClick(View view) {
        resetStatus();

        switch (view.getId()) {
            case R.id.video_stream_auto:
            case R.id.video_stream_supper:
            case R.id.video_stream_high:
            case R.id.video_stream_standard:
            case R.id.video_stream_low:
                TextView streamView = (TextView) view;
                String stream = streamView.getText().toString();
                logger.debug("stream:{}  streamValue{}:  mUIPlayContext.streamType:{} ",
                        stream, StreamUtils.getStreamValue(stream), mUIPlayContext.streamType);
                if (!mUIPlayContext.streamType.equals(stream)) {
                    mUIPlayContext.streamType = stream;
                    int streamValue = StreamUtils.getStreamValue(stream.trim());
                    switch (mUIPlayContext.skinType) {
                        case VideoSkin.SKIN_TYPE_PIC:
                        case VideoSkin.SKIN_TYPE_TOPIC:
                        case VideoSkin.SKIN_TYPE_VOD:
                            SharePreUtils.getInstance().setVodCurrentStream(streamValue);
                            logger.debug("stream:{}, streamValue:{}", stream, streamValue);
                            SharePreUtils.getInstance().setHasUserSelectedVodStream(true);
                            break;

                        case VideoSkin.SKIN_TYPE_TV:
                            SharePreUtils.getInstance().setLiveCurrentStream(streamValue);
                            break;
                        default:
                            break;

                    }
                    mPlayerControl.setStream(stream);
                    PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_DEF_ITEM, null, getCurPage());
                }

                break;
            default:
                return;
        }

        this.setVisibility(GONE);
    }

    private void resetStatus() {
        // 确定当前码流
        int len = mStreams.size();
        mCurrentPosition = 0;
        for (int i = 0; i < len; i++) {
            if (mStreams.get(i).equals(mUIPlayContext.streamType)) {
                mCurrentPosition = i;
                break;
            }
        }
        // 重置未选中
        int streamViewSize = mStreamView.size();
        for (int i = 0; i < streamViewSize; i++) {
            mStreamView.get(i).setSelected(false);
        }
        // 选中当前码流
        if (streamViewSize > mCurrentPosition) {
            mStreamView.get(mCurrentPosition).setSelected(true);
        }

    }

    public void showView() {
        if (!ScreenUtils.isLand()) {
            return;
        }
        resetStatus();
        this.setVisibility(View.VISIBLE);

    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (bundle != null) {
            boolean isAudio = bundle.getBoolean("isAudio");
            if (isAudio) {
                return;
            }
        }
        switch (status) {
            case STATE_PREPARING:
                bufferCount = 0;
                updateStream();
                break;
            case STATE_BUFFERING_START:
                millions = System.currentTimeMillis();
                logger.debug("start buffer");
                break;
            case STATE_BUFFERING_END:
                long currentMillions = System.currentTimeMillis();
                if (currentMillions - millions > BUFFER_MILLIONS_DISTANCE && !mUIPlayContext.isAdvert) {
                    logger.debug("start buffer end");
                    bufferCount++;
                }
                if (bufferCount == BUFFER_TIMES) {
                    if (!isLowStream()) {
                        ToastUtils.getInstance().showShortToast("当前网络缓慢，建议切换至低清晰度观看");
                    }
                    bufferCount = 0;
                }

                break;

            case ORIENTATION_PORTRAIT:
                if (this.isShown()) {
                    this.setVisibility(View.GONE);
                }
                break;

            default:
                break;
        }
    }

    private int size;

    @Override
    public void attachUIContext(UIPlayContext uiPlayContext) {
        super.attachUIContext(uiPlayContext);
        mStreams = new ArrayList<>();
        mStreamView = new ArrayList<>();

    }

    private void addStreamView() {

        if (mStreams.contains(mContext.getString(R.string.common_video_auto))) {
            mStreamView.add(mAutoTextView);
        }

        if (mStreams.contains(mContext.getString(R.string.common_video_supper))) {
            mStreamView.add(mSuperTextView);
        }
        if (mStreams.contains(mContext.getString(R.string.common_video_high))) {
            mStreamView.add(mHighTextView);
        }

        if (mStreams.contains(mContext.getString(R.string.common_video_mid))) {
            mStreamView.add(mStandardTextView);
        }

        if (mStreams.contains(mContext.getString(R.string.common_video_low))) {
            mStreamView.add(mLowTextView);
        }
        hideAllStreamView();
        size = mStreamView.size();
        for (int i = 0; i < size; i++) {
            mStreamView.get(i).setVisibility(View.VISIBLE);
        }

    }

    private void hideAllStreamView() {
        mAutoTextView.setVisibility(View.GONE);
        mSuperTextView.setVisibility(View.GONE);
        mHighTextView.setVisibility(View.GONE);
        mStandardTextView.setVisibility(View.GONE);
        mLowTextView.setVisibility(View.GONE);
    }

    /**
     * 设置选取码流的View
     */
    private void updateStream() {
        this.mStreams.clear();
        mStreamView.clear();
        mStreams.addAll(getStreams());
        addStreamView();
        resetStatus();
    }

    /**
     * @return 表示当前选择是否低清晰度
     */
    private boolean isLowStream() {
        return mCurrentPosition == mStreamView.size() - 1;
    }


    /**
     * 用于设置所有支持的流
     */
    private List<String> getStreams() {
        //直播的不处理,因为直播自己处理了
        if (mSkinType == VideoSkin.SKIN_TYPE_TV) {
            return getTVLiveStream();
        }
        List<FileType> videoFiles = new ArrayList<>();
        videoFiles.clear();
        if (mSkinType == VideoSkin.SKIN_TYPE_PIC) {
            if (mUIPlayContext.videoFilesBeanList != null) {
                List<ChannelBean.VideoFilesBean> beans = mUIPlayContext.videoFilesBeanList;
                for (ChannelBean.VideoFilesBean bean : beans) {
                    FileType type = new FileType();
                    type.useType = bean.getUseType();
                    type.mediaUrl = bean.getMediaUrl();
                    type.filesize = bean.getFilesize();
                    type.spliteTime = bean.getSpliteTime();
                    videoFiles.add(type);
                }
            }
        } else if (mSkinType == VideoSkin.SKIN_TYPE_TOPIC || mSkinType == VideoSkin.SKIN_TYPE_VOD) {
            if (mUIPlayContext.videoItem != null) {
                videoFiles.addAll(mUIPlayContext.videoItem.videoFiles);
            }
        }
        List<String> streams = StreamUtils.getAllStream(videoFiles);
        if (!streams.contains(mUIPlayContext.streamType)) {
            mUIPlayContext.streamType = StreamUtils.getValidateStream(videoFiles);
        }

        return streams;

    }

    /**
     * 获取码流类型
     */
    private List<String> getTVLiveStream() {
        TVLiveInfo tvLiveInfo = mUIPlayContext.tvLiveInfo;
        List<String> streams = new ArrayList<>();
        if (!TextUtils.isEmpty(tvLiveInfo.getVideo())) {
            streams.add(mContext.getString(R.string.common_video_auto));
        }
        if (!TextUtils.isEmpty(tvLiveInfo.getVideoH())) {
            streams.add(mContext.getString(R.string.common_video_high));
        }
        if (!TextUtils.isEmpty(tvLiveInfo.getVideoM())) {
            streams.add(mContext.getString(R.string.common_video_mid));
        }
        if (!TextUtils.isEmpty(tvLiveInfo.getVideoL())) {
            streams.add(mContext.getString(R.string.common_video_low));
        }
        return streams;
    }

}
