package com.ifeng.newvideo.videoplayer.widget.skin;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;

/**
 * Created by fanshell on 2016/7/25.
 */
public class PlayButton extends BaseView implements UIObserver, View.OnClickListener {

    private ImageView mPlayView;

    public PlayButton(Context context) {
        this(context, null);
    }

    public PlayButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView() {
        LayoutInflater.from(mContext)
                .inflate(R.layout.default_video_play, this);
        mPlayView = (ImageView) this.findViewById(R.id.btn_play_pause);
        mPlayView.setOnClickListener(this);
        this.setVisibility(View.GONE);
    }

    @Override
    protected void initPlayer() {
        if (mPlayerControl != null) {
            this.mPlayerControl.addUIObserver(this);
        }
    }

    @Override
    public void onClick(View view) {
        if (mPlayerControl != null && mPlayerControl.isPlaying()) {
            PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, false, getCurPage());
            mPlayerControl.pause();
            if (mPlayListener != null) {
                mPlayListener.onPausePlayButton();
            }
            mUIPlayContext.status = IPlayer.PlayerState.STATE_PAUSED;
        } else if (mPlayerControl != null) {
            PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_PAUSE, true, getCurPage());
            mPlayerControl.start();
            if (mPlayListener != null) {
                mPlayListener.onPlayButton();
            }
        }
    }

    public void showPlayButton() {
        if (mPlayerControl != null && mPlayerControl.isPlaying()) {
            mPlayView.setImageResource(R.drawable.btn_pause);
        } else {
            mPlayView.setImageResource(R.drawable.btn_play);
        }

        this.setVisibility(View.VISIBLE);

    }

    public void hiddenPlayButton() {
        this.setVisibility(View.GONE);
    }

    private OnPlayOrPauseListener mPlayListener;
    public interface OnPlayOrPauseListener {
        void onPausePlayButton();

        void onPlayButton();
    }

    public void setPlayOrPauseListener (OnPlayOrPauseListener listener) {
        this.mPlayListener = listener;
    }

    @Override
    public void update(IPlayer.PlayerState status, Bundle bundle) {
        if (status == IPlayer.PlayerState.STATE_PAUSED) {
            mUIPlayContext.status = IPlayer.PlayerState.STATE_PAUSED;
            mPlayView.setImageResource(R.drawable.btn_play);
        }
        if (status == IPlayer.PlayerState.STATE_PLAYING) {
            mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYING;
            mPlayView.setImageResource(R.drawable.btn_pause);
        }

        if (status == IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED) {
            mUIPlayContext.status = IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED;
        }
        if (status == IPlayer.PlayerState.STATE_IDLE) {
            mUIPlayContext.status = IPlayer.PlayerState.STATE_IDLE;
        }
    }
}
