package com.ifeng.newvideo.videoplayer.player;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;
import com.ifeng.video.core.BuildConfig;

public class IfengSurfaceView extends SurfaceView {


    private static final String TAG = "IfengSurfaceView";

    public IfengSurfaceView(Context context) {
        this(context, null);
    }

    public IfengSurfaceView(Context context, AttributeSet attrs) {
        this(context, null, 0);
    }

    public IfengSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void resetSurface() {

        Canvas canvas = null;
        try {
            canvas = getHolder().lockCanvas();
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            canvas.drawColor(Color.BLACK);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.w(TAG, "resetSurface clear falied");
            }
        } finally {
            if (canvas != null) {
                getHolder().unlockCanvasAndPost(canvas);
            }
        }
    }


}
