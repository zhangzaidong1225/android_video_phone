package com.ifeng.newvideo.videoplayer.player.playController;

import android.os.Bundle;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.player.history.HistoryManager;
import com.ifeng.newvideo.videoplayer.widget.skin.OrientationSensorUtils;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.dao.db.model.HistoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/11/21.
 */
public abstract class BasePlayController implements IPlayController {

    protected UIPlayContext mUIPlayContext;
    protected VideoSkin mVideoSkin;
    protected Bundle mBundle = new Bundle();

    protected List<UIObserver> mObservers = new ArrayList<>();
    protected IPlayer.PlayerState mCurrentState = IPlayer.PlayerState.STATE_IDLE;
    protected OnAutoPlayListener mAutoPlayListener;
    protected OnPlayCompleteListener mOnPlayCompleteListener;

    public static int PIP_VIDEO_PLAYER_TYPE_IJK = 0;
    public static int PIP_VIDEO_PLAYER_TYPE_IFENG = 1;

    public void setStream(String stream) {
        mBundle.clear();
        IPlayer.PlayerState oldState = mCurrentState;
        mCurrentState = IPlayer.PlayerState.STREAM_CHANGE;
        mBundle.putString(StreamUtils.KEY_STREAM, stream);
        notifyObserver();
        mCurrentState = oldState;
    }

    public void addUIObserver(UIObserver observer) {
        if (observer != null && !mObservers.contains(observer))
            synchronized (mObservers) {
                mObservers.add(observer);
            }
    }

    public void removeUIObserver(UIObserver observer) {
        if (observer != null && mObservers != null && mObservers.contains(observer))
            synchronized (mObservers) {
                mObservers.remove(observer);
            }
    }


    @Override
    public void setOrientation(IPlayer.PlayerState orientation) {

        mBundle.clear();
        mBundle.putSerializable(OrientationSensorUtils.KEY_ORIENTATION, orientation);
        IPlayer.PlayerState oldState = mCurrentState;
        mCurrentState = orientation;
        notifyObserver();
        mCurrentState = oldState;
    }

    protected void notifyObserver() {
        if (!mObservers.isEmpty())
            synchronized (mObservers) {
                for (UIObserver observer : mObservers) {
                    observer.update(mCurrentState, mBundle);
                }
            }
    }

    public void sendScreenOffStatistics() {
    }

    @Override
    public void setFromHistory(boolean fromHistory) {
    }


    @Override
    public void setOnPlayCompleteListener(OnPlayCompleteListener listener) {
        this.mOnPlayCompleteListener = listener;
    }

    @Override
    public void setOnAutoPlayListener(OnAutoPlayListener listener) {
        this.mAutoPlayListener = listener;
    }

    @Override
    public void skipAdvert() {
    }

    protected long lastPosition;

    /**
     * 设置看过的位置
     */
    public void setBookMark(long bookMark) {
        lastPosition = bookMark;
    }


    @Override
    public void onDestroy() {
        this.mAutoPlayListener = null;
        this.mOnPlayCompleteListener = null;
        if (this.mObservers != null) {
            this.mObservers.clear();
        }
    }

    Logger logger = LoggerFactory.getLogger(BasePlayController.class);

    /**
     * 实现看过保存的类
     */
    protected HistoryManager mHistoryManger;

    /**
     * 保存之前VideoItem
     */
    protected VideoItem mPreViewVideoItem;

    /**
     * 保存看过记录
     *
     * @param isComplete 是否播放完成
     */
    protected void saveHistory(boolean isComplete) {
        saveHistory(isComplete, lastPosition);
    }

    /**
     * 保存看过记录
     *
     * @param isComplete 是否播放完成
     * @param position   需要保存的位置
     */
    protected void saveHistory(boolean isComplete, final long position) {
        if (mUIPlayContext.isAdvert) {
            return;
        }
        if (!isComplete && position < 1000) {// 播放时长小于1秒不保存观看记录
            return;
        }
        mHistoryManger.saveHistory(isComplete, mPreViewVideoItem, getResourceType(), getVideoType(), position);
    }

    /**
     * 得到资源类型，视频还是音频
     */
    private String getResourceType() {
        switch (mUIPlayContext.skinType) {
            case VideoSkin.SKIN_TYPE_FM:
                return HistoryModel.RESOURCE_AUDIO;
            default:
                return HistoryModel.RESOURCE_VIDEO;
        }
    }

    /**
     * 得到用用保存到看过的类型
     *
     * @return
     */
    private String getVideoType() {
        switch (mUIPlayContext.skinType) {
            case VideoSkin.SKIN_TYPE_TOPIC:
                return HistoryModel.TYPE_TOPIC;
            case VideoSkin.SKIN_TYPE_FM:
                return HistoryModel.HISTORY_FM_TYPE;
            case VideoSkin.SKIN_TYPE_VOD:
            case VideoSkin.SKIN_TYPE_PIC:
            case VideoSkin.SKIN_TYPE_LOCAL:
            default:
                return HistoryModel.TYPE_VOD;
        }
    }
}
