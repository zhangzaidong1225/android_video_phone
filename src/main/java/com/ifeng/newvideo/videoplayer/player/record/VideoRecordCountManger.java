package com.ifeng.newvideo.videoplayer.player.record;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.format.DateUtils;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.LiveRecord;
import com.ifeng.newvideo.statistics.domains.VideoRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.statistics.smart.domains.UserOperator;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.utils.IPushUtils;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.smart.VideoSmartManager;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.player.ChoosePlayerUtils;

/**
 * Created by fanshell on 2016/10/17.
 */
public class VideoRecordCountManger {

    /**
     * 统计相关的变量
     */
    private VideoRecord mVideoRecord;
    private UserOperator mUserOperator;

    /**
     * BN 用于统计，卡顿每两秒，计一次数
     */
    private long BN_INTERVAL = 2 * DateUtils.SECOND_IN_MILLIS;

    /**
     * 卡顿次数
     */
    private final Handler mBNHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (mVideoRecord != null) {
                mVideoRecord.statisticBN();
            }
        }
    };

    private UIPlayContext mUIPlayContext;
    private TVLiveInfo mLiveInfo;


    /**
     * 用户行为视频播放完成统计
     */
    private VideoSmartManager mVideoSmartManager;

    public void insertStatistics(IPlayer.PlayerState playState, Context context) {
        switch (playState) {
            case STATE_PREPARING:
                initRecord(context);
                break;
            case STATE_BUFFERING_START:
                bufferStartRecord();
                break;
            case STATE_BUFFERING_END:
                bufferEndRecord();
                break;
            case STATE_PLAYING:
                playingRecord();
                break;
            case STATE_PAUSED:
                pauseRecord();
                break;
            case STATE_PLAYBACK_COMPLETED:
                completeRecord();
                break;
            case STATE_IDLE:
                idleRecord();
                break;
            case STATE_ERROR:
                errorRecord();
                break;
            default:
                break;
        }
    }

    public VideoRecordCountManger(UIPlayContext mUIPlayContext) {
        this.mUIPlayContext = mUIPlayContext;
        mVideoSmartManager = new VideoSmartManager();

    }

    private void initRecord(Context context) {
        if (mUIPlayContext == null || mUIPlayContext.isAdvert || mUIPlayContext.isChangeStream) { // 广告不报V
            return;
        }

        mUserOperator = new UserOperator(context);
        String echId = mUIPlayContext.channelId;
        String wmId = "";
        String wmName = "";
        boolean useIJKPlayer = ChoosePlayerUtils.useIJKPlayer(context);
        switch (mUIPlayContext.skinType) {
            case VideoSkin.SKIN_TYPE_TV:
                mLiveInfo = mUIPlayContext.tvLiveInfo;
                if (mLiveInfo == null) {
                    return;
                }
                mVideoRecord = new LiveRecord(mLiveInfo.getTitle(), getLiveRecordTitle(mLiveInfo), "",
                        VodRecord.P_TYPE_LV, "", (LiveRecord) mVideoRecord, echId, useIJKPlayer, wmId, wmName, "", "");
                break;

            case VideoSkin.SKIN_TYPE_VOD:
                VideoItem videoItem = mUIPlayContext.videoItem;
                if (videoItem == null) {
                    return;
                }
                if (mUIPlayContext.isRelate) {
                    echId = "";
                }
                if (videoItem.weMedia != null) {
                    wmId = videoItem.weMedia.id;
                    wmName = videoItem.weMedia.name;
                }
                mVideoRecord = new VodRecord(videoItem.guid, videoItem.title, videoItem.searchPath, echId, wmId, wmName,
                        String.valueOf(videoItem.duration), VodRecord.P_TYPE_RV, getOp(), videoItem.cpName,
                        (VodRecord) mVideoRecord, VodRecord.V_TAG_RELATE, useIJKPlayer, videoItem.simId, "");
                break;
            case VideoSkin.SKIN_TYPE_LOCAL:
                VideoItem localItem = mUIPlayContext.videoItem;
                if (localItem == null) {
                    return;
                }
                if (mUIPlayContext.isRelate) {
                    echId = "";
                }
                if (localItem.weMedia != null) {
                    wmId = localItem.weMedia.id;
                    wmName = localItem.weMedia.name;
                }
                mVideoRecord = new VodRecord(localItem.guid, localItem.title, localItem.searchPath, echId, wmId, wmName,
                        String.valueOf(localItem.duration), VodRecord.P_TYPE_RV, getOp(), localItem.cpName,
                        (VodRecord) mVideoRecord, VodRecord.V_TAG_EMPTY, useIJKPlayer, localItem.simId, "");
                break;

            case VideoSkin.SKIN_TYPE_PIC:
                VideoItem picItem = mUIPlayContext.videoItem;
                if (picItem == null) {
                    return;
                }
                if (picItem.weMedia != null) {
                    wmId = picItem.weMedia.id;
                    wmName = picItem.weMedia.name;
                }
                mVideoRecord = new VodRecord(picItem.guid, picItem.title, picItem.searchPath, echId, wmId, wmName,
                        String.valueOf(picItem.duration), VodRecord.P_TYPE_RV, getOp(), picItem.cpName,
                        (VodRecord) mVideoRecord, VodRecord.V_TAG_EMPTY, useIJKPlayer, picItem.simId, "");
                mUserOperator.setTitle(picItem.title);
                break;

            case VideoSkin.SKIN_TYPE_TOPIC:
                VideoItem topicItem = mUIPlayContext.videoItem;
                if (topicItem == null) {
                    return;
                }
                if (topicItem.weMedia != null) {
                    wmId = topicItem.weMedia.id;
                    wmName = topicItem.weMedia.name;

                }
                mVideoRecord = new VodRecord(topicItem.guid, topicItem.name, topicItem.topicType, topicItem.topicId,
                        wmId, wmName, String.valueOf(topicItem.duration), VodRecord.P_TYPE_RV, getOp(),
                        topicItem.cpName, (VodRecord) mVideoRecord, echId, VodRecord.V_TAG_TOPIC, useIJKPlayer, topicItem.simId, "");

                break;
            default:
                break;
        }

        if (mUIPlayContext.videoItem != null) {
            IPushUtils.saveSearchPath(mUIPlayContext.videoItem.searchPath, mUIPlayContext.isFromPush);
        }
    }

    private void bufferEndRecord() {
        if (mVideoRecord != null) {
            mBNHandler.removeCallbacksAndMessages(null);
            mVideoRecord.startPlayTime();
        }
    }


    private void bufferStartRecord() {
        if (mVideoRecord != null) {
            mBNHandler.removeCallbacksAndMessages(null);
            mBNHandler.sendEmptyMessageDelayed(0, BN_INTERVAL);
            mVideoRecord.stopPlayTime();
            setRealPlayDuration();

        }
    }

    private void completeRecord() {
        if (mVideoRecord != null) {
            mVideoRecord.stopPlayTime();
            setRealPlayDuration();
            if (mVideoRecord instanceof LiveRecord) {
                ((LiveRecord) mVideoRecord).setLiveTitle(getLiveRecordTitle(mUIPlayContext.tvLiveInfo));
                ((LiveRecord) mVideoRecord).setAudio(false);
                if (!mUIPlayContext.isChangeStream) {
                    CustomerStatistics.sendLiveRecord((LiveRecord) mVideoRecord);
                }
            } else if (mVideoRecord instanceof VodRecord) {
                if (!mUIPlayContext.isChangeStream) {
                    CustomerStatistics.sendVODRecord((VodRecord) mVideoRecord);
                }
            }
            sendUserAction();
        }
    }


    private void pauseRecord() {
        if (mVideoRecord != null) {
            mVideoRecord.stopPlayTime();
            setRealPlayDuration();
        }
    }

    private void playingRecord() {
        if (mVideoRecord != null) {
            mVideoRecord.setSuccessPlayFirstFrame(true);
            if (mVideoRecord instanceof LiveRecord) {
                ((LiveRecord) mVideoRecord).setLiveTitle(getLiveRecordTitle(mLiveInfo));
                mVideoRecord.startPlayTime();
                ((LiveRecord) mVideoRecord).stopPrepareTime();
            } else if (mVideoRecord instanceof VodRecord) {
                ((VodRecord) mVideoRecord).stopPrepareTime();
                mVideoRecord.startPlayTime();

            }
        }
    }

    private void errorRecord() {
        if (mVideoRecord != null) {
            if (mVideoRecord instanceof LiveRecord) {
                mVideoRecord.stopPlayTime();
                setRealPlayDuration();
                ((LiveRecord) mVideoRecord).setLiveTitle(getLiveRecordTitle(mLiveInfo));
                LiveRecord temLiveRecord = (LiveRecord) this.mVideoRecord;
                temLiveRecord.setAudio(false);
                if (this.mVideoRecord.isSuccessPlayFirstFrame()) {
                    temLiveRecord.setErr(true);
                }
                temLiveRecord.stopPrepareTime();
                if (!mUIPlayContext.isChangeStream) {
                    CustomerStatistics.sendLiveRecord(temLiveRecord);
                }
            } else if (mVideoRecord instanceof VodRecord) {
                ((VodRecord) mVideoRecord).stopPrepareTime();
                mVideoRecord.stopPlayTime();
                setRealPlayDuration();
                ((VodRecord) mVideoRecord).setAudio(false);
                realPlayDuration = mVideoRecord.getTotalPlayTime();
                if (!mUIPlayContext.isChangeStream) {
                    CustomerStatistics.sendVODRecord((VodRecord) mVideoRecord);
                }
            }
            sendUserAction();
        }
    }

    private void idleRecord() {
        if (mVideoRecord != null) {
            if (mVideoRecord instanceof LiveRecord) {
                mVideoRecord.stopPlayTime();
                setRealPlayDuration();
                ((LiveRecord) mVideoRecord).setLiveTitle(getLiveRecordTitle(mLiveInfo));
                LiveRecord temLiveRecord = (LiveRecord) this.mVideoRecord;
                temLiveRecord.setAudio(false);
                if (this.mVideoRecord.isSuccessPlayFirstFrame()) {
                    temLiveRecord.setErr(true);
                }
                temLiveRecord.stopPrepareTime();
                if (!mUIPlayContext.isChangeStream) {
                    CustomerStatistics.sendLiveRecord(temLiveRecord);
                }

            } else if (mVideoRecord instanceof VodRecord) {
                ((VodRecord) mVideoRecord).stopPrepareTime();
                mVideoRecord.stopPlayTime();
                setRealPlayDuration();
                ((VodRecord) mVideoRecord).setAudio(false);
                if (!mUIPlayContext.isChangeStream) {
                    CustomerStatistics.sendVODRecord((VodRecord) mVideoRecord);
                }
            }

            sendUserAction();
        }
    }


    private String getLiveRecordTitle(TVLiveInfo info) {
        if (info == null) {
            return "";
        }
        String programTitle = "";
        if (!ListUtils.isEmpty(info.getSchedule()) &&
                info.getSchedule().get(0) != null
                && !TextUtils.isEmpty(info.getSchedule().get(0).getProgramTitle())) {
            programTitle = info.getSchedule().get(0).getProgramTitle();
        }
        return programTitle;
    }


    private String getOp() {
        return mUIPlayContext.isFromCache ? "yes" : "no";
    }


    public void onDestroy() {
        mBNHandler.removeCallbacksAndMessages(null);
    }

    private long realPlayDuration;

    private void setRealPlayDuration() {
        if (mVideoRecord.getTotalPlayTime() > 0) {
            realPlayDuration = mVideoRecord.getTotalPlayTime();
        }
    }

    /**
     * 发送用户锁屏统计
     */
    public void sendScreenOffSmartStatistics() {
        sendUserAction();
    }

    /**
     * 用于发送用户行为的统计
     */
    private void sendUserAction() {
        if (mUserOperator == null || realPlayDuration == 0L || mUIPlayContext.isChangeStream || mUIPlayContext.isBigReportVideo) {
            return;
        }

        mUserOperator.setOperation(UserOperatorConst.OPERATION_PLAY);
        mUserOperator.setType(UserOperatorConst.TYPE_VIDEO);
        if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TOPIC) {
            mUserOperator.setType(getSmartType());
        } else if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TV) {
            mUserOperator.setType(UserOperatorConst.TYPE_LIVE);
        }
        if (mVideoRecord instanceof VodRecord) {
            VodRecord temRecord = (VodRecord) mVideoRecord;
            mUserOperator.setTitle(temRecord.getTitle());
            mUserOperator.setKeyword(mVideoRecord.getWmname());
        } else if (mVideoRecord instanceof LiveRecord) {
            LiveRecord liveRecord = (LiveRecord) mVideoRecord;
            mUserOperator.setTitle(liveRecord.getLiveTitle());
            mUserOperator.setKeyword(liveRecord.getWmname());
        }

        String secondDuration = String.format("%.2f", realPlayDuration * 1.0F);
        mUserOperator.setDuration(secondDuration);
        mVideoSmartManager.send(mUserOperator);
        realPlayDuration = 0;

    }

    /**
     * 专题播放页
     * 智能推荐的统计上报的专题都是cmpptopic，应该根据类型显示对应的
     */
    String getSmartType() {
        if (CheckIfengType.isFocus(mUIPlayContext.mFromForAd)) {
            return UserOperatorConst.TYPE_FOCUS;
        } else if (CheckIfengType.isLianBo(mUIPlayContext.mFromForAd)) {
            return UserOperatorConst.TYPE_LIANBO;
        } else {
            return UserOperatorConst.TYPE_CMPPTOPIC;
        }
    }


}
