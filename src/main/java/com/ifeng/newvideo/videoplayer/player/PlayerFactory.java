package com.ifeng.newvideo.videoplayer.player;

import android.view.Surface;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.video.player.ChoosePlayerUtils;

/**
 * Created by fanshell on 2016/8/1.
 */
public class PlayerFactory {

    public static IPlayer createPlayer(Surface surface) {
        IPlayer mediaPlayer;
        if (ChoosePlayerUtils.useIJKPlayer(IfengApplication.getAppContext())) {
            mediaPlayer = new WrapperIjkPlayer();
        } else {
            mediaPlayer = new WrapperIfengPlayer();
        }

        mediaPlayer.setSurface(surface);
        return mediaPlayer;
    }
}
