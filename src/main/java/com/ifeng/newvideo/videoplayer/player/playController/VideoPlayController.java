package com.ifeng.newvideo.videoplayer.player.playController;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.utils.TimeUtils;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.dao.SurveyDao;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.IfengSurfaceView;
import com.ifeng.newvideo.videoplayer.player.OnPlayStateListener;
import com.ifeng.newvideo.videoplayer.player.OnPlayVideoSizeChangeListener;
import com.ifeng.newvideo.videoplayer.player.PlayUrlAuthUtils;
import com.ifeng.newvideo.videoplayer.player.PlayerFactory;
import com.ifeng.newvideo.videoplayer.player.VideoAdvertManager;
import com.ifeng.newvideo.videoplayer.player.WrapperIfengPlayer;
import com.ifeng.newvideo.videoplayer.player.WrapperIjkPlayer;
import com.ifeng.newvideo.videoplayer.player.advert.AdvertEventManager;
import com.ifeng.newvideo.videoplayer.player.history.HistoryManager;
import com.ifeng.newvideo.videoplayer.player.record.ContinueRecordManager;
import com.ifeng.newvideo.videoplayer.player.record.VideoRecordCountManger;
import com.ifeng.newvideo.videoplayer.widget.AdvertView;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.VideoAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/11/17.
 */
public class VideoPlayController extends BasePlayController implements IPlayController, OnPlayStateListener, TextureView.SurfaceTextureListener {
    private Logger logger = LoggerFactory.getLogger(VideoPlayController.class);
    /**
     * 广告相关的变量
     */
    private VideoAdvertManager mAdvertManager;
    private List<VideoAdInfoModel> mAdvertList;
    private int mCurrentAdPosition;
    private volatile boolean isAdPlayComplete;
    private AdvertEventManager mAdEventManager;

    /**
     * 统计相关
     */
    private VideoRecordCountManger mRecordCountManager;

    /**
     * 播放相关的
     */
    private IfengSurfaceView mSurfaceView;
    private TextureView mTextureView;
    private Surface mSurface;
    private IPlayer mPlayer;
    private IPlayer.PlayerState mCurrentOrientation;
    private volatile boolean isSurfaceCreate;
    private String mPath;
    private VideoSkin mSkin;

    /**
     * 离开时,是否是暂停
     */
    private boolean leaveStateIsPause = false;

    /**
     * 是否自动播放下一个视频的标记
     */
    private boolean isProgrammeAutoNext = true;
    /**
     * 保证只做一次实始化的操作
     */
    private volatile boolean isInitFinish;

    public VideoPlayController() {
        isInitFinish = false;
        createSurfaceIfNeed();
    }

    public void init(VideoSkin skin, UIPlayContext uiPlayContext) {
        if (isInitFinish) {
            return;
        }
        isInitFinish = true;
        this.mSkin = skin;
        this.mUIPlayContext = uiPlayContext;
        mSkin.buildSkin(uiPlayContext);
        createSurfaceIfNeed();
        init();
    }

    /**
     * 初始化相关的成员变量,包括广告,看过,统计相关
     */
    private void init() {
        mAdEventManager = new AdvertEventManager();
        mAdvertManager = new VideoAdvertManager();
        mAdvertManager.setUIPlayContext(mUIPlayContext);
        mAdvertManager.setOnAdSuccessListener(mAdListener);
        isAdPlayComplete = false;
        mRecordCountManager = new VideoRecordCountManger(mUIPlayContext);
        mHistoryManger = new HistoryManager();
    }

    private void changeStream(Bundle data) {
        String stream = data.getString(StreamUtils.KEY_STREAM);
        ToastUtils.getInstance().showShortToast(stream + "切换中，请稍后...");
        if (!checkPath()) return;
        playSource(mPath);
        UserFeed.initIfengAddress(IfengApplication.getAppContext(), mPath);
    }

    @Override
    public void setStream(String stream) {
        super.setStream(stream);
        updatePlayStatus(IPlayer.PlayerState.STREAM_CHANGE, mBundle);
    }

    /**
     * 检测是否是一个有效的地址
     *
     * @return 有效地址:true
     */
    private boolean checkPath() {
        if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TV) {
            String changePath = StreamUtils.getMediaUrlForTV(mUIPlayContext.tvLiveInfo);
            if (TextUtils.isEmpty(changePath)) {
                return false;
            }
            mPath = changePath;
        } else if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_PIC) {
            if (mUIPlayContext.videoFilesBeanList == null || mUIPlayContext.videoFilesBeanList.isEmpty()) {
                return false;
            }
            mPath = StreamUtils.getMediaUrlForPic(mUIPlayContext.videoFilesBeanList);
        } else {
            //得到真实的地址
            if (mUIPlayContext.videoItem == null || ListUtils.isEmpty(mUIPlayContext.videoItem.videoFiles)) {
                return false;
            }
            mPath = StreamUtils.getMediaUrl(mUIPlayContext.videoItem.videoFiles);
        }
        return true;
    }

    /**
     * 广告load完以后的回调
     */
    private VideoAdvertManager.OnAdCompleteListener mAdListener = new VideoAdvertManager.OnAdCompleteListener() {
        @Override
        public void onAdComplete(List<VideoAdInfoModel> adList) {
            logger.debug("onAdComplete ad...........");
            if (User.isVip()) {
                adList = null;
            }
            if (!ListUtils.isEmpty(adList)) {
                mAdvertList = new ArrayList<>();
                mAdvertList.addAll(adList);
                mCurrentAdPosition = 0;
                logger.debug("start ad...........");
                mUIPlayContext.adVideoTotalLength = 0;
                for (VideoAdInfoModel adInfoModel : mAdvertList) {
                    try {
                        if (!VideoAdvertManager.isSponsorHead(adInfoModel)) {// 片头时间不计入
                            mUIPlayContext.adVideoTotalLength += Double.parseDouble(adInfoModel.getLength());
                        }
                    } catch (Exception e) {
                        logger.debug("ad total duration error");
                    }
                }
                logger.debug("total duration :{}", mUIPlayContext.adVideoTotalLength);
                playAdvert();
                AdvertView.mAdTotalLength = 0;
            } else {
                isAdPlayComplete = true;
                mAdvertList = null;
                playProgramme();

            }

        }
    };

    private boolean isCanSeek() {
        return mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TOPIC ||
                mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_VOD ||
                mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_PIC ||
                mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_LOCAL;
    }

    private void handlePlayCountForAd() {
        if (mUIPlayContext.isTopic) {
            mUIPlayContext.topicPlayCount++;
        } else {
            IfengApplication.getInstance().mVodPlayCount++;
            mUIPlayContext.vodPlayCount = IfengApplication.getInstance().mVodPlayCount;
        }
    }

    @Override
    public long getLastPosition() {
        return lastPosition;
    }

    @Override
    public int getPlayerType() {
        if (mPlayer instanceof WrapperIjkPlayer) {
            return PIP_VIDEO_PLAYER_TYPE_IJK;
        } else if (mPlayer instanceof WrapperIfengPlayer) {
            return PIP_VIDEO_PLAYER_TYPE_IFENG;
        }
        return 0;
    }

    @Override
    public void stop() {

    }

    /**
     * 用于播放正片节目
     */
    private void playProgramme() {
        mAdEventManager.clearEvent();
        if (mPlayer == null) {
            return;
        }
        mUIPlayContext.isAdvert = false;
        mUIPlayContext.videoAdModel = null;

        AdvertView.mAdTotalLength = 0;
        mTextureView.setKeepScreenOn(true);

        String guid = "";
        if (mUIPlayContext.videoItem != null) {
            guid = mUIPlayContext.videoItem.guid;
            SurveyDao.accumulatorPlayCount(guid);
        }
        if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TV) {
            mPlayer.setSourcePath(PlayUrlAuthUtils.getLiveAuthUrl(mPath));
        } else {
            mPlayer.setSourcePath(PlayUrlAuthUtils.getVideoAuthUrl(mPath, guid));
        }

        seekToLastPosition();

        if (mPlayer instanceof WrapperIjkPlayer) {
            ((WrapperIjkPlayer) mPlayer).setLeaveStateIsPause(leaveStateIsPause);
        } else if (mPlayer instanceof WrapperIfengPlayer) {
            ((WrapperIfengPlayer) mPlayer).setLeaveStateIsPause(leaveStateIsPause);
        }
        leaveStateIsPause = false;
        mCurrentState = IPlayer.PlayerState.STATE_PREPARING;
        notifyObserver();
        mPlayer.prepareAsync();
        handleAdPlayTimes();
        if (mUIPlayContext.isAdvert) {
            UserPointManager.addRewards(UserPointManager.PointType.addByOpenAD);
        } else {
            UserPointManager.addRewards(UserPointManager.PointType.addByOpenVideo);
        }
    }


    private void handleAdPlayTimes() {
        resetAdPlayTimesIfNeeded();

        // 播放正片时允许跳过广告次数-1
        int adPlayTimes = SharePreUtils.getInstance().getAdPlayTimes();
        SharePreUtils.getInstance().setAdPlayTimes(--adPlayTimes);
        // ToastUtils.getInstance().showShortToast(adPlayTimes + " 次");
    }

    private void resetAdPlayTimesIfNeeded() {
        if (!TimeUtils.isSameDay()) {
            // 重置次数
            SharePreUtils.getInstance().setAdPlayTimes(8);

            // 重置日期，这么处理其实不太合适，但能解决重置次数的问题
            TimeUtils.setTimeDiff(System.currentTimeMillis() + "");
            SharePreUtils.getInstance().setServerDate(
                    new SimpleDateFormat("yyyyMMdd").format(System.currentTimeMillis()));
        }
    }


    /**
     * seek 到上次播放 记录的位置 根据不同播放器
     */
    private void seekToLastPosition() {
        if (isCanSeek() && lastPosition > 0) {
            if (mPlayer instanceof WrapperIjkPlayer) {
                mPlayer.seekTo(lastPosition);
            } else if (mPlayer instanceof WrapperIfengPlayer) {
                mPlayer.setLastPosition(lastPosition);
            }
        }
    }

    /**
     * 用于播放广告
     */

    private void playAdvert() {
        mUIPlayContext.isAdvert = true;
        VideoAdInfoModel videoAdInfoModel = mAdvertList.get(mCurrentAdPosition);
        mUIPlayContext.videoAdModel = videoAdInfoModel;
        if (mPlayer == null) {
            return;
        }
        String advertPath = videoAdInfoModel.getUrl();
        mPlayer.setSourcePath(advertPath);
        mTextureView.setKeepScreenOn(true);

        seekToLastPosition();

        mCurrentState = IPlayer.PlayerState.STATE_PREPARING;
        notifyObserver();
        mPlayer.prepareAsync();
        mUIPlayContext.mOpenFirst = 0;

        if (lastPosition == 0) {
            mAdEventManager.sendStartEvent(videoAdInfoModel.getStart());
        }
        //delay
        long delayTime = Long.parseLong(videoAdInfoModel.getLength()) / 2L * DateUtils.SECOND_IN_MILLIS - lastPosition;
        mAdEventManager.sendMiddleEventDelay(delayTime, videoAdInfoModel.getMiddle());
        UserPointManager.addRewards(UserPointManager.PointType.addByOpenAD);


    }

    private void stopAndRelease() {
        if (mPlayer != null) {
            mPlayer.pause();
            mPlayer.stop();
            mPlayer.release();
        }

        mPlayer = null;
        mAdEventManager.clearEvent();
    }

    private synchronized void createOnePlayer(Surface surface) {
        mPlayer = PlayerFactory.createPlayer(surface);
        if (TextUtils.isEmpty(mPath)) {
            return;
        }
    }

    private synchronized void createSurfaceIfNeed() {
        if (mTextureView == null && mSkin != null) {
            this.mTextureView = new TextureView(mSkin.getContext());
            this.mTextureView.setSurfaceTextureListener(this);
//            this.mSurfaceView.getHolder().addCallback(mSurfaceCallBack);
            mSkin.addVideoView(mTextureView);
        }
    }

    private void reset() {
//        mSurfaceView.resetSurface();
        mSkin.registerPlayController(this);
        mPlayer.setOnPlayStateListener(this);
        mPlayer.setOnPlayVideoSizeChangeListener(mVideoSizeChangeListener);
    }

    @Override
    public void playSource(String path) {
        logger.debug("playUrl = {}", path);
        createSurfaceIfNeed();
        stopAndRelease();
        createOnePlayer(mSurface);
        reset();
        if (TextUtils.isEmpty(path)) {
            mCurrentState = IPlayer.PlayerState.STATE_ERROR;
            notifyObserver();
            return;
        }
        if (!path.equals(mPath) && !TextUtils.isEmpty(mPath)) {
            //新的视频
            logger.debug("resetPosition path:{}", path);
            logger.debug("resetPosition mPath:{}", mPath);
            lastPosition = 0;
            mAdvertList = null;
            isAdPlayComplete = false;
            if (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_TV) {//如果不是直播，重置状态
                leaveStateIsPause = false;//
            }
        }
        this.mPath = path;
        if (!isValidateNetWork()) {
            mCurrentState = IPlayer.PlayerState.STATE_ERROR;
            notifyObserver();
            return;
        }
        if (!isSurfaceCreate) {
            mCurrentState = IPlayer.PlayerState.STATE_PREPARING;
            notifyObserver();
            return;
        }
        if (ActivityMainTab.mAudioService != null) {
            logger.debug("ActivityMainTab.mAudioService.stopSelf()");
            ActivityMainTab.mAudioService.stopAudioService(mSkin.getSkinType());
        }

        logger.debug("isSurfaceCreate:{}", isSurfaceCreate);

        if (isFromHistory) {//如果从看过页面跳转过来，不需要播放广告
            mAdvertList = null;
            isAdPlayComplete = true;
            playProgramme();
            return;
        }

        switch (mUIPlayContext.skinType) {
            case VideoSkin.SKIN_TYPE_TOPIC:
            case VideoSkin.SKIN_TYPE_VOD:
                if (!isAdPlayComplete) {
                    if (mAdvertList == null) {
                        logger.debug("getAddData");
                        handlePlayCountForAd();
                        mAdvertManager.getADData();
                    } else {
                        playAdvert();
                    }
                } else {
                    isAdPlayComplete = true;
                    playProgramme();
                }

                break;
            default:
                if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_LOCAL) {//如果是缓存播放器，设置为横屏
                    setOrientation(IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
                }
                isAdPlayComplete = true;
                playProgramme();
                break;
        }

        if (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_LOCAL && mSkin.getSkinType() != VideoSkin.SKIN_TYPE_TV) {
            User.updateUserVipInfo(mSkin.getContext());
        }
    }

    public void setIsAdComplete(boolean isAdComplete) {
        this.isAdPlayComplete = isAdComplete;
    }

    /**
     * 自动播放
     */
    private void autoNextPlay() {
        if (isAdPlayComplete && !mUIPlayContext.isTopic) {
            mUIPlayContext.isRelate = true;
        }
        //播放完成
        if (!isAdPlayComplete) {
            mCurrentAdPosition++;
            lastPosition = 0;
            logger.debug("autoNextPlay:" + lastPosition);
            if (mAdvertList != null && mCurrentAdPosition >= 0 && mCurrentAdPosition < mAdvertList.size()) {
                stopAndRelease();
                createOnePlayer(mSurface);
                reset();
                playAdvert();
                return;
            }
            isAdPlayComplete = true;
            mCurrentAdPosition = -1;
            mAdvertList = null;
            //playSource(mPath);
            stopAndRelease();
            createOnePlayer(mSurface);
            reset();
            playProgramme();
            if (mAutoPlayListener != null && mUIPlayContext.videoItem != null) {
                String title = mUIPlayContext.videoItem.title;
                if (TextUtils.isEmpty(title)) {
                    title = mUIPlayContext.videoItem.name;
                }
                mUIPlayContext.title = title;
                mAutoPlayListener.onPlayListener(mUIPlayContext.videoItem);
            }
        } else {
            if (ListUtils.isEmpty(mUIPlayContext.VideoItemList)) {
                return;
            }
            if (!isProgrammeAutoNext) {
                return;
            }
            int position = getProgrammePosition();
            mUIPlayContext.videoItem = mUIPlayContext.VideoItemList.get(position);
            String title = mUIPlayContext.videoItem.title;
            if (TextUtils.isEmpty(title)) {
                title = mUIPlayContext.videoItem.name;
            }
            mUIPlayContext.title = title;
            String path = StreamUtils.getMediaUrl(mUIPlayContext.videoItem.videoFiles);
            playSource(path);
            if (mAutoPlayListener != null) {
                this.mAutoPlayListener.onPlayListener(mUIPlayContext.videoItem);
            }
        }
    }

    @Override
    public void skipAdvert() {
        super.skipAdvert();
        if (mAdvertList != null) {
            mCurrentAdPosition = mAdvertList.size();
        }
        if (mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_VOD) {
            setIsAdComplete(true);
            mUIPlayContext.VideoItemList.add(0, mUIPlayContext.videoItem);
        }
        autoNextPlay();
    }

    @Override
    public void onResume() {
        logger.debug("onResume");
        //FIXME:直播页播放视频时,切换到大图播放,上下滑动时会造成surface重叠而冲突,暂时用 ondestroy移除mSurfaceView,onresume重加mSurfaceView的方式
        if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV && mTextureView != null && mTextureView.getParent() == null) {
            mSkin.addVideoView(mTextureView);
        }
        insertCustomerStatistics(IPlayer.PlayerState.STATE_PREPARING);//初始化统计:保证下次上报不被拦截
        if (mPlayer != null) {
            mPlayer.start();
            //TODO:设置朝向是独立逻辑为何要与mPlayer是否为空关联?
//            if (mCurrentOrientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
//                setOrientation(mCurrentOrientation);
//            }
        }
        if (mCurrentOrientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
            setOrientation(mCurrentOrientation);
        }
        if (mPlayer == null && !TextUtils.isEmpty(mPath)) {
            playSource(mPath);

        }
        if (mPlayer != null || !TextUtils.isEmpty(mPath)) {
            if (User.isVip()) {
                skipAdvert();
                resetAdvertList();
            }
        }
        mSkin.onResume();

    }


    @Override
    public void onPause() {
        logger.debug("onPause");
        mAdEventManager.clearEvent();
        if (mPlayer != null) {
            if (mPlayer instanceof WrapperIjkPlayer) {
                if (((WrapperIjkPlayer) mPlayer).isInPlayState()) {
                    leaveStateIsPause = !mPlayer.isPlaying();//onPause()时播放器是否为暂停状态
                }
            } else if (mPlayer instanceof WrapperIfengPlayer) {
                if (((WrapperIfengPlayer) mPlayer).isInPlayState()) {
                    leaveStateIsPause = !mPlayer.isPlaying();//onPause()时播放器是否为暂停状态
                }
            }
            mPlayer.pause();
            mCurrentOrientation = mUIPlayContext.orientation;
        }

        mSkin.onPause();
        if (mSkin.getLoadView().getVisibility() == View.VISIBLE) {
            stopAndRelease();
        }
        //FIXME:直播页播放视频时,切换到大图播放,上下滑动时会造成surface重叠而冲突,暂时用 ondestroy移除mSurfaceView,onresume重加mSurfaceView的方式
        if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV && mTextureView != null) {
            mSkin.removeVideoView(mTextureView);
        }
    }

    public void setLeaveStateIsPause(boolean leaveStateIsPause) {
        this.leaveStateIsPause = leaveStateIsPause;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        logger.debug("onDestroy");
        stopAndRelease();
        mRecordCountManager.onDestroy();
        if (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_TV) {//如果不是直播，重置状态
            leaveStateIsPause = false;
        }
        if (mAdvertManager != null) {
            mAdvertManager.setOnAdSuccessListener(null);
        }

    }

    @Override
    public void rePlaySource(String path) {
        lastPosition = 0;
        playSource(path);
    }

    private void insertCustomerStatistics(IPlayer.PlayerState playState) {
        mRecordCountManager.insertStatistics(playState, mSkin.getContext());
    }

    /**
     * 发送播放页锁屏统计
     */
    @Override
    public void sendScreenOffStatistics() {
        if (mRecordCountManager != null) {
            if (mPlayer instanceof WrapperIjkPlayer) {
                if (((WrapperIjkPlayer) mPlayer).isInPlayState()) {
                    mRecordCountManager.sendScreenOffSmartStatistics();
                }
            } else if (mPlayer instanceof WrapperIfengPlayer) {
                if (((WrapperIfengPlayer) mPlayer).isInPlayState()) {
                    mRecordCountManager.sendScreenOffSmartStatistics();
                }
            }
        }
    }

    /**
     * 根据当前的VideoItem得到在列表中的位置
     * 根据GUID
     *
     * @return
     */
    private int getProgrammePosition() {
        int position = -1;
        int size = mUIPlayContext.VideoItemList.size();
        VideoItem item;
        for (int i = 0; i < size; i++) {
            item = mUIPlayContext.VideoItemList.get(i);
            if (mUIPlayContext.videoItem.guid.equals(item.guid)) {
                position = i;
                break;
            }
        }
        if (position == -1) {
            position = 0;
        }

//        else {
//            position = position + 1;
//        }

        if (mUIPlayContext.videoItem.isColumn()
                || mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_TOPIC
                || mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_LOCAL) {

            position = position + 1;
        }

        if (position >= mUIPlayContext.VideoItemList.size()) {
            position = 0;
        }

        return position;
    }


    /**
     * 是否从看过页面跳转而来
     */
    private boolean isFromHistory;

    public void setFromHistory(boolean fromHistory) {
        isFromHistory = fromHistory;
    }


    @Override
    public long getCurrentPosition() {
        if (mPlayer != null) {
            return mPlayer.getCurrentPosition();
        }
        return 0;
    }


    private OnPlayVideoSizeChangeListener mVideoSizeChangeListener = new OnPlayVideoSizeChangeListener() {

        @Override
        public void onVideoSizeChangeListener(int width, int height) {
            logger.debug("videoWidth:{} videoHeight:{}", width, height);
            mSkin.setVideoSize(width, height);
        }
    };

    @Override
    public void setProgrammeAutoNext(boolean isAuto) {
        isProgrammeAutoNext = isAuto;
    }

    /**
     * 节目进行自动播放
     */
    public void autoNextProgramme() {
        isProgrammeAutoNext = true;
        if (!isAdPlayComplete) {
            return;
        }
        if (mPlayer != null && mPlayer.isPlaying()) {
            return;
        }
        if (mPlayer != null && mPlayer.getCurrentPosition() + 2 * DateUtils.SECOND_IN_MILLIS >= mPlayer.getDuration()) {
            autoNextPlay();
        }

    }

    private SurfaceHolder.Callback mSurfaceCallBack = new SurfaceHolder.Callback() {
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            isSurfaceCreate = false;
            logger.debug("surfaceDestroyed");
            stopAndRelease();
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            logger.debug("surfaceCreated");
            isSurfaceCreate = true;
            if (TextUtils.isEmpty(mPath)) {
                return;
            }
            if (isPlayWhenSurfaceCreated) {
                playSource(mPath);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            logger.debug("surfaceChanged");
        }
    };

    @Override
    public void onConfigureChange(Configuration newConfig) {
        mSkin.changeLayoutParams();
        setOrientation(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? IPlayer.PlayerState.ORIENTATION_LANDSCAPE : IPlayer.PlayerState.ORIENTATION_PORTRAIT);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            int uiOptions = ((Activity) mSkin.getContext()).getWindow().getDecorView().getSystemUiVisibility();
            uiOptions |= (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            ((Activity) mSkin.getContext()).getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        } else {
            int uiOptions = ((Activity) mSkin.getContext()).getWindow().getDecorView().getSystemUiVisibility();
            uiOptions &= ~(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            ((Activity) mSkin.getContext()).getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    public void setOrientation(IPlayer.PlayerState orientation) {
        insertContinueStatistics(orientation);
        super.setOrientation(orientation);
    }

    private boolean isPlayWhenSurfaceCreated = true;//当Surface创建完成时是否要进行播放操作，默认是要播放的

    public boolean isPlayWhenSurfaceCreated() {
        return isPlayWhenSurfaceCreated;
    }

    public void setIsPlayWhenSurfaceCreated(boolean isPlayVideo) {
        isPlayWhenSurfaceCreated = isPlayVideo;
    }

    @Override
    public boolean isPlaying() {
        return mPlayer != null && mPlayer.isPlaying();
    }

    @Override
    public void pause() {
        if (mPlayer != null) {
            mPlayer.pause();
        }
    }

    @Override
    public void start() {
        if (mPlayer != null) {
            mPlayer.start();
        }
    }

    @Override
    public long getDuration() {
        if (mPlayer != null) {
            return mPlayer.getDuration();
        }
        return 0L;
    }

    @Override
    public void seekTo(long duration) {
        if (mPlayer != null) {
            mPlayer.seekTo(duration);
        }
    }


    /**
     * 实现播放状态的监听
     */

    @Override
    public void updatePlayStatus(IPlayer.PlayerState state, Bundle data) {
        mCurrentState = state;
        mBundle = data;
        notifyObserver();
        //统计
        insertCustomerStatistics(state);
        insertContinueStatistics(state);
        switch (state) {
            case STATE_PAUSED://暂停
                lastPosition = mPlayer.getCurrentPosition();
                saveHistory(false);
                logger.debug("STATE_PAUSED:lastPostion:{}", lastPosition);
                mSkin.setContentDescription("pause");
                break;
            case STATE_PLAYBACK_COMPLETED:
                saveHistory(true);
                if (isFromHistory) {
                    isFromHistory = false;
                }
                if (mOnPlayCompleteListener != null) {
                    mOnPlayCompleteListener.onPlayComplete();
                }

                if (mUIPlayContext.isAdvert) {
                    //发送广告end
                    mAdEventManager.sendEndEvent(mUIPlayContext.videoAdModel.getEnd());
                }
                autoNextPlay();
                mSkin.setContentDescription("playback_completed");
                break;
            case STREAM_CHANGE://清晰度切换
                mUIPlayContext.isChangeStream = true;
                changeStream(data);
                mUIPlayContext.isChangeStream = false;
                mSkin.setContentDescription("stream_change");
                break;
            case STATE_PLAYING:
                mSkin.setContentDescription("playing");
                mPreViewVideoItem = mUIPlayContext.videoItem;
                if (User.isVip() && mUIPlayContext.skinType == VideoSkin.SKIN_TYPE_VOD) {
                    mSkin.setVipNoticeViewVisibility();
                }
                break;
            case ORIENTATION_LANDSCAPE:
                mSkin.setContentDescription("orientation_landscape");
                break;
            case ORIENTATION_PORTRAIT:
                mSkin.setContentDescription("orientation_portrait");
                break;
            default:
                break;
        }
    }

    /**
     * check netWork
     *
     * @return 有效
     */
    private boolean isValidateNetWork() {
        boolean result = true;
        if (!NetUtils.isNetAvailable(mSkin.getContext()) && (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_LOCAL)) {
            result = false;
        } else if (NetUtils.isMobile(mSkin.getContext()) && !IfengApplication.mobileNetCanPlay && (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_LOCAL)) {
            result = false;
        }
        return result;
    }

    public void reSetPlayPath(String path) {
        mPath = path;
    }

    /**
     * 用于实现连播行为统计的类
     */
    private ContinueRecordManager mContinueManager;

    public void setContinueManager(ContinueRecordManager manager) {
        this.mContinueManager = manager;
    }

    private void insertContinueStatistics(IPlayer.PlayerState state) {
        if (mContinueManager != null) {
            if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TOPIC) {
                mContinueManager.init(mUIPlayContext.channelId, mUIPlayContext.videoItem == null ? "" : VodRecord.convertTopicChid(mUIPlayContext.videoItem.topicId, mUIPlayContext.videoItem.topicType));
            } else {
                mContinueManager.init(mUIPlayContext.channelId, mUIPlayContext.videoItem == null ? "" : mUIPlayContext.videoItem.searchPath);
            }
            mContinueManager.insertCustomerStatistics(state, mPath);
        }
    }

    public void resetAdvertList() {
        mAdvertList = null;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        this.mSurface = new Surface(surface);
        isSurfaceCreate = true;
        if (TextUtils.isEmpty(mPath)) {
            return;
        }
        if (isPlayWhenSurfaceCreated) {
            playSource(mPath);
        }

    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        isSurfaceCreate = false;
        logger.debug("surfaceDestroyed");
        mSurface.release();
        stopAndRelease();

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}
