package com.ifeng.newvideo.videoplayer.player.playController;

import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.domains.ContinueSplayRecord;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.OnPlayStateListener;
import com.ifeng.newvideo.videoplayer.player.OnPlayVideoSizeChangeListener;
import com.ifeng.newvideo.videoplayer.player.PlayUrlAuthUtils;
import com.ifeng.newvideo.videoplayer.player.PlayerFactory;
import com.ifeng.newvideo.videoplayer.player.WrapperIfengPlayer;
import com.ifeng.newvideo.videoplayer.player.WrapperIjkPlayer;
import com.ifeng.newvideo.videoplayer.player.history.HistoryManager;
import com.ifeng.newvideo.videoplayer.player.record.ContinueRecordManager;
import com.ifeng.newvideo.videoplayer.player.record.VideoRecordCountManger;
import com.ifeng.newvideo.videoplayer.widget.skin.FloatVideoViewSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.model.ChannelBean;
import com.ifeng.video.dao.db.model.HomePageBeanBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ll on 2017/9/26.
 */
public class FloatVideoPlayController extends BasePlayController implements IPlayController, OnPlayStateListener, TextureView.SurfaceTextureListener{
    private TextureView mTextureView;
    private Surface mSurface;
    private IPlayer mPlayer;
    private IPlayer.PlayerState mCurrentOrientation;
    private volatile boolean isSurfaceCreate;
    private String mPath;
    private FloatVideoViewSkin mSkin;
    private List<ChannelBean.HomePageBean> mDataList;
    private OnListViewScrollToPiPVideoView mOnListViewScrollToPiPVideoView;
    private ContinueRecordManager mContinueRecordManger;
    /**
     * 统计相关
     */
    private VideoRecordCountManger mRecordCountManager;

    public void setOnListViewScrollToPiPVideoView(OnListViewScrollToPiPVideoView listener) {
        mOnListViewScrollToPiPVideoView = listener;
    }


    public FloatVideoPlayController() {
        createSurfaceIfNeed();
    }

    public void init(FloatVideoViewSkin skin, UIPlayContext uiPlayContext) {
        mSkin = skin;
        mUIPlayContext = uiPlayContext;
        mSkin.attachUIContext(mUIPlayContext);
        createSurfaceIfNeed();
        mDataList = new ArrayList<>();
        mHistoryManger = new HistoryManager();
        if (mContinueRecordManger == null) {
            mContinueRecordManger = new ContinueRecordManager(ContinueSplayRecord.TYPE_PIP_PLAYER);
        }
        mContinueRecordManger.init(mUIPlayContext.channelId, mUIPlayContext.videoItem == null ? "" : mUIPlayContext.videoItem.searchPath);
        mRecordCountManager = new VideoRecordCountManger(mUIPlayContext);
    }

    public void setData(List<ChannelBean.HomePageBean> list, long lastpos) {
        lastPosition = lastpos;
        if (EmptyUtils.isNotEmpty(list)) {//大图
            for (ChannelBean.HomePageBean bean : list) {
                if (!TextUtils.isEmpty(bean.getMemberType())) {
                    if (!CheckIfengType.isAdBackend(bean.getMemberType())) {
                        mDataList.add(bean);
                    }
                }
            }
        } else {//精选
            ViewGroup group = (ViewGroup) mSkin.getParent();
            if (EmptyUtils.isNotEmpty(group)) {
                group.setVisibility(View.VISIBLE);
                String stream = StreamUtils.getMediaUrlForPic(mUIPlayContext.videoFilesBeanList);
                playSource(stream);
            }
        }
        for (ChannelBean.HomePageBean bean : mDataList) {
            if (EmptyUtils.isNotEmpty(bean) && EmptyUtils.isNotEmpty(bean.getMemberItem())
                    && EmptyUtils.isNotEmpty(bean.getMemberItem().getGuid())
                    && bean.getMemberItem().getGuid().equals(mUIPlayContext.videoItem.guid)) {
                ViewGroup group = (ViewGroup) mSkin.getParent();
                if (EmptyUtils.isNotEmpty(group)) {
                    group.setVisibility(View.VISIBLE);
                    String stream = StreamUtils.getMediaUrlForPic(mUIPlayContext.videoFilesBeanList);
                    playSource(stream);//http://ips.ifeng.com/video19.ifeng.com/video09/2017/09/09/6210016-544-080-000158.mp4
                }
                break;
            }
        }

    }

    private synchronized void createSurfaceIfNeed() {
        if (mTextureView == null && mSkin != null) {
            this.mTextureView = new TextureView(mSkin.getContext());
            this.mTextureView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toVideoActivity(mUIPlayContext.videoItem.guid, false, true, getCurrentPosition(), false);
                    PageActionTracker.clickBtn(ActionIdConstants.PIP_CLICK_TO_PLAY_DETAIL, PageActionTracker.getPageName(false, mSkin.getContext()));
                }
            });
            this.mTextureView.setSurfaceTextureListener(this);
//            this.mSurfaceView.getHolder().addCallback(mSurfaceCallBack);
            mSkin.addVideoView(mTextureView);
        }
    }
    private void toVideoActivity(String guid, boolean anchor, boolean isFromHistory, long bookMark, boolean isEditDanma) {
        IntentUtils.toVodDetailActivity(mSkin.getContext(), EmptyUtils.isNotEmpty(guid) ? guid : "",
                "pip", anchor, isFromHistory, bookMark, "", isEditDanma);
        mSkin.removeViews();
    }


    @Override
    public void init(VideoSkin skin, UIPlayContext uiPlayContext) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {
        mSkin.onPause();
    }

    @Override
    public void rePlaySource(String path) {

    }

    public void playSource(String path) {
        mPath = path;
        createSurfaceIfNeed();
        stopAndRelease();
        createOnePlayer(mSurface);
        mPlayer.setOnPlayVideoSizeChangeListener(mVideoSizeChangeListener);
        mSkin.registerPlayController(this);
        mSkin.setKeepScreenOn(true);
        mPlayer.setOnPlayStateListener(this);
        mPlayer.setSourcePath(PlayUrlAuthUtils.getVideoAuthUrl(mPath, mUIPlayContext.videoItem.guid));
        seekToLastPosition();
        mCurrentState = IPlayer.PlayerState.STATE_PREPARING;
        notifyObserver();
        mPlayer.prepareAsync();
        mOnListViewScrollToPiPVideoView.onPlayToVisibleView(mUIPlayContext.videoItem.guid, mSkin);
        if (IfengApplication.mobileNetCanPlay && NetUtils.isMobile(mSkin.getContext()) && isSurfaceCreate) {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }
    }
    /**
     * seek 到上次播放 记录的位置 根据不同播放器
     */
    private void seekToLastPosition() {
        if (lastPosition > 0) {
            if (mPlayer instanceof WrapperIjkPlayer) {
                mPlayer.seekTo(lastPosition);
            } else if (mPlayer instanceof WrapperIfengPlayer) {
                mPlayer.setLastPosition(lastPosition);
            }
        }
    }
    @Override
    public void onConfigureChange(Configuration newConfig) {

    }

    private OnPlayVideoSizeChangeListener mVideoSizeChangeListener = new OnPlayVideoSizeChangeListener() {

        @Override
        public void onVideoSizeChangeListener(int width, int height) {
            logger.debug("videoWidth:{} videoHeight:{}", width, height);
            mSkin.setVideoSize(width, height);
        }
    };

    @Override
    public long getCurrentPosition() {
        if (mPlayer != null) {
            return mPlayer.getCurrentPosition();
        }
        return 0;
    }

    @Override
    public void setProgrammeAutoNext(boolean isAuto) {

    }

    private synchronized void createOnePlayer(Surface surface) {
        mPlayer = PlayerFactory.createPlayer(surface);
        if (TextUtils.isEmpty(mPath)) {
            return;
        }
    }

    private void stopAndRelease() {
        if (mPlayer != null) {
            mPlayer.pause();
            mPlayer.stop();
            mPlayer.release();
        }
        mPlayer = null;
    }
    public void autoNextProgramme() {
        stopAndRelease();
        createOnePlayer(mSurface);
    }


    /**
     * 根据当前的VideoItem得到在列表中的位置
     * 根据GUID
     *
     * @return
     */
    private int getProgrammePosition() {
        int position = -1;
        int size = mDataList.size();
        VideoItem item;
        for (int i = 0; i < size; i++) {
            ChannelBean.HomePageBean bean = mDataList.get(i);
            if (EmptyUtils.isNotEmpty(bean) && EmptyUtils.isNotEmpty(bean.getMemberItem())
                    && EmptyUtils.isNotEmpty(bean.getMemberItem().getGuid())
                    && bean.getMemberItem().getGuid().equals(mUIPlayContext.videoItem.guid)) {
                position = i + 1;
            }
        }
        if (position == -1) {
            position = 0;
        }


        if (position >= size) {
            position = 0;
        }

        return position;
    }

    public boolean isPlaying() {
        return null != mPlayer&&mPlayer.isPlaying();
    }

    public void pause() {
        if (mPlayer != null) {
            mPlayer.pause();
        }
    }

    public void start() {

    }

    @Override
    public long getDuration() {
        if (mPlayer != null) {
            return mPlayer.getDuration();
        }
        return 0;
    }

    @Override
    public void seekTo(long duration) {
        if (mPlayer != null) {
            mPlayer.seekTo(duration);
        }
    }

    public long getLastPosition() {
        return lastPosition;
    }

    /**
     * 自动播放
     */
    private void autoNextPlay() {
        if (EmptyUtils.isEmpty(mDataList)) {
            mSkin.removeViews();
        } else {
            lastPosition = 0;
            int pos = getProgrammePosition();
            mUIPlayContext.videoItem = getVideoItem(mDataList.get(pos));
            List<ChannelBean.VideoFilesBean> videoFilesBeanList = mDataList.get(pos).getMemberItem().getVideoFiles();
            mUIPlayContext.videoFilesBeanList = videoFilesBeanList;
            String path = StreamUtils.getMediaUrlForPic(mUIPlayContext.videoFilesBeanList);
            playSource(path);
        }
    }


    private VideoItem getVideoItem(HomePageBeanBase bean) {
        ChannelBean.MemberItemBean memberItem = bean.getMemberItem();
        VideoItem item = new VideoItem();
        ChannelBean.WeMediaBean weMedia = bean.getWeMedia();
        if (weMedia != null) {
            item.weMedia = new WeMedia();
            item.weMedia.id = weMedia.getId();
            item.weMedia.name = weMedia.getName();
            item.weMedia.headPic = weMedia.getHeadPic();
            item.weMedia.desc = weMedia.getDesc();
        }
        item.guid = memberItem.getGuid();
        item.simId = memberItem.getSimId();
        item.cpName = memberItem.getCpName();
        item.title = TextUtils.isEmpty(bean.getTitle()) ? memberItem.getName() : bean.getTitle();//title为空取name
        item.searchPath = memberItem.getSearchPath();
        item.image = "";
        item.duration = memberItem.getDuration();
        item.mUrl = bean.getMemberItem().getmUrl();
        return item;
    }
    @Override
    public void updatePlayStatus(IPlayer.PlayerState state, Bundle data) {
        mCurrentState = state;
        notifyObserver();
        mContinueRecordManger.insertCustomerStatistics(state, mPath);
        mRecordCountManager.insertStatistics(state, mSkin.getContext());
        switch (state) {
            case STATE_PAUSED://暂停
                lastPosition = mPlayer.getCurrentPosition();
                logger.debug("STATE_PAUSED:lastPostion:{}", lastPosition);
                mSkin.setContentDescription("pause");
                break;
            case STATE_PLAYBACK_COMPLETED:
                autoNextPlay();
                mSkin.setContentDescription("playback_completed");
                break;
            case STREAM_CHANGE://清晰度切换
                mUIPlayContext.isChangeStream = true;
                mUIPlayContext.isChangeStream = false;
                mSkin.setContentDescription("stream_change");
                break;
            case STATE_PLAYING:
                mSkin.setContentDescription("playing");
                mPreViewVideoItem = mUIPlayContext.videoItem;
                break;
            case ORIENTATION_LANDSCAPE:
                mSkin.setContentDescription("orientation_landscape");
                break;
            case ORIENTATION_PORTRAIT:
                mSkin.setContentDescription("orientation_portrait");
                break;
            default:
                break;
        }

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        this.mSurface = new Surface(surface);
        isSurfaceCreate = true;
        if (TextUtils.isEmpty(mPath)) {
            return;
        }
        playSource(mPath);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        isSurfaceCreate = false;
        surface.release();
        stopAndRelease();
        mSkin.onPause();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    public interface OnListViewScrollToPiPVideoView {
        void onPlayToVisibleView(String guid, FloatVideoViewSkin skin);
    }

    public int getPlayerType() {
        if (mPlayer instanceof WrapperIjkPlayer) {
            return PIP_VIDEO_PLAYER_TYPE_IJK;
        } else if (mPlayer instanceof WrapperIfengPlayer) {
            return PIP_VIDEO_PLAYER_TYPE_IFENG;
        }
        return 0;
    }

    @Override
    public void stop() {
        stopAndRelease();
        if (mContinueRecordManger != null) {
            mContinueRecordManger.onDestroy();
        }
    }

}
