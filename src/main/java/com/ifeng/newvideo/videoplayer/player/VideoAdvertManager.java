package com.ifeng.newvideo.videoplayer.player;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.dao.VideoAdInfoDao;
import com.ifeng.video.dao.db.model.VideoAdConfigModel;
import com.ifeng.video.dao.db.model.VideoAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanshell on 2016/8/18.
 * 得到视频广告的数据
 */
public class VideoAdvertManager {

    private Logger logger = LoggerFactory.getLogger(VideoAdvertManager.class);
    /**
     * 广告赞助商片头
     */
    public static final String SPONSOR_HEAD = "sponsorhead";

    //private int mCurrentPosition;
    private VideoAdConfigModel.VideoADBean bean;
    private VideoAdConfigModel.VideoADBean.ADUNITIDBean adUnite;
    private List<VideoAdInfoModel> mAdvertList = new ArrayList<>();
    private SharePreUtils mSharePreferenceUtils;
    private OnAdCompleteListener mAdListener;

    public VideoAdvertManager() {
        if (VideoAdConfigDao.adConfig != null) {
            bean = VideoAdConfigDao.adConfig.getVideoAD();
            if (bean != null) {
                adUnite = bean.getADUNITID();
            }
        }
        mSharePreferenceUtils = SharePreUtils.getInstance();
    }

    private String getParam() {
        StringBuilder builder = new StringBuilder();
        List<String> request = getRequestList();
        if (!request.isEmpty()) {
            for (String item : request) {
                builder.append(item + ",");
            }
            builder.delete(builder.toString().length() - 1, builder.length());
        }
        builder.append("&c=").append(System.currentTimeMillis());
        builder.append("&FID=").append(PhoneConfig.UID);
        builder.append("&CUSTOM=");
        StringBuilder paramCustom = new StringBuilder();
        try {
            paramCustom.append("mFrom=").append(getFromParam())
                    .append("&mOpenFirst=").append(mUIPlayContext.mOpenFirst)
                    .append("&mCount=").append(mUIPlayContext.isTopic ? mUIPlayContext.topicPlayCount : mUIPlayContext.vodPlayCount)
                    .append("&mVid=").append(mUIPlayContext.videoItem.guid)
                    .append("&mDuration=").append(String.valueOf(mUIPlayContext.videoItem.duration))
                    .append("&mWemedia=").append(mUIPlayContext.videoItem.weMedia.id)
                    .append("&mChannel=").append(mUIPlayContext.channelId)
                    .append("&mSubject=").append(mUIPlayContext.videoItem.topicId != null ? mUIPlayContext.videoItem.topicId : "")
                    .append("&mKeyword=").append(mUIPlayContext.title)
                    .append("&mProv=").append(mSharePreferenceUtils.getProvince())
                    .append("&mCity=").append(mSharePreferenceUtils.getCity());

            logger.debug("request param：{}", paramCustom.toString());

            builder.append(URLEncoderUtils.encodeInUTF8(paramCustom.toString()));

        } catch (UnsupportedEncodingException e) {

            builder.append(paramCustom.toString());
            logger.error("广告贴片请求参数有误 {}", e);
        }
        return builder.toString().replaceAll("null", "");
    }


    /**
     * 用于得到视频播放广告的信息
     */
    public void getADData() {
        mAdvertList = new ArrayList<>();
        if (bean == null || adUnite == null || adUnite.getHead() == null) {
            logger.debug("getAdData bean is null");
            if (mAdListener != null) {
                mAdListener.onAdComplete(mAdvertList);
                mAdvertList.clear();
            }
            return;
        }
        VideoAdInfoDao.getAdInfo(bean.getMultinterface(), getParam(), new Response.Listener<Object>() {
            @Override
            public void onResponse(Object response) {
                if (response != null && !TextUtils.isEmpty(response.toString())) {
                    JSONObject jsonObject = JSON.parseObject(response.toString());
                    List<String> requestList = getRequestList();
                    int len = requestList.size();
                    for (int i = 0; i < len; i++) {
                        try {
                            String id = requestList.get(i);
                            JSONObject value = jsonObject.getJSONObject(id);
                            JSONArray adsArray = value.getJSONArray("ads");
                            if (!adsArray.isEmpty()) {
                                JSONObject realAd = adsArray.getJSONObject(0);
                                VideoAdInfoModel model = new VideoAdInfoModel(realAd);
                                // setId用于前贴广告点击后的广告页面ADActivity中分享统计
                                model.setId(id);
                                mAdvertList.add(model);
                            }
                        } catch (Exception e) {
                            logger.debug("data parse error");
                        }
                    }

                }


                if (mAdListener != null) {
                    mAdListener.onAdComplete(mAdvertList);
                    mAdvertList.clear();
                    return;
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mAdListener != null) {
                    mAdListener.onAdComplete(mAdvertList);
                    mAdvertList.clear();
                    return;
                }
            }
        });
    }


    public void setOnAdSuccessListener(OnAdCompleteListener listener) {
        this.mAdListener = listener;
    }


    /**
     * 广告加载完成的回调
     */
    public interface OnAdCompleteListener {
        void onAdComplete(List<VideoAdInfoModel> mAdList);
    }

    private UIPlayContext mUIPlayContext;

    public void setUIPlayContext(UIPlayContext context) {
        this.mUIPlayContext = context;
    }


    public static final String FROM_WEMEDIA = "wemedia";
    public static final String FROM_SPECIAL = "special";
    public static final String FROM_FOCUS = "focus";
    public static final String FROM_LIANBO = "lianbo";


    private List<String> getRequestList() {
        List<String> result = new ArrayList<>();
        List<String> tem;
        String mFromForAd = getFromParam();
        if (FROM_WEMEDIA.equals(mFromForAd)) {
            tem = adUnite.getHead().getWemedia();
            if (!ListUtils.isEmpty(tem)) {
                result.addAll(tem);
            }
        } else if (FROM_SPECIAL.equals(mFromForAd)) {
            tem = adUnite.getHead().getSpecial();
            if (!ListUtils.isEmpty(tem)) {
                result.addAll(tem);
            }
        } else if (FROM_FOCUS.equals(mFromForAd)) {
            tem = adUnite.getHead().getFocus();
            if (!ListUtils.isEmpty(tem)) {
                result.addAll(tem);
            }
        } else if (FROM_LIANBO.equals(mFromForAd)) {
            tem = adUnite.getHead().getLianbo();
            if (!ListUtils.isEmpty(tem)) {
                result.addAll(tem);
            }
        }
        return result;
    }

    /**
     * 用于广告请求，不同类型，会返回不同广告
     * <p/>
     * 平台标识（不可为空）。
     * wemedia：自媒体频道内所有栏目
     * special：专题频道
     * lianbo：联播台频道
     * focus：焦点频道
     */
    String getFromParam() {
        if (mUIPlayContext.isTopic) {
            if (CheckIfengType.isFocus(mUIPlayContext.mFromForAd)) {
                return FROM_FOCUS;
            }
            if (CheckIfengType.isLianBo(mUIPlayContext.mFromForAd)) {
                return FROM_LIANBO;
            }
            return FROM_SPECIAL;
        }
        return FROM_WEMEDIA;
    }

    /**
     * 是否为广告片头
     */
    public static boolean isSponsorHead(VideoAdInfoModel adInfoModel) {
        return adInfoModel != null && SPONSOR_HEAD.equalsIgnoreCase(adInfoModel.getPos());
    }

}
