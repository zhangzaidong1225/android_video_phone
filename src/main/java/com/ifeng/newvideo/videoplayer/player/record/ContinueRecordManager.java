package com.ifeng.newvideo.videoplayer.player.record;

import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.ContinueSplayRecord;
import com.ifeng.newvideo.videoplayer.player.IPlayer;

/**
 * Created by fanshell on 2016/12/5.
 * 用于实现连播行为
 */
public class ContinueRecordManager {

    //    private ContinueSplayRecord mLandRecord;
//    private ContinueSplayRecord mPortRecord;
    private ContinueSplayRecord mContinueSplayRecord;
    private boolean mIsBigPicChannel;
    String mEchid, mChid;

    public ContinueRecordManager(final String type) {
        mContinueSplayRecord = new ContinueSplayRecord();
        if (type.equals(ContinueSplayRecord.TYPE_PIP_PLAYER)) {
            mContinueSplayRecord.type = ContinueSplayRecord.TYPE_PIP_PLAYER;
        } else {
            this.mIsBigPicChannel = type.equals(ContinueSplayRecord.TYPE_PIC_PLAYER);
            if (mIsBigPicChannel) {
                mContinueSplayRecord.type = ContinueSplayRecord.TYPE_PIC_PLAYER;
            } else {
                mContinueSplayRecord.type = ContinueSplayRecord.TYPE_V_PLAYER;
            }
        }
//        mLandRecord = new ContinueSplayRecord();
//        mLandRecord.type = ContinueSplayRecord.TYPE_V_PLAYER;
//
//        mPortRecord = new ContinueSplayRecord();
//        mPortRecord.type = ContinueSplayRecord.TYPE_H_PLAYER;
    }


    public void init(String echid, String chid) {
        mEchid = echid;
        mChid = chid;
        if (mContinueSplayRecord != null) {
            mContinueSplayRecord.echid = echid;
            mContinueSplayRecord.chid = chid;
        }
//        mLandRecord.echid = echid;
//        mLandRecord.chid = chid;
//        mPortRecord.echid = echid;
//        mLandRecord.chid = chid;
    }


    public void insertCustomerStatistics(IPlayer.PlayerState state, String currentPath) {
        switch (state) {
            case STATE_PLAYING:
                if (currentPath != null && !currentPath.equals(mContinueSplayRecord.path)) {
                    mContinueSplayRecord.path = currentPath;
                    mContinueSplayRecord.number++;
                }
                mContinueSplayRecord.startCountTime();
                break;
            case ORIENTATION_LANDSCAPE:
                sendContinuePlayRecordIfNecessary();
                mContinueSplayRecord = new ContinueSplayRecord();
                mContinueSplayRecord.echid = mEchid;
                mContinueSplayRecord.chid = mChid;
                mContinueSplayRecord.type = ContinueSplayRecord.TYPE_H_PLAYER;
                break;
            case ORIENTATION_PORTRAIT:
                sendContinuePlayRecordIfNecessary();
                mContinueSplayRecord = new ContinueSplayRecord();
                mContinueSplayRecord.echid = mEchid;
                mContinueSplayRecord.chid = mChid;
                if (mIsBigPicChannel) {
                    mContinueSplayRecord.type = ContinueSplayRecord.TYPE_PIC_PLAYER;
                } else {
                    mContinueSplayRecord.type = ContinueSplayRecord.TYPE_V_PLAYER;
                }
                break;
            default:
                break;
        }
    }

    public void sendContinuePlayRecordIfNecessary() {
        if (mContinueSplayRecord != null && mContinueSplayRecord.number >= 2) {
            mContinueSplayRecord.endCountTime();
            CustomerStatistics.sendContinueSplazyRecord(mContinueSplayRecord);
            mContinueSplayRecord.number = 0;
        }

    }

    public void onDestroy() {
        sendContinuePlayRecordIfNecessary();
    }


    /* 暂时注掉,先按ios的简单逻辑走
    public void insertCustomerStatistics(IPlayer.PlayerState state, String currentPath) {

        boolean isLand = ScreenUtils.isLand(IfengApplication.getAppContext());
        switch (state) {
            case STATE_PLAYING:
                //依据方向进行加
                if (!currentPath.equals(mLandRecord.path)) {
                    mLandRecord.path = currentPath;
                    mPortRecord.path = currentPath;
                    if (isLand) {
                        mLandRecord.number++;
                    } else {

                        mPortRecord.number++;
                    }
                }
                if (isLand) {
                    mLandRecord.startCountTime();
                } else {
                    mPortRecord.startCountTime();

                }
                break;
            case STATE_PAUSED:
                if (isLand) {
                    mLandRecord.pauseTime();
                } else {
                    mPortRecord.pauseTime();
                }

                break;
            case ORIENTATION_LANDSCAPE:
                mLandRecord.number++;
                mPortRecord.pauseTime();
                mLandRecord.startCountTime();

                break;
            case ORIENTATION_PORTRAIT:
                mPortRecord.number++;
                mLandRecord.pauseTime();
                mPortRecord.startCountTime();

                break;
            default:
                break;
        }
    }

    private void sendLandRecord() {
        if (mLandRecord.number >= 2) {
            mLandRecord.endCountTime();
            CustomerStatistics.sendContinueSplazyRecord(mLandRecord);
            mLandRecord.number = 0;
        }
    }

    private void sendPortRecord() {
        if (mPortRecord.number >= 2) {
            mPortRecord.endCountTime();
            CustomerStatistics.sendContinueSplazyRecord(mPortRecord);
            mPortRecord.number = 0;
        }

    }
     public void onDestroy() {
        sendLandRecord();
        sendPortRecord();
    }

    */

}
