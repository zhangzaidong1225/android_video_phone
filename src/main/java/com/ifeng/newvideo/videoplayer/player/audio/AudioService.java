package com.ifeng.newvideo.videoplayer.player.audio;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityAudioFMPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityCacheVideoPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.OnPlayStateListener;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.player.record.AudioRecordCountManager;
import com.ifeng.newvideo.videoplayer.widget.skin.INetWorkListener;
import com.ifeng.newvideo.videoplayer.widget.skin.NetworkConnectionReceiver;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BuildUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.model.HistoryModel;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by fanshell on 2016/11/16.
 */
public class AudioService extends Service implements OnPlayStateListener {

    private Logger logger = LoggerFactory.getLogger(AudioService.class);

    public static final String CMD_PLAY = "play";
    public static final String CMD_PAUSE = "pause";
    public static final String CMD_START = "start";
    public static final String CMD_STOP = "stop";
    public static final String CMD_SEEK = "seek";
    public static final String CMD_NEXT = "next";
    public static final String ACTION_NOTIFICATION_FINISH = "action_notification_finish";
    public static final String ACTION_VIRTUAL_FINISH_LIVE = "action_virtual_finish_live";//已经播放其他音频,直播页再可视时应播放视频,场景:直播播音频,切到fm也播放音频,再回看直播
    public static final String CMD_PREVIOUS = "previous";
    public static final String EXTRA_PATH = "path";
    public static final String EXTRA_POSITION = "position";
    public static final String SOURCE_CLASS = "source_class";
    public static final String ACTION_ONCREATE = "action_oncreate";
    public static final String ACTION_ONDESTROY = "action_ondestroy";
    public static final String ACTION_ONPAUSE = "action_onpause";
    public static final String ACTION_ONSTART = "action_onstart";
    public static final String ACTION_ONERROR = "action_onerror";
    private static final int NOTIFICATION_ID = 1123;

    private IPlayer.PlayerState mState = IPlayer.PlayerState.STATE_IDLE;

    private AudioPlayer mAudioPlayer;

    @VideoSkin.SkinType
    private int mSkinType = VideoSkin.SKIN_TYPE_SPECIAL;

    private OnPlayStateListener mOnPlayStateListener;

    private NetworkConnectionReceiver mNetWorkReceiver;
    private MyBinder mBinder;

    @Override
    public void onCreate() {
        super.onCreate();
        logger.debug("onCreate");
        mAudioPlayer = new AudioPlayer(this);
        ActivityMainTab.mAudioService = this;
        mAudioPlayer.setOnPlayStateListener(this);
        mNetWorkReceiver = new NetworkConnectionReceiver(this);
        mNetWorkReceiver.setNetWorkListener(mNetWorkListener);
        mNetWorkReceiver.registerReceiver();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logger.debug("onStartCommand");
        if (intent == null) {
            return START_STICKY;
        }

        String action = intent.getAction();
        logger.debug(action);
        @VideoSkin.SkinType int newSkinType = intent.getIntExtra(SOURCE_CLASS, mSkinType);
        boolean isCheckCommand = CMD_PLAY.equals(action) || CMD_PREVIOUS.equals(action) || CMD_NEXT.equals(action) || CMD_START.equals(action);
        //checkMobile
        if (isCheckCommand) {
            if (!isValidateNetWork(newSkinType)) {
                return START_STICKY;
            }
        }

        if (CMD_PLAY.equals(action)) {
            String path = intent.getStringExtra(EXTRA_PATH);
            logger.debug("newSkinType:{}", newSkinType);
            //if (!TextUtils.isEmpty(path) && newSkinType != VideoSkin.SKIN_TYPE_SPECIAL) {
            handleSourceCode(newSkinType);
            mSkinType = newSkinType;
            long position = intent.getLongExtra(EXTRA_POSITION, 0);
            mAudioPlayer.play(path, position);
            //}
        } else if (CMD_START.equals(action)) {
            start();
        } else if (CMD_PAUSE.equals(action)) {
            saveHistory(getCurrentPosition());
            pause();
        } else if (CMD_SEEK.equals(action)) {
            long position = intent.getExtras().getLong(EXTRA_POSITION);
            mAudioPlayer.seekTo(position);
        } else if (CMD_STOP.equals(action)) {
            saveHistory(getCurrentPosition());
            Activity mActivity = IfengApplication.getInstance().getTopActivity();
            if (mActivity != null) {
                if (mActivity instanceof ActivityMainTab) {
                    ((ActivityMainTab) mActivity).switchFragmentLiveController();
                } else if (mActivity instanceof ActivityAudioFMPlayer
                        || mActivity instanceof ActivityCacheVideoPlayer
                        || mActivity instanceof ActivityTopicPlayer
                        || mActivity instanceof ActivityVideoPlayerDetail) {
                    mActivity.finish();
                }
            } else if (mAudioPlayer != null) {
                mAudioPlayer.stop();
                //TODO:FragmentLive 不是可视时,此时服务连接断开,mActivity为null,需停音频并播视频
                Intent finish = new Intent();
                finish.setAction(ACTION_NOTIFICATION_FINISH);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(finish);
            }
            stopSelf();
        } else if (CMD_PREVIOUS.equals(action)) {
            saveHistory(getCurrentPosition());
            mAudioPlayer.playPre();
        } else if (CMD_NEXT.equals(action)) {
            saveHistory(getCurrentPosition());
            mAudioPlayer.playNext();
        }
        return START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        logger.debug("onUnbind");
        mOnPlayStateListener = null;
        mAudioPlayer.setOnAutoPlayListener(null);
        return true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        logger.debug("onBind");
        if (mBinder == null) {
            mBinder = new MyBinder();
        }
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        logger.debug("onRebind");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.debug("onDestroy");
        saveHistory(getCurrentPosition());
        if (mAudioPlayer != null) {
            mAudioPlayer.stop();
        }

        stopForeground(true);
        PlayQueue.getInstance().onDestroy();
        ActivityMainTab.mAudioService = null;
        Intent destroy = new Intent(ACTION_ONDESTROY);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(destroy);

        mNetWorkReceiver.setNetWorkListener(null);
        mNetWorkReceiver.unregisterReceiver();

    }

    public class MyBinder extends Binder {
        public AudioService getAudioService() {
            return AudioService.this;
        }
    }

    /**
     * @param listener
     * @return 最新的播放状态
     */
    public IPlayer.PlayerState setOnPlayStateListener(OnPlayStateListener listener) {
        mOnPlayStateListener = listener;
        return mState;
    }

    public void setOnAutoPlayListener(IPlayController.OnAutoPlayListener listener) {
        mAudioPlayer.setOnAutoPlayListener(listener);
    }

    public boolean isPlaying() {
        return mAudioPlayer != null && mAudioPlayer.isPlaying();
    }

    public void pause() {
        if (mAudioPlayer != null) {
            mAudioPlayer.pause();
            sendPlayStateBoradCast(ACTION_ONPAUSE);
        }
    }

    public void start() {
        if (mAudioPlayer != null) {
            mAudioPlayer.start();
            sendPlayStateBoradCast(ACTION_ONSTART);
        }
    }

    public long getDuration() {
        if (mAudioPlayer != null) {
            return mAudioPlayer.getDuration();
        }
        return 0L;
    }

    public void seekTo(long position) {
        if (mAudioPlayer != null) {
            mAudioPlayer.seekTo(position);
        }
    }

    public long getCurrentPosition() {
        if (mAudioPlayer != null) {
            return mAudioPlayer.getCurrentPosition();
        }
        return 0;
    }

    private void showControlBarNotification(final IPlayer.PlayerState state) {
        logger.debug("showControlBarNotification:{}", state);
        switch (state) {
            case STATE_PAUSED:
            case STATE_PLAYING:
                VideoItem videoItem = PlayQueue.getInstance().getCurrentVideoItem();
                TVLiveInfo tvLiveInfo = PlayQueue.getInstance().getTVLiveInfo();

                String title1 = null;
                String image = null;
                if (videoItem != null) {
                    title1 = TextUtils.isEmpty(videoItem.title) ? videoItem.name : videoItem.title;
                    image = videoItem.image;
                } else if (tvLiveInfo != null) {
                    title1 = tvLiveInfo.getcName();
                    image = tvLiveInfo.getImg490_490();
                }
                final String title = title1;
                if (TextUtils.isEmpty(image)) {
                    logger.debug("mPicUrl is null");
                    configNotificationController(state, null, title);
                } else {
                    VolleyHelper.getImageLoader().get(image, new ImageLoader.ImageListener() {

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                            if (response != null && response.getBitmap() != null) {
                                configNotificationController(state, response.getBitmap(), title);
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            logger.debug("onErrorResponse");
                            configNotificationController(state, null, title);
                        }
                    });
                }
                break;
            case STATE_ERROR:
                stopForeground(true);
                sendPlayStateBoradCast(ACTION_ONERROR);
                break;
        }

    }

    private void configNotificationController(IPlayer.PlayerState state, Bitmap bitmap, String title) {
        logger.debug("configNotificationController:{}", bitmap);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        RemoteViews mNormalRemoteViews = new RemoteViews(getPackageName(), R.layout.notification_audio_normal);
        RemoteViews mBigRemoteViews = null;

        mNormalRemoteViews.setTextViewText(R.id.news_title, title);
        mNormalRemoteViews.setImageViewResource(R.id.statusbar_news_image, R.drawable.ic_launcher);
        builder.setContent(mNormalRemoteViews);
        builder.setOngoing(true);
        builder.setContentTitle(getString(R.string.app_name));
        builder.setSmallIcon(R.drawable.ic_launcher_small);
        builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher));
        Intent intent = createIntent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        if (IPlayer.PlayerState.STATE_PAUSED == state) {
            mNormalRemoteViews.setImageViewResource(R.id.statusbar_super_content_pause_or_play, R.drawable.status_bar_play);
        } else {
            mNormalRemoteViews.setImageViewResource(R.id.statusbar_super_content_pause_or_play, R.drawable.status_bar_pause);
        }
        Notification notification = builder.build();
        if (BuildUtils.hasJellyBean()) {
            mBigRemoteViews = getBigContentView(title, bitmap, state);
            notification.bigContentView = mBigRemoteViews;
        }
        configItemClickPendingIntent(mNormalRemoteViews, mBigRemoteViews, state);
        startForeground(NOTIFICATION_ID, notification);
    }

    private void configItemClickPendingIntent(@NonNull RemoteViews normalRemoteViews, RemoteViews bigRemoteViews, IPlayer.PlayerState state) {
        Intent finish_intent = new Intent(this, AudioService.class);
        finish_intent.setAction(CMD_STOP);
        PendingIntent finish = PendingIntent.getService(this, 0, finish_intent, 0);

        Intent start_pause_intent = new Intent(this, AudioService.class);
        if (IPlayer.PlayerState.STATE_PAUSED == state) {
            start_pause_intent.setAction(CMD_START);
        } else if (IPlayer.PlayerState.STATE_PLAYING == state) {
            start_pause_intent.setAction(CMD_PAUSE);
        }
        PendingIntent start_pause = PendingIntent.getService(this, 0, start_pause_intent, 0);

        if (mSkinType == VideoSkin.SKIN_TYPE_TV) {
            normalRemoteViews.setImageViewResource(R.id.statusbar_super_content_next_btn, R.drawable.normal_status_bar_next);
            if (bigRemoteViews != null) {
                bigRemoteViews.setOnClickPendingIntent(R.id.statusbar_finish_btn, finish);
                bigRemoteViews.setOnClickPendingIntent(R.id.statusbar_big_content_pause_or_play, start_pause);
                bigRemoteViews.setImageViewResource(R.id.statusbar_big_content_prev_btn, R.drawable.big_statusbar_bar_previous);
                bigRemoteViews.setImageViewResource(R.id.statusbar_big_content_next_btn, R.drawable.big_statusbar_bar_next);
            }
        } else {
            Intent next_intent = new Intent(this, AudioService.class);
            next_intent.setAction(CMD_NEXT);
            PendingIntent next = PendingIntent.getService(this, 0, next_intent, 0);
            normalRemoteViews.setOnClickPendingIntent(R.id.statusbar_super_content_next_btn, next);
            if (bigRemoteViews != null) {
                bigRemoteViews.setOnClickPendingIntent(R.id.statusbar_finish_btn, finish);
                bigRemoteViews.setOnClickPendingIntent(R.id.statusbar_big_content_pause_or_play, start_pause);
                bigRemoteViews.setOnClickPendingIntent(R.id.statusbar_big_content_next_btn, next);

                Intent previous_intent = new Intent(this, AudioService.class);
                previous_intent.setAction(CMD_PREVIOUS);
                PendingIntent previous = PendingIntent.getService(this, 0, previous_intent, 0);
                bigRemoteViews.setOnClickPendingIntent(R.id.statusbar_big_content_prev_btn, previous);
            }
        }
        normalRemoteViews.setOnClickPendingIntent(R.id.statusbar_super_content_pause_or_play, start_pause);
        normalRemoteViews.setOnClickPendingIntent(R.id.statusbar_finish_btn, finish);


    }

    private Class getTargetClass() {
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_FM:
                return ActivityAudioFMPlayer.class;
            case VideoSkin.SKIN_TYPE_VOD:
                return ActivityVideoPlayerDetail.class;
            case VideoSkin.SKIN_TYPE_TV:
                return ActivityMainTab.class;
            case VideoSkin.SKIN_TYPE_TOPIC:
                return ActivityTopicPlayer.class;
            case VideoSkin.SKIN_TYPE_LOCAL:
                return ActivityCacheVideoPlayer.class;
            default:
                logger.warn("getTargetClass return null");
                return null;
        }
    }

    @Override
    public void updatePlayStatus(IPlayer.PlayerState state, Bundle data) {
        mState = state;
        showControlBarNotification(state);
        if (mOnPlayStateListener != null) {
            mOnPlayStateListener.updatePlayStatus(state, data);
        }
        //handle count v
        handleCount(state);
    }

    private RemoteViews getBigContentView(String title, Bitmap bitmap, IPlayer.PlayerState state) {
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.notification_audio_expand);
        if (bitmap != null) {
            views.setImageViewBitmap(R.id.statusbar_big_news_image, bitmap);
        } else {
            views.setImageViewResource(R.id.statusbar_big_news_image, R.drawable.audio_statusbar_big_img);
        }

        views.setTextViewText(R.id.statusbar_news_name, title);
        if (IPlayer.PlayerState.STATE_PAUSED == state) {
            views.setImageViewResource(R.id.statusbar_big_content_pause_or_play, R.drawable.status_bar_play);
        } else {
            views.setImageViewResource(R.id.statusbar_big_content_pause_or_play, R.drawable.status_bar_pause);
        }
        views.setImageViewResource(R.id.statusbar_big_content_prev_btn, R.drawable.statusbar_big_pre);
        views.setImageViewResource(R.id.statusbar_big_content_next_btn, R.drawable.statusbar_big_next);

        views.setImageViewResource(R.id.statusbar_finish_btn, R.drawable.audio_finish_btn);
        return views;
    }

    public static final String EXTRA_FROM = "service_from";

    public void backToActivity() {
        startActivity(createIntent());
    }

    private Intent createIntent() {
        Class<?> clazz = getTargetClass();
        if (clazz == null) {
            return new Intent();
        }
        Intent intent = new Intent(this, clazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(EXTRA_FROM, true);
        return intent;
    }

    private void handleSourceCode(@VideoSkin.SkinType int newSourceCode) {
        logger.debug("handleSourceCode:{}", newSourceCode);
        if (mSkinType == VideoSkin.SKIN_TYPE_TV && newSourceCode != VideoSkin.SKIN_TYPE_TV) {
            Intent finish = new Intent();
            finish.setAction(ACTION_VIRTUAL_FINISH_LIVE);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(finish);
        }
        Intent playIntent = new Intent();
        playIntent.setAction(ACTION_ONSTART);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(playIntent);

    }

    private void sendPlayStateBoradCast(String action) {
        Intent finish = new Intent();
        finish.setAction(action);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(finish);
    }


    /**
     * 判断网络
     *
     * @return
     */
    private boolean isValidateNetWork(int sourceCode) {
        boolean result = true;
        if (!NetUtils.isNetAvailable(this) && (sourceCode != VideoSkin.SKIN_TYPE_LOCAL)) {
            result = false;
        } else if (NetUtils.isMobile(this) && !IfengApplication.mobileNetCanPlay && (sourceCode != VideoSkin.SKIN_TYPE_LOCAL)) {
            result = false;
        }
        return result;
    }

    /**
     * 保存看过记录
     *
     * @param playPosition 当前播放时长
     */
    private void saveHistory(long playPosition) {
        if (mSkinType == VideoSkin.SKIN_TYPE_TV) {
            return;
        }

        VideoItem item = PlayQueue.getInstance().getCurrentVideoItem();
        if (item == null || ListUtils.isEmpty(item.videoFiles)) {
            return;
        }

        if (playPosition < 1000) {
            return;
        }
        HistoryModel model = new HistoryModel();
        model.setProgramGuid(item.guid);
        model.setColumnName(item.cpName);
        String title;
        if (TextUtils.isEmpty(item.title)) {
            title = item.name;
        } else {
            title = item.title;
        }
        model.setName(title);
        if (mSkinType == VideoSkin.SKIN_TYPE_LOCAL) {
            String url = StreamUtils.getMediaUrl(item.videoFiles);
            model.setType(TextUtils.isEmpty(url) ? HistoryModel.HISTORY_FM_TYPE : HistoryModel.TYPE_VOD);
            model.setResource(TextUtils.isEmpty(url) ? HistoryModel.RESOURCE_AUDIO : HistoryModel.RESOURCE_VIDEO);
        } else {
            model.setType(mSkinType == VideoSkin.SKIN_TYPE_FM ? HistoryModel.HISTORY_FM_TYPE : HistoryModel.TYPE_VOD);
            model.setResource(mSkinType == VideoSkin.SKIN_TYPE_FM ? HistoryModel.RESOURCE_AUDIO : HistoryModel.RESOURCE_VIDEO);
        }
        logger.debug("saveHistory()  type={},resource={}", model.getType(), model.getResource());
        model.setImgUrl(item.image);
        model.setBookMark(playPosition);
        model.setVideoDuration(item.duration);
        model.setTopicId(item.topicId);
        model.setTopicMemberType(item.topicType);
        model.setFmProgramId(item.itemId);
        int fileSize;
        if (!ListUtils.isEmpty(item.videoFiles) && item.videoFiles.get(0) != null) {
            fileSize = item.videoFiles.get(0).filesize;
        } else {
            fileSize = 0;
        }
        model.setFileSize(fileSize);
        model.setFmUrl(item.mUrl);
        model.setSimId(item.simId);
        logger.debug("saveHistory() audioUrl={}", model.getFmUrl());
        HistoryDAO.getInstance(IfengApplication.getInstance()).saveHistoryData(model);
    }


    private AudioRecordCountManager mAudioRecordCount;

    /**
     * 用于做统计的处理
     *
     * @param state
     */
    private void handleCount(IPlayer.PlayerState state) {
        if (mAudioRecordCount == null) {
            mAudioRecordCount = new AudioRecordCountManager();
        }
        mAudioRecordCount.insertCustomerStatistics(state, mSkinType);

    }


    /**
     * 用于监听网络变化
     */
    private INetWorkListener mNetWorkListener = new INetWorkListener() {
        @Override
        public void onNetWorkChange() {
            Context context = IfengApplication.getAppContext();
            if (!NetUtils.isNetAvailable(context) && (mSkinType != VideoSkin.SKIN_TYPE_LOCAL)) {
                //无网,且不是本地播放
                pause();
                stopForeground(true);
            } else if (NetUtils.isMobile(context) && !IfengApplication.mobileNetCanPlay && (mSkinType != VideoSkin.SKIN_TYPE_LOCAL)) {
                //数据网不能播放,不是本地播放,
                pause();
                stopForeground(true);
            }

        }
    };

    public static interface CanPlayAudio {
        boolean isPlayAudio();
    }

    public void stopAudioService(@VideoSkin.SkinType int skinType) {
        //目前只有点播底页和直播tv接收此广播
        if (skinType != VideoSkin.SKIN_TYPE_EXIT && (mSkinType == VideoSkin.SKIN_TYPE_VOD || mSkinType == VideoSkin.SKIN_TYPE_TV) && mSkinType != skinType) {
            Intent finish = new Intent();
            finish.setAction(ACTION_VIRTUAL_FINISH_LIVE);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(finish);
        }
        stopSelf();
    }


}
