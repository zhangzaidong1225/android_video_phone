package com.ifeng.newvideo.videoplayer.player;

import android.view.Surface;
import android.view.SurfaceHolder;

/**
 * Created by fanshell on 2016/8/1.
 * 用于播放器的通用
 */
public interface IPlayer {

    void prepareAsync();

    void setSourcePath(String source);

    void start();

    void pause();

    void stop();

    void seekTo(long position);

    void release();

    boolean isPlaying();

    long getCurrentPosition();

    long getDuration();

    void setDisplay(SurfaceHolder surfaceHolder);

    void setSurface(Surface surface);

    void setOnPlayStateListener(OnPlayStateListener listener);

    void setOnPlayVideoSizeChangeListener(OnPlayVideoSizeChangeListener listener);

    void setLastPosition(long lastPosition);

    int getVideoWidth();

    int getVideoHeight();

    enum PlayerState {
        STATE_ERROR,
        STATE_IDLE,
        STATE_PREPARING,
        STATE_BUFFERING_START,
        STATE_BUFFERING_END,
        STATE_PLAYING,
        STATE_PAUSED,
        STATE_PLAYBACK_COMPLETED,
        STATE_SEEK_END,
        ORIENTATION_LANDSCAPE,
        ORIENTATION_PORTRAIT,
        STREAM_CHANGE,
        STATE_SAVE_HOSITORY,
        PIP_STATE_PLAYING
    }

}
