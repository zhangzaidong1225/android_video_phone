package com.ifeng.newvideo.videoplayer.player.history;

import android.text.TextUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.HistoryDAO;
import com.ifeng.video.dao.db.model.HistoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fanshell on 2016/11/17.
 */
public class HistoryManager {

    /**
     * 记录看过的处理
     */
    private HistoryDAO mHistoryDao;

    public HistoryManager() {
        mHistoryDao = HistoryDAO.getInstance(IfengApplication.getInstance());
    }


    Logger logger = LoggerFactory.getLogger(HistoryManager.class);

    /**
     * 保存看过记录
     *
     * @param isComplete
     * @param resourceType 资源类型：视频或音频 HistoryModel.RESOURCE_VIDEO，HistoryModel.RESOURCE_AUDIO
     * @param type         类型：点播，专题，VR
     */
    public void saveHistory(boolean isComplete, VideoItem item, String resourceType, String type, long duration) {
        logger.debug("saveHistory() item is null = {} ,type={}", item == null, type);
        if (item == null || TextUtils.isEmpty(type)) {
            return;
        }
        HistoryModel model = new HistoryModel();
        model.setProgramGuid(item.guid);
        model.setColumnName(item.cpName);
        String title;
        if (TextUtils.isEmpty(item.title)) {
            title = item.name;
        } else {
            title = item.title;
        }
        model.setName(title);
        model.setType(type);
        model.setResource(resourceType);
        model.setImgUrl(item.image);
        model.setBookMark(isComplete ? (item.duration * 1000) : duration);
        model.setVideoDuration(item.duration);
        model.setTopicId(item.topicId);
        model.setTopicMemberType(item.topicType);
        model.setFmProgramId(item.itemId);
        model.setSimId(item.simId);
        int fileSize;
        if (!ListUtils.isEmpty(item.videoFiles) && item.videoFiles.get(0) != null) {
            fileSize = item.videoFiles.get(0).filesize;
        } else {
            fileSize = 0;
        }
        model.setFileSize(fileSize);
        if (HistoryModel.RESOURCE_AUDIO.equals(resourceType)) {
            model.setFmUrl(item.mUrl);
        }
        mHistoryDao.saveHistoryData(model);

    }
}
