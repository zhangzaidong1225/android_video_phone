package com.ifeng.newvideo.videoplayer.player.playController;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.OnPlayStateListener;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.player.history.HistoryManager;
import com.ifeng.newvideo.videoplayer.widget.skin.OrientationSensorUtils;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by fanshell on 2016/11/17.
 */
public class AudioPlayController extends BasePlayController implements IPlayController, OnPlayStateListener {

    private static final Logger logger = LoggerFactory.getLogger(AudioPlayController.class);
    private volatile boolean isInitFinish;
    private AudioService mAudioService;
    private IPlayer.PlayerState mCurrentOrientation;

    public AudioPlayController() {
        isInitFinish = false;
    }

    @Override
    public void init(VideoSkin skin, UIPlayContext context) {
        if (isInitFinish) {
            return;
        }
        isInitFinish = true;
        mVideoSkin = skin;
        this.mUIPlayContext = context;
        mHistoryManger = new HistoryManager();
        mVideoSkin.buildSkin(mUIPlayContext);
        if (mAudioService == null) {
            mAudionConnection = new AudioServiceConnection();
            Intent intent = new Intent(mVideoSkin.getContext(), AudioService.class);
            mVideoSkin.getContext().bindService(intent, mAudionConnection, BIND_AUTO_CREATE);
            mVideoSkin.registerPlayController(this);
        }
    }

    @Override
    public void onResume() {
        reBindAudioServiceIfNecessary();
        if (mVideoSkin != null) {
            mVideoSkin.onResume();
            if (mCurrentOrientation == IPlayer.PlayerState.ORIENTATION_LANDSCAPE) {
                setOrientation(mCurrentOrientation);
            }
        }
    }

    @Override
    public void onPause() {
        if (mVideoSkin != null) {
            mVideoSkin.onPause();
            mCurrentOrientation = mUIPlayContext.orientation;
        }
        unbindServiceIfNecessary();
    }

    public void onDestroy(boolean stopService) {
        super.onDestroy();
        stopService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void rePlaySource(String path) {
        playSource(path);
    }

    @Override
    public void playSource(String path) {
        play(path, lastPosition);
        lastPosition = 0;
        if (mVideoSkin.getSkinType() == VideoSkin.SKIN_TYPE_LOCAL) {
            setOrientation(IPlayer.PlayerState.ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    public void onConfigureChange(Configuration newConfig) {
        mVideoSkin.changeLayoutParams();
        setOrientation(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? IPlayer.PlayerState.ORIENTATION_LANDSCAPE : IPlayer.PlayerState.ORIENTATION_PORTRAIT);

    }

    @Override
    public long getCurrentPosition() {
        if (mAudioService != null) {
            return mAudioService.getCurrentPosition();
        }
        return 0;
    }

    @Override
    public void setProgrammeAutoNext(boolean isAuto) {

    }

    @Override
    public void autoNextProgramme() {
    }


    @Override
    public boolean isPlaying() {
        return mAudioService != null && mAudioService.isPlaying();
    }

    @Override
    public void pause() {
        if (mAudioService != null) {
            mAudioService.pause();
        }
    }

    @Override
    public void start() {
        if (mAudioService != null) {
            mAudioService.start();
        }
    }

    @Override
    public long getDuration() {
        if (mAudioService != null) {
            return mAudioService.getDuration();
        }
        return 0L;
    }

    @Override
    public void seekTo(long position) {
        if (mAudioService != null) {
            mAudioService.seekTo(position);
        }
    }

    @Override
    public long getLastPosition() {
        return lastPosition;
    }

    @Override
    public int getPlayerType() {
        return 0;
    }

    @Override
    public void stop() {

    }

    private AudioServiceConnection mAudionConnection;

    private void play(String path, long seek) {
        Bundle bundle = new Bundle();
        bundle.putString(AudioService.EXTRA_PATH, path);
        bundle.putLong(AudioService.EXTRA_POSITION, seek);
        bundle.putInt(AudioService.SOURCE_CLASS, mVideoSkin.getSkinType());
        Intent intent = new Intent(mVideoSkin.getContext(), AudioService.class);
        intent.putExtras(bundle);
        intent.setAction(AudioService.CMD_PLAY);
        mVideoSkin.getContext().startService(intent);
        mVideoSkin.registerPlayController(this);

    }

    @Override
    public void updatePlayStatus(IPlayer.PlayerState state, Bundle data) {
        switch (state) {
            case STATE_PLAYING:
                mPreViewVideoItem = mUIPlayContext.videoItem;
                break;
//            case STATE_PAUSED://暂停
//                lastPosition = getCurrentPosition();
//                saveHistory(false);
//                break;
            case STATE_PLAYBACK_COMPLETED:
                saveHistory(true);
            case STATE_SAVE_HOSITORY:
                saveHistory(false, getCurrentPosition());
                break;
        }
        logger.debug("status:{}", state.toString());
        mCurrentState = state;
        mBundle = data;
        if (mBundle == null) {
            mBundle = new Bundle();
        }
        mBundle.putBoolean("isAudio", true);
        notifyObserver();
    }

    @Override
    public void setOrientation(IPlayer.PlayerState orientation) {
        mBundle.clear();
        mBundle.putBoolean("isAudio", true);
        mBundle.putSerializable(OrientationSensorUtils.KEY_ORIENTATION, orientation);
        IPlayer.PlayerState oldState = mCurrentState;
        mCurrentState = orientation;
        notifyObserver();
        mCurrentState = oldState;
    }

    private class AudioServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof AudioService.MyBinder) {
                AudioService.MyBinder myBinder = (AudioService.MyBinder) iBinder;
                mAudioService = myBinder.getAudioService();
                if (mAudioService != null) {
                    IPlayer.PlayerState state = mAudioService.setOnPlayStateListener(AudioPlayController.this);
                    updatePlayStatus(state, mBundle);
                    if (mAutoPlayListener != null) {
                        mAudioService.setOnAutoPlayListener(mAutoPlayListener);
                    }
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mAudioService = null;
            mAutoPlayListener = null;
        }


    }

    public void unbindServiceIfNecessary() {
        logger.debug("unbindServiceIfNecessary");
        if (mAudioService != null && mAudionConnection != null) {
            mVideoSkin.getContext().unbindService(mAudionConnection);
            mAudionConnection = null;
            mAudioService = null;
        }
    }

    private void stopService() {
        logger.debug("stopService");
        Intent intent = new Intent(mVideoSkin.getContext(), AudioService.class);
        mVideoSkin.getContext().stopService(intent);
    }

    @Override
    public void setOnAutoPlayListener(OnAutoPlayListener listener) {
        super.setOnAutoPlayListener(listener);
        if (mAudioService != null) {
            mAudioService.setOnAutoPlayListener(mAutoPlayListener);
        }
    }

    private boolean reBindAudioServiceIfNecessary() {
        //mAudionConnection判空避免 init方法中绑定和此处重复
        if (mAudionConnection == null && mAudioService == null &&
                ActivityMainTab.mAudioService != null) {
            logger.debug("reBindAudioService");
            mAudionConnection = new AudioServiceConnection();
            Intent intent = new Intent(mVideoSkin.getContext(), AudioService.class);
            mVideoSkin.registerPlayController(this);
            return mVideoSkin.getContext().bindService(intent, mAudionConnection, BIND_AUTO_CREATE);
        }
        return false;
    }


}
