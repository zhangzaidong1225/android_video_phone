package com.ifeng.newvideo.videoplayer.player;

import android.content.res.Configuration;
import android.support.annotation.IntDef;
import android.text.TextUtils;
import android.view.View;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.statistics.domains.ContinueSplayRecord;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.TopicDetailList;
import com.ifeng.newvideo.videoplayer.player.playController.AudioPlayController;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.player.playController.VideoPlayController;
import com.ifeng.newvideo.videoplayer.player.record.ContinueRecordManager;
import com.ifeng.newvideo.videoplayer.widget.skin.TitleView;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.ClickUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * Created by fanshell on 2016/8/1.
 * 使用此类时,必须要调用init()方法
 */
public class NormalVideoHelper {

    private static final Logger logger = LoggerFactory.getLogger(NormalVideoHelper.class);

    private IPlayController mPlayController;

    /**
     * 用于记录当前所作用的播放控制器的类型
     */
    private int mCurrentControllerType;
    /**
     * 视频播放控制器的类型
     */
    public static final int CONTROLLER_TYPE_VIDEO = 0;
    /**
     * 音频播放控制器的类型
     */
    public static final int CONTROLLER_TYPE_AUDIO = 1;

    private boolean hasSetSuccess;
    private ContinueRecordManager mContinueRecordManger;

    private TopicDetailList mTopicList;
    private String mGuid;

    @IntDef({CONTROLLER_TYPE_VIDEO, CONTROLLER_TYPE_AUDIO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ControllerType {
    }


    private VideoSkin mSkin;
    private UIPlayContext mUIPlayContext;

    public NormalVideoHelper() {
        this(CONTROLLER_TYPE_VIDEO);
    }

    public NormalVideoHelper(@ControllerType int controllerType) {
        mCurrentControllerType = controllerType;
        mPlayController = createController(controllerType);
    }

    public void init(VideoSkin skin, UIPlayContext uiPlayContext) {
        this.mSkin = skin;
        this.mUIPlayContext = uiPlayContext;
        mPlayController.init(skin, uiPlayContext);
        mSkin.getTitleView().setOnVideoAudioChangeListener(new TitleView.OnVideoAudioChangeListener() {
            @Override
            public void onVideoAudioChange() {
                doVideoAudioChange();
                mSkin.hideController();

            }
        });

        mSkin.getErrorView().findViewById(R.id.video_error_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClickUtils.isFastDoubleClick()) {
                    return;
                }
                doVideoAudioChange();
                mSkin.hideController();
            }
        });
        hasSetSuccess = false;
        createContinueRecordIfNeed();
    }

    public void doVideoAudioChange() {
        changeController();
        String path = "";
        if (mCurrentControllerType == CONTROLLER_TYPE_VIDEO) {
            if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV && mUIPlayContext.tvLiveInfo != null) {//直播视频流地址
                path = StreamUtils.getMediaUrlForTV(mUIPlayContext.tvLiveInfo);
            } else {
                //change current videoItem
                mUIPlayContext.videoItem = PlayQueue.getInstance().getCurrentVideoItem();
                // //when first load error, videoItem is null
                if (mUIPlayContext.videoItem != null) {
                    path = StreamUtils.getMediaUrl(mUIPlayContext.videoItem.videoFiles);
                }

            }
        } else if (mCurrentControllerType == CONTROLLER_TYPE_AUDIO) {
            if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV && mUIPlayContext.tvLiveInfo != null) {//直播音频地址
                path = mUIPlayContext.tvLiveInfo.getAudio();
            } else {
                //when first load error, videoItem is null
                if (mUIPlayContext.videoItem != null) {
                    path = StreamUtils.getMp3Url(mUIPlayContext.videoItem.videoFiles);
                }
            }
        }


        if (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_TV) {
            /**
             * 保存当前播放的列表
             */
            PlayQueue.getInstance().setPlayList(mUIPlayContext.VideoItemList);
            PlayQueue.getInstance().setVideoItem(mUIPlayContext.videoItem);
            if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TOPIC) {
                if (mTopicList != null) {
                    PlayQueue.getInstance().setTopicList(mTopicList);
                }
                PlayQueue.getInstance().setGuid(mGuid);
            }
            PlayQueue.getInstance().setTVLiveInfo(null);
        } else {
            PlayQueue.getInstance().setPlayList(null);
            PlayQueue.getInstance().setVideoItem(null);
            PlayQueue.getInstance().setTVLiveInfo(mUIPlayContext.tvLiveInfo);
            preTime = 0;//音频播放用系统的MediaPlayer底层库各机型不同,部分机型直播的seek会导致播放失败,如小米note
        }

        createContinueRecordIfNeed();
        mPlayController.setBookMark(preTime);
        mPlayController.setFromHistory(true);
        mPlayController.playSource(path);
        UserFeed.initIfengAddress(IfengApplication.getAppContext(), path);
    }

    public void onResume() {
        mPlayController.onResume();
    }

    public void setIsPlayWhenSurfaceCreated(boolean isPlay) {
        if (mPlayController instanceof VideoPlayController) {
            ((VideoPlayController) mPlayController).setIsPlayWhenSurfaceCreated(isPlay);
        }
    }


    public void onPause() {
        mPlayController.onPause();
        if (mPlayController instanceof VideoPlayController && mSkin.getSkinType() == VideoSkin.SKIN_TYPE_PIC && mContinueRecordManger != null) {
            mContinueRecordManger.sendContinuePlayRecordIfNecessary();
        }
    }


    public void onDestroy() {
        mPlayController.onDestroy();
        setOnAutoPlayListener(null);
        setOnPlayCompleteListener(null);
        //大图频道报continueplay单独处理
        if (mContinueRecordManger != null && mSkin.getSkinType() != VideoSkin.SKIN_TYPE_PIC) {
            mContinueRecordManger.onDestroy();
        }
    }

    public void reOpenVideo(String path) {
        mPlayController.rePlaySource(path);
    }

    public void openVideo(String path) {

        if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV) {
            mPlayController.setBookMark(0);
            PlayQueue.getInstance().setTVLiveInfo(mUIPlayContext.tvLiveInfo);
        } else {
            PlayQueue.getInstance().setTVLiveInfo(null);
        }
        mPlayController.playSource(path);
        UserFeed.initIfengAddress(IfengApplication.getAppContext(), path);
    }


    public void onConfigureChange(Configuration newConfig) {
        mPlayController.onConfigureChange(newConfig);

    }

    public void setLeaveStateIsPause(boolean leaveStateIsPause) {
        if (mPlayController instanceof VideoPlayController) {
            ((VideoPlayController) mPlayController).setLeaveStateIsPause(leaveStateIsPause);
        }
    }

    /**
     * 发送播放页锁屏统计
     */
    public void sendScreenOffStatistics() {
        mPlayController.sendScreenOffStatistics();
    }

    private IPlayController.OnAutoPlayListener mAutoPlayListener;

    public void setOnAutoPlayListener(IPlayController.OnAutoPlayListener listener) {
        mAutoPlayListener = listener;
        mPlayController.setOnAutoPlayListener(mAutoPlayListener);
    }

    public void setFromHistory(boolean fromHistory) {
        mPlayController.setFromHistory(fromHistory);
    }

    /**
     * 设置看过的位置
     */
    public void setBookMark(long bookMark) {
        mPlayController.setBookMark(bookMark);
    }

    private IPlayController.OnPlayCompleteListener mOnPlayCompleteListener;

    public void setOnPlayCompleteListener(IPlayController.OnPlayCompleteListener listener) {
        this.mOnPlayCompleteListener = listener;
        mPlayController.setOnPlayCompleteListener(mOnPlayCompleteListener);
    }

    public long getCurrentPosition() {
        return mPlayController.getCurrentPosition();
    }

    public long getLastPosition() {
        return mPlayController.getLastPosition();
    }


    public void setProgrammeAutoNext(boolean isAuto) {
        mPlayController.setProgrammeAutoNext(isAuto);
    }

    /**
     * 节目进行自动播放
     */
    public void autoNextProgramme() {
        mPlayController.autoNextProgramme();

    }


    /**
     * 创建指定类型的插入控制器
     *
     * @param controllerType 类型
     */
    private IPlayController createController(@ControllerType int controllerType) {
        if (controllerType == CONTROLLER_TYPE_VIDEO) {
            return new VideoPlayController();
        } else if (controllerType == CONTROLLER_TYPE_AUDIO) {
            return new AudioPlayController();
        } else {
            throw new RuntimeException("controller type error");
        }
    }


    /**
     * 音视频切换之前的时间
     */
    private long preTime;

    /**
     * 切换播放控制类
     */
    private void changeController() {
        if (mCurrentControllerType == CONTROLLER_TYPE_VIDEO) {
            mCurrentControllerType = CONTROLLER_TYPE_AUDIO;
        } else if (mCurrentControllerType == CONTROLLER_TYPE_AUDIO) {
            mCurrentControllerType = CONTROLLER_TYPE_VIDEO;
        }

        if (mSkin.getSkinType() != VideoSkin.SKIN_TYPE_TV) {
            //直播不需要seek
            preTime = mPlayController.getCurrentPosition();
        }
        if (mPlayController instanceof AudioPlayController) {
            ((AudioPlayController) mPlayController).unbindServiceIfNecessary();
            //保存topList的数据及guid
            if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TOPIC) {
                TopicDetailList tem = PlayQueue.getInstance().getTopicList();
                if (tem != null) {
                    mTopicList = tem;
                }
                String guid = PlayQueue.getInstance().getGuid();
                if (!TextUtils.isEmpty(guid)) {
                    mGuid = guid;
                }
            }
            ((AudioPlayController) mPlayController).onDestroy(true);
        } else {
            mPlayController.onDestroy();
            mSkin.getLoadView().setVisibility(View.VISIBLE);
        }
        mPlayController = null;
        mPlayController = createController(mCurrentControllerType);
        mPlayController.init(mSkin, mUIPlayContext);
        resetListener();
    }

    /**
     * 重新设置之前的监听
     */
    private void resetListener() {
        setOnAutoPlayListener(mAutoPlayListener);
        setOnPlayCompleteListener(mOnPlayCompleteListener);

    }

    /**
     * 判断是否在播放音频
     *
     * @return
     */
    public boolean isPlayAudio() {
        return mCurrentControllerType == CONTROLLER_TYPE_AUDIO;
    }

    public void switchToVideoControllerAndUI() {
        if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV || VideoSkin.SKIN_TYPE_VOD == mSkin.getSkinType()) {
            if (mCurrentControllerType == CONTROLLER_TYPE_AUDIO) {
                mCurrentControllerType = CONTROLLER_TYPE_VIDEO;
                if (mPlayController instanceof AudioPlayController) {
                    ((AudioPlayController) mPlayController).unbindServiceIfNecessary();//unBindService
                    ((AudioPlayController) mPlayController).onDestroy(true);//stopService
                }
                mPlayController.onDestroy();
                mSkin.getLoadView().setVisibility(View.VISIBLE);
                mPlayController = null;
                mPlayController = createController(mCurrentControllerType);
                mPlayController.init(mSkin, mUIPlayContext);
                resetListener();
                String path = "";
                if (mSkin.getSkinType() == VideoSkin.SKIN_TYPE_TV && mUIPlayContext.tvLiveInfo != null) {//直播视频流地址
                    path = StreamUtils.getMediaUrlForTV(mUIPlayContext.tvLiveInfo);
                } else {
                    mUIPlayContext.videoItem = PlayQueue.getInstance().getCurrentVideoItem();
                    if (mUIPlayContext.videoItem != null) {
                        path = StreamUtils.getMediaUrl(mUIPlayContext.videoItem.videoFiles);
                    }
                }
                if (mPlayController instanceof VideoPlayController) {
                    ((VideoPlayController) mPlayController).reSetPlayPath(path);
                }
            }

        }
    }

    public void setPlayPathFromPip(String path) {
        if (mPlayController instanceof VideoPlayController) {
            ((VideoPlayController) mPlayController).reSetPlayPath(path);
        }
    }


    private void createContinueRecordIfNeed() {
        switch (mUIPlayContext.skinType) {
            case VideoSkin.SKIN_TYPE_LOCAL:
            case VideoSkin.SKIN_TYPE_VOD:
            case VideoSkin.SKIN_TYPE_TOPIC:
                if (mContinueRecordManger == null) {
                    mContinueRecordManger = new ContinueRecordManager(ContinueSplayRecord.TYPE_V_PLAYER);
                }
                if (!hasSetSuccess && mPlayController instanceof VideoPlayController) {
                    ((VideoPlayController) mPlayController).setContinueManager(mContinueRecordManger);
                    hasSetSuccess = true;
                }

                break;
            case VideoSkin.SKIN_TYPE_PIC:
                if (mContinueRecordManger == null) {
                    mContinueRecordManger = new ContinueRecordManager(ContinueSplayRecord.TYPE_PIC_PLAYER);
                }
                if (!hasSetSuccess && mPlayController instanceof VideoPlayController) {
                    ((VideoPlayController) mPlayController).setContinueManager(mContinueRecordManger);
                    hasSetSuccess = true;
                }
            default:
                break;
        }

    }

    public IPlayController getPlayController() {
        return mPlayController;
    }

    public boolean isPlaying() {
        return mPlayController.isPlaying();
    }

}
