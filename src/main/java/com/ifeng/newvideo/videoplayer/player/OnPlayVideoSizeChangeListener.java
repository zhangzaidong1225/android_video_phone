package com.ifeng.newvideo.videoplayer.player;

/**
 * Created by fanshell on 2016/9/22.
 */
public interface OnPlayVideoSizeChangeListener {

    void onVideoSizeChangeListener(int width, int height);
}
