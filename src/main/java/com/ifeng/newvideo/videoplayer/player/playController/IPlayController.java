package com.ifeng.newvideo.videoplayer.player.playController;

import android.content.res.Configuration;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.UIObserver;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;

/**
 * Created by fanshell on 2016/11/17.
 * 在使用IPlayController,必须先调用int方法
 */
public interface IPlayController {

    void init(VideoSkin skin, UIPlayContext uiPlayContext);

    void onResume();

    void onPause();

    void onDestroy();

    void rePlaySource(String path);

    void playSource(String path);

    void onConfigureChange(Configuration newConfig);

    /**
     * 发送播放页锁屏统计
     */
    void sendScreenOffStatistics();

    void setOnAutoPlayListener(OnAutoPlayListener listener);

    interface OnAutoPlayListener {
        void onPlayListener(VideoItem item);
    }

    void setFromHistory(boolean fromHistory);

    /**
     * 设置看过的位置
     */
    void setBookMark(long bookMark);

    void setOnPlayCompleteListener(OnPlayCompleteListener listener);

    /**
     * 视频播放完成的监听
     */
    interface OnPlayCompleteListener {

        void onPlayComplete();

    }

    long getCurrentPosition();

    void setProgrammeAutoNext(boolean isAuto);

    void autoNextProgramme();

    void skipAdvert();

    void setOrientation(IPlayer.PlayerState orientation);

    void addUIObserver(UIObserver observer);

    void removeUIObserver(UIObserver observer);

    void setStream(String stream);

    boolean isPlaying();

    void pause();

    void start();

    long getDuration();

    void seekTo(long duration);
    //获取播放暂停位置
    long getLastPosition();

    int getPlayerType();

    void stop();
}
