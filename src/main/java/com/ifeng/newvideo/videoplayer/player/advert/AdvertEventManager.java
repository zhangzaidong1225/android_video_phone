package com.ifeng.newvideo.videoplayer.player.advert;

import android.os.Handler;
import android.text.TextUtils;
import com.android.volley.Request;
import com.ifeng.video.core.net.RequestString;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;

import java.util.ArrayList;

/**
 * Created by fanshell on 2016/11/17.
 */
public class AdvertEventManager {

    /**
     * 用于发送广告中间时间的延时的处理
     */
    private Handler mAdEventHandler;


    public AdvertEventManager() {
        mAdEventHandler = new Handler();
    }

    /**
     * 上报贴片广告的曝光统计
     *
     * @param adEventLog 贴片接口返回的eventLog中的start
     */
    private void sendAdEventLog(ArrayList<String> adEventLog) {
        if (ListUtils.isEmpty(adEventLog)) {
            return;
        }
        for (String impression : adEventLog) {
            if (!TextUtils.isEmpty(impression)) {
                RequestString requestString = new RequestString(Request.Method.GET, impression, null, null, null);
                VolleyHelper.getRequestQueue().add(requestString);
            }
        }
    }

    /**
     * 上报贴片广告的曝光统计
     *
     * @param adEventLog 贴片接口返回的eventLog中的start
     */
    public void sendStartEvent(ArrayList<String> adEventLog) {
        sendAdEventLog(adEventLog);
    }

    /**
     * 上报贴片广告的曝光统计
     *
     * @param adEventLog 贴片接口返回的eventLog中的end
     */
    public void sendEndEvent(ArrayList<String> adEventLog) {
        sendAdEventLog(adEventLog);
    }

    /**
     * 上报贴片广告的曝光统计
     *
     * @param adEventLog 贴片接口返回的eventLog中的start
     */
    public void sendMiddleEventDelay(long delayTime, final ArrayList<String> adEventLog) {
        if (delayTime >= 0 && !ListUtils.isEmpty(adEventLog) && mAdEventHandler != null) {
            mAdEventHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendAdEventLog(adEventLog);
                }
            }, delayTime);
        }
    }

    public void clearEvent() {
        removeADMiddleEvent();
    }


    private void removeADMiddleEvent() {
        if (mAdEventHandler != null) {
            mAdEventHandler.removeCallbacksAndMessages(null);
        }
    }


}
