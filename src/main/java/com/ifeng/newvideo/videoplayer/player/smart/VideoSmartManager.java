package com.ifeng.newvideo.videoplayer.player.smart;

import com.ifeng.newvideo.statistics.smart.SmartStatistic;
import com.ifeng.newvideo.statistics.smart.domains.UserOperator;

/**
 * Created by fanshell on 2016/10/21.
 */
public class VideoSmartManager {

    private SmartStatistic mSmartSatistic;

    public VideoSmartManager() {
        mSmartSatistic = SmartStatistic.getInstance();
    }

    public void send(UserOperator opt) {
        mSmartSatistic.sendSmartStatistic(SmartStatistic.OPERATION_URL, opt);
    }
}
