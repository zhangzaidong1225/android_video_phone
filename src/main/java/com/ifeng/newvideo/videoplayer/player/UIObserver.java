package com.ifeng.newvideo.videoplayer.player;

import android.os.Bundle;

/**
 * Created by fanshell on 2016/8/1.
 */
public interface UIObserver {
    void update(IPlayer.PlayerState status, Bundle bundle);

}
