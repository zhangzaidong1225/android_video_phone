package com.ifeng.newvideo.videoplayer.player;

import com.ifeng.newvideo.videoplayer.bean.TopicDetailList;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;

import java.util.List;

/**
 * Created by fanshell on 2016/11/22.
 */
public class PlayQueue {

    private static volatile PlayQueue instance;
    private List<VideoItem> mPlayList;
    private VideoItem mVideoItem;
    private int mPageNum = 1;
    private TVLiveInfo mTVLiveInfo;

    private TopicDetailList mTopicList;

    private String mEchID;
    /**
     * 用于保存从列表进入点播页面的guid
     */
    private String mGuid;

    private PlayQueue() {

    }

    public static PlayQueue getInstance() {
        if (instance == null) {
            synchronized (PlayQueue.class) {
                if (instance == null) {
                    instance = new PlayQueue();
                }
            }
        }
        return instance;
    }

    public List<VideoItem> getPlayList() {
        return mPlayList;
    }


    public VideoItem getCurrentVideoItem() {
        return mVideoItem;
    }

    public void setPlayList(List<VideoItem> videoItems) {
        this.mPlayList = videoItems;
    }

    public void setVideoItem(VideoItem videoItem) {
        this.mVideoItem = videoItem;
    }


    public void setTopicList(TopicDetailList topicList) {
        this.mTopicList = topicList;
    }

    public TopicDetailList getTopicList() {
        return mTopicList;
    }

    public int getPageNum() {
        return mPageNum;
    }

    public void setPageNum(int mPageNum) {
        this.mPageNum = mPageNum;
    }

    public TVLiveInfo getTVLiveInfo() {
        return mTVLiveInfo;
    }

    public void setTVLiveInfo(TVLiveInfo mTVLiveInfo) {
        this.mTVLiveInfo = mTVLiveInfo;
    }


    public void setEchid(String echID) {
        this.mEchID = echID;
    }

    public String getEchid() {
        return mEchID;
    }

    private String vTag;

    public void setVTag(String vtag) {
        this.vTag = vtag;
    }

    public String getVTag() {
        return vTag;
    }

    public void setGuid(String guid) {
        mGuid = guid;
    }

    public String getGuid() {
        return mGuid;
    }

    public void onDestroy() {
        mEchID = "";
        mVideoItem = null;
        mPlayList = null;
        mTVLiveInfo = null;
        this.mTopicList = null;
        mPageNum = 1;
        mGuid = "";
        vTag = "";

    }
}
