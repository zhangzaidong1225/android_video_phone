package com.ifeng.newvideo.videoplayer.player;

import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.videoplayer.base.VideoPlayerConfig;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.MD5Utils;
import com.ifeng.video.player.ChoosePlayerUtils;
import com.video.videosdk.videoauth.AuthParam;
import com.video.videosdk.videoauth.VideoAuthClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 防盗链工具类
 */
public class PlayUrlAuthUtils {
    private static final Logger logger = LoggerFactory.getLogger(PlayUrlAuthUtils.class);

    /**
     * 点播防盗链
     * <p>
     * 测试方法：用10.90.3.38:8081替换掉ips.ifeng.com，在内网测，参数不对就播不了
     *
     * @param videoUrl 播放地址
     * @param videoId  视频id
     */
    public static String getVideoAuthUrl(String videoUrl, String videoId) {
        if (EmptyUtils.isEmpty(videoUrl)) {
            logger.debug("videoId = {} \n raw videoUrl = {} \n auth videoUrl = {}", videoId, videoUrl, "");
            return "";
        }

        // videoUrl = videoUrl.replaceAll("ips.ifeng.com", "10.90.3.38:8081");

        String authUrl = "";

        // 本地缓存，不加防盗链处理
        boolean isLocal = !videoUrl.contains("http");
        if (isLocal) {
            authUrl = videoUrl;
        }
        // 网络地址，做防盗链处理
        else {
            try {
                AuthParam param = new AuthParam();
                param.url = videoUrl;
                param.vid = videoId;
                if (ChoosePlayerUtils.useIJKPlayer(IfengApplication.getAppContext())) {
                    param.pver = VideoPlayerConfig.IJKPLAYER_VERSION;   //播放器版本
                } else {
                    param.pver = VideoPlayerConfig.IFENGPLAYER_VERSION;
                }
                param.sver = PhoneConfig.softversion;   //app version
                param.plantform = "android";
                param.sourceType = VideoPlayerConfig.SOURCE;   //app 来源
                param.publishID = PhoneConfig.publishid;
                param.tm = System.currentTimeMillis();
                param.uid = User.getUid();
                authUrl = VideoAuthClient.getAuthParamString(param);
            } catch (Exception e) {
                logger.error("getVideoAuthUrl error ! {}", e);
            }
        }
        logger.debug("videoId = {} \n raw videoUrl = {} \n auth videoUrl = {}", videoId, videoUrl, authUrl);
        return authUrl;
    }

    /**
     * 直播防盗链
     */
    public static String getLiveAuthUrl(String liveUrl) {
        String authUrl = "";
        if (EmptyUtils.isNotEmpty(liveUrl)) {
            authUrl = MD5Utils.getMD5UrlForLive(liveUrl);
        }
        logger.debug("raw liveUrl = {} \nauth liveUrl = {}", liveUrl, authUrl);
        return authUrl;
    }
}
