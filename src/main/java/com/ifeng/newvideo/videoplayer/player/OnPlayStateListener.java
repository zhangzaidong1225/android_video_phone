package com.ifeng.newvideo.videoplayer.player;

import android.os.Bundle;

/**
 * Created by fanshell on 2016/8/1.
 */
public interface OnPlayStateListener {

    void updatePlayStatus(IPlayer.PlayerState state, Bundle data);

}
