package com.ifeng.newvideo.videoplayer.player.record;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.format.DateUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.LiveRecord;
import com.ifeng.newvideo.statistics.domains.VideoRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.player.ChoosePlayerUtils;

/**
 * Created by fanshell on 2016/12/1.
 */
public class AudioRecordCountManager {

    private VideoRecord mVRecord;
    private int mSkinType;


    /**
     * BN 用于统计，卡顿每两秒，计一次数
     */
    private long BN_INTERVAL = 2 * DateUtils.SECOND_IN_MILLIS;

    /**
     * 卡顿次数
     */
    private final Handler mDelayStatisticHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (mVRecord != null) {
                mVRecord.statisticBN();
            }
        }
    };

    public AudioRecordCountManager() {

    }

    public void insertCustomerStatistics(IPlayer.PlayerState playState, int skinType) {
        this.mSkinType = skinType;

        switch (playState) {
            case STATE_PREPARING:
                handlePreparingStatistics();
                break;
            case STATE_PAUSED:
                handlePausedStatistics();
                break;
            case STATE_PLAYING:
                handlePlayingStatistics();
                break;
            case STATE_BUFFERING_START:
                handleBufferStartStatistics();
                break;
            case STATE_BUFFERING_END:
                handleBufferEndStatistics();
                break;
            case STATE_PLAYBACK_COMPLETED:
                handlePlaybackCompletedStatistics();
                break;
            case STATE_IDLE:
                handleIdleStatistics();
                break;
            case STATE_ERROR:
                handleErrorStatistics();
                break;
        }
    }


    private String getCache() {
        return mSkinType == VideoSkin.SKIN_TYPE_LOCAL ? "yes" : "no";
    }


    private String getPType() {
        String result = "";
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_FM:
                result = VodRecord.P_TYPE_FM;
                break;
            case VideoSkin.SKIN_TYPE_VOD:
                result = VodRecord.P_TYPE_RA;
                break;
            case VideoSkin.SKIN_TYPE_TV:
                result = VodRecord.P_TYPE_LA;
                break;
            case VideoSkin.SKIN_TYPE_TOPIC:
                result = VodRecord.P_TYPE_RA;
                break;
            case VideoSkin.SKIN_TYPE_LOCAL:
                result = VodRecord.P_TYPE_FM;
                break;
            default:
                result = "";
                break;
        }
        return result;
    }


    public String getFromTag() {
        String result = "";
        switch (mSkinType) {
            case VideoSkin.SKIN_TYPE_FM:
                result = PlayQueue.getInstance().getVTag();
                break;
            case VideoSkin.SKIN_TYPE_VOD:
                result = VodRecord.V_TAG_RELATE;
                break;
            case VideoSkin.SKIN_TYPE_TV:
                result = "";
                break;
            case VideoSkin.SKIN_TYPE_TOPIC:
                result = VodRecord.V_TAG_TOPIC;
                break;
            case VideoSkin.SKIN_TYPE_LOCAL:
                result = VodRecord.V_TAG_FM_DLOAD;
                break;
            default:
                result = "";
                break;
        }
        return result;
    }

    private TVLiveInfo tvLiveInfo;

    private void handlePreparingStatistics() {
        boolean useIJKPlayer = ChoosePlayerUtils.useIJKPlayer(IfengApplication.getAppContext());
        String echId = PlayQueue.getInstance().getEchid();
        String pType = getPType();
        String fromTag = getFromTag();
        String wmId = "";
        String wmName = "";

        if (mSkinType == VideoSkin.SKIN_TYPE_TV) {//直播
            tvLiveInfo = PlayQueue.getInstance().getTVLiveInfo();
            if (tvLiveInfo != null) {
                if (!(mVRecord instanceof LiveRecord)) {
                    mVRecord = null;
                }
                mVRecord = new LiveRecord(tvLiveInfo.getTitle(), getLiveRecordTitle(tvLiveInfo), "",
                        pType, "", (LiveRecord) mVRecord, echId, useIJKPlayer, wmId, wmName, "", "");
            }
        } else {//点播、专题、FM等
            VideoItem videoItem = PlayQueue.getInstance().getCurrentVideoItem();
            if (videoItem != null) {
                if (videoItem.weMedia != null) {
                    wmId = videoItem.weMedia.id;
                    wmName = videoItem.weMedia.name;
                }
                //当不是第一个播放的音频时,echid为空
                if (mSkinType == VideoSkin.SKIN_TYPE_VOD && !videoItem.guid.equalsIgnoreCase(PlayQueue.getInstance().getGuid())) {
                    echId = "";
                }

                if (!(mVRecord instanceof VodRecord)) {
                    mVRecord = null;
                }
                mVRecord = new VodRecord(videoItem.guid, videoItem.title, videoItem.searchPath, echId, wmId, wmName,
                        String.valueOf(videoItem.duration), pType, getCache(), videoItem.cpName,
                        (VodRecord) mVRecord, fromTag, useIJKPlayer, videoItem.simId, "");

            }
        }
    }


    private void handlePausedStatistics() {
        if (mVRecord != null) {
            mVRecord.stopPlayTime();
        }
    }


    private void handlePlayingStatistics() {
        if (mVRecord != null) {
            mVRecord.setSuccessPlayFirstFrame(true);
            if (mVRecord instanceof LiveRecord) {
                ((LiveRecord) mVRecord).setLiveTitle(getLiveRecordTitle(tvLiveInfo));
                mVRecord.startPlayTime();
                ((LiveRecord) mVRecord).stopPrepareTime();
            } else {
                mVRecord.startPlayTime();
                ((VodRecord) mVRecord).stopPrepareTime();
            }
        }
    }


    private void handleBufferStartStatistics() {
        if (mVRecord != null) {
            mVRecord.stopPlayTime();
            mDelayStatisticHandler.removeMessages(0);
            mDelayStatisticHandler.sendEmptyMessageDelayed(0, BN_INTERVAL);
        }
    }


    private void handleBufferEndStatistics() {
        if (mVRecord != null) {
            mVRecord.startPlayTime();
            mDelayStatisticHandler.removeMessages(0);
        }
    }


    private void handlePlaybackCompletedStatistics() {
        if (mVRecord != null) {
            mVRecord.stopPlayTime();
            if (mVRecord instanceof LiveRecord) {
                ((LiveRecord) mVRecord).setLiveTitle(getLiveRecordTitle(tvLiveInfo));
                ((LiveRecord) mVRecord).setAudio(true);
            } else {
                ((VodRecord) mVRecord).setAudio(true);
                CustomerStatistics.sendVODRecord(((VodRecord) mVRecord));
            }
        }
    }


    private void handleIdleStatistics() {
        if (mVRecord != null) {
            mVRecord.stopPlayTime();
            if (mVRecord instanceof LiveRecord) {
                ((LiveRecord) mVRecord).setLiveTitle(getLiveRecordTitle(tvLiveInfo));
                LiveRecord temLiveRecord = (LiveRecord) this.mVRecord;
                temLiveRecord.setAudio(true);
                if (this.mVRecord.isSuccessPlayFirstFrame()) {
                    temLiveRecord.setErr(true);
                }
                temLiveRecord.stopPrepareTime();
                CustomerStatistics.sendLiveRecord(temLiveRecord);
            } else {
                ((VodRecord) mVRecord).setAudio(true);
                ((VodRecord) mVRecord).stopPrepareTime();
                CustomerStatistics.sendVODRecord(((VodRecord) mVRecord));
            }
        }
    }


    private void handleErrorStatistics() {
        if (mVRecord != null) {
            mVRecord.stopPlayTime();
            if (mVRecord instanceof LiveRecord) {
                ((LiveRecord) mVRecord).setLiveTitle(getLiveRecordTitle(tvLiveInfo));
                LiveRecord temLiveRecord = (LiveRecord) this.mVRecord;
                temLiveRecord.setAudio(true);
                if (this.mVRecord.isSuccessPlayFirstFrame()) {
                    temLiveRecord.setErr(true);
                }
                temLiveRecord.stopPrepareTime();
                CustomerStatistics.sendLiveRecord(temLiveRecord);
            } else {
                ((VodRecord) mVRecord).setAudio(true);
                ((VodRecord) mVRecord).stopPrepareTime();
                CustomerStatistics.sendVODRecord(((VodRecord) mVRecord));
            }
        }
    }


    private String getLiveRecordTitle(TVLiveInfo info) {
        if (info == null) {
            return "";
        }
        String programTitle = "";
        if (!ListUtils.isEmpty(info.getSchedule()) &&
                info.getSchedule().get(0) != null
                && !TextUtils.isEmpty(info.getSchedule().get(0).getProgramTitle())) {
            programTitle = info.getSchedule().get(0).getProgramTitle();
        } else {
            programTitle = "";
        }
        return programTitle;
    }
}
