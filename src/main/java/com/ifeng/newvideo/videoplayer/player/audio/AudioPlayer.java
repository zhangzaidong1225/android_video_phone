package com.ifeng.newvideo.videoplayer.player.audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.dao.SurveyDao;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.OnPlayStateListener;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.PlayUrlAuthUtils;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.video.core.utils.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

import static android.media.MediaPlayer.MEDIA_INFO_BUFFERING_END;
import static android.media.MediaPlayer.MEDIA_INFO_BUFFERING_START;

/**
 * Created by fanshell on 2016/11/15.
 */
public class AudioPlayer implements AudioManager.OnAudioFocusChangeListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener {

    /**
     * logger
     */
    private Logger logger = LoggerFactory.getLogger(AudioPlayer.class);

    /**
     * 实现声音播放的类
     */
    private MediaPlayer mMediaPlayer;

    private static final int AUDIO_NO_FOCUSED_NO_DUCK = 0;
    private static final int AUDIO_NO_FOCUSED_CAN_DUCK = 1;
    private static final int AUDIO_FOCUSED = 2;
    private int mAudioFocusStatus;
    private AudioManager mAudioManger;
    private AudioService mAudioService;


    private IPlayer.PlayerState mPlayStatus = IPlayer.PlayerState.STATE_IDLE;

    private boolean mPlayOnFocus;

    private String mPath;
    // The volume we set the media player to when we lose audio focus, but are
    // allowed to reduce the volume instead of stopping playback.
    private static final float VOLUME_DUCK = 0.2f;
    // The volume we set the media player when we have audio focus.
    private static final float VOLUME_NORMAL = 1.0f;

    private volatile long mCurrentPosition;

    public AudioPlayer(AudioService service) {
        logger.debug(" new AudioPlayer");
        this.mAudioService = service;
        mAudioManger = (AudioManager) this.mAudioService.getSystemService(Context.AUDIO_SERVICE);
        mAudioFocusStatus = AUDIO_NO_FOCUSED_NO_DUCK;
    }

    public void play(String path, long position) {
        //路径是空的处理
        if (TextUtils.isEmpty(path)) {
            logger.debug("path is empty");
            mPlayStatus = IPlayer.PlayerState.STATE_ERROR;
            updateStatus(new Bundle());
            return;
        }
        logger.debug("path is:{}", path);
        mPlayOnFocus = true;
        tryToGetAudioFocus();
        boolean mediaHasChange = !TextUtils.equals(mPath, path);
        if (mediaHasChange) {
            this.mPath = path;
        } else {
            //正在播放时,重新打开
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                updateStatus(new Bundle());
                return;
            }
        }
        //暂停状态的处理
        if (mPlayStatus == IPlayer.PlayerState.STATE_PAUSED && mMediaPlayer != null && !mediaHasChange) {
            configurePlayStatus();
            updateStatus(new Bundle());
            return;
        }
        mCurrentPosition = position;
        releaseResource(true);
        createMediaPlayIfNeed();
        try {
            VideoItem currentVideoItem = PlayQueue.getInstance().getCurrentVideoItem();
            String guid = "";
            if (currentVideoItem != null) {
                guid = currentVideoItem.guid;
                if (!currentVideoItem.isConvertFromFM) {
                    SurveyDao.accumulatorPlayCount(guid);
                }
            }
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setDataSource(PlayUrlAuthUtils.getVideoAuthUrl(mPath, guid));
            mPlayStatus = IPlayer.PlayerState.STATE_PREPARING;
            updateStatus(new Bundle());
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            //handle error
            mPlayStatus = IPlayer.PlayerState.STATE_ERROR;
            updateStatus(new Bundle());
        }

    }

    /**
     * 创建一个MediaPlayer
     */
    private void createMediaPlayIfNeed() {
        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
            configMediaPlayListener();
        }
    }


    /**
     * Try to get the system audio focus.
     */
    private void tryToGetAudioFocus() {
        if (mAudioFocusStatus != AUDIO_FOCUSED) {
            /**
             * 用于监听音视焦点
             */
            int result = mAudioManger.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                mAudioFocusStatus = AUDIO_FOCUSED;
            }
        }
    }

    /**
     * give up the system audio focus
     */
    private void giveUpAudioFocus() {
        if (mAudioFocusStatus == AUDIO_FOCUSED) {
            mAudioManger.abandonAudioFocus(this);
            mAudioFocusStatus = AUDIO_NO_FOCUSED_NO_DUCK;
        }
    }

    /**
     * 配置mediaPlayer的监听
     */
    private void configMediaPlayListener() {
        mMediaPlayer.setWakeMode(mAudioService.getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnSeekCompleteListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnInfoListener(this);
        mMediaPlayer.setOnBufferingUpdateListener(this);

    }

    /**
     * 监听声音发生变化时的回调
     *
     * @param focusChange
     */
    @Override
    public void onAudioFocusChange(int focusChange) {
        logger.debug("onAudioFocusChange :{}", focusChange);
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                mAudioFocusStatus = AUDIO_FOCUSED;
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                mAudioFocusStatus = AUDIO_NO_FOCUSED_NO_DUCK;
                if (mPlayStatus == IPlayer.PlayerState.STATE_PLAYING) {
                    mPlayOnFocus = true;
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                mAudioFocusStatus = AUDIO_NO_FOCUSED_CAN_DUCK;
                break;
            default:
                logger.debug("audio status ignore handle");

        }

        configurePlayStatus();

    }

    /**
     * 配置播放状态的处理
     */
    private void configurePlayStatus() {
        logger.debug("configurePlayStatus :{}", mAudioFocusStatus);
        if (mAudioFocusStatus == AUDIO_NO_FOCUSED_NO_DUCK) {
            if (mPlayStatus == IPlayer.PlayerState.STATE_PLAYING) {
                pause();
            }
        } else {
            //we have focus
            if (mMediaPlayer != null) {
                if (mAudioFocusStatus == AUDIO_NO_FOCUSED_CAN_DUCK) {
                    mMediaPlayer.setVolume(VOLUME_DUCK, VOLUME_DUCK);
                } else {
                    mMediaPlayer.setVolume(VOLUME_NORMAL, VOLUME_NORMAL);
                }
            }
            if (mPlayOnFocus) {
                if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
                    if (mMediaPlayer.getCurrentPosition() != mCurrentPosition) {
                        mMediaPlayer.seekTo((int) mCurrentPosition);
                    } else {
                        mMediaPlayer.start();
                        mPlayStatus = IPlayer.PlayerState.STATE_PLAYING;
                        updateStatus(new Bundle());
                    }
                }

                mPlayOnFocus = false;
            }
        }
    }


    /**
     * 暂停的处理
     */
    public void pause() {
        logger.debug("pause:{}");
        if (mPlayStatus == IPlayer.PlayerState.STATE_PAUSED) {
            return;
        }
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mPlayStatus = IPlayer.PlayerState.STATE_PAUSED;
            mMediaPlayer.pause();
            mCurrentPosition = mMediaPlayer.getCurrentPosition();
            updateStatus(new Bundle());
        }
        giveUpAudioFocus();
    }

    /**
     * seek to position
     *
     * @param position
     */
    public void seekTo(long position) {
        if (mMediaPlayer == null) {
            mCurrentPosition = position;
        } else {
            mMediaPlayer.seekTo((int) position);
        }
    }


    public boolean isPlaying() {
        return mMediaPlayer != null && mMediaPlayer.isPlaying();
    }

    public long getCurrentPosition() {
        if (mMediaPlayer != null) {
            return mMediaPlayer.getCurrentPosition();
        }
        return mCurrentPosition;
    }


    public long getDuration() {
        if (mPlayStatus != IPlayer.PlayerState.STATE_PREPARING && mPlayStatus != IPlayer.PlayerState.STREAM_CHANGE) {
            if (mMediaPlayer != null) {
                return mMediaPlayer.getDuration();
            }
        }
        return 0L;
    }

    private void releaseResource(boolean release) {
        if (release && mMediaPlayer != null) {
            updateStatus(new Bundle(), IPlayer.PlayerState.STATE_SAVE_HOSITORY);
            mMediaPlayer.pause();
            mMediaPlayer.stop();
            mPlayStatus = IPlayer.PlayerState.STATE_IDLE;
            updateStatus(new Bundle());
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }


    public void stop() {
        logger.debug("stop");
        releaseResource(true);
        giveUpAudioFocus();
    }

    /**
     * 用于实现监听准备完成
     */

    @Override
    public void onPrepared(MediaPlayer mp) {
        logger.debug("onPrepared");
        configurePlayStatus();
        updateStatus(new Bundle());
    }


    /**
     * 用于实现监听完成
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        mPlayStatus = IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED;
        updateStatus(new Bundle());
        //自动播放下一个
        autoNextOrPrePlay(true);
    }

    /**
     * 用于实现监听seekComplete
     */
    @Override
    public void onSeekComplete(MediaPlayer mp) {
        mCurrentPosition = mp.getCurrentPosition();
        mMediaPlayer.start();
        mPlayStatus = IPlayer.PlayerState.STATE_PLAYING;
        updateStatus(new Bundle());
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d("ERRORTAG", "onError event:what:" + what + "extra :" + extra);
        if (what == -38 && extra == 0) {
            return true;
        }
        mPlayStatus = IPlayer.PlayerState.STATE_ERROR;
        updateStatus(new Bundle());
        return true;
    }


    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        if (mPlayStateListener != null) {
            switch (what) {
                case MEDIA_INFO_BUFFERING_START:
                    mPlayStateListener.updatePlayStatus(IPlayer.PlayerState.STATE_BUFFERING_START, new Bundle());
                    break;
                case MEDIA_INFO_BUFFERING_END:
                    mPlayStateListener.updatePlayStatus(IPlayer.PlayerState.STATE_BUFFERING_END, new Bundle());
                    break;
                default:
                    break;

            }
        }

        return false;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    private OnPlayStateListener mPlayStateListener;

    public void setOnPlayStateListener(OnPlayStateListener listener) {
        this.mPlayStateListener = listener;
    }


    private void updateStatus(Bundle bundle) {
        updateStatus(bundle, mPlayStatus);
    }

    private void updateStatus(Bundle bundle, IPlayer.PlayerState mPlayStatus) {
        if (mPlayStateListener != null) {
            this.mPlayStateListener.updatePlayStatus(mPlayStatus, bundle);
        }
    }


    public void start() {
        play(mPath, mCurrentPosition);
    }


    private IPlayController.OnAutoPlayListener mAutoPlayListener;

    public void setOnAutoPlayListener(IPlayController.OnAutoPlayListener listener) {
        this.mAutoPlayListener = listener;
    }


    /**
     * 根据当前的VideoItem得到在列表中的下一首位置
     *
     * @return
     */
    private int getNextPlayIndex(VideoItem videoItem, List<VideoItem> videoItems) {
        int size = videoItems.size();
        int position = -1;
        VideoItem item;
        for (int i = 0; i < size; i++) {
            item = videoItems.get(i);
            if (videoItem.guid.equals(item.guid)) {
                position = i;
            }
        }

        if (position == -1) {
            position = 0;
        } else {
            position = position + 1;
        }
        if (position >= size) {
            position = 0;
        }


        return position;
    }


    /**
     * 根据当前的VideoItem得到在列表中的上一首位置
     *
     * @return
     */
    private int getPrePlayIndex(VideoItem videoItem, List<VideoItem> videoItems) {
        int size = videoItems.size();
        int position = -1;
        VideoItem item;
        for (int i = 0; i < size; i++) {
            item = videoItems.get(i);
            if (videoItem.guid.equals(item.guid)) {
                position = i;
            }
        }
        position = position - 1;
        //前一首
        if (position < 0) {
            position = size - 1;
        }
        //check validate
        if (position < 0 || position >= size) {
            position = 0;
        }
        return position;
    }

    /**
     * 自动播放下一首or前一首
     *
     * @param isNext true下一首,false 上一首
     */
    private void autoNextOrPrePlay(boolean isNext) {
        List<VideoItem> videoItems = PlayQueue.getInstance().getPlayList();
        if (ListUtils.isEmpty(videoItems)) {
            return;
        }
        VideoItem currentPlayItem = PlayQueue.getInstance().getCurrentVideoItem();
        int position;
        if (isNext) {
            position = getNextPlayIndex(currentPlayItem, videoItems);
        } else {
            position = getPrePlayIndex(currentPlayItem, videoItems);
        }
        String path = StreamUtils.getMp3Url(videoItems.get(position).videoFiles);
        logger.debug("auto path:{}", path);
        //更新播放
        PlayQueue.getInstance().setVideoItem(videoItems.get(position));
        if (mAutoPlayListener != null) {
            mAutoPlayListener.onPlayListener(videoItems.get(position));
        }
        play(path, 0);
    }

    public void playPre() {
        autoNextOrPrePlay(false);
    }

    public void playNext() {
        autoNextOrPrePlay(true);
    }


}
