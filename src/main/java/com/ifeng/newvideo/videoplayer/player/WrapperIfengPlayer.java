package com.ifeng.newvideo.videoplayer.player;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.video.player.IMediaPlayer.MediaSource;
import com.video.videosdk.MediaPlayerCommon;
import com.video.videosdk.MediaPlayerSDK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by fanshell on 2016/8/1.
 */
public class WrapperIfengPlayer implements IPlayer {

    private Logger logger = LoggerFactory.getLogger(WrapperIfengPlayer.class);

    private MediaPlayerSDK mPlayer;
    private OnPlayStateListener mPlayStateListener;

    private Surface mSurface;

    private Bundle mBundle = new Bundle();

    private String mPath;

    private PlayerState mCurrentState = PlayerState.STATE_IDLE;

    private long mPosition;

    private static final int ID = 1024;

    private OnPlayVideoSizeChangeListener mVideoSizeChange;

    private long mCurrentTime;

    private long mLastPosition;

    private Handler mHanlder = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                /** 解析完头部回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_START:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        mCurrentState = PlayerState.STATE_BUFFERING_START;
                        notifyStatusChange();
                        if (mLastPosition > 0) {
                            seekTo(mLastPosition);
                        }
                        if (getVideoWidth() > 1 || getVideoHeight() > 1) {
                            if (mVideoSizeChange != null) {
                                mVideoSizeChange.onVideoSizeChangeListener(getVideoWidth(), getVideoHeight());
                            }
                        }
                    }
                    break;
                case MediaPlayerCommon.IFENG_PLAYER_PAUSE:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        mCurrentState = PlayerState.STATE_PAUSED;
                        notifyStatusChange();
                    }

                    break;
                /** 当前播放时间回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_PLAYING:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        if (ActivityMainTab.getPiPMode()) {
                            mCurrentState = PlayerState.PIP_STATE_PLAYING;
                            notifyStatusChange();
                        } else {
                            if (mCurrentState != PlayerState.STATE_PLAYING) {
                                mCurrentState = PlayerState.STATE_PLAYING;
                                notifyStatusChange();
                            }
                        }
                        mCurrentTime = msg.arg1;
                    }
                    break;
                /** seek结束回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_SEEK:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        mCurrentState = PlayerState.STATE_BUFFERING_START;
                        notifyStatusChange();
                    }
                    break;
                /** 播放结束回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_END:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        if (mCurrentState != PlayerState.STATE_PLAYBACK_COMPLETED) {
                            mCurrentState = PlayerState.STATE_PLAYBACK_COMPLETED;
                            notifyStatusChange();
                        }
                }
                    break;
                /** 播放缓冲区满回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_BUFFER_FULL:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        mCurrentState = PlayerState.STATE_BUFFERING_END;
                        notifyStatusChange();
                        if (leaveStateIsPause) {
                            pause();
                        } else {
                            start();
                        }
                    }

                    break;
                /** 播放缓冲区空回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_BUFFER_EMPTY:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        mCurrentState = PlayerState.STATE_PREPARING;
                        notifyStatusChange();
                    }
                    break;
                /** 错误回调消息 */
                case MediaPlayerCommon.IFENG_PLAYER_ERROR:
                    if (mPlayer.isMyMessage(msg.obj)) {
                        mCurrentState = PlayerState.STATE_ERROR;
                        notifyStatusChange();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    public WrapperIfengPlayer() {

    }

    @Override
    public void prepareAsync() {
        try {
            logger.debug("prepareAsync");
            mCurrentState = PlayerState.STATE_PREPARING;
            notifyStatusChange();
        } catch (IllegalStateException e) {
            error();
        }
    }

    @Override
    public void setSourcePath(String path) {
        mPath = path;
        openVideo();
    }

    @Override
    public void start() {
        if (isInPlaybackState()) {
            try {
                logger.debug("play start");
                mPlayer.ifengPlayerStart();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                error();
            }
        }
    }

    @Override
    public void pause() {
        try {
            if (isInPlaybackState() && (mCurrentState == PlayerState.STATE_PLAYING || mCurrentState == PlayerState.PIP_STATE_PLAYING)) {
                mPlayer.ifengPlayerPause();
                mCurrentState = PlayerState.STATE_PAUSED;
                notifyStatusChange();
            }
        } catch (Exception e) {
            error();
        }

    }

    @Override
    public void stop() {
        if (isInPlaybackState()) {
            try {
                mPlayer.ifengPlayerStop();
                idle();
            } catch (Exception e) {
                error();
            }
        }
    }

    @Override
    public void seekTo(long position) {
        try {
            this.mPosition = position;
            if (isInPlaybackState()) {
                if (getDuration() > 0) {
                    mPlayer.ifengPlayerSeekTo(position * 100 / getDuration());
                    mCurrentTime = position;
                } else {
                    mPlayer.ifengPlayerSeekTo(0);
                    mCurrentTime = 0;
                }
            }
        } catch (IllegalStateException e) {
            logger.error("seekTo  error! {}", e);
            error();
        }
    }


    @Override
    public void release() {
        if (isInPlaybackState()) {
            try {
                mPlayer.ifengPlayerRelease();
                if (null != mHanlder) {
                    mHanlder.removeCallbacksAndMessages(null);
                }
                idle();
            } catch (Exception e) {
                error();
            }
        }
    }

    private void idle() {
        mCurrentState = PlayerState.STATE_IDLE;
        notifyStatusChange();
    }

    private void error() {
        mCurrentState = PlayerState.STATE_ERROR;
        notifyStatusChange();
    }

    @Override
    public boolean isPlaying() {
        if (mPlayer != null) {
            return mCurrentState == PlayerState.STATE_PLAYING || mCurrentState == PlayerState.PIP_STATE_PLAYING;
        }
        return false;
    }

    @Override
    public long getCurrentPosition() {

        if (mPlayer != null) {
            if (leaveStateIsPause && mCurrentTime == 0) {
                return mPosition;
            }
            return mCurrentTime;
        }
        return 0;
    }

    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
    }

    @Override
    public void setSurface(Surface surface) {
        this.mSurface = surface;
    }

    @Override
    public void setOnPlayStateListener(OnPlayStateListener listener) {
        if (listener != null) {
            mPlayStateListener = listener;
        }
    }


    private void attachListener() {

    }


    private boolean leaveStateIsPause = false;

    public void setLeaveStateIsPause(boolean pause) {
        leaveStateIsPause = pause;
    }



    private void notifyStatusChange() {
        if (mPlayStateListener != null) {
            mPlayStateListener.updatePlayStatus(mCurrentState, mBundle);
        }

    }


    private void release(boolean clearState) {
        if (mPlayer != null) {
            mPlayer.ifengPlayerRelease();
            if (null != mHanlder) {
                mHanlder.removeCallbacksAndMessages(null);
            }
            if (clearState) {
                mCurrentState = PlayerState.STATE_IDLE;
            }
            mPlayer = null;

        }


    }

    private void openVideo() {
        release(false);

        mPlayer = new MediaPlayerSDK();
        mPlayer.setCallBackInfo(mHanlder);
        mPlayer.init(MediaPlayerCommon.MediaPlayerLogFlag.MEDIA_PLAYER_LOG_FLAG_UNKNOW);
        mPlayer.ifengPlayerSetVideoSurface(mSurface);
        mLastPosition = 0;

        mBundle = new Bundle();
        attachListener();
        MediaSource[] mediaSource = new MediaSource[1];
        MediaSource src = new MediaSource();
        src.id = ID;
        src.playUrl = mPath;
        mediaSource[0] = src;
        try {
//            mMediaPlayer.setDataSource(mediaSource);
            mPlayer.ifengPlayerSetURL(mPath);
        } catch (Exception ex) {
            mCurrentState = PlayerState.STATE_ERROR;
            logger.error("data source error");

        }
    }

    @Override
    public long getDuration() {
        if (mPlayer == null) {
            return 0;
        }
        return mPlayer.ifengPlayerGetVideoLength();
    }


    private boolean isInPlaybackState() {
        return mPlayer != null;
    }

    public boolean isInPlayState() {
        return mPlayer != null
                && mCurrentState != PlayerState.STATE_ERROR
                && mCurrentState != PlayerState.STATE_IDLE
                && mCurrentState != PlayerState.STATE_PREPARING;
    }


    public void setOnPlayVideoSizeChangeListener(OnPlayVideoSizeChangeListener listener) {
        this.mVideoSizeChange = listener;
    }

    @Override
    public void setLastPosition(long lastPosition) {
        mLastPosition = lastPosition;
    }

    @Override
    public int getVideoWidth() {
        return mPlayer.ifengVideoWidth();
    }

    @Override
    public int getVideoHeight() {
        return mPlayer.ifengVideoHeight();
    }
}
