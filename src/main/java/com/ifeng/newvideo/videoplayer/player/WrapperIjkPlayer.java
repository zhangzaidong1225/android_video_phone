package com.ifeng.newvideo.videoplayer.player;

import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.ifeng.video.player.IMediaPlayer;
import com.ifeng.video.player.IMediaPlayer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;


/**
 * Created by fanshell on 2016/8/1.
 */
public class WrapperIjkPlayer implements IPlayer {

    private Logger logger = LoggerFactory.getLogger(WrapperIjkPlayer.class);

    private IMediaPlayer mMediaPlayer;
    private OnPlayStateListener mPlayStateListener;
    private SurfaceHolder mSurfaceHolder;
    private Surface mSurface;

    private Bundle mBundle = new Bundle();

    private String mPath;

    private PlayerState mCurrentState = PlayerState.STATE_IDLE;

    private long mPosition;

    private static final int ID = 1024;

    private OnPlayVideoSizeChangeListener mVideoSizeChange;

    public WrapperIjkPlayer() {

    }

    @Override
    public void prepareAsync() {
        try {
            logger.debug("prepareAsync");
            mMediaPlayer.prepareAsync();
            mCurrentState = PlayerState.STATE_PREPARING;
            notifyStatusChange();
            logger.debug("startBuffer prepareAsync");
            startCheckingBuffer();
        } catch (IllegalStateException e) {
            logger.error("state prepareAsync error! {}", e);
            stopCheckingBuffer();
            error();
        }
    }

    @Override
    public void setSourcePath(String path) {
        mPath = path;
        openVideo();
    }

    @Override
    public void start() {
        if (isInPlaybackState()) {
            try {
                logger.debug("play start");
                mMediaPlayer.start();
                mCurrentState = PlayerState.STATE_PLAYING;
                notifyStatusChange();
            } catch (IllegalStateException e) {
                logger.error("state start error! {}", e);
                error();
            }
        }
    }

    @Override
    public void pause() {
        try {
            if (isInPlaybackState() && mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                mCurrentState = PlayerState.STATE_PAUSED;
                notifyStatusChange();
            }
        } catch (Exception e) {
            logger.error("state pause error! {}", e);
            error();
        }

    }

    @Override
    public void stop() {
        if (isInPlaybackState()) {
            try {
                mMediaPlayer.stop();
                idle();
            } catch (Exception e) {
                logger.error("state stop error! {}", e);
                error();
            }
        }
    }

    @Override
    public void seekTo(long position) {
        try {
            this.mPosition = position;
            if (isInPlaybackState()) {
                mMediaPlayer.seekTo(position);
            }
        } catch (IllegalStateException e) {
            logger.error("seekTo  error! {}", e);
            error();
        }
    }


    @Override
    public void release() {
        stopCheckingBuffer();
        if (isInPlaybackState()) {
            try {
                mMediaPlayer.release();
                idle();
            } catch (Exception e) {
                logger.error("state release error! {}", e);
                error();
            }
        }
    }

    private void idle() {
        mCurrentState = PlayerState.STATE_IDLE;
        notifyStatusChange();
    }

    private void error() {
        mCurrentState = PlayerState.STATE_ERROR;

        notifyStatusChange();
    }

    @Override
    public boolean isPlaying() {
        if (mMediaPlayer != null) {
            return mMediaPlayer.isPlaying();
        }
        return false;
    }

    @Override
    public long getCurrentPosition() {

        if (mMediaPlayer != null) {
            long currentPosition = mMediaPlayer.getCurrentPosition();
            if (leaveStateIsPause && currentPosition == 0) {
                return mPosition;
            }
            return currentPosition;
        }

        return 0;
    }

    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
        this.mSurfaceHolder = surfaceHolder;
    }

    @Override
    public void setSurface(Surface surface) {
        this.mSurface = surface;
    }

    @Override
    public void setOnPlayStateListener(OnPlayStateListener listener) {
        if (listener != null) {
            mPlayStateListener = listener;
        }
    }


    private void attachListener() {
        mMediaPlayer.setOnPreparedListener(mPreparedListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mSizeChangedListener);
        mMediaPlayer.setOnCompletionListener(mCompletionListener);
        mMediaPlayer.setOnErrorListener(mErrorListener);
        mMediaPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
        mMediaPlayer.setOnInfoListener(mInfoListener);
        mMediaPlayer.setOnSeekCompleteListener(mSeekCompleteListener);

    }


    private boolean leaveStateIsPause = false;

    public void setLeaveStateIsPause(boolean pause) {
        leaveStateIsPause = pause;
    }

    private OnPreparedListener mPreparedListener = new OnPreparedListener() {
        public void onPrepared(IMediaPlayer mp, int extra) {
            stopCheckingBuffer();
            if (mPosition != 0) {
                mMediaPlayer.seekTo(mPosition);
            }
            if (leaveStateIsPause) {
                pause();
            } else {
                start();
            }
        }
    };

    private OnVideoSizeChangedListener mSizeChangedListener = new OnVideoSizeChangedListener() {
        public void onVideoSizeChanged(IMediaPlayer mp, int width, int height, int sarNum, int sarDen) {
            if (width <= 1 || height <= 1) {
                return;
            }

            if (mVideoSizeChange != null) {
                mVideoSizeChange.onVideoSizeChangeListener(width, height);
            }
        }
    };

    private OnCompletionListener mCompletionListener = new OnCompletionListener() {
        @Override
        public void onCompletion(IMediaPlayer mp, int extra) {
            mCurrentState = IPlayer.PlayerState.STATE_PLAYBACK_COMPLETED;
            notifyStatusChange();
        }
    };

    private OnErrorListener mErrorListener = new OnErrorListener() {
        @Override
        public boolean onError(IMediaPlayer mp, int what, int extra, String jsonReport) {
            logger.debug("state error onErrorListener! ");
            error();
            return true;
        }
    };

    private OnBufferingUpdateListener mBufferingUpdateListener = new OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(IMediaPlayer mp, int percent) {

            if (percent > 0) {
                stopCheckingBuffer();//FIXME:ijk的native层post上来的回调中，未知原因下（一定概率）onInfoListener中buffer方法没有回调
                //TODO:排除调用状态的异常造成的上述情况
            }
        }
    };

    private OnInfoListener mInfoListener = new OnInfoListener() {
        @Override
        public boolean onInfo(IMediaPlayer mp, int what, int extra, String jsonReport) {
            if (mMediaPlayer != null) {
                if (what == IMediaPlayer.MEDIA_INFO_WHAT_BUFFERING_START) {
                    mCurrentState = PlayerState.STATE_BUFFERING_START;
                    notifyStatusChange();
                    logger.debug("buffer start check");
                    startCheckingBuffer();
                } else if (what == IMediaPlayer.MEDIA_INFO_WHAT_BUFFERING_END) {
                    stopCheckingBuffer();
                    logger.debug("buffer end");
                    mCurrentState = PlayerState.STATE_BUFFERING_END;
                    notifyStatusChange();
                }
            }

            return true;
        }

    };

    private OnSeekCompleteListener mSeekCompleteListener = new OnSeekCompleteListener() {
        @Override
        public void onSeekComplete(IMediaPlayer mp) {

        }
    };


    private void notifyStatusChange() {
        if (mPlayStateListener != null) {
            mPlayStateListener.updatePlayStatus(mCurrentState, mBundle);
        }

    }


    private void release(boolean clearState) {
        stopCheckingBuffer();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            if (clearState) {
                mCurrentState = PlayerState.STATE_IDLE;
            }
            mMediaPlayer = null;

        }


    }

    private void openVideo() {
        release(false);
        mMediaPlayer = new IjkMediaPlayer();
//        mMediaPlayer.setDisplay(mSurfaceHolder);
        mMediaPlayer.setSurface(mSurface);
        mBundle = new Bundle();
        attachListener();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        MediaSource[] mediaSource = new MediaSource[1];
        MediaSource src = new MediaSource();
        src.id = ID;
        src.playUrl = mPath;
        mediaSource[0] = src;
        try {
            mMediaPlayer.setDataSource(mediaSource);
        } catch (Exception ex) {
            mCurrentState = PlayerState.STATE_ERROR;
            logger.error("data source error");

        }
    }

    @Override
    public long getDuration() {
        if (mMediaPlayer == null) {
            return 0;
        }
        return mMediaPlayer.getDuration();
    }


    private boolean isInPlaybackState() {
        return mMediaPlayer != null;
    }

    public boolean isInPlayState() {
        return mMediaPlayer != null
                && mCurrentState != PlayerState.STATE_ERROR
                && mCurrentState != PlayerState.STATE_IDLE
                && mCurrentState != PlayerState.STATE_PREPARING;
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            int what = msg.what;
            switch (what) {
                case MSG_CHECK_BUFFER:
                    logger.debug("state  check buffer ,show error!");
                    error();
                    if (mMediaPlayer != null) {
                        if (mMediaPlayer.isPlaying()) {
                            mMediaPlayer.pause();
                            mMediaPlayer.stop();
                            mMediaPlayer.release();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    };


    private void startCheckingBuffer() {
        logger.debug("startCheckingBuffer");
        handler.removeMessages(MSG_CHECK_BUFFER);
        Message msg = handler.obtainMessage();
        msg.what = MSG_CHECK_BUFFER;
        handler.sendMessageDelayed(msg, TIME_BUFFER_CHECK_INTERVAL);
    }


    private void stopCheckingBuffer() {
        if (handler != null) {
            handler.removeMessages(MSG_CHECK_BUFFER);
        }
        logger.debug("stopCheckingBuffer");
    }

    private final int MSG_CHECK_BUFFER = 101;

    private static final long TIME_BUFFER_CHECK_INTERVAL = 10 * DateUtils.SECOND_IN_MILLIS;


    public void setOnPlayVideoSizeChangeListener(OnPlayVideoSizeChangeListener listener) {
        this.mVideoSizeChange = listener;
    }

    @Override
    public void setLastPosition(long lastPosition) {

    }

    @Override
    public int getVideoWidth() {
        return 0;
    }

    @Override
    public int getVideoHeight() {
        return 0;
    }
}
