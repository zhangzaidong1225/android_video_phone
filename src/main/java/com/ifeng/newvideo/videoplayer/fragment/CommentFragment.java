package com.ifeng.newvideo.videoplayer.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.videoplayer.adapter.CommentAdapter;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.videoplayer.interfaced.CommentDataCallBack;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.RequestString;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.core.utils.URLEncoderUtils;
import com.ifeng.video.core.utils.XmlUtils;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.model.CommentInfoModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


/**
 * 播放页面的评论fragment
 */
public class CommentFragment extends FragmentBase {

    private static final Logger logger = LoggerFactory.getLogger(CommentFragment.class);

    public CommentAdapter commentAdapter;
    private MyPullToRefreshListView myPullToRefreshListView;
    private View comment_empty;
    private int PAGE_NUM = 1;
    private int PAGE_SIZE = 10;
    private View mHeaderView;
    private TextView headerTextView;
    private CommentDataCallBack mCommentDataProvider;
    private List<CommentInfoModel.Newest> mNewests = new ArrayList<>();
    private CommentInfoModel mCommentInfoModel;
    private Bundle bundle;
    private String currentGuid;
    private String ERROR_NET = "net_error";
    private String ERROR_DATA = "data_error";
    private AdapterView.OnItemClickListener mOnItemClickListener;
    private String mGuid;
    private boolean isConvertShareInfo = false;

    public String getGuid() {
        return mGuid;
    }

    public void publishComment(CommentInfoModel.Newest newest) {
        if (mNewests.size() == 0) {
            mNewests.add(newest);
            refreshUI(false);
        } else {
            mNewests.add(0, newest);
        }
        commentAdapter.voteds.clear();
        commentAdapter.setData(mNewests);
        commentAdapter.notifyDataSetChanged();
    }

    public CommentFragment() {
        super();
    }

    public static CommentFragment newInstance(Bundle bundle, AdapterView.OnItemClickListener onItemClickListener) {
        CommentFragment f = new CommentFragment();
        f.setArguments(bundle);
        f.mOnItemClickListener = onItemClickListener;
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        currentGuid = bundle.getString(IntentKey.VOD_GUID);
        initData();
    }

    private void initData() {
        commentAdapter = new CommentAdapter(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.video_comment, container, false);
        progressView = root.findViewById(R.id.video_detail_loading_progress_layout);
        comment_empty = root.findViewById(R.id.no_comment);
        comment_empty.setOnClickListener(this);
        comment_empty.setVisibility(View.GONE);
        emptyView_RL = root.findViewById(R.id.emptyView_RL);
        empty_tv = (TextView) emptyView_RL.findViewById(R.id.empty_tv);
        empty_img = (ImageView) root.findViewById(R.id.empty_nonet_img);
        emptyView_RL.setVisibility(View.GONE);
        emptyView_RL.setOnClickListener(this);
        myPullToRefreshListView = (MyPullToRefreshListView) root.findViewById(R.id.comment_list);
        myPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        myPullToRefreshListView.setShowIndicator(false);
        myPullToRefreshListView.hideFootView();
        myPullToRefreshListView.setVisibility(View.VISIBLE);
        myPullToRefreshListView.setOnItemClickListener(mOnItemClickListener);
        mHeaderView = inflater.inflate(R.layout.video_comment_list_header, null);
        headerTextView = (TextView) mHeaderView.findViewById(R.id.tv_comment_list_header);
        myPullToRefreshListView.getRefreshableView().addHeaderView(mHeaderView);
        return root;
    }

    private void initListener() {
        myPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {

            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                refreshData();//重新刷新
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
            }
        });

        myPullToRefreshListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            @Override
            public void onLastItemVisible() {
                //加载更多
                PAGE_NUM++;
                requestCommentsList(false);
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initListener();
        myPullToRefreshListView.setAdapter(commentAdapter);
        refreshData();
    }

    /**
     * 切换视频时加载
     */
    public void updateComments() {
        if (progressView != null) {
            progressView.setVisibility(View.VISIBLE);
        }
        refreshData();
    }

    public void refreshData() {
        PAGE_NUM = 1;
        requestCommentsList(true);
    }

    /**
     * 请求评论数据
     */
    private void requestCommentsList(boolean isFirst) {
        if (getActivity() == null) {
            return;
        }
        if (!NetUtils.isNetAvailable(getActivity())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        if (commentAdapter.commentClickPopupWindow.isShowing()) {
            commentAdapter.commentClickPopupWindow.dismiss();
        }

        if (mCommentDataProvider == null && getActivity() instanceof CommentDataCallBack) {
            mCommentDataProvider = (CommentDataCallBack) getActivity();
        }
        PlayerInfoModel playerInfoModel = null;
        if (mCommentDataProvider != null) {
            playerInfoModel = mCommentDataProvider.callGetCurrentProgram();
        }
        if (playerInfoModel != null) {
            String guid = playerInfoModel.getGuid();
            if (guid != null) {
                loadShareUrl(guid, isFirst);
            }
        } else {
            if (!NetUtils.isNetAvailable(getActivity())) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showErrorComment(ERROR_NET);
                    }
                }, 200);
            } else {
                showErrorComment(ERROR_DATA);
            }
        }
    }

    private void loadShareUrl(final String guid, final boolean isFirst) {
        if (!isFirst) {
            myPullToRefreshListView.showFootView();
        }
        //是否需要转换shareInfo,不需要转换的可以直接用
        if (!isConvertShareInfo) {
            try {
                mGuid = guid;
                onDocUrlSuccess(mGuid, isFirst);
            } catch (UnsupportedEncodingException e) {
                logger.error("loadShareUrl error {} ", e);
                showErrorComment(ERROR_DATA);
            }
            return;
        }
        RequestString requestString = new RequestString(Request.Method.GET, guid, null,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        InputStream in = stringToInputStream(response);
                        mGuid = XmlUtils.getAttributeValueFromXML(in, "PlayerUrl");
                        if (!TextUtils.isEmpty(mGuid)) {
                            try {
                                onDocUrlSuccess(mGuid, isFirst);
                            } catch (UnsupportedEncodingException e) {
                                logger.error("loadShareUrl error! {}", e);
                                showErrorComment(ERROR_DATA);
                            }
                        } else {
                            myPullToRefreshListView.onRefreshComplete();
                            myPullToRefreshListView.hideFootView();
                            showErrorComment(ERROR_DATA);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myPullToRefreshListView.onRefreshComplete();
                        if (!isFirst) {
                            myPullToRefreshListView.hideFootView();
                        }
                        logger.debug("the loaderURlXML shareInfo error");
                        if (error instanceof NetworkError) {
                            showErrorComment(ERROR_NET);
                        } else {
                            showErrorComment(ERROR_DATA);
                        }
                    }
                });
        VolleyHelper.getRequestQueue().add(requestString);
    }

    private void onDocUrlSuccess(String guid, final boolean isFirst) throws UnsupportedEncodingException {
        logger.debug("in getComments the docUrl == {}", guid);
        String params = "?docUrl=" + URLEncoderUtils.encodeInUTF8(guid) + "&p=" + PAGE_NUM + "&pagesize=" + PAGE_SIZE + "&type=new";
        logger.debug("in getComments the params == {}", params);
        CommentDao.getComments(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                myPullToRefreshListView.onRefreshComplete();
                if (response != null) {
                    mCommentInfoModel = (CommentInfoModel) response;
                    List<CommentInfoModel.Newest> returnNewests = mCommentInfoModel.getComments().getNewest();
                    if (isFirst) {
                        mNewests = returnNewests;
                    } else {
                        if (!mNewests.containsAll(returnNewests)) {
                            mNewests.addAll(returnNewests);
                        }
                        myPullToRefreshListView.hideFootView();
                    }
                    if (isFirst) {
                        myPullToRefreshListView.setAdapter(commentAdapter);
                    }
                    refreshUI(isFirst);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                myPullToRefreshListView.onRefreshComplete();
                if (!isFirst) {
                    myPullToRefreshListView.hideFootView();
                }
                if (error instanceof NetworkError) {
                    showErrorComment(ERROR_NET);
                } else {
                    showErrorComment(ERROR_DATA);
                }
                logger.debug("the get comments error");
            }
        }, params);
    }

    private void refreshUI(boolean isFirst) {
        if (mNewests == null || mNewests.size() == 0) {
            showEmptyComment();
        } else {
            progressView.setVisibility(View.GONE);
            emptyView_RL.setVisibility(View.GONE);
            comment_empty.setVisibility(View.GONE);
            headerTextView.setText(String.format(getString(R.string.comment_sum), mCommentInfoModel.getCount()));
            if (commentAdapter != null) {
                if (isFirst) {
                    commentAdapter.voteds.clear();
                }
                commentAdapter.setData(mNewests);
                commentAdapter.notifyDataSetChanged();
            }
        }
    }

    private void showEmptyComment() {
        progressView.setVisibility(View.GONE);
        emptyView_RL.setVisibility(View.GONE);
        comment_empty.setVisibility(View.VISIBLE);
    }

    private void showErrorComment(String error) {
        if (mNewests != null && mNewests.size() > 0) {
            refreshUI(true);
            if (!NetUtils.isNetAvailable(getActivity())) {
                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            }
            return;
        }
        progressView.setVisibility(View.GONE);
        comment_empty.setVisibility(View.GONE);
        emptyView_RL.setVisibility(View.VISIBLE);
        if (error.equals(ERROR_NET)) {
            empty_tv.setText(R.string.common_net_useless_try_again);
            empty_img.setVisibility(View.VISIBLE);
        } else {
            empty_img.setVisibility(View.GONE);
            empty_tv.setText(R.string.common_load_data_error);
        }
    }

    private InputStream stringToInputStream(String str) {
        return new ByteArrayInputStream(str.getBytes());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        commentAdapter = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.emptyView_RL:
                emptyView_RL.setVisibility(View.GONE);
                progressView.setVisibility(View.VISIBLE);
                refreshData();
                break;
            default:
                break;
        }
    }

}
