package com.ifeng.newvideo.videoplayer.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.video.core.exception.ResponseEmptyException;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.dao.db.dao.VideoPlayDao;
import com.ifeng.video.dao.db.model.SubAudioFMDetailInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 电台播放页面的详情fragment
 */
public class AudioFMDescFragment extends FragmentBase {

    private static final Logger logger = LoggerFactory.getLogger(AudioFMDescFragment.class);

    private String programId;
    private String type;

    NetworkImageView image;
    TextView title;
    TextView anchorman;
    TextView playtime;
    TextView playType;
    TextView columnInfo;
    TextView chanelId;
    private LinearLayout chanelId_l, anchorman_l, playtime_l, playType_l;

    public AudioFMDescFragment() {
        super();
    }

    public static AudioFMDescFragment newInstance(Bundle bundle) {
        AudioFMDescFragment fragment = new AudioFMDescFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            programId = bundle.getString(IntentKey.AUDIO_PROGRAM_ID);
            type = bundle.getString(IntentKey.AUDIO_FM_TYPE);
        }
    }

    private void getData() {
        progressView.setVisibility(View.VISIBLE);
        VideoPlayDao.requestAudioFMDetail(programId, new Response.Listener<SubAudioFMDetailInfo>() {
            @Override
            public void onResponse(SubAudioFMDetailInfo response) {
                bindDataToView(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof ResponseEmptyException) {
                    showEmpty(false);
                } else {
                    showEmpty(true);
                }
            }
        });
    }

    @Override
    public void refreshData() {
        getData();
    }

    public void showEmpty(boolean isError) {
        if (!isAdded()) {
            return;
        }
        progressView.setVisibility(View.GONE);
        if (emptyView_RL != null) {
            emptyView_RL.setVisibility(View.VISIBLE);
            if (NetUtils.isNetAvailable(getActivity())) {
                if (isError) {
                    empty_img.setBackgroundResource(R.drawable.icon_common_load_fail);
                    empty_tv.setText(getResources().getText(R.string.load_data_fail_retry));
                } else {
                    empty_img.setBackgroundResource(R.drawable.fm_desc_empty);
                    empty_tv.setText(getResources().getText(R.string.editor_trying));
                }
            } else {
                empty_img.setBackgroundResource(R.drawable.common_no_net);
                empty_tv.setText(getResources().getText(R.string.common_net_useless_try_again));
            }
        }
    }

    private void bindDataToView(SubAudioFMDetailInfo response) {
        logger.debug("SubAudioFMDetailInfo=={}", response);

        if (response == null || response.getData() == null) {
            showEmpty(false);
        } else {
            boolean isEmpty = true;
            progressView.setVisibility(View.GONE);
            if (emptyView_RL != null) {
                emptyView_RL.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(response.getData().getProgramName())) {
                isEmpty = false;
            }
            title.setText(response.getData().getProgramName());
            //主持
            String compere = response.getData().getCompere();
            if (TextUtils.isEmpty(compere)) {
                anchorman_l.setVisibility(View.GONE);
            } else {
                isEmpty = false;
                anchorman_l.setVisibility(View.VISIBLE);
                anchorman.setText(response.getData().getCompere());
            }
            //时间
            String lastPublishTime = response.getData().getUpdateTime();
            if (TextUtils.isEmpty(lastPublishTime)) {
                playtime_l.setVisibility(View.GONE);
            } else {
                isEmpty = false;
                lastPublishTime = generateTime(Long.parseLong(lastPublishTime));
                if (lastPublishTime.length() >= 4) {
                    playtime_l.setVisibility(View.VISIBLE);
                    playtime.setText(lastPublishTime.substring(0, 4));
                }
            }

            //类型
            if (TextUtils.isEmpty(type)) {
                playType_l.setVisibility(View.GONE);
            } else {
                playType_l.setVisibility(View.VISIBLE);
                playType.setText(type);
            }
            String detail = "介    绍：";
            if (!TextUtils.isEmpty(response.getData().getProgramDetails())) {
                isEmpty = false;
                detail = detail + response.getData().getProgramDetails();
            } else {
                detail = detail + "暂无";
            }
            columnInfo.setText(detail);

            //来源
            String comeFrom = response.getData().getComfrom();
            if (TextUtils.isEmpty(comeFrom)) {
                chanelId_l.setVisibility(View.GONE);
            } else {
                isEmpty = false;
                chanelId_l.setVisibility(View.VISIBLE);
                chanelId.setText(response.getData().getComfrom());
            }

            image.setDefaultImageResId(R.drawable.column_fav_default_bg);
            if (!TextUtils.isEmpty(response.getData().getImg370_370())) {
                isEmpty = false;
            }
            image.setImageUrl(response.getData().getImg370_370(), VolleyHelper.getImageLoader());

            if (isEmpty) {
                showEmpty(false);
            }
        }
    }

    private static String generateTime(long position) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(new Date(position * 1000));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.video_fragment_introduction, container, false);
        progressView = root.findViewById(R.id.video_detail_loading_progress_layout);
        empty_tv = (TextView) root.findViewById(R.id.empty_tv);
        empty_img = (ImageView) root.findViewById(R.id.empty_nonet_img);
        emptyView_RL = root.findViewById(R.id.emptyView_RL);

        image = (NetworkImageView) root.findViewById(R.id.column_detail_content_image);
        title = (TextView) root.findViewById(R.id.title);
        anchorman = (TextView) root.findViewById(R.id.anchorman);
        anchorman_l = (LinearLayout) root.findViewById(R.id.anchorman_l);
        playtime = (TextView) root.findViewById(R.id.play_time);
        playtime_l = (LinearLayout) root.findViewById(R.id.play_time_l);
        playType = (TextView) root.findViewById(R.id.play_type);
        playType_l = (LinearLayout) root.findViewById(R.id.play_type_l);
        columnInfo = (TextView) root.findViewById(R.id.columnInfo_tv);
        chanelId = (TextView) root.findViewById(R.id.play_chanel);
        chanelId_l = (LinearLayout) root.findViewById(R.id.play_chanel_l);
        emptyView_RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });
        getData();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
