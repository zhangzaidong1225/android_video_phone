package com.ifeng.newvideo.videoplayer.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 评论item点击图层
 * Created by yongq_000 on 2015/11/6.
 */
public class CommentClickPopupWindow extends PopupWindow implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(CommentClickPopupWindow.class);

    private Context mContext;
    private TextView agreeTextView;
    private View upView, downView;
    private CommentClickPopCallback mCommentClickPopCallback;
    private int sWidth, sHeight, width, height;


    public CommentClickPopupWindow(Context context, CommentClickPopCallback commentClickPopCallback) {
        super(context);
        mContext = context;
        sWidth = DisplayUtils.getWindowWidth();
        sHeight = DisplayUtils.getWindowHeight();
        width = sWidth / 2; //542
        height = sHeight * 7 / 100; //138
        setWidth(width);
        setHeight(height);
        setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        mCommentClickPopCallback = commentClickPopCallback;
        init();
    }

    private CommentClickPopupWindow init() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.comment_click_pop_layer, null);
        setContentView(contentView);

        setFocusable(true);
        setOutsideTouchable(true);
        setAnimationStyle(android.R.style.Animation_Dialog);
        LinearLayout linearLayout = (LinearLayout) contentView.findViewById(R.id.comment_click_pop_middle_layout);
        ViewGroup.LayoutParams layoutParamsL = linearLayout.getLayoutParams();
        layoutParamsL.width = width;
        layoutParamsL.height = height * 11 / 14;
        linearLayout.setLayoutParams(layoutParamsL);

        upView = contentView.findViewById(R.id.comment_click_pop_up);
        downView = contentView.findViewById(R.id.comment_click_pop_down);
        ViewGroup.LayoutParams layoutParams = upView.getLayoutParams();
        layoutParams.width = width / 9;
        layoutParams.height = height * 3 / 14;
        upView.setLayoutParams(layoutParams);
        downView.setLayoutParams(layoutParams);


        agreeTextView = (TextView) contentView.findViewById(R.id.comment_click_pop_agree);
        agreeTextView.setOnClickListener(this);
        contentView.findViewById(R.id.comment_click_pop_reply).setOnClickListener(this);
        contentView.findViewById(R.id.comment_click_pop_copy).setOnClickListener(this);
        return this;
    }

    /**
     * @param isAgree true 表示 “已赞” false 表示 “赞”
     * @param parent  PopupWindow 的父布局
     */
    public void show(boolean isAgree, View parent) {

        if (isAgree) {
            agreeTextView.setText(R.string.comment_click_agreed);
        } else {
            agreeTextView.setText(R.string.comment_click_agree);
        }

        int[] location = new int[2];
        parent.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        logger.debug("getLocationOnScreen   x : {}  --- y : {}", x, y);

        //true 是展示向上的箭头，false 展示向下的箭头
        if ((y + parent.getPaddingTop()) - DisplayUtils.getStatusBarHeight()
                - (sWidth * 9 / 16) - DisplayUtils.convertDipToPixel(40) < height) {
            upView.setVisibility(View.VISIBLE);
            downView.setVisibility(View.GONE);
            this.showAsDropDown(parent, (parent.getWidth() - width) / 2, 0);
        } else {
            upView.setVisibility(View.GONE);
            downView.setVisibility(View.VISIBLE);
            this.showAsDropDown(parent, (parent.getWidth() - width) / 2, -(parent.getMeasuredHeight() + height));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.comment_click_pop_agree:
                dismiss();
                if (mCommentClickPopCallback != null) {
                    mCommentClickPopCallback.onClickAgree();
                }
                break;
            case R.id.comment_click_pop_reply:
                dismiss();
                if (mCommentClickPopCallback != null) {
                    mCommentClickPopCallback.onClickReply();
                }
                break;
            case R.id.comment_click_pop_copy:
                dismiss();
                if (mCommentClickPopCallback != null) {
                    mCommentClickPopCallback.onClickCopy();
                }
                break;
        }
    }

    public interface CommentClickPopCallback {
        void onClickAgree();

        void onClickReply();

        void onClickCopy();
    }
}


