package com.ifeng.newvideo.videoplayer.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.NetworkError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.videoplayer.adapter.AudioFMAdapter;
import com.ifeng.newvideo.videoplayer.base.FragmentBase;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.exception.IllegalParamsException;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.VideoPlayDao;
import com.ifeng.video.dao.db.model.SubAudioFMListInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 电台的播放的选集页面
 */
public class AudioFMListFragment extends FragmentBase {

    private static final Logger logger = LoggerFactory.getLogger(AudioFMListFragment.class);
    public List<VideoItem> programList = new ArrayList<VideoItem>();//初次传入
    public List<VideoItem> pageList = new ArrayList<VideoItem>();//每次接口返回的数据
    public MyPullToRefreshListView listView;
    public AudioFMAdapter adapter;
    private Bundle bundle;
    private String programId;
    private OnEpisodeTaskListener mOnEpisodeTaskListener;
    private AdapterView.OnItemClickListener mOnItemClickListener;
    private static final int DATA_REFRESH_SUCCESS = 2007;//数据刷新成功
    private static final int DATA_REFRESH_FAIL_NET_ERROR = 2008;//数据刷新失败(网络连接原因)
    private static final int DATA_REFRESH_FAIL_DATA_ERROR = 2009;//数据刷新失败(数据加载原因)
    private int pageNum = 1;
    private int netState = -2;
    private boolean isDownRefresh = false;
    private boolean isloading = false;
    private int selectedPosition = 0;

    public interface OnEpisodeTaskListener {
        void onTaskEpisodeFinished(List<VideoItem> Info, boolean isDownRefrsh);
    }

    public AudioFMListFragment() {
        super();
    }

    public static AudioFMListFragment newInstance(Bundle bundle, OnEpisodeTaskListener listener, AdapterView.OnItemClickListener onItemClickListener, List<VideoItem> list) {
        AudioFMListFragment episodeFragment = new AudioFMListFragment();
        episodeFragment.setArguments(bundle);
        episodeFragment.mOnEpisodeTaskListener = listener;
        episodeFragment.mOnItemClickListener = onItemClickListener;
        if (list != null && list.size() > 0) {
            episodeFragment.programList = list;
        }
        return episodeFragment;
    }

    public void setSelection(int position) {
        if (adapter != null && listView != null) {
            adapter.setSelected(position);
        } else {
            selectedPosition = position;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        programId = bundle.getString(IntentKey.AUDIO_PROGRAM_ID);
        if (programList.size() > 0) {
            pageNum = bundle.getInt(IntentKey.AUDIO_PAGE_NUM);
        }
        logger.debug("requestAudioFMList{},{}", programId, programList.size());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.audio_player_ptr_listview, container, false);
        progressView = root.findViewById(R.id.video_detail_loading_progress_layout);
        emptyView_RL = root.findViewById(R.id.emptyView_RL);
        empty_img = (ImageView) root.findViewById(R.id.empty_nonet_img);
        empty_tv = (TextView) root.findViewById(R.id.empty_tv);
        listView = (MyPullToRefreshListView) root.findViewById(R.id.pull_refresh_list);
        listView.setOnItemClickListener(mOnItemClickListener);
        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        listView.setShowIndicator(false);
        listView.setVisibility(View.VISIBLE);

        initListener();
        emptyView_RL.setOnClickListener(this);
        progressView.setVisibility(View.GONE);
        emptyView_RL.setVisibility(View.VISIBLE);

        return root;
    }

    private void initListener() {
        listView.setMode(PullToRefreshBase.Mode.DISABLED);
        listView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            @Override
            public void onLastItemVisible() {
                if (adapter != null && !isloading) {
                    taskAudioFMList(programId, pageNum, false);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (pageNum == 1 && programList.size() == 0) {
            refreshData(programId);
        } else {
            //service里进来不重新拉数据
            try {
                initListView();
            } catch (IllegalParamsException e) {
                e.printStackTrace();
            }
            setSelection(selectedPosition);
            handler.sendEmptyMessage(DATA_REFRESH_SUCCESS);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.emptyView_RL:
                refreshData(programId);
                break;
        }
    }

    private void changeEmptyViewState(int emptyViewState) {
        if (!isAdded()) {
            return;
        }
        if (emptyViewState == DATA_REFRESH_SUCCESS) {
            emptyView_RL.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);
        } else if (emptyViewState == DATA_REFRESH_FAIL_NET_ERROR) {
            empty_img.setBackgroundResource(R.drawable.common_no_net);
            empty_tv.setText(getResources().getText(R.string.common_net_useless_try_again));
            emptyView_RL.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.GONE);
        } else {
            empty_img.setBackgroundResource(R.drawable.icon_common_load_fail);
            empty_tv.setText(getResources().getText(R.string.load_data_fail_retry));
            emptyView_RL.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.GONE);
        }
    }

    private final Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case DATA_REFRESH_SUCCESS:
                    changeEmptyViewState(DATA_REFRESH_SUCCESS);
                    break;
                case DATA_REFRESH_FAIL_NET_ERROR:
                    changeEmptyViewState(DATA_REFRESH_FAIL_NET_ERROR);
                    break;
                case DATA_REFRESH_FAIL_DATA_ERROR:
                    changeEmptyViewState(DATA_REFRESH_FAIL_DATA_ERROR);
                default:
                    break;
            }
        }

    };

    @Override
    public void onDestroy() {
        VolleyHelper.getRequestQueue().cancelAll(VideoPlayDao.TAG_FM_LIST);
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }

    @Override
    public void refreshData() {
        refreshData(programId);
    }

    public void refreshData(String programId) {
        emptyView_RL.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);
        taskAudioFMList(programId, pageNum, true);
    }

    @SuppressWarnings("unchecked")
    private void taskAudioFMList(String programId, int pagenum, final boolean isFirst) {

        if (isloading) {//如果正在请求则不重新请求
            return;
        } else {
            isloading = true;
        }
        listView.changeState(MyPullToRefreshListView.FootViewState.ISLOADING);
        listView.showFootView();
        if (StringUtils.isBlank(programId)) {
            if (!NetUtils.isNetAvailable(getActivity())) {
                netState = DATA_REFRESH_FAIL_NET_ERROR;
            } else {
                netState = DATA_REFRESH_FAIL_DATA_ERROR;
            }
            if (isFirst) {
                listView.onRefreshComplete();
                listView.hideFootView();
                if (mOnEpisodeTaskListener != null) {
                    mOnEpisodeTaskListener.onTaskEpisodeFinished(null, false);
                }
                handler.sendEmptyMessage(netState);
            } else {
                if (NetUtils.isNetAvailable(getActivity())) {
                    ToastUtils.getInstance().showShortToast(R.string.cache_net_invalid);
                } else {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                }
            }
            return;
        }
        VideoPlayDao.requestAudioFMList(programId,
                pagenum,
                new Response.Listener<SubAudioFMListInfo>() {

                    @Override
                    public void onResponse(SubAudioFMListInfo subAudioFMListInfo) {
                        isloading = false;

                        if (subAudioFMListInfo != null && subAudioFMListInfo.getData() != null) {
                            pageNum++;
                            pageList.clear();
                            for (SubAudioFMListInfo.AudioData.AudioItem audioItem : subAudioFMListInfo.getData().getList()) {
                                VideoItem playerInfoModel = transformAudioFMModel(audioItem);
                                if (playerInfoModel != null) {
                                    pageList.add(playerInfoModel);
                                }
                            }

                            if (isDownRefresh && adapter != null) {
                                isDownRefresh = false;
                                adapter.clearList();
                            }

                            listView.onRefreshComplete();
                            listView.hideFootView();

                            handler.sendEmptyMessage(DATA_REFRESH_SUCCESS);
                            try {
                                refreshView();
                            } catch (IllegalParamsException e) {
                                e.printStackTrace();
                            }

                            if (mOnEpisodeTaskListener != null) {
                                mOnEpisodeTaskListener.onTaskEpisodeFinished(pageList, isDownRefresh);
                            }
                        } else {
                            if (NetUtils.isNetAvailable(getActivity())) {
                                netState = DATA_REFRESH_FAIL_DATA_ERROR;
                            } else {
                                netState = DATA_REFRESH_FAIL_NET_ERROR;
                            }
                            handler.sendEmptyMessage(netState);
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isloading = false;
                        if (error != null) {
                            logger.error(error.toString(), error);
                        }
                        listView.onRefreshComplete();
                        listView.hideFootView();
                        if (error instanceof NetworkError) {
                            netState = DATA_REFRESH_FAIL_NET_ERROR;
                        } else {
                            netState = DATA_REFRESH_FAIL_DATA_ERROR;
                        }
                        if (isFirst) {
                            if (pageList != null && adapter != null && adapter.getList() != null && !adapter.getList().isEmpty()) {
                                if (NetUtils.isNetAvailable(getActivity())) {
                                    ToastUtils.getInstance().showShortToast(R.string.cache_net_invalid);
                                } else {
                                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                                }
                                return;
                            }
                            if (mOnEpisodeTaskListener != null) {
                                mOnEpisodeTaskListener.onTaskEpisodeFinished(null, false);
                            }
                            handler.sendEmptyMessage(netState);
                        } else {
                            if (NetUtils.isNetAvailable(getActivity())) {
                                ToastUtils.getInstance().showShortToast(R.string.cache_net_invalid);
                            } else {
                                ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                            }
                        }
                    }
                });

        if (!isFirst) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.PLAY_FM_V);
        }
    }

    private void refreshView() throws IllegalParamsException {
        if (adapter == null) {
            adapter = new AudioFMAdapter(this, programList);
            adapter.resetSelected();
            listView.setAdapter(adapter);
        }
        adapter.addList(pageList);
        adapter.notifyDataSetChanged();
    }

    private void initListView() throws IllegalParamsException {
        if (adapter == null) {
            adapter = new AudioFMAdapter(this, programList);
            adapter.resetSelected();
            listView.setAdapter(adapter);
        }
        adapter.addList(programList);
        adapter.notifyDataSetChanged();
    }


    private VideoItem transformAudioFMModel(SubAudioFMListInfo.AudioData.AudioItem audioItem) {
        if (audioItem == null || ListUtils.isEmpty(audioItem.audiolist)) {
            return null;
        }
        VideoItem videoItem = new VideoItem();
        videoItem.mUrl = audioItem.audiolist.get(0).filePath;
        videoItem.image = audioItem.getImg370_370();
        videoItem.title = audioItem.getTitle();
        videoItem.guid = audioItem.id;
        videoItem.name = audioItem.programName;
        videoItem.itemId = audioItem.programId;
        videoItem.duration = (Integer.parseInt(audioItem.getDuration()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        videoItem.createDate = sdf.format(new Date(Long.parseLong(audioItem.getCreateTime()) * 1000));
        List<FileType> videoFiles = new ArrayList<>();
        for (SubAudioFMListInfo.AudioData.AudioItem.Audio audio : audioItem.audiolist) {
            FileType fileType = new FileType();
            fileType.mediaUrl = audio.filePath;
            if (TextUtils.isDigitsOnly(audio.size)) {
                fileType.filesize = Integer.parseInt(audio.size);
            } else {
                fileType.filesize = 0;
            }
            fileType.useType = "mp3";
            videoFiles.add(fileType);
        }
        videoItem.videoFiles = videoFiles;
        videoItem.isConvertFromFM = true;

        return videoItem;
    }
}
