package com.ifeng.newvideo.videoplayer.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.*;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.*;
import android.widget.ImageView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.ifeng.IfengProxyUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.dialogUI.CommentEditFragment;
import com.ifeng.newvideo.dialogUI.DialogUtilsFor3G;
import com.ifeng.newvideo.dlna.DLNADeviceManager;
import com.ifeng.newvideo.dlna.DLNAPlayerActivity;
import com.ifeng.newvideo.dlna.DLNAPlayerActivityForVideo;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.utils.ColumnUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.adapter.BottomLayoutInit;
import com.ifeng.newvideo.videoplayer.fragment.CommentFragment;
import com.ifeng.newvideo.videoplayer.listener.VideoPlayerDialogClickListener;
import com.ifeng.newvideo.videoplayer.listener.VideoPlayerTouchListener;
import com.ifeng.newvideo.videoplayer.widget.IfengLandMediaController;
import com.ifeng.newvideo.videoplayer.widget.IfengPortraitMediaController;
import com.ifeng.video.core.net.RequestString;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.*;
import com.ifeng.video.dao.db.constants.ChannelId;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.dao.*;
import com.ifeng.video.dao.db.model.*;
import com.ifeng.video.dao.util.CacheUtil;
import com.ifeng.video.player.ControllerToVideoPlayerListener;
import com.ifeng.video.player.IfengMediaController;
import com.ifeng.video.player.IfengVideoView;
import com.ifeng.video.player.PlayState;
import com.j256.ormlite.dao.Dao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by yuelong on 2014/8/26
 * 视频播放页面的父类，处理视频的播放逻辑
 */
public abstract class ActivityVideoPlayer extends BaseVideoPlayerActivity implements
        IfengMediaController.DoPauseResumeCallBack,
        IfengPortraitMediaController.OnHiddenListener,
        IfengPortraitMediaController.OnShownListener {

    protected static final Logger logger = LoggerFactory.getLogger(ActivityVideoPlayer.class);
    String topicId;
    String topicType;
    private String mOpenFirst = "1";
    public static final int INVALID_URL = -1;
    private static final int MAX_CHANGE_TIMES = 3;
    public int currentPlayStream = MediaConstants.STREAM_HIGH;
    public int currentDownStream = MediaConstants.STREAM_HIGH;
    //广告播放list
    private final Vector<VideoAdInfoModel> mCurCPModelList = new Vector<VideoAdInfoModel>();
    //成功取到了第几条广告
    private int mCurCPProgramIndex = 0;
    //播放到了第几条广告
    public int mCurPlayingCPProgramIndex = 0;
    public boolean isPlayedCP = false;
    public String columnName = "";
    public final List<PlayerInfoModel> programList = new ArrayList<PlayerInfoModel>();
    PlayerInfoModel mCurrentProgram;
    public boolean isPlayerInit = false;
    public boolean isActive = false;
    private final int dlnaDelayTime = 1000;
    private final int STREAM_SWITCH_TOAST_DELAY_TIME = 3000;
    public String mAdClickUrl = "";
    public String mAdId = "";
    //是否正在切换下一条
    boolean isToNextVideo = false;
    private boolean isShowingOverFlowDialog = false;
    public String echid;
    //底部操作栏
    public ImageView mBottomCollect;
    private FragmentManager mFragmentManager;
    public String currProGUID;
    String currProID;
    private int preProgramIndex = -1;
    public int curProgramIndex = 0;

    private PlayerInfoModel preProgram;


    private VideoPlayerTouchListener myTouchListener;//= new VideoPlayerTouchListener(this);


    public final ArrayList<String> mPreToDownloadGuidList = new ArrayList<String>();
    public int mDownLoadSize = 0;
    public IfengType.LayoutType currentLayoutType;
    public IfengType.ListTag currentPlayingTag;
    public IfengType.ListTag currentTag;
    public SubColumnInfoNew subColumnInfo;
    private final DLNADeviceManager mDLNADeviceManager = DLNADeviceManager.getInstance();
    FavoritesDAO favoritesDAO;
    public int mCurrentDownloadState;

    public MyPauseAdPopWindow pauseAdPopWindow;
    private SharedPreferences preferences;
    private final static float SHORT_VIDEO_DURATION = 3 * 60; // s


    boolean isIfengType = false;
    private HistoryDAO mHistoryDAO;
    public boolean isErroring = false;
    public boolean isCompleted = false;
    SubColumnVideoListInfo mSubColumnVideoListInfo;
    public boolean shouldRefresh = true;


    /**
     * 相关一栏里的的视频列表信息
     */
    RelativeVideoListInfo mGuidRelativeVideoListInfo;

    /**
     * 排行一栏里的的视频列表信息
     */
    RankListVideoInfo mRankListVideoInfo;

    /**
     * 热点一栏里的的视频列表信息
     */
    RankListVideoInfo mHotListVideoInfo;
    private boolean isCurrPauseState = false;// 记录离开页面的时候是否本身就为暂停状态
    private final VideoPlayerDialogClickListener mDialogClickListener = new VideoPlayerDialogClickListener(this);
    private DialogUtilsFor3G m3GDialogUtils;
    OneKeyShare mOneKeyShare;
    public boolean hasFragmentAdded = false;
    //AD
    private static final int AD_TIME_COUNTDOWN = 0x1100;
    protected String mFromParamForVideoAd = "";
    protected String mFromParamForVideo = "";

    /**
     * 点播播放次数，for广告播放规则计数
     */
    int mTopicPlayCount;
    private int broadcastPriority = 0;

    private Dao<CacheVideoModel, Integer> dao;
    protected ViewGroup mVideoParentView;

    //正在播放的广告
    public PlayerInfoModel currentPlayingADModel;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger.debug("onCreate");
        initPlayerClickListener();
        initDateUtils();
        initAppStartTime();

        initPauseAdView();
        registerPushReceiver(IntentKey.ACTION_NOTIFY_UI_CHANGE);
    }

    private void initPlayerClickListener() {

        m3GDialogUtils = new DialogUtilsFor3G();
        m3GDialogUtils.setDialogCallBack(mDialogClickListener);
    }

    void playVodVideo() {
        if (mVideoView != null && mVideoView.isInPlaybackState()) {
            mVideoView.stopPlayback();
        }
    }

    @Override
    public String getSelectedStream() {
        String currentSelectStream = "";
        switch (currentPlayStream) {
            case MediaConstants.STREAM_ORIGINAL:
                currentSelectStream = "原画";
                break;
            case MediaConstants.STREAM_SUPPER:
                currentSelectStream = "超清";
                break;
            case MediaConstants.STREAM_HIGH:
                currentSelectStream = "高清";
                break;
            case MediaConstants.STREAM_MID:
                currentSelectStream = "标清";
                break;
            case MediaConstants.STREAM_LOW:
                currentSelectStream = "流畅";
                break;
        }
        return currentSelectStream;
    }

    synchronized void getVideoCp(boolean isFromUser) {
        if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
            resetAdInfo();
            if (mVideoView != null) {
                mVideoView.stopPlayback();
            }
            updateErrorPauseLayer(true);
            return;
        }
        logger.debug("getVideoCp=======start");
        if (isFromUser) {
            resetAdInfo();
            if (isTopicVideo()) {
                if (preProgramIndex != curProgramIndex) {
                    mTopicPlayCount++;
                    preProgramIndex = curProgramIndex;
                }
            } else {
                if ((getCurrProgram() != null && getCurrProgram().getDuration() <= SHORT_VIDEO_DURATION
                        && !isPlayingColumn(getCurrProgram())) || IfengApplication.getInstance().mVodPlayCount == 0) {
                    IfengApplication.getInstance().mVodPlayCount++;
                }
            }
        }
        if (VideoAdConfigDao.adConfig == null || (isUnicomUser() && NetUtils.isMobile(this))) {
            logger.debug("getAdInfo  start adConfig is null");
            autoPlayNextCPVideo();
            return;
        }
        VideoAdConfigModel.VideoADBean.ADUNITIDBean.HeadBean head = VideoAdConfigDao.adConfig.getVideoAD().getADUNITID().getHead();
        List<String> headType = null;//head.get(getFromParamForVideoAd());
        if (mCurCPProgramIndex > headType.size() - 1) {
            autoPlayNextCPVideo();
            return;
        }
        final String typeNum = headType.get(mCurCPProgramIndex);
        int playCount = isTopicVideo() ? mTopicPlayCount : IfengApplication.getInstance().mVodPlayCount;
        String columnFlag = isPlayingColumn(getCurrProgram()) ? subColumnInfo.getSubColumnID() : "";
        String topicFlag = isTopicVideo() ? topicId : "";
        String param = getParam(typeNum, getFromParamForVideoAd(), playCount, getCurrProgram(), columnFlag, echid, topicFlag);
        logger.debug("VideoAdInfoDao is url {}", VideoAdConfigDao.adConfig.getVideoAD().getInterfaceUrl() + param);

        VideoAdInfoDao.getAdInfo(VideoAdConfigDao.adConfig.getVideoAD().getInterfaceUrl(), param, new Response.Listener<Object>() {
            @Override
            public void onResponse(Object response) {
                VideoAdConfigModel.VideoADBean.ADUNITIDBean.HeadBean head = VideoAdConfigDao.adConfig.getVideoAD().getADUNITID().getHead();
                List<String> headType = null;//head.get(getFromParamForVideoAd());
                try {
                    JSONObject jsonObject = JSON.parseObject(response.toString());
                    logger.debug("VideoAdInfoDao json is {}", response.toString());
                    VideoAdInfoModel model = new VideoAdInfoModel(jsonObject);
                    model.setId(typeNum);
                    sendAdImpression(model.getStart());
                    mCurCPModelList.add(model);
                    mCurCPProgramIndex++;
                    if (mCurCPProgramIndex > headType.size() - 1) {
                        logger.debug("mCurCPModelList ======={}", mCurCPModelList.toString());
                        autoPlayNextCPVideo();
                    } else {
                        getVideoCp(false);
                    }
                } catch (Exception e) {
                    mCurCPProgramIndex++;
                    if (mCurCPProgramIndex > headType.size() - 1) {
                        logger.debug("mCurCPModelList ======={}", mCurCPModelList.toString());
                        autoPlayNextCPVideo();
                    } else {
                        getVideoCp(false);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.debug("VideoAdInfoDao json is error");
                logger.debug("getAdInfo  start onErrorResponse is error");
                VideoAdConfigModel.VideoADBean.ADUNITIDBean.HeadBean head = VideoAdConfigDao.adConfig.getVideoAD().getADUNITID().getHead();
                List<String> headType = null;// head.get(getFromParamForVideoAd());
                mCurCPProgramIndex++;
                if (mCurCPProgramIndex > headType.size() - 1) {
                    logger.debug("mCurCPModelList ======={}", mCurCPModelList.toString());
                    autoPlayNextCPVideo();
                } else {
                    getVideoCp(false);
                }
            }
        });
    }

    public void hidePauseAdPopWindow() {
        if (pauseAdPopWindow != null) {
            pauseAdPopWindow.dismissAd();
        }
    }

    private void playAdVideo(VideoAdInfoModel model) {
        isPlayedCP = false;
        PlayerInfoModel result = new PlayerInfoModel(null, null, model.getId(), null,
                model.getUrl(), model.getUrl(), model.getUrl(), model.getUrl(), model.getUrl(),
                DateUtils.getTimeInSeconds(model.getLength()), 0, 0, 0, 0, 0, null, null, null, null, null, null, null, null);
        bindListener();
        logger.debug("mCurPlayingCPProgramIndex======={}", mCurPlayingCPProgramIndex);
        if (mCurPlayingCPProgramIndex == 0) {
            isAdSilent = false;
        }

        playCPVideo(result);
        mAdClickUrl = model.getClick();
        mAdId = model.getId();
    }

    private void sendAdImpression(ArrayList<String> impressions) {
        for (final String impression : impressions) {
            if (impression != null) {
                RequestString requestString = new RequestString(Request.Method.GET, impression, null,
                        new SendAdImpSuccessListener(impression), new SendAdImpErrorListener());
                VolleyHelper.getRequestQueue().add(requestString);
            }
        }
    }

    // 播放广告
    private void playCPVideo(PlayerInfoModel program) {
        if (program == null) {
            ToastUtils.getInstance().showShortToast(R.string.video_play_url_error);
            return;
        }
        currentPlayingADModel = program;
        String url = program.getAvailableHighVideoURL();
        if (mVideoView != null && mVideoView.isInPlaybackState()) {
            mVideoView.stopPlayback();
        }
        if (!JsonUtils.checkDataInJSONObject(url)) {
            ToastUtils.getInstance().showShortToast(R.string.video_play_url_error);
        }
        if (show3GDialogForCp()) {
            return;
        }
        sendAdTime(0, true);
        videoPlayerInitOnline(url);
    }

    private void initAppStartTime() {
        if (mSharePreUtils.getStartTime() <= 3) {
            if (IfengApplication.getInstance().getAttribute(IntentKey.VIDEO_START_TIME) == null
                    || ((Integer) IfengApplication.getInstance().getAttribute(IntentKey.VIDEO_START_TIME)).intValue() == 0) {
                ToastUtils.getInstance().showShortToast(getString(R.string.toast_audio_video_switch));
                IfengApplication.getInstance().setAttribute(IntentKey.VIDEO_START_TIME, Integer.valueOf(-1));
            }
        }
    }

    protected void initDateUtils() {
        super.initDateUtils();
        mOneKeyShare = new OneKeyShare(IfengApplication.getInstance());
        preferences = getSharedPreferences("province", Context.MODE_PRIVATE);
        if (!isOffLine()) {
            mCurVideoLayout = IfengVideoView.VIDEO_LAYOUT_PORTRAIT;
        } else {
            mCurVideoLayout = IfengVideoView.VIDEO_LAYOUT_FULL;
        }
        mVideoView.setVideoLayout(mCurVideoLayout);
        myTouchListener = new VideoPlayerTouchListener(this);
        mVideoController = new IfengPortraitMediaController(this);
        mHistoryDAO = HistoryDAO.getInstance(this);
        favoritesDAO = FavoritesDAO.getInstance(this);

        currentPlayStream = mSharePreUtils.getVodCurrentStream();
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mFromParamForVideoAd = intent.getExtras().getString(IntentKey.FROM_PARAM_FOR_VIDEO_AD);
            mFromParamForVideo = intent.getExtras().getString(IntentKey.FROM_PARAM_FOR_VIDEO);

            playPosWhenSwitchAV = (int) intent.getExtras().getLong(IntentKey.HISTORY_BOOK_MARK);
            logger.debug("playPosWhenSwitchAV={}", "" + playPosWhenSwitchAV);
            if (playPosWhenSwitchAV != 0) {
                ToastUtils.getInstance().showShortToast(getWatchedTimeString((long) playPosWhenSwitchAV));
            }
        }

    }


    private String getWatchedTimeString(long watchedTime) {
        if (watchedTime != -1) {
            watchedTime /= 1000;
            int h = (int) (watchedTime / 3600);
            int m = (int) ((watchedTime % 3600) / 60);
            int s = (int) (watchedTime % 60);
            StringBuilder builder = new StringBuilder();
            builder.append("0");
            builder.append(h);
            builder.append(":");
            if (m / 10 == 0) builder.append("0");
            builder.append(m);
            builder.append(":");
            if (s / 10 == 0) builder.append("0");
            builder.append(s);
            if (h == 0 && m == 0) {
                return getResources().getString(R.string.history_one_minute);
            } else {
                return getResources().getString(R.string.history_player_last) + builder.toString() + getResources().getString(R.string.history_player_continue);
            }
        } else {
            return getResources().getString(R.string.history_player_replay);
        }
    }


    private final List<Boolean> isCanPlayOfflineAudioList = new ArrayList<Boolean>();


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        handleAdLayerChildView();
        showTitleView(getTitleText());
    }

    private void handleAdLayerChildView() {

        VolleyHelper.getRequestQueue().cancelAll(VideoAdInfoDao.TAG);

    }

    /**
     * 是否正在播放片头广告
     *
     * @return
     */
    public boolean isPlayingAdTitles() {
        try {
            return "sponsorhead".equalsIgnoreCase(mCurCPModelList.get(mCurPlayingCPProgramIndex).getPos());
        } catch (Exception e) {
            return false;
        }
    }

    private void initPauseAdView() {
        pauseAdPopWindow = new MyPauseAdPopWindow();
    }

    /**
     * 用于广告请求，不同类型，会返回不同广告
     * <p/>
     * 平台标识（不可为空）。
     * news：资讯下的所有频道，除专题、焦点、联播台频道
     * column：栏目频道内所有栏目
     * documentary：纪实频道所有频道
     * special：专题频道
     * lianbo：联播台频道
     * focus：焦点频道
     */
    String getFromParamForVideoAd() {
        if (isTopicVideo()) {
            if (StringUtils.isBlank(topicType)) {
                return "special";
            } else if (CheckIfengType.isFocus(topicType)) {
                return "focus";
            } else if (CheckIfengType.isLianBo(topicType)) {
                return "lianbo";
            } else if (CheckIfengType.isCmppTopic(topicType)) {
                return "special";
            }
        }
        if (ChannelId.SUBCHANNELID_COLUMN.equalsIgnoreCase(echid) || isColumnVideo()) {
            if (isPlayingColumn(getCurrProgram())) {
                return "column";
            } else {
                if (!StringUtils.isBlank(mFromParamForVideoAd)) {
                    return mFromParamForVideoAd;
                }
                return "news";
            }
        } else if (ChannelId.SUBCHANNELID_DOCUMENTARY.equalsIgnoreCase(echid)) {
            return "documentary";
        } else {
            if (!StringUtils.isBlank(mFromParamForVideoAd)) {
                return mFromParamForVideoAd;
            }
            return "news";
        }
    }


    public void refreshVideoPlay() {
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
        prepareToPlay();

    }


    private boolean isInPlaybackState() {
        boolean isInPlaybackState = mVideoView != null && (mVideoView.isInPlaybackState() && mVideoView.mCurrentState != PlayState.STATE_PLAYBACK_COMPLETED);
        logger.debug("isInPlaybackState = {}", isInPlaybackState);
        return isInPlaybackState;
    }


    private boolean isDialogShow() {
        if (isShowingOverFlowDialog) {
            return true;
        }
        if (m3GDialogUtils == null) {
            return false;
        }
        if (m3GDialogUtils.businessVideoDialog != null && m3GDialogUtils.businessVideoDialog.isShowing()) {
            return true;
        }
        if (m3GDialogUtils.m3GNetAlertDialog != null && m3GDialogUtils.m3GNetAlertDialog.isShowing()) {
            return true;
        }
        if (m3GDialogUtils.mNoMobileOpenDialog != null && m3GDialogUtils.mNoMobileOpenDialog.isShowing()) {
            return true;
        }
        return m3GDialogUtils.mCurMobileWapDialog != null && m3GDialogUtils.mCurMobileWapDialog.isShowing();
    }


    private boolean isMobileNetOpen() {
        boolean isMobile = NetUtils.isMobile(this);
        boolean isMobileOpen = true;
        return !isMobile || isMobileOpen;
    }

    @Override
    protected void onStart() {
        super.onStart();
        logger.debug("onStart");


        if (!isOffLine()) {
            sensorManager.registerListener(sensorListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public synchronized void getPlayInfoByGuid(String guid) {

    }


    private GestureDetector popBottomViewDetector;


    /**
     * 广告播完回调
     */
    public void columnCPCompletedListener() {
        mCurPlayingCPProgramIndex++;
        autoPlayNextCPVideo();
    }

    /**
     * 插入看过
     */
    public void insertWatched() {
        if (getCurrProgram() == null || isIfengType) {
        }

    }

    private void insertPreWatched() {
        if (preProgram == null || isIfengType) {
        }

    }

    private HistoryModel getHistoryModel(PlayerInfoModel playerModel) {
        if (playerModel == null) {
            return null;
        }
        HistoryModel model = new HistoryModel();
        model.setProgramGuid(playerModel.getGuid());
        if (isPlayingColumn(playerModel) || (playerModel.getType() != null && CheckIfengType.isColumn(playerModel.getType()))) {
            model.setColumnName(columnName);
            model.setType(IfengType.TYPE_COLUMN);
        } else if (isTopicVideo()) {
            model.setType(IfengType.TYPE_TOPIC);
        } else {
            model.setType(IfengType.TYPE_VIDEO);
        }
        model.setName(playerModel.getName());

        model.setImgUrl(playerModel.stage_url_photo);
        model.setExtra1(getFromParamForVideoAd());
        addTopicData(model);
        model.setResource(HistoryModel.RESOURCE_VIDEO);
        return model;
    }


    /**
     * 专题复写，添加topicId，topicType
     *
     * @param model
     */
    void addTopicData(HistoryModel model) {

    }

    public boolean isPlayingColumn(PlayerInfoModel model) {
        return model != null && isColumnVideo() && model.getCurrentPlayingTag() != null && model.getCurrentPlayingTag() == IfengType.ListTag.columnplayed;
    }

    public boolean isTopicVideo() {
        return currentLayoutType == IfengType.LayoutType.topic;
    }

    public boolean isColumnVideo() {
        return currentLayoutType == IfengType.LayoutType.column;
    }

    /**
     * 初始化底部的操作栏
     */
    void initBottomLayout() {
        //TODO:--------------------TODO

        popBottomViewDetector = BottomLayoutInit.create(this, (ViewGroup) this.findViewById(R.id.video_bottom), null);
        if (BottomLayoutInit.bottomLayout != null) {
            BottomLayoutInit.bottomLayout.setVisibility(View.VISIBLE);

            BottomLayoutInit.bottomLayout.showBottomLayout();
        }
    }


    @Override
    protected void onResume() {
        logger.debug("onResume");


        if (isClickSinaWeibo) {
            isClickSinaWeibo = false;

        }
        if (!isLandScape()) {
            DisplayUtils.setDisplayStatusBar(this, false);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        if (mobileNetShowing) {//如果正在显示运营商网络界面，直接返回bug #10026
            super.onResume();
            return;
        }
        //初始化统计v
        if (IfengApplication.isShareVertialActivityFinish) {
            IfengApplication.isShareVertialActivityFinish = false;
        } else {
            // playStateChangeListener.insertCustomerStatistics(PlayState.STATE_PREPARING);
        }
        logger.debug("isSurfaceCreate={}, isCurrPauseState={}, isActive={}", mVideoView.isSurfaceCreate, isCurrPauseState, isActive);
        if (!isCurrPauseState) {
            OneKeyShare.isLandWebQQ = false;
            if (mVideoController != null && mVideoView != null) {
                final boolean isToContinue = playPosWhenSwitchAV != 0 && playAdPosWhenSwitchAV != 0;
                if (isToContinue && !isActive) {
                    logger.debug("onResume playPosWhenSwitchAV==={}", playPosWhenSwitchAV);

                }
                mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
                mVideoView.start();
            }
        } else {

        }
        isActive = true;


        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
        logger.debug("!!!!!! ActivityVideoPlayer onPause");
        hideLandRight();
        if (mVideoView != null) {
            if (mVideoView.isInPlaybackState()) {
                getVideoOrAudioPosition();
                logger.debug("OnPause playPosWhenSwitchAV==={}", playPosWhenSwitchAV);
            }
            isCurrPauseState = !mVideoView.isPlaying();
            if (!NetUtils.isNetAvailable(this)) {
                isCurrPauseState = false;
            }
            if (isCurrPauseState) {
                if (mVideoController != null && mVideoView.isInPlaybackState()) {
                    if (mVideoView != null) {
                        getVideoOrAudioPosition();
                        mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                    }
                    mVideoView.pause();
                } else {
                    if (mVideoView != null) {
                        mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                    }
                }
            } else {
                mVideoView.pause();

            }
        }
    }

    @Override
    protected void onStop() {
        logger.debug("!!!!!! ActivityVideoPlayer onStop");
        isActive = false;
        if (!isOffLine()) {
            sensorManager.unregisterListener(sensorListener, sensor); // 取消监听器
        }
        if (mVideoView != null && mVideoView.isInPlaybackState()) {
            if (mVideoController != null) {
                mVideoView.setPausePlayState(PlayState.STATE_PAUSED);
                mVideoView.pause();
            }
            //playStateChangeListener.handleIdleStatistics();
        }
        if (isShareShow) {
            hasHomeClick = true;
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logger.debug("!!!!!! ActivityVideoPlayer onDestroy");
        unRegisterPushReceiver();
        adTimeHandle.removeMessages(AD_TIME_COUNTDOWN);

        VolleyHelper.getRequestQueue().cancelAll(VideoPlayDao.SINGLEVIDEOBYGUIDTAG);
        VolleyHelper.getRequestQueue().cancelAll(VideoAdInfoDao.TAG);
    }

    public void prepareToPlay() {

        //更新评论内容
        updateComment(true);

        if (mVideoView != null && mVideoView.isInPlaybackState()) {
            mVideoView.stopPlayback();
        }


        if (isOffLine()) {
            videoPlayerInitOffline(getCurrProgram().getGuid());
        } else {
            videoPlayerInitOnline(getCorrectUrl());
        }
        isPlayerInit = true;
        logger.debug("prepareToPlay------end---------");
        updateDateAndViewByPlay();
    }


    public void updateComment(boolean shouldClean) {
        if (commentEditFragment != null && shouldClean) {
            commentEditFragment.lastComment = "";
        }
    }

    protected void updateDateAndViewByPlay() {
        //更新看过数据
        preProgram = getCurrProgram();
        if (preProgram == null) {
            return;
        }
        preProgram.setCurrentPlayingTag(currentPlayingTag);
        PlayerInfoModel playerInfoModel = getCurrProgram();


        //用于分享
        mOneKeyShare.getShareUrl(getCurrProgram().getShareUrl());
        //判断是否有音频
        hasAudio = getCurrProgram() != null && JsonUtils.checkDataInJSONObject(getCurrProgram().getMediaAudioUrl());
        if (isOffLine() && getCurrProgram() != null) {
            try {
                String url = CacheManager.getAudioPath(this, getCurrProgram().getGuid());
                hasAudio = !StringUtils.isBlank(url);
            } catch (Exception e) {
                logger.error("offline audioUrl is null");
                hasAudio = false;
            }
        }
    }


    public boolean showDialogFor3G() {
        logger.debug("Business  showDialogFor3G============{}");
        if (isDialogShow()) {
            updateMobileNetLayer(true);
            return true;
        }
        if (isActive) {
            if (NetUtils.isMobile(this)) {
                logger.debug("Business  show3GNetAlertDialog============{}");
                if (IfengApplication.mobileNetCanPlay) {
                    //提示运营商网络
                    ToastUtils.getInstance().showLongToast(R.string.play_moblie_net_hint);
                    return false;
                } else {
                    updateMobileNetLayer(true);
                    return true;
                }
            }
        }
        return false;
    }

    public void continueMobilePlay() {
        IfengApplication.mobileNetCanPlay = true;
        updateMobileNetLayer(false);
        if (mVideoView != null) {
            if (mVideoView.isPauseState()) {
                mVideoView.start();
            } else {
                if (getCurrProgram() == null) {
                    getPlayInfoByGuid(currProGUID);
                    return;
                }
                refreshVideoPlay();
            }
        }
    }

    private boolean show3GDialogForCp() {
        if (!isOffLine()) {
            if (NetUtils.isMobile(this) && !isMobileNetOpen()) {
                updateErrorPauseLayer(true);
                m3GDialogUtils.showNoMobileOpenDialog(this);
                return true;
            } else if (NetUtils.isMobile(this)) {
                if (IfengApplication.mobileNetCanPlay) {
                    //提示运营商网络
                    if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                        ToastUtils.getInstance().showLongToast(R.string.play_moblie_net_hint);
                        return false;
                    }
                } else {
                    updateMobileNetLayer(true);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isUnicomUser() {
        return false;
    }

    private boolean show3GDialogForNetChange() {
        if (isDialogShow()) {
            updateErrorPauseLayer(true);
            return true;
        }
        if (isActive) {
            if (isMobileNetOpen()) {
                if (!IfengApplication.mobileNetCanPlay) {
                    getVideoOrAudioPosition();
                    mVideoView.pause();
                    updateMobileNetLayer(true);
//                        m3GDialogUtils.show3GNetAlertDialog(this);
                    return true;
                } else {
                    if (isInPlaybackState()) {
                        if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                            ToastUtils.getInstance().showLongToast(R.string.play_moblie_net_hint);
                            return true;
                        }
                    }
                    getVideoOrAudioPosition();
                    prepareToPlay();
                    return true;
                }
            } else {
                updateErrorPauseLayer(true);
                mVideoView.stopPlayback();
                m3GDialogUtils.showNoMobileOpenDialog(this);
                return true;
            }
        }
        return false;
    }

    void refreshDataOnlyVideo() {
        getVideoOrAudioPosition();
        resetAdPlayPosition();
        curProgramIndex--;
        autoSwitchNextVideo();
    }

    protected void updateVideoMediaController() {
        if (isLandScape()) {
            mVideoController = new IfengLandMediaController(this);
        } else {
            mVideoController = new IfengPortraitMediaController(this);
        }
        mVideoController.setOnHiddenListener(this);
        mVideoController.setOnShownListener(this);
        mVideoController.setControllerToVideoPlayerListener(controllerListener);
        mVideoController.setDoPauseResumeCallBack(this);
        mVideoView.setMediaController(mVideoController);

        updateDLNAView();
    }


    /**
     * 返回现在这个guid所对应的playInfo
     */
    private int getCurrPlayProIndexByGuid(String proGuid) {
        if (ListUtils.isEmpty(programList)) {
            logger.error("video play list invalid!");
            return 0;
        }
        for (int i = 0; i < programList.size(); i++) {
            if (proGuid != null && programList.get(i) != null && programList.get(i).getGuid() != null && proGuid.equals(programList.get(i).guid)) {
                return i;
            }
        }
        return curProgramIndex;
    }

    /**
     * 返回现在这个id所对应的playInfo
     */
    protected int getCurrPlayProIndexById(String id) {
        if (ListUtils.isEmpty(programList)) {
            throw new NullPointerException("video play list invalid!");
        }
        if (id == null) {
            return -1;
        }
        for (int i = 0; i < programList.size(); i++) {
            if (id.equals(programList.get(i).id)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void handleOfflineVideoPlayError() {
        autoSwitchNextVideo();
    }

    void updatePlayerInfoModelList(PlayerInfoModel program) {
        if (programList.size() > 0) {
            programList.clear();
        }
        programList.add(program);
    }

    public void addPlayerInfoModelToList(PlayerInfoModel program) {
        if (programList != null && program != null) {
            programList.add(program);
        }
    }

    /**
     * 专为专题统计留的一个接口
     *
     * @param vRecord
     * @return 专题的VodRecord
     */
    public VodRecord getTopicVodRecord(VodRecord vRecord) {
        return null;
    }

    /**
     * 此方法主要是提供专题转换chid使用
     *
     * @param id
     * @param type
     */
    private String getTopicChid(String id, String type) {
        if (CheckIfengType.isCmppTopic(type)) {
            return "12768_" + id;
        }
        if (CheckIfengType.isLianBo(type)) {
            return "12766_" + id;
        }
        if (CheckIfengType.isFocus(type)) {
            return "12767_" + id;
        }
        if (CheckIfengType.isImcpTopic(type)) {
            return "12765_" + id;
        }
        return "";
    }

    public void removePlayerInfoModelFromList(PlayerInfoModel program) {
        if (programList != null && program != null) {
            programList.remove(program);
        }
    }


    @Override
    public PlayerInfoModel getCurrProgram() {
        if (ListUtils.isEmpty(programList)) {
            return null;
        }
        if (curProgramIndex >= programList.size() - 1) {
            curProgramIndex = programList.size() - 1;
        }
        if (curProgramIndex <= 0) {
            curProgramIndex = 0;
        }
        return programList.get(curProgramIndex);
    }

    PlayerInfoModel getNextProgram() {
        if (ListUtils.isEmpty(programList)) {
            return null;
        }
        curProgramIndex++;
        if (curProgramIndex == programList.size() || curProgramIndex < 0) {
            curProgramIndex = 0;
        }
        return programList.get(curProgramIndex);
    }

    private PlayerInfoModel getPreProgram() {
        if (programList == null) {
            return null;
        }
        curProgramIndex--;
        if (curProgramIndex == -1) {
            curProgramIndex = programList.size() - 1;
        }
        return programList.get(curProgramIndex);
    }

    public void autoSwitchNextVideo() {

        mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
        PlayerInfoModel pro = getNextProgram();
        if (pro != null) {
            currProGUID = pro.getGuid();
            prepareToPlay();
        }
    }

    public void autoSwitchPreVideo() {
        PlayerInfoModel pro = getPreProgram();
        currProGUID = pro.getGuid();
        prepareToPlay();
    }


    //改变底部栏的订阅状态，栏目重写
    void updateSubscribeBtnImgBg() {

    }

    private void updateSurscribeBtnVisibility(boolean show) {
        ImageView subscribe = (ImageView) this.findViewById(R.id.video_detail_root).findViewById(R.id.bottom_subscribe_iv);
        if (subscribe != null) {
//            subscribe.setVisibility(show ? View.VISIBLE : View.GONE);
            subscribe.setVisibility(show ? View.GONE : View.GONE);
        }
    }


    public boolean isOffLine() {
        return currentLayoutType == IfengType.LayoutType.offline;
    }


    /**
     * 获取选集数据
     *
     * @return
     */
    public String[] getRightListData() {
        String[] channels;
        if (programList == null || programList.isEmpty()) {
            return null;
        }
        channels = new String[programList.size()];
        for (int i = 0; i < channels.length; i++) {
            channels[i] = programList.get(i).getName();
        }
        return channels;
    }

    /**
     * 获取下载列表数据
     *
     * @return
     */
   /* public String[] getDownloadListData() {
        String[] channels;
        if (programList == null || programList.isEmpty()) {
            return null;
        }
        List<PlayerInfoModel> downloadProgramList = new ArrayList<PlayerInfoModel>();
        for (int i = 0;i<programList.size();i++){
            if (CheckIfengType.isLiveType(programList.get(i).getType())){
                continue;
            }
            downloadProgramList.add(programList.get(i));
        }
        channels = new String[downloadProgramList.size()];
        for (int i = 0; i < channels.length; i++) {
            channels[i] = downloadProgramList.get(i).getName();
        }
        return channels;
    }*/
    public void resetPlayPosition() {
        playPosWhenSwitchAV = 0;
    }

    public void resetAdPlayPosition() {
        playAdPosWhenSwitchAV = 0;
    }

    public void resetAllPlayerPostion() {
        resetAdPlayPosition();
        resetPlayPosition();
        mVideoView.resetPrePosition();
    }

    @Override
    public boolean doPauseOrResum(boolean isDoPause) {
        logger.debug("VideoAdInfoDao adConfig{}", VideoAdConfigDao.adConfig);
        if (VideoAdConfigDao.adConfig != null && !isOffLine() && !(isUnicomUser() && NetUtils.isMobile(this))) {
            if (isDoPause) {

                try {
                    VolleyHelper.getRequestQueue().cancelAll(VideoAdInfoDao.TAG);
                    String typeNum = VideoAdConfigDao.adConfig.getVideoAD().getADUNITID().getPause().get(getFromParamForVideoAd());
                    int playCount = isTopicVideo() ? mTopicPlayCount : IfengApplication.getInstance().mVodPlayCount;
                    String columnFlag = isPlayingColumn(getCurrProgram()) ? subColumnInfo.getSubColumnID() : "";
                    String topicFlag = isTopicVideo() ? topicId : "";
                    pauseAdPopWindow.setmMediaPlayerControl(mVideoView);
                    pauseAdPopWindow.setParam(typeNum, getFromParamForVideoAd(), playCount, getCurrProgram(), columnFlag, echid, topicFlag, mOpenFirst);
                    pauseAdPopWindow.showPopAd(isLandScape());
                } catch (Exception e) {
                    logger.debug("VideoAdInfoDao pauseAdPopWindow error  dismiss");
                    pauseAdPopWindow.dismissAd();
                }
            } else {
                logger.debug("VideoAdInfoDao pauseAdPopWindow  dismiss");
                pauseAdPopWindow.dismissAd();
            }
        }
        return isDialogShow();
    }

    /**
     * 没有加载出资源时候弹的dialog
     */
    private Dialog dialog;

    public void showNoResourceDialog() {
        if (isFromPush()) {
            AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.video_play_url_miss), getString(R.string.common_i_know), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    finish();
                    Intent intent = new Intent(ActivityVideoPlayer.this, ActivityMainTab.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }
    }


    private String getCorrectUrl() {
        PlayerInfoModel curProgram = getCurrProgram();
        if (curProgram == null) {
            return null;
        }
        String url = null;
        if (currentPlayStream == INVALID_URL) {
            currentPlayStream = MediaConstants.STREAM_HIGH;
        }
        //不符合播高清的机子
//        if ((Boolean) IfengApplication.getInstance().getAttribute(IfengVideoApplication.PLAY_STREAM_MID)) {
//            if (currentPlayStream == MediaConstants.STREAM_HIGH) {
//                currentPlayStream--;
//            }
//        }
        int times = 0;
        while (!JsonUtils.checkDataInJSONObject(url)) {
            switch (currentPlayStream) {
                case MediaConstants.STREAM_ORIGINAL:
                    url = curProgram.changeVideoURL(MediaConstants.STREAM_ORIGINAL);
                    break;
                case MediaConstants.STREAM_SUPPER:
                    url = curProgram.changeVideoURL(MediaConstants.STREAM_SUPPER);
                    break;
                case MediaConstants.STREAM_HIGH:
                    url = curProgram.changeVideoURL(MediaConstants.STREAM_HIGH);
                    break;
                case MediaConstants.STREAM_MID:
                    url = curProgram.changeVideoURL(MediaConstants.STREAM_MID);
                    break;
                case MediaConstants.STREAM_LOW:
                    url = curProgram.changeVideoURL(MediaConstants.STREAM_LOW);
                    break;
                default:
                    break;
            }
            if (times == MAX_CHANGE_TIMES) {
                break;
            }
            if (!JsonUtils.checkDataInJSONObject(url)) {
                times++;
                currentPlayStream--;
                if (currentPlayStream == INVALID_URL) {
                    currentPlayStream = MediaConstants.STREAM_HIGH;
                }
            }
        }
        currentPlayStream = curProgram.playingDataStream;
        url = IfengProxyUtils.getProxyUrl(url);
        url = VDNIdUtils.addId(url);
        logger.debug("video playUrl = {}", url);
        return url;
    }


    /**
     * Controll隐藏后的回调函数
     */
    @Override
    public void onHidden() {

        mOneKeyShare.popDismiss();
//        if (mLeftClock.getVisibility() == View.VISIBLE) {
//            mLeftClock.setVisibility(View.GONE);
//        }

    }

    /**
     * Controll显示后的回调函数
     */
    @Override
    public void onShown() {
        if (isLandScape()) {
            hideLandRight();
            DisplayUtils.setDisplayStatusBar(this, false);
//            updateVolumeByKeyEvent();
        }
        updateSelectView();
        if (mVideoController != null && mVideoController.mSelectStream != null) {
            if (isOffLine()) {
                mVideoController.mSelectStream.setVisibility(View.GONE);
                mVideoController.mSelectStreamLine.setVisibility(View.GONE);
            } else {
                mVideoController.mSelectStream.setVisibility(View.VISIBLE);
                mVideoController.mSelectStreamLine.setVisibility(View.VISIBLE);
                mVideoController.mSelectStream.setTextColor(getResources().getColor(R.drawable.common_pop_spinner_text_selector));
                mVideoController.mSelectStream.setClickable(true);
            }
        }
        updateSurbseView();
        showTitleView(getTitleText());

    }


    private String getTitleText() {
        if (isLandScape()) {
            if (currentLayoutType == IfengType.LayoutType.column) {

                if (getCurrProgram() != null) {
                    String title = "";
                    if (subColumnInfo != null) {
                        title = ColumnUtils.convertColumnTitle(getCurrProgram().getName(), subColumnInfo.getSubColumnName());
                    }
                    if (currentPlayingTag == IfengType.ListTag.ranking) {
                        title = getCurrProgram().getName();
                    }
                    return title;
                }

            } else {
                if (getCurrProgram() != null) {
                    return getCurrProgram().getName();
                }
            }
        }
        return "";
    }


    public void updateSurbseView() {
        if (currentLayoutType == IfengType.LayoutType.column) {
            updateSurscribeBtnVisibility(true);
        } else {
            updateSurscribeBtnVisibility(false);
        }
        updateSubscribeBtnImgBg();
    }


    public void updateDLNAView() {
        boolean showDLNA = mSharePreUtils.getDLNAState()
                && mDLNADeviceManager.hasDevice()
                && getCurrProgram() != null
                && JsonUtils.checkDataInJSONObject(getCurrProgram().getAvailableHighVideoURL());

        ImageView right_DLNA_iv = null;
        ImageView bottom_DLNA_iv = null;
        View dlnaDivider = null;
        if (BottomLayoutInit.bottomLayout != null) {
            bottom_DLNA_iv = (ImageView) BottomLayoutInit.bottomLayout.findViewById(R.id.bottom_dlna_iv);
        }
        if (mVideoController != null && mVideoController.mDlnaButton != null) {
            right_DLNA_iv = mVideoController.mDlnaButton;
            dlnaDivider = mVideoController.mDlnaDivider;
        }
        if (showDLNA) {
            if (right_DLNA_iv != null) {
                right_DLNA_iv.setVisibility(View.VISIBLE);
                if (dlnaDivider != null) {
                    dlnaDivider.setVisibility(View.VISIBLE);
                }
            }
            if (bottom_DLNA_iv != null) {
//                bottom_DLNA_iv.setVisibility(View.VISIBLE);
                bottom_DLNA_iv.setVisibility(View.GONE);
            }
        } else {
            if (bottom_DLNA_iv != null) {
                bottom_DLNA_iv.setVisibility(View.GONE);
            }
            if (right_DLNA_iv != null) {
                right_DLNA_iv.setVisibility(View.GONE);
                if (dlnaDivider != null) {
                    dlnaDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isLandScape()) {
            if (mVideoController != null) {
                if (mVideoView.isInPlaybackState() && !mobileNetShowing && myTouchListener.onTouchEvent(ev)) {
                    return true;
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        //竖屏事件处理
        else {
            if (mVideoController != null) {
                if (mVideoView.isInPlaybackState() && !mobileNetShowing && myTouchListener.onTouchEvent(ev)) {
                    return true;
                }
            }
            //底部栏事件处理
            if (popBottomViewDetector != null) {
                if (popBottomViewDetector.onTouchEvent(ev)) {
                    return true;
                }
            }
            return super.dispatchTouchEvent(ev);
        }
    }


    public CacheFolderModel getCacheFolderModel() {
        CacheFolderModel model = new CacheFolderModel();
        if (getCurrProgram() == null) {
            return null;
        }
        if (isPlayingColumn(getCurrProgram())) {
            model.setGuid(columnName);
            model.setName(columnName);
            model.setImgUrl(subColumnInfo.getStills());
            model.setType(IfengType.TYPE_COLUMN);
            model.setId(CacheUtil.getIdFromGuid(IfengType.TYPE_COLUMN, columnName));
            return model;
        } else {
            return null;
        }
    }

    private void updateSelectView() {
        if (mVideoController != null) {
            mVideoController.updateSelectStreamBtn(!(isOffLine()));
            mVideoController.updateSelectStreamView(currentPlayStream);
        }
    }

    public void autoPlayNextCPVideo() {
        if (!isOffLine()) {
            if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                if (mCurPlayingCPProgramIndex > mCurCPModelList.size() - 1) {
                    preparePlayPositive();
                }
                if (mVideoView != null) {
                    mVideoView.stopPlayback();
                }
                updateErrorPauseLayer(true);
                return;
            }
        }
        mOpenFirst = "0";
        if ((VideoAdConfigDao.adConfig == null) || (isUnicomUser() && NetUtils.isMobile(this))) {
            preparePlayPositive();
            playVodVideo();
            return;
        }
        if (mCurPlayingCPProgramIndex > mCurCPModelList.size() - 1) {
            preparePlayPositive();
            playVodVideo();
        } else {
            playAdVideo(mCurCPModelList.get(mCurPlayingCPProgramIndex));
        }
    }

    private void preparePlayPositive() {
        isPlayedCP = true;
        resetAdInfo();

    }

    void resetAdInfo() {
        VolleyHelper.getRequestQueue().cancelAll(VideoAdInfoDao.TAG);
        mCurCPProgramIndex = 0;
        mCurPlayingCPProgramIndex = 0;
        mCurCPModelList.clear();
//        if (tvCountDown != null) {
//            tvCountDown.setText("");
//        }
    }

    private final ControllerToVideoPlayerListener controllerListener = new ControllerToVideoPlayerListener() {

        @Override
        public void onGyroClick() {

        }

        @Override
        public void onFullScreenClick() {
            if (isDialogShow()) {
                return;
            }
            switchOrientation();
        }

        @Override
        public void onSubscribeClick() {
            if (isDialogShow()) {
                return;
            }
            updateSurbseView();
        }


        @Override
        public void onDlnaClick() {
            if (isDialogShow()) {
                return;
            }
            showDLNADialog(getCurrProgram());
        }

        @Override
        public void onStreamItemClick(int definition) {

        }

        @Override
        public void onSelectClick() {
            showLandRight(RIGHT_LAYER_STREAM);
        }

        @Override
        public void onLockClick() {
        }

        @Override
        public void onSwitchAVMode() {
            if (isDialogShow()) {
            }
        }

        @Override
        public void onSwitchProgram(boolean nextOrPre) {
            if (isDialogShow()) {
                return;
            }
            if (programList.size() == 1 && isOffLine()) {
                ToastUtils.getInstance().showShortToast(getString(R.string.video_now_playing));
                return;
            }
            if (!NetUtils.isNetAvailable(IfengApplication.getInstance()) && !isOffLine()) {
                mVideoView.stopPlayback();
                hideController();
                updateErrorPauseLayer(true);
                return;
            }
            setIntent(new Intent());
            resetAllPlayerPostion();
            mVideoView.stopPlayback();
            if (nextOrPre) {
                autoSwitchNextVideo();
            } else {
                autoSwitchPreVideo();
            }

        }
    };

    private long firstTme = 0;
    private final int DOUBLE_CLICK_MAX_TIME = 1500;

    private boolean isDoubleClick() {
        if (!PhoneConfig.ua_for_video.contains("huawei")) {
            return false;
        }
        if (firstTme <= 0) {
            firstTme = System.currentTimeMillis();
            return false;
        } else {
            long endTime = System.currentTimeMillis();
            if (endTime - firstTme < DOUBLE_CLICK_MAX_TIME) {
                return true;
            } else {
                firstTme = endTime;
                return false;
            }
        }
    }

    private boolean isKeyDown = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isKeyDown = true;
        }
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            if (isAdSilent) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (isDoubleClick() || !isActive || !isKeyDown) {
                    return true;
                }
                isKeyDown = false;
                if (!isOffLine()) {
                    if (isLandScape()) {
                        toPortrait();
                        return true;
                    }

                    finish();
                    return true;
                } else {
                    finish();
                    return true;
                }
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        volumeHandler.sendEmptyMessage(0);
                    }
                }.start();
                return super.onKeyUp(keyCode, event);
            default:
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    private final Handler volumeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            updateVolumeByKeyEvent();
        }
    };

    private void updateVolumeByKeyEvent() {
        int tempVolume = getCurrentVolume();
        logger.debug("volume  updateVolumeByKeyEvent {} ,mCurVolume = {} ", tempVolume, mCurVolume);

    }

    public void onBottomCollectClick() {
        if (mBottomCollect.getTag() == null) {
        }
    }

    private final Handler streamHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            hideStreamSwitchToast();
        }
    };

    public void showStreamSwitchToast() {
        if (!isLandScape() || mVideoView.isSeeking || currentPlayStream == MediaConstants.STREAM_LOW) {
            return;
        }
        streamHandler.removeMessages(0);
        mStreamSwitchToast.setVisibility(View.VISIBLE);
        streamHandler.sendEmptyMessageDelayed(0, STREAM_SWITCH_TOAST_DELAY_TIME);
    }

    @Override
    public void hideStreamSwitchToast() {
        mStreamSwitchToast.setVisibility(View.GONE);
    }

    protected void saveFavorite(PlayerInfoModel playerInfoModel, FavoritesModel favoritesModel, String guid) {
        favoritesModel.setProgramGuid(guid);
        favoritesModel.setImgUrl(playerInfoModel.stage_url_photo);
        favoritesModel.setName(playerInfoModel.getName());
        favoritesModel.setDesc(DateUtils.getTimeStr(playerInfoModel.getDuration()));
        favoritesModel.setType(playerInfoModel.getType());
        favoritesModel.setExtra1(getFromParamForVideoAd());
        favoritesModel.setColumnName(playerInfoModel.getColumnName());
        String playTime = playerInfoModel.getPlayTime();
        favoritesModel.setPlayTime(playTime);
        setTopicForCollect(favoritesModel);
        favoritesModel.setResource(FavoritesModel.RESOURCE_VIDEO);
        favoritesDAO.saveFavoritesData(favoritesModel);
    }

    void updateFavorite(FavoritesModel favoritesModel, PlayerInfoModel playerInfoModel) {
        logger.debug("updateFavorite ---favoritesModel = {}", favoritesModel);
        logger.debug("updateFavorite ---playerInfoModel = {}", playerInfoModel);
        if (favoritesModel == null || playerInfoModel == null) {
            return;
        }
        try {
            saveFavorite(playerInfoModel, favoritesModel, playerInfoModel.guid != null ? playerInfoModel.guid : "");
        } catch (Exception e) {
            logger.error("updateFavorite Exception ! {}", e);
        }
    }

    void setTopicForCollect(FavoritesModel favoritesModel) {
    }


    /**
     * 设置底部栏收藏按钮
     */
    void setBottomCollectState(boolean isCollected) {
        if (!isOffLine()) {
            mBottomCollect.setTag(isCollected);
            if (isCollected) {
                mBottomCollect.setImageResource(R.drawable.video_player_bottom_collect_selected);
            } else {
                mBottomCollect.setImageResource(R.drawable.video_player_bottom_collect);
            }
        }
    }

    public void showDLNADialog(final PlayerInfoModel model) {
        if (model == null) {
            return;
        }
        final String[] devices = mDLNADeviceManager.getDevicesName();
        mDLNADeviceManager.showDialog(this, devices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = devices[which];
                mDLNADeviceManager.setRenderName(name);
                Intent intent = new Intent();
                List<PlayerInfoModel> playlist = new ArrayList<PlayerInfoModel>();
                if (isOffLine() || isTopicVideo()) {
                    intent.setClass(ActivityVideoPlayer.this, DLNAPlayerActivity.class);
                    int playIndex = curProgramIndex;
                    for (int i = 0; i < programList.size(); i++) {
                        if (programList.get(i) != null && JsonUtils.checkDataInJSONObject(programList.get(i).getAvailableHighVideoURL())) {
                            playlist.add(programList.get(i));
                        } else {
                            if (i < playIndex) {
                                playIndex--;
                            }
                        }
                    }
                    mDLNADeviceManager.playIndex = playIndex;
//                    CustomerStatistics.sendDlnaRecord();
                } else {
                    intent.setClass(ActivityVideoPlayer.this, DLNAPlayerActivityForVideo.class);
                    intent.putExtra(IntentKey.LIST_TAG, currentPlayingTag);
                    switch (currentPlayingTag) {
                        case related:
                            intent.putExtra(IntentKey.DATA_INFO, mGuidRelativeVideoListInfo);
                            break;
                        case hotspot:
                            intent.putExtra(IntentKey.DATA_INFO, mHotListVideoInfo);
                            break;
                        case columnplayed:
                            intent.putExtra(IntentKey.DATA_INFO, mSubColumnVideoListInfo);
                            break;
                        case ranking:
                            intent.putExtra(IntentKey.DATA_INFO, mRankListVideoInfo);
                            break;
                        default:
                            break;
                    }
                    playlist.add(model);
                    mDLNADeviceManager.playIndex = 0;
//                    CustomerStatistics.sendDlnaRecord();
                }
                mDLNADeviceManager.playList = playlist;
                mDLNADeviceManager.activity = ActivityVideoPlayer.this;
                startActivityForResult(intent, 0);
            }
        });
    }

    boolean isShareShow = false;
    private boolean isClickSinaWeibo = false;


    public Context getContext() {
        return this;
    }

    private String getParam(String typeNum, String type, int playCount, PlayerInfoModel model, String mColumn, String echId, String topicId) {
        if (model == null) {
            return "";
        }
        String province = preferences.getString("province", "");
        String city = preferences.getString("city", "");
        StringBuilder builder = new StringBuilder();
        builder.append(typeNum);
        builder.append("&c=").append(System.currentTimeMillis());
        builder.append("&FID=").append(PhoneConfig.UID);
        builder.append("&CUSTOM=");
        StringBuilder paramBuilder = new StringBuilder();
        try {
            paramBuilder.append("mFrom=").append(URLEncoderUtils.encodeInUTF8(type));
            paramBuilder.append("&OpenFirst=").append(URLEncoderUtils.encodeInUTF8(mOpenFirst))
                    .append("&mCount=").append(playCount);
            String vid = model.getGuid() == null ? "" : model.getGuid();
            paramBuilder.append("&mVid=").append(URLEncoderUtils.encodeInUTF8(vid));
            paramBuilder.append("&mDuration=").append(model.getDuration());
            paramBuilder.append("&mColumn=").append(URLEncoderUtils.encodeInUTF8(mColumn))
                    .append("&mChannel=").append(URLEncoderUtils.encodeInUTF8(echId))
                    .append("&mSubject=").append(URLEncoderUtils.encodeInUTF8(topicId));
            String keyword = model.getName() == null ? "" : model.getName();
            paramBuilder.append("&mKeyword=").append(URLEncoderUtils.encodeInUTF8(keyword));
            paramBuilder.append("&mProv=").append(URLEncoderUtils.encodeInUTF8(province))
                    .append("&mCity=").append(URLEncoderUtils.encodeInUTF8(city));
            builder.append(URLEncoderUtils.encodeInUTF8(paramBuilder.toString()));
        } catch (UnsupportedEncodingException e) {
            builder.append(paramBuilder.toString());
        }
        logger.debug("paramBuilder ===={}", paramBuilder.toString());
        logger.debug("paramBuilder mCount======={}", playCount);
        return builder.toString();
    }

    private final Handler adTimeHandle = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            long pos;
            switch (msg.what) {
                case AD_TIME_COUNTDOWN:
                    pos = setAdCountDown();
                    sendAdTime(pos, false);
                    break;
            }

        }
    };

    /**
     * 播放广告中，通过不断发送message来进行倒计时计算刷新
     *
     * @param pos
     */
    private void sendAdTime(long pos, boolean isFirst) {

    }


    /**
     * 设置广告刷新 同时返回播放Position
     */
    private long setAdCountDown() {
        if (mVideoView == null) {
            return 0;
        }
        long position = mVideoView.getCurrentPosition();
        int time = getCountTime(position);

        if (time <= 0) {
            return 0;
        }

//        if (tvCountDown != null && position != 0) {
//            tvCountDown.setVisibility(View.VISIBLE);
//            tvCountDown.setText(time + "");
//        }

        return position;
    }

    /**
     * 倒计时时间算法
     *
     * @param position
     * @return
     */
    private int getCountTime(long position) {
        int totalTime = 0;
        for (VideoAdInfoModel adModel : mCurCPModelList) {
            if ("sponsorhead".equalsIgnoreCase(adModel.getPos())) {
                continue;
            }

            if (adModel.getLength() != null) {
                totalTime += Integer.valueOf(adModel.getLength());
            } else {
                logger.error("时长为空！！！！！！！！！！！：：：：：：");
            }
        }

        int playTime = 0;
        for (int i = 0; i < mCurPlayingCPProgramIndex; i++) {
            playTime += Integer.valueOf(mCurCPModelList.get(i).getLength());
        }
        playTime += generateTime(position);

        return totalTime - playTime;
    }

    private int generateTime(long position) {
        int totalSeconds = (int) (position / 1000);
        return totalSeconds % 60;
    }

    /**
     * 播放音频的服务用广播来通知UI改变
     */
    private void registerPushReceiver(String receiverName) {
        if (notifyUiReceiver == null) {
            notifyUiReceiver = new NotifyBroadcastReceiver();
            IntentFilter filter = new IntentFilter(receiverName);
            filter.addAction(IntentKey.ACTION_INIT_CONTROLL_AUDIO_SERVICE);
            filter.addAction(IntentKey.ACTION_AUDIO_PREPARING);
            filter.addAction(IntentKey.ACTION_AUDIO_PLAYING);
            filter.addAction(IntentKey.ACTION_AUDIO_PAUSE);
            filter.addAction(IntentKey.ACTION_AUDIO_STATE_ERROR);
            filter.addAction(IntentKey.ACTION_OVERFLOW_AUDIO_SERVICE);
            filter.addAction(IntentKey.ACTION_CHANGE_3GNET_SERVICE);
            LocalBroadcastManager.getInstance(this).registerReceiver(notifyUiReceiver, filter);
        }
        broadcastPriority = (int) System.currentTimeMillis();
    }

    private void unRegisterPushReceiver() {
        if (notifyUiReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(notifyUiReceiver);
            notifyUiReceiver = null;
        }
    }

    /**
     * 消息接受类
     */
    private NotifyBroadcastReceiver notifyUiReceiver;


    private static class SendAdImpSuccessListener implements Response.Listener {
        private final String impression;

        public SendAdImpSuccessListener(String impression) {
            this.impression = impression;
        }

        @Override
        public void onResponse(Object response) {
            logger.debug("VideoAdInfoModel impression is success! {}", impression);
        }
    }

    private static class SendAdImpErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if (volleyError != null && volleyError.getMessage() != null) {
                logger.error("failed to get VideoAdInfoModel {}", volleyError.getMessage());
            }
        }
    }

    /**
     * 类描述： 用于消息推送广播的接收 创建人： 揭耀祖 创建时间： 2013-10-28
     */
    private class NotifyBroadcastReceiver extends BroadcastReceiver {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onReceive(Context context, Intent intent) {
            //开始的时候做个一一对应的判断，因为有可能同时存在两个播放activity
            if (intent == null) {
                return;
            }
            if (intent.getIntExtra(IntentKey.ONE_TO_ONE_BROADCAST, 0) != broadcastPriority) {
                return;
            }
            if (IntentKey.ACTION_NOTIFY_UI_CHANGE.equals(intent.getAction())) {
                ACTION_NOTIFY_UI_CHANGE(intent);
            } else if (IntentKey.ACTION_CHANGE_3GNET_SERVICE.equals(intent.getAction())) {
                ACTION_CHANGE_3GNET_SERVICE(intent);
            }
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void ACTION_NOTIFY_UI_CHANGE(Intent intent) {

        curProgramIndex = intent.getIntExtra(IntentKey.CURRENT_PARM_INDEX, 0);
        if (curProgramIndex != -1) {
            //高亮显示
            autoSwitchNextPosition(curProgramIndex);
        }
        //高亮显示
        setCurProgramIndex(curProgramIndex);
        updateDateAndViewByPlay();
    }

    protected void ACTION_CHANGE_3GNET_SERVICE(Intent intent) {
        updateMobileNetLayer(true);
    }

    private void onErroPlay() {
        if (!isOffLine()) {
            if (NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                updateErrorRetryLayer(true);
            } else {
                updateErrorPauseLayer(true);
            }
        }
    }

    void setCurProgramIndex(int index) {

    }

    private void playVodVideo(PlayerInfoModel currentProgram) {
        if (currentProgram == null) {
            return;
        }
        currProGUID = currentProgram.getGuid();
    }

    public void autoSwitchNextPosition(int position) {
    }

    public class MyPauseAdPopWindow extends com.ifeng.newvideo.ui.ad.PauseAdPopWindow {
        public MyPauseAdPopWindow() {
            super(ActivityVideoPlayer.this);
        }

        public boolean onTouchEvent(MotionEvent event) {
            return true;
        }
    }

    private CommentEditFragment commentEditFragment;


    public CommentFragment getCommentFragment() {
        return null;
    }

    /**
     * 跳到评论页面
     */
    public void gotoCommentFragment() {
    }


    private PlayState mCurrentPlayState;

    /**
     * 暂停播放
     */
    public void pausePlayback() {
        if (mVideoView != null) {
            if (mVideoView.isPauseState()) {
                mCurrentPlayState = PlayState.STATE_PAUSED;
            } else {
                mCurrentPlayState = PlayState.STATE_PLAYING;
                mVideoView.delayPause();
            }
        }
    }

    /**
     * 恢复播放
     */
    public void resumePlayBack() {
        if (mVideoView != null && (mCurrentPlayState == PlayState.STATE_PLAYING)) {
            mVideoView.setPausePlayState(PlayState.STATE_PLAYING);
            mVideoView.start();
            logger.debug("resumePlayBack  mVideoView.start()");
        }
    }
}

