package com.ifeng.newvideo.videoplayer.activity.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteItem;
import com.ifeng.newvideo.videoplayer.bean.TopicVotePoint;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteResultArray;
import com.ifeng.newvideo.videoplayer.dao.TopicVideoDao;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.ToastUtils;

import java.text.DecimalFormat;
import java.util.List;


public class TopicVoteView extends LinearLayout implements View.OnClickListener {

    private TextView vote_title, vote_content;
    private TextView red_title, vote_red_num, red_percentage;
    private TextView blue_title, vote_blue_num, blue_percentage;
    private ImageView iv_vote_red, iv_vote_blue;
    private LinearLayout ll_vote_percentage;
    private ProgressBar progressBar;

    private int red_num, blue_num, total_num;
    private TopicVotePoint topicVotePoint1;
    private TopicVotePoint topicVotePoint;


    public TopicVoteView(Context context) {
        this(context, null);
    }

    public TopicVoteView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TopicVoteView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.topic_play_vote_view_layout, this, true);

        vote_title = (TextView) this.findViewById(R.id.topic_vote_title);
        vote_content = (TextView) this.findViewById(R.id.topic_vote_content);
        red_title = (TextView) this.findViewById(R.id.vote_red_title);
        vote_red_num = (TextView) this.findViewById(R.id.vote_red_num);
        red_percentage = (TextView) this.findViewById(R.id.vote_red_percentage);
        blue_title = (TextView) this.findViewById(R.id.vote_blue_title);
        vote_blue_num = (TextView) this.findViewById(R.id.vote_blue_num);
        blue_percentage = (TextView) this.findViewById(R.id.vote_blue_percentage);
        iv_vote_red = (ImageView) this.findViewById(R.id.vote_red_iv);
        iv_vote_blue = (ImageView) this.findViewById(R.id.vote_blue_iv);
        ll_vote_percentage = (LinearLayout) this.findViewById(R.id.ll_vote_percentage);
        ll_vote_percentage.setVisibility(View.GONE);
        progressBar = (ProgressBar) this.findViewById(R.id.vote_progress);

    }

    public void updateVoteView(final TopicVoteItem voteItem, final List<TopicVoteResultArray> resultData) {
        if (null == voteItem) {
            return;
        }

        if (!TextUtils.isEmpty(voteItem.getData().getSurveyinfo().getTitle())) {
            vote_title.setText(voteItem.getData().getSurveyinfo().getTitle());
        } else {
            vote_title.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(voteItem.getData().getSurveyinfo().getLeadtxt())) {
            vote_content.setText(voteItem.getData().getSurveyinfo().getLeadtxt());
        } else {
            vote_content.setVisibility(View.GONE);
        }

        if (!ListUtils.isEmpty(resultData)) {
            final TopicVoteResultArray result = resultData.get(0);
            topicVotePoint = result.getOption().get(0);
            if (!TextUtils.isEmpty(topicVotePoint.getTitle())) {
                red_title.setText(topicVotePoint.getTitle());
                red_num = Integer.parseInt(topicVotePoint.getNum());
                vote_red_num.setText(topicVotePoint.getNum());
            } else {
                red_title.setVisibility(View.GONE);
                vote_red_num.setVisibility(View.GONE);
            }

            topicVotePoint1 = result.getOption().get(1);
            if (!TextUtils.isEmpty(topicVotePoint1.getTitle())) {
                blue_title.setText(topicVotePoint1.getTitle());
                blue_num = Integer.parseInt(topicVotePoint1.getNum());
                vote_blue_num.setText(topicVotePoint1.getNum());
            } else {
                blue_title.setVisibility(View.GONE);
            }

            iv_vote_red.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    ++red_num;
                    uploadTopicVoteResult(voteItem.getData().getSurveyinfo().getId(), topicVotePoint.getItemid(), true);
                    iv_vote_red.setClickable(false);
                }
            });
            iv_vote_blue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    ++blue_num;
                    uploadTopicVoteResult(voteItem.getData().getSurveyinfo().getId(), topicVotePoint1.getItemid(), false);
                    iv_vote_blue.setClickable(false);
                }
            });
        }

    }

    private void uploadTopicVoteResult(String id, String sur, final boolean isFromLeft) {
        TopicVideoDao.commitTopicVoteResult(id, "",
                sur, new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        try {
                            com.alibaba.fastjson.JSONObject result = (com.alibaba.fastjson.JSONObject) response;
                            if (result.getString("ifsuccess").equals("1")) {
                                ToastUtils.getInstance().showShortToast(result.getString("msg"));
                                updatePercentageInfo(isFromLeft);
                                ll_vote_percentage.setVisibility(View.VISIBLE);
                            } else {
                                ToastUtils.getInstance().showShortToast(result.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    }

    private void updatePercentageInfo(boolean isFromLeft) {
        total_num = red_num + blue_num;

        float redtmp = red_num / (float) total_num;
        float bluetmp = blue_num / (float) total_num;

        DecimalFormat df = new DecimalFormat("0.00");
        String redStr = String.valueOf((int) (Double.parseDouble(df.format(redtmp)) * 100));
        String blueStr = String.valueOf((int) (Double.parseDouble(df.format(bluetmp)) * 100));

        red_percentage.setText(redStr + "%");
        blue_percentage.setText(blueStr + "%");

        if (isFromLeft) {
            iv_vote_red.setVisibility(View.GONE);
            if (red_num >= 100000) {
                vote_red_num.setText("10万+");
            } else {
                vote_red_num.setText(red_num + "");
            }
            vote_red_num.setVisibility(View.VISIBLE);
        } else {
            iv_vote_blue.setVisibility(View.GONE);
            if (blue_num >= 100000) {
                vote_blue_num.setText("10万+");
            } else {
                vote_blue_num.setText(blue_num + "");
            }
            vote_blue_num.setVisibility(View.VISIBLE);
        }
        progressBar.setProgress(Integer.parseInt(redStr));
    }

    @Override
    public void onClick(View view) {
    }
}
