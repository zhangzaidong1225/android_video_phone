package com.ifeng.newvideo.videoplayer.activity;

import android.text.TextUtils;
import com.alibaba.fastjson.JSONArray;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.ADInfoDAO;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.db.model.VideoAdConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by fanshell on 2016/8/25.
 */
public class VideoRelativeAdvertManager {

    private static final Logger logger = LoggerFactory.getLogger(VideoRelativeAdvertManager.class);
    public static final String TAG = VideoRelativeAdvertManager.class.getName();
    private SharePreUtils mSharePreferenceUtils;
    private String adID;

    private  int position;
    public VideoRelativeAdvertManager() {
        mSharePreferenceUtils = SharePreUtils.getInstance();
    }

    private boolean checkValidate() {
        String videoInfo = mSharePreferenceUtils.getVideoRelativeAD();
        if (!TextUtils.isEmpty(videoInfo)) {
            Gson gson = new Gson();
            MainAdInfoModel mainAdInfoModel = gson.fromJson(videoInfo, MainAdInfoModel.class);
            long currentTime = System.currentTimeMillis();
            if (currentTime - mainAdInfoModel.getCreate_time() < mainAdInfoModel.getCacheTime() * 1000) {
                if (mOnVideoRelativeListener != null) {
                    int index = mSharePreferenceUtils.getVideoRelativeIndex();
                    index = (index + 1) % (mainAdInfoModel.getAdMaterials().size());
                    mSharePreferenceUtils.setVideoRelativeIndex(index);
                    mOnVideoRelativeListener.onVideoRelativeAdvertSuccess(mainAdInfoModel, index);
                    return true;
                }
            }
        }

        return false;
    }

    public void getRelativeAdInfo() {
        //check validate
        boolean isValidate = checkValidate();
        if (isValidate) {
            return;
        }

        String url = getTextBarRequestUrl();
        if (TextUtils.isEmpty(url)) {
            if (mOnVideoRelativeListener != null) {
                mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
            }
            return;
        }

        //得到广告
        ADInfoDAO.getBodyAd(url,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        handleResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
                    }
                }, TAG);

    }

    private void handleResponse(Object response) {
        if (response == null || TextUtils.isEmpty(response.toString())) {
            if (mOnVideoRelativeListener != null) {
                mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
            }
            return;
        }

        try {
            List<MainAdInfoModel> mainAdInfoModels = JSONArray.parseArray(response.toString(), MainAdInfoModel.class);
            if (ListUtils.isEmpty(mainAdInfoModels)) {
                if (mOnVideoRelativeListener != null) {
                    mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
                }
                return;
            }

            boolean isFound = false;
            MainAdInfoModel findModel = null;
            for (MainAdInfoModel adInfoModel : mainAdInfoModels) {
                MainAdInfoModel model = AdTools.filterVideoBannerADModel(adInfoModel);
                if (adID.trim().equals(model.getAdPositionId()) && AdTools.isValidAdModel(model)) {
                    model.setCreate_time(System.currentTimeMillis());//手动赋值用以判断是否过期
                    Gson gson = new Gson();
                    String json = gson.toJson(model);
                    mSharePreferenceUtils.setVideoRelativeAd(json);
                    mSharePreferenceUtils.setVideoRelativeIndex(0);
                    isFound = true;
                    findModel = model;
                    break;
                }
            }

            if (isFound) {
                mOnVideoRelativeListener.onVideoRelativeAdvertSuccess(findModel, 0);
            } else {
                mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
            }
        } catch (Exception e) {
            if (mOnVideoRelativeListener != null) {
                mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
            }
        }
    }

    private OnVideoRelativeAdListener mOnVideoRelativeListener;

    public void setOnVideoRelativeSuccess(OnVideoRelativeAdListener listener) {
        this.mOnVideoRelativeListener = listener;
    }


    private String getTextBarRequestUrl() {
        if (VideoAdConfigDao.adConfig == null ||
                (VideoAdConfigDao.adConfig.getInfoAD() == null) ||
                ListUtils.isEmpty(VideoAdConfigDao.adConfig.getInfoAD().getTextbar())) {
            return "";
        }
        String adHost = VideoAdConfigDao.adConfig.getInfoAD().getAdHost();
        return getUrl(adHost);
    }

    private String getMoreRequestUrl() {
        if (VideoAdConfigDao.adConfig == null || VideoAdConfigDao.adConfig.getInfoAD() == null) {
            return "";
        }
        VideoAdConfigModel.InfoADBean infoAD = VideoAdConfigDao.adConfig.getInfoAD();
        String adHost = infoAD.getAdMoreUrl();
        return getUrl(adHost);

    }

    private String getUrl(String host) {

        List<VideoAdConfigModel.InfoADBean.TextbarBean> textbarBeans =  VideoAdConfigDao.adConfig.getInfoAD().getTextbar();
        for (VideoAdConfigModel.InfoADBean.TextbarBean textbarBean : textbarBeans) {
            String  channelId = textbarBean.getChannelId();
            if("video".equalsIgnoreCase(channelId)){
                adID = textbarBean.getAdId();
                position = textbarBean.getIndex();
            }
        }

        if (TextUtils.isEmpty(adID)) {
            return "";
        }
        StringBuilder params = new StringBuilder();
        params.append(host);
        params.append("?adids=");
        params.append(adID);
        params.append(',');
        AdTools.appendParams(params, IfengApplication.getAppContext());
        return params.toString();
    }

    public interface OnVideoRelativeAdListener {
        void onVideoRelativeAdvertSuccess(MainAdInfoModel model, int index);

        void onVideoRelativeAdvertFailed();

        void onVideoRelativeAdvertMoreSuccess(MainAdInfoModel model);

        void onVideoRelativeAdvertMoreADFailed();
    }


    /**
     * 请求余量广告
     */
    public void requestADMore(MainAdInfoModel model, int index) {
        adID = model.getAdPositionId();
        String url = getMoreRequestUrl();
        if (TextUtils.isEmpty(url)) {
            if (mOnVideoRelativeListener != null) {
                mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
            }
            return;
        }

        ADInfoDAO.getBodyAd(url, new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        handleMoreResponse(response);
                    }
                },
                new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mOnVideoRelativeListener != null) {
                            mOnVideoRelativeListener.onVideoRelativeAdvertFailed();
                        }
                    }

                }, TAG);
    }

    private void handleMoreResponse(Object response) {
        if (response == null || TextUtils.isEmpty(response.toString())) {
            mOnVideoRelativeListener.onVideoRelativeAdvertMoreADFailed();
            return;
        }
        try {
            List<MainAdInfoModel> mainAdInfoModels = JSONArray.parseArray(response.toString(), MainAdInfoModel.class);
            for (MainAdInfoModel adInfoModel : mainAdInfoModels) {
                MainAdInfoModel model = AdTools.filterVideoBannerADModel(adInfoModel);
                if (AdTools.isValidAdModel(model) && !TextUtils.isEmpty(model.getAdPositionId())
                        && model.getAdPositionId().equals(adID)) {
                    mOnVideoRelativeListener.onVideoRelativeAdvertMoreSuccess(model);
                    break;
                }
            }
        } catch (Exception e) {
            mOnVideoRelativeListener.onVideoRelativeAdvertMoreADFailed();
        }
    }

}
