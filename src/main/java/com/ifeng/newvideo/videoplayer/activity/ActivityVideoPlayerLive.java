package com.ifeng.newvideo.videoplayer.activity;


import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.ui.live.adapter.TVLivePagerAdapter;
import com.ifeng.newvideo.ui.maintab.LiveData;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.widget.skin.TitleView;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.dao.LiveDao;
import com.ifeng.video.dao.db.model.live.TVLiveInfo;
import com.ifeng.video.dao.db.model.live.TVLiveListInfo;
import com.ifeng.video.dao.db.model.live.TVLiveProgramInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 卫视直播
 */
public class ActivityVideoPlayerLive extends BaseFragmentActivity implements NotifyShareCallback {

    private static final Logger logger = LoggerFactory.getLogger(ActivityVideoPlayerDetail.class);

    private RecyclerView recyclerView;
    private TVListAdapter tvListAdapter;

    private NormalVideoHelper mPlayerHelper;
    private VideoSkin mVideoSkin;
    private UIPlayContext mPlayContext;

    private OneKeyShare mOneKeyShare;
    private String channelName;//频道名称
    private LiveData liveData;//直播页需要的数据，包括channelId,echid,chid

    private TVLiveInfo tvLiveInfo;
    private int currentPlayPosition = 0;//当前正在播放的电视台位置，默认第一个

    private List<TVLiveListInfo.LiveInfoEntity> tvList = new ArrayList<>();//电视台列表
    private String currentChannelId = "";//当前正在播放的电视台id

    private PagerSlidingTabStrip slidingTabStrip;
    private ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(null);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tv_live_video_layout);
        initView();
        initSkin();
        initAdatper();
        mOneKeyShare = new OneKeyShare(this);
//        Bundle bundle = getIntent().getExtras();
        liveData = (LiveData) getIntent().getSerializableExtra("liveData");
        logger.debug("liveData={}", liveData.toString());

//        liveData.setChannelId( bundle.getString("channelId"));
        getTVLiveList(true);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        liveData = (LiveData) getIntent().getSerializableExtra("liveData");
        getTVLiveList(true);
    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_live);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        slidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pager_tab_container_live);
        slidingTabStrip.setShouldExpand(true);
        viewPager = (ViewPager) findViewById(R.id.view_pager_live);

    }

    private void initSkin() {
        mVideoSkin = (VideoSkin) findViewById(R.id.video_skin_live);
        mPlayContext = new UIPlayContext();
        mPlayContext.skinType = VideoSkin.SKIN_TYPE_TV;
        mPlayerHelper = new NormalVideoHelper();
        mPlayerHelper.init(mVideoSkin, mPlayContext);
        mVideoSkin.setNoNetWorkListener(new VideoSkin.OnNetWorkChangeListener() {
            @Override
            public void onNoNetWorkClick() {
                rePlayVideo();
            }

            @Override
            public void onMobileClick() {
                rePlayVideo();
            }
        });
        mVideoSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener() {
            @Override
            public void onLoadFailedListener() {
                rePlayVideo();
            }
        });

        if (null != mVideoSkin.getTitleView()) {
            mVideoSkin.getTitleView().setOnShareProgramLClickListener(new TitleView.OnShareProgramLClickListener() {
                @Override
                public void onShareClick() {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE, PageIdConstants.PAGE_LIVE);
                    showSharePop();
                }

                @Override
                public void onLiveProgramClick() {
//                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_EPG, PageIdConstants.PAGE_LIVE);
//                    dialogFragment = TVProgramDialogFragment.newInstance(tvLiveInfo.getChannelId());
//                    FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    dialogFragment.show(ft, "TVProgramDialog");
                }
            });
        }
    }

    private void initAdatper() {
        tvListAdapter = new TVListAdapter();
        recyclerView.setAdapter(tvListAdapter);
//        if (ListUtils.isEmpty(tvList)) {
//            tvList.clear();
//            tvList.addAll(getTVFakeData());
//        }
        tvListAdapter.setData(tvList);
        tvListAdapter.setOnTvClick(new TVListAdapter.OnTvClick() {
            @Override
            public void onTvClick(int position, TVLiveListInfo.LiveInfoEntity entity) {
                if (mPlayerHelper != null) {//切换电视台，重置当前是否是暂停状态，保证切换到其他电视台能正常播放而不是暂停状态
                    mPlayerHelper.setLeaveStateIsPause(false);
                }
                if (currentPlayPosition == position) {//如果选择的电视台跟当前播放的一致，返回
                    return;
                }
                mPlayContext.channelId = "";
                PlayQueue.getInstance().setEchid("");
                currentPlayPosition = position;//设置电视台位置
                tvListAdapter.setPlayPosition(position);//重新选择电视台后重置选中的状态
                getProgramList(entity, false);//数据获取成功取当前电视台节目单数据
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_TV_ITEM, PageIdConstants.PAGE_LIVE);
            }
        });

    }

    private void setProgramList() {
        TVLivePagerAdapter pagerAdapter = new TVLivePagerAdapter(this, getSupportFragmentManager(), currentChannelId);
        viewPager.setOffscreenPageLimit(3);//三天
        viewPager.setAdapter(pagerAdapter);
        slidingTabStrip.setViewPager(viewPager);
        pagerAdapter.notifyDataSetChanged();
        slidingTabStrip.notifyDataSetChanged();
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPlayerHelper != null) {
            mPlayerHelper.onResume();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mPlayerHelper != null) {
            mPlayerHelper.onConfigureChange(newConfig);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPlayerHelper != null) {
            mPlayerHelper.onPause();
        }
    }

    private void rePlayVideo() {
        if (tvLiveInfo == null) {
            getTVLiveList(false);
        } else {
            playVideo();
        }
    }

    private void getTVLiveList(final boolean isRefresh) {
        LiveDao.getTVLiveAddress(TVLiveListInfo.class,
                new Response.Listener<TVLiveListInfo>() {
                    @Override
                    public void onResponse(TVLiveListInfo response) {
                        if (response != null && !ListUtils.isEmpty(response.getLiveInfo())) {
                            tvList.clear();
                            tvList.addAll(response.getLiveInfo());
                            tvListAdapter.setData(tvList);
                            int size = tvList.size();
                            for (int i = 0; i < size; i++) {
                                TVLiveListInfo.LiveInfoEntity entity = tvList.get(i);
                                if (TextUtils.isEmpty(entity.getChannelId())) {
                                    continue;
                                }
                                // 拿传进来的channelId去接口中匹配channelId或者title
                                boolean isChannelIdExist = entity.getChannelId().toLowerCase().equalsIgnoreCase(liveData.getChannelId())
                                        || entity.getTitle().toLowerCase().equalsIgnoreCase(liveData.getChannelId());
                                if (isChannelIdExist) {
                                    currentPlayPosition = i;//找到对应的电视台索引，没找到就是当前正在播放的电视台索引
                                }
                                logger.debug("currentPlayPosition={}", currentPlayPosition);
                            }
                            if (currentPlayPosition >= size) {
                                currentPlayPosition = 0;
                            }
                            getProgramList(tvList.get(currentPlayPosition), liveData.isPlayVideo());//数据获取成功取当前电视台节目单数据
                            tvListAdapter.setPlayPosition(currentPlayPosition);
                        }
//                        else {
//                            if (ListUtils.isEmpty(tvList)) {
//                                tvList.addAll(getTVFakeData());
//                                tvListAdapter.setData(tvList);
//                                initTVLiveData(tvList.get(currentPlayPosition), liveData.isPlayVideo(), "");
//                                tvListAdapter.setPlayPosition(currentPlayPosition);
//                            }
//
//                        }
                        //默认报一次选中视频
                        if (!isRefresh) {
                            PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_TV_ITEM, PageIdConstants.PAGE_LIVE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //默认报一次选中视频
                        if (!isRefresh) {
                            PageActionTracker.clickBtn(ActionIdConstants.CLICK_LIVE_TV_ITEM, PageIdConstants.PAGE_LIVE);
                        }
                        if (ListUtils.isEmpty(tvList)) {
                            tvList.addAll(getTVFakeData());
                            tvListAdapter.setData(tvList);
                            initTVLiveData(tvList.get(currentPlayPosition), liveData.isPlayVideo(), "");
                            tvListAdapter.setPlayPosition(currentPlayPosition);
                        }

                    }
                }, false);
    }

    private void playVideo() {
        if (NetUtils.isMobile(IfengApplication.getAppContext()) && IfengApplication.mobileNetCanPlay) {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }
        mPlayContext.isFromPush = liveData != null && liveData.isFromPush();
        mPlayContext.title = channelName;
        mPlayContext.tvLiveInfo = tvLiveInfo;
        String url;
        if (null != mPlayerHelper && mPlayerHelper.isPlayAudio()) {//当前播音频
            url = tvLiveInfo.getAudio();//取音频地址
            PlayQueue.getInstance().setTVLiveInfo(tvLiveInfo);
        } else {//否则取视频地址
            url = StreamUtils.getMediaUrlForTV(tvLiveInfo);
        }
        if (null != mPlayerHelper) {
            mPlayerHelper.openVideo(url);
        }
        logger.debug("playVideo() url={}", url);
        mPlayContext.streamType = StreamUtils.getLiveStreamType();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoSkin != null) {
            mVideoSkin.setOnLoadFailedListener(null);
            mVideoSkin.setNoNetWorkListener(null);
        }

        if (mPlayerHelper != null) {
            mPlayerHelper.onDestroy();
            mPlayerHelper = null;
        }
    }

    private void initTVLiveData(TVLiveListInfo.LiveInfoEntity liveInfoEntity, boolean changeToVideo, String title) {
        tvLiveInfo = new TVLiveInfo();
        tvLiveInfo.setAudio(liveInfoEntity.getAudio());
        tvLiveInfo.setChannelId(liveInfoEntity.getChannelId());
        tvLiveInfo.setmUrl(liveInfoEntity.getMUrl());
        tvLiveInfo.setTitle(liveInfoEntity.getTitle());
        tvLiveInfo.setcName(liveInfoEntity.getCName());
        tvLiveInfo.setVideo(liveInfoEntity.getVideo());
        tvLiveInfo.setVideoInReview(liveInfoEntity.getVideoInReview());
        tvLiveInfo.setVideoH(liveInfoEntity.getVideoH());
        tvLiveInfo.setVideoM(liveInfoEntity.getVideoM());
        tvLiveInfo.setVideoL(liveInfoEntity.getVideoL());
        tvLiveInfo.setBigIconURL(liveInfoEntity.getBigIconURL());
        tvLiveInfo.setSmallIconURL(liveInfoEntity.getSmallIconURL());
        tvLiveInfo.setDescription(liveInfoEntity.getDescription());
        tvLiveInfo.setImg490_490(liveInfoEntity.getImg490_490());

        List<TVLiveInfo.Schedule> scheduleList = new ArrayList<>();
        TVLiveInfo.Schedule schedule = tvLiveInfo.new Schedule();
        schedule.setProgramTitle(title);
        scheduleList.add(schedule);
        tvLiveInfo.setSchedule(scheduleList);//添加当前正在播放的节目单

        channelName = tvLiveInfo.getcName();
//        mOneKeyShare.initShareStatisticsData(liveInfoEntity.getChannelId(), liveData.getEchid(), "", "", PageIdConstants.PLAY_LIVE_V);
        mOneKeyShare.initSmartShareData(UserOperatorConst.TYPE_LIVE, channelName, tvLiveInfo.getTitle());

        if (changeToVideo) {
            currentChannelId = "";
        }
        if (!currentChannelId.equals(liveInfoEntity.getChannelId())) {//channelId不同时重新播放，否则播放当前的，保证音频模式下会续播而不是重新加载播放
            currentChannelId = liveInfoEntity.getChannelId();
            setProgramList();
            playVideo();
        }
    }

    /**
     * 分享
     */
    private void showSharePop() {
        if (tvLiveInfo != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            String name = channelName;
            String image = tvLiveInfo.getBigIconURL();
            //精选首页进来，分享标题和图片为推荐位标题和图片
            if (!TextUtils.isEmpty(liveData.getShareTitle()) && !TextUtils.isEmpty(liveData.getShareImg())) {
                name = liveData.getShareTitle();
                image = liveData.getShareImg();
            }
            mOneKeyShare.shareLive(tvLiveInfo.getmUrl(), mVideoSkin, name, ActivityVideoPlayerLive.this, tvLiveInfo.getcName(), image, false);
        }
    }


    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageLive();
        } else {
            PageActionTracker.enterPage();
        }
    }


    static class TVListAdapter extends RecyclerView.Adapter<RecyclerHolder> {
        private List<TVLiveListInfo.LiveInfoEntity> list = new ArrayList<>();

        private int playPosition = 0;//哪个电视台正在播放，默认是第一个电视台

        public void setPlayPosition(int playPosition) {
            this.playPosition = playPosition;
            notifyDataSetChanged();
        }

        private TVListAdapter.OnTvClick onTvClick;

        public interface OnTvClick {
            /**
             * 点击电视台回调
             *
             * @param position 点击的位置
             * @param entity   点击的电视台实体
             */
            void onTvClick(int position, TVLiveListInfo.LiveInfoEntity entity);
        }

        public void setOnTvClick(TVListAdapter.OnTvClick onTvClick) {
            this.onTvClick = onTvClick;
        }

        public void setData(List<TVLiveListInfo.LiveInfoEntity> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_tv_live_item_layout, viewGroup, false);
            RecyclerHolder holder = new RecyclerHolder(view);
            holder.rootView = view.findViewById(R.id.view_parent);
            holder.tvName = (TextView) view.findViewById(R.id.tv_name);
            holder.tvShape = (ImageView) view.findViewById(R.id.tv_shape);
            holder.tvImg = (NetworkImageView) view.findViewById(R.id.tv_icon);
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ClickUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (onTvClick != null) {
                        int position = (int) view.getTag(R.id.view_parent);
                        onTvClick.onTvClick(position, list.get(position));
                    }
                }
            });

            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerHolder recyclerHolder, int i) {
            TVLiveListInfo.LiveInfoEntity entity = list.get(i);
            if (entity != null) {
                recyclerHolder.tvName.setText(entity.getCName());
                recyclerHolder.tvImg.setImageUrl(entity.getImg490_490(), VolleyHelper.getImageLoader());
                if (i == playPosition) {
                    recyclerHolder.tvShape.setImageResource(R.drawable.icon_live_shape);
                } else {
                    recyclerHolder.tvShape.setImageResource(R.drawable.shape_tv_no_play);
                }
                recyclerHolder.tvImg.setDefaultImageResId(R.drawable.icon_default_tv_live);
                recyclerHolder.tvImg.setErrorImageResId(R.drawable.icon_default_tv_live);
            }
            recyclerHolder.rootView.setTag(R.id.view_parent, i);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    static class RecyclerHolder extends RecyclerView.ViewHolder {

        private RecyclerHolder(View itemView) {
            super(itemView);
        }

        private View rootView;//
        private TextView tvName;//电视台名称
        private NetworkImageView tvImg;//电视台图标
        private ImageView tvShape;//电视台图标边框
    }

    private List<TVLiveListInfo.LiveInfoEntity> getTVFakeData() {
        List<TVLiveListInfo.LiveInfoEntity> list = new ArrayList<>();
        TVLiveListInfo.LiveInfoEntity ziXun = new TVLiveListInfo.LiveInfoEntity();
        ziXun.setChannelId("4AC51C17-9FBE-47F2-8EE0-8285A66EAFF5");
        ziXun.setTitle("info");
        ziXun.setCName("资讯台");
        ziXun.setVideo("http://zv.3gv.ifeng.com/live/zixun.m3u8");
        ziXun.setAudio("http://zv.3gv.ifeng.com/live/zixun64kaudio.m3u8");
        ziXun.setImg490_490("http://y1.ifengimg.com/a/2016_16/94eb9bbac81aa7d.png");

        TVLiveListInfo.LiveInfoEntity zhongWen = new TVLiveListInfo.LiveInfoEntity();
        zhongWen.setChannelId("270DE943-3CDF-45E1-8445-9403F93E80C4");
        ziXun.setTitle("chinese");
        zhongWen.setCName("中文台");
        zhongWen.setVideo("http://zv.3gv.ifeng.com/live/zhongwen.m3u8");
        zhongWen.setAudio("http://zv.3gv.ifeng.com/live/zhongwen64kaudio.m3u8");
        zhongWen.setImg490_490("http://y2.ifengimg.com/a/2016_16/3c47ac7abc05e8b.png");

        TVLiveListInfo.LiveInfoEntity hongKong = new TVLiveListInfo.LiveInfoEntity();
        hongKong.setChannelId("2c942450-2165-4750-80de-7dff9c224153");
        ziXun.setTitle("hongkong");
        hongKong.setCName("香港台");
        hongKong.setVideo("http://zv.3gv.ifeng.com/live/hongkong.m3u8");
        hongKong.setAudio("http://zv.3gv.ifeng.com/live/hongkong64kaudio.m3u8");
        hongKong.setImg490_490("http://y0.ifengimg.com/a/2016_16/1711140b6b2491f.png");

        TVLiveListInfo.LiveInfoEntity tianJin = new TVLiveListInfo.LiveInfoEntity();
        tianJin.setChannelId("35383695-26c3-4ce5-b535-0001abce11e4");
        ziXun.setTitle("tjws");
        tianJin.setCName("天津卫视");
        tianJin.setVideo("http://zv.3gv.ifeng.com/live/CQWS.m3u8");
        tianJin.setAudio("http://zv.3gv.ifeng.com/live/166.m3u8");
        tianJin.setImg490_490("http://y2.ifengimg.com/a/2016_16/18d2325f353f669.png");

        list.add(ziXun);
        list.add(zhongWen);
        list.add(hongKong);
        list.add(tianJin);
        return list;
    }

    public void switchAudioVideoController(boolean shouldPlay) {
        if (mPlayerHelper != null) {
            if (shouldPlay) {
                mPlayerHelper.doVideoAudioChange();
            } else {
                mPlayerHelper.switchToVideoControllerAndUI();
            }
        }
    }

    private void getProgramList(final TVLiveListInfo.LiveInfoEntity entity, final boolean changeToVideo) {
        LiveDao.getTVLiveProgramInfo(entity.getChannelId(), 1, TVLiveProgramInfo.class,
                new Response.Listener<TVLiveProgramInfo>() {
                    @Override
                    public void onResponse(TVLiveProgramInfo response) {
                        if (response == null || ListUtils.isEmpty(response.getSchedule())) {
                            initTVLiveData(entity, changeToVideo, "");
                        } else {
                            Date date = DateUtils.getDateFormat(System.currentTimeMillis());
                            int focusPosition = LiveUtils.handleTVLiveState(response, date);
                            TVLiveProgramInfo.ScheduleEntity scheduleEntity = response.getSchedule().get(focusPosition);
                            initTVLiveData(entity, changeToVideo, scheduleEntity.getProgramTitle());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        initTVLiveData(entity, changeToVideo, "");
                    }
                }, false);
    }


}
