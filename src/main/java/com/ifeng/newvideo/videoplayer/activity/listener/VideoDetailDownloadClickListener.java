package com.ifeng.newvideo.videoplayer.activity.listener;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ifeng.newvideo.BuildConfig;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.cache.NetDealListener;
import com.ifeng.newvideo.cache.NetDealManager;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.player.PlayUrlAuthUtils;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by fanshell on 2016/7/31.
 * 下载的单击事件
 */
public class VideoDetailDownloadClickListener implements View.OnClickListener {
    private static Logger logger = LoggerFactory.getLogger(VideoDetailDownloadClickListener.class);

    private final NetDealManager netDealManager;
    private Context context;
    private String resourceType;//资源类型，视频或音频：IfengType.TYPE_VIDEO、IfengType.TYPE_AUDIO

    public VideoDetailDownloadClickListener(Context context, String resourceType) {
        netDealManager = new NetDealManager(context);
        this.context = context;
        this.resourceType = resourceType;
    }

    @Override
    public void onClick(final View view) {
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }
        if (!NetUtils.isNetAvailable(view.getContext())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        final VideoItem videoItem = (VideoItem) view.getTag();
        if (videoItem == null) {
            if (BuildConfig.DEBUG) {
                ToastUtils.getInstance().showShortToast("数据有点小问题");
            }
            return;
        }

        String weMediaName = videoItem.weMedia == null ? "" : videoItem.weMedia.name;
        SendSmartStatisticUtils.sendDownloadOperatorStatistics(view.getContext(), weMediaName, videoItem.title);
        if (resourceType.equals(IfengType.TYPE_AUDIO)) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_CACHE, PageIdConstants.PLAY_FM_V);
        } else {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_CACHE, PageIdConstants.PLAY_VIDEO_V);
        }
        netDealManager.dealNetTypeWithSetting(new NetDealListener() {
            @Override
            public void onDealByState(int state) {
                handleToDownload(state, videoItem, (ViewGroup) view);
            }
        });
    }

    private void handleToDownload(int state, VideoItem videoItem, ViewGroup view) {
        if (state == NetDealListener.NO_ACTION) {
            return;
        }
        PlayerInfoModel model = new PlayerInfoModel();
        model.setName(videoItem.title);
        int duration = videoItem.duration;
        List<FileType> videoFiles = videoItem.videoFiles;
        String mediaOriginalUrl = getFileUrl(videoFiles, String.valueOf(MediaConstants.MEDIA_ORIGINAL));
        String mediaSupperUrl = getFileUrl(videoFiles, String.valueOf(MediaConstants.MEDIA_SUPPER));
        String mediaHighUrl = getFileUrl(videoFiles, String.valueOf(MediaConstants.MEDIA_HIGH));
        String mediaMiddleUrl = getFileUrl(videoFiles, String.valueOf(MediaConstants.MEDIA_MIDDLE));
        String mediaLowUrl = getFileUrl(videoFiles, String.valueOf(MediaConstants.MEDIA_LOW));

        int fileOriginalSize = getFileSize(videoFiles, String.valueOf(MediaConstants.MEDIA_ORIGINAL));
        int fileSupperSize = getFileSize(videoFiles, String.valueOf(MediaConstants.MEDIA_SUPPER));
        int fileHighSize = getFileSize(videoFiles, String.valueOf(MediaConstants.MEDIA_HIGH));
        int fileMiddleSize = getFileSize(videoFiles, String.valueOf(MediaConstants.MEDIA_MIDDLE));
        int mediaLowSize = getFileSize(videoFiles, String.valueOf(MediaConstants.MEDIA_LOW));

        String shareUrl = videoItem.mUrl;
        String newShareUrl = videoItem.mUrl;
        String createDate = videoItem.createDate;
        String mediaAudioUrl = null;
        if (resourceType.equals(IfengType.TYPE_AUDIO)) {
            mediaAudioUrl = videoItem.mUrl;
        }
        String id = videoItem.itemId;
        String cpName = videoItem.cpName;
        String searchPath = videoItem.searchPath;
        model = new PlayerInfoModel(videoItem.title,
                videoItem.image,
                videoItem.guid,
                "",
                mediaOriginalUrl,
                mediaSupperUrl,
                mediaHighUrl,
                mediaMiddleUrl,
                mediaLowUrl,
                duration,
                fileOriginalSize,
                fileSupperSize,
                fileHighSize,
                fileMiddleSize,
                mediaLowSize,
                shareUrl,
                newShareUrl,
                createDate,
                resourceType,
                mediaAudioUrl,
                id,
                cpName,
                searchPath
        );
        model.setWmId(videoItem.weMedia == null ? "" : videoItem.weMedia.id);
        model.setWmName(videoItem.weMedia == null ? "" : videoItem.weMedia.name);
        model.setColumnName(videoItem.itemId);//FM的programId
        if (!ListUtils.isEmpty(videoFiles)) {
            FileType downLoadFileType;
            if (resourceType.equals(IfengType.TYPE_AUDIO)) {
                downLoadFileType = StreamUtils.getAudioDownLoadFileType(videoFiles);
            } else {
                downLoadFileType = StreamUtils.getMediaDownLoadFileType(videoFiles);
            }
            String fileUrl = PlayUrlAuthUtils.getVideoAuthUrl(downLoadFileType.mediaUrl, videoItem.guid);
            Log.d("cachePlayerUrl", fileUrl);
            long fileSize = downLoadFileType.filesize;
            model.setMedia_cache_url(fileUrl);
            model.setVideo_size(fileSize);//设置视频大小
            logger.debug("fileUrl={},fileSize={}", fileUrl, fileSize);
            CacheManager.addDownload(context, model, state, MediaConstants.STREAM_HIGH, resourceType, null);
            if (view.getChildCount() > 0 && view.getChildAt(0) != null) {
                if (view.getChildAt(0) instanceof ImageView) {
                    ((ImageView) view.getChildAt(0)).setImageResource(R.drawable.video_playerbtn_audio_p);
                }
            }
            view.setEnabled(false);
        } else {
            ToastUtils.getInstance().showShortToast(R.string.cache_url_error);
        }
    }

    private int getIndex(List<FileType> fileTypes, String type) {
        int position = -1;
        if (ListUtils.isEmpty(fileTypes)) {
            return -1;
        }
        int len = fileTypes.size();
        for (int i = 0; i < len; i++) {
            if (type.equals(String.valueOf(fileTypes.get(i)).trim())) {
                position = i;
                break;
            }
        }

        return position;
    }

    private int getFileSize(List<FileType> videoFiles, String type) {
        int position = getIndex(videoFiles, type);
        if (position >= 0 && position < videoFiles.size()) {
            return videoFiles.get(position).filesize;
        }
        return 0;
    }

    private String getFileUrl(List<FileType> videoFiles, String type) {
        int position = getIndex(videoFiles, type);
        if (position >= 0 && position < videoFiles.size()) {
            return videoFiles.get(position).mediaUrl;
        }
        return "";
    }
}
