package com.ifeng.newvideo.videoplayer.activity.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.dao.db.dao.WeMediaDao;


public class WeMediaHotNewView extends LinearLayout implements View.OnClickListener {

    private Context mContext;
    private String weMediaId = "";
    private RelativeLayout ll_new;//最新、最热布局
    private RelativeLayout ll_hot;//最新、最热布局
    private TextView tv_new;//最新
    private TextView tv_hot;//最热
    private String type = WeMediaDao.WE_MEDIA_TYPE_NEW;//默认请求最新的数据
    private View redView;
    private int mWidth;

    private WeMediaHomePageActivity mActivity;

    public WeMediaHotNewView(Context context) {
        this(context, null);
    }

    public WeMediaHotNewView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeMediaHotNewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LayoutInflater.from(context).inflate(R.layout.wemedia_head_viewpager_layout, this, true);
        initView();
        mWidth = DisplayUtils.getWindowWidth();
    }

    public void setMainActivity(WeMediaHomePageActivity activity) {
        if (activity instanceof WeMediaHomePageActivity) {
            this.mActivity = activity;
        }
    }

    public void setWeMediaId(String weMediaId) {
        this.weMediaId = weMediaId;
    }

    public void setType(String type) {
        this.type = type;
        if (type.equals(WeMediaDao.WE_MEDIA_TYPE_NEW)) {
            tv_hot.setTextColor(getResources().getColor(R.color.black));
            tv_new.setTextColor(getResources().getColor(R.color.subscribe_text_color));
            startLineMoveLeft();
        } else {
            tv_hot.setTextColor(getResources().getColor(R.color.subscribe_text_color));
            tv_new.setTextColor(getResources().getColor(R.color.black));
            startLineMoveRight();
        }
    }


    private void initView() {
        ll_new = (RelativeLayout) findViewById(R.id.ll_new);
        tv_new = (TextView) findViewById(R.id.tv_new);
        tv_new.setOnClickListener(this);
        ll_new.setOnClickListener(this);

        tv_hot = (TextView) findViewById(R.id.tv_hot);
        ll_hot = (RelativeLayout) findViewById(R.id.ll_hot);
        tv_hot.setOnClickListener(this);
        ll_hot.setOnClickListener(this);

        redView = findViewById(R.id.ll_red_line);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_new:
            case R.id.ll_new:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA_NEW, PageIdConstants.WEMEDIA_HOME);
                type = WeMediaDao.WE_MEDIA_TYPE_NEW;
                mActivity.getWeMediaInfoView(weMediaId, type, true);
                tv_hot.setTextColor(getResources().getColor(R.color.black));
                tv_new.setTextColor(getResources().getColor(R.color.subscribe_text_color));
                startLineMoveLeft();
                break;
            case R.id.tv_hot:
            case R.id.ll_hot:
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA_HOT, PageIdConstants.WEMEDIA_HOME);
                type = WeMediaDao.WE_MEDIA_TYPE_HOT;
                mActivity.getWeMediaInfoView(weMediaId, type, false);
                tv_hot.setTextColor(getResources().getColor(R.color.subscribe_text_color));
                tv_new.setTextColor(getResources().getColor(R.color.black));
                startLineMoveRight();
                break;
        }
    }

    private void startLineMoveRight() {
        redView.clearAnimation();
        ValueAnimator mAnimator = null;

        mAnimator = ValueAnimator.ofFloat(0,  DisplayUtils.convertDipToPixel(80.0f));
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (redView != null) {
                    redView.bringToFront();
                    redView.setTranslationX((Float) valueAnimator.getAnimatedValue());
                    redView.invalidate();
                }
            }
        });
        mAnimator.setDuration(100);
        mAnimator.setRepeatCount(0);
        mAnimator.start();
    }

    private void startLineMoveLeft() {
        redView.clearAnimation();
        ValueAnimator mAnimator = null;

        mAnimator = ValueAnimator.ofFloat(DisplayUtils.convertDipToPixel(80.0f), 0);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (redView != null) {
                    redView.bringToFront();
                    redView.setTranslationX((Float) valueAnimator.getAnimatedValue());
                    redView.invalidate();
                }
            }
        });
        mAnimator.setDuration(100);
        mAnimator.setRepeatCount(0);
        mAnimator.start();
    }


}
