package com.ifeng.newvideo.videoplayer.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.dlna.DLNADeviceManager;
import com.ifeng.newvideo.setting.entity.UserFeed;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.adapter.BottomLayoutInit;
import com.ifeng.newvideo.videoplayer.listener.VideoPlayerClickListener;
import com.ifeng.newvideo.videoplayer.listener.VideoPlayerVolumeSeekBarChangeListener;
import com.ifeng.newvideo.videoplayer.listener.VideoSensorEventListener;
import com.ifeng.newvideo.videoplayer.widget.IfengPortraitMediaController;
import com.ifeng.newvideo.videoplayer.widget.SeekBarVer;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.player.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by yuelong on 2014/8/26
 * 视频播放页面的父类，处理视频的播放逻辑
 */
public abstract class BaseVideoPlayerActivity extends BaseFragmentActivity implements StateListener,
        IfengMediaController.DoPauseResumeCallBack,
        IfengPortraitMediaController.OnHiddenListener,
        IfengPortraitMediaController.OnShownListener

{
    private static final Logger logger = LoggerFactory.getLogger(BaseVideoPlayerActivity.class);

    public IfengVideoView mVideoView;
    public IfengMediaController mVideoController;
    // 最大声音
    public int mMaxVolume;
    // 用于记录手势滑动时的音量
    public int mVolumeOnSlide = -1;
    //当前声音
    public int mCurVolume = 0;
    public float mPreBright = 0;
    //当前亮度
    public float mCurBright = -1f;

    /**
     * 当前缩放模式
     */
    public int mCurVideoLayout = IfengVideoView.VIDEO_LAYOUT_ZOOM;
    int playPosWhenSwitchAV = 0;
    int playAdPosWhenSwitchAV = 0;

    private static final int mPlayerPositionOffset = 1000;


    //二级页面标记

    public static final int RIGHT_LAYER_SHARE = 3002;//分享
    static final int RIGHT_LAYER_STREAM = 3003;//码流切换

    /**
     * 手势引导提示
     */
    private RelativeLayout gestureGuideLayout;
    private RelativeLayout gestureGuideLayoutLandscape;
    private RelativeLayout gestureGuideLayoutPortrait;

    /**
     * 视频播放器广告
     */
    private RelativeLayout advertLayout;
    private ViewGroup advertBackView;
    private ViewGroup advertMuteView;
    private View advertDetailView;
    private ViewGroup advertFullScreenView;

    /**
     * 标题栏
     */
    private ViewGroup landTitleLayout;
    private ViewGroup landBackView;
    private TextView landTitleView;
    private ViewGroup portraitLayout;
    private View portraitBackView;
    /**
     * 加载层
     */
    private ViewGroup loadingLayout;

    /**
     * 缓冲层
     */
    private ViewGroup bufferLayout;
    /**
     * 重试层
     */
    private ViewGroup retryLayout;
    private View retryView;
    /**
     * 没有网络的提示
     */
    private ViewGroup noNetWorkLayout;

    /**
     * 流量提示层
     */
    private ViewGroup mobileTipLayout;
    private View mobileContinueView;

    /**
     * 清晰度切换层
     */
    private ViewGroup streamSwitchLayout;
    private TextView streamView;


    /**
     * 全屏时声音操作
     */
    private ViewGroup rightVolumeLayout;
    private SeekBarVer rightVolumeSeekBar;


    private VideoPlayerClickListener clickListener;
    /**
     * 手势操作时屏幕中间的音量、亮度、进度等
     */


    public View mGestureRootView;

    public View mProgress_graph_fl;
    public View mProgress_text_ll;
    public TextView mCurTimeTv;
    public TextView mTotalTimeTv;


    private PlayState currentPlayState;

    //播放器全屏右侧二级页面

    LinearLayout mRightShareLayer;


    public boolean hasAudio = true;

    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener sensorListener;


    private final VideoPlayerVolumeSeekBarChangeListener mOnSeekBarVolumeChangeListener = new VideoPlayerVolumeSeekBarChangeListener(this);


    RelativeLayout mRightStreamLayer;


    public boolean hasClickStreamSelect = false;

    LinearLayout mStreamSwitchToast;

    //底部按钮是否可点（分享评论）
    public boolean canCLickBottomBtn = false;
    public boolean mobileNetShowing;


    public final List<PlayerInfoModel> programList = new ArrayList<PlayerInfoModel>();
    public IfengType.LayoutType currentLayoutType = IfengType.LayoutType.vod;
    public IfengType.ListTag currentPlayingTag;
    public IfengType.ListTag currentTag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        startDLNAService();
        super.onCreate(savedInstanceState);
        //initView();
//        initDateUtils();
//        volumeBusiness();
//        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopDLNAService();
    }

    /**
     * 设置控制栏是否可见
     *
     * @param canShow true可见 false不可见
     */
    public void setControllerShowHide(boolean canShow) {
        canCLickBottomBtn = canShow;

        //控制栏是否可见
        if (mVideoView != null) {
            mVideoView.setControllerVisibily(canShow);
        }
    }

    void initDateUtils() {
        sensorListener = new VideoSensorEventListener(this);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE); // get
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); // get
        mMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        sensorManager.registerListener(sensorListener,sensor,SensorManager.SENSOR_DELAY_NORMAL);
    }


    private final List<Boolean> isCanPlayOfflineAudioList = new ArrayList<Boolean>();


    public void getVideoOrAudioPosition() {

    }

    /**
     * 播放调用的方法
     */
    protected abstract void prepareToPlay();


    public void hideController() {
        if (mVideoController != null) {
            mVideoController.hide();
        }
    }

    public void showController() {
        if (mVideoController != null) {
            mVideoController.show();
        }
    }

    private void continueShowController() {
        if (mVideoController != null) {
            mVideoController.continueShow();

        }
    }

    void bindListener() {
        updateVideoMediaController();

    }

    /**
     * 更新视频controller状态
     */
    protected abstract void updateVideoMediaController();

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    private void handlePlayerPortrait() {
//        mTopPortraitBack.setVisibility(View.VISIBLE);
        DisplayUtils.setDisplayStatusBar(this, false);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);



        if (mVideoView != null) {
            mCurVideoLayout = IfengVideoView.VIDEO_LAYOUT_PORTRAIT;
            mVideoView.setVideoLayout(IfengVideoView.VIDEO_LAYOUT_PORTRAIT);
            updateVideoMediaController();
        }
        if (BottomLayoutInit.bottomLayout != null) {
            BottomLayoutInit.bottomLayout.showBottomLayout();
        }
    }

    private void handlePlayerLandscape() {
        hideCommentFragment();
        //转全屏时，正在播放隐藏back键，否则不隐藏
        if (mVideoView != null && mVideoView.isInPlaybackState()) {
//            mTopPortraitBack.setVisibility(View.GONE);
        }
        DisplayUtils.setDisplayStatusBar(this, true);


        if (mVideoView != null) {

            mCurVideoLayout = IfengVideoView.VIDEO_LAYOUT_FULL;
            mVideoView.setVideoLayout(IfengVideoView.VIDEO_LAYOUT_FULL);
            updateVideoMediaController();
        }
        if (BottomLayoutInit.bottomLayout != null) {
            BottomLayoutInit.bottomLayout.hideBottomLayout();
        }
    }

    public void hideCommentFragment() {
    }

    void updateDLNAView() {
    }

    public int getCurrentVolume() {
        return mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    private void initControllerShowState() {
        //shouldShowController = mVideoController != null && mVideoController.isShowing();
    }

    void hideStreamSwitchToast() {
    }


    /**
     * 展示二级界面
     */
    public void showLandRight(int layerType) {
        hideController();
        switch (layerType) {

            case RIGHT_LAYER_SHARE:
                if (mRightShareLayer != null && mRightShareLayer.getVisibility() == View.GONE) {
                    showAnimation(mRightShareLayer);
                }
                break;
            case RIGHT_LAYER_STREAM:
                if (mRightStreamLayer != null && mRightStreamLayer.getVisibility() == View.GONE) {
                    showAnimation(mRightStreamLayer);

                }
                break;
        }
    }


    /**
     * 隐藏二级界面
     */
    void hideLandRight() {

        if (mRightShareLayer != null && mRightShareLayer.getVisibility() == View.VISIBLE) {
            hideAnimation(mRightShareLayer);
        }
        if (mRightStreamLayer != null && mRightStreamLayer.getVisibility() == View.VISIBLE) {
            hideAnimation(mRightStreamLayer);
        }
    }

    /**
     * 隐藏二级页面动画
     */
    private void hideAnimation(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_slide_right_out));
        view.setVisibility(View.GONE);
    }

    /**
     * 显示二级页面动画
     */
    private void showAnimation(View view) {
        view.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.common_slide_left_in));
    }

    private void initView() {
        clickListener = new VideoPlayerClickListener(this);
        initGuideView();
        initAdvertView();
        initLoadingView();
        initTitleView();
        initBufferView();
        //initShareView();
        initErrorView();
        initRetryView();
        initMobileView();
        initSwitchStreamView();
        initRightVolumeView();
        mVideoView = (IfengVideoView) findViewById(R.id.player_videoview);

        mGestureRootView = findViewById(R.id.video_gesture_handle_lay);
        mVideoView.setStateListener(this);
        mVideoView.setVideoLayout(IfengVideoView.VIDEO_LAYOUT_PORTRAIT);


    }

    /**
     * 用于初始化首次使用的引导操作的提示层
     */
    private void initGuideView() {
        gestureGuideLayout = (RelativeLayout) findViewById(R.id.video_gesture_guide);
        gestureGuideLayoutLandscape = (RelativeLayout) findViewById(R.id.video_gesture_guide_land);
        gestureGuideLayoutPortrait = (RelativeLayout) findViewById(R.id.video_gesture_guide_portrait);
        gestureGuideLayout.setOnClickListener(clickListener);
    }

    /**
     * 更新引导提示层
     */
    public void updateGuideView() {
        gestureGuideLayout.setVisibility(View.GONE);
        gestureGuideLayoutLandscape.setVisibility(View.GONE);
        gestureGuideLayoutPortrait.setVisibility(View.GONE);
        if (SharePreUtils.getInstance().getVodPlayGestureState()) {
            if (isLandScape()) {
                gestureGuideLayoutLandscape.setVisibility(View.VISIBLE);
            } else {
                gestureGuideLayoutPortrait.setVisibility(View.VISIBLE);
            }
        }
    }


    /**
     * 用于初始化广告提示层
     */
    private void initAdvertView() {
        advertLayout = (RelativeLayout) findViewById(R.id.video_ad_image_layout);
        advertBackView = (ViewGroup) advertLayout.findViewById(R.id.iv_return_rl);
        advertMuteView = (ViewGroup) advertLayout.findViewById(R.id.video_ad_mute_rl);
        advertDetailView = advertLayout.findViewById(R.id.tv_getdetail);
        advertFullScreenView = (ViewGroup) advertLayout.findViewById(R.id.rl_fullscreen);
        advertBackView.setOnClickListener(clickListener);
        advertMuteView.setOnClickListener(clickListener);
        advertDetailView.setOnClickListener(clickListener);
        advertFullScreenView.setOnClickListener(clickListener);
    }

    private void showAdvert(boolean isShow) {
        advertLayout.setVisibility(isShow ? View.VISIBLE : View.GONE);
        if (isShow) {
            if (isLandScape()) {
                advertFullScreenView.setVisibility(View.GONE);
            } else {
                advertFullScreenView.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 用于初始化loading层
     */
    private void initLoadingView() {
        loadingLayout = (ViewGroup) findViewById(R.id.video_loading_layer);
        loadingLayout.setVisibility(View.VISIBLE);
    }


    private void showLoadingView() {
        loadingLayout.setVisibility(View.VISIBLE);
        hideBufferView();
        showAdvert(false);
        hideNoNetWork();
        hideRetryView();
        hideMobileView();
    }

    private void hideLoadingView() {
        loadingLayout.setVisibility(View.GONE);
    }

    /**
     * 用于初始化title层
     */
    private void initTitleView() {
        landTitleLayout = (ViewGroup) findViewById(R.id.video_top_title_land_layout);
        landBackView = (RelativeLayout) landTitleLayout.findViewById(R.id.video_landscape_title_back);
        landTitleView = (TextView) landTitleLayout.findViewById(R.id.video_landscape_title);

        portraitLayout = (ViewGroup) findViewById(R.id.video_top_title_portrait_layout);
        portraitBackView = portraitLayout.findViewById(R.id.video_portrait_title_back);

        landBackView.setOnClickListener(clickListener);
        portraitBackView.setOnClickListener(clickListener);
    }

    /**
     * 显示标题样
     */
    protected void showTitleView(String title) {
        if (isLandScape()) {
            landTitleLayout.setVisibility(View.VISIBLE);
            portraitLayout.setVisibility(View.GONE);
            landTitleView.setText(title);
        } else {
            landTitleLayout.setVisibility(View.GONE);
            portraitLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 用于初始化Buffer层
     */
    private void initBufferView() {
        bufferLayout = (ViewGroup) findViewById(R.id.video_buffering_layout);
    }

    private void showBufferView() {
        bufferLayout.setVisibility(View.VISIBLE);

    }

    private void hideBufferView() {
        bufferLayout.setVisibility(View.GONE);
    }

    /**
     * 用于初始化分享层
     */
//    void initShareView() {
//
//    }

    /**
     * 用于错误层
     */
    private void initErrorView() {
        noNetWorkLayout = (ViewGroup) findViewById(R.id.video_error_pause_lay);
    }

    private void showNoNetWork() {
        hideBufferView();
        hideLoadingView();
        showAdvert(false);
        hideRetryView();
        hideMobileView();
        noNetWorkLayout.setVisibility(View.VISIBLE);
    }

    private void hideNoNetWork() {
        noNetWorkLayout.setVisibility(View.GONE);
    }

    /**
     * 用于重试层
     */
    private void initRetryView() {
        retryLayout = (ViewGroup) findViewById(R.id.video_error_retry_layout);
        retryView = retryLayout.findViewById(R.id.video_error_retry_img);
        retryView.setOnClickListener(clickListener);
    }

    private void showRetryView() {
        retryLayout.setVisibility(View.VISIBLE);
        hideLoadingView();
        hideBufferView();
        hideLoadingView();
        showAdvert(false);
        hideRetryView();
        hideMobileView();
    }

    private void hideRetryView() {
        retryLayout.setVisibility(View.GONE);
    }

    /**
     * 用于流量提示层
     */
    private void initMobileView() {
        mobileTipLayout = (ViewGroup) findViewById(R.id.video_mobile_net_layout);
        mobileContinueView = mobileTipLayout.findViewById(R.id.video_mobile_continue);
        mobileContinueView.setOnClickListener(clickListener);
    }

    private void showMobileView() {
        mobileTipLayout.setVisibility(View.VISIBLE);
    }

    private void hideMobileView() {
        mobileTipLayout.setVisibility(View.GONE);
    }

    /**
     * 用于清晰度切换提示层
     */
    private void initSwitchStreamView() {
        streamSwitchLayout = (ViewGroup) findViewById(R.id.video_stream_switch_layout);
        streamView = (TextView) streamSwitchLayout.findViewById(R.id.stream_selected);
    }

    private void showStreamView() {
        streamSwitchLayout.setVisibility(View.VISIBLE);
    }

    private void hideStreamView() {
        streamSwitchLayout.setVisibility(View.GONE);
    }


    private void initRightVolumeView() {
        rightVolumeLayout = (ViewGroup) findViewById(R.id.video_landscape_right_lay);
        rightVolumeSeekBar = (SeekBarVer) rightVolumeLayout.findViewById(R.id.seekBar_volume);
    }

    private void hideAllLoadingView() {
        hideLoadingView();
        hideBufferView();
        showAdvert(false);
        hideNoNetWork();
        hideRetryView();
        hideMobileView();
    }

    private void volumeBusiness() {
        mCurVolume = getCurrentVolume();
//        setVolumeBtnDrawable(mCurVolume);
//        setAdVolumeBtnDrawable(mCurVolume);
        if (rightVolumeSeekBar != null && rightVolumeSeekBar instanceof SeekBarVer) {
            rightVolumeSeekBar.setEnabled(true);
            rightVolumeSeekBar.setMax(mMaxVolume);
            rightVolumeSeekBar.setProgress(mCurVolume);
        }
    }

    public void switchOrientation() {
        Configuration config = this.getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            toLand();

        } else {
            toPortrait();

        }
    }

    void toLand() {
        if (Build.VERSION.SDK_INT >= 9) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    public void toPortrait() {
        if (isLandScape()) {

            if (Build.VERSION.SDK_INT >= 9) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    }

    protected abstract void handleOfflineVideoPlayError();


    void videoPlayerInitOffline(String guid) {
        updateVideoMediaController();


        String url;
        try {
            url = CacheManager.getPlayPathForOffline(IfengApplication.getInstance(), guid);
        } catch (Exception e) {
            handleOfflineVideoPlayError();
            ToastUtils.getInstance().showShortToast(getString(R.string.video_play_url_error));
            return;
        }
        UserFeed.initIfengAddress(IfengApplication.getAppContext(), url);
        IMediaPlayer.MediaSource source = new IMediaPlayer.MediaSource();
        source.id = PlayType.FEATRUE;
        source.playUrl = url;
        IMediaPlayer.MediaSource[] mediaSources = new IMediaPlayer.MediaSource[1];
        mediaSources[0] = source;
        mVideoView.setVideoPath(mediaSources);
    }


    void videoPlayerInitOnline(String url) {
        updateVideoMediaController();
        IMediaPlayer.MediaSource source = new IMediaPlayer.MediaSource();
        source.id = PlayType.FEATRUE;
        source.playUrl = url;
        IMediaPlayer.MediaSource[] mediaSources = new IMediaPlayer.MediaSource[1];
        mediaSources[0] = source;
        mVideoView.setVideoPath(mediaSources);
    }

    /**
     * 判断是否为离线
     */
    protected abstract boolean isOffLine();

    /**
     * 在有网情况下展示的error界面
     */
    public void updateErrorRetryLayer(boolean show) {
        if (show) {


            mobileNetShowing = false;
            hideGestureView();
            hideCommentFragment();
            hideController();
            //进度条不可显状态
            setControllerShowHide(false);

            RelativeLayout.LayoutParams errorLayerLp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            errorLayerLp.addRule(RelativeLayout.CENTER_IN_PARENT);


        } else {
            //进度条可显状态
            setControllerShowHide(true);


        }

    }


    /**
     * 在无网情况下展示的erro界面
     *
     * @param show
     */
    public void updateErrorPauseLayer(boolean show) {
        if (show) {

            hideLandRight();
            mobileNetShowing = false;
            hideGestureView();
            hideCommentFragment();
            hideController();
            //进度条不可显状态
            setControllerShowHide(false);


        } else {
            //进度条可显状态
            setControllerShowHide(true);

        }
    }

    /**
     * 在运营商网络下展示提示
     *
     * @param show
     */
    public void updateMobileNetLayer(boolean show) {
        mobileNetShowing = show;
        if (mVideoView != null) {
            mVideoView.isNeedTodPlay(!show);//bug #10026：多次后台唤醒播放页停留在运营商界面，但是概率出现有时声音在播
        }
        if (show) {
            hideLandRight();
            hideGestureView();
            hideCommentFragment();
            hideController();
            //进度条不可显状态
            setControllerShowHide(false);


            RelativeLayout.LayoutParams loadingLp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            loadingLp.addRule(RelativeLayout.CENTER_IN_PARENT);


        } else {
            //进度条可显状态
            setControllerShowHide(true);

        }
    }


    /**
     * 更新广告controller的子view显隐状态
     *
     * @param isTitles
     */
    void updateAdChildViewVisibility(boolean isTitles) {
        //顶部返回按钮
        RelativeLayout iv_return = (RelativeLayout) findViewById(R.id.iv_return_rl);
        //全屏按钮
        RelativeLayout iv_fullscreen = (RelativeLayout) findViewById(R.id.rl_fullscreen);
        if (iv_fullscreen != null) {
            iv_fullscreen.setVisibility(isLandScape() ? View.GONE : View.VISIBLE);

        }
        if (iv_return != null) {
            iv_return.setVisibility(isLandScape() ? View.VISIBLE : View.GONE);
        }
        //倒计时textView
        TextView tv_ad_count_down = (TextView) findViewById(R.id.tv_ad_count_down);
        //“了解详情”按钮
        TextView tv_get_detail = (TextView) findViewById(R.id.tv_getdetail);
        if (tv_ad_count_down != null) {
            tv_ad_count_down.setVisibility(isTitles ? View.GONE : View.VISIBLE);
        }
        if (tv_get_detail != null) {
            tv_get_detail.setVisibility(isTitles ? View.GONE : View.VISIBLE);
        }
    }


    public void updateLoadingLayer(boolean show, String title) {
        logger.debug("updateLoadingLayer show = {}, title = {} ", show, title);

        if (show) {
            mobileNetShowing = false;
            //隐藏手势栏
            hideGestureView();
            hideController();
            //控制栏不可见
            setControllerShowHide(false);


        } else {


            //控制栏可见
            setControllerShowHide(true);
        }
    }

    /**
     * 隐藏手势标示
     */
    private void hideGestureView() {
        if (mGestureRootView != null && mGestureRootView.getVisibility() == View.VISIBLE) {
            mGestureRootView.setVisibility(View.GONE);
        }
    }

    String getSelectedStream() {
        return null;
    }

    public void updateBufferLayer(boolean show) {

        bufferLayout.setVisibility(show ? View.VISIBLE : View.GONE);

//        if (!show && hasCP()) {
//            updateADController(true);
//        }


    }


    /**
     * 没有加载出资源时候弹的dialog
     */
    private Dialog dialog;

    void showNoResourceDialog() {
        if (isFromPush()) {
            AlertUtils.getInstance().showOneBtnDialog(this, getString(R.string.video_play_url_miss), getString(R.string.common_i_know), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    finish();
                    Intent intent = new Intent(BaseVideoPlayerActivity.this, ActivityMainTab.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }
    }

    public boolean isFromPush() {
        return getIntent() != null && getIntent().getBooleanExtra(IntentKey.IS_FROM_PUSH, false);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            if (mVideoController != null && mVideoController.isShowing()) {
                mVideoController.continueShow();
            }
        } else {
            if (mVideoController != null) {
                mVideoController.rmMsgFadeOut();
            }
        }
    }

    private void setCurrentVolume(int currentVolume) {
        mCurVolume = currentVolume;
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
    }


    private boolean isSilent = false;
    boolean isAdSilent = false;


    /**
     * 启动DLNAService
     */
    private void startDLNAService() {
        if (SharePreUtils.getInstance().getDLNAState()) {
            DLNADeviceManager.getInstance().startSearch(this);
        }
    }

    /**
     * 停止DLNAService
     */
    private void stopDLNAService() {
        if (SharePreUtils.getInstance().getDLNAState()) {
            DLNADeviceManager.getInstance().exitSearch(this);
        }
    }


    public abstract PlayerInfoModel getCurrProgram();


    @Override
    public PlayState getCurrState() {
        return currentPlayState;
    }


    @Override
    public void onStateChange(PlayState state) {
        logger.debug("onStateChange---->{}", state);
        currentPlayState = state;
        switch (state) {
            case STATE_PREPARING:
            case STATE_PREPARED:
                showLoadingView();
                break;
            case STATE_IDLE:
                break;
            case STATE_PLAYING:
                hideLoadingView();
                mVideoView.setControllerVisibily(true);
                break;
            case STATE_PAUSED:
                break;
            case STATE_PLAYBACK_COMPLETED:

                break;
            case STATE_BUFFERING_START:

                break;
            case STATE_BUFFERING_END:

                break;
            case STATE_ERROR:
                //ERROR
                if (!NetUtils.isNetAvailable(this)) {
                    showNoNetWork();
                } else {
                    showRetryView();
                }

                break;
            case STATE_PLAY_NEXT:

                break;
            case STATE_PLAY_PRE:

                break;
            default:
                break;
        }
    }


    @Override
    public boolean doPauseOrResum(boolean isDoPause) {

        return false;
    }

    /**
     * Controll隐藏后的回调函数
     */
    @Override
    public void onHidden() {


    }


    /**
     * Controll显示后的回调函数
     */
    @Override
    public void onShown() {


    }
}
