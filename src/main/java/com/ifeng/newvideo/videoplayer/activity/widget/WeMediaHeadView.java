package com.ifeng.newvideo.videoplayer.activity.widget;


import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.R;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BitmapUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.subscribe.WeMediaInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WeMediaHeadView extends RelativeLayout implements View.OnClickListener {
    private static final Logger logger = LoggerFactory.getLogger(WeMediaHeadView.class);

    private Context mContext;
    private NetworkImageView img_banner;//背景图
    private NetworkImageView icon_header;//头像
    private TextView tv_media_title;//自媒体名称
    private TextView tv_subscribe_num;//订阅数量
    private TextView tv_issue_video_num;//发布视频数量
    private TextView tv_media_description;//自媒体描述
    private View back_content;

    public static int mFollowNo;


    public WeMediaHeadView(Context context) {
        this(context, null);
    }

    public WeMediaHeadView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeMediaHeadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LayoutInflater.from(context).inflate(R.layout.wemedia_head_view_layout, this, true);
        initView();
    }

    private void initView() {
        img_banner = (NetworkImageView) findViewById(R.id.img_banner);
        back_content = findViewById(R.id.rl_back_content);
        icon_header = (NetworkImageView) findViewById(R.id.icon_header);
        tv_media_title = (TextView) findViewById(R.id.tv_media_title);
        tv_subscribe_num = (TextView) findViewById(R.id.tv_subscribe_num);
        tv_issue_video_num = (TextView) findViewById(R.id.tv_issue_video_num);
        tv_media_description = (TextView) findViewById(R.id.tv_media_description);
    }

    @Override
    public void onClick(View view) {

    }

    public View getHeadView () {
        return icon_header;
    }

    public TextView getContentView () {
        return tv_media_title;
    }

    /**
     * 初始化WeMedia数据
     */
    public void initWeMediaData(WeMediaInfoList.InfoListEntity.WeMediaEntity weMediaEntity) {
        setAvatar(icon_header, weMediaEntity.getHeadPic());
        tv_media_title.setText(weMediaEntity.getName());
        String followNo = weMediaEntity.getFollowNo();
        if (TextUtils.isDigitsOnly(followNo) && !TextUtils.isEmpty(followNo)) {
            try {
                mFollowNo = Integer.parseInt(followNo);
            } catch (NumberFormatException e) {
                logger.error("followNo  = {},  error! {}", followNo, e);
            }
        }
        setFollowNo();
        if (TextUtils.isEmpty(weMediaEntity.getDesc())) {
            tv_media_description.setText(R.string.no_introduction);
        } else {
            tv_media_description.setText(weMediaEntity.getDesc());
        }
        String  issue_video_num = StringUtils.changeNumberMoreThen10000(weMediaEntity.getTotalNum());
        tv_issue_video_num.setText("发布" + issue_video_num + "条");
        setBannerBg(weMediaEntity.getHeadPic(), img_banner);//设置Banner背景
    }

    private void setAvatar(final NetworkImageView imageView, String url) {
        VolleyHelper.getImageLoader().get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    imageView.setImageBitmap(BitmapUtils.makeRoundCorner(response.getBitmap()));
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                imageView.setImageResource(R.drawable.icon_login_default_header);
            }
        });
    }


    /**
     * 设置订阅数量
     */
    public void setFollowNo() {
        tv_subscribe_num.setVisibility(mFollowNo > 0 ? View.VISIBLE : View.GONE);
        tv_subscribe_num.setText(StringUtils.changeNumberMoreThen10000(String.valueOf(mFollowNo)) + "人订阅");
    }

    /**
     * 设置Banner背景,并实现毛玻璃效果
     *
     * @param url       背景图url
     * @param imageView
     */
    private void setBannerBg(String url, final ImageView imageView) {
        VolleyHelper.getImageLoader().get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null && response.getBitmap() != null) {
                    Bitmap bitmap = BitmapUtils.gsBlurFilter(response.getBitmap());
                    imageView.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                imageView.setImageResource(R.drawable.icon_login_default_header);
            }
        });
    }

}
