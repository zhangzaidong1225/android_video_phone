package com.ifeng.newvideo.videoplayer.activity.widget;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.bean.OnInnerItemClickListener;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.newvideo.videoplayer.activity.VideoBannerAdvertManager;
import com.ifeng.newvideo.videoplayer.activity.VideoRelativeAdListener;
import com.ifeng.newvideo.videoplayer.activity.VideoRelativeAdvertManager;
import com.ifeng.newvideo.videoplayer.activity.listener.VideoDetailCollectionClickListener;
import com.ifeng.newvideo.videoplayer.activity.listener.VideoDetailDownloadClickListener;
import com.ifeng.newvideo.videoplayer.activity.listener.VideoDetailSubscribeClickListener;
import com.ifeng.newvideo.videoplayer.bean.ADvertItem;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInfoItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.bean.YoukuVideoItem;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.widget.AdBannerViewContainer;
import com.ifeng.newvideo.widget.AdViewContainer;
import com.ifeng.newvideo.widget.AdvertViewContainer;
import com.ifeng.newvideo.widget.CustomRecyclerView;
import com.ifeng.newvideo.widget.GoodView;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.dao.FavoritesDAO;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.dao.WeMediaDao;
import com.ifeng.video.dao.db.model.FavoritesModel;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeRelation;
import com.ifeng.video.dao.util.AdsExposureSesssion;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * 播放底页头部，包含：视频标题、自媒体信息，广告信息流，相关视频等
 * Created by fanshell on 2016/7/31.
 */
public class DetailVideoHeadView extends LinearLayout implements View.OnClickListener {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DetailVideoHeadView.class);
    private Context mContext;
    private VideoItem mCurrentItem;
    private final int INIT_SHOW_COUNT = 10;
    private TextView mTitleView;

    private LinearLayout ll_zan, ll_step;
    private ImageView mZanIv, mStepIv;
    private TextView tvZan, tvStep;
    private int zanNum = 0;
    private int stepNum = 0;//踩
    private GoodView mGoodView;

    private boolean isClickZan = false;
    private boolean isClickStep = false;
    private boolean isFirstClick = false;

    private View mDownloadView;
    private View mCollectView;
    private View mShareView;
    private TextView tv_download;
    private ImageView iv_download;
    private ImageView iv_share;
    private TextView tv_share;
    private ImageView iv_collect;
    private TextView tv_collect;
    private String guid;
    private UIPlayContext mPlayContext;


    private List<RelativeVideoInfoItem> videoList;
    private List<MainAdInfoModel.AdMaterial> mADList = new ArrayList<>();
    private List<VideoItem> bottomList = new ArrayList<>();


    /**
     * 用户相关的View
     */
    private NetworkImageView mUserIconView;
    private TextView mUserNameView;
    private TextView mFollowView;
    private VideoDetailSubscribeClickListener subscribeClickListener;

    /**
     * 相关视频的View及监听单击
     */
    private LinearLayout mRelativeVideoContainer;
    private LinearLayout mRecommendTopContainer;
    private LinearLayout mRecommendBottomContainer;
    private OnItemClickListener mItemClickListener;
    private View mMorView;
    private LinearLayout mRecyclerView;
    private LinearLayout mRelativeVideoView;
    private LinearLayout mRelativeAdView;

    /**
     * 广告
     */
    private AdBannerViewContainer mAdViewContainer;
    private TextView mAdLabelView;
    private NetworkImageView mAdView;
    private MainAdInfoModel.AdMaterial mAdMaterial;

    private boolean isStartAdvert;
    private RelativeLayout mVideoDetailContainer;
    private ImageView mOpenView;
    private TextView mDescView;
    private TextView mCreateDateView;

    private View mErrorView;
    private ImageView mErrorIconView;
    private TextView mErrorTipView;

    private View mVideoInfoView;
    private boolean mAttach = false;
    private ActivityVideoPlayerDetail mActivity;

    private VideoBannerAdvertManager mVideoInfoAdManager;
    private String echid;
    //    private VideoDetailShareClickListener mShareClickListener;
    private View mMediaContainer;

    private CustomRecyclerView recyclerView;
    private DramaRecyclerAdapter dramaRecyclerAdapter;
    private TextView tv_see_all;
    private List<Boolean> itemsIsClick = new ArrayList<>();
    private int currentPlay = -1;

    /**
     * 相关视频信息流广告
     */
    private VideoRelativeAdvertManager mRelativeAd;
    private VideoRelativeAdListener mRelativeListener;
    private static final int ZAN_STATUS = 111;
    private static final int STEP_STATUS = 112;
    private static final int DEFAULT_STATUS = 113;


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!mAttach) {
            registerLoginBroadcast();
            mAttach = true;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        logger.debug("onDetachedFromWindow ");
        if (mAttach) {
            unregisterLoginReceiver();
        }
        WeMediaDao.cancelAll();
        VolleyHelper.getRequestQueue().cancelAll(VideoBannerAdvertManager.TAG);
        if (mVideoInfoAdManager != null) {
            mVideoInfoAdManager.setOnVideoInfoHeaderSuccess(null);
        }
    }

    public DetailVideoHeadView(Context context) {
        this(context, null);
    }

    public DetailVideoHeadView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailVideoHeadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.video_detail_header, this, true);
        mVideoDetailContainer = (RelativeLayout) findViewById(R.id.video_detail_container);
        mTitleView = (TextView) findViewById(R.id.video_detail_title);
        mZanIv = (ImageView) findViewById(R.id.video_zan);
        mStepIv = (ImageView) findViewById(R.id.video_detail_step);
        ll_zan = (LinearLayout) findViewById(R.id.ll_zan);
        tvZan = (TextView) findViewById(R.id.tv_zan_num);
        tvStep = (TextView) findViewById(R.id.tv_step_num);
        ll_step = (LinearLayout) findViewById(R.id.ll_step);
        mDownloadView = findViewById(R.id.video_detail_download);
        tv_download = (TextView) findViewById(R.id.tv_video_download);
        iv_download = (ImageView) findViewById(R.id.iv_video_download);
        mShareView = findViewById(R.id.ll_video_share);
        iv_share = (ImageView) findViewById(R.id.video_detail_bottom_share_icon);
        tv_share = (TextView) findViewById(R.id.tv_video_share);
        mCollectView = findViewById(R.id.ll_video_collect);
        iv_collect = (ImageView) findViewById(R.id.video_detail_header_collect_icon);
        tv_collect = (TextView) findViewById(R.id.tv_video_collect);

        mGoodView = new GoodView(context);

        mZanIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setZanStatus(view);
            }
        });
        ll_zan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setZanStatus(view);
            }
        });
        mStepIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setStepStatus(view);
            }
        });
        ll_step.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setStepStatus(view);
            }
        });

        mUserIconView = (NetworkImageView) findViewById(R.id.video_detail_user_icon);
        mUserNameView = (TextView) findViewById(R.id.video_detail_user_name);
        mFollowView = (TextView) findViewById(R.id.video_detail_follow);
        subscribeClickListener = new VideoDetailSubscribeClickListener();
        mFollowView.setOnClickListener(subscribeClickListener);

        mMorView = findViewById(R.id.video_detail_recommend_show_more);
        mMorView.setOnClickListener(this);
        mRelativeVideoContainer = (LinearLayout) findViewById(R.id.video_detail_relative_container);
        mRecommendTopContainer = (LinearLayout) findViewById(R.id.video_detail_recommend_top_container);
        mRecommendBottomContainer = (LinearLayout) findViewById(R.id.video_detail_recommend_bottom_container);
        mRecommendTopContainer.removeAllViews();
        mRecyclerView = (LinearLayout) findViewById(R.id.ll_recyclerview);
        mRelativeVideoView = (LinearLayout) findViewById(R.id.ll_relative_video_view);

        mRelativeAdView = (LinearLayout) findViewById(R.id.ll_ad_container);
        mRelativeAdView.setVisibility(View.VISIBLE);
        mAdView = (NetworkImageView) findViewById(R.id.video_detail_ad);
        mAdViewContainer = (AdBannerViewContainer) findViewById(R.id.video_detail_ad_container);
        mAdLabelView = (TextView) findViewById(R.id.video_detail_ad_label);
        mAdView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdView(view);
            }
        });

        mOpenView = (ImageView) findViewById(R.id.video_detail_open);
        mDescView = (TextView) findViewById(R.id.video_detail_desc);
        mCreateDateView = (TextView) findViewById(R.id.video_detail_create_date);


        mVideoInfoView = findViewById(R.id.video_detail_information_container);
        mVideoInfoView.setVisibility(View.GONE);
        mRelativeVideoContainer.setVisibility(View.GONE);
        mErrorView = findViewById(R.id.video_detail_load_data_error);
        mErrorIconView = (ImageView) mErrorView.findViewById(R.id.video_detail_load_fail_icon);
        mErrorTipView = (TextView) mErrorView.findViewById(R.id.video_detail_load_fail_text);
        resetErrorView();
        mErrorView.setVisibility(View.GONE);

        mPlayContext = new UIPlayContext();
        mDownloadView.setOnClickListener(new VideoDetailDownloadClickListener(mContext, IfengType.TYPE_VIDEO));
        mCollectView.setOnClickListener(new VideoDetailCollectionClickListener(FavoritesModel.RESOURCE_VIDEO, echid, mPlayContext));

        mShareView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnShareVideoListener != null) {
                    mOnShareVideoListener.onShareVideoItem();
                }
            }
        });

        mVideoInfoAdManager = new VideoBannerAdvertManager();

        mVideoInfoAdManager.setOnVideoInfoHeaderSuccess(new VideoInfoHeadListener(this));
        isStartAdvert = false;
        mRelativeAd = new VideoRelativeAdvertManager();
        mRelativeListener = new VideoRelativeAdListener(this);
        mRelativeListener.setAdvertManager(mRelativeAd);
        mRelativeAd.setOnVideoRelativeSuccess(mRelativeListener);
        mMediaContainer = findViewById(R.id.video_detail_media_container);

        recyclerView = (CustomRecyclerView) findViewById(R.id.rv_slide);
        tv_see_all = (TextView) findViewById(R.id.tv_see_all);
        tv_see_all.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSeeAllClickListener != null) {
                    mSeeAllClickListener.onSeeAll();
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        dramaRecyclerAdapter = new DramaRecyclerAdapter();

        recyclerView.setAdapter(dramaRecyclerAdapter);
    }

    public OnClickRecyclerItem onClickRecyclerItem;

    public interface OnClickRecyclerItem {
        void onClickRecyclerItem(VideoItem item);
    }

    public void setOnClickRecyclerItem(OnClickRecyclerItem listener) {
        this.onClickRecyclerItem = listener;
    }

    private class DramaRecyclerAdapter extends RecyclerView.Adapter<DramaRecyclerAdapter.DramaRecyclerHolder> {

        @Override
        public DramaRecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View containerView = LayoutInflater.from(mContext).inflate(R.layout.item_drama_recycler, viewGroup, false);
            DramaRecyclerHolder dramaRecyclerHolder = new DramaRecyclerHolder(containerView);

            containerView.setOnClickListener(new OnInnerItemClickListener() {
                @Override
                public void onItemInnerClick(View view, int positon) {
                    for (int i = 0; i < itemsIsClick.size(); i++) {
                        itemsIsClick.set(i, false);
                    }
                    itemsIsClick.set(positon, true);
                    notifyDataSetChanged();
                    if (onClickRecyclerItem != null) {
                        onClickRecyclerItem.onClickRecyclerItem(videoList.get(positon).videoInfo);
                    }
                    recyclerView.smoothScrollToPosition(positon);
                }
            });

            return dramaRecyclerHolder;
        }

        @Override
        public void onBindViewHolder(DramaRecyclerHolder dramaRecyclerHolder, int position) {
            RelativeVideoInfoItem entity = videoList.get(position);
            VideoItem videoItem = entity.videoInfo;
            if (videoItem != null) {
                dramaRecyclerHolder.timer.setText(videoItem.createDate.substring(0, 10));
                dramaRecyclerHolder.title.setText(videoItem.title);

                CommonStatictisListUtils.getInstance().addVideoDetailFocusList(mActivity, videoItem.itemId, position, PageIdConstants.REFTYPE_EDITOR, echid,
                        0, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.normal);
                CommonStatictisListUtils.getInstance().sendYoukuConstatic(videoItem, CommonStatictisListUtils.YK_EXPOSURE, CommonStatictisListUtils.YK_FEED_DETAIL);

            }
            if (itemsIsClick.get(position)) {
                dramaRecyclerHolder.title.setTextColor(Color.parseColor("#F54343"));
            } else {
                dramaRecyclerHolder.title.setTextColor(Color.parseColor("#262626"));
            }
            dramaRecyclerHolder.itemView.setTag(R.id.tag_key_click, position);


        }

        @Override
        public int getItemCount() {
            if (!ListUtils.isEmpty(videoList)) {
                return videoList.size();
            }
            return 0;
        }

        class DramaRecyclerHolder extends RecyclerView.ViewHolder {
            TextView timer, title;

            public DramaRecyclerHolder(View itemView) {
                super(itemView);
                timer = (TextView) itemView.findViewById(R.id.tv_timer);
                title = (TextView) itemView.findViewById(R.id.tv_content_title);
            }
        }
    }

    static class VideoInfoHeadListener implements VideoBannerAdvertManager.OnVideoBannerAdvertListener {
        WeakReference<DetailVideoHeadView> weakReference;

        VideoInfoHeadListener(DetailVideoHeadView view) {
            weakReference = new WeakReference<>(view);
        }

        @Override
        public void onVideoBannerAdvertSuccess(MainAdInfoModel model, int index) {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }
            MainAdInfoModel.AdMaterial adMaterial = model.getAdMaterials().get(index);

            // 余量广告
            if (adMaterial.getNeedMoreAd() == 1) {
                view.mVideoInfoAdManager.requestADMore(model, index);
                return;
            }
            view.showAd(adMaterial);
        }

        @Override
        public void onVideoBannerAdvertFailed() {

            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }

            if (null != VideoAdConfigDao.adConfig && null != VideoAdConfigDao.adConfig.getInfoAD() && !ListUtils.isEmpty(VideoAdConfigDao.adConfig.getInfoAD().getBanner())) {
                String adid = VideoAdConfigDao.adConfig.getInfoAD().getBanner().get(0).getAdId();
                CommonStatictisListUtils.getInstance().sendADEmptyExpose(adid);
                return;
            }


            view.mAdViewContainer.setVisibility(View.GONE);
        }

        @Override
        public void onVideoBannerAdvertMoreSuccess(MainAdInfoModel model) {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }

            view.showAd(model.getAdMaterials().get(0));
        }

        @Override
        public void onVideoBannerAdvertMoreADFailed() {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }

            view.mAdViewContainer.setVisibility(View.GONE);
        }
    }

    private void showGoodView(View targerView) {
        if (mGoodView.isShowing()) {
            mGoodView.dismiss();
        }

        mGoodView.setText("+1");
        mGoodView.show(targerView);

        //scaleAnimator(targerView);
    }

    private void scaleAnimator(View targerView) {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(targerView, "scaleX", 1.0f, 0.0f);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(targerView, "scaleY", 1.0f, 0.0f);
        ObjectAnimator animatorX2 = ObjectAnimator.ofFloat(targerView, "scaleX", 0.0f, 1.0f);
        ObjectAnimator animatorY2 = ObjectAnimator.ofFloat(targerView, "scaleY", 0.0f, 1.0f);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(800);
        set.setInterpolator(new SpringScaleInterpolator(0.4f));
        set.playTogether(animatorX, animatorY, animatorX2, animatorY2);
        set.start();
    }

    public class SpringScaleInterpolator implements Interpolator {
        private float factor;

        public SpringScaleInterpolator(float factor) {
            this.factor = factor;
        }

        @Override
        public float getInterpolation(float input) {
            return (float) (Math.pow(2, -10 * input) * Math.sin(input - factor / 4) * (2 * Math.PI) / factor + 1);
        }
    }

    private void setZanStatus(View view) {

        if (!isClickZan) {
            showGoodView(view);
            isClickZan = true;
            addZanNum();
            uploadZanStatus();
            if (isClickStep) {
                initStepView();
            }
        } else {
            initZanView();
            removeZanStepMap();
        }
    }

    private void uploadZanStatus() {
        if (!isFirstClick) {
            onClickDingView();
            isFirstClick = true;
        }
    }

    private void uploadStepStatus() {
        if (!isFirstClick) {
            onClickCaiView();
            isFirstClick = true;
        }
    }

    private void addZanNum() {
        zanNum += 1;
        tvZan.setVisibility(View.VISIBLE);
        if (zanNum > 999) {
            setText(tvZan, "999+");
        } else {
            setText(tvZan, String.valueOf(zanNum));
        }
        mZanIv.setImageResource(R.drawable.iv_zan_selected);
        SharePreUtils.getInstance().getDingMap().put(guid, ZAN_STATUS);
    }

    private void setStepStatus(View view) {

        if (!isClickStep) {
            showGoodView(view);
            isClickStep = true;
            addStepNum();
            uploadStepStatus();
            if (isClickZan) {
                initZanView();
            }
        } else {
            initStepView();
            removeZanStepMap();
        }
    }

    private void addStepNum() {
        stepNum += 1;
        tvStep.setVisibility(View.VISIBLE);
        if (stepNum > 999) {
            setText(tvStep, "999+");
        } else {
            setText(tvStep, String.valueOf(stepNum));
        }
        mStepIv.setImageResource(R.drawable.iv_step_selected);
        SharePreUtils.getInstance().getDingMap().put(guid, STEP_STATUS);

    }

    private void initZanView() {
        isClickZan = false;
        mZanIv.setImageResource(R.drawable.iv_zan);
        zanNum = zanNum > 0 ? zanNum - 1 : 0;
        tvZan.setVisibility(View.VISIBLE);
        if (0 == zanNum) {
            tvZan.setVisibility(View.INVISIBLE);
        } else if (zanNum > 999) {
            setText(tvZan, "999+");
        } else {
            setText(tvZan, String.valueOf(zanNum));
        }
    }

    private void initStepView() {
        isClickStep = false;
        mStepIv.setImageResource(R.drawable.iv_step);
        stepNum = stepNum > 0 ? stepNum - 1 : 0;
        tvStep.setVisibility(View.VISIBLE);
        if (0 == stepNum) {
            tvStep.setVisibility(View.INVISIBLE);
        } else if (stepNum > 999) {
            setText(tvStep, "999+");
        } else {
            setText(tvStep, String.valueOf(stepNum));
        }
    }

    private void updateDingCaiStatus() {
        if (SharePreUtils.getInstance().getDingMap().containsKey(guid)) {
            for (Map.Entry<String, Integer> entry : SharePreUtils.getInstance().getDingMap().entrySet()) {
                if (guid.equalsIgnoreCase(entry.getKey())) {
                    switch (entry.getValue()) {
                        case ZAN_STATUS:
                            mZanIv.setImageResource(R.drawable.iv_zan_selected);
                            mStepIv.setImageResource(R.drawable.iv_step);
                            zanNum += 1;
                            isClickZan = true;
                            break;
                        case STEP_STATUS:
                            mStepIv.setImageResource(R.drawable.iv_step_selected);
                            mZanIv.setImageResource(R.drawable.iv_zan);
                            stepNum += 1;
                            isClickStep = true;
                            break;
                        case DEFAULT_STATUS:
                            mZanIv.setImageResource(R.drawable.iv_zan);
                            mStepIv.setImageResource(R.drawable.iv_step);
                            break;
                        default:
                            break;
                    }
                }
            }
        } else {
            initZanAndStepStatus();
        }
    }

    private void initZanAndStepStatus() {
        mZanIv.setImageResource(R.drawable.iv_zan);
        mStepIv.setImageResource(R.drawable.iv_step);
        zanNum = 0;
        stepNum = 0;
        isClickStep = false;
        isClickZan = false;
        isFirstClick = false;
    }

    private void removeZanStepMap() {
        if (SharePreUtils.getInstance().getDingMap().containsKey(guid)) {
            Iterator iterator = SharePreUtils.getInstance().getDingMap().keySet().iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals(guid)) {
                    iterator.remove();
                }
            }
        }
    }

    private void onClickDingView() {

        CommentDao.videoDingAction(guid, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                logger.debug("the vote response is == {}", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.debug("the vote error");
            }
        });

        CommonStatictisListUtils.getInstance().sendYoukuConstatic(mCurrentItem, CommonStatictisListUtils.YK_LIKE, CommonStatictisListUtils.YK_FEED_DETAIL);

    }

    private void onClickCaiView() {

        CommentDao.videoCaiAction(guid, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                logger.debug("the vote response is == {}", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.debug("the vote error");
            }
        });

    }

    private void onClickAdView(View view) {
        if (view.getTag() == null) {
            return;
        }
        MainAdInfoModel.AdMaterial adMaterial = (MainAdInfoModel.AdMaterial) view.getTag();
        String adId = adMaterial.getAdId();
        String title = adMaterial.getText();
        String imageUrl = adMaterial.getImageURL();

        String adType = "";
        String clickUrl = "";
        List<String> downloadCompletedUrls = null;
        MainAdInfoModel.AdMaterial.AdAction adAction = adMaterial.getAdAction();
        if (adAction != null) {
            if (!IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType())) {
                adType = IfengType.TYPE_WEBFULL;
            }

            clickUrl = adAction.getUrl();

            // 如果是广告app下载类型，点击地址取loadingUrl
            if (adMaterial.getAdConditions() != null && "app".equalsIgnoreCase(adMaterial.getAdConditions().getShowType())) {
                clickUrl = adAction.getLoadingurl();
            }

            if (!ListUtils.isEmpty(adAction.getAsync_downloadCompletedurl())) {
                downloadCompletedUrls.addAll(adAction.getAsync_downloadCompletedurl());
            }
            if (!ListUtils.isEmpty(adAction.getDownloadCompletedurl())) {
                downloadCompletedUrls.addAll(adAction.getDownloadCompletedurl());
            }

            //广告点击曝光
            AdvertExposureDao.sendAdvertClickReq(adId, (ArrayList<String>) adAction.getAsync_click());
        }

        ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, clickUrl, null, null, view.getContext(), downloadCompletedUrls, "", "", adAction != null ? adAction.getAsync_download() : null);
    }

    private void showAd(MainAdInfoModel.AdMaterial adMaterial) {
        if (adMaterial == null) {
            return;
        }

        mAdMaterial = adMaterial;
        mAdView.setTag(adMaterial);
        mAdView.setImageUrl(adMaterial.getImageURL(), VolleyHelper.getImageLoader());
        mAdViewContainer.setVisibility(View.VISIBLE);
        MainAdInfoModel.AdMaterial.Icon icon = adMaterial.getIcon();
        if (icon != null) {
            String text = icon.getText();
            mAdLabelView.setText(TextUtils.isEmpty(text) ? "广告" : text);
            mAdLabelView.setVisibility(icon.getShowIcon() == 1 ? View.VISIBLE : View.GONE);
        }

    }

    private void resetErrorView() {
        mErrorIconView.setImageResource(R.drawable.icon_common_load_fail);
        mErrorTipView.setText(R.string.common_load_data_error);
    }

    private void updateErrorView() {
        mErrorIconView.setImageResource(R.drawable.icon_common_loading);
        mErrorTipView.setText(R.string.common_onloading);
    }

    public void updateVideoInformation(final VideoItem item, boolean isRelate) {
        this.mVideoInfoView.setVisibility(View.VISIBLE);
        if (item == null) {
            return;
        }
        if (mRelativeVideoContainer.isShown()) {
            mErrorView.setVisibility(View.GONE);
        }

        zanNum = 0;
        stepNum = 0;
        setText(mTitleView, item.title);
        guid = item.guid;
        mCurrentItem = item;
        updateDingCaiStatus();
        subscribeClickListener.setVideoItem(item);

        if (!TextUtils.isEmpty(item.shareTimes)) {
            zanNum += Integer.parseInt(item.shareTimes);
            if (0 == zanNum) {
                tvZan.setVisibility(View.INVISIBLE);
            } else {
                tvZan.setVisibility(View.VISIBLE);
                if (zanNum > 999) {
                    setText(tvZan, "999+");
                } else {
                    setText(tvZan, String.valueOf(zanNum));
                }
            }
        } else {
            mZanIv.setVisibility(View.GONE);
            tvZan.setVisibility(View.GONE);
        }

        /*if (!TextUtils.isEmpty(item.dislikeNo)) {
            stepNum += Integer.parseInt(item.dislikeNo);
            if (0 == stepNum) {
                tvStep.setVisibility(View.INVISIBLE);
            } else {
                tvStep.setVisibility(View.VISIBLE);
                if (stepNum > 999) {
                    setText(tvStep, "999+");
                } else {
                    setText(tvStep, String.valueOf(stepNum));
                }
            }
        } else {
            mStepIv.setVisibility(View.GONE);
            tvStep.setVisibility(View.GONE);
        }*/
        tvStep.setVisibility(View.GONE);
//        setText(mPlayCountView, StringUtils.changePlayTimes(item.playTime));

        mDescView.setText(item.abstractDesc);
        mCreateDateView.setText(convertCreateDate(item));
//        closeVideoBriefViews();
        if (TextUtils.isEmpty(item.abstractDesc)) {
            hideVideoBriefViews();
        } else {
            defaultVideoBriefViews();
        }

        setText(tv_download, "缓存");
        updateDownloadIcon(item.guid);
        setText(tv_collect, "收藏");
        updateCollectIcon(item.guid);
        setText(tv_share, "分享");

        /**
         * 当评论数为0时，不显示数字0
         */
        String replyCount = StringUtils.changeNumberMoreThen10000(item.commentNo);
        if (replyCount.equalsIgnoreCase("0")) {
            replyCount = "";
        }


        mVideoDetailContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateVideoBriefViewsAndSendStatistics(item);
            }
        });

        requestAdvertInfo();

        final WeMedia media = item.weMedia;
        if (media == null || TextUtils.isEmpty(media.id) || TextUtils.isEmpty(media.name)) {
            mMediaContainer.setVisibility(View.GONE);
            return;
        }
        this.media = media;
        if (!User.isLogin()) {
            updateFollowView(false);
        } else {
            mFollowView.setEnabled(false);
            getSubscribeRelation(media.id, new User(IfengApplication.getInstance()).getUid());//获取订阅关系
        }
        mMediaContainer.setVisibility(View.VISIBLE);
        setImageUrl(mUserIconView, media.headPic, R.drawable.avatar_default);
        //updateFollowView();
        setText(mUserNameView, media.name);
        final String mediaId = media.id;

        mUserIconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(media.id) || TextUtils.isEmpty(media.name)) {
                    return;
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA, PageIdConstants.PLAY_VIDEO_V);
                IntentUtils.startWeMediaHomePageActivity(view.getContext(), mediaId, echid);
            }
        });

        mUserNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(media.id) || TextUtils.isEmpty(media.name)) {
                    return;
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_WEMEDIA, PageIdConstants.PLAY_VIDEO_V);
                IntentUtils.startWeMediaHomePageActivity(view.getContext(), mediaId, echid);
            }
        });

    }

    public void updateDownloadIcon(String guid) {
        iv_download.setVisibility(View.VISIBLE);
        boolean isInCache = CacheManager.isInCache(mContext, guid);
        iv_download.setImageResource(isInCache ? R.drawable.video_playerbtn_audio_p : R.drawable.video_playerbtn_audio_n);
        mDownloadView.setEnabled(!isInCache);

    }

    public void updateCollectIcon(String guid) {
        FavoritesModel model = FavoritesDAO.getInstance(mContext).getFavoriteVideo(guid, FavoritesModel.RESOURCE_VIDEO);
        if (model != null) {
            iv_collect.setImageResource(R.drawable.video_player_bottom_collect_selected);
        } else {
            iv_collect.setImageResource(R.drawable.video_player_bottom_collect);
        }
    }


    /**
     * @param item 2014-12-20  16:02:19
     * @return 2014年12月20日发布
     */
    private String convertCreateDate(VideoItem item) {
        if (item == null || TextUtils.isEmpty(item.createDate)) {
            return "";
        }
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(item.createDate);
            return new SimpleDateFormat("yyyy年M月d日", Locale.getDefault()).format(date) + "发布";
        } catch (ParseException e) {
            logger.error("convertCreateDate error! {}", e);
        }
        return "2016年12月30日发布";
    }

    private WeMedia media;

    /**
     * 更新订阅图标
     */
    public void updateFollowView(boolean isSubscribe) {

        setFollowViewStyle(isSubscribe);
        mFollowView.setTag(media);
        mFollowView.setTag(R.id.video_detail_follow, isSubscribe ? SubscribeRelationModel.SUBSCRIBE : SubscribeRelationModel.UN_SUBSCRIBE);
    }

    /**
     * 获取订阅关系
     *
     * @param weMediaId 自媒体id
     * @param uid       用户id
     */
    private void getSubscribeRelation(String weMediaId, String uid) {
        WeMediaDao.getWeMediaRelation(weMediaId, uid, System.currentTimeMillis() + "", SubscribeRelation.class,
                new Response.Listener<SubscribeRelation>() {
                    @Override
                    public void onResponse(SubscribeRelation response) {
                        if (response == null || ListUtils.isEmpty(response.getWeMediaList())) {
                            updateFollowView(false);
                            mFollowView.setEnabled(true);
                            return;
                        }
                        if (response.getWeMediaList().size() > 0) {
                            boolean subscribe = response.getWeMediaList().get(0).getFollowed() == SubscribeRelationModel.SUBSCRIBE;
                            if (subscribe) {//如果已订阅，更新订阅状态
                                updateFollowView(true);
                            } else {
                                updateFollowView(false);//没有订阅
                                if (subscribeClickListener.isClickSubscribeBtn()) {//没有订阅，并且点击了订阅按钮，订阅该自媒体
                                    mFollowView.performClick();
                                    subscribeClickListener.setClickSubscribeBtn(false);
                                }
                            }
                        } else {
                            updateFollowView(false);
                        }
                        mFollowView.setEnabled(true);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        updateFollowView(false);
                        mFollowView.setEnabled(true);
                    }
                });
    }

    /**
     * 设置订阅样式
     *
     * @param isFollowed 是否订阅
     */
    private void setFollowViewStyle(boolean isFollowed) {
        if (isFollowed) {
            mFollowView.setText(R.string.subscribed);
            mFollowView.setTextColor(getContext().getResources().getColor(R.color.home_divider_color_bottom));
            mFollowView.setBackgroundResource(R.drawable.btn_subscribe_shape_gray_border);
        } else {
            mFollowView.setText(R.string.subscribe);
            mFollowView.setTextColor(getContext().getResources().getColor(R.color.add_subscribe_text_color));
            mFollowView.setBackgroundResource(R.drawable.btn_subscribe_shape_border);
        }
    }

    private void updateVideoBriefViewsAndSendStatistics(VideoItem item) {
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_VIDEO_BRIEF, !mCreateDateView.isShown(), PageIdConstants.PLAY_VIDEO_V);

        if (!TextUtils.isEmpty(item.abstractDesc)) {
            if (mCreateDateView.isShown()) {
                closeVideoBriefViews();
            } else {
                openVideoBriefViews(item);
            }
        }
    }

    private void defaultVideoBriefViews() {
        mDescView.setVisibility(VISIBLE);
        mOpenView.setImageResource(R.drawable.btn_open);
        mOpenView.setVisibility(View.VISIBLE);
        mCreateDateView.setVisibility(GONE);
    }

    private void hideVideoBriefViews() {
        mDescView.setVisibility(GONE);
        mOpenView.setVisibility(GONE);
        mCreateDateView.setVisibility(VISIBLE);
    }

    private void closeVideoBriefViews() {
        mOpenView.setImageResource(R.drawable.btn_open);
        mDescView.setVisibility(View.VISIBLE);
        mDescView.setMaxLines(3);
        mCreateDateView.setVisibility(View.GONE);
    }

    private void openVideoBriefViews(VideoItem item) {
        mOpenView.setImageResource(R.drawable.btn_open_up);
        mDescView.setMaxLines(Integer.MAX_VALUE);
        mDescView.setVisibility(TextUtils.isEmpty(item.abstractDesc) ? GONE : VISIBLE);
        mCreateDateView.setVisibility(VISIBLE);
    }

    public void updateRelativeVideoView(List<RelativeVideoInfoItem> dataList) {
        this.videoList = dataList;
        if (ListUtils.isEmpty(dataList)) {
            mMorView.setVisibility(View.GONE);
            return;
        }
        if (mVideoInfoView.isShown()) {
            mErrorView.setVisibility(View.GONE);
        }
        mRelativeVideoContainer.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        mRelativeVideoView.setVisibility(View.VISIBLE);
        int count = dataList.size();
        VideoItem videoItem;
        MainAdInfoModel.AdMaterial adItem;
        YoukuVideoItem youkuVideoItem;
        ADvertItem advertItem;
        //top
        mRecommendTopContainer.removeAllViews();
        for (int i = 0; i < INIT_SHOW_COUNT; i++) {
            if (i >= count) {
                continue;
            }
            boolean isVideo = dataList.get(i).videoInfo != null;
            boolean isAD = dataList.get(i).adbackend != null;
            boolean isYouku = dataList.get(i).youkuInfo != null;
            boolean isAdvert = dataList.get(i).advert != null;

            if (isVideo) {
                videoItem = dataList.get(i).videoInfo;
                View view = LayoutInflater.from(this.getContext()).inflate(R.layout.item_listview_mix_text_picture, mRecommendTopContainer, false);
                if (i == INIT_SHOW_COUNT - 1) {
                    view.findViewById(R.id.divider).setVisibility(View.GONE);
                }
                setVideoItem(videoItem, view);
                CommonStatictisListUtils.getInstance().addVideoDetailFocusList(mActivity, videoItem.itemId, i, PageIdConstants.REFTYPE_EDITOR, echid,
                        0, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.normal);
                mRecommendTopContainer.addView(view);
            }
            if (isAD) {
                adItem = dataList.get(i).adbackend;
                if (TextUtils.isEmpty(adItem.getImageURL())) {
                    View emptyView = LayoutInflater.from(mContext).inflate(R.layout.item_ad_empty_layout, this, false);
                    sendExposure(adItem);
                    mRecommendTopContainer.addView(emptyView);
                } else {
                    AdViewContainer view = new AdViewContainer(getContext());
                    if (i == INIT_SHOW_COUNT - 1) {
                        view.findViewById(R.id.divider).setVisibility(View.GONE);
                    }
                    setADItem(adItem, view);
                    mRecommendTopContainer.addView(view);
                }
            }
            if (isYouku) {
                youkuVideoItem = dataList.get(i).youkuInfo;
                View view = LayoutInflater.from(this.getContext()).inflate(R.layout.youku_mix_text_pic, mRecommendTopContainer, false);
                if (i == INIT_SHOW_COUNT - 1) {
                    view.findViewById(R.id.divider).setVisibility(View.GONE);
                }
                setYoukuVideoItem(youkuVideoItem, view);
                mRecommendTopContainer.addView(view);
            }

            if (isAdvert) {
                advertItem = dataList.get(i).advert;
                AdvertViewContainer view = new AdvertViewContainer(getContext());
                if (i == INIT_SHOW_COUNT - 1) {
                    view.findViewById(R.id.divider).setVisibility(View.GONE);
                }
                setADvertItem(advertItem, view);
                mRecommendTopContainer.addView(view);
            }

        }
        //bottom
        mRecommendBottomContainer.removeAllViews();
        int len = count - INIT_SHOW_COUNT;
        bottomList.clear();
        for (int i = 0; i < len; i++) {
            boolean isVideo = dataList.get(i + INIT_SHOW_COUNT).videoInfo != null;
            boolean isAD = dataList.get(i + INIT_SHOW_COUNT).adbackend != null;
            boolean isAdvert = dataList.get(i + INIT_SHOW_COUNT).advert != null;

            if (isVideo) {
                videoItem = dataList.get(i + INIT_SHOW_COUNT).videoInfo;
                View view = LayoutInflater.from(this.getContext()).inflate(R.layout.item_listview_mix_text_picture, mRecommendBottomContainer, false);
                setVideoItem(videoItem, view);
                mRecommendBottomContainer.addView(view);
                bottomList.add(i, videoItem);
            }
            if (isAD) {
                adItem = dataList.get(i + INIT_SHOW_COUNT).adbackend;
                if (TextUtils.isEmpty(adItem.getImageURL())) {
                    View emptyView = LayoutInflater.from(mContext).inflate(R.layout.item_ad_empty_layout, this, false);
                    sendExposure(adItem);
                    mRecommendBottomContainer.addView(emptyView);
                } else {
                    AdViewContainer view = new AdViewContainer(getContext());
                    setADItem(adItem, view);
                    mRecommendBottomContainer.addView(view);
                }

            }

            if (isAdvert) {
                advertItem = dataList.get(i).advert;
                AdvertViewContainer view = new AdvertViewContainer(getContext());
                setADvertItem(advertItem, view);
                mRecommendBottomContainer.addView(view);
            }

        }
        if (this.videoList.size() <= INIT_SHOW_COUNT) {
            mMorView.setVisibility(View.GONE);
            //  this.findViewById(R.id.video_detail_recommend_below_divider).setVisibility(View.GONE);
        } else {
            mMorView.setVisibility(View.VISIBLE);
            mRecommendBottomContainer.setVisibility(View.GONE);
        }

        //loadAD Banner
        requestAdvertInfo();

        //load relative ad
//        mRelativeAd.getRelativeAdInfo();

    }

    //更新剧集相关列表
    public void updateRelativeDramaVideoInfo(List<RelativeVideoInfoItem> dataList) {
        this.videoList = dataList;
        mRelativeVideoContainer.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRelativeVideoView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        dramaItemOnClick();
        if (currentPlay != -1) {
            recyclerView.scrollToPosition(currentPlay);
        }
        dramaRecyclerAdapter.notifyDataSetChanged();
    }

    public void updateDrameList(VideoItem item) {
        dramaItemCurrentPlayPosition(item);
        dramaRecyclerAdapter.notifyDataSetChanged();
        if (currentPlay != -1) {
            recyclerView.scrollToPosition(currentPlay);
        }

    }

    @Override
    public int getNextFocusDownId() {
        return super.getNextFocusDownId();
    }

    private void dramaItemOnClick() {
        if (!ListUtils.isEmpty(videoList)) {
            itemsIsClick.clear();
            for (int i = 0; i < videoList.size(); i++) {
                itemsIsClick.add(false);
                if (guid.equalsIgnoreCase(videoList.get(i).videoInfo.guid)) {
                    currentPlay = i;
                }
            }
            if (currentPlay != -1) {
                itemsIsClick.set(currentPlay, true);
            }
        }
    }

    private void dramaItemCurrentPlayPosition(VideoItem item) {
        if (item != null && !ListUtils.isEmpty(itemsIsClick)) {
            for (int i = 0; i < itemsIsClick.size(); i++) {
                itemsIsClick.set(i, false);
            }

            for (int i = 0; i < videoList.size(); i++) {
                if (item.equals(videoList.get(i).videoInfo)) {
                    currentPlay = i;
                    itemsIsClick.set(i, true);
                }
            }
        }
    }

    public void showRelativeVideoError() {
        mRelativeVideoContainer.setVisibility(View.GONE);
        resetErrorView();
        mErrorView.setVisibility(View.VISIBLE);
        mErrorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mErrorClickListener != null) {
                    updateErrorView();
                    mErrorClickListener.onRelativeErrorClick();
                }
            }
        });
    }

    public void showVideoError() {
        mVideoInfoView.setVisibility(View.GONE);
        resetErrorView();
        mErrorView.setVisibility(View.VISIBLE);
        mErrorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateErrorView();
                if (mErrorClickListener != null) {
                    mErrorClickListener.onVideoErrorClick();
                }
            }
        });
    }

    private void setADItem(MainAdInfoModel.AdMaterial adMaterial, AdViewContainer view) {
        if (adMaterial == null) {
            return;
        }
        CommonStatictisListUtils.getInstance().sendADInfo(adMaterial.getAdPositionId(), adMaterial.getAdId(), echid);

        mADList.add(adMaterial);
        view.updateView(adMaterial);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdView(view);
            }
        });
    }

    private void sendExposure(MainAdInfoModel.AdMaterial adMaterial) {
        Rect localRect = new Rect();
        boolean localVisibility = getLocalVisibleRect(localRect);
        if (!AdsExposureSesssion.getInstance().containsADItemRecord(adMaterial.getAdId())) {
            if (localVisibility) {
                AdsExposureSesssion.getInstance().addADItemRecord(adMaterial.getAdId());
                AdvertExposureDao.addIfengAdvExposureForChannel(adMaterial.getAdId(), null,
                        adMaterial.getAdAction().getPvurl(), adMaterial.getAdAction().getAdpvurl());
            }
        } else {
            if (!localVisibility) {
                AdsExposureSesssion.getInstance().removeADItemRecord(adMaterial.getAdId());
            }
        }

    }

    private void setADvertItem(ADvertItem item, AdvertViewContainer view) {
        if (item == null) {
            return;
        }

        view.updateView(item);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdvertView(view);
            }
        });
    }

    private void onClickAdvertView(View view) {
        if (view.getTag() == null) {
            return;
        }
        ADvertItem advertItem = (ADvertItem) view.getTag();
        String adId = advertItem.getAppId();
        String title = advertItem.getTitle();
        String imageUrl = advertItem.getImage();

        String adType = "";
        String clickUrl = "";
        String scheme = "";
        if (advertItem != null) {
            if (IfengType.TYPE_AD_IN.equalsIgnoreCase(advertItem.getClickType())) {
                adType = IfengType.TYPE_WEBFULL;
            } else {
                adType = advertItem.getClickType();
            }
            clickUrl = advertItem.getClickUrl();

            // 如果是广告app下载类型，点击地址取loadingUrl
            if ("adapp".equalsIgnoreCase(advertItem.getClickType())) {
                clickUrl = advertItem.getAppUrl();
            }

            scheme = advertItem.getAppScheme();
        }

        ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, clickUrl, null, scheme, view.getContext(), null, "", "", null);

    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
    }

    private void setVideoItem(final VideoItem videoItem, View view) {

        TextView titleView = (TextView) view.findViewById(R.id.tv_left_title);
        TextView durationView = (TextView) view.findViewById(R.id.tv_duration);
        NetworkImageView picView = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
        TextView mediaNameView = (TextView) view.findViewById(R.id.tv_author);
        TextView playCountView = (TextView) view.findViewById(R.id.tv_play_times);
        NetworkImageView avatarView = (NetworkImageView) view.findViewById(R.id.niv_left_emoji);

        setText(titleView, videoItem.title);
        setText(durationView, StringUtils.changeDuration(videoItem.duration));
        setText(playCountView, StringUtils.changePlayTimes(videoItem.playTime));

        setImageUrl(picView, videoItem.image, R.drawable.bg_default_small);

        WeMedia media = videoItem.weMedia;
        if (media == null) {
            mediaNameView.setVisibility(View.GONE);
            avatarView.setVisibility(View.GONE);
            return;
        }
        mediaNameView.setVisibility(VISIBLE);
        avatarView.setVisibility(View.GONE);
        setText(mediaNameView, media.name);
//        setImageUrl(avatarView, media.headPic, R.drawable.avatar_default);
        CommonStatictisListUtils.getInstance().sendYoukuConstatic(videoItem, CommonStatictisListUtils.YK_EXPOSURE, CommonStatictisListUtils.YK_FEED_DETAIL);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextColor(videoItem);

                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(videoItem);
                }
            }
        });

    }

    private void setYoukuVideoItem(final YoukuVideoItem videoItem, View view) {
        TextView titleView = (TextView) view.findViewById(R.id.tv_left_title);
        NetworkImageView avatarView = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
        setText(titleView, videoItem.getTitle());
        setImageUrl(avatarView, videoItem.getImage(), R.drawable.bg_default_small);

        String yvId = videoItem.getYvId() == null ? "" : videoItem.getYvId();
        CommonStatictisListUtils.getInstance().sendYoukuConstatic(yvId, CommonStatictisListUtils.YK_EXPOSURE, CommonStatictisListUtils.YK_FEED_DETAIL);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtils.startAppByScheme(getContext(), videoItem.getAPPscheme());

                String yvId = videoItem.getYvId() == null ? "" : videoItem.getYvId();
                CommonStatictisListUtils.getInstance().sendYoukuConstatic(yvId, CommonStatictisListUtils.YK_OPEN_YOULU_APP, CommonStatictisListUtils.YK_FEED_DETAIL);
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_detail_recommend_show_more:
                if (mRecommendBottomContainer.getVisibility() == View.GONE) {
                    if (this.videoList != null && this.videoList.size() > INIT_SHOW_COUNT) {
                        showLastDivider(mRecommendTopContainer);
                        mRecommendBottomContainer.setVisibility(View.VISIBLE);
                        hideLastDivider(mRecommendBottomContainer);
                        for (int i = 0; i < bottomList.size(); i++) {
                            if (null != bottomList.get(i)) {
                                CommonStatictisListUtils.getInstance().addVideoDetailFocusList(mActivity, bottomList.get(i).itemId, 10 + i, PageIdConstants.REFTYPE_EDITOR, echid,
                                        0, CommonStatictisListUtils.RECYVLERVIEW_TYPE, CommonStatictisListUtils.normal);
                            }
                        }
                        bottomList.clear();
                        // this.findViewById(R.id.video_detail_recommend_below_divider).setVisibility(View.GONE);
                    } else {
                        hideLastDivider(mRecommendTopContainer);
                        //this.findViewById(R.id.video_detail_recommend_below_divider).setVisibility(View.GONE);
                    }
                    mMorView.setVisibility(View.GONE);
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_PLAY_ITEM_MORE, PageIdConstants.PLAY_VIDEO_V);
                }
                break;
        }
    }

    private void hideLastDivider(ViewGroup view) {
        int lastIndex = view.getChildCount() - 1;
        View child = view.getChildAt(lastIndex);
        if (child != null) {
            child.findViewById(R.id.divider).setVisibility(View.GONE);
        }
    }

    private void showLastDivider(ViewGroup view) {
        int lastIndex = view.getChildCount() - 1;
        view.getChildAt(lastIndex).findViewById(R.id.divider).setVisibility(View.VISIBLE);
    }


    private void setText(TextView view, String text) {
        view.setText(TextUtils.isEmpty(text) ? "" : text);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(VideoItem videoItem);
    }

    private OnSeeAllClickListener mSeeAllClickListener;

    public void setOnSeeAllClickListener(OnSeeAllClickListener listener) {
        this.mSeeAllClickListener = listener;
    }

    public interface OnSeeAllClickListener {
        void onSeeAll();

        void onDownload();
    }

    private OnSharePopwindow mOnShareVideoListener;

    public void setOnShareVideoItem(OnSharePopwindow listener) {
        this.mOnShareVideoListener = listener;
    }

    public interface OnSharePopwindow {
        void onShareVideoItem();
    }

    public void changeTextColor(VideoItem videoItem) {
        int position = getIndex(videoItem);
        if (position < 0 || position >= videoList.size()) {
            return;
        }
        resetTextColor();
        View view;
        boolean isBottomItem = mRecommendTopContainer.getChildCount() < position + 1;
        if (isBottomItem) {
            view = mRecommendBottomContainer.getChildAt(
                    position - mRecommendTopContainer.getChildCount());
        } else {
            view = mRecommendTopContainer.getChildAt(position);
        }
        TextView titleView = (TextView) view.findViewById(R.id.tv_left_title);
        titleView.setTextColor(Color.RED);
    }

    private int getIndex(VideoItem videoItem) {
        if (videoItem == null || TextUtils.isEmpty(videoItem.guid) || ListUtils.isEmpty(videoList)) {
            return -1;
        }
        int size = videoList.size();
        for (int i = 0; i < size; i++) {
            if (videoList.get(i) == null || videoList.get(i).videoInfo == null
                    || TextUtils.isEmpty(videoList.get(i).videoInfo.guid)) {
                continue;
            }
            if (videoItem.guid.equals(videoList.get(i).videoInfo.guid)) {
                return i;
            }
        }
        return -1;
    }

    private void resetTextColor() {
        for (int i = 0; i < mRecommendTopContainer.getChildCount(); i++) {
            View view = mRecommendTopContainer.getChildAt(i);
            View titleView = view.findViewById(R.id.tv_left_title);
            if (titleView != null && titleView instanceof TextView) {
                ((TextView) titleView).setTextColor(Color.parseColor("#262626"));
            }

        }

        for (int i = 0; i < mRecommendBottomContainer.getChildCount(); i++) {
            View view = mRecommendBottomContainer.getChildAt(i);
            View titleView = view.findViewById(R.id.tv_left_title);
            if (titleView != null && titleView instanceof TextView) {
                ((TextView) titleView).setTextColor(Color.parseColor("#262626"));
            }
        }
    }

    private onErrorListener mErrorClickListener;

    public void setOnErrorListener(onErrorListener listener) {
        this.mErrorClickListener = listener;

    }

    private OnCommentClickListener mOnCommentClickListener;

    public void setOnCommentListener(OnCommentClickListener l) {
        this.mOnCommentClickListener = l;
    }

    public interface OnCommentClickListener {
        void onCommentListener();
    }

    public interface onErrorListener {
        void onVideoErrorClick();

        void onRelativeErrorClick();
    }


    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        IfengApplication.getInstance().registerReceiver(mLoginReceiver, filter);
    }

    private void unregisterLoginReceiver() {
        try {
            IfengApplication.getInstance().unregisterReceiver(mLoginReceiver);
        } catch (Exception e) {
        }
    }

    /**
     * 登陆广播接收器
     */
    private final BroadcastReceiver mLoginReceiver = new LoginReceiver(this);

    static class LoginReceiver extends BroadcastReceiver {
        WeakReference<DetailVideoHeadView> weakReference;

        LoginReceiver(DetailVideoHeadView view) {
            weakReference = new WeakReference<>(view);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }

            if (intent.getAction().equals(IntentKey.LOGINBROADCAST)) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING) {
                    view.mFollowView.setEnabled(false);
                    //登录成功之后获取订阅关系
                    if (view.media != null) {
                        view.getSubscribeRelation(view.media.id, User.getUid());
                    }
                    view.subscribeClickListener.setUserClicked(false);
                } else {
                    view.subscribeClickListener.setClickSubscribeBtn(false);
                }
            }
        }
    }

    private void requestAdvertInfo() {
        if (!isStartAdvert) {
            isStartAdvert = true;
            mVideoInfoAdManager.getHeadAdInfo();
        }

    }

    public void setEchid(String echid) {
        this.echid = echid;
    }

    public void setUIPlayContext(UIPlayContext context) {
        this.mPlayContext = context;
    }

    public void setActivity(ActivityVideoPlayerDetail activity) {
        this.mActivity = activity;
    }


    private void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
    }

//    public void setShareParam(String guid, String title, String imgUrl) {
//        if (mShareClickListener == null) {
//            mShareClickListener = new VideoDetailShareClickListener();
//        }
//        mShareClickListener.setShareParm(guid, title, imgUrl);
//    }


    private View mRelativeAdvertView;
    private int adPosition = -1;
    private MainAdInfoModel.AdMaterial mRelativeAdModle;

    public void loadAdvertLayout(final MainAdInfoModel.AdMaterial mode) {
        mRelativeAdModle = mode;
        if (mode == null) {
            return;
        }
        MainAdInfoModel.AdMaterial.AdConditions conditions = mode.getAdConditions();
        int index;
        try {
            index = Integer.parseInt(conditions.getIndex());
        } catch (Exception e) {
            return;
        }
        if (videoList != null && index > videoList.size()) {
            return;
        }
        adPosition = index;

        if (mRelativeAdvertView == null) {
            mRelativeAdvertView = LayoutInflater.from(this.getContext()).inflate(R.layout.ad_mix_text_pic, mRecommendTopContainer, false);
        }
        TextView title = (TextView) mRelativeAdvertView.findViewById(R.id.tv_left_title);

        TextView tagView = (TextView) mRelativeAdvertView.findViewById(R.id.tv_ad_tag);
        mRelativeAdvertView.findViewById(R.id.tv_ad_download).setVisibility(View.GONE);
        NetworkImageView right_pic = (NetworkImageView) mRelativeAdvertView.findViewById(R.id.niv_right_picture);
        MainAdInfoModel.AdMaterial.Icon icon = mode.getIcon();
        String tag = "";
        if (icon != null) {
            tag = icon.getText();
        }
        if (TextUtils.isEmpty(tag)) {
            tag = " 广告";
        }
        tagView.setVisibility(View.VISIBLE);
        tagView.setText(tag);
        String imgUrl = mode.getImageURL();
        title.setText(mode.getText());
        right_pic.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        if (index < INIT_SHOW_COUNT) {
            mRecommendTopContainer.addView(mRelativeAdvertView, index);
        } else {
            mRecommendBottomContainer.addView(mRelativeAdvertView, index);
        }

        //add click
        mRelativeAdvertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mode == null) {
                    return;
                }
                String adId = mode.getAdId();
                String title = mode.getText();
                String imageUrl = mode.getImageURL();

                String adType = "";
                String clickUrl = "";
                List<String> downloadCompletedUrls = null;
                MainAdInfoModel.AdMaterial.AdAction adAction = mode.getAdAction();
                if (adAction != null) {
                    if (!IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType())) {
                        adType = IfengType.TYPE_WEBFULL;
                    }

                    clickUrl = adAction.getUrl();

                    // 如果是广告app下载类型，点击地址取loadingUrl
                    if (mode.getAdConditions() != null && "app".equalsIgnoreCase(mode.getAdConditions().getShowType())) {
                        clickUrl = adAction.getLoadingurl();
                    }

                    if (!ListUtils.isEmpty(adAction.getAsync_downloadCompletedurl())) {
                        downloadCompletedUrls.addAll(adAction.getAsync_downloadCompletedurl());
                    }
                    if (!ListUtils.isEmpty(adAction.getDownloadCompletedurl())) {
                        downloadCompletedUrls.addAll(adAction.getDownloadCompletedurl());
                    }

                    //广告点击曝光
                    AdvertExposureDao.sendAdvertClickReq(adId, (ArrayList<String>) adAction.getAsync_click());
                }

                ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, clickUrl, null, null, v.getContext(), downloadCompletedUrls, "", "", adAction != null ? adAction.getAsync_download() : null);
            }

        });
    }


    /**
     * 上报广告曝光统计
     */
    public void sendHeadExpose() {
        if (!ListUtils.isEmpty(mADList)) {
            for (MainAdInfoModel.AdMaterial item : mADList) {
                if (null != item.getCheckForExposureListener()) {
                    item.getCheckForExposureListener().onViewScroll();
                }
            }
        }

        if (mAdMaterial != null && mAdMaterial.getAdAction() != null) {
            mAdViewContainer.sendExposure(mAdMaterial);
        }
    }

    public void deleteADId() {
        if (null != mAdMaterial && !TextUtils.isEmpty(mAdMaterial.getAdId())) {
            AdsExposureSesssion.getInstance().remove(mAdMaterial.getAdId());
        }
    }

    public View getDownloadView() {
        return mDownloadView;
    }

    public ImageView getCaiView() {
        return mStepIv;
    }

    public View getCollectView() {
        return mCollectView;
    }

    public View getShareView() {
        return mShareView;
    }


    /**
     * 上报广告曝光统计
     */
//    public void sendRelativeExpose(int realContent, int top) {
//        if (hasRelativeSend || adPosition == -1 || mRelativeAdvertView == null || mRelativeAdvertView.getVisibility() == View.GONE ) {
//            return;
//        }
//
//        int scrollDistance = realContent + Math.abs(top);
//        int relativeTop = mRelativeVideoContainer.getTop();
//        int viewTop = mRelativeAdvertView.getTop();
//        View parentView = (View) mRelativeAdvertView.getParent();
//        int parentTop = parentView.getTop();
//        int height = relativeTop + parentTop + viewTop;
//
//        if (scrollDistance < height) {
//            return;
//        }
//        if (mRelativeAdModle != null && mRelativeAdModle.getAdAction() != null) {
//            hasRelativeSend = true;
//            AdvertExposureDao.sendAdvertClickReq(mRelativeAdModle.getAdId(), (ArrayList<String>) mRelativeAdModle.getAdAction().getPvurl());
//        }
//    }
}
