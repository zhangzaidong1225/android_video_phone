package com.ifeng.newvideo.videoplayer.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.CacheVideoModel;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 缓存播放器页面
 * Created by Administrator on 2016/8/28.
 */
public class ActivityCacheVideoPlayer extends BaseFragmentActivity implements AudioService.CanPlayAudio {
    private static final Logger logger = LoggerFactory.getLogger(ActivityCacheVideoPlayer.class);

    /**
     * 视频播放相关的
     */
    private NormalVideoHelper mPlayerHelper;
    private VideoSkin mVideoSkin;
    private UIPlayContext mPlayContext;

    private VideoItem mCurrentVideoItem;//当前播放实体
    private List<VideoItem> videoItemList = new ArrayList<>();//播放列表

    private boolean isAudio;//是否是音频

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVideoSkin = new VideoSkin(this, VideoSkin.SKIN_TYPE_LOCAL);
        mVideoSkin.setId(R.id.video_skin);
        mVideoSkin.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mVideoSkin.setContentDescription(getString(R.string.auto_test_content_des_local_player));
        setContentView(mVideoSkin);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);

        boolean isFromFloatBarOrNotify = getIntent().getExtras().getBoolean(AudioService.EXTRA_FROM, false);

        ArrayList<CacheVideoModel> cacheVideoModels = (ArrayList<CacheVideoModel>) getIntent().getSerializableExtra(IntentKey.CACHE_VIDEO_MODELS);

        CacheVideoModel cacheVideoModel = (CacheVideoModel) getIntent().getExtras().get(IntentKey.CACHE_VIDEO_MODEL);

        if (cacheVideoModel != null && IfengType.TYPE_AUDIO.equals(cacheVideoModel.getType())) {
            isAudio = true;
        }

        if (isFromFloatBarOrNotify) {
            isAudio = true;
        }

        if (!ListUtils.isEmpty(cacheVideoModels)) {
            initVideoItem(cacheVideoModel);
            initVideoItemList(cacheVideoModels);
        }

        initSkin();

        if (isFromFloatBarOrNotify) {
            mCurrentVideoItem = PlayQueue.getInstance().getCurrentVideoItem();
            videoItemList = PlayQueue.getInstance().getPlayList();
            playVideo();
            return;
        }

        playVideo();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPlayerHelper.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPlayerHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayerHelper.onDestroy();
    }

    /**
     * 初始化皮肤
     */
    private void initSkin() {
        mPlayContext = new UIPlayContext();
        mPlayContext.VideoItemList = videoItemList;
        mPlayContext.skinType = VideoSkin.SKIN_TYPE_LOCAL;
        mPlayContext.isFromCache = true;
        mPlayerHelper = new NormalVideoHelper(isAudio ? NormalVideoHelper.CONTROLLER_TYPE_AUDIO : NormalVideoHelper.CONTROLLER_TYPE_VIDEO);
        mVideoSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener() {
            @Override
            public void onLoadFailedListener() {
                playVideo();
            }
        });
        mPlayerHelper.setOnAutoPlayListener(new IPlayController.OnAutoPlayListener() {
            @Override
            public void onPlayListener(VideoItem item) {
                mPlayContext.videoItem = item;
                mPlayContext.title = item.name;
                mVideoSkin.getLoadView().updateText();
            }
        });
        mPlayerHelper.init(mVideoSkin, mPlayContext);
    }

    /**
     * 播放
     */
    private void playVideo() {
        if (mCurrentVideoItem == null) {
            mPlayerHelper.openVideo("");
            return;
        }
        logger.debug("mCurrentVideoItem={}", mCurrentVideoItem.toString());
        mPlayContext.videoItem = mCurrentVideoItem;
        mPlayContext.title = mCurrentVideoItem.name;
        mPlayContext.VideoItemList = videoItemList;

        mVideoSkin.getLoadView().updateText();

        String url;
        if (mPlayerHelper.isPlayAudio()) {
            PlayQueue.getInstance().setVideoItem(mCurrentVideoItem);
            PlayQueue.getInstance().setPlayList(videoItemList);
            url = StreamUtils.getMp3Url(mCurrentVideoItem.videoFiles);
        } else {
            url = StreamUtils.getMediaUrl(mCurrentVideoItem.videoFiles);
        }
        mPlayerHelper.openVideo(url);
    }

    private void initVideoItem(CacheVideoModel model) {
        mCurrentVideoItem = convert2VideoItem(model);
    }

    private VideoItem convert2VideoItem(CacheVideoModel model) {
        VideoItem videoItem = new VideoItem();
        videoItem.guid = model.getGuid();
        videoItem.duration = model.getDuration();
        videoItem.image = model.getImgUrl();
        videoItem.name = model.getName();
        videoItem.title = model.getName();
        PlayerInfoModel playerInfo = (PlayerInfoModel) model.getPlayerInfo();
        if (playerInfo != null) {
            videoItem.searchPath = playerInfo.getSearchPath();
            videoItem.cpName = playerInfo.getCpName();
            videoItem.weMedia = new WeMedia();
            videoItem.weMedia.id = playerInfo.getWmId();
            videoItem.weMedia.name = playerInfo.getWmName();
            videoItem.itemId = playerInfo.getColumnName();//如果ItemId为空的话，缓存记录看过，从看过进入时是不能播放的，因为programId为空
        }
        videoItem.videoFiles = getFileTypes(model);
        return videoItem;
    }


    private List<FileType> getFileTypes(CacheVideoModel model) {
        List<FileType> videoFiles = new ArrayList<>();

        FileType fileTypeAudio = new FileType();//音频类型
        if (isAudio) {
            fileTypeAudio.useType = "mp3";
            fileTypeAudio.mediaUrl = model.getPath();
        } else {
            FileType fileTypeVideo = new FileType();//视频类型
            fileTypeVideo.useType = "mp4350k";//该值得取值去StreamUtils--STREAM_BYTE中查看,可以任意指定,但必须是其中一种类型
            fileTypeVideo.mediaUrl = model.getPath();

            fileTypeAudio.useType = "mp3";//音频播放地址
            try {
                fileTypeAudio.mediaUrl = CacheManager.getAudioPath(this, model.getGuid());
            } catch (Exception e) {
                e.printStackTrace();
                fileTypeAudio.mediaUrl = "";
            }
            videoFiles.add(fileTypeVideo);
        }
        videoFiles.add(fileTypeAudio);
        return videoFiles;
    }

    private void initVideoItemList(ArrayList<CacheVideoModel> list) {
        for (CacheVideoModel model : list) {
            if (TextUtils.isEmpty(model.getGuid())) {
                continue;
            }
            videoItemList.add(convert2VideoItem(model));
        }
    }

    private boolean isKeyDown = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isKeyDown = true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!isKeyDown) {
            return true;
        }
        isKeyDown = false;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public boolean isPlayAudio() {
        return mPlayerHelper != null && mPlayerHelper.isPlayAudio();
    }
}
