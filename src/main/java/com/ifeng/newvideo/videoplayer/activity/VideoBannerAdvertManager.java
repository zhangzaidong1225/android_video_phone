package com.ifeng.newvideo.videoplayer.activity;

import android.text.TextUtils;
import com.alibaba.fastjson.JSONArray;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.ADInfoDAO;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.db.model.VideoAdConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by fanshell on 2016/8/25.
 */
public class VideoBannerAdvertManager {

    private static final Logger logger = LoggerFactory.getLogger(VideoBannerAdvertManager.class);
    public static final String TAG = VideoBannerAdvertManager.class.getName();
    private SharePreUtils mSharePreferenceUtils;
    private String adID;

    public VideoBannerAdvertManager() {
        mSharePreferenceUtils = SharePreUtils.getInstance();
    }

    private boolean checkLocalAdDataValidate() {
        String videoInfo = mSharePreferenceUtils.getVideoHeadInfo();

        if (!TextUtils.isEmpty(videoInfo)) {
            Gson gson = new Gson();
            MainAdInfoModel mainAdInfoModel = gson.fromJson(videoInfo, MainAdInfoModel.class);
            long currentTime = System.currentTimeMillis();
            if (currentTime - mainAdInfoModel.getCreate_time() < mainAdInfoModel.getCacheTime() * 1000) {
                if (mOnVideoHeaderListener != null) {
                    int index = mSharePreferenceUtils.getVideoHeadInfoIndex();
                    index = (index + 1) % (mainAdInfoModel.getAdMaterials().size());
                    mSharePreferenceUtils.setVideoHeadInfoIndex(index);
                    mOnVideoHeaderListener.onVideoBannerAdvertSuccess(mainAdInfoModel, index);
                    return true;
                }
            }
        }

        return false;
    }

    public void getHeadAdInfo() {
        //check validate
        boolean isValidate = checkLocalAdDataValidate();
        if (isValidate) {
            return;
        }

        String url = getBannerRequestUrl();
        if (TextUtils.isEmpty(url)) {
            if (mOnVideoHeaderListener != null) {
                mOnVideoHeaderListener.onVideoBannerAdvertFailed();
            }
            return;
        }

        //得到广告
        ADInfoDAO.getBodyAd(url,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        handleResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mOnVideoHeaderListener.onVideoBannerAdvertFailed();
                    }
                }, TAG);

    }

    private void handleResponse(Object response) {
        if (response == null || TextUtils.isEmpty(response.toString())) {
            if (mOnVideoHeaderListener != null) {
                mOnVideoHeaderListener.onVideoBannerAdvertFailed();
            }
            return;
        }

        try {
            List<MainAdInfoModel> mainAdInfoModels = JSONArray.parseArray(response.toString(), MainAdInfoModel.class);
            if (ListUtils.isEmpty(mainAdInfoModels)) {
                if (mOnVideoHeaderListener != null) {
                    mOnVideoHeaderListener.onVideoBannerAdvertFailed();
                }
                return;
            }

            boolean isFound = false;
            MainAdInfoModel findModel = null;
            for (MainAdInfoModel adInfoModel : mainAdInfoModels) {
                MainAdInfoModel model = AdTools.filterVideoBannerADModel(adInfoModel);
                if (adID.trim().equals(model.getAdPositionId()) && AdTools.isValidAdModel(model)) {
                    model.setCreate_time(System.currentTimeMillis());//手动赋值用以判断是否过期
                    Gson gson = new Gson();
                    String json = gson.toJson(model);
                    mSharePreferenceUtils.setVideoHeadInfo(json);
                    mSharePreferenceUtils.setVideoHeadInfoIndex(0);
                    isFound = true;
                    findModel = model;
                    break;
                }
            }

            if (isFound) {
                mOnVideoHeaderListener.onVideoBannerAdvertSuccess(findModel, 0);
            } else {
                mOnVideoHeaderListener.onVideoBannerAdvertFailed();
            }
        } catch (Exception e) {
            if (mOnVideoHeaderListener != null) {
                mOnVideoHeaderListener.onVideoBannerAdvertFailed();
            }
        }
    }

    private OnVideoBannerAdvertListener mOnVideoHeaderListener;

    public void setOnVideoInfoHeaderSuccess(OnVideoBannerAdvertListener listener) {
        this.mOnVideoHeaderListener = listener;
    }


    private String getBannerRequestUrl() {
        if (VideoAdConfigDao.adConfig == null ||
                (VideoAdConfigDao.adConfig.getInfoAD() == null) ||
                ListUtils.isEmpty(VideoAdConfigDao.adConfig.getInfoAD().getBanner())) {
            return "";
        }
        String adHost = VideoAdConfigDao.adConfig.getInfoAD().getAdHost();
        return getUrl(adHost);
    }

    private String getMoreRequestUrl() {
        if (VideoAdConfigDao.adConfig == null || VideoAdConfigDao.adConfig.getInfoAD() == null) {
            return "";
        }
        VideoAdConfigModel.InfoADBean infoAD = VideoAdConfigDao.adConfig.getInfoAD();
        String adHost = infoAD.getAdMoreUrl();
        return getUrl(adHost);

    }

    private String getUrl(String host) {
        adID = VideoAdConfigDao.adConfig.getInfoAD().getBanner().get(0).getAdId();
        if (TextUtils.isEmpty(adID)) {
            return "";
        }
        StringBuilder params = new StringBuilder();
        params.append(host);
        params.append("?adids=");
        params.append(adID);
        params.append(',');
        AdTools.appendParams(params, IfengApplication.getAppContext());
        return params.toString();
    }

    public interface OnVideoBannerAdvertListener {
        void onVideoBannerAdvertSuccess(MainAdInfoModel model, int index);

        void onVideoBannerAdvertFailed();

        void onVideoBannerAdvertMoreSuccess(MainAdInfoModel model);

        void onVideoBannerAdvertMoreADFailed();
    }


    /**
     * 请求余量广告
     */
    public void requestADMore(MainAdInfoModel model, int index) {
        adID = model.getAdPositionId();
        String url = getMoreRequestUrl();
        if (TextUtils.isEmpty(url)) {
            if (mOnVideoHeaderListener != null) {
                mOnVideoHeaderListener.onVideoBannerAdvertFailed();
            }
            return;
        }

        ADInfoDAO.getBodyAd(url, new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        handleMoreResponse(response);
                    }
                },
                new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mOnVideoHeaderListener != null) {
                            mOnVideoHeaderListener.onVideoBannerAdvertFailed();
                        }
                    }

                }, TAG);
    }

    private void handleMoreResponse(Object response) {
        if (response == null || TextUtils.isEmpty(response.toString())) {
            mOnVideoHeaderListener.onVideoBannerAdvertMoreADFailed();
            return;
        }
        try {
            List<MainAdInfoModel> mainAdInfoModels = JSONArray.parseArray(response.toString(), MainAdInfoModel.class);
            for (MainAdInfoModel adInfoModel : mainAdInfoModels) {
                MainAdInfoModel model = AdTools.filterVideoBannerADModel(adInfoModel);
                if (AdTools.isValidAdModel(model) && !TextUtils.isEmpty(model.getAdPositionId())
                        && model.getAdPositionId().equals(adID)) {
                    mOnVideoHeaderListener.onVideoBannerAdvertMoreSuccess(model);
                    break;
                }
            }
        } catch (Exception e) {
            mOnVideoHeaderListener.onVideoBannerAdvertMoreADFailed();
        }
    }

}
