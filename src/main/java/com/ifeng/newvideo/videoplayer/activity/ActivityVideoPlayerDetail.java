package com.ifeng.newvideo.videoplayer.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.activity.CacheDialogFragment;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.dialogUI.CommentEditFragment;
import com.ifeng.newvideo.dialogUI.DanmakuCommentFragment;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.smart.SmartStatistic;
import com.ifeng.newvideo.statistics.smart.domains.UserOperator;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.ui.subscribe.WeMediaDialogFragment;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.activity.widget.DetailVideoHeadView;
import com.ifeng.newvideo.videoplayer.activity.widget.LoadingLayout;
import com.ifeng.newvideo.videoplayer.adapter.VideoPlayDetailAdapter;
import com.ifeng.newvideo.videoplayer.bean.DanmuItem;
import com.ifeng.newvideo.videoplayer.bean.RelativeDramaVideoInfo;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInfoItem;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInformation;
import com.ifeng.newvideo.videoplayer.bean.VideoDanmuItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.dao.VideoDao;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.VideoAdvertManager;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.DanmuEditView;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayButton;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.DefaultControllerView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.ChannelConstants;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.model.CommentInfoModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 点播视频播放页
 */
public class ActivityVideoPlayerDetail extends BaseFragmentActivity implements NotifyShareCallback, AudioService.CanPlayAudio,
        DefaultControllerView.OnShowOrHideDanmuView, DanmuEditView.OnClickEditButton, PlayButton.OnPlayOrPauseListener {
    private static final Logger logger = LoggerFactory.getLogger(ActivityVideoPlayerDetail.class);

    private String guid;
    private VideoItem nextVideoItem;
    private String chid;
    private String weMediaId;

    private LoadingLayout mLoadingLayout;
    private MyPullToRefreshListView mPullToRefreshListView;
    private VideoPlayDetailAdapter mAdapter;
    /**
     * 头信息，相关视频
     */
    private DetailVideoHeadView mHeaderView;

    /**
     * 视频播放相关的
     */
    private NormalVideoHelper mPlayerHelper;
    private VideoSkin mVideoSkin;
    private UIPlayContext mPlayContext;

    private boolean isLoadVideoInformationError = false;
    private boolean isLoadRelativeVideoError = false;

    /**
     * 底部相关的View
     */
    private RelativeLayout rl_bottom;
    private LinearLayout ll_bottom;
    private LinearLayout ll_bottom_view;
    private View mCommentView;
    private ImageView commentIcon;
    private TextView commentNum;
    private View mCommentInputView;

    private OneKeyShare mOnekeyShare;


    private CommentEditFragment mCommentEditFragment;
    private VideoItem mCurrentVideoItem;
    private DanmakuCommentFragment mDanmakuEditFragment;

    /**
     * 评论相关
     */

    private int mPageNum = 1;
    private volatile boolean isRefresh = false;
    private static final String PAGE_SIZE = DataInterface.PAGESIZE_10;
    private String echid;
    private boolean isFromCache;

    private boolean isFromHistory;
    private long bookMark;//看过位置

    private boolean anchor;

    // 精选传入分享数据，需记录最初的videoItem
    private String homeTitle = null;
    private String homeImg = null;
    // 首页中的guid
    private String homeGuid = null;
    private String homeWeMediaId = null;

    //是否来自悬浮或通知
    private boolean isFromFloatBarOrNotify;

    private int mPlayHeight;
    private int mHeightWithoutPlayAndBottom;

    private WeMediaDialogFragment dialogFragment;
    private CacheDialogFragment cacheDialogFragment;
    private RelativeLayout rl_up;


    private int mHeight;
    private boolean isEditDanma = false;
    private boolean isCanPost;

    private List<String> keyList = new ArrayList<>();
    private List<VideoDanmuItem> danmuItems = new ArrayList<>();
    private List<Integer> tmpList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(null);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_detail_layout);
        mPlayHeight = DisplayUtils.getWindowWidth() * 9 / 16
                + DisplayUtils.getStatusBarHeight() + DisplayUtils.convertDipToPixel(55);
        mHeightWithoutPlayAndBottom = DisplayUtils.getWindowHeight()
                - DisplayUtils.getWindowWidth() * 9 / 16
                - DisplayUtils.getStatusBarHeight()
                - DisplayUtils.convertDipToPixel(44);
        if (!checkAndInitIntent(getIntent())) {
            this.finish();
            return;
        }
        if (!isFromFloatBarOrNotify) {
            if (ActivityMainTab.mAudioService != null) {
                ActivityMainTab.mAudioService.stopAudioService(VideoSkin.SKIN_TYPE_SPECIAL);
            }

        }
        mOnekeyShare = new OneKeyShare(this);
        enableExitWithSlip(false);
        initLoadingView();
        initHeadView();
        initSkin();
        initBottomView();
        mHeight = DisplayUtils.getWindowHeight();
        handleNotificationFinishReceiver(true);
        mAdapter = new VideoPlayDetailAdapter(this) {
            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                if (anchor) {
                    anchor = false;
                    int headCount = mPullToRefreshListView.getRefreshableView().getHeaderViewsCount();
                    mPullToRefreshListView.getRefreshableView().setSelection(headCount);

                    mPullToRefreshListView.getRefreshableView().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mAdapter != null && mAdapter.isShowEmptyView()) {
                                sendCommentAction();
                                if (mAdapter.getData() != null && mAdapter.getData().isAllowComment()) {
                                    showEditCommentWindow("", "");
                                }
                            }
                        }
                    }, 800);

                }
            }
        };
        mAdapter.setOnLoadFailClick(new VideoPlayDetailAdapter.OnLoadFailedClick() {
            @Override
            public void onLoadFailedClick() {
                loadComment(false);
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_RETRY, PageIdConstants.PLAY_VIDEO_V);
            }
        });
        mPullToRefreshListView.setAdapter(mAdapter);
        mPullToRefreshListView.setShowIndicator(false);
//        MobileFlowUtils.getInstance(this).checkMobileFlow();
        if (NetUtils.isNetAvailable(IfengApplication.getInstance())) {
            loadData();
        }
        mFocusList = new ArrayList<>(20);
    }

    private void setVideoSkinListener() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getBaseControlView().setShowListener(this);
            initDanmuViewStatus();
        }
        if (null != mVideoSkin.getPlayView()) {
            mVideoSkin.getPlayView().setPlayOrPauseListener(this);
        }

        if (null != mVideoSkin.getDanmuEditView()) {
            mVideoSkin.getDanmuEditView().setEditListener(this);
        }
    }

    private void initDanmuViewStatus() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().showView();
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            mVideoSkin.getDanmuView().initDanmukuView();
            mVideoSkin.getDanmuView().showDanmukuView();
        }

    }

    @Override
    protected void onScreenOff() {
        if (mPlayerHelper != null) {
            mPlayerHelper.sendScreenOffStatistics();
        }
    }

    protected boolean isExit(MotionEvent ev) {
        if (isLandScape()) {
            return false;
        }

        if (ev != null && ev.getY() > mPlayHeight) {
            return true;
        }
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!checkAndInitIntent(intent)) {
            this.finish();
            return;
        }
        mPlayContext.mOpenFirst = 1;
    }

    private boolean checkAndInitIntent(Intent intent) {
        //Fix intent is null
        if (intent == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return false;
        }

        isFromFloatBarOrNotify = intent.getBooleanExtra(AudioService.EXTRA_FROM, false);
        if (isFromFloatBarOrNotify) {
            VideoItem item = PlayQueue.getInstance().getCurrentVideoItem();
            if (item == null) {
                return false;
            }
            guid = PlayQueue.getInstance().getCurrentVideoItem().guid;
            weMediaId = PlayQueue.getInstance().getCurrentVideoItem().weMedia.id;
            echid = PlayQueue.getInstance().getEchid();
            return true;
        }
        anchor = intent.getBooleanExtra(IntentKey.ANCHOR, false);
        guid = extras.getString(IntentKey.VOD_GUID);

        homeGuid = guid;
        homeTitle = extras.getString(IntentKey.HOME_VIDEO_TITLE);
        homeImg = extras.getString(IntentKey.HOME_VIDEO_IMG);

        echid = extras.getString(IntentKey.E_CHID);
        isFromCache = extras.getBoolean(IntentKey.IS_FROM_CACHE, false);
        isFromHistory = extras.getBoolean(IntentKey.IS_FROM_HISTORY, false);
        bookMark = extras.getLong(IntentKey.HISTORY_BOOK_MARK, 0);
        //isShowDanma = extras.getBoolean(IntentKey.IS_SHOW_DANMA, true);
        isEditDanma = extras.getBoolean(IntentKey.IS_SHOW_EDIT_DANMU, false);
        logger.debug("isFromHistory={},bookMark={}, showEditDanma={}", isFromHistory, bookMark, isEditDanma);
        Log.d("danmu", isFromHistory + "--" + bookMark + "--");
        if (TextUtils.isEmpty(guid)) {
            return false;
        }
        PlayQueue.getInstance().setEchid(echid);
        PlayQueue.getInstance().setGuid(guid);
        return true;
    }


    private void initLoadingView() {
        mLoadingLayout = (LoadingLayout) findViewById(R.id.video_detail_loading_layout);
        if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            mLoadingLayout.setShowStatus(LoadingLayout.STATUS_ERROR);
        }

        mLoadingLayout.setLoadDataListener(new LoadingLayout.OnLoadDataListener() {
            @Override
            public void onLoadData() {
                loadData();
            }
        });
        mPullToRefreshListView = mLoadingLayout.getRefreshView();
        //锚定到评论区
        mPullToRefreshListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            @Override
            public void onLastItemVisible() {
                if (mAdapter.isShowEmptyView()) {
                    return;
                }
                if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                loadComment(true);
            }
        });
        rl_up = (RelativeLayout) findViewById(R.id.rl_up);
        rl_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPullToRefreshListView.getRefreshableView().setSelection(0);
                rl_up.setVisibility(View.GONE);
            }
        });
    }

    private void initSkin() {
        mVideoSkin = (VideoSkin) findViewById(R.id.video_skin);
        mPlayContext = new UIPlayContext();
        mPlayContext.skinType = VideoSkin.SKIN_TYPE_VOD;
        mPlayContext.mFromForAd = VideoAdvertManager.FROM_WEMEDIA;
        mPlayContext.channelId = echid;
        mPlayContext.isFromPush = getIntent() != null && getIntent().getBooleanExtra(IntentKey.IS_FROM_PUSH, false);
        mPlayContext.isFromCache = isFromCache;
        mPlayContext.mOpenFirst = 1;
        if (isFromFloatBarOrNotify) {
            mPlayerHelper = new NormalVideoHelper(NormalVideoHelper.CONTROLLER_TYPE_AUDIO);
        } else {
            mPlayerHelper = new NormalVideoHelper();
        }

        mPlayerHelper.setFromHistory(isFromHistory);
        mPlayerHelper.setOnPlayCompleteListener(new IPlayController.OnPlayCompleteListener() {
            @Override
            public void onPlayComplete() {
                if (!mCurrentVideoItem.isColumn()) {
                    if (!mPlayContext.isAdvert) {
                        if (null != nextVideoItem && !TextUtils.isEmpty(nextVideoItem.guid)) {
                            logger.debug("playComplete:next:gid:{},next:title:{}", nextVideoItem.guid, nextVideoItem.title);
                            playVideo(nextVideoItem);
                            loadRelativeVideo(nextVideoItem.guid, true);
                        }
                    } else {
                        if (null != mCurrentVideoItem && !TextUtils.isEmpty(mCurrentVideoItem.guid)) {
                            logger.debug("playComplete:mCurrentVideoIten:gid:{},mCurrentVideoIten:title:{}", mCurrentVideoItem.guid, mCurrentVideoItem.title);
                            mPlayContext.VideoItemList.add(0, mCurrentVideoItem);
                            // 走自动播放逻辑
                        }
                    }
                    isEditDanma = false;
                }

            }
        });

        mPlayerHelper.setOnAutoPlayListener(new IPlayController.OnAutoPlayListener() {
            @Override
            public void onPlayListener(VideoItem item) {
                logger.debug("playComplete:mCurrentVideoIten:gid:{},mCurrentVideoIten:title:{}", mCurrentVideoItem.guid, mCurrentVideoItem.title);
                logger.debug("playComplete:mCurrentVideoIten:gid:{},item:title:{}", item.guid, item.title);

                mCurrentVideoItem = resetVideoItemIfNecessary(item);
                mPlayContext.videoItem = item;
                mPlayContext.title = TextUtils.isEmpty(item.title) ? item.name : item.title;
//                    mCollectionView.setTag(mCurrentVideoItem);
                mHeaderView.getCollectView().setTag(mCurrentVideoItem);
                mHeaderView.getDownloadView().setTag(mCurrentVideoItem);
//                    mAdapter.setVideoItem(mCurrentVideoItem);
                updateCommentIcon(item.commentNo);
//                    updateCollectIcon(mCurrentVideoItem.guid);
                mHeaderView.updateCollectIcon(mCurrentVideoItem.guid);
                mHeaderView.updateDownloadIcon(mCurrentVideoItem.guid);
                if (mHeaderView == null) {
                    initHeadView();
                }
                if (!item.isColumn()) {
                    mHeaderView.changeTextColor(mCurrentVideoItem);
                }

                destoryAndCreateDanmuView();

                mHeaderView.updateVideoInformation(mCurrentVideoItem, mPlayContext.isRelate);
                if (item.isColumn()) {
                    mHeaderView.updateDrameList(item);
                }
                //初始化分享统计数据
                String searchPath = mCurrentVideoItem.searchPath;
                String weMediaId = mCurrentVideoItem.weMedia != null ? mCurrentVideoItem.weMedia.id : "";
                mOnekeyShare.initShareStatisticsData(mCurrentVideoItem.guid, mPlayContext.isRelate ? "" : echid, searchPath, weMediaId, PageIdConstants.PLAY_VIDEO_V);
                mOnekeyShare.initSmartShareData(UserOperatorConst.TYPE_VIDEO, mCurrentVideoItem.weMedia.name, mCurrentVideoItem.title);
                chid = mCurrentVideoItem.searchPath;

                loadComment(false);
                requestVideoDanmuData(mCurrentVideoItem.guid);
//                requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
                requesetDanmaAllowSend(mCurrentVideoItem.guid);
                if (!mPlayContext.isAdvert) {
                    if (NetUtils.isMobile(ActivityVideoPlayerDetail.this) && IfengApplication.mobileNetCanPlay) {
//                        if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {}
                        ToastUtils.getInstance()
                                .showShortToast(R.string.play_moblie_net_hint);

                    }
                }

            }
        });

        mPlayerHelper.init(mVideoSkin, mPlayContext);
        mVideoSkin.setNoNetWorkListener(new VideoSkin.OnNetWorkChangeListener()

        {
            @Override
            public void onNoNetWorkClick() {
                if (mCurrentVideoItem == null) {
                    loadData();
                } else {
                    playVideo(mCurrentVideoItem);
                }
                pauseAndHideDanmuView();
                String page = isLandScape() ? PageIdConstants.PLAY_VIDEO_H : PageIdConstants.PLAY_VIDEO_V;
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_RETRY, null, page);
            }

            @Override
            public void onMobileClick() {
                if (mCurrentVideoItem == null) {
                    loadData();
                } else {
                    playVideo(mCurrentVideoItem);
                }
                pauseAndHideDanmuView();
                String page = isLandScape() ? PageIdConstants.PLAY_VIDEO_H : PageIdConstants.PLAY_VIDEO_V;
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_CONTINUE, null, page);
            }
        });

        mVideoSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener()

        {
            @Override
            public void onLoadFailedListener() {
                if (mCurrentVideoItem == null) {
                    loadData();
                } else {
                    playVideo(mCurrentVideoItem);
                }
                pauseAndHideDanmuView();
                String page = isLandScape() ? PageIdConstants.PLAY_VIDEO_H : PageIdConstants.PLAY_VIDEO_V;
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_RETRY, null, page);
            }
        });
        setVideoSkinListener();
        setControlViewImage();
    }

    private void pauseAndHideDanmuView() {
        if (null != mVideoSkin.getDanmuView()) {
            if (null == mVideoSkin.getDanmuView().getDanmakuView()) {
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
            }
            mVideoSkin.getDanmuView().getDanmakuView().pause();
            mVideoSkin.getDanmuView().getDanmakuView().hide();
        }
    }

    private void setControlViewImage() {
        if (IfengApplication.danmaSwitchStatus) {
            Log.d("danmu", "open");
            mVideoSkin.getDanmuView().currentVideoPosition((int) (bookMark / 1000));
        } else {
            Log.d("danmu", "closed");
            mVideoSkin.getBaseControlView().setShowControlView(false);
            mVideoSkin.getDanmuView().hideView();
            mVideoSkin.getDanmuView().hideDanmakuView();
            mVideoSkin.getDanmuEditView().hideView();
            mVideoSkin.getDanmuView().setShowEditView(false);
            mVideoSkin.getDanmuView().currentVideoPosition((int) (bookMark / 1000));
        }
    }

    private void destoryAndCreateDanmuView() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().clearDanmaku();
            mVideoSkin.getDanmuView().onDestory();

            mVideoSkin.getDanmuView().showView();
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getDanmuView().showDanmukuView();
            } else {
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
            }
        }
    }

    private void initBottomView() {
        rl_bottom = (RelativeLayout) findViewById(R.id.rl_bottom);
        ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);
        mCommentInputView = findViewById(R.id.comment_input);
        mCommentInputView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetUtils.isNetAvailable(view.getContext())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (mCurrentVideoItem == null) {
                    return;
                }
                showEditCommentWindow("", "");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_INPUT, PageIdConstants.PLAY_VIDEO_V);
            }
        });

        ll_bottom_view = (LinearLayout) findViewById(R.id.ll_bottom_view);
        mCommentView = findViewById(R.id.video_detail_bottom_comment);
        commentIcon = (ImageView) mCommentView.findViewById(R.id.video_detail_bottom_comment_icon);
        commentNum = (TextView) mCommentView.findViewById(R.id.tv_comment_number);

        mCommentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int headCount = mPullToRefreshListView.getRefreshableView().getHeaderViewsCount();
                mPullToRefreshListView.getRefreshableView().setSelection(headCount);

                if (mAdapter != null) {
                    boolean isAllowComment = mAdapter.getData() != null && mAdapter.getData().isAllowComment();
                    if (mAdapter.isShowEmptyView() && isAllowComment) {
                        showEditCommentWindow("", "");
                    }
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_ICON, PageIdConstants.PLAY_VIDEO_V);

            }
        });
//        updateDownloadIcon(guid);
        //下载接口
//        mCommentView.setOnClickListener(new VideoDetailDownloadClickListener(ActivityVideoPlayerDetail.this, IfengType.TYPE_VIDEO));

        //收藏
/*        mCollectionView = findViewById(R.id.video_detail_bottom_collect);
        mCollectionIcon = (ImageView) mCollectionView.findViewById(R.id.video_detail_bottom_collect_icon);
        updateCollectIcon(guid);
        mCollectionView.setOnClickListener(new VideoDetailCollectionClickListener(FavoritesModel.RESOURCE_VIDEO, echid, mPlayContext));
        //分享
        mShareView = findViewById(R.id.video_detail_bottom_share);
        mShareView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetUtils.isNetAvailable(view.getContext())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (mCurrentVideoItem != null && !TextUtils.isEmpty(mCurrentVideoItem.mUrl)) {
                    showSharePop();
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE, PageIdConstants.PLAY_VIDEO_V);
            }
        });*/

    }

    /**
     * 评论智能统计
     */
    public void sendCommentAction() {
        UserOperator comment = new UserOperator(this);
        comment.setOperation(UserOperatorConst.OPERATION_COMMENT);
        comment.setType(UserOperatorConst.TYPE_VIDEO);
        if (mCurrentVideoItem != null) {
            comment.setTitle(mCurrentVideoItem.title);
            WeMedia weMedia = mCurrentVideoItem.weMedia;
            if (weMedia != null) {
                String name = mCurrentVideoItem.weMedia.name;
                if (!TextUtils.isEmpty(name)) {
                    comment.setKeyword(name);
                }

            }
        }
        //上报评论
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.OPERATION_URL, comment);
    }

//    private void updateCollectIcon(String guid) {
//        FavoritesModel model = FavoritesDAO.getInstance(this.getApplication()).getFavoriteVideo(guid, FavoritesModel.RESOURCE_VIDEO);
//        if (model != null) {
//            mCollectionIcon.setImageResource(R.drawable.video_player_bottom_collect_selected);
//        } else {
//            mCollectionIcon.setImageResource(R.drawable.video_player_bottom_collect);
//        }
//    }

    /*    private void updateDownloadIcon(String guid) {
            boolean isInCache = CacheManager.isInCache(ActivityVideoPlayerDetail.this, guid);
            commentIcon.setImageResource(isInCache ? R.drawable.video_playerbtn_audio_p : R.drawable.video_playerbtn_audio_n);
            mCommentView.setEnabled(!isInCache);
        }*/
    private void updateCommentIcon(String commentNo) {
        if (commentNo != null) {
            int number = Integer.parseInt(commentNo);
            commentNum.setVisibility(View.VISIBLE);
            if (0 == number) {
                commentNum.setVisibility(View.GONE);
            } else if (number > 999) {
                commentNum.setText("999+");
            } else {
                commentNum.setText(commentNo);
            }
        }
    }

    private boolean isFinishBookMark;

    private void playVideo(VideoItem mCurrentVideoItem) {
        if (mCurrentVideoItem == null) {
            //error
            mPlayerHelper.openVideo("");
            return;
        }
        if (NetUtils.isMobile(this) && IfengApplication.mobileNetCanPlay) {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }

        String title = mCurrentVideoItem.title;
        if (TextUtils.isEmpty(title)) {
            title = mCurrentVideoItem.name;
        }
        mPlayContext.title = title;
        mVideoSkin.getLoadView().updateText();
        mPlayContext.videoItem = mCurrentVideoItem;
        if (!ListUtils.isEmpty(mCurrentVideoItem.videoFiles)) {
            mPlayContext.streamType = StreamUtils.getStreamType();
            if (!isFinishBookMark) {
                mPlayerHelper.setBookMark(bookMark);
                isFinishBookMark = true;
            }
            String realPath = "";
            if (mPlayerHelper.isPlayAudio()) {
                //更新音频的数据
                PlayQueue.getInstance().setVideoItem(mCurrentVideoItem);
                realPath = StreamUtils.getMp3Url(mCurrentVideoItem.videoFiles);
            } else {
                realPath = StreamUtils.getMediaUrl(mCurrentVideoItem.videoFiles);
            }
            mPlayerHelper.openVideo(realPath);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPlayerHelper != null && !isEditDanma) {
            mPlayerHelper.onResume();
        }

        if (null != mVideoSkin && null != mVideoSkin.getDanmuView() && !isEditDanma) {
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getBaseControlView().setShowControlView(true);
            } else {
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }
            mVideoSkin.getDanmuView().onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPlayerHelper != null) {
            mPlayerHelper.onPause();
        }

        if (null != mVideoSkin && null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sendStaticList();

        handleNotificationFinishReceiver(false);
        VideoDao.cancelAll();
        CommentDao.cancelAll();
        VolleyHelper.getRequestQueue().cancelAll(VideoBannerAdvertManager.TAG);
        if (mAdapter != null) {
            mAdapter.setOnLoadFailClick(null);
        }
        if (mVideoSkin != null) {
            mVideoSkin.setOnLoadFailedListener(null);
            mVideoSkin.setNoNetWorkListener(null);
        }
        if (mLoadingLayout != null) {
            mLoadingLayout.setLoadDataListener(null);
        }
        mHeaderView = null;
        mAdapter = null;
        if (mPlayerHelper != null) {
            mPlayerHelper.setOnAutoPlayListener(null);
            mPlayerHelper.setOnPlayCompleteListener(null);
            mPlayerHelper.onDestroy();
            mPlayerHelper = null;
        }
        if (null != mVideoSkin && null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().onDestory();
        }

    }

    private void sendStaticList() {
        mFocusList = CommonStatictisListUtils.videoDetailFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(mFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.videoDetailFocusList.clear();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.common_slide_right_in, R.anim.common_slide_right_out);
    }


    private void loadData() {
        mLoadingLayout.setShowStatus(LoadingLayout.STATUS_LOADING);
        if (isFromFloatBarOrNotify) {
            //memory data
            updateVideoInformation(PlayQueue.getInstance().getCurrentVideoItem());
            updateRelativeVideoInfo(getLocalRelativeVideos(), false);
        } else {
            loadVideoInfo();
//            loadRelativeVideo();
//            loadRelativeDramaVideo();
        }
    }

    private RelativeVideoInformation getLocalRelativeVideos() {
        RelativeVideoInformation localVideoInformation = new RelativeVideoInformation();
        localVideoInformation.guidRelativeVideoInfo = new ArrayList<>();

        try {
            List<VideoItem> playList = PlayQueue.getInstance().getPlayList();
            List<RelativeVideoInfoItem> videoInfoItems = new ArrayList<>();
            RelativeVideoInfoItem videoInfoItem;
            for (VideoItem item : playList) {
                videoInfoItem = new RelativeVideoInfoItem();
                videoInfoItem.videoInfo = item;
                videoInfoItems.add(videoInfoItem);
            }
            localVideoInformation.guidRelativeVideoInfo.addAll(videoInfoItems);
        } catch (Exception e) {
            logger.error("getLocalRelativeVideos error !!", e);
        }
        return localVideoInformation;
    }

    /**
     * 加载视频信息
     */
    private void loadVideoInfo() {
        isLoadVideoInformationError = false;
        VideoDao.getVideoInformationById(guid, "",
                new Response.Listener<VideoItem>() {
                    @Override
                    public void onResponse(VideoItem response) {
                        updateVideoInformation(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLoadVideoInformationError = true;

                        if (isLoadRelativeVideoError || isLoadVideoInformationError) {
                            mLoadingLayout.setShowStatus(LoadingLayout.STATUS_ERROR);
                        } else {
                            mAdapter.showErrorView();
                            mHeaderView.showVideoError();
                        }
                        playVideo(mCurrentVideoItem);
                    }
                });
    }

    private void updateVideoInformation(VideoItem item) {
        mLoadingLayout.setShowStatus(LoadingLayout.STATUS_NORMAL);
//        mCollectionView.setTag(item);
        mHeaderView.getCollectView().setTag(item);
        mHeaderView.getDownloadView().setTag(item);
        mCurrentVideoItem = resetVideoItemIfNecessary(item);

        //初始化分享统计数据
        if (mCurrentVideoItem != null) {
            String searchPath = mCurrentVideoItem.searchPath;
            String weMediaId = mCurrentVideoItem.weMedia != null ? mCurrentVideoItem.weMedia.id : "";
            String weMediaName = mCurrentVideoItem.weMedia != null ? mCurrentVideoItem.weMedia.name : "";
            mOnekeyShare.initShareStatisticsData(guid, mPlayContext.isRelate ? "" : echid, searchPath, weMediaId, PageIdConstants.PLAY_VIDEO_V);
            mOnekeyShare.initSmartShareData(UserOperatorConst.TYPE_VIDEO, weMediaName, mCurrentVideoItem.title);
        }
        weMediaId = item.weMedia.id;
        mHeaderView.updateVideoInformation(item, mPlayContext.isRelate);

        playVideo(mCurrentVideoItem);
        if (mCurrentVideoItem != null) {
            if (!item.isColumn()) {
                mHeaderView.changeTextColor(mCurrentVideoItem);
            }
            updateCommentIcon(mCurrentVideoItem.commentNo);
        }
        if (!item.isColumn()) {
            loadRelativeVideo(guid, false);
            setIsDramaScroll(false);
        } else {
            loadRelativeDramaVideo(guid, false);
            setIsDramaScroll(true);
        }

        loadComment(false);
        requestVideoDanmuData(guid);
//        requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
        requesetDanmaAllowSend(guid);
        CommonStatictisListUtils.getInstance().sendYoukuConstatic(item, CommonStatictisListUtils.YK_PLAY, CommonStatictisListUtils.YK_FEED_DETAIL);

    }

    /**
     * 保持首页和播放页标题、图片一致
     */
    private VideoItem resetVideoItemIfNecessary(VideoItem item) {
        if (item != null && item.guid != null && item.guid.equals(homeGuid)) {
            if (!TextUtils.isEmpty(homeTitle)) {
                item.title = homeTitle;
            }
            if (!TextUtils.isEmpty(homeImg)) {
                item.image = homeImg;
            }
        }
        return item;
    }

    /**
     * 加载相关视频信息
     */
    private void loadRelativeVideo(String guid, final boolean isComplete) {
        isLoadRelativeVideoError = false;
        sendStaticList();
        VideoDao.getRelativeVideoById(guid, new Response.Listener<RelativeVideoInformation>() {
                    @Override
                    public void onResponse(RelativeVideoInformation response) {
                        updateRelativeVideoInfo(response, isComplete);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLoadRelativeVideoError = true;
                        if (isLoadVideoInformationError) {
                            mLoadingLayout.setShowStatus(LoadingLayout.STATUS_ERROR);
                        } else {
                            mHeaderView.showRelativeVideoError();
                        }
                    }
                });
    }

    private void loadRelativeDramaVideo(String guid, final boolean isPlayComplete) {
        isLoadRelativeVideoError = false;
        VideoDao.getRelativeDramaVideoById(guid,
                weMediaId,
                "",
                "",
                DataInterface.PAGESIZE_20,
                ChannelConstants.DEFAULT,
                new Response.Listener<RelativeDramaVideoInfo>() {
                    @Override
                    public void onResponse(RelativeDramaVideoInfo response) {
                        updateRelativeDramaVideoInfo(response, isPlayComplete);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    }

    private void updateRelativeVideoInfo(RelativeVideoInformation response, boolean isPlayComplete) {
        isLoadRelativeVideoError = false;
        List<VideoItem> list = new ArrayList<>();
        if (!ListUtils.isEmpty(response.guidRelativeVideoInfo)) {
            for (RelativeVideoInfoItem videoItem : response.guidRelativeVideoInfo) {
                VideoItem item = videoItem.videoInfo;
                if (item == null ||
                        TextUtils.isEmpty(item.guid) ||
                        TextUtils.isEmpty(item.name) ||
                        ListUtils.isEmpty(item.videoFiles)) {
                    continue;
                }
                item = resetVideoItemIfNecessary(item);
                if (!list.contains(item)) {
                    list.add(item);
                }
            }
        }
        if (isPlayComplete) {
            updatePlayCompleteVideoInfo(nextVideoItem);
        }
        if (!ListUtils.isEmpty(list)) {
            nextVideoItem = list.get(0);
        }
        mHeaderView.updateRelativeVideoView(response.guidRelativeVideoInfo);
        mLoadingLayout.setShowStatus(LoadingLayout.STATUS_NORMAL);
        if (mCurrentVideoItem != null) {
            mHeaderView.changeTextColor(mCurrentVideoItem);
        }
        mPlayContext.VideoItemList = list;
    }

    private void updateRelativeDramaVideoInfo(RelativeDramaVideoInfo response, boolean isPlayComplete) {
        isLoadRelativeVideoError = false;
        List<VideoItem> list = new ArrayList<>();
        if (!ListUtils.isEmpty(response.columnVideoInfo)) {
            for (RelativeVideoInfoItem videoItem : response.columnVideoInfo) {
                VideoItem item = videoItem.videoInfo;
                if (item == null ||
                        TextUtils.isEmpty(item.guid) ||
                        TextUtils.isEmpty(item.name) ||
                        ListUtils.isEmpty(item.videoFiles)) {
                    continue;
                }
                item = resetVideoItemIfNecessary(item);
                list.add(item);
            }
            if (isPlayComplete) {
                updatePlayCompleteVideoInfo(nextVideoItem);
            }

            if (!ListUtils.isEmpty(list)) {
                nextVideoItem = list.get(1);
            }
            mHeaderView.updateRelativeDramaVideoInfo(response.columnVideoInfo);
            mLoadingLayout.setShowStatus(LoadingLayout.STATUS_NORMAL);
            mPlayContext.VideoItemList = list;
        }
    }

    private void updatePlayCompleteVideoInfo(VideoItem item) {
        mCurrentVideoItem = resetVideoItemIfNecessary(item);
//        mCollectionView.setTag(mCurrentVideoItem);
        mHeaderView.getCollectView().setTag(mCurrentVideoItem);
        mHeaderView.getDownloadView().setTag(mCurrentVideoItem);
        mAdapter.setVideoItem(mCurrentVideoItem);
        if (mCurrentVideoItem != null) {
            mPlayContext.isRelate = true;
            mPlayContext.isFromPush = false;
            guid = mCurrentVideoItem.guid;
            chid = mCurrentVideoItem.searchPath;
            weMediaId = mCurrentVideoItem.weMedia.id;
//            updateCollectIcon(mCurrentVideoItem.guid);
            mHeaderView.setUIPlayContext(mPlayContext);
            mHeaderView.updateCollectIcon(mCurrentVideoItem.guid);
            updateCommentIcon(mCurrentVideoItem.commentNo);
            mHeaderView.updateDownloadIcon(mCurrentVideoItem.guid);
            mPlayerHelper.setBookMark(0);//点击下一个视频从头开始播放
            mPlayerHelper.setFromHistory(false);//重置状态

//            playVideo(mCurrentVideoItem);
            loadComment(false);

            //初始化分享统计数据
            String searchPath = mCurrentVideoItem.searchPath;
            String weMediaId = mCurrentVideoItem.weMedia != null ? mCurrentVideoItem.weMedia.id : "";
            mOnekeyShare.initShareStatisticsData(guid, mPlayContext.isRelate ? "" : echid, searchPath, weMediaId, PageIdConstants.PLAY_VIDEO_V);
            mOnekeyShare.initSmartShareData(UserOperatorConst.TYPE_VIDEO, mCurrentVideoItem.weMedia.name, mCurrentVideoItem.title);

        }
        mHeaderView.updateVideoInformation(mCurrentVideoItem, mPlayContext.isRelate);
    }


    /**
     * 初始化View
     */
    private void initHeadView() {
        mHeaderView = new DetailVideoHeadView(this);
        mHeaderView.setEchid(echid);
        mHeaderView.setUIPlayContext(mPlayContext);
        mHeaderView.setActivity(this);
//        mHeaderView.setShareParam(homeGuid, homeTitle, homeImg);
        mPullToRefreshListView.getRefreshableView().addHeaderView(mHeaderView, null, false);
        mHeaderView.setOnItemClickListener(new DetailVideoHeadView.OnItemClickListener() {
            @Override
            public void onItemClick(VideoItem item) {
                destoryAndCreateDanmuView();
                playCurrentVideoItem(item);
                requestVideoDanmuData(guid);
//                requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
                requesetDanmaAllowSend(guid);
                loadRelativeVideo(guid, false);
                CommonStatictisListUtils.getInstance().sendYoukuConstatic(item, CommonStatictisListUtils.YK_PLAY, CommonStatictisListUtils.YK_FEED_DETAIL);
                isEditDanma = false;
            }
        });

        mHeaderView.setOnClickRecyclerItem(new DetailVideoHeadView.OnClickRecyclerItem() {
            @Override
            public void onClickRecyclerItem(VideoItem item) {
                playCurrentVideoItem(item);
                CommonStatictisListUtils.getInstance().sendYoukuConstatic(item, CommonStatictisListUtils.YK_PLAY, CommonStatictisListUtils.YK_FEED_DETAIL);
            }
        });

        mHeaderView.setOnErrorListener(new DetailVideoHeadView.onErrorListener() {
            @Override
            public void onVideoErrorClick() {
                loadVideoInfo();
            }

            @Override
            public void onRelativeErrorClick() {
                loadRelativeVideo(guid, false);
            }
        });

/*        mHeaderView.setOnCommentListener(new DetailVideoHeadView.OnCommentClickListener() {
            @Override
            public void onCommentListener() {
                int headCount = mPullToRefreshListView.getRefreshableView().getHeaderViewsCount();
                mPullToRefreshListView.getRefreshableView().setSelection(headCount);

                if (mAdapter != null) {
                    boolean isAllowComment = mAdapter.getData() != null && mAdapter.getData().isAllowComment();
                    if (mAdapter.isShowEmptyView() && isAllowComment) {
                        showEditCommentWindow("");
                    }
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_ICON, PageIdConstants.PLAY_VIDEO_V);
            }
        });*/
        mHeaderView.setOnSeeAllClickListener(new DetailVideoHeadView.OnSeeAllClickListener() {
            @Override
            public void onSeeAll() {
                dialogFragment = WeMediaDialogFragment.newInstance(guid, weMediaId);
                //剧集列表
                dialogFragment.setOnClickProgramContent(new WeMediaDialogFragment.OnClickProgramContent() {
                    @Override
                    public void transferGuid(String videoId) {
                        guid = videoId;
                        loadVideoInfo();
                    }
                });

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                dialogFragment.setCancelable(false);
                dialogFragment.show(ft, "WeMediaDialog");

            }

            @Override
            public void onDownload() {
                cacheDialogFragment = CacheDialogFragment.newInstance(guid, weMediaId);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                cacheDialogFragment.setCancelable(false);
                cacheDialogFragment.show(ft, "CacheDialog");
            }
        });

        mHeaderView.setOnShareVideoItem(new DetailVideoHeadView.OnSharePopwindow() {
            @Override
            public void onShareVideoItem() {
                if (!NetUtils.isNetAvailable(getApplicationContext())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (mCurrentVideoItem != null && !TextUtils.isEmpty(mCurrentVideoItem.mUrl)) {
                    showSharePop();
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE, PageIdConstants.PLAY_VIDEO_V);
            }
        });

        mPullToRefreshListView.getRefreshableView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mHeaderView != null) {
                    mHeaderView.sendHeadExpose();
//                    mHeaderView.sendRelativeExpose(mHeightWithoutPlayAndBottom, mHeaderView.getTop());
                }

                int scrollHeight = getScrollHeight(view, firstVisibleItem);
                if (scrollHeight >= 0 && scrollHeight <= mHeight * 2) {
                    rl_up.setVisibility(View.GONE);
                } else if (scrollHeight > mHeight * 2) {
                    if (!ScreenUtils.isLand()) {
                        rl_up.setVisibility(View.VISIBLE);
                        rl_up.bringToFront();
                    } else {
                        rl_up.setVisibility(View.GONE);
                    }
                }

            }
        });
    }

    private void playCurrentVideoItem(VideoItem item) {
        if (mCurrentVideoItem != null && !TextUtils.isEmpty(mCurrentVideoItem.guid)
                && mCurrentVideoItem.guid.equals(item.guid)) {
            return;
        }
        mCurrentVideoItem = resetVideoItemIfNecessary(item);
//        mCollectionView.setTag(mCurrentVideoItem);
        mHeaderView.getCollectView().setTag(mCurrentVideoItem);
        mHeaderView.getDownloadView().setTag(mCurrentVideoItem);
        mAdapter.setVideoItem(mCurrentVideoItem);
        mPullToRefreshListView.getRefreshableView().setSelection(0);
        if (mCurrentVideoItem != null) {
            mPlayContext.isRelate = true;
            mPlayContext.isFromPush = false;
            guid = mCurrentVideoItem.guid;
            chid = mCurrentVideoItem.searchPath;
            weMediaId = mCurrentVideoItem.weMedia.id;
//            updateCollectIcon(mCurrentVideoItem.guid);
            mHeaderView.setUIPlayContext(mPlayContext);
            mHeaderView.updateCollectIcon(mCurrentVideoItem.guid);
            updateCommentIcon(mCurrentVideoItem.commentNo);
            mHeaderView.setUIPlayContext(mPlayContext);
            mHeaderView.updateDownloadIcon(mCurrentVideoItem.guid);
            mPlayerHelper.setBookMark(0);//点击下一个视频从头开始播放
            mPlayerHelper.setFromHistory(false);//重置状态

            playVideo(mCurrentVideoItem);
            loadComment(false);

            //初始化分享统计数据
            String searchPath = mCurrentVideoItem.searchPath;
            String weMediaId = mCurrentVideoItem.weMedia != null ? mCurrentVideoItem.weMedia.id : "";
            mOnekeyShare.initShareStatisticsData(guid, mPlayContext.isRelate ? "" : echid, searchPath, weMediaId, PageIdConstants.PLAY_VIDEO_V);
            mOnekeyShare.initSmartShareData(UserOperatorConst.TYPE_VIDEO, mCurrentVideoItem.weMedia.name, mCurrentVideoItem.title);

        }
        mHeaderView.updateVideoInformation(mCurrentVideoItem, mPlayContext.isRelate);
    }


    /**
     * 用于显示评论对话框
     *
     * @param comments 评论
     */
    public void showEditCommentWindow(String comments, String comment_id) {
        if (mCommentEditFragment == null) {
            mCommentEditFragment = new CommentEditFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putCharSequence(IntentKey.E_CHID, echid);
        if (mCommentEditFragment.getArguments() != null) {
            mCommentEditFragment.getArguments().putCharSequence(IntentKey.E_CHID, echid);
        } else {
            mCommentEditFragment.setArguments(bundle);
        }
        if (mAdapter != null && mAdapter.isShowEmptyView()) {
            mCommentEditFragment.setShowInputMethod(true);
        } else {
            mCommentEditFragment.setShowInputMethod(true);
        }
        mCommentEditFragment.setCommends(comments, comment_id);
        mCommentEditFragment.setVideoItem(mCurrentVideoItem);
        if (!mCommentEditFragment.isAdded() && !this.isFinishing()) {
            mCommentEditFragment.show(getSupportFragmentManager(), "dialog");

        }
        mPlayerHelper.setProgrammeAutoNext(false);

    }

    private synchronized void loadComment(final boolean mIsLoadMore) {
        if (isRefresh) {
            return;
        }
        isRefresh = true;
        mPullToRefreshListView.showFootView();
        if (mCurrentVideoItem == null) {
            mPullToRefreshListView.hideFootView();
            mPullToRefreshListView.onRefreshComplete();
            //加延迟的是为让用户看到加载的View
            mPullToRefreshListView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAdapter.showErrorView();
                }
            }, 500);
            isRefresh = false;
            return;
        }
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.DISABLED);
        if (mIsLoadMore) {
            mAdapter.setVideoItem(mCurrentVideoItem);
        } else {
            mPageNum = 1;
        }
        logger.debug("edit loadComment mPageNum = {}", mPageNum);

        String paramsBuilder = "?docUrl=" + mCurrentVideoItem.guid +
                "&p=" + mPageNum +
                "&pagesize=" + PAGE_SIZE +
                "&type=new" +
                "&job=1";

        CommentDao.getComments(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                mPullToRefreshListView.hideFootView();
                mPullToRefreshListView.onRefreshComplete();

                boolean isCommentEmpty = response == null
                        || ((CommentInfoModel) response).getComments() == null
                        || ListUtils.isEmpty(((CommentInfoModel) response).getComments().getNewest());


                boolean isAllowComment = response != null && ((CommentInfoModel) response).isAllowComment();

                mCommentInputView.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                mCommentView.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                mHeaderView.getCaiView().setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                if (!ScreenUtils.isLand()) {
                    ll_bottom.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                    rl_bottom.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                } else {
                    ll_bottom.setVisibility(View.GONE);
                    rl_bottom.setVisibility(View.GONE);
                }

                if (!isAllowComment || isCommentEmpty) {
                    handleCommentEmpty(mIsLoadMore);
                    isRefresh = false;
                    return;
                }

                handleCommentData((CommentInfoModel) response, mIsLoadMore);
                isRefresh = false;
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mPageNum == 1) {
                    mAdapter.showErrorView();
                } else {
                    if (!NetUtils.isNetAvailable(ActivityVideoPlayerDetail.this)) {
                        ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    } else {
                        ToastUtils.getInstance().showShortToast(R.string.common_load_data_error);
                    }
                }

                mPullToRefreshListView.hideFootView();
                mPullToRefreshListView.onRefreshComplete();
                isRefresh = false;

            }
        }, paramsBuilder);

        if (mIsLoadMore) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.PLAY_VIDEO_V);
        }
    }

    private synchronized void handleCommentData(CommentInfoModel commentInfoModel, boolean mIsLoadMore) {
        // CommentInfoModel.Comments comments = commentInfoModel.getComments();
        // List<CommentInfoModel.Newest> newestList = comments.getNewest();
        if (!mIsLoadMore) {
            mAdapter.setData(commentInfoModel);
        } else {
            mAdapter.addData(commentInfoModel);
        }
        mPageNum++;
    }

    private void handleCommentEmpty(boolean isLoadMore) {
        if (!isLoadMore) {
            setEmptyView();
        } else {
            noMore();
        }
    }

    private void setEmptyView() {
        mPullToRefreshListView.hideFootView();
        mPullToRefreshListView.onRefreshComplete();
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.DISABLED);
        mAdapter.showEmptyView();
    }

    private void noMore() {
        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
        mPullToRefreshListView.hideFootView();
        mPullToRefreshListView.onRefreshComplete();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mPlayerHelper != null) {
            mPlayerHelper.onConfigureChange(newConfig);
        }

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (mCommentEditFragment != null && !mCommentEditFragment.isHidden()) {
                mCommentEditFragment.dismissAllowingStateLoss();
            }
            if (dialogFragment != null && !dialogFragment.isHidden()) {
                dialogFragment.dismissAllowingStateLoss();
            }
            if (null != mDanmakuEditFragment && !mDanmakuEditFragment.isHidden()) {
                mDanmakuEditFragment.dismissAllowingStateLoss();
            }
            rl_up.setVisibility(View.GONE);
            ll_bottom.setVisibility(View.GONE);
            rl_bottom.setVisibility(View.GONE);

        } else {
            ll_bottom.setVisibility(View.VISIBLE);
            rl_bottom.setVisibility(View.VISIBLE);
        }
    }


    /**
     * 分享
     */
    private void showSharePop() {
        if (mCurrentVideoItem != null) {
            OneKeyShareContainer.oneKeyShare = mOnekeyShare;
            String name = mCurrentVideoItem.title;
            String image = mCurrentVideoItem.image;
            //精选首页进来，分享标题和图片为推荐位标题和图片
            if (mCurrentVideoItem.guid.equals(homeGuid) && !TextUtils.isEmpty(homeTitle) && !TextUtils.isEmpty(homeImg)) {
                name = homeTitle;
                image = homeImg;
            }
            mOnekeyShare.shareVodWithPopWindow(name,
                    image,
                    mCurrentVideoItem.mUrl,
                    mHeaderView.getShareView(),
                    this,
                    false);

        }
    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageVideoPlay(isLandScape(), "", "");
        } else {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }


    public void autoNextProgramme() {
        mPlayerHelper.autoNextProgramme();
    }

    /**
     * 当在本地发送成功以后，模拟数据更新Adapter
     *
     * @param comment 评论
     */
    public void updateLocalData(String comment) {
        /**
         * 构建一个本地的数据评价
         */
        CommentInfoModel.Newest localNewest = new CommentInfoModel.Newest();
        User user = new User(this);
        localNewest.setUname(user.getUserName());
        localNewest.setComment_contents(comment);
        localNewest.setComment_date(DateUtils.parseTimeStamp(System.currentTimeMillis(), "yyyy-MM-dd HH:mm"));
        localNewest.setUptimes("0");
        localNewest.setUser_id(user.getUid());
        localNewest.setFaceurl(user.getUserIcon());
        CommentInfoModel.UserRole role = new CommentInfoModel.UserRole();
        CommentInfoModel.Video video = new CommentInfoModel.Video();
        video.setVip(User.isVip() ? 1 : 0);
        role.setVideo(video);
        localNewest.setUser_role(role);
        mAdapter.addLocalData(localNewest);
    }

    public void updateLocalDanmu(String comment) {

        if (null != mPlayerHelper) {
            mPlayerHelper.onResume();
        }

        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }

        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().sendTextMessage(comment);
        }

        isEditDanma = false;
    }

    public boolean isPlayAudio() {
        return mPlayerHelper != null && mPlayerHelper.isPlayAudio();
    }

    private NofiticationControlFinishReceiver mNofiticationControlFinishReceiver;

    public void handleNotificationFinishReceiver(boolean register) {

        if (register) {
            if (mNofiticationControlFinishReceiver == null) {
                mNofiticationControlFinishReceiver = new NofiticationControlFinishReceiver(this);
            }
            IntentFilter filter = new IntentFilter();
            filter.addAction(AudioService.ACTION_NOTIFICATION_FINISH);
            filter.addAction(AudioService.ACTION_VIRTUAL_FINISH_LIVE);
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mNofiticationControlFinishReceiver, filter);
        } else {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mNofiticationControlFinishReceiver);
            mNofiticationControlFinishReceiver = null;
        }
    }

    private static class NofiticationControlFinishReceiver extends BroadcastReceiver {
        WeakReference<ActivityVideoPlayerDetail> weakReference;

        public NofiticationControlFinishReceiver(ActivityVideoPlayerDetail activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            ActivityVideoPlayerDetail activity = weakReference.get();
            if (null == activity) {
                return;
            }
            if (null == intent) {
                return;
            }
            String action = intent.getAction();
            if (AudioService.ACTION_NOTIFICATION_FINISH.equals(action) || AudioService.ACTION_VIRTUAL_FINISH_LIVE.equals(action)) {
                logger.debug("onReceive:{}", action);
                activity.switchAudioVideoController(false);
            }
        }

    }

    private void switchAudioVideoController(boolean shouldPlay) {
        if (mPlayerHelper != null) {
            if (shouldPlay) {
                mPlayerHelper.doVideoAudioChange();
            } else {
                mPlayerHelper.switchToVideoControllerAndUI();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHeaderView.deleteADId();
    }


    @Override
    public void showDanmuView(boolean isShow) {
        if (isShow) {
            mVideoSkin.getDanmuView().showView();
            if (isCanPost) {
                mVideoSkin.getDanmuEditView().showView();
            }
            IfengApplication.danmaSwitchStatus = true;
            showDanmuViewAndResume();
        } else {
            mVideoSkin.getDanmuView().hideView();
            mVideoSkin.getDanmuView().hideDanmakuView();
            mVideoSkin.getDanmuEditView().hideView();
            IfengApplication.danmaSwitchStatus = false;
        }
    }

    private void showDanmuViewAndResume() {
        mVideoSkin.getDanmuView().initEmptyDanmakuView();
        mVideoSkin.getDanmuView().showDanmukuView();
        if (mVideoSkin.getDanmuView().getDanmakuView().isPaused()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
        }
        if (mPlayContext.status != IPlayer.PlayerState.STATE_PAUSED) {
            mVideoSkin.getDanmuView().resumeDanmaku();
        } else {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }
    }


    private void requestVideoDanmuData(String guid) {
        String url = DataInterface.getVideoDanmuUrl(guid);
        Log.d("danmu", "url---" + url);

        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        try {
                            JSONObject jsonObj = new JSONObject(response.toString());
                            String responseStr = response.toString();
                            if (responseStr.contains("code")) {
                                if (!jsonObj.getString("code").equals("1")) {
                                    if (IfengApplication.danmaSwitchStatus) {
                                        danmuItems.clear();
                                        ToastUtils.getInstance().showShortToast("获取弹幕信息失败，请稍后重试。");
                                    }
                                    return;
                                }
                            }

                            keyList.clear();
                            tmpList.clear();
                            Iterator<String> iterable = jsonObj.keys();
                            while (iterable.hasNext()) {
                                String str = iterable.next();
                                int index = str.indexOf("t");
                                tmpList.add(Integer.parseInt(str.substring(index + 1, str.length())));
                            }
                            Collections.sort(tmpList);
                            for (int i = 0; i < tmpList.size(); i++) {
                                String tmpStr = "t" + tmpList.get(i);
                                keyList.add(tmpStr);
                            }
                            getVideoDammukuData(response.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    private void getVideoDammukuData(String jsonStr) {
        if (TextUtils.isEmpty(jsonStr)) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);

            danmuItems.clear();
            int arratSize = keyList.size();
            for (int i = 0; i < arratSize; i++) {
                JSONArray array = jsonObject.getJSONArray(keyList.get(i));
                int size = array.length();
                for (int j = 0; j < size; j++) {
                    VideoDanmuItem item = new Gson().fromJson(array.get(j).toString(), VideoDanmuItem.class);
                    item.setFromUser(false);
                    danmuItems.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().addDataSource(danmuItems);
        }
    }

    private void requesetDanmaAllowSend(String guid) {
        String url = DataInterface.getVideoEditUrl(guid);
        Log.d("danmu", "IsSend:" + url);
        CommonDao.sendRequest(url, DanmuItem.class, new Response.Listener<DanmuItem>() {
            @Override
            public void onResponse(DanmuItem response) {
                if (response == null || TextUtils.isEmpty(response.toString())) {
                    return;
                }
                DanmuItem item = response;
                if (!TextUtils.isEmpty(item.getCode())) {
                    if (item.getCode().equals("1")) {
                        if ("1".equals(item.getData().getCan_post())) {
                            isCanPost = true;
                            setDanmuSendStatus(isCanPost);
                        } else {
                            isCanPost = false;
                            setDanmuSendStatus(isCanPost);
                        }
                    } else {
                        isCanPost = false;
                        setDanmuSendStatus(isCanPost);
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    public void setDanmuSendStatus(boolean isSend) {
        if (null != mVideoSkin.getDanmuView()) {
            if (!isSend) {
                mVideoSkin.getDanmuEditView().hideView();
            }
            mVideoSkin.getDanmuView().setShowEditView(isSend);
            if (isEditDanma) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pause2SendDanma();
                    }
                }, 2000);
            }
        }
    }

    @Override
    public void onClickEditButton() {
        isEditDanma = true;
        pause2SendDanma();
    }

    private void pause2SendDanma() {
        if (mPlayerHelper != null) {
            mPlayerHelper.onPause();
        }
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }

        showEditDanmaWindow("", "");
    }


    @Override
    public void onPausePlayButton() {
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }

    }

    @Override
    public void onPlayButton() {
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();

        }
    }


    public void showEditDanmaWindow(String comments, String comment_id) {
        if (mDanmakuEditFragment == null) {
            mDanmakuEditFragment = new DanmakuCommentFragment();
        }

        mDanmakuEditFragment.setShowInputMethod(true);
        mDanmakuEditFragment.setCommends(comments, comment_id);
        mDanmakuEditFragment.setVideoItem(mCurrentVideoItem);
        mDanmakuEditFragment.setNormalVideoPlayer(mPlayerHelper);

        if (!mDanmakuEditFragment.isAdded() && !this.isFinishing()) {
            mDanmakuEditFragment.show(getSupportFragmentManager(), "danmuDialog");
        }

    }

}



