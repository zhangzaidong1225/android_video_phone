package com.ifeng.newvideo.videoplayer.activity;

import com.ifeng.newvideo.videoplayer.activity.widget.DetailVideoHeadView;
import com.ifeng.video.dao.db.model.MainAdInfoModel;

import java.lang.ref.WeakReference;

public class VideoRelativeAdListener implements VideoRelativeAdvertManager.OnVideoRelativeAdListener {
        WeakReference<DetailVideoHeadView> weakReference;
        private  VideoRelativeAdvertManager mRelativeAdvert;

        public void setAdvertManager(VideoRelativeAdvertManager manager){
            this.mRelativeAdvert = manager;
        }
        public VideoRelativeAdListener(DetailVideoHeadView view) {
            weakReference = new WeakReference<>(view);
        }


        @Override
        public void onVideoRelativeAdvertSuccess(MainAdInfoModel model, int index) {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }
            MainAdInfoModel.AdMaterial adMaterial = model.getAdMaterials().get(index);
            // 余量广告
            if (adMaterial.getNeedMoreAd() == 1 && mRelativeAdvert!=null) {
                mRelativeAdvert.requestADMore(model, index);
                return;
            }
            //show advert
            view.loadAdvertLayout(adMaterial);
        }

        @Override
        public void onVideoRelativeAdvertFailed() {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }
            //show advert
        }

        @Override
        public void onVideoRelativeAdvertMoreSuccess(MainAdInfoModel model) {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }
            MainAdInfoModel.AdMaterial adMaterial = model.getAdMaterials().get(0);
            //show advert
            view.loadAdvertLayout(adMaterial);

        }

        @Override
        public void onVideoRelativeAdvertMoreADFailed() {
            DetailVideoHeadView view = weakReference.get();
            if (view == null) {
                return;
            }
        }
    }