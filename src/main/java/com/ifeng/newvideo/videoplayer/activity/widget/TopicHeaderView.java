package com.ifeng.newvideo.videoplayer.activity.widget;

import android.content.Context;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifeng.newvideo.R;

/**
 * Created by fanshell on 2016/8/1.
 */
public class TopicHeaderView extends RelativeLayout {

    private TextView mTopicDescView;
    private TextView mGuideView;
    private int leftPadding = 5;
    private String EMPTY = " ";

    public TopicHeaderView(Context context) {
        this(context, null);
    }

    public TopicHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TopicHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        leftPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftPadding, context.getResources().getDisplayMetrics());
        LayoutInflater.from(context).inflate(R.layout.topic_play_header, this, true);
        mTopicDescView = (TextView) this.findViewById(R.id.topic_play_header_desc);
        mGuideView = (TextView) this.findViewById(R.id.topic_play_header_guide);
    }


    public void setDesText(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        /* Ui变左右布局，去掉计算前面导读tag的长度
        int width = mGuideView.getMeasuredWidth();
        if (width <= 0) {
            mGuideView.measure(0, 0);
            width = mGuideView.getMeasuredWidth();
        }
        width = width + leftPadding;
        String prefix = buildEmptyText(width);*/
        mTopicDescView.setText(text);
    }

    /**
     * 计算空格的字符
     */
    private String buildEmptyText(int width) {
        TextPaint paint = mTopicDescView.getPaint();
        StringBuilder sb = new StringBuilder("");

        while (true) {
            int realWidth = (int) paint.measureText(sb.toString());
            if (realWidth >= width) {
                break;
            }
            sb.append(EMPTY);
        }

        return sb.toString();

    }
}
