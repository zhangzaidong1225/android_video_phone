package com.ifeng.newvideo.videoplayer.activity;

import android.text.TextUtils;
import com.alibaba.fastjson.JSONArray;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.ADInfoDAO;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by fanshell on 2016/8/26.
 * 专题播放列表广告的管理
 * wiki 地址：http://adwiki.ziscn.com/4
 */
public class VideoTopicAdvertManager {

    private static final Logger logger = LoggerFactory.getLogger(VideoTopicAdvertManager.class);
    public static final String TAG = VideoBannerAdvertManager.class.getName();
    private OnTopicAdvertListener mOnTopicAdvertListener;

    public VideoTopicAdvertManager() {

    }

    /**
     * 加载信息流的广告数据
     *
     * @param topicType 专题的类型
     * @param topicId   专题的ID
     * @param uid       用户的id
     */
    public void getTopicAdvert(String topicType, String topicId, String uid) {
        String url = buildUrl(topicType, topicId, uid);
        ADInfoDAO.getBodyAd(url,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        handleResponse(response);
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError();
                    }
                },
                TAG);
    }

    /**
     * 处理服务器返回的数据
     *
     * @param response 服务器返回的数据
     */
    private void handleResponse(Object response) {
        if (response == null || TextUtils.isEmpty(response.toString())) {
            mOnTopicAdvertListener.onTopicAdvertFailed();
            return;
        }
        try {
            List<MainAdInfoModel> mainAdInfoModels = JSONArray.parseArray(response.toString(), MainAdInfoModel.class);
            if (ListUtils.isEmpty(mainAdInfoModels)) {
                return;
            }
            this.mOnTopicAdvertListener.onTopicAdvertSuccess(mainAdInfoModels);
        } catch (Exception e) {
            mOnTopicAdvertListener.onTopicAdvertFailed();
        }
    }

    private void handleError() {

    }

    /**
     * 构建请求的参数
     *
     * @param topicType 专题的类型
     * @param topicId   专题的ID
     * @param uid       用户的id
     * @return
     */
    private String buildUrl(String topicType, String topicId, String uid) {
        StringBuilder builder = new StringBuilder(DataInterface.getAdTopicVideoUrl());
        builder.append('?')
                .append("platform=").append(ADInfoDAO.OS_ANDROID)
                .append("&ztname=").append(topicType).append('_').append(topicId)
                .append("&uid=").append(uid != null ? uid : "");
        return builder.toString();
    }


    public void setOnTopicAdvertListener(OnTopicAdvertListener listener) {
        this.mOnTopicAdvertListener = listener;
    }

    /**
     * 专题播放列表广告成功或失败回调
     */
    public interface OnTopicAdvertListener {
        void onTopicAdvertSuccess(List<MainAdInfoModel> ads);

        void onTopicAdvertFailed();
    }

}
