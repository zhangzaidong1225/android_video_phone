package com.ifeng.newvideo.videoplayer.activity.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.ui.adapter.basic.ChannelBaseAdapter;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.videoplayer.activity.VideoBannerAdvertManager;
import com.ifeng.newvideo.videoplayer.activity.VideoRelativeAdvertManager;
import com.ifeng.newvideo.videoplayer.bean.TopicPlayerItem;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteItem;
import com.ifeng.newvideo.videoplayer.bean.TopicVotePoint;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteResultArray;
import com.ifeng.newvideo.videoplayer.bean.UITopicData;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.dao.TopicVideoDao;
import com.ifeng.newvideo.widget.AdBannerViewContainer;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.dao.VideoAdConfigDao;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.util.AdsExposureSesssion;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class TopicBottomView extends LinearLayout implements View.OnClickListener {

    public static final int CLICK_RED = 1;
    public static final int CLICK_BLUE = 2;
    public static final int CLICK_NONE = -1;

    private final int INIT_SHOW_COUNT = 10;
    private Context context;
    private LinearLayout mRelativeVideoContainer;
    private LinearLayout mTopicTopContainer;
    private LinearLayout mTopicBottomContainer;
    private OnItemClickListener mItemClickListener;
    //    private View mMorView;
    private LinearLayout mRelativeVideoView;

    private LinearLayout mRelativeAdView;
    private AdBannerViewContainer mAdViewContainer;
    private TextView mAdLabelView;
    private NetworkImageView mAdView;
    //    private boolean isStartAdvert;
    private MainAdInfoModel.AdMaterial mAdMaterial;
    private VideoBannerAdvertManager mVideoInfoAdManager;
    private VideoRelativeAdvertManager mRelativeAd;
    private VideoRelativeAdListener mRelativeAdListener;
    private View mRelativeAdvertView;
    private int adPosition = -1;
    private MainAdInfoModel.AdMaterial mRelativeAdModle;

    private View mErrorView;
    private ImageView mErrorIconView;
    private TextView mErrorTipView;

    private List<UITopicData> videoList;
    private List<MainAdInfoModel.AdMaterial> mADList = new ArrayList<>();
    private List<VideoItem> bottomList = new ArrayList<>();

    //headerview
    private TextView mTopicDescView;
    private TextView mGuideView;
    private int leftPadding = 5;
    private String EMPTY = " ";
    //voteview
    private LinearLayout llVoteContainer;
    private TextView voteTitle, voteContent;
    private TextView redTitle, voteRedNum, redPercentage;
    private TextView blueTitle, voteBlueNum, bluePercentage;
    private ImageView ivVoteRed, ivVoteBlue;
    private LinearLayout llVotePercentage;
    private ProgressBar progressBar;
    private View llRedContainer, llBlueContainer;

    private int redNum, blueNum, totalNum;
    private TopicVotePoint mVoteOptionBlue;
    private TopicVotePoint mVoteOptionRed;
    private boolean mAttach = false;

    // 投票参数
    private String mSurveyInfoId;
    private String mQuestionId;
    private String mRedItemId;
    private String mBlueItemId;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!mAttach) {
            registerLoginBroadcast();
            mAttach = true;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAttach) {
            unregisterLoginReceiver();
        }
        VolleyHelper.getRequestQueue().cancelAll(VideoBannerAdvertManager.TAG);
        if (mVideoInfoAdManager != null) {
            mVideoInfoAdManager.setOnVideoInfoHeaderSuccess(null);
        }
        clickPosition = CLICK_NONE;
    }


    public TopicBottomView(Context context) {
        this(context, null);
    }

    public TopicBottomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TopicBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.topic_bottom_view_layout, this, true);

        leftPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftPadding, context.getResources().getDisplayMetrics());
        mTopicDescView = (TextView) findViewById(R.id.topic_play_header_desc);
        mGuideView = (TextView) findViewById(R.id.topic_play_header_guide);

        mRelativeVideoContainer = (LinearLayout) findViewById(R.id.video_topic_relative_container);
        mTopicTopContainer = (LinearLayout) findViewById(R.id.video_topic_recommend_top_container);
        mTopicBottomContainer = (LinearLayout) findViewById(R.id.video_topic_recommend_bottom_container);
        mTopicTopContainer.removeAllViews();
        mRelativeVideoView = (LinearLayout) findViewById(R.id.ll_relative_video_topic_view);

        llVoteContainer = (LinearLayout) findViewById(R.id.ll_vote_container);
        llVoteContainer.setVisibility(View.GONE);
        voteTitle = (TextView) findViewById(R.id.topic_vote_title);
        voteContent = (TextView) findViewById(R.id.topic_vote_content);
        redTitle = (TextView) findViewById(R.id.vote_red_title);
        voteRedNum = (TextView) findViewById(R.id.vote_red_num);
        redPercentage = (TextView) findViewById(R.id.vote_red_percentage);
        blueTitle = (TextView) findViewById(R.id.vote_blue_title);
        voteBlueNum = (TextView) findViewById(R.id.vote_blue_num);
        bluePercentage = (TextView) findViewById(R.id.vote_blue_percentage);
        ivVoteRed = (ImageView) findViewById(R.id.vote_red_iv);
        ivVoteBlue = (ImageView) findViewById(R.id.vote_blue_iv);
        llVotePercentage = (LinearLayout) findViewById(R.id.ll_vote_percentage);
        llVotePercentage.setVisibility(View.GONE);
        progressBar = (ProgressBar) findViewById(R.id.vote_progress);
        llRedContainer = findViewById(R.id.ll_red_container);
        llBlueContainer = findViewById(R.id.ll_blue_container);

        mRelativeAdView = (LinearLayout) findViewById(R.id.ll_topic_ad_container);
        mRelativeAdView.setVisibility(View.VISIBLE);
        mAdView = (NetworkImageView) findViewById(R.id.video_topic_ad);
        mAdViewContainer = (AdBannerViewContainer) findViewById(R.id.video_topic_ad_container);
        mAdLabelView = (TextView) findViewById(R.id.video_topic_ad_label);
        mAdView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdView(view);
            }
        });

        mErrorView = findViewById(R.id.video_topic_load_data_error);
        mErrorIconView = (ImageView) mErrorView.findViewById(R.id.video_topic_load_fail_icon);
        mErrorTipView = (TextView) mErrorView.findViewById(R.id.video_topic_load_fail_text);
        resetErrorView();
        mErrorView.setVisibility(View.GONE);

        mVideoInfoAdManager = new VideoBannerAdvertManager();
        mVideoInfoAdManager.setOnVideoInfoHeaderSuccess(new VideoInfoHeadListener(this));
//        isStartAdvert = false;
        mRelativeAd = new VideoRelativeAdvertManager();
        mRelativeAdListener = new VideoRelativeAdListener(this);
        mRelativeAdListener.setAdvertManager(mRelativeAd);
        mRelativeAd.setOnVideoRelativeSuccess(mRelativeAdListener);

    }


    private void hideLastDivider(ViewGroup view) {
        int lastIndex = view.getChildCount() - 1;
        View child = view.getChildAt(lastIndex);
        if (child != null) {
            child.findViewById(R.id.divider).setVisibility(View.GONE);
        }
    }

    private void showLastDivider(ViewGroup view) {
        int lastIndex = view.getChildCount() - 1;
        View child = view.getChildAt(lastIndex);
        if (child != null) {
            child.findViewById(R.id.divider).setVisibility(View.VISIBLE);
        }
    }

    static class VideoInfoHeadListener implements VideoBannerAdvertManager.OnVideoBannerAdvertListener {
        WeakReference<TopicBottomView> weakReference;

        VideoInfoHeadListener(TopicBottomView view) {
            weakReference = new WeakReference<>(view);
        }

        @Override
        public void onVideoBannerAdvertSuccess(MainAdInfoModel model, int index) {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }
            MainAdInfoModel.AdMaterial adMaterial = model.getAdMaterials().get(index);

            // 余量广告
            if (adMaterial.getNeedMoreAd() == 1) {
                view.mVideoInfoAdManager.requestADMore(model, index);
                return;
            }
            view.showAd(adMaterial);
        }

        @Override
        public void onVideoBannerAdvertFailed() {

            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }

            if (null != VideoAdConfigDao.adConfig && null != VideoAdConfigDao.adConfig.getInfoAD() && !ListUtils.isEmpty(VideoAdConfigDao.adConfig.getInfoAD().getBanner())) {
                String adid = VideoAdConfigDao.adConfig.getInfoAD().getBanner().get(0).getAdId();
                CommonStatictisListUtils.getInstance().sendADEmptyExpose(adid);
                return;
            }

            view.mAdViewContainer.setVisibility(View.GONE);
        }

        @Override
        public void onVideoBannerAdvertMoreSuccess(MainAdInfoModel model) {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }

            view.showAd(model.getAdMaterials().get(0));
        }

        @Override
        public void onVideoBannerAdvertMoreADFailed() {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }

            view.mAdViewContainer.setVisibility(View.GONE);
        }
    }

    private void showAd(MainAdInfoModel.AdMaterial adMaterial) {
        if (adMaterial == null) {
            return;
        }

        mAdMaterial = adMaterial;
        mAdView.setTag(adMaterial);
        mAdView.setImageUrl(adMaterial.getImageURL(), VolleyHelper.getImageLoader());
        mAdViewContainer.setVisibility(View.VISIBLE);
        MainAdInfoModel.AdMaterial.Icon icon = adMaterial.getIcon();
        if (icon != null) {
            String text = icon.getText();
            mAdLabelView.setText(TextUtils.isEmpty(text) ? "广告" : text);
            mAdLabelView.setVisibility(icon.getShowIcon() == 1 ? View.VISIBLE : View.GONE);
        }

    }

    class VideoRelativeAdListener implements VideoRelativeAdvertManager.OnVideoRelativeAdListener {
        WeakReference<TopicBottomView> weakReference;
        private VideoRelativeAdvertManager mRelativeAdvert;

        public void setAdvertManager(VideoRelativeAdvertManager manager) {
            this.mRelativeAdvert = manager;
        }

        public VideoRelativeAdListener(TopicBottomView view) {
            weakReference = new WeakReference<>(view);
        }

        @Override
        public void onVideoRelativeAdvertSuccess(MainAdInfoModel model, int index) {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }
            MainAdInfoModel.AdMaterial adMaterial = model.getAdMaterials().get(index);
            // 余量广告
            if (adMaterial.getNeedMoreAd() == 1 && mRelativeAdvert != null) {
                mRelativeAdvert.requestADMore(model, index);
                return;
            }
            //show advert
            view.loadAdvertLayout(adMaterial);
        }

        @Override
        public void onVideoRelativeAdvertFailed() {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }
        }

        @Override
        public void onVideoRelativeAdvertMoreSuccess(MainAdInfoModel model) {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }
            MainAdInfoModel.AdMaterial adMaterial = model.getAdMaterials().get(0);
            //show advert
            view.loadAdvertLayout(adMaterial);

        }

        @Override
        public void onVideoRelativeAdvertMoreADFailed() {
            TopicBottomView view = weakReference.get();
            if (view == null) {
                return;
            }
        }
    }

    public void loadAdvertLayout(final MainAdInfoModel.AdMaterial mode) {
        mRelativeAdModle = mode;
        if (mode == null) {
            return;
        }
        MainAdInfoModel.AdMaterial.AdConditions conditions = mode.getAdConditions();
        int index;
        try {
            index = Integer.parseInt(conditions.getIndex());
        } catch (Exception e) {
            return;
        }
        if (videoList != null && index > videoList.size()) {
            return;
        }
        adPosition = index;

        if (mRelativeAdvertView == null) {
            mRelativeAdvertView = LayoutInflater.from(this.getContext()).inflate(R.layout.ad_mix_text_pic, mTopicTopContainer, false);
        }
        TextView title = (TextView) mRelativeAdvertView.findViewById(R.id.tv_left_title);

        TextView tagView = (TextView) mRelativeAdvertView.findViewById(R.id.tv_ad_tag);
        mRelativeAdvertView.findViewById(R.id.tv_ad_download).setVisibility(View.GONE);
        NetworkImageView right_pic = (NetworkImageView) mRelativeAdvertView.findViewById(R.id.niv_right_picture);
        MainAdInfoModel.AdMaterial.Icon icon = mode.getIcon();
        String tag = "";
        if (icon != null) {
            tag = icon.getText();
        }
        if (TextUtils.isEmpty(tag)) {
            tag = " 广告";
        }
        tagView.setVisibility(View.VISIBLE);
        tagView.setText(tag);
        String imgUrl = mode.getImageURL();
        title.setText(mode.getText());
        right_pic.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        if (index < INIT_SHOW_COUNT) {
            mTopicTopContainer.addView(mRelativeAdvertView, index);
        } else {
            mTopicBottomContainer.addView(mRelativeAdvertView, index);
        }

        //add click
        mRelativeAdvertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mode == null) {
                    return;
                }
                String adId = mode.getAdId();
                String title = mode.getText();
                String imageUrl = mode.getImageURL();

                String adType = "";
                String clickUrl = "";
                List<String> downloadCompletedUrls = null;
                MainAdInfoModel.AdMaterial.AdAction adAction = mode.getAdAction();
                if (adAction != null) {
                    if (!IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType())) {
                        adType = IfengType.TYPE_WEBFULL;
                    }

                    clickUrl = adAction.getUrl();

                    // 如果是广告app下载类型，点击地址取loadingUrl
                    if (mode.getAdConditions() != null && "app".equalsIgnoreCase(mode.getAdConditions().getShowType())) {
                        clickUrl = adAction.getLoadingurl();
                    }

                    if (!ListUtils.isEmpty(adAction.getAsync_downloadCompletedurl())) {
                        downloadCompletedUrls.addAll(adAction.getAsync_downloadCompletedurl());
                    }
                    if (!ListUtils.isEmpty(adAction.getDownloadCompletedurl())) {
                        downloadCompletedUrls.addAll(adAction.getDownloadCompletedurl());
                    }

                    //广告点击曝光
                    AdvertExposureDao.sendAdvertClickReq(adId, (ArrayList<String>) adAction.getAsync_click());
                }

                ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, clickUrl, null, null, v.getContext(), downloadCompletedUrls, "", "", adAction != null ? adAction.getAsync_download() : null);
            }

        });
    }

    private void onClickAdView(View view) {
        if (view.getTag() == null) {
            return;
        }
        MainAdInfoModel.AdMaterial adMaterial = (MainAdInfoModel.AdMaterial) view.getTag();
        String adId = adMaterial.getAdId();
        String title = adMaterial.getText();
        String imageUrl = adMaterial.getImageURL();

        String adType = "";
        String clickUrl = "";
        List<String> downloadCompletedUrls = null;
        MainAdInfoModel.AdMaterial.AdAction adAction = adMaterial.getAdAction();
        if (adAction != null) {
            if (!IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType())) {
                adType = IfengType.TYPE_WEBFULL;
            }

            clickUrl = adAction.getUrl();

            // 如果是广告app下载类型，点击地址取loadingUrl
            if (adMaterial.getAdConditions() != null && "app".equalsIgnoreCase(adMaterial.getAdConditions().getShowType())) {
                clickUrl = adAction.getLoadingurl();
            }

            if (!ListUtils.isEmpty(adAction.getAsync_downloadCompletedurl())) {
                downloadCompletedUrls.addAll(adAction.getAsync_downloadCompletedurl());
            }
            if (!ListUtils.isEmpty(adAction.getDownloadCompletedurl())) {
                downloadCompletedUrls.addAll(adAction.getDownloadCompletedurl());
            }

            //广告点击曝光
            AdvertExposureDao.sendAdvertClickReq(adId, (ArrayList<String>) adAction.getAsync_click());
        }

        ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, clickUrl, null, null, view.getContext(), downloadCompletedUrls, "", "", adAction != null ? adAction.getAsync_download() : null);
    }


    private void resetErrorView() {
        mErrorIconView.setImageResource(R.drawable.icon_common_load_fail);
        mErrorTipView.setText(R.string.common_load_data_error);
    }

    private void updateErrorView() {
        mErrorIconView.setImageResource(R.drawable.icon_common_loading);
        mErrorTipView.setText(R.string.common_onloading);
    }

    public void updateTopicVideoView(List<UITopicData> dataList) {
        this.videoList = dataList;

        mRelativeVideoContainer.setVisibility(View.VISIBLE);
        mRelativeVideoView.setVisibility(View.VISIBLE);
        int count = dataList.size();
        UITopicData topicItem;

        mTopicTopContainer.removeAllViews();
        for (int i = 0; i < count; i++) {
            boolean isVideo = dataList.get(i).isVideo;
            boolean isAD = dataList.get(i).isAd;
            boolean isTitle = dataList.get(i).isTitle;

            if (isVideo) {
                topicItem = dataList.get(i);
                View view = LayoutInflater.from(this.getContext()).inflate(R.layout.item_listview_mix_text_picture, mTopicTopContainer, false);
                setVideoItem(topicItem, view);
                mTopicTopContainer.addView(view);
            }

            if (isAD) {
                final UITopicData data = dataList.get(i);
                final TopicPlayerItem material = data.item;

                if (null != material && TextUtils.isEmpty(material.getImageURL())) {
                    View emptyView = LayoutInflater.from(this.getContext()).inflate(R.layout.item_ad_empty_layout, mTopicTopContainer, false);
                    Log.d("topic", "Ad is Empty");
                    AdvertExposureDao.addIfengAdvExposureStatistics(material.getAdId(), null, material.getAdAction().getPvurl());
                    mTopicTopContainer.addView(emptyView);
                } else {
                    if (null != material) {
//                        View view = null;
                        if ("large".equals(material.getAdConditions().getShowType().trim())) {
                            //大图版
                            View view = LayoutInflater.from(this.getContext()).inflate(R.layout.item_kk_live, mTopicTopContainer, false);
                            setAdItem(data, view);
                            mTopicTopContainer.addView(view);

                        } else {
                            View view = LayoutInflater.from(this.getContext()).inflate(R.layout.ad_mix_text_pic, mTopicTopContainer, false);
                            setAdItem(data, view);
                            mTopicTopContainer.addView(view);
                        }
                    }
                }

            }

            if (isTitle) {
                topicItem = dataList.get(i);
                View view = LayoutInflater.from(this.getContext()).inflate(R.layout.topic_play_item_title, mTopicTopContainer, false);
                setTitleView(topicItem, view);
                mTopicTopContainer.addView(view);
            }
        }


    }

    private void setTitleView(UITopicData item, View view) {
        if (null == item) {
            return;
        }
        TextView mTopicTitle = (TextView) view.findViewById(R.id.topic_play_title);
        View mRootView = view.findViewById(R.id.topic_play_title_container);

        setText(mTopicTitle, item.topicName);
        mRootView.setVisibility(View.VISIBLE);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                ToastUtils.getInstance().showShortToast("aaaaaaaa");
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(UITopicData videoItem);
    }

    private void setVideoItem(final UITopicData data, View view) {

        NetworkImageView mAvatarView = (NetworkImageView) view.findViewById(R.id.niv_left_emoji);
        NetworkImageView mPicView = (NetworkImageView) view.findViewById(R.id.niv_right_picture);
        TextView mTitleView = (TextView) view.findViewById(R.id.tv_left_title);
        TextView mAuthorNameView = (TextView) view.findViewById(R.id.tv_author);
        TextView mPlayCountView = (TextView) view.findViewById(R.id.tv_play_times);
        TextView mPlayDuration = (TextView) view.findViewById(R.id.tv_duration);

        final TopicPlayerItem topicItem = data.item;
        setText(mTitleView, topicItem.name);
        WeMedia media = topicItem.weMedia;
        if (media != null) {
            mAuthorNameView.setText(media.name);
            mAuthorNameView.setVisibility(View.VISIBLE);
        } else {
            mAuthorNameView.setText("");
            mAuthorNameView.setVisibility(View.GONE);
        }
        mAvatarView.setVisibility(View.GONE);

        mPlayCountView.setText(StringUtils.changePlayTimes(topicItem.playTime));
        mPlayDuration.setText(StringUtils.changeDuration(topicItem.duration));
        if (data.isHighlight) {
            mTitleView.setTextColor(Color.RED);
        } else {
            mTitleView.setTextColor(Color.parseColor("#262626"));
        }
        setImageUrl(mPicView, topicItem.image, R.drawable.bg_default_mid);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextColor(topicItem);

                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(data);
                }
            }
        });

    }


    private void setText(TextView view, String text) {
        view.setText(TextUtils.isEmpty(text) ? "" : text);
    }

    private void setImageUrl(NetworkImageView imageView, String imgUrl, int defaultImgResId) {
        imageView.setImageUrl(imgUrl, VolleyHelper.getImageLoader());
        imageView.setDefaultImageResId(defaultImgResId);
        imageView.setErrorImageResId(defaultImgResId);
    }

    private void sendExposure(MainAdInfoModel.AdMaterial adMaterial) {
        Rect localRect = new Rect();
        boolean localVisibility = getLocalVisibleRect(localRect);
        if (!AdsExposureSesssion.getInstance().containsADItemRecord(adMaterial.getAdId())) {
            if (localVisibility) {
                AdsExposureSesssion.getInstance().addADItemRecord(adMaterial.getAdId());
                AdvertExposureDao.addIfengAdvExposureForChannel(adMaterial.getAdId(), null,
                        adMaterial.getAdAction().getPvurl(), adMaterial.getAdAction().getAdpvurl());
            }
        } else {
            if (!localVisibility) {
                AdsExposureSesssion.getInstance().removeADItemRecord(adMaterial.getAdId());
            }
        }

    }

    private void setAdvertTag(TopicPlayerItem material, TextView tag) {
        tag.setVisibility(View.GONE);
        MainAdInfoModel.AdMaterial.Icon icon = material.getIcon();
        if (icon != null && icon.getShowIcon() == 1) {
            String text = icon.getText();

            if (TextUtils.isEmpty(text)) {
                text = "广告";
            }
            tag.setText(text);
            tag.setTextColor(Color.parseColor("#2a90d7"));
            tag.setBackgroundResource(R.drawable.home_item_tag_blue);
            tag.setVisibility(View.VISIBLE);
        }
    }

    private void setAdItem(final UITopicData data, View view) {
        if (null == data) {
            return;
        }
        TextView titleView;
        TextView tag;
        final TopicPlayerItem material = data.item;

        if ("large".equals(material.getAdConditions().getShowType().trim())) {
            //大图版
            tag = (TextView) view.findViewById(R.id.tv_ad_banner_label);
            titleView = (TextView) view.findViewById(R.id.tv_kk_title);
            TextView online_count = (TextView) view.findViewById(R.id.tv_online_count);
            NetworkImageView image = (NetworkImageView) view.findViewById(R.id.iv_right_picture_0);
            setAdvertTag(material, tag);
            titleView.setText(material.getText());
            online_count.setText(material.getKkrand());
            setImageUrl(image, material.getImageURL(), R.drawable.bg_default_ad_banner_big);
        } else {
            // 其他版式
            titleView = (TextView) view.findViewById(R.id.tv_left_title);
            TextView adDesc = (TextView) view.findViewById(R.id.tv_ad_desc);
            tag = (TextView) view.findViewById(R.id.tv_ad_tag);
            TextView download = (TextView) view.findViewById(R.id.tv_ad_download);
            NetworkImageView right_picture = (NetworkImageView) view.findViewById(R.id.niv_right_picture);

            titleView.setText(material.getText());
            setImageUrl(right_picture, material.getImageURL(), R.drawable.bg_default_mid);

            //设置download的显示
            download.setVisibility(View.GONE);
            final MainAdInfoModel.AdMaterial.AdConditions conditions = material.getAdConditions();
            String showType = conditions != null ? conditions.getShowType() : "";
            if (ChannelBaseAdapter.AD_TYPE_APP.equalsIgnoreCase(showType)) {
                download.setVisibility(View.VISIBLE);
                ((View) (download.getParent())).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (material != null && material.getAdAction() != null) {
                            MainAdInfoModel.AdMaterial.AdAction adAction = material.getAdAction();
                            AdvertExposureDao.sendAdvertClickReq(material.getAdId(), adAction.getAsync_click());

                            List<String> downloadCompleteUrl = new ArrayList<>();
                            if (!ListUtils.isEmpty(material.getAdAction().getAsync_downloadCompletedurl())) {
                                downloadCompleteUrl.addAll(material.getAdAction().getAsync_downloadCompletedurl());
                            }
                            if (!ListUtils.isEmpty(material.getAdAction().getDownloadCompletedurl())) {
                                downloadCompleteUrl.addAll(material.getAdAction().getDownloadCompletedurl());
                            }
                            DownLoadUtils.download(IfengApplication.getAppContext(), material.getAdId(), adAction.getLoadingurl(),
                                    (ArrayList<String>) adAction.getAsync_download(), (ArrayList<String>) downloadCompleteUrl);
                        }
                    }
                });
            }
            //描述的修改
            String desc = material.getVdescription();
            if (!TextUtils.isEmpty(desc)) {
                adDesc.setText(desc);
            }
            //广告标签
            setAdvertTag(material, tag);
        }

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(data);
                }
            }
        });

        //expose
        AdvertExposureDao.addIfengAdvExposureForChannel(material.getAdId(), material.getAdPositionId(),
                material.getAdAction().getPvurl(), material.getAdAction().getAdpvurl());
        ADRecord.addAdShow(material.getAdId(), ADRecord.AdRecordModel.ADTYPE_INFO);


    }

    private DetailVideoHeadView.onErrorListener mErrorClickListener;

    public void setOnErrorListener(DetailVideoHeadView.onErrorListener listener) {
        this.mErrorClickListener = listener;

    }

    public void showRelativeVideoError() {
        mRelativeVideoContainer.setVisibility(View.GONE);
        resetErrorView();
        mErrorView.setVisibility(View.VISIBLE);
        mErrorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mErrorClickListener != null) {
                    updateErrorView();
                    mErrorClickListener.onRelativeErrorClick();
                }
            }
        });
    }

    public void changeTextColor(VideoItem videoItem) {
        int position = getIndex(videoItem);
        if (position < 0 || position >= videoList.size()) {
            return;
        }
        resetTextColor();
        View view;
        boolean isBottomItem = mTopicTopContainer.getChildCount() < position + 1;
        if (isBottomItem) {
            view = mTopicBottomContainer.getChildAt(
                    position - mTopicTopContainer.getChildCount());
        } else {
            view = mTopicTopContainer.getChildAt(position);
        }
        TextView titleView = (TextView) view.findViewById(R.id.tv_left_title);
        titleView.setTextColor(Color.RED);
    }

    private int getIndex(VideoItem videoItem) {
        if (videoItem == null || TextUtils.isEmpty(videoItem.guid) || ListUtils.isEmpty(videoList)) {
            return -1;
        }
        int size = videoList.size();
        for (int i = 0; i < size; i++) {
            if (videoList.get(i) == null || videoList.get(i).item == null
                    || TextUtils.isEmpty(videoList.get(i).item.guid)) {
                continue;
            }
            if (videoItem.guid.equals(videoList.get(i).item.guid)) {
                return i;
            }
        }
        return -1;
    }

    private void resetTextColor() {
        for (int i = 0; i < mTopicTopContainer.getChildCount(); i++) {
            View view = mTopicTopContainer.getChildAt(i);
            View titleView = view.findViewById(R.id.tv_left_title);
            if (titleView != null && titleView instanceof TextView) {
                ((TextView) titleView).setTextColor(Color.parseColor("#262626"));
            }

        }

        for (int i = 0; i < mTopicBottomContainer.getChildCount(); i++) {
            View view = mTopicBottomContainer.getChildAt(i);
            View titleView = view.findViewById(R.id.tv_left_title);
            if (titleView != null && titleView instanceof TextView) {
                ((TextView) titleView).setTextColor(Color.parseColor("#262626"));
            }
        }
    }


    @Override
    public void onClick(View view) {

    }

    public void setDesText(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        mTopicDescView.setText(text);
    }

    public void setVoteVisible() {
        llVoteContainer.setVisibility(View.GONE);
        mRelativeVideoContainer.setVisibility(View.VISIBLE);
        mRelativeVideoView.setVisibility(View.VISIBLE);
    }

    public void updateVoteView(final TopicVoteItem voteItem, final List<TopicVoteResultArray> resultData) {
        if (null == voteItem) {
            return;
        }

        mRelativeVideoContainer.setVisibility(View.VISIBLE);
        mRelativeVideoView.setVisibility(View.VISIBLE);
        llVoteContainer.setVisibility(View.VISIBLE);
        currentItem = voteItem;
        mSurveyInfoId = voteItem.getData().getSurveyinfo().getId();

        if (!TextUtils.isEmpty(voteItem.getData().getSurveyinfo().getTitle())) {
            voteTitle.setText(voteItem.getData().getSurveyinfo().getTitle());
        } else {
            voteTitle.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(voteItem.getData().getSurveyinfo().getLeadtxt())) {
            voteContent.setText(voteItem.getData().getSurveyinfo().getLeadtxt());
        } else {
            voteContent.setVisibility(View.GONE);
        }

        if (!ListUtils.isEmpty(resultData)) {
            final TopicVoteResultArray result = resultData.get(0);
            mQuestionId = result.getQuestionid();

            mVoteOptionRed = result.getOption().get(0);
            mRedItemId = mVoteOptionRed.getItemid();

            mVoteOptionBlue = result.getOption().get(1);
            mBlueItemId = mVoteOptionBlue.getItemid();

            updateRedContainer();
            updateBlueContainer();
        }
    }

    private void updateRedContainer() {
        if (!TextUtils.isEmpty(mVoteOptionRed.getTitle())) {
            redTitle.setText(mVoteOptionRed.getTitle());
            redNum = Integer.parseInt(mVoteOptionRed.getNum());
            voteRedNum.setText(mVoteOptionRed.getNum());
        } else {
            redTitle.setVisibility(View.GONE);
            voteRedNum.setVisibility(View.GONE);
        }

        llRedContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ClickUtils.isFastDoubleClick()) {
                    return;
                }
                if (isVoted) {
                    ToastUtils.getInstance().showShortToast("您已经提交过");
                    return;
                }
                clickPosition = CLICK_RED;
                Log.d("topicBottomView", "red");
                if (User.isLogin()) {
                    uploadTopicVoteResult(mSurveyInfoId, mQuestionId, mRedItemId, true);
                } else {
                    IntentUtils.startLoginActivity(context);
                }
            }
        });
    }

    private void updateBlueContainer() {
        if (!TextUtils.isEmpty(mVoteOptionBlue.getTitle())) {
            blueTitle.setText(mVoteOptionBlue.getTitle());
            blueNum = Integer.parseInt(mVoteOptionBlue.getNum());
            voteBlueNum.setText(mVoteOptionBlue.getNum());
        } else {
            blueTitle.setVisibility(View.GONE);
        }

        llBlueContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ClickUtils.isFastDoubleClick()) {
                    return;
                }
                if (isVoted) {
                    ToastUtils.getInstance().showShortToast("您已经提交过");
                    return;
                }
                clickPosition = CLICK_BLUE;
                Log.d("topicBottomView", "blue");
                if (User.isLogin()) {
                    uploadTopicVoteResult(mSurveyInfoId, mQuestionId, mBlueItemId, false);
                } else {
                    IntentUtils.startLoginActivity(context);
                }
            }
        });
    }

    private int clickPosition = CLICK_NONE;

    private void uploadTopicVoteResult(String id, String questionId, String sur, final boolean isClickRed) {
        TopicVideoDao.cancelAll();
        TopicVideoDao.commitTopicVoteResult(id, questionId,
                sur, new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (null == response || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        try {
                            com.alibaba.fastjson.JSONObject result = (com.alibaba.fastjson.JSONObject) response;
                            if ("1".equals(result.getString("ifsuccess"))) {
                                if (isClickRed) {
                                    ++redNum;
                                } else {
                                    ++blueNum;
                                }
                                updatePercentageInfo();
                                llVotePercentage.setVisibility(View.VISIBLE);
                            } else {
                                if (result.getString("msg").contains("您已经提交过")) {
                                    updatePercentageInfo();
                                    llVotePercentage.setVisibility(View.VISIBLE);
                                }
                            }
                            ToastUtils.getInstance().showShortToast(result.getString("msg"));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    }

    private void updatePercentageInfo() {
        isVoted = true;

        totalNum = redNum + blueNum;

        float redtmp = redNum / (float) totalNum;
        float bluetmp = blueNum / (float) totalNum;

        DecimalFormat df = new DecimalFormat("0.00");
        String redStr = String.valueOf((int) (Double.parseDouble(df.format(redtmp)) * 100));
        String blueStr = String.valueOf((int) (Double.parseDouble(df.format(bluetmp)) * 100));

        redPercentage.setText(redStr + "%");
        bluePercentage.setText(blueStr + "%");

        ivVoteRed.setVisibility(View.GONE);
        if (redNum >= 100000) {
            voteRedNum.setText("10万+");
        } else {
            voteRedNum.setText(redNum + "");
        }
        voteRedNum.setVisibility(View.VISIBLE);

        ivVoteBlue.setVisibility(View.GONE);
        if (blueNum >= 100000) {
            voteBlueNum.setText("10万+");
        } else {
            voteBlueNum.setText(blueNum + "");
        }
        voteBlueNum.setVisibility(View.VISIBLE);

        progressBar.setProgress(Integer.parseInt(redStr));
    }

    private void registerLoginBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentKey.LOGINBROADCAST);
        IfengApplication.getInstance().registerReceiver(mLoginReceiver, filter);
    }

    private void unregisterLoginReceiver() {
        try {
            IfengApplication.getInstance().unregisterReceiver(mLoginReceiver);
        } catch (Exception e) {
        }
    }

    private BroadcastReceiver mLoginReceiver = new LoginReceiver(this);
    private TopicVoteItem currentItem;
    private boolean isVoted = false;

    static class LoginReceiver extends BroadcastReceiver {
        WeakReference<TopicBottomView> weakReference;

        LoginReceiver(TopicBottomView view) {
            weakReference = new WeakReference<>(view);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            TopicBottomView view = weakReference.get();
            if (null == view) {
                return;
            }
            if (IntentKey.LOGINBROADCAST.equals(intent.getAction())) {
                int loginState = intent.getIntExtra(IntentKey.LOGINSTATE, 2);
                if (loginState == IntentKey.LOGINING) {

                    if (CLICK_RED == view.clickPosition) {
                        view.uploadTopicVoteResult(view.mSurveyInfoId, view.mQuestionId, view.mRedItemId, true);
                    }

                    if (CLICK_BLUE == view.clickPosition) {
                        view.uploadTopicVoteResult(view.mSurveyInfoId, view.mQuestionId, view.mBlueItemId, false);
                    }
                }

            }
        }
    }

}
