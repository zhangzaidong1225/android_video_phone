package com.ifeng.newvideo.videoplayer.activity.listener;

import android.text.TextUtils;
import android.view.View;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;


/**
 * Created by fanshell on 2016/7/31.
 */
public class VideoDetailShareClickListener implements View.OnClickListener {
    private OneKeyShare mOneKeyShare;

    @Override
    public void onClick(View view) {

        if (!NetUtils.isNetAvailable(view.getContext())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        VideoItem item = (VideoItem) view.getTag();
//        String echid = (String) view.getTag(R.id.video_detail_weibo);
//        boolean isRelate = (boolean) view.getTag(R.id.video_detail_qq);

        if (item == null || TextUtils.isEmpty(item.mUrl)) {
            return;
        }
        if (mOneKeyShare == null) {
            mOneKeyShare = new OneKeyShare(view.getContext());
        }

        OneKeyShareContainer.oneKeyShare = mOneKeyShare;
        String searchPath = item.searchPath;
        String weMediaId = item.weMedia != null ? item.weMedia.id : "";
        String weMediaName = item.weMedia != null ? item.weMedia.name : "";
//        mOneKeyShare.initShareStatisticsData(item.guid, isRelate ? "" : echid, searchPath, weMediaId, "");
        mOneKeyShare.initSmartShareData(UserOperatorConst.TYPE_VIDEO, weMediaName, item.title);
        String shareTitle = item.title;
        String shareImg = item.image;
        if (item.guid.equals(guid) && !TextUtils.isEmpty(title) && !TextUtils.isEmpty(imgeUrl)) {
            shareTitle = title;
            shareImg = imgeUrl;
        }

//        switch (view.getId()) {
//            case R.id.video_detail_webchat:
//                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WX_QUICK, PageIdConstants.PLAY_VIDEO_V);
//                mOneKeyShare.shareVodWithoutPopWindow(new Wechat(IfengApplication.getInstance()), shareTitle, shareImg, item.mUrl, false);
//                break;
//            case R.id.video_detail_friend:
//                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_WXF_QUICK, PageIdConstants.PLAY_VIDEO_V);
//                mOneKeyShare.shareVodWithoutPopWindow(new WechatMoments(IfengApplication.getInstance()), shareTitle, shareImg, item.mUrl, false);
//                break;
//            case R.id.video_detail_weibo:
//                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_SINA_QUICK, PageIdConstants.PLAY_VIDEO_V);
//                mOneKeyShare.shareVodWithoutPopWindow(new SinaWeibo(IfengApplication.getInstance()), shareTitle, shareImg, item.mUrl, true);
//                break;
//            case R.id.video_detail_qq:
//                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE_QQ_QUICK, PageIdConstants.PLAY_VIDEO_V);
//                mOneKeyShare.shareVodWithoutPopWindow(new QQ(IfengApplication.getInstance()), shareTitle, shareImg, item.mUrl, false);
//                break;
//            default:
//                break;
//        }
    }

    private String guid;
    private String title;
    private String imgeUrl;

    public void setShareParm(String guid, String title, String imgUrl) {
        this.guid = guid;
        this.title = title;
        this.imgeUrl = imgUrl;
    }
}
