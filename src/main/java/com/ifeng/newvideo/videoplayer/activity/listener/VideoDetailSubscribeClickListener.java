package com.ifeng.newvideo.videoplayer.activity.listener;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.SubscribeWeMediaUtil;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.model.SubscribeRelationModel;
import com.ifeng.video.dao.db.model.subscribe.SubscribeWeMediaResult;

/**
 * Created by fanshell on 2016/7/31.
 * 订阅的单击事件
 */
public class VideoDetailSubscribeClickListener implements View.OnClickListener {

    private Context context;
    private TextView mFollowView;
    private boolean isClickSubscribeBtn = false;//未登录状态下是否点击了订阅按钮
    private VideoItem item;

    public boolean isClickSubscribeBtn() {
        return isClickSubscribeBtn;
    }

    public void setClickSubscribeBtn(boolean clickSubscribeBtn) {
        isClickSubscribeBtn = clickSubscribeBtn;
    }

    public void setVideoItem(VideoItem videoItem) {
        this.item = videoItem;
    }

    @Override
    public void onClick(View view) {
        this.context = view.getContext();
        this.mFollowView = (TextView) view;
        int subscribe = (int) view.getTag(R.id.video_detail_follow);

        if (!NetUtils.isNetAvailable(context)) {
            ToastUtils.getInstance().showShortToast(R.string.toast_no_net);
            return;
        }

        WeMedia media = (WeMedia) view.getTag();
        if (media == null || TextUtils.isEmpty(media.id)) {
            return;
        }
        User user = new User(context);
        if (User.isLogin()) {
            subscribe = subscribe == SubscribeRelationModel.SUBSCRIBE ? SubscribeRelationModel.UN_SUBSCRIBE : SubscribeRelationModel.SUBSCRIBE;
            sendSubScribe(media, user.getUid(), subscribe);
            CommonStatictisListUtils.getInstance().sendYoukuConstatic(item, CommonStatictisListUtils.YK_SUBSCIRBE, CommonStatictisListUtils.YK_FEED_DETAIL);
            PageActionTracker.clickWeMeidaSub(subscribe == SubscribeRelationModel.SUBSCRIBE, PageIdConstants.PLAY_VIDEO_V);
        } else {
            isClickSubscribeBtn = true;
            IntentUtils.startLoginActivity(context);
        }
        isUserClicked = true;
    }

    // fix bug 新浪微博分享后自动登录 && 当前自媒体用户已订阅 ---> 弹出订阅成功Toast
    private boolean isUserClicked = true;

    public void setUserClicked(boolean userClicked) {
        isUserClicked = userClicked;
    }

    private void sendSubScribe(final WeMedia media, String uid, final int subscribe) {
        SubscribeWeMediaUtil.subscribe(media.id, uid, subscribe, System.currentTimeMillis() + "",
                SubscribeWeMediaResult.class,
                new Response.Listener<SubscribeWeMediaResult>() {
                    @Override
                    public void onResponse(SubscribeWeMediaResult response) {
                        if (response == null || TextUtils.isEmpty(response.toString()) || response.getResult() == 0) {
                            showFailToast(subscribe);
                            return;
                        }
                        boolean isSubscribe = subscribe == SubscribeRelationModel.SUBSCRIBE;
                        sendSubscribeStatistics(media.id, ColumnRecord.TYPE_WM, isSubscribe, media.name);
                        setFollowViewStyle(isSubscribe);
                        if (isSubscribe) {
                            ToastUtils.getInstance().showShortToast(R.string.toast_subscribe_success);
                        } else {
                            if (isUserClicked) {
                                ToastUtils.getInstance().showShortToast(R.string.toast_cancel_subscribe_success);
                            }
                        }
                        mFollowView.setTag(R.id.video_detail_follow, subscribe);//重新设置Tag值,保证再次点击获取“是否订阅”关系正确
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showFailToast(subscribe);
                    }
                });

    }

    /**
     * 失败Toast
     */
    private void showFailToast(int type) {
        int msg;
        boolean subscribe = type == SubscribeRelationModel.SUBSCRIBE;
        msg = subscribe ? R.string.toast_subscribe_fail : R.string.toast_cancel_subscribe_fail;
        ToastUtils.getInstance().showShortToast(msg);
    }

    /**
     * 发送订阅/退订统计
     *
     * @param id     自媒体id
     * @param type   自媒体填写：ColumnRecord.WM
     * @param action 是否订阅
     * @param title  自媒体名称
     */
    public void sendSubscribeStatistics(String id, String type, boolean action, String title) {
        ColumnRecord columnRecord = new ColumnRecord(id, type, action, title);
        CustomerStatistics.sendWeMediaSubScribe(columnRecord);
        SendSmartStatisticUtils.sendWeMediaOperatorStatistics(context, action, title, "");
    }

    /**
     * 设置订阅样式
     *
     * @param isFollowed 是否订阅
     */
    private void setFollowViewStyle(boolean isFollowed) {
        if (isFollowed) {
            mFollowView.setText(R.string.subscribed);
            mFollowView.setTextColor(context.getResources().getColor(R.color.subscribed_text_color));
            mFollowView.setBackgroundResource(R.drawable.btn_subscribe_shape_gray_border);
        } else {
            mFollowView.setText(R.string.subscribe);
            mFollowView.setTextColor(context.getResources().getColor(R.color.add_subscribe_text_color));
            mFollowView.setBackgroundResource(R.drawable.btn_subscribe_shape_border);
        }
    }
}
