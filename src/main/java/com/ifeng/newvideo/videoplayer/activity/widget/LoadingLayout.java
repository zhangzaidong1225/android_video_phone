package com.ifeng.newvideo.videoplayer.activity.widget;

import android.content.Context;
import android.support.annotation.IntDef;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.ifeng.newvideo.R;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.video.core.utils.NetUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by fanshell on 2016/8/11.
 */
public class LoadingLayout extends RelativeLayout implements View.OnClickListener {
    public static final int STATUS_LOADING = 1;
    public static final int STATUS_NORMAL = 2;
    public static final int STATUS_ERROR = 3;
    private OnLoadDataListener mListener;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.no_net_layer:
            case R.id.load_data_fail_layer:
                if (mListener != null) {
                    mListener.onLoadData();
                }
                break;
        }
    }


    @IntDef({STATUS_LOADING, STATUS_NORMAL, STATUS_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LoadingStatus {
    }

    private View mLoadingView;
    private View mNoNetView;
    private View mLoadErrorView;
    private MyPullToRefreshListView mPullRefreshView;

    public LoadingLayout(Context context) {
        this(context, null);
    }


    public LoadingLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.common_loading_layout, this, true);
        mLoadingView = findViewById(R.id.loading_layer);
        mNoNetView = findViewById(R.id.no_net_layer);
        mLoadErrorView = findViewById(R.id.load_data_fail_layer);
        mPullRefreshView = (MyPullToRefreshListView) findViewById(R.id.listView);
        initStatus();
        mNoNetView.setOnClickListener(this);
        mLoadErrorView.setOnClickListener(this);
    }

    private void initStatus() {
        mLoadingView.setVisibility(GONE);
        mNoNetView.setVisibility(View.GONE);
        mLoadErrorView.setVisibility(View.GONE);
        mPullRefreshView.setVisibility(View.GONE);
    }

    /**
     * 展示布局
     */
    public void setShowStatus(@LoadingStatus int status) {
        if (status == STATUS_LOADING) {
            mLoadingView.setVisibility(VISIBLE);
            mNoNetView.setVisibility(View.GONE);
            mLoadErrorView.setVisibility(View.GONE);
            mPullRefreshView.setVisibility(View.GONE);
        } else if (status == STATUS_NORMAL) {
            mPullRefreshView.setVisibility(View.VISIBLE);
            mLoadingView.setVisibility(GONE);
            mNoNetView.setVisibility(View.GONE);
            mLoadErrorView.setVisibility(View.GONE);
        } else if (status == STATUS_ERROR) {
            mLoadingView.setVisibility(GONE);
            mPullRefreshView.setVisibility(View.GONE);
            mLoadErrorView.setVisibility(View.GONE);
            mNoNetView.setVisibility(View.GONE);
            if (NetUtils.isNetAvailable(this.getContext())) {
                mLoadErrorView.setVisibility(View.VISIBLE);
            } else {
                mNoNetView.setVisibility(View.VISIBLE);
            }

        } else {
            mLoadingView.setVisibility(VISIBLE);
            mNoNetView.setVisibility(View.GONE);
            mPullRefreshView.setVisibility(View.GONE);
            mPullRefreshView.setVisibility(View.GONE);
        }

    }

    public MyPullToRefreshListView getRefreshView() {
        return mPullRefreshView;
    }

    public void setLoadDataListener(OnLoadDataListener listener) {
        this.mListener = listener;
    }

    public interface OnLoadDataListener {
        void onLoadData();
    }


}
