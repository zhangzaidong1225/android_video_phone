package com.ifeng.newvideo.videoplayer.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.dialogUI.CommentEditFragment;
import com.ifeng.newvideo.dialogUI.DanmakuCommentFragment;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.statistics.smart.SmartStatistic;
import com.ifeng.newvideo.statistics.smart.domains.UserOperator;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.ad.ADJumpType;
import com.ifeng.newvideo.ui.ad.AdTools;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.utils.ScreenUtils;
import com.ifeng.newvideo.utils.StreamUtils;
import com.ifeng.newvideo.videoplayer.activity.widget.DetailVideoHeadView;
import com.ifeng.newvideo.videoplayer.activity.widget.LoadingLayout;
import com.ifeng.newvideo.videoplayer.activity.widget.TopicBottomView;
import com.ifeng.newvideo.videoplayer.adapter.VideoPlayDetailAdapter;
import com.ifeng.newvideo.videoplayer.bean.DanmuItem;
import com.ifeng.newvideo.videoplayer.bean.SubTopicItem;
import com.ifeng.newvideo.videoplayer.bean.TopicDetailList;
import com.ifeng.newvideo.videoplayer.bean.TopicItem;
import com.ifeng.newvideo.videoplayer.bean.TopicPlayerItem;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteItem;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteResultArray;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteResultItem;
import com.ifeng.newvideo.videoplayer.bean.UITopicData;
import com.ifeng.newvideo.videoplayer.bean.VideoDanmuItem;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.bean.WeMedia;
import com.ifeng.newvideo.videoplayer.dao.TopicVideoDao;
import com.ifeng.newvideo.videoplayer.player.IPlayer;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.DanmuEditView;
import com.ifeng.newvideo.videoplayer.widget.skin.PlayButton;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.videoplayer.widget.skin.controll.DefaultControllerView;
import com.ifeng.ui.pulltorefreshlibrary.MyPullToRefreshListView;
import com.ifeng.ui.pulltorefreshlibrary.PullToRefreshBase;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import com.ifeng.video.dao.db.dao.CommentDao;
import com.ifeng.video.dao.db.dao.CommonDao;
import com.ifeng.video.dao.db.model.CommentInfoModel;
import com.ifeng.video.dao.db.model.MainAdInfoModel;
import com.ifeng.video.dao.util.AdsExposureSesssion;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 专题播放页面
 * Created by cuihz on 2014/9/12.
 * 默认播放第一条
 * topic id topic type
 */
public class ActivityTopicPlayer extends BaseFragmentActivity implements NotifyShareCallback, AudioService.CanPlayAudio,
        DefaultControllerView.OnShowOrHideDanmuView, DanmuEditView.OnClickEditButton, PlayButton.OnPlayOrPauseListener {

    private Logger logger = LoggerFactory.getLogger(ActivityTopicPlayer.class);
    /**
     * 头部相关的View back title,share
     */
    private View mTopView;

    private View mShareView;
    private NetworkImageView mTopImgView;

    /**
     * ListView相关的数据
     */
    private MyPullToRefreshListView mTopicListView;
    private VideoPlayDetailAdapter mAdapter;
    private LoadingLayout mLoadingLayout;

    private TopicBottomView mBottomView;
//    private TopicVoteView mBottomView;

    // 用于adapter显示
    private List<UITopicData> mUiTopicData = new ArrayList<>();
    // 用于播放视频，不含广告
    private List<VideoItem> mPlayVideoItems = new ArrayList<>();

    /**
     * 播放器及皮肤相关的处理
     */
    private VideoSkin mVideoSkin;
    private NormalVideoHelper mVideoHelper;
    private UIPlayContext mUIPlayContext;

    private VideoItem mCurrentVideoItem;

    // 首页列表页的topicId
    private String mItemId;
    // 首页列表页的topicType
    private String itemType;

    private String echid = "";
    private String chid = "";
    private boolean isFromCache;
    private List<MainAdInfoModel> mAdvertList = new ArrayList<>();

    //精选传入分享数据，需记录最初的videoItem
    private String homeTitle = null;
    private String homeImg = null;
    private String originGuid = null;

    private String mTopicShareUrl;

    /**
     * //是否来自悬浮或通知
     */
    private boolean isFromFloatBarOrNotify;

    private boolean isCanPost;
    //弹幕数据相关列表
    private List<String> keyList = new ArrayList<>();
    private List<VideoDanmuItem> danmuItems = new ArrayList<>();
    private List<Integer> tmpList = new ArrayList<>();
    private DanmakuCommentFragment mDanmakuEditFragment;
    private boolean isEditDanma = false;

    //评论相关的
    private CommentEditFragment mCommentEditFragment;
    private int mPageNum = 1;
    private volatile boolean isRefresh = false;
    private static final String PAGE_SIZE = DataInterface.PAGESIZE_10;

    //底部评论
    private View mCommentInputView;
    private RelativeLayout rl_bottom;
    private LinearLayout ll_bottom;
    private LinearLayout ll_bottom_view;
    private View mCommentView;
    private TextView commentNum;
    private ImageView commentIcon;
    //回到顶部
    private RelativeLayout rl_up;
    private int mHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(null);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        super.onCreate(savedInstanceState);
        if (getIntent() == null) {
            this.finish();
            return;
        }
        initParam();
        if (TextUtils.isEmpty(mItemId) || TextUtils.isEmpty(itemType)) {
            this.finish();
            return;
        }
        if (!isFromFloatBarOrNotify) {
            if (ActivityMainTab.mAudioService != null) {
                ActivityMainTab.mAudioService.stopAudioService(VideoSkin.SKIN_TYPE_SPECIAL);
            }

        }
        setContentView(R.layout.topic_play_layout);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        enableExitWithSlip(false);
        mOneKeyShare = new OneKeyShare(this);
        mHeight = DisplayUtils.getWindowHeight();
        initTopView();
        initListView();
        initTopicRelativeView();
        initBottomView();
        initSkin();
        loadData();
        mFocusList = new ArrayList<>(20);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initParam();
        if (TextUtils.isEmpty(mItemId) || TextUtils.isEmpty(itemType)) {
            this.finish();
            return;
        }

    }

    @Override
    protected void onScreenOff() {
        if (mVideoHelper != null) {
            mVideoHelper.sendScreenOffStatistics();
        }
    }

    @Override
    protected boolean isExit(MotionEvent ev) {
        if (isLandScape()) {
            return false;
        }

        int playerHeight = DisplayUtils.getWindowWidth() * 9 / 16
                + DisplayUtils.getStatusBarHeight() + DisplayUtils.convertDipToPixel(100);

        if (ev != null && ev.getY() > playerHeight) {
            return true;
        }
        return false;
    }

    private void initListView() {
        mLoadingLayout = (LoadingLayout) findViewById(R.id.topic_video_loading_layout);
        mLoadingLayout.setLoadDataListener(new LoadingLayout.OnLoadDataListener() {
            @Override
            public void onLoadData() {
                loadData();
            }
        });
        mTopicListView = mLoadingLayout.getRefreshView();
        mTopicListView.setShowIndicator(false);
        mAdapter = new VideoPlayDetailAdapter(this);
//        mBottomView = new TopicVoteView(this);
        mTopicListView.setMode(PullToRefreshBase.Mode.DISABLED);
        mTopicListView.hideFootView();


//        mTopicListView.getRefreshableView().addHeaderView(mBottomView, null, false);
        mTopicListView.setAdapter(mAdapter);
        mAdapter.setOnLoadFailClick(new VideoPlayDetailAdapter.OnLoadFailedClick() {
            @Override
            public void onLoadFailedClick() {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_RETRY, PageIdConstants.PLAY_TOPIC_V);
            }
        });

        mTopicListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            @Override
            public void onLastItemVisible() {
                if (mAdapter.isShowEmptyView()) {
                    return;
                }
                if (!NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
            }
        });

        rl_up = (RelativeLayout) findViewById(R.id.rl_up);
        rl_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTopicListView.getRefreshableView().setSelection(0);
                rl_up.setVisibility(View.GONE);
            }
        });

        mTopicListView.getRefreshableView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int scrollY = getScrollHeight(view, firstVisibleItem);
                if (scrollY >= 0 && scrollY <= mHeight * 2) {
                    rl_up.setVisibility(View.GONE);
                } else if (scrollY > mHeight * 2) {
                    if (!ScreenUtils.isLand()) {
                        rl_up.setVisibility(View.VISIBLE);
                        rl_up.bringToFront();
                    } else {
                        rl_up.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void clickToAdActivity(UITopicData object) {
        if (object == null || object.item == null) {
            return;
        }
        TopicPlayerItem mAdMaterial = object.item;
        String adId = mAdMaterial.getAdId();
        String title = mAdMaterial.getText();
        String imageUrl = mAdMaterial.getImageURL();

        String adType = "";
        String clickUrl = "";
        List<String> downloadCompletedUrls = null;
        MainAdInfoModel.AdMaterial.AdAction adAction = mAdMaterial.getAdAction();
        if (adAction != null) {
            if (IfengType.TYPE_BROWSER.equalsIgnoreCase(adAction.getType())) {
                adType = IfengType.TYPE_BROWSER;
            } else {
                adType = IfengType.TYPE_WEBFULL;
            }

            clickUrl = adAction.getUrl();

            // 如果是广告app下载类型，点击地址取loadingUrl
            if (mAdMaterial.getAdConditions() != null && "app".equalsIgnoreCase(mAdMaterial.getAdConditions().getShowType())) {
                clickUrl = adAction.getLoadingurl();
            }

            if (!ListUtils.isEmpty(adAction.getAsync_downloadCompletedurl())) {
                downloadCompletedUrls.addAll(adAction.getAsync_downloadCompletedurl());
            }
            if (!ListUtils.isEmpty(adAction.getDownloadCompletedurl())) {
                downloadCompletedUrls.addAll(adAction.getDownloadCompletedurl());
            }

            //广告点击曝光
            AdvertExposureDao.sendAdvertClickReq(adId, (ArrayList<String>) adAction.getAsync_click());
        }

        ADJumpType.jump(adId, adType, title, imageUrl, clickUrl, clickUrl, null, null, this,
                downloadCompletedUrls, "", "", adAction != null ? adAction.getAsync_download() : null);
    }

    private void clickToPlayVideo(UITopicData realData) {
        if (realData == null || realData.item == null || mCurrentVideoItem.guid.equals(realData.item.guid)) {
            return;
        }
        mUIPlayContext.isFromPush = false;
        mCurrentVideoItem = realData.item;
        playVideo();

    }

    private void initParam() {
        mItemId = getIntent().getStringExtra(IntentKey.TOPIC_ID);
        itemType = getIntent().getStringExtra(IntentKey.TOPIC_TYPE);
        echid = getIntent().getStringExtra(IntentKey.E_CHID);
        chid = getIntent().getStringExtra(IntentKey.CHID);
        isFromCache = getIntent().getExtras().getBoolean(IntentKey.IS_FROM_CACHE, false);

        homeTitle = getIntent().getExtras().getString(IntentKey.HOME_VIDEO_TITLE);
        homeImg = getIntent().getExtras().getString(IntentKey.HOME_VIDEO_IMG);

        //------------------from
        isFromFloatBarOrNotify = getIntent().getBooleanExtra(AudioService.EXTRA_FROM, false);
        if (isFromFloatBarOrNotify) {
            mItemId = PlayQueue.getInstance().getCurrentVideoItem().itemId;
            itemType = PlayQueue.getInstance().getCurrentVideoItem().topicType;
            echid = PlayQueue.getInstance().getEchid();
        }

    }

    private void initTopView() {
        mTopView = findViewById(R.id.topic_video_title);
        View mBackView = findViewById(R.id.topic_video_top_back);
        mShareView = findViewById(R.id.topic_video_top_share);
        mTopImgView = (NetworkImageView) findViewById(R.id.topic_video_top_img);
        mBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK, PageIdConstants.PLAY_TOPIC_V);
                ActivityTopicPlayer.this.finish();
            }
        });
        mShareView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetUtils.isNetAvailable(ActivityTopicPlayer.this)) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE, PageIdConstants.PLAY_TOPIC_V);
                shareTopicVideo();
            }
        });
    }

    private void initTopicRelativeView() {
        mBottomView = new TopicBottomView(this);
        mTopicListView.getRefreshableView().addHeaderView(mBottomView, null, false);
        mBottomView.setOnItemClickListener(new TopicBottomView.OnItemClickListener() {
            @Override
            public void onItemClick(UITopicData videoItem) {

                if (videoItem.isTitle) {
                    return;
                }
                if (videoItem.isVideo) {
                    clickToPlayVideo(videoItem);
                } else if (videoItem.isAd) {
                    clickToAdActivity(videoItem);
                }
                destoryAndCreateDanmuView();
                requestVideoDanmuData(videoItem.item.guid);
                requesetDanmaAllowSend(videoItem.item.guid);
//                requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
                isEditDanma = false;

            }
        });

        mBottomView.setOnErrorListener(new DetailVideoHeadView.onErrorListener() {
            @Override
            public void onVideoErrorClick() {
                loadTopicDetail();
                loadTopicVoteRecord();
            }

            @Override
            public void onRelativeErrorClick() {

            }
        });
    }

    private void initBottomView() {
        rl_bottom = (RelativeLayout) findViewById(R.id.rl_bottom);
        ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);
        mCommentInputView = findViewById(R.id.comment_input);
        mCommentInputView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetUtils.isNetAvailable(view.getContext())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (mCurrentVideoItem == null) {
                    return;
                }
                showEditCommentWindow("", "");
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_INPUT, PageIdConstants.PLAY_TOPIC_V);
            }
        });

        ll_bottom_view = (LinearLayout) findViewById(R.id.ll_bottom_view);
        mCommentView = findViewById(R.id.video_detail_bottom_comment);
        commentIcon = (ImageView) mCommentView.findViewById(R.id.video_detail_bottom_comment_icon);
        commentNum = (TextView) mCommentView.findViewById(R.id.tv_comment_number);

        mCommentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int headCount = mTopicListView.getRefreshableView().getHeaderViewsCount();
                mTopicListView.getRefreshableView().setSelection(headCount);

                if (mAdapter != null) {
                    boolean isAllowComment = mAdapter.getData() != null && mAdapter.getData().isAllowComment();
                    if (mAdapter.isShowEmptyView() && isAllowComment) {
                        showEditCommentWindow("", "");
                    }
                }
                PageActionTracker.clickBtn(ActionIdConstants.CLICK_COMMENT_ICON, PageIdConstants.PLAY_TOPIC_V);

            }
        });
    }


    private void initSkin() {
        if (isFromFloatBarOrNotify) {
            mVideoHelper = new NormalVideoHelper(NormalVideoHelper.CONTROLLER_TYPE_AUDIO);
        } else {
            mVideoHelper = new NormalVideoHelper();
        }
        mUIPlayContext = new UIPlayContext();
        mUIPlayContext.isFromPush = getIntent() != null && getIntent().getBooleanExtra(IntentKey.IS_FROM_PUSH, false);
        mUIPlayContext.channelId = echid;
        mUIPlayContext.isFromCache = isFromCache;
        mUIPlayContext.mFromForAd = itemType;
        mUIPlayContext.isTopic = true;
        mUIPlayContext.skinType = VideoSkin.SKIN_TYPE_TOPIC;
        mVideoSkin = (VideoSkin) findViewById(R.id.video_skin);
        mVideoHelper.init(mVideoSkin, mUIPlayContext);
        mVideoSkin.setNoNetWorkListener(new VideoSkin.OnNetWorkChangeListener() {
            @Override
            public void onNoNetWorkClick() {
                if (mCurrentVideoItem == null) {
                    loadData();
                } else {
                    logger.debug("open video");
                    playVideo();
                }
                pauseAndHideDanmuView();
                String page = isLandScape() ? PageIdConstants.PLAY_TOPIC_H : PageIdConstants.PLAY_TOPIC_V;
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_RETRY, null, page);
            }

            @Override
            public void onMobileClick() {
                if (mCurrentVideoItem == null) {
                    loadData();
                } else {
                    logger.debug("open video");
                    playVideo();
                }
                pauseAndHideDanmuView();
                String page = isLandScape() ? PageIdConstants.PLAY_TOPIC_H : PageIdConstants.PLAY_TOPIC_V;
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_CONTINUE, null, page);
            }
        });

        mVideoSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener() {
            @Override
            public void onLoadFailedListener() {
                if (mCurrentVideoItem == null) {
                    loadData();
                } else {
                    playVideo();
                }
                pauseAndHideDanmuView();
                String page = isLandScape() ? PageIdConstants.PLAY_TOPIC_H : PageIdConstants.PLAY_TOPIC_V;
                PageActionTracker.clickPlayerBtn(ActionIdConstants.CLICK_PLAY_RETRY, null, page);
            }
        });
        mVideoHelper.setOnAutoPlayListener(new IPlayController.OnAutoPlayListener() {
            @Override
            public void onPlayListener(VideoItem item) {
                Log.d("topic-------", "title:" + item.name);
//                mAdapter.changeHighlight(item);
                mBottomView.changeTextColor(item);
                mCurrentVideoItem = item;
                mUIPlayContext.videoItem = item;
                mUIPlayContext.title = TextUtils.isEmpty(item.title) ? item.name : item.title;
                updateCommentIcon(item.commentNo);
                destoryAndCreateDanmuView();
                if (!mUIPlayContext.isAdvert) {
                    if (NetUtils.isMobile(ActivityTopicPlayer.this) && IfengApplication.mobileNetCanPlay) {
                        ToastUtils.getInstance().showShortToast(R.string.play_moblie_net_hint);
                    }
                }
                isEditDanma = false;

                requestVideoDanmuData(mCurrentVideoItem.guid);
//                requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
                requesetDanmaAllowSend(mCurrentVideoItem.guid);

            }
        });
        mVideoSkin.getNoNetWorkView().hideBackView();
        setVideoSkinListener();

    }

    private void setVideoSkinListener() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getBaseControlView().setShowListener(this);
            initDanmuViewStatus();
        }
        if (null != mVideoSkin.getPlayView()) {
            mVideoSkin.getPlayView().setPlayOrPauseListener(this);
        }

        if (null != mVideoSkin.getDanmuEditView()) {
            mVideoSkin.getDanmuEditView().setEditListener(this);
        }
    }


    private void initDanmuViewStatus() {
        if (!IfengApplication.danmaSwitchStatus) {
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuEditView() && null != mVideoSkin.getBaseControlView()) {
                mVideoSkin.getDanmuView().hideView();
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
                mVideoSkin.getDanmuView().setShowEditView(false);
                mVideoSkin.getDanmuView().initDanmukuView();
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }

        } else {
            if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuEditView() && null != mVideoSkin.getBaseControlView()) {
                mVideoSkin.getDanmuView().showView();
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
                mVideoSkin.getDanmuView().initDanmukuView();
                mVideoSkin.getBaseControlView().setShowControlView(true);
            }
        }
    }

    private void destoryAndCreateDanmuView() {
        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().clearDanmaku();
            mVideoSkin.getDanmuView().onDestory();

            mVideoSkin.getDanmuView().showView();
            mVideoSkin.getDanmuView().initEmptyDanmakuView();
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getDanmuView().showDanmukuView();
                //mVideoSkin.getDanmuEditView().showView();
            } else {
                mVideoSkin.getDanmuView().hideDanmakuView();
                mVideoSkin.getDanmuEditView().hideView();
            }
        }
    }

    private void pauseAndHideDanmuView() {
        if (null != mVideoSkin.getDanmuView()) {
            if (null == mVideoSkin.getDanmuView().getDanmakuView()) {
                mVideoSkin.getDanmuView().initEmptyDanmakuView();
                mVideoSkin.getDanmuView().showDanmukuView();
            }
            mVideoSkin.getDanmuView().getDanmakuView().pause();
            mVideoSkin.getDanmuView().getDanmakuView().hide();
        }
    }

    private void loadData() {
        logger.debug("load data");
//        loadAdvert();
        if (isFromFloatBarOrNotify) {
            updateTopicList(PlayQueue.getInstance().getTopicList());
            return;
        }
        mLoadingLayout.setShowStatus(LoadingLayout.STATUS_LOADING);
        loadTopicVoteRecord();
        loadTopicDetail();
    }


    /**
     * 加载信息流的广告数据
     */
/*    private void loadAdvert() {
        if (PhoneConfig.isGooglePlay()) {
            return;
        }
        VideoTopicAdvertManager mTopicAdvertManager = new VideoTopicAdvertManager();
        mTopicAdvertManager.setOnTopicAdvertListener(new VideoTopicAdvertManager.OnTopicAdvertListener() {
            @Override
            public void onTopicAdvertSuccess(List<MainAdInfoModel> ads) {
                mAdvertList.addAll(ads);
                if (mAdapter.getCount() > 0) {
                    mAdapter.setAdInformation(mAdvertList.get(0));
                }
                logger.debug("{}", mAdvertList.toString());

                // 曝光
                adExpose(ads);
            }

            @Override
            public void onTopicAdvertFailed() {
                logger.debug("fetch failed");
            }
        });

        mTopicAdvertManager.getTopicAdvert(itemType, mItemId, PhoneConfig.UID);
    }*/

    /**
     * 加载专辑的数据信息
     */
    private void loadTopicDetail() {
        TopicVideoDao.getTopicDetailList(mItemId, itemType, new Response.Listener<TopicDetailList>() {
                    @Override
                    public void onResponse(TopicDetailList response) {
                        PlayQueue.getInstance().setTopicList(response);
                        PlayQueue.getInstance().setEchid(echid);
                        updateTopicList(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mLoadingLayout.setShowStatus(LoadingLayout.STATUS_ERROR);
                        playVideo();
                    }
                });
    }

    private void updateTopicList(TopicDetailList response) {
        if (response == null) {
            return;
        }
        updateTopAndHeadView(response);
        mTopicShareUrl = response.topicShareURL;
        mUiTopicData.clear();
        List<SubTopicItem> subTopicItems = response.subTopicList;
        if (ListUtils.isEmpty(subTopicItems)) {
            return;
        }
        filterTopic(subTopicItems);
        mLoadingLayout.setShowStatus(LoadingLayout.STATUS_NORMAL);

        if (!ListUtils.isEmpty(mUiTopicData)) {
            if (!isFromFloatBarOrNotify) {
                mUiTopicData.get(0).isHighlight = true;
                mCurrentVideoItem = mPlayVideoItems.get(0);
                originGuid = mCurrentVideoItem.guid;
            }
            mBottomView.updateTopicVideoView(mUiTopicData);
        }

        if (mCurrentVideoItem != null) {
            playVideo();
            String weMediaId = TextUtils.isEmpty(mCurrentVideoItem.weMedia.id) ? "" : mCurrentVideoItem.weMedia.id;
            mOneKeyShare.initShareStatisticsData(mCurrentVideoItem.guid, echid, VodRecord.convertTopicChid(mItemId, itemType),
                    weMediaId, PageIdConstants.PLAY_TOPIC_V);
            mOneKeyShare.initSmartShareData(itemType, mCurrentVideoItem.weMedia.name, mCurrentVideoItem.title);

            updateCommentIcon(mCurrentVideoItem.commentNo);

            loadComment(false);
            requestVideoDanmuData(mCurrentVideoItem.guid);
//        requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
            requesetDanmaAllowSend(mCurrentVideoItem.guid);
        }
    }

    private void loadTopicVoteRecord() {
        TopicVideoDao.getTopicVoteInfo(mItemId,
                new Response.Listener<TopicVoteItem>() {
                    @Override
                    public void onResponse(TopicVoteItem response) {
                        if (null == response || EmptyUtils.isEmpty(response)) {
                            return;
                        }
                        Log.d("vote", response.toString() + "");
                        handleTopicVote(response);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    }

    private void handleTopicVote(TopicVoteItem response) {
        if (!ListUtils.isEmpty(filterTopicVoteData(response))) {
            updateTopicVoteView(response, voteData);
        } else {
            mBottomView.setVoteVisible();
        }
    }

    private List<TopicVoteResultArray> voteData = new ArrayList<>();

    private List<TopicVoteResultArray> filterTopicVoteData(TopicVoteItem response) {
        voteData.clear();
        if ("1".equals(response.getIfsuccess())) {
            if (!ListUtils.isEmpty(response.getData().getResult())) {
                List<TopicVoteResultItem> data = response.getData().getResult();
                for (TopicVoteResultItem resultItem : data) {
                    if (resultItem.getResultArray().getOption().size() != 2) {
                        continue;
                    }
                    voteData.add(resultItem.getResultArray());
                    break;
                }
                return voteData;
            }
        }
        return voteData;
    }

    private void updateTopicVoteView(TopicVoteItem response, List<TopicVoteResultArray> resultData) {
        mBottomView.updateVoteView(response, resultData);
    }


    /**
     * 赞助位广告曝光
     *
     * @param ads 广告的实体列表
     */
    private void adExpose(List<MainAdInfoModel> ads) {
        if (ListUtils.isEmpty(ads)) {
            return;
        }
        for (MainAdInfoModel adInfoModel : ads) {
            MainAdInfoModel model = AdTools.filterMainADModel(adInfoModel);
            if (AdTools.isValidAdModel(model)) {
                List<MainAdInfoModel.AdMaterial> materials = model.getAdMaterials();
                if (!ListUtils.isEmpty(materials)) {
                    for (MainAdInfoModel.AdMaterial material : materials) {
                        AdTools.exposeAdPvUrl(material);
                    }
                }
            }
        }
    }

    private void filterTopic(List<SubTopicItem> subTopicItems) {
        for (SubTopicItem item : subTopicItems) {
            if (item == null || ListUtils.isEmpty(item.detailList)) {
                continue;
            }

            // 加title
            String subTitle = item.subTitle;
            UITopicData titleItem = new UITopicData();
            titleItem.topicName = subTitle;
            titleItem.isTitle = true;
            mUiTopicData.add(titleItem);

            // 加video、ad
            for (TopicItem topicItem : item.detailList) {
                if (topicItem == null) {
                    continue;
                }
                UITopicData videoOrAdItem = new UITopicData();
                TopicPlayerItem memberItem = topicItem.memberItem;
                if (CheckIfengType.isVideo(topicItem.memberType)) {
                    if (isInvalidVideoItem(memberItem)) {
                        continue;
                    } else {
                        videoOrAdItem.isVideo = true;
                        mPlayVideoItems.add(memberItem);
                    }
                } else if (CheckIfengType.isAdBackend(topicItem.memberType)) {
                    if (isInvalidAdItem(memberItem)) {
                        continue;
                    }
                    videoOrAdItem.isAd = true;
                }
                videoOrAdItem.topicName = subTitle;
                memberItem.topicId = mItemId;//设置TopicId、TopicType
                memberItem.topicType = itemType;
                videoOrAdItem.item = memberItem;
                videoOrAdItem.item.image = topicItem.thumbImageUrl;
                videoOrAdItem.item.topicType = topicItem.memberType;
                videoOrAdItem.item.searchPath = topicItem.memberItem.searchPath;

                if (!TextUtils.isEmpty(topicItem.itemId) && topicItem.itemId.equals(mItemId)) {
                    videoOrAdItem.isHighlight = true;
                    mCurrentVideoItem = PlayQueue.getInstance().getCurrentVideoItem();
                }
                mUiTopicData.add(videoOrAdItem);
            }

            // 容错：如果接口数据detailList中的数据都不可用时，舍弃这条
            if (mUiTopicData.size() == 1 && mUiTopicData.get(0).isTitle) {
                mUiTopicData.clear();
            }
        }
    }

    private boolean isInvalidVideoItem(TopicPlayerItem memberItem) {
        return memberItem == null ||
                TextUtils.isEmpty(memberItem.guid) ||
                TextUtils.isEmpty(memberItem.name) ||
                ListUtils.isEmpty(memberItem.videoFiles);
    }

    private boolean isInvalidAdItem(TopicPlayerItem memberItem) {
        return memberItem == null ||
                TextUtils.isEmpty(memberItem.getAdId());
    }

    private void updateTopAndHeadView(TopicDetailList response) {
        if (!ListUtils.isEmpty(response.extendData) && !TextUtils.isEmpty(response.extendData.get(0).image)) {
            mTopImgView.setImageUrl(response.extendData.get(0).image, VolleyHelper.getImageLoader());
        }
        mBottomView.setDesText(response.desc);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mVideoHelper != null && !isEditDanma) {
            mVideoHelper.onResume();
        }
        if (null != mVideoSkin && null != mVideoSkin.getDanmuView() && !isEditDanma) {
            if (IfengApplication.danmaSwitchStatus) {
                mVideoSkin.getBaseControlView().setShowControlView(true);
            } else {
                mVideoSkin.getBaseControlView().setShowControlView(false);
            }
            mVideoSkin.getDanmuView().onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoHelper != null) {
            mVideoHelper.onPause();
        }
        if (null != mVideoSkin && null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TopicVideoDao.cancelAll();
        VolleyHelper.getRequestQueue().cancelAll(VideoTopicAdvertManager.TAG);
        removeAdExposeIds();

        if (mVideoHelper != null) {
            mVideoHelper.onDestroy();
        }

        if (null != mVideoSkin && null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().onDestory();
        }
        sendStaticList();
    }

    private void sendStaticList() {
        mFocusList = CommonStatictisListUtils.videoDetailFocusList;
        CommonStatictisListUtils.getInstance().sendStatictisList(mFocusList);
        mFocusList.clear();
        CommonStatictisListUtils.videoDetailFocusList.clear();
    }

    /**
     * 退出页面清除专题广告曝光id，下次进入重新开始发pvurl和adpvurl，不清除的话会第二次进入只发adpvurl
     */
    private void removeAdExposeIds() {
        for (UITopicData data : mUiTopicData) {
            if (data.isAd && data.item != null && !TextUtils.isEmpty(data.item.getAdId())) {
                AdsExposureSesssion.getInstance().remove(data.item.getAdId());
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mVideoHelper != null) {
            mVideoHelper.onConfigureChange(newConfig);
        }
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (null != mDanmakuEditFragment && !mDanmakuEditFragment.isHidden()) {
                mDanmakuEditFragment.dismissAllowingStateLoss();
            }
            if (mCommentEditFragment != null && !mCommentEditFragment.isHidden()) {
                mCommentEditFragment.dismissAllowingStateLoss();
            }
            rl_up.setVisibility(View.GONE);
            ll_bottom.setVisibility(View.GONE);
            rl_bottom.setVisibility(View.GONE);
        } else {
            ll_bottom.setVisibility(View.VISIBLE);
            rl_bottom.setVisibility(View.VISIBLE);
        }
        updateTopViewShowStatus(newConfig);

    }

    private void updateTopViewShowStatus(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mTopView.setVisibility(View.GONE);
            mVideoSkin.getLoadView().showBackView();
            mVideoSkin.getNoNetWorkView().showBackView();
        } else {
            mTopView.setVisibility(View.VISIBLE);
            mVideoSkin.getLoadView().hideBackView();
            mVideoSkin.getNoNetWorkView().hideBackView();
        }
    }

    private void playCurrentVideoItem(TopicPlayerItem item) {
        if (mCurrentVideoItem != null && !TextUtils.isEmpty(mCurrentVideoItem.guid)
                && mCurrentVideoItem.guid.equals(item.guid)) {
            return;
        }

        mCurrentVideoItem = resetVideoItemIfNecessary(item);
        if (mCurrentVideoItem != null) {
            mUIPlayContext.isRelate = true;
            mUIPlayContext.isFromPush = false;
            mVideoHelper.setBookMark(0);
            mVideoHelper.setFromHistory(false);
            updateCommentIcon(mCurrentVideoItem.commentNo);
            playVideo();

        }
    }

    private VideoItem resetVideoItemIfNecessary(TopicPlayerItem item) {
        if (item != null && item.guid != null && item.guid.equals(originGuid)) {
            if (!TextUtils.isEmpty(homeTitle)) {
                item.title = homeTitle;
            }
            if (!TextUtils.isEmpty(homeImg)) {
                item.image = homeImg;
            }
        }
        return item;
    }


    private void playVideo() {
        if (mCurrentVideoItem == null) {
            mVideoHelper.openVideo("");
            return;
        }

        mBottomView.changeTextColor(mCurrentVideoItem);

        if (NetUtils.isMobile(this) && IfengApplication.mobileNetCanPlay) {
            ToastUtils.getInstance().showShortToast(R.string.play_moblie_net_hint);
        }
        String title = mCurrentVideoItem.title;
        if (TextUtils.isEmpty(title)) {
            mCurrentVideoItem.title = mCurrentVideoItem.name;
            title = mCurrentVideoItem.name;
        }
        mUIPlayContext.title = title;
        mVideoSkin.getLoadView().updateText();
        mUIPlayContext.videoItem = mCurrentVideoItem;
        mUIPlayContext.videoItem.topicId = mItemId;//设置TopicId、TopicType
        mUIPlayContext.videoItem.topicType = itemType;
        mUIPlayContext.streamType = StreamUtils.getStreamType();
        mUIPlayContext.VideoItemList = mPlayVideoItems;
        String realPath;
        if (mVideoHelper.isPlayAudio()) {
            realPath = StreamUtils.getMp3Url(mCurrentVideoItem.videoFiles);
            PlayQueue.getInstance().setVideoItem(mCurrentVideoItem);
        } else {
            realPath = StreamUtils.getMediaUrl(mCurrentVideoItem.videoFiles);
        }
        mVideoHelper.openVideo(realPath);
        requestVideoDanmuData(mCurrentVideoItem.guid);
//        requestVideoDanmuData("cacbfdc2-26fd-46ac-927d-14224e8f4326");
        requesetDanmaAllowSend(mCurrentVideoItem.guid);
        CommonStatictisListUtils.getInstance().sendYoukuConstatic(mCurrentVideoItem, CommonStatictisListUtils.YK_PLAY, CommonStatictisListUtils.YK_FEED_DETAIL);
    }

    private OneKeyShare mOneKeyShare;

    private void shareTopicVideo() {
        if (mOneKeyShare == null) {
            mOneKeyShare = new OneKeyShare(this);
        }
        if (mCurrentVideoItem != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            String name = mCurrentVideoItem.title;
            String image = mCurrentVideoItem.image;
            //精选首页进来，分享标题和图片为推荐位标题和图片
            if (mCurrentVideoItem.guid.equals(originGuid) && !TextUtils.isEmpty(homeTitle) && !TextUtils.isEmpty(homeImg)) {
                name = homeTitle;
                image = homeImg;
            }

            if (!TextUtils.isEmpty(mTopicShareUrl)) {
                String topicShareUrl = mTopicShareUrl + "#" + mCurrentVideoItem.guid;
                mOneKeyShare.shareTopic(name,
                        image,
                        topicShareUrl,
                        mShareView,
                        this,
                        false);
            }
        }
    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageTopicPlay(isLandScape(), "", "");
        } else {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public boolean isPlayAudio() {
        return mVideoHelper != null && mVideoHelper.isPlayAudio();
    }

    private void requestVideoDanmuData(String guid) {
        String url = DataInterface.getVideoDanmuUrl(guid);

        CommonDao.sendRequest(url, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        try {
                            JSONObject jsonObj = new JSONObject(response.toString());
                            String responseStr = response.toString();
                            if (responseStr.contains("code")) {
                                if (!jsonObj.getString("code").equals("1")) {
                                    if (IfengApplication.danmaSwitchStatus) {
                                        danmuItems.clear();
                                        ToastUtils.getInstance().showShortToast("获取弹幕信息失败，请稍后重试。");
                                    }
                                    return;
                                }
                            }

                            keyList.clear();
                            tmpList.clear();
                            Iterator<String> iterable = jsonObj.keys();
                            while (iterable.hasNext()) {
                                String str = iterable.next();
                                int index = str.indexOf("t");
                                tmpList.add(Integer.parseInt(str.substring(index + 1, str.length())));
                            }
                            Collections.sort(tmpList);
                            for (int i = 0; i < tmpList.size(); i++) {
                                String tmpStr = "t" + tmpList.get(i);
                                keyList.add(tmpStr);
                            }
                            getVideoDammukuData(response.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    private void getVideoDammukuData(String jsonStr) {
        if (TextUtils.isEmpty(jsonStr)) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);

            danmuItems.clear();
            int arratSize = keyList.size();
            for (int i = 0; i < arratSize; i++) {
                JSONArray array = jsonObject.getJSONArray(keyList.get(i));
                int size = array.length();
                for (int j = 0; j < size; j++) {
                    VideoDanmuItem item = new Gson().fromJson(array.get(j).toString(), VideoDanmuItem.class);
                    item.setFromUser(false);
                    danmuItems.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().addDataSource(danmuItems);
        }
    }

    private void requesetDanmaAllowSend(String guid) {
        String url = DataInterface.getVideoEditUrl(guid);
        Log.d("danmu", "IsSend:" + url);
        CommonDao.sendRequest(url, DanmuItem.class, new Response.Listener<DanmuItem>() {
            @Override
            public void onResponse(DanmuItem response) {
                if (response == null || TextUtils.isEmpty(response.toString())) {
                    return;
                }
                DanmuItem item = response;
                if (!TextUtils.isEmpty(item.getCode())) {
                    if (item.getCode().equals("1")) {
                        if ("1".equals(item.getData().getCan_post())) {
                            isCanPost = true;
                            setDanmuSendStatus(isCanPost);
                        } else {
                            isCanPost = false;
                            setDanmuSendStatus(isCanPost);
                        }
                    } else {
                        isCanPost = false;
                        setDanmuSendStatus(isCanPost);
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, CommonDao.RESPONSE_TYPE_POST_JSON);
    }

    public void setDanmuSendStatus(boolean isSend) {
        if (null != mVideoSkin.getDanmuView()) {
            if (!isSend) {
                mVideoSkin.getDanmuEditView().hideView();
            }
            mVideoSkin.getDanmuView().setShowEditView(isSend);
        }
    }

    @Override
    public void onClickEditButton() {
        if (mVideoHelper != null) {
            mVideoHelper.onPause();
        }
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }
        isEditDanma = true;
        showEditDanmaWindow("", "");
    }


    @Override
    public void onPausePlayButton() {
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }
    }

    @Override
    public void onPlayButton() {
        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }
    }


    public void showEditDanmaWindow(String comments, String comment_id) {
        if (mDanmakuEditFragment == null) {
            mDanmakuEditFragment = new DanmakuCommentFragment();
        }

        mDanmakuEditFragment.setShowInputMethod(true);
        mDanmakuEditFragment.setCommends(comments, comment_id);
        mDanmakuEditFragment.setVideoItem(mCurrentVideoItem);
        mDanmakuEditFragment.setNormalVideoPlayer(mVideoHelper);

        if (!mDanmakuEditFragment.isAdded() && !this.isFinishing()) {
            mDanmakuEditFragment.show(getSupportFragmentManager(), "danmuDialog");
        }

    }

    public void updateLocalDanmu(String comment) {

        if (null != mVideoHelper) {
            mVideoHelper.onResume();
        }

        if (null != mVideoSkin.getDanmuView() && null != mVideoSkin.getDanmuView().getDanmakuView()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
            mVideoSkin.getDanmuView().resumeDanmaku();
        }

        if (null != mVideoSkin.getDanmuView()) {
            mVideoSkin.getDanmuView().sendTextMessage(comment);
        }

        isEditDanma = false;
    }

    @Override
    public void showDanmuView(boolean isShow) {
        if (isShow) {
            mVideoSkin.getDanmuView().showView();
            if (isCanPost) {
                mVideoSkin.getDanmuEditView().showView();
            }
            IfengApplication.danmaSwitchStatus = true;
            showDanmuViewAndResume();
        } else {
            mVideoSkin.getDanmuView().hideView();
            mVideoSkin.getDanmuView().hideDanmakuView();
            mVideoSkin.getDanmuEditView().hideView();
            IfengApplication.danmaSwitchStatus = false;
        }
    }

    private void showDanmuViewAndResume() {
        mVideoSkin.getDanmuView().initEmptyDanmakuView();
        mVideoSkin.getDanmuView().showDanmukuView();
        if (mVideoSkin.getDanmuView().getDanmakuView().isPaused()) {
            mVideoSkin.getDanmuView().getDanmakuView().resume();
        }
        if (mUIPlayContext.status != IPlayer.PlayerState.STATE_PAUSED) {
            mVideoSkin.getDanmuView().resumeDanmaku();
        } else {
            mVideoSkin.getDanmuView().getDanmakuView().pause();
        }
    }

    /**
     * 用于显示评论对话框
     *
     * @param comments 评论
     */
    public void showEditCommentWindow(String comments, String comment_id) {
        if (mCommentEditFragment == null) {
            mCommentEditFragment = new CommentEditFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putCharSequence(IntentKey.E_CHID, echid);
        if (mCommentEditFragment.getArguments() != null) {
            mCommentEditFragment.getArguments().putCharSequence(IntentKey.E_CHID, echid);
        } else {
            mCommentEditFragment.setArguments(bundle);
        }
        if (mAdapter != null && mAdapter.isShowEmptyView()) {
            mCommentEditFragment.setShowInputMethod(true);
        } else {
            mCommentEditFragment.setShowInputMethod(true);
        }
        mCommentEditFragment.setCommends(comments, comment_id);
        mCommentEditFragment.setTopicId(mTopicShareUrl, homeTitle);
        if (!mCommentEditFragment.isAdded() && !this.isFinishing()) {
            mCommentEditFragment.show(getSupportFragmentManager(), "dialog");

        }
        mVideoHelper.setProgrammeAutoNext(false);

    }

    private synchronized void loadComment(final boolean mIsLoadMore) {
        if (isRefresh) {
            return;
        }

        isRefresh = true;
        mTopicListView.showFootView();
        if (null == mCurrentVideoItem) {
            mTopicListView.hideFootView();
            mTopicListView.onRefreshComplete();
            mTopicListView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAdapter.showErrorView();
                }
            }, 500);
//            isRefresh = false;
            return;
        }

        mTopicListView.setMode(PullToRefreshBase.Mode.DISABLED);
        if (mIsLoadMore) {
            mAdapter.setVideoItem(mCurrentVideoItem);
        } else {
            mPageNum = 1;
        }

        String paramsBuilder = "?docUrl=" + mTopicShareUrl +
                "&p=" + mPageNum +
                "&pagesize=" + PAGE_SIZE +
                "&type=new" +
                "&job=1";

        CommentDao.getComments(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                mTopicListView.hideFootView();
                mTopicListView.onRefreshComplete();
                boolean isCommentEmpty = response == null
                        || ((CommentInfoModel) response).getComments() == null
                        || ListUtils.isEmpty(((CommentInfoModel) response).getComments().getNewest());

                boolean isAllowComment = response != null && ((CommentInfoModel) response).isAllowComment();
                mCommentInputView.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                mCommentView.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                if (!ScreenUtils.isLand()) {
                    ll_bottom.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                    rl_bottom.setVisibility(isAllowComment ? View.VISIBLE : View.GONE);
                } else {
                    ll_bottom.setVisibility(View.GONE);
                    rl_bottom.setVisibility(View.GONE);
                }

                if (!isAllowComment || isCommentEmpty) {
                    handleCommentEmpty(mIsLoadMore);
                    isRefresh = false;
                    return;
                }

                handleCommentData((CommentInfoModel) response, mIsLoadMore);
                isRefresh = false;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mPageNum == 1) {
                    mAdapter.showErrorView();
                } else {
                    if (!NetUtils.isNetAvailable(ActivityTopicPlayer.this)) {
                        ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    } else {
                        ToastUtils.getInstance().showShortToast(R.string.common_load_data_error);
                    }
                }

                mTopicListView.hideFootView();
                mTopicListView.onRefreshComplete();
                isRefresh = false;
            }
        }, paramsBuilder);

        if (mIsLoadMore) {
            PageActionTracker.clickBtn(ActionIdConstants.PULL_MORE, PageIdConstants.PLAY_TOPIC_V);
        }
    }

    private void handleCommentEmpty(boolean isLoadMore) {
        if (!isLoadMore) {
            setEmptyView();
        } else {
            noMore();
        }
    }

    private void setEmptyView() {
        mTopicListView.hideFootView();
        mTopicListView.onRefreshComplete();
        mTopicListView.setMode(PullToRefreshBase.Mode.DISABLED);
        mAdapter.showEmptyView();
    }

    private void noMore() {
        ToastUtils.getInstance().showShortToast(R.string.toast_no_more);
        mTopicListView.hideFootView();
        mTopicListView.onRefreshComplete();
    }

    private synchronized void handleCommentData(CommentInfoModel commentInfoModel, boolean mIsLoadMore) {
        // CommentInfoModel.Comments comments = commentInfoModel.getComments();
        // List<CommentInfoModel.Newest> newestList = comments.getNewest();
        if (!mIsLoadMore) {
            mAdapter.setData(commentInfoModel);
        } else {
            mAdapter.addData(commentInfoModel);
        }
        mPageNum++;
    }


    public void sendCommentAction() {
        UserOperator comment = new UserOperator(this);
        comment.setOperation(UserOperatorConst.OPERATION_COMMENT);
        comment.setType(UserOperatorConst.TYPE_VIDEO);
        if (mCurrentVideoItem != null) {
            comment.setTitle(mCurrentVideoItem.title);
            WeMedia weMedia = mCurrentVideoItem.weMedia;
            if (weMedia != null) {
                String name = mCurrentVideoItem.weMedia.name;
                if (!TextUtils.isEmpty(name)) {
                    comment.setKeyword(name);
                }

            }
        }
        //上报评论
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.OPERATION_URL, comment);
    }


    /**
     * 当在本地发送成功以后，模拟数据更新Adapter
     *
     * @param comment 评论
     */
    public void updateLocalData(String comment) {
        /**
         * 构建一个本地的数据评价
         */
        CommentInfoModel.Newest localNewest = new CommentInfoModel.Newest();
//        User user = new User(this);
        localNewest.setUname(User.getUserName());
        localNewest.setComment_contents(comment);
        localNewest.setComment_date(DateUtils.parseTimeStamp(System.currentTimeMillis(), "yyyy-MM-dd HH:mm"));
        localNewest.setUptimes("0");
        localNewest.setUser_id(User.getUid());
        localNewest.setFaceurl(User.getUserIcon());
        CommentInfoModel.UserRole role = new CommentInfoModel.UserRole();
        CommentInfoModel.Video video = new CommentInfoModel.Video();
        video.setVip(User.isVip() ? 1 : 0);
        role.setVideo(video);
        localNewest.setUser_role(role);
        mAdapter.addLocalData(localNewest);
    }

    private void updateCommentIcon(String commentNo) {
        if (commentNo != null) {
            int number = Integer.parseInt(commentNo);
            commentNum.setVisibility(View.VISIBLE);
            if (0 == number) {
                commentNum.setVisibility(View.GONE);
            } else if (number > 999) {
                commentNum.setText("999+");
            } else {
                commentNum.setText(commentNo);
            }
        }
    }
}
