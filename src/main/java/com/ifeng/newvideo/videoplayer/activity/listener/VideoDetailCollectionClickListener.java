package com.ifeng.newvideo.videoplayer.activity.listener;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.ifeng.newvideo.BuildConfig;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.CollectRecord;
import com.ifeng.newvideo.statistics.smart.SendSmartStatisticUtils;
import com.ifeng.newvideo.utils.CommonStatictisListUtils;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.FavoritesDAO;
import com.ifeng.video.dao.db.model.FavoritesModel;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

/**
 * Created by fanshell on 2016/7/31.
 * 收藏的单击事件
 */
public class VideoDetailCollectionClickListener implements View.OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(VideoDetailCollectionClickListener.class);
    private String mEchid;
    private String resourceType;//资源类型，视频或音频：FavoritesModel.RESOURCE_VIDEO、FavoritesModel.RESOURCE_AUDIO
    private UIPlayContext mUIPlayContext;

    public VideoDetailCollectionClickListener(String resourceType, String echid, UIPlayContext uiPlayContext) {
        this.resourceType = resourceType;
        mEchid = echid;
        mUIPlayContext = uiPlayContext;
    }

    @Override
    public void onClick(View view) {
        if (!NetUtils.isNetAvailable(view.getContext())) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        VideoItem videoItem = (VideoItem) view.getTag();
        if (videoItem == null) {
            if (BuildConfig.DEBUG) {
                ToastUtils.getInstance().showShortToast("数据有点小问题");
            }
            return;
        }
        FavoritesModel model = FavoritesDAO.getInstance(view.getContext()).getFavoriteVideo(videoItem.guid, resourceType);
        ImageView mCollectionIcon;
        if (resourceType.equals(FavoritesModel.RESOURCE_VIDEO)) {
            mCollectionIcon = (ImageView) view.findViewById(R.id.video_detail_header_collect_icon);
        } else {
            mCollectionIcon = (ImageView) view.findViewById(R.id.fm_detail_bottom_collect_icon);
        }
        boolean hasCollected = false;
        if (model == null) {
            model = new FavoritesModel();
            model.setProgramGuid(videoItem.guid);
            model.setType(resourceType.equals(IfengType.TYPE_AUDIO) ? FavoritesModel.FAVORITES_FM_TYPE : FavoritesModel.TYPE_VOD);//FM或点播
            model.setResource(resourceType);
            model.setPlayTime(String.valueOf(videoItem.duration));
            model.setImgUrl(videoItem.image);
            model.setName(videoItem.title);
            model.setVideoDuration(videoItem.duration);
            model.setFmProgramId(videoItem.itemId);
            int fileSize;
            if (!ListUtils.isEmpty(videoItem.videoFiles) && videoItem.videoFiles.get(0) != null) {
                fileSize = videoItem.videoFiles.get(0).filesize;
            } else {
                fileSize = 0;
            }
            model.setFileSize(fileSize);
            if (resourceType.equals(IfengType.TYPE_AUDIO)) {
                model.setFmUrl(videoItem.mUrl);
            }
            FavoritesDAO.getInstance(view.getContext()).saveFavoritesData(model);
            hasCollected = true;
            mCollectionIcon.setImageResource(R.drawable.video_player_bottom_collect_selected);
            ToastUtils.getInstance().showShortToast(R.string.favorites_success);
        } else {
            try {
                FavoritesDAO.getInstance(view.getContext()).delete(videoItem.guid, resourceType);
            } catch (Exception e) {
                logger.error("取消收藏 error! {}", e);
            }
            hasCollected = false;
            mCollectionIcon.setImageResource(R.drawable.video_player_bottom_collect);
            ToastUtils.getInstance().showShortToast(R.string.favorites_cancel);
        }

        String weMediaId = videoItem.weMedia == null ? "" : videoItem.weMedia.id;
        String weMediaName = videoItem.weMedia == null ? "" : videoItem.weMedia.name;

        String searchPath = TextUtils.isEmpty(videoItem.searchPath) ? "" : videoItem.searchPath;


        SendSmartStatisticUtils.sendCollectionOperatorStatistics(view.getContext(), hasCollected, weMediaName, videoItem.title);
        if (resourceType == IfengType.TYPE_AUDIO) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_COLLECT, hasCollected, PageIdConstants.PLAY_FM_V);
            CustomerStatistics.sendCollectRecord(new CollectRecord(model.getProgramGuid(), mUIPlayContext.isRelate ? "" : mEchid,
                    searchPath, weMediaId, hasCollected, PageIdConstants.PLAY_FM_V));
        } else {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_COLLECT, hasCollected, PageIdConstants.PLAY_VIDEO_V);
            CustomerStatistics.sendCollectRecord(new CollectRecord(model.getProgramGuid(), mUIPlayContext.isRelate ? "" : mEchid,
                    searchPath, weMediaId, hasCollected, PageIdConstants.PLAY_VIDEO_V));
        }

        CommonStatictisListUtils.getInstance().sendYoukuConstatic(videoItem, CommonStatictisListUtils.YK_FAVOURITE, CommonStatictisListUtils.YK_FEED_DETAIL);

    }


}
