package com.ifeng.newvideo.videoplayer.activity;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.coustomshare.NotifyShareCallback;
import com.ifeng.newvideo.coustomshare.OneKeyShare;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.activity.listener.VideoDetailCollectionClickListener;
import com.ifeng.newvideo.videoplayer.activity.listener.VideoDetailDownloadClickListener;
import com.ifeng.newvideo.videoplayer.bean.FileType;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.newvideo.videoplayer.fragment.AudioFMDescFragment;
import com.ifeng.newvideo.videoplayer.fragment.AudioFMListFragment;
import com.ifeng.newvideo.videoplayer.player.NormalVideoHelper;
import com.ifeng.newvideo.videoplayer.player.PlayQueue;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.newvideo.videoplayer.player.playController.IPlayController;
import com.ifeng.newvideo.videoplayer.widget.skin.UIPlayContext;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.newvideo.widget.PagerSlidingTabStrip;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.FavoritesDAO;
import com.ifeng.video.dao.db.model.FavoritesModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ActivityAudioFMPlayer extends BaseFragmentActivity implements View.OnClickListener, NotifyShareCallback, AudioFMListFragment.OnEpisodeTaskListener, AudioService.CanPlayAudio {
    Logger logger = LoggerFactory.getLogger(ActivityAudioFMPlayer.class);
    /**
     * 播放器相关
     */
    private VideoSkin mSkin;
    private UIPlayContext mPlayContext;
    private NormalVideoHelper mPlayerHelper;

    private PagerSlidingTabStrip mSlidingTabStrip;
    private ViewPager mViewPager;
    private FmPagerAdapter mPagerAdapter;
    /**
     * 底部相关的View
     */
    private View mDownloadView;
    private ImageView downLoadIcon;
    private View mCollectionView;
    private ImageView mCollectionIcon;
    private View mShareView;
    OneKeyShare mOneKeyShare;

    private final List<VideoItem> programList = new ArrayList<VideoItem>();
    private int pageNum = 1;
    private VideoItem mCurrentProgram;
    private int mCurPos;

    //节目的id
    protected String mProgramId;
    //首次传入的guid
    private String firstGuid;
    //音频地址目前只有从收藏页面或者看过页面跳过来会传这个值，其它页面跳过来的都会是空的
    private String audioUrl, imageurl, title;
    private long bookMark;

    public String echid;
    public String vTag;
    private String mProgramType;
    private Bundle bundle;
    private String EXTRA_FROM = "service_from";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //不是来自service，先停止播放
        if (getIntent() != null && !getIntent().getBooleanExtra(EXTRA_FROM, false)) {
            if (ActivityMainTab.mAudioService != null) {
                ActivityMainTab.mAudioService.stopAudioService(VideoSkin.SKIN_TYPE_SPECIAL);
            }
        }
        setContentView(R.layout.activity_audio_fm_detail);
        enableExitWithSlip(false);
        setAnimFlag(ANIM_FLAG_LEFT_RIGHT);
        initData();
        initSkin();
        initView();
    }


    private void initSkin() {
        mSkin = (VideoSkin) findViewById(R.id.fm_skin);
        mPlayContext = new UIPlayContext();
        mPlayContext.skinType = VideoSkin.SKIN_TYPE_FM;
        mPlayerHelper = new NormalVideoHelper(NormalVideoHelper.CONTROLLER_TYPE_AUDIO);
        mPlayerHelper.init(mSkin, mPlayContext);
        mSkin.setNoNetWorkListener(new VideoSkin.OnNetWorkChangeListener() {
            @Override
            public void onNoNetWorkClick() {
                requestData();
            }

            @Override
            public void onMobileClick() {
                requestData();
            }
        });

        mSkin.setOnLoadFailedListener(new VideoSkin.OnLoadFailedListener() {
            @Override
            public void onLoadFailedListener() {
                requestData();
            }
        });

        mPlayerHelper.setOnPlayCompleteListener(null);
        mPlayerHelper.setOnAutoPlayListener(new IPlayController.OnAutoPlayListener() {
            @Override
            public void onPlayListener(VideoItem item) {
                mCurPos = getCurrPlayFmIndexByGuid(item.guid);
                playProgram(mCurPos, false);
            }
        });

    }

    private void initData() {
        Intent intent = getIntent();
        if (intent.getBooleanExtra(EXTRA_FROM, false)) {
            if (PlayQueue.getInstance().getCurrentVideoItem() == null) {
                finish();
                return;
            }
            mProgramId = PlayQueue.getInstance().getCurrentVideoItem().itemId;
            firstGuid = PlayQueue.getInstance().getCurrentVideoItem().guid;
            programList.clear();
            programList.addAll(PlayQueue.getInstance().getPlayList());
            pageNum = PlayQueue.getInstance().getPageNum();
            echid = PlayQueue.getInstance().getEchid();
            vTag = VodRecord.V_TAG_FM_HPAGE;
        } else {
            mProgramId = intent.getStringExtra(IntentKey.AUDIO_PROGRAM_ID);
            mProgramType = intent.getStringExtra(IntentKey.AUDIO_FM_TYPE);
            firstGuid = intent.getStringExtra(IntentKey.AUDIO_FM_GUID);
            audioUrl = intent.getStringExtra(IntentKey.AUDIO_FM_URl);
            imageurl = intent.getStringExtra(IntentKey.AUDIO_FM_IMAGE_URl);
            title = intent.getStringExtra(IntentKey.AUDIO_FM_NAME);
            echid = intent.getStringExtra(IntentKey.E_CHID);
            vTag = intent.getStringExtra(IntentKey.FM_STATISTICS_V_TAG);
            bookMark = intent.getLongExtra(IntentKey.HISTORY_BOOK_MARK, 0);
        }
        bundle = new Bundle();
        bundle.putString(IntentKey.AUDIO_PROGRAM_ID, mProgramId);
        bundle.putString(IntentKey.AUDIO_FM_TYPE, mProgramType);
        bundle.putString(IntentKey.E_CHID, echid);
        if (programList.size() > 0) {
            bundle.putSerializable(IntentKey.AUDIO_PAGE_NUM, pageNum);
        }
    }

    public void initView() {
        mSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.fm_activity_slidingtab);
        mSlidingTabStrip.setShouldExpand(true);
        mViewPager = (ViewPager) findViewById(R.id.fm_activity_pager);
        initAdapter();
        initBottomView();

        //如果从服务传入programList数据不为空，播放并选中当前位置
        if (pageNum > 1 && programList.size() > 0) {
            mCurPos = getCurrPlayFmIndexByGuid(firstGuid);
            playProgram(mCurPos, true);
        } else {
            //从播放记录或收藏传入播放数据，设置当前播放数据（可能不在第一页数据里，下拉加载到有该数据才选中定位）
            if (!TextUtils.isEmpty(audioUrl) && !TextUtils.isEmpty(imageurl) && !TextUtils.isEmpty(title)) {
                int duration = getIntent().getIntExtra(IntentKey.FM_DURATION, 0);
                int fileSize = getIntent().getIntExtra(IntentKey.FM_FILE_SIZE, 0);
                logger.debug("duration={},fileSize={}", duration, fileSize);
                setCurrentProgram(duration, fileSize);
            }
        }
    }

    private void requestData() {
        int size = programList.size();
        if (size > mCurPos) {
            playProgram(mCurPos, true);
        } else {
            //fix  首次使用运营商网络无法播放电台非首屏
            if (size > 0 && this.mCurrentProgram.guid.equalsIgnoreCase(firstGuid)) {
                playCurrentProgram();
                return;
            }
            if (mPagerAdapter != null && mPagerAdapter.getFMListFragment() != null) {
                mPagerAdapter.getFMListFragment().refreshData();
            }
        }
    }

    @Override
    public void onTaskEpisodeFinished(List<VideoItem> videoItems, boolean isDownRefresh) {
        if (videoItems != null) {
            //从服务进来programList不为空
            boolean isFirstPlay = (pageNum == 1 && programList.size() == 0);
            pageNum++;
            programList.addAll(videoItems);
            refreshPlayQueue();

            if (isFirstPlay) {
                mCurPos = getCurrPlayFmIndexByGuid(firstGuid);
                //播放数据不在播放列表里
                if (mCurrentProgram != null) {
                    if (mCurPos < programList.size()) {
                        setCurrentProgram(mCurPos);
                    }
                    playCurrentProgram();
                } else {
                    playProgram(mCurPos, true);
                }
            }
        } else {
            if (programList.size() == 0) {
                mPlayerHelper.openVideo("");
            }
        }
    }


    public AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0 && position <= programList.size() && mCurPos != position - 1) {
                mCurPos = position - 1;
                playProgram(mCurPos, true);
            }
        }
    };

    /**
     * 每次有新数据返回，更新播放列表。播放数据如果在列表中呈选中状态
     */
    public void refreshPlayQueue() {
        PlayQueue.getInstance().setPlayList(programList);
        PlayQueue.getInstance().setPageNum(pageNum);
        PlayQueue.getInstance().setEchid(echid);
        PlayQueue.getInstance().setVTag(vTag);
        //首次传入的guid的定位
        if (mCurrentProgram != null && firstGuid.equals(mCurrentProgram.guid)) {
            int position = getCurrPlayFmIndexByGuid(firstGuid);
            if (position < programList.size()) {
                if (mPagerAdapter != null && mPagerAdapter.getFMListFragment() != null) {
                    mPagerAdapter.getFMListFragment().setSelection(position);
                }
            }
        }
    }

    /**
     * 播放当前位置数据，如果数据不在列表中播放，播放（列表，收藏或缓存）传入的数据
     *
     * @param position
     * @param needPlay 是否需要播放当前地址（连播只需要重定位）
     */
    public void playProgram(int position, boolean needPlay) {

        setCurrentProgram(position);

        PlayQueue.getInstance().setVideoItem(mCurrentProgram);
        PlayQueue.getInstance().setEchid(echid);
        PlayQueue.getInstance().setVTag(vTag);
        mPlayContext.videoItem = mCurrentProgram;
        mPlayContext.title = mCurrentProgram.title;
        // mCurrentProgram.searchPath=chid;
        if (needPlay) {
            mPlayerHelper.openVideo(mCurrentProgram.mUrl);
        }
        if (NetUtils.isMobile(ActivityAudioFMPlayer.this) && IfengApplication.mobileNetCanPlay) {
            if (0 == SharePreUtils.getInstance().getMobileOrderStatus()) {
                ToastUtils.getInstance().showShortToast(R.string.play_moblie_net_hint);
            }
        }
    }

    public void setCurrentProgram(int position) {
        if (programList.size() <= position) {
            position = 0;
        }
        mCurrentProgram = programList.get(position);

        mCollectionView.setTag(mCurrentProgram);
        mDownloadView.setTag(mCurrentProgram);
        updateDownloadIcon(mCurrentProgram.guid);
        updateCollectIcon(mCurrentProgram.guid);

        if (mPagerAdapter != null && mPagerAdapter.getFMListFragment() != null) {
            mPagerAdapter.getFMListFragment().setSelection(position);
        }
    }

    public void playCurrentProgram() {
        mCollectionView.setTag(mCurrentProgram);
        mDownloadView.setTag(mCurrentProgram);
        updateDownloadIcon(mCurrentProgram.guid);
        updateCollectIcon(mCurrentProgram.guid);
        PlayQueue.getInstance().setVideoItem(mCurrentProgram);
        PlayQueue.getInstance().setEchid(echid);
        mPlayContext.videoItem = mCurrentProgram;
        mPlayContext.title = mCurrentProgram.title;
        mPlayerHelper.setBookMark(bookMark > 0 ? bookMark : 0);
        bookMark = 0;
        mPlayerHelper.openVideo(mCurrentProgram.mUrl);
        if (NetUtils.isMobile(ActivityAudioFMPlayer.this) && IfengApplication.mobileNetCanPlay) {
            ToastUtils.getInstance()
                    .showShortToast(R.string.play_moblie_net_hint);
        }
    }

    private void setCurrentProgram(int duration, int fileSize) {
        mCurrentProgram = new VideoItem();
        mCurrentProgram.mUrl = audioUrl;
        mCurrentProgram.image = imageurl;
        mCurrentProgram.title = title;
        mCurrentProgram.guid = firstGuid;
        mCurrentProgram.name = title;
        mCurrentProgram.itemId = mProgramId;
        mCurrentProgram.duration = duration;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        mCurrentProgram.createDate = sdf.format(new Date(System.currentTimeMillis()));
        List<FileType> videoFiles = new ArrayList<>();
        FileType fileType = new FileType();
        fileType.mediaUrl = audioUrl;
        fileType.filesize = fileSize;
        fileType.useType = "mp3";
        videoFiles.add(fileType);
        mCurrentProgram.videoFiles = videoFiles;
        mCurrentProgram.isConvertFromFM = true;
    }

    /**
     * 返回现在这个guid所对应的 位置
     */
    private int getCurrPlayFmIndexByGuid(String proGuid) {
        if (ListUtils.isEmpty(programList)) {
            return 0;
        }
        for (int i = 0; i < programList.size(); i++) {
            if (proGuid != null && programList.get(i) != null && programList.get(i).guid != null && proGuid.equals(programList.get(i).guid)) {
                return i;
            }
        }
        return Integer.MAX_VALUE;
    }

    public void initBottomView() {

        //缓存下载
        mDownloadView = findViewById(R.id.fm_detail_bottom_download);
        downLoadIcon = (ImageView) mDownloadView.findViewById(R.id.fm_detail_bottom_download_icon);
        updateDownloadIcon(firstGuid);
        mDownloadView.setOnClickListener(new VideoDetailDownloadClickListener(ActivityAudioFMPlayer.this, IfengType.TYPE_AUDIO));
        //收藏
        mCollectionView = findViewById(R.id.fm_detail_bottom_collect);
        mCollectionIcon = (ImageView) mCollectionView.findViewById(R.id.fm_detail_bottom_collect_icon);
        updateCollectIcon(firstGuid);
        mCollectionView.setOnClickListener(new VideoDetailCollectionClickListener(FavoritesModel.RESOURCE_AUDIO, echid, mPlayContext));
        //分享
        mOneKeyShare = new OneKeyShare(this);
        mShareView = findViewById(R.id.fm_detail_bottom_share);
        mShareView.setOnClickListener(this);
    }


    private void updateCollectIcon(String guid) {
        FavoritesModel model = FavoritesDAO.getInstance(this).getFavoriteVideo(guid, FavoritesModel.RESOURCE_AUDIO);
        if (model != null) {
            mCollectionIcon.setImageResource(R.drawable.video_player_bottom_collect_selected);
        } else {
            mCollectionIcon.setImageResource(R.drawable.video_player_bottom_collect);
        }
    }

    private void updateDownloadIcon(String guid) {
        boolean isInCache = CacheManager.isInCache(this, guid);
        downLoadIcon.setImageResource(isInCache ? R.drawable.video_playerbtn_audio_p : R.drawable.video_playerbtn_audio_n);
        mDownloadView.setEnabled(!isInCache);
    }


    /**
     * 初始化适配器
     */
    private void initAdapter() {
        mPagerAdapter = new FmPagerAdapter(getSupportFragmentManager());
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mPagerAdapter);
        mSlidingTabStrip.setViewPager(mViewPager);
        mPagerAdapter.notifyDataSetChanged();
        mSlidingTabStrip.notifyDataSetChanged();
        mViewPager.setCurrentItem(0);
        mSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_EPISODE, PageIdConstants.PLAY_FM_V);
                } else {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_DETAIL, PageIdConstants.PLAY_FM_V);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        //默认报一次选中选集的事件
        PageActionTracker.clickBtn(ActionIdConstants.CLICK_EPISODE, PageIdConstants.PLAY_FM_V);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fm_detail_bottom_share:
                if (!NetUtils.isNetAvailable(v.getContext())) {
                    ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
                    return;
                }
                if (mCurrentProgram != null && !TextUtils.isEmpty(mCurrentProgram.mUrl)) {
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_SHARE, PageIdConstants.PLAY_FM_V);
                    showSharePop();
                }
                break;
        }
    }


    /**
     * 分享
     */
    private void showSharePop() {
        if (mCurrentProgram != null) {
            OneKeyShareContainer.oneKeyShare = mOneKeyShare;
            String name = mCurrentProgram.title;
            String image = mCurrentProgram.image;
            String url = mOneKeyShare.getFMSingleShareUrl(mCurrentProgram.guid, mCurrentProgram.itemId);
            //精选首页进来，分享标题和图片为推荐位标题和图片
            if (mCurrentProgram.guid.equals(firstGuid) && !TextUtils.isEmpty(title) && !TextUtils.isEmpty(imageurl)) {
                name = title;
                image = imageurl;
            }
            mOneKeyShare.initShareStatisticsData(mCurrentProgram.guid, echid, "", "", PageIdConstants.PLAY_FM_V);
            mOneKeyShare.shareFMWithPopWindow(name,
                    image,
                    url,
                    mShareView,
                    this,
                    false);

        }
    }

    @Override
    public void notifyShare(EditPage page, boolean show) {

    }

    @Override
    public void notifyPlayerPauseForShareWebQQ(boolean isWebQQ) {

    }

    @Override
    public void notifyShareWindowIsShow(boolean isShow) {
        if (isShow) {
            PageActionTracker.endPageFmPlay(isLandScape(), "", "");
        } else {
            PageActionTracker.enterPage();
        }
    }

    @Override
    public boolean isPlayAudio() {
        return true;
    }

    class FmPagerAdapter extends FragmentStatePagerAdapter {

        AudioFMListFragment audioFMListFragment;

        public FmPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        public AudioFMListFragment getFMListFragment() {
            return audioFMListFragment;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                audioFMListFragment = AudioFMListFragment.newInstance(bundle, ActivityAudioFMPlayer.this, mOnItemClickListener, programList);
                if (programList.size() > 0) {
                    audioFMListFragment.setSelection(mCurPos);
                }
                return audioFMListFragment;
            } else {
                return AudioFMDescFragment.newInstance(bundle);
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "选集";
            } else {

                return "详情";
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPlayerHelper != null) {
            mPlayerHelper.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPlayerHelper != null) {
            mPlayerHelper.onPause();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mPlayerHelper.onConfigureChange(newConfig);

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayerHelper.setOnPlayCompleteListener(null);
        mPlayerHelper.setOnAutoPlayListener(null);
        mPlayerHelper.onDestroy();
    }

    protected boolean isExit(MotionEvent ev) {
        if (isLandScape()) {
            return false;
        }
        int playerHeight = DisplayUtils.getWindowWidth() * 9 / 16
                + DisplayUtils.getStatusBarHeight() + DisplayUtils.convertDipToPixel(30);
        if (ev != null && ev.getY() > playerHeight) {
            if (mViewPager.getCurrentItem() == 0) {
                return true;
            }
        }
        return false;
    }
}
