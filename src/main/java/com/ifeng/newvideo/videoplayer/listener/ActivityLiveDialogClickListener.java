package com.ifeng.newvideo.videoplayer.listener;

import android.view.WindowManager;
import com.ifeng.newvideo.dialogUI.DialogUtilsFor3G;
import com.ifeng.newvideo.ui.live.ActivityLive;
import com.ifeng.video.core.utils.DisplayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VideoPlayer的OnClickListener
 * Created by yuelong on 2014/10/10.
 */
public class ActivityLiveDialogClickListener implements DialogUtilsFor3G.DialogCallBackFor3G {

    private static final Logger logger = LoggerFactory.getLogger(ActivityLiveDialogClickListener.class);
    private final ActivityLive activityVideoPlayer;

    public ActivityLiveDialogClickListener(ActivityLive activityVideoPlayer) {
        this.activityVideoPlayer = activityVideoPlayer;
    }

    @Override
    public void onClickDialogBtn(int flag) {
        logger.debug("onClickDialogBtn start");
        switch (flag) {
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CANCEL:
                handleDialogUnicomCancel();
                break;
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CONTINUE:
                handleDialogUnicomContinue();
                break;
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_ORDER:

                break;
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_RETRY:

                break;
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CONTINUE_DOWNLOAD:
                break;
            case DialogUtilsFor3G.FLAG_UNICOM_DIALOG_CANCEL_DOWNLOAD:
                break;
            case DialogUtilsFor3G.FLAG_DIALOG_CONTINUE:
                handleDialogUnicomContinue();
                break;
            case DialogUtilsFor3G.FLAG_DIALOG_CANCEL:
            case DialogUtilsFor3G.FLAG_DIALOG_NO_MOBILE_OPEN:
                handleDialogBtnCancelDone();
                break;
        }
    }

    private void handleDialogUnicomContinue() {
        // activityLivePlayer.mIs3GMobileNetOpenDialogShown = false;
        if (activityVideoPlayer.mVideoView != null) {
            if (activityVideoPlayer.mVideoView.isPauseState()) {
                activityVideoPlayer.mVideoView.start();
            } else {
                activityVideoPlayer.prepareToPlay();
            }
        }
    }

    private void handleDialogUnicomCancel() {
        if (activityVideoPlayer.mVideoView != null) {
            activityVideoPlayer.mVideoView.stopPlayback();
            activityVideoPlayer.updateErrorPauseLayer(true);
        }
    }

    private void handleDialogBtnCancelDone() {
        if (activityVideoPlayer.isLandScape()) {
            activityVideoPlayer.hideController();
            DisplayUtils.setDisplayStatusBar(activityVideoPlayer, false);
            activityVideoPlayer.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            activityVideoPlayer.toPortrait();
        }
        if (activityVideoPlayer.isPlayerInit) {// v5.3.1_lx
            activityVideoPlayer.mVideoView.stopPlayback();
        }
        activityVideoPlayer.updateErrorPauseLayer(true);
    }
}
