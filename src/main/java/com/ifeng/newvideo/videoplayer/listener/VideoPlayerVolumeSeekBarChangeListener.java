package com.ifeng.newvideo.videoplayer.listener;

import android.media.AudioManager;
import com.ifeng.newvideo.videoplayer.activity.BaseVideoPlayerActivity;
import com.ifeng.newvideo.videoplayer.widget.SeekBarVer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 音量seekBar
 * Created by yuelong on 2014/10/18.
 */
public class VideoPlayerVolumeSeekBarChangeListener implements SeekBarVer.OnSeekBarChangeListenerVer {

    private static final Logger logger = LoggerFactory.getLogger(VideoPlayerVolumeSeekBarChangeListener.class);
    private final BaseVideoPlayerActivity videoPlayerActivity;
    public boolean isFirstVolumeChange = true;
    private final static int CONTROLLER_SHOW_TIME = 3000 * 1000; // s

    public VideoPlayerVolumeSeekBarChangeListener(BaseVideoPlayerActivity videoPlayerActivity) {
        this.videoPlayerActivity = videoPlayerActivity;
    }

    @Override
    public void onStopTrackingTouch(SeekBarVer seekBar) {
        videoPlayerActivity.mVideoController.show();
        videoPlayerActivity.mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
    }

    private boolean mInstantSeeking = false;

    @Override
    public void onStartTrackingTouch(SeekBarVer seekBar) {
        videoPlayerActivity.mVideoController.show(CONTROLLER_SHOW_TIME);
        if (mInstantSeeking) {
            videoPlayerActivity.mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        }
    }

    @Override
    public void onProgressChanged(SeekBarVer seekBar, int curVolume, boolean fromUser) {
        videoPlayerActivity.mCurVolume = videoPlayerActivity.getCurrentVolume();
        if (!isFirstVolumeChange) {
            if (curVolume > videoPlayerActivity.mMaxVolume) {
                //videoPlayerActivity.updateVolume(videoPlayerActivity.mMaxVolume);
            } else if (curVolume <= 0) {
               // videoPlayerActivity.updateVolume(0);
            } else {
                //videoPlayerActivity.updateVolume(curVolume);
            }
        }
        isFirstVolumeChange = false;
    }

}
