package com.ifeng.newvideo.videoplayer.listener;

import android.view.View;
import cn.sharesdk.alipay.friends.Alipay;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.cache.NetDealListener;
import com.ifeng.newvideo.cache.NetDealManager;
import com.ifeng.newvideo.coustomshare.OneKeyShareContainer;
import com.ifeng.newvideo.coustomshare.SharePlatform;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayer;
import com.ifeng.newvideo.videoplayer.activity.BaseVideoPlayerActivity;
import com.ifeng.video.core.utils.ClickUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ToastUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VideoPlayer的OnClickListener
 * Created by yuelong on 2014/10/10.
 */
public class VideoPlayerClickListener implements View.OnClickListener, SharePlatform {

    private static final Logger logger = LoggerFactory.getLogger(VideoPlayerClickListener.class);
    private final BaseVideoPlayerActivity activityVideoPlayer;
    public final NetDealManager netDealManager;
    private Platform platform;

    public VideoPlayerClickListener(BaseVideoPlayerActivity activityVideoPlayer) {
        this.activityVideoPlayer = activityVideoPlayer;
        netDealManager = new NetDealManager(activityVideoPlayer);
    }

    @Override
    public void onClick(View v) {
        if (ClickUtils.isFastDoubleClick()) {
            return;
        }

        /**
         * advertBackView = (ViewGroup) advertLayout.findViewById(R.id.iv_return_rl);
         advertMuteView = (ViewGroup)advertLayout.findViewById(R.id.rl_mute);
         advertDetailView = advertLayout.findViewById(R.id.tv_getdetail);
         advertFullScreenView = (ViewGroup) advertLayout.findViewById(R.id.rl_fullscreen);
         */
        int vId = v.getId();
        switch (vId) {
            case R.id.video_gesture_guide://手势指导层
                SharePreUtils.getInstance().setVodPlayGestureState(false);
                activityVideoPlayer.updateGuideView();
                break;
            case R.id.iv_return_rl://广告返回
                break;
            case R.id.video_ad_mute_rl://广告静音
                break;
            case R.id.tv_getdetail://广告详情
                break;
            case R.id.rl_fullscreen://广告全屏
                break;
            case R.id.video_error_retry_img://重试
                break;
            case R.id.video_mobile_continue://运营商网继续播放
                break;


        }

        if (activityVideoPlayer == null) {
            return;
        }
        switch (vId) {
            case R.id.video_land_right_stream_original: //原画
                //activityVideoPlayer.onRightStreamItemClick(MediaConstants.STREAM_ORIGINAL);
                break;
            case R.id.video_land_right_stream_supper: //超清
                //activityVideoPlayer.onRightStreamItemClick(MediaConstants.STREAM_SUPPER);
                break;
            case R.id.video_land_right_stream_high: //高清
                //activityVideoPlayer.onRightStreamItemClick(MediaConstants.STREAM_HIGH);
                break;
            case R.id.video_land_right_stream_mid: //标准
                //activityVideoPlayer.onRightStreamItemClick(MediaConstants.STREAM_MID);
                break;
            case R.id.video_land_right_stream_low: //流畅
                //activityVideoPlayer.onRightStreamItemClick(MediaConstants.STREAM_LOW);
                break;
            case R.id.share_iv_sina: // 分享-新浪微博
                platform = ShareSDK.getPlatform(SinaWeibo.NAME);
                shareToPlatform(platform, true);
                break;
            case R.id.share_iv_wechat: // 分享-微信
                platform = ShareSDK.getPlatform(Wechat.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.share_iv_qq: // 分享-QQ
                platform = ShareSDK.getPlatform(QQ.NAME);
                shareToPlatform(platform, false);
                break;

            case R.id.share_iv_wechat_moment: // 分享-微信朋友圈
                platform = ShareSDK.getPlatform(WechatMoments.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.share_iv_qzone: // 分享qq空间
                platform = ShareSDK.getPlatform(QZone.NAME);
                shareToPlatform(platform, false);
                break;
            case R.id.share_iv_alipay: // 分享 支付宝
                platform = ShareSDK.getPlatform(Alipay.NAME);
                shareToPlatform(platform, false);
                break;


            case R.id.rl_fullscreen: // 横竖屏互转
            case R.id.video_detail_landscape_top_back_btn_click://顶部返回键
            case R.id.iv_return_rl: //广告back键
                handleTitleBack();
                break;
            case R.id.off_line_down_load://缓存fragment“开始缓存”按钮
                handleToDownloadBtnCallBack();
                break;
            case R.id.off_line_cancel://缓存fragment“取消”按钮
                handleToCancel();
                break;
            case R.id.off_line_btn_LL://缓存fragment 清晰度按钮
                updateStreamSeleterView();
                break;
            case R.id.spinner_original_btn://缓存fragment 原画按钮
                handleDownloadOriginalStreamBtn();
                break;
            case R.id.spinner_supper_btn://缓存fragment 超清按钮
                handleDownloadSupperStreamBtn();
                break;
            case R.id.spinner_high_btn://缓存fragment 高清按钮
                handleDownloadHightStreamBtn();
                break;
            case R.id.spinner_normal_btn://缓存fragment 标清按钮
                handleDownloadMidStreamBtn();
                break;
            case R.id.spinner_low_btn://缓存fragment 流畅按钮
                handleDownloadLowStreamBtn();
                break;
//            case R.id.bottom_play_download_iv://竖屏缓存按钮
            case R.id.bottom_play_download_rl://竖屏缓存按钮
                handleDownloadFragment();
                break;

            case R.id.media_controller_back://竖屏常显示的返回按钮
                handleBack();
                break;
            case R.id.bottom_subscribe_iv://竖屏栏目订阅按钮
                handleSubscribeBtn();
                break;
//            case R.id.bottom_collect_iv://底部收藏按钮
            case R.id.bottom_collect_rl://底部收藏按钮
                handleBottomCollect();
                break;

            case R.id.video_ad_mute_rl:// 静音
                handleADBtnVolume();
                break;
            case R.id.btn_volume://播放器静音按钮
                handleBtnVolume();
                break;
            case R.id.pauseToResume://播放器播放按钮
            case R.id.video_error_retry_img://播放器刷新按钮
                handleRetryBtn(vId);
                break;
//            case R.id.video_av_switch_img://播放器切换按钮
//                handleAvSwitchBtn();
//                break;
            case R.id.bottom_dlna_iv://竖屏DLNA按钮
                handleBottomDLNABtn();
                break;
            case R.id.bottom_comment_input://评论按钮
                handleCommentBtn();
                if (!activityVideoPlayer.canCLickBottomBtn) {
                    break;
                }
                if (!ClickUtils.isFastDoubleClick()) {
                    //activityVideoPlayer.showEditCommentWindow("");
                }
                break;
//            case R.id.bottom_share_iv:// 竖屏分享按钮
            case R.id.bottom_share_rl:// 竖屏分享按钮
                handleBottomShareBtn();
                break;

            case R.id.video_land_left_clock: // 锁屏
                //activityVideoPlayer.clockScreen();
                break;

            case R.id.media_controller_audio:
                handleAvSwitchBtn();
                break;
            case R.id.tv_getdetail: // 广告了解详情
                //IntentUtils.startADActivity(activityVideoPlayer, null, activityVideoPlayer.mAdClickUrl, activityVideoPlayer.mAdClickUrl, null, "", "", null);
                //ADRecord.addAdClick(activityVideoPlayer.mAdId, ADRecord.AdRecordModel.ADTYPE_VIDEO);
                break;

            case R.id.video_mobile_continue: // 运营商网继续播放
                //activityVideoPlayer.continueMobilePlay();
                break;
        }
    }

    private void handleCommentBtn() {
        if (!activityVideoPlayer.canCLickBottomBtn) {
            return;
        }
        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
        // activityVideoPlayer.showEditCommentWindow("");
    }

    private void handleBack() {
        if (activityVideoPlayer.isLandScape()) {
            //全屏返回键
            handleTitleBack();
        } else {
            //竖屏返回键
            handleBottomBack();
        }
    }

    public void handleToDownloadBtnCallBack() {
        netDealManager.dealNetTypeWithSetting(new NetDealListener() {
            @Override
            public void onDealByState(int state) {
                handleToDownloadBtn(state);
            }
        });
    }

    public void handleHorizontalDownloadBtnCallBack() {
        netDealManager.dealNetTypeWithSetting(new NetDealListener() {
            @Override
            public void onDealByState(int state) {
                if (state == NO_ACTION) {
                    return;
                }
                //activityVideoPlayer.mCurrentDownloadState = state;
                handleHorizontalDownloadBtn(state);
            }
        });
    }

    private void handleAvSwitchBtn() {
        if (!NetUtils.isNetAvailable(activityVideoPlayer) /**&& !activityVideoPlayer.isOffLine()**/) {
            //ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            if (activityVideoPlayer.mVideoView != null) {
                activityVideoPlayer.mVideoView.stopPlayback();
            }
            activityVideoPlayer.updateErrorPauseLayer(true);
            return;
        }
    }

    private void handleHorizontalDownloadBtn(int state) {
        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
//                CustomerStatistics.OnclickBtn(StatisticsConstants.BtnName.B_DOWNLOAD, "", getPageName());
//        if (!activityVideoPlayer.isOffLine()) {
//
//            if (CacheManager.isInCache(activityVideoPlayer, activityVideoPlayer.getCurrProgram().getGuid())) {
//                Toast.makeText(activityVideoPlayer, R.string.already_to_download, Toast.LENGTH_SHORT).show();
//                return;
//            }
//            CacheManager.addDownload(activityVideoPlayer, activityVideoPlayer.getCurrProgram(), state, activityVideoPlayer.currentDownStream, IfengType.TYPE_VIDEO, activityVideoPlayer.getCacheFolderModel());
//        }
    }

    private void handleBottomShareBtn() {
        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            return;
        }
        if (!activityVideoPlayer.canCLickBottomBtn) {
            return;
        }
        //activityVideoPlayer.oneShare();
    }

    private void handleHorizontalShareBtn(View v) {
        activityVideoPlayer.showLandRight(ActivityVideoPlayer.RIGHT_LAYER_SHARE);
//        activityVideoPlayer.oneShareHorizontal(v);
//        activityVideoPlayer.pauseAdPopWindow.dismissAd();
    }

    private void handleBottomDLNABtn() {
        // activityVideoPlayer.showDLNADialog(activityVideoPlayer.getCurrProgram());
    }

    private void handleRetryBtn(int vId) {

        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            //ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
//        if (activityVideoPlayer.getCurrProgram() == null) {
//            activityVideoPlayer.refreshFragmentData();
//            return;
//        }
        if (vId == R.id.video_error_retry_img) {
//            activityVideoPlayer.currentPlayStream--;
//            if (activityVideoPlayer.currentPlayStream == ActivityVideoPlayer.INVALID_URL) {
//                activityVideoPlayer.currentPlayStream = MediaConstants.STREAM_HIGH;
//            }
        }
        activityVideoPlayer.getVideoOrAudioPosition();
        //activityVideoPlayer.refreshVideoPlay();
    }

    private void handleBtnVolume() {

    }

    private void handleADBtnVolume() {
//        if (activityVideoPlayer.hasCP()) {
//            activityVideoPlayer.adSilentBtnClick();
//        }
    }

    private void handleSubscribeBtn() {
        //ColumnUtils.subColumnOrder(activityVideoPlayer, new SubColumnDAO(activityVideoPlayer).querySubInfo(activityVideoPlayer.columnName));
        // activityVideoPlayer.updateSurbseView();

    }

    private void handleBottomBack() {
//        if (!activityVideoPlayer.hasCP()) {
//            activityVideoPlayer.mVideoView.stopPlayback();
        //activityVideoPlayer.insertWatched();
        //}
        activityVideoPlayer.finish();
    }

    private void handleDownloadFragment() {
        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            return;
        }
//                CustomerStatistics.OnclickBtn(StatisticsConstants.BtnName.B_DOWNLOAD, "", getPageName());
        //activityVideoPlayer.updateDownLoadFm(true);
    }

    private void handleDownloadLowStreamBtn() {
//        activityVideoPlayer.currentDownStream = MediaConstants.STREAM_LOW;
//        activityVideoPlayer.setFragmentSpinerBtnText(activityVideoPlayer.getResources().getString(R.string.common_video_low));
        updateStreamSeleterView();
    }

    private void handleDownloadMidStreamBtn() {
//        activityVideoPlayer.currentDownStream = MediaConstants.STREAM_MID;
//        activityVideoPlayer.setFragmentSpinerBtnText(activityVideoPlayer.getResources().getString(R.string.common_video_mid));
        updateStreamSeleterView();
    }

    private void handleDownloadHightStreamBtn() {
//        activityVideoPlayer.currentDownStream = MediaConstants.STREAM_HIGH;
//        activityVideoPlayer.setFragmentSpinerBtnText(activityVideoPlayer.getResources().getString(R.string.common_video_high));
        updateStreamSeleterView();
    }

    private void handleDownloadSupperStreamBtn() {
//        activityVideoPlayer.currentDownStream = MediaConstants.STREAM_SUPPER;
//        activityVideoPlayer.setFragmentSpinerBtnText(activityVideoPlayer.getResources().getString(R.string.common_video_supper));
        updateStreamSeleterView();
    }

    private void handleDownloadOriginalStreamBtn() {
//        activityVideoPlayer.currentDownStream = MediaConstants.STREAM_ORIGINAL;
//        activityVideoPlayer.setFragmentSpinerBtnText(activityVideoPlayer.getResources().getString(R.string.common_video_original));
        updateStreamSeleterView();
    }


    private void updateStreamSeleterView() {
        //activityVideoPlayer.updateStreamSeleterView();
    }

    private void handleToCancel() {
//        activityVideoPlayer.mPreToDownloadGuidList.clear();
//        activityVideoPlayer.mDownLoadSize = 0;
//        activityVideoPlayer.updateDownLoadFm(false);
    }

    private void handleToDownloadBtn(int state) {
        if (state == NetDealListener.NO_ACTION) {
            return;
        }
        // activityVideoPlayer.mCurrentDownloadState = state;
        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            ToastUtils.getInstance().showShortToast(R.string.common_net_useless);
            return;
        }
//        activityVideoPlayer.updateDownLoadFm(false);
//        activityVideoPlayer.startDownLoadList(state);
//        activityVideoPlayer.mDownLoadSize = activityVideoPlayer.mPreToDownloadGuidList.size();
    }

    private void handleTitleBack() {
//        if (!activityVideoPlayer.isOffLine()) {
//            activityVideoPlayer.isClocked = false;
//            activityVideoPlayer.switchOrientation();
//        } else {
//            handleBottomBack();
//        }
//                CustomerStatistics.OnclickBtn(StatisticsConstants.BtnName.B_BACK, "", getPageName());
//        activityVideoPlayer.hideController();
    }


    private void hideController() {
        if (activityVideoPlayer.mVideoController != null) {
            activityVideoPlayer.mVideoController.hide();
        }
    }

    private void showController() {
        if (activityVideoPlayer.mVideoController != null) {
            activityVideoPlayer.mVideoController.show();
        }
    }

    private void handleBottomCollect() {
        if (!NetUtils.isNetAvailable(activityVideoPlayer)) {
            return;
        }
        //activityVideoPlayer.onBottomCollectClick();
    }


    @Override
    public void shareToPlatform(Platform platform, boolean isShowEditPage) {
        showController();
        if (OneKeyShareContainer.oneKeyShare != null) {
            OneKeyShareContainer.oneKeyShare.shareToPlatform(platform, isShowEditPage);
            if (!isShowEditPage && !(platform.getName()).equals("QQ")) {
                OneKeyShareContainer.oneKeyShare = null;
            }
        }
    }

}
