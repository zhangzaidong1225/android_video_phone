package com.ifeng.newvideo.videoplayer.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 监测网络状况BroadcastReceiver
 * 当注册的activity跟网络所有变化都会调用（包括连接，断开连接）
 * @author liux
 *
 */
public class ConnectivityReceiver extends BroadcastReceiver {

	private final ConnectivityChangeListener connectivityChangeListener;

	public ConnectivityReceiver(
			ConnectivityChangeListener connectivityChangeListener) {
		this.connectivityChangeListener = connectivityChangeListener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		connectivityChangeListener.onConnectivityChange(action);
	}
	
	/**
	 * 当网络变化是做的事情
	 * @author liux
	 *
	 */
	public interface ConnectivityChangeListener {
		void onConnectivityChange(String action);
	}

}
