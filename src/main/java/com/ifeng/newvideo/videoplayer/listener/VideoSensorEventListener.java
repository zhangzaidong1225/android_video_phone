package com.ifeng.newvideo.videoplayer.listener;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import com.ifeng.newvideo.videoplayer.activity.BaseVideoPlayerActivity;
import com.ifeng.video.core.utils.GravityUtils;
import com.ifeng.video.core.utils.NetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VideoPlayer的重力感应器
 * Created by yuelong on 2014/9/29.
 */
public final class VideoSensorEventListener implements SensorEventListener {

    private static final Logger logger = LoggerFactory.getLogger(VideoSensorEventListener.class);

    public VideoSensorEventListener(BaseVideoPlayerActivity activity) {
        this.activity = activity;
    }

    private final BaseVideoPlayerActivity activity;

    private float x;
    private float y;
    private float z;

    @SuppressLint("InlinedApi")
    /**
     * 这个是当重力发生改变的时候的回调，通过一些状态的判断进行相应的操作
     */
    public void onSensorChanged(SensorEvent event) {
        //activity.isClocked &&
        if (activity.isLandScape()) {
            return;
        }
        if (activity.mVideoView == null) {
            return;
        }
        boolean isNotPlaying = !activity.mVideoView.isInPlaybackState();
        if (isNotPlaying && !activity.mobileNetShowing && NetUtils.isNetAvailable(activity)) {
            return;
        }
        if (!GravityUtils.isSystemGravityOpened(activity) ) {
            if (!activity.isLandScape()) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            return;
        }

        x = event.values[0];
        y = event.values[1];
        z = event.values[2];

        //接收到重力变为竖着的时候做的一些操作
        if (Math.abs(x) <= 3.0f && y >= 7.0f && Math.abs(z) <= 5) {

//            if (!activity.isClickToLandScape) {
                if (!activity.isLandScape()) {// 当前是竖屏了不用再发了
                    return;
                }
//                if (!activity.isSensorToPortrait) {
                if (android.os.Build.VERSION.SDK_INT >= 9) {
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
//                    activity.isSensorToPortrait = true;
//                }
            //}
        }
        //接收到重力变为横着的时候做的一些操作
        if (Math.abs(x) >= 7.0f && Math.abs(y) <= 4f && Math.abs(z) <= 6.0) {

//            activity.isClickToLandScape = false;
            if (activity.isLandScape()) {// 当前是横屏了不用再发了
                return;
            }

            if (android.os.Build.VERSION.SDK_INT >= 9) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }

        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

}
