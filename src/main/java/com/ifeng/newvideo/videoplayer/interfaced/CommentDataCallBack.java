package com.ifeng.newvideo.videoplayer.interfaced;

import com.ifeng.newvideo.videoplayer.fragment.CommentFragment;
import com.ifeng.video.dao.db.model.PlayerInfoModel;

/**
 * Created by jieyz on 2015/12/14.
 */
public interface CommentDataCallBack {
      /**
       * 获取播放的实体对像
       * @return
       */
      public PlayerInfoModel callGetCurrentProgram();

      /**
       * 展示评论编辑页面
       * @param comments
       */
      public void callShowEditCommentWindow(String comments);

      /**
       * 获取评论Fragment对像
       * @return
       */
      public CommentFragment callGetCommentFragment();

      /**
       * 评论编辑窗口显示时调用
       */
      public void callEditCommentWindowShow();

      /**
       * 评论编辑窗口隐藏时调用
       */
      public void callEditCommentWindowHide();
}
