package com.ifeng.newvideo.videoplayer.interfaced;

import android.view.View;
import android.view.ViewGroup;

/**
 * 导航条tab适配接口
 */
public interface NavigatorTabsAdapter {
    /**
     * make sure return object is View type to avoid nonresponse click event
     *
     * @param position
     * @return
     */
    View getView(int position);

    /**
     * @param mContainer
     */
    void changeViewState(int position, ViewGroup mContainer);
}
