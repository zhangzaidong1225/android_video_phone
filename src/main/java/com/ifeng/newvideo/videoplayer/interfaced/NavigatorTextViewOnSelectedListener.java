package com.ifeng.newvideo.videoplayer.interfaced;


/**
 * NavigaotrTextView是否选中接口
 */
public interface NavigatorTextViewOnSelectedListener {

    void onSelected(boolean selected);
}
