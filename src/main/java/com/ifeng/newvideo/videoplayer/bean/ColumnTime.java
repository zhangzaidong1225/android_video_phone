package com.ifeng.newvideo.videoplayer.bean;


import java.util.List;

public class ColumnTime {

    public List<Timer> timeList;

    public List<Timer> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<Timer> timeList) {
        this.timeList = timeList;
    }

    @Override
    public String toString() {
        return "ColumnTime{" +
                "timeList=" + timeList +
                '}';
    }

    public static class Timer {
        private String year;
        private String month;

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getYear() {

            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        @Override
        public String toString() {
            return "ColumnTime{" +
                    "year='" + year + '\'' +
                    ", month='" + month + '\'' +
                    '}';
        }
    }
}
