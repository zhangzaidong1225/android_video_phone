package com.ifeng.newvideo.videoplayer.bean;


public class BigAdItem {

    private String adTitle;
    private String adStopText;
    private String adStopUrl;
    private String adClickUrl;
    private String adStopImageUrl;

    public BigAdItem(String adTitle, String adStopText, String adStopUrl, String adClickUrl, String adStopImageUrl) {
        this.adTitle = adTitle;
        this.adStopText = adStopText;
        this.adStopUrl = adStopUrl;
        this.adClickUrl = adClickUrl;
        this.adStopImageUrl = adStopImageUrl;
    }


    public String getAdStopImageUrl() {
        return adStopImageUrl;
    }

    public void setAdStopImageUrl(String adStopImageUrl) {
        this.adStopImageUrl = adStopImageUrl;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getAdStopText() {
        return adStopText;
    }

    public void setAdStopText(String adStopText) {
        this.adStopText = adStopText;
    }

    public String getAdStopUrl() {
        return adStopUrl;
    }

    public void setAdStopUrl(String adStopUrl) {
        this.adStopUrl = adStopUrl;
    }

    public String getAdClickUrl() {
        return adClickUrl;
    }

    public void setAdClickUrl(String adClickUrl) {
        this.adClickUrl = adClickUrl;
    }

    @Override
    public String toString() {
        return "BigAdItem{" +
                "adTitle='" + adTitle + '\'' +
                ", adStopText='" + adStopText + '\'' +
                ", adStopUrl='" + adStopUrl + '\'' +
                ", adClickUrl='" + adClickUrl + '\'' +
                ", adStopImageUrl='" + adStopImageUrl + '\'' +
                '}';
    }
}
