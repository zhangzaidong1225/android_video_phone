package com.ifeng.newvideo.videoplayer.bean;


public class YoukuVideoItem {

    private String APPscheme;
    private String title;
    private String image;
    private String duration;
    private String yvId;



    public String getAPPscheme() {
        return APPscheme;
    }

    public void setAPPscheme(String APPscheme) {
        this.APPscheme = APPscheme;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getYvId() {
        return yvId;
    }

    public void setYvId(String yvId) {
        this.yvId = yvId;
    }

    @Override
    public String toString() {
        return "YoukuVideoItem{" +
                "APPscheme='" + APPscheme + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", duration='" + duration + '\'' +
                ", yvId='" + yvId + '\'' +
                '}';
    }
}
