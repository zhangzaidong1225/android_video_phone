package com.ifeng.newvideo.videoplayer.bean;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVoteSurveySetting {

    private String surid;
    private String period;
    private String expinfo;
    private String weight;
    private String tags;
    private String titlelink;
    private String iswarn;
    private String ishidden;
    private String limitstatus;
    private String limittime;
    private String limitnum;
    private String ticknum;
    private String userstatus;
    private String userinfo;
    private String isshow;
    private String iscomment;
    private String finishinfo;
    private String isjum;
    private String jumplink;
    private String h5;
    private String liststyle;

    public String getSurid() {
        return surid;
    }

    public void setSurid(String surid) {
        this.surid = surid;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getExpinfo() {
        return expinfo;
    }

    public void setExpinfo(String expinfo) {
        this.expinfo = expinfo;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTitlelink() {
        return titlelink;
    }

    public void setTitlelink(String titlelink) {
        this.titlelink = titlelink;
    }

    public String getIswarn() {
        return iswarn;
    }

    public void setIswarn(String iswarn) {
        this.iswarn = iswarn;
    }

    public String getIshidden() {
        return ishidden;
    }

    public void setIshidden(String ishidden) {
        this.ishidden = ishidden;
    }

    public String getLimitstatus() {
        return limitstatus;
    }

    public void setLimitstatus(String limitstatus) {
        this.limitstatus = limitstatus;
    }

    public String getLimittime() {
        return limittime;
    }

    public void setLimittime(String limittime) {
        this.limittime = limittime;
    }

    public String getLimitnum() {
        return limitnum;
    }

    public void setLimitnum(String limitnum) {
        this.limitnum = limitnum;
    }

    public String getTicknum() {
        return ticknum;
    }

    public void setTicknum(String ticknum) {
        this.ticknum = ticknum;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    public String getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(String userinfo) {
        this.userinfo = userinfo;
    }

    public String getIsshow() {
        return isshow;
    }

    public void setIsshow(String isshow) {
        this.isshow = isshow;
    }

    public String getIscomment() {
        return iscomment;
    }

    public void setIscomment(String iscomment) {
        this.iscomment = iscomment;
    }

    public String getFinishinfo() {
        return finishinfo;
    }

    public void setFinishinfo(String finishinfo) {
        this.finishinfo = finishinfo;
    }

    public String getIsjum() {
        return isjum;
    }

    public void setIsjum(String isjum) {
        this.isjum = isjum;
    }

    public String getJumplink() {
        return jumplink;
    }

    public void setJumplink(String jumplink) {
        this.jumplink = jumplink;
    }

    public String getH5() {
        return h5;
    }

    public void setH5(String h5) {
        this.h5 = h5;
    }

    public String getListstyle() {
        return liststyle;
    }

    public void setListstyle(String liststyle) {
        this.liststyle = liststyle;
    }
}
