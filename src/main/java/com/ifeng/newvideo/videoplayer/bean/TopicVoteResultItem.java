package com.ifeng.newvideo.videoplayer.bean;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVoteResultItem {

    private TopicVoteResultArray resultArray;

    public TopicVoteResultArray getResultArray() {
        return resultArray;
    }

    public void setResultArray(TopicVoteResultArray resultArray) {
        this.resultArray = resultArray;
    }
}
