package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by fanshell on 2016/7/31.
 */
public class RelativeVideoInformation {

    public List<RelativeVideoInfoItem> guidRelativeVideoInfo;

    @Override
    public String toString() {
        return "RelativeVideoInformation{" +
                "guidRelativeVideoInfo=" + guidRelativeVideoInfo +
                '}';
    }
}
