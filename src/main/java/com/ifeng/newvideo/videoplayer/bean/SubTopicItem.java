package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by fanshell on 2016/8/3.
 */
public class SubTopicItem {
    public String subId;
    public String subTitle;
    public String orderCount;
    public List<TopicItem> detailList;


}
