package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by fanshell on 2016/7/31.
 */
public class VideoItem {
    public String itemId;
    public String searchPath;
    public String abstractDesc;
    public String guid;
    public String title;
    public String name;
    public String playTime;
    public String dislikeNo;
    public String isColumn; // 是否为剧集  0 非剧集   1 剧集
    public String columnYear;
    public String columnMonth;
    public String programNo;
    public String simId = ""; // 推荐属性
    public String image;
    public String cpName;
    public String createDate;
    public String updateDate;
    public String shareTimes;
    public String commentNo;
    public int duration;
    public String memberType;

    public WeMedia weMedia = new WeMedia();
    public List<FileType> videoFiles;
    public String mUrl;//视频新版分享地址
    public String pcUrl;//评论url

    public String topicId;
    public String topicType;
    public boolean isConvertFromFM; // 非接口字段，标记是否从FM数据转换来的

    public String yvId;

    public boolean isColumn() {
        return "1".equals(isColumn);
    }

    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;

        if (object != null && object instanceof VideoItem) {
            sameSame = this.guid.equals(((VideoItem) object).guid);
        }

        return sameSame;
    }

    @Override
    public String toString() {
        return "VideoItem{" +
                "itemId='" + itemId + '\'' +
                ", searchPath='" + searchPath + '\'' +
                ", abstractDesc='" + abstractDesc + '\'' +
                ", guid='" + guid + '\'' +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", playTime='" + playTime + '\'' +
                ", dislikeNo='" + dislikeNo + '\'' +
                ", isColumn='" + isColumn + '\'' +
                ", columnYear='" + columnYear + '\'' +
                ", columnMonth='" + columnMonth + '\'' +
                ", programNo='" + programNo + '\'' +
                ", simId='" + simId + '\'' +
                ", image='" + image + '\'' +
                ", cpName='" + cpName + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                ", shareTimes='" + shareTimes + '\'' +
                ", commentNo='" + commentNo + '\'' +
                ", duration=" + duration +
                ", weMedia=" + weMedia +
                ", videoFiles=" + videoFiles +
                ", mUrl='" + mUrl + '\'' +
                ", pcUrl='" + pcUrl + '\'' +
                ", topicId='" + topicId + '\'' +
                ", topicType='" + topicType + '\'' +
                ", isConvertFromFM=" + isConvertFromFM +
                ", yvId='" + yvId + '\'' +
                '}';
    }
}
