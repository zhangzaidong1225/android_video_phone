package com.ifeng.newvideo.videoplayer.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Pengcheng on 2016/8/10 010.
 */
public class TopicExtendData {

    private static final Logger logger = LoggerFactory.getLogger(TopicExtendData.class);
    public String title;
    public String linkType;
    public String url;
    public String image;

}
