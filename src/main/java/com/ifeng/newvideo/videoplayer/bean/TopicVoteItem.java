package com.ifeng.newvideo.videoplayer.bean;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVoteItem {

    private String ifsuccess;
    private String msg;
    private TopicVoteDataItem data;

    public String getIfsuccess() {
        return ifsuccess;
    }

    public void setIfsuccess(String ifsuccess) {
        this.ifsuccess = ifsuccess;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public TopicVoteDataItem getData() {
        return data;
    }

    public void setData(TopicVoteDataItem data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "TopicVoteItem{" +
                "ifsuccess='" + ifsuccess + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
