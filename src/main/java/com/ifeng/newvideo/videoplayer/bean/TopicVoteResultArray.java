package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVoteResultArray {

    private String questionid;
    private String votecount;
    private String question;
    private String choosetype;
    private String ismust;
    private List<TopicVotePoint> option;

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public String getVotecount() {
        return votecount;
    }

    public void setVotecount(String votecount) {
        this.votecount = votecount;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getChoosetype() {
        return choosetype;
    }

    public void setChoosetype(String choosetype) {
        this.choosetype = choosetype;
    }

    public String getIsmust() {
        return ismust;
    }

    public void setIsmust(String ismust) {
        this.ismust = ismust;
    }

    public List<TopicVotePoint> getOption() {
        return option;
    }

    public void setOption(List<TopicVotePoint> option) {
        this.option = option;
    }
}
