package com.ifeng.newvideo.videoplayer.bean;

import com.ifeng.video.dao.db.model.MainAdInfoModel;

import java.util.List;

/**
 * Created by ll on 2017/4/24.
 */
public class TopicPlayerItem extends VideoItem {
    private boolean have_content=false;
    private String adId;
    private String adPositionId;
    private String adStartTime;
    private String adEndTime;
    private String imageURL;
    private String text;
    private String kkrand; // KK直播专用随机人数，其他接口请忽略此字段
    private String vdescription;
    private List<String> photos; // 三联图信息流图片地址

    public boolean isHave_content() {
        return have_content;
    }

    public void setHave_content(boolean have_content) {
        this.have_content = have_content;
    }

    public String getKkrand() {
        return kkrand;
    }

    public void setKkrand(String kkrand) {
        this.kkrand = kkrand;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public MainAdInfoModel.AdMaterial.Icon getIcon() {
        return icon;
    }

    public void setIcon(MainAdInfoModel.AdMaterial.Icon icon) {
        this.icon = icon;
    }

    private MainAdInfoModel.AdMaterial.Icon icon;
    private MainAdInfoModel.AdMaterial.AdAction adAction;
    private MainAdInfoModel.AdMaterial.AdConditions adConditions;
    private int NeedMoreAd = -100; // 1.支持余量；0.关闭余量；

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getAdStartTime() {
        return adStartTime;
    }

    public void setAdStartTime(String adStartTime) {
        this.adStartTime = adStartTime;
    }

    public String getAdEndTime() {
        return adEndTime;
    }

    public void setAdEndTime(String adEndTime) {
        this.adEndTime = adEndTime;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVdescription() {
        return vdescription;
    }

    public void setVdescription(String vdescription) {
        this.vdescription = vdescription;
    }

    public MainAdInfoModel.AdMaterial.AdAction getAdAction() {
        return adAction;
    }

    public void setAdAction(MainAdInfoModel.AdMaterial.AdAction adAction) {
        this.adAction = adAction;
    }

    public MainAdInfoModel.AdMaterial.AdConditions getAdConditions() {
        return adConditions;
    }

    public void setAdConditions(MainAdInfoModel.AdMaterial.AdConditions adConditions) {
        this.adConditions = adConditions;
    }

    public int getNeedMoreAd() {
        return NeedMoreAd;
    }

    public void setNeedMoreAd(int needMoreAd) {
        NeedMoreAd = needMoreAd;
    }

    public String getAdPositionId() {
        return adPositionId;
    }

    public void setAdPositionId(String adPositionId) {
        this.adPositionId = adPositionId;
    }

    @Override
    public String toString() {
        return "\n\nAdMaterial{" +
                "adId='" + adId + '\'' +
                ", adStartTime='" + adStartTime + '\'' +
                ", adEndTime='" + adEndTime + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", text='" + text + '\'' +
                ", kkrand='" + kkrand + '\'' +
                ", vdescription='" + vdescription + '\'' +
                ", photos=" + photos +
                ", icon=" + icon +
                ", adAction=" + adAction +
                ", adConditions=" + adConditions +
                ", NeedMoreAd=" + NeedMoreAd +
                '}';

    }
}
