package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVoteSurveryInfo {

    private String id;
    private String leadtxt;
    private String starttime;
    private String createby;
    private String email;
    private String user;
    private String pnum;
    private String votecount;
    private String is_vote;
    private String channelid;
    private String channelename;
    private String channelurl;
    private String channellogourl;
    private String url;
    private String endtime;
    private String expire;
    private String createtime;
    private String currenttime;
    private String title;
    private String isactive;
    private String skey;
    private List<String> questionids;
    private String is_voted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeadtxt() {
        return leadtxt;
    }

    public void setLeadtxt(String leadtxt) {
        this.leadtxt = leadtxt;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getCreateby() {
        return createby;
    }

    public void setCreateby(String createby) {
        this.createby = createby;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPnum() {
        return pnum;
    }

    public void setPnum(String pnum) {
        this.pnum = pnum;
    }

    public String getVotecount() {
        return votecount;
    }

    public void setVotecount(String votecount) {
        this.votecount = votecount;
    }

    public String getIs_vote() {
        return is_vote;
    }

    public void setIs_vote(String is_vote) {
        this.is_vote = is_vote;
    }

    public String getChannelid() {
        return channelid;
    }

    public void setChannelid(String channelid) {
        this.channelid = channelid;
    }

    public String getChannelename() {
        return channelename;
    }

    public void setChannelename(String channelename) {
        this.channelename = channelename;
    }

    public String getChannelurl() {
        return channelurl;
    }

    public void setChannelurl(String channelurl) {
        this.channelurl = channelurl;
    }

    public String getChannellogourl() {
        return channellogourl;
    }

    public void setChannellogourl(String channellogourl) {
        this.channellogourl = channellogourl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getCurrenttime() {
        return currenttime;
    }

    public void setCurrenttime(String currenttime) {
        this.currenttime = currenttime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public List<String> getQuestionids() {
        return questionids;
    }

    public void setQuestionids(List<String> questionids) {
        this.questionids = questionids;
    }

    public String getIs_voted() {
        return is_voted;
    }

    public void setIs_voted(String is_voted) {
        this.is_voted = is_voted;
    }
}
