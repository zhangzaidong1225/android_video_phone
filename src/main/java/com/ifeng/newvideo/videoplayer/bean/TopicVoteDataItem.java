package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVoteDataItem {

    private TopicVoteSurveryInfo surveyinfo;
    private TopicVoteSurveySetting surverysetting;
    private String count;
    private List<TopicVoteResultItem> result;

    public TopicVoteSurveryInfo getSurveyinfo() {
        return surveyinfo;
    }

    public void setSurveyinfo(TopicVoteSurveryInfo surveyinfo) {
        this.surveyinfo = surveyinfo;
    }

    public TopicVoteSurveySetting getSurverysetting() {
        return surverysetting;
    }

    public void setSurverysetting(TopicVoteSurveySetting surverysetting) {
        this.surverysetting = surverysetting;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<TopicVoteResultItem> getResult() {
        return result;
    }

    public void setResult(List<TopicVoteResultItem> result) {
        this.result = result;
    }
}
