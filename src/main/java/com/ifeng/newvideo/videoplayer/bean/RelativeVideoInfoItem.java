package com.ifeng.newvideo.videoplayer.bean;

import com.ifeng.video.dao.db.model.MainAdInfoModel;

/**
 * Created by android-dev on 2017/4/17 0017.
 */
public class RelativeVideoInfoItem {

    public VideoItem videoInfo;
    public MainAdInfoModel.AdMaterial adbackend;
    public YoukuVideoItem youkuInfo;
    public ADvertItem advert;

    @Override
    public String toString() {
        return "RelativeVideoInfoItem{" +
                "videoInfo=" + videoInfo +
                ", adbackend=" + adbackend +
                ", youkuInfo=" + youkuInfo +
                ", advert=" + advert +
                '}';
    }
}
