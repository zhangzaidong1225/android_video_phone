package com.ifeng.newvideo.videoplayer.bean;

/**
 * 视频源相关信息
 * Created by fanshell on 2016/7/31.
 */
public class FileType {

    public String useType = "";
    public String mediaUrl = "";
    public int filesize;
    public int isSplite;
    public int spliteNo;
    public String spliteTime = "";
    public String fileId = "";

    @Override
    public String toString() {
        return "FileType{" +
                "useType=" + useType +
                ", mediaUrl='" + mediaUrl + '\'' +
                ", filesize=" + filesize +
                ", isSplite=" + isSplite +
                ", spliteNo=" + spliteNo +
                ", spliteTime='" + spliteTime + '\'' +
                ", fileId='" + fileId + '\'' +
                '}';
    }
}
