package com.ifeng.newvideo.videoplayer.bean;


public class DanmuItem {

    private String code;
    private Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "DanmuItem{" +
                "code='" + code + '\'' +
                ", data=" + data +
                '}';
    }

    public static class Data {
        private String can_post;

        public String getCan_post() {
            return can_post;
        }

        public void setCan_post(String can_post) {
            this.can_post = can_post;
        }
    }
}
