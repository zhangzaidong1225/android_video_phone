package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;


public class RelativeDramaVideoInformation {

    public List<RelativeVideoInfoItem> columnVideoInfo;

    @Override
    public String toString() {
        return "RelativeDramaVideoInformation{" +
                "columnVideoInfo=" + columnVideoInfo +
                '}';
    }
}
