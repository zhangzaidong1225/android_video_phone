package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;

/**
 * Created by fanshell on 2016/8/3.
 */
public class TopicDetailList {
    public String topicShareURL;
    public String title;
    public String smallPoster;
    public String largePoster;
    public String desc;
    public List<TopicExtendData> extendData;

    public List<SubTopicItem> subTopicList;


}
