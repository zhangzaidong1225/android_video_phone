package com.ifeng.newvideo.videoplayer.bean;

/**
 * Created by zhangzd on 2017/10/23.
 */
public class TopicVotePoint {

    private String title;
    private String itemid;
    private String num;
    private String pic_url;
    private String url;
    private String is_both;
    private String nump;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIs_both() {
        return is_both;
    }

    public void setIs_both(String is_both) {
        this.is_both = is_both;
    }

    public String getNump() {
        return nump;
    }

    public void setNump(String nump) {
        this.nump = nump;
    }
}
