package com.ifeng.newvideo.videoplayer.bean;

/**
 * Created by fanshell on 2016/8/3.
 */
public class UITopicData {
    public String topicName;
    public TopicPlayerItem item;

    // 非接口数据，为了便于理解业务而加的变量
    public boolean isTitle;
    public boolean isAd;
    public boolean isVideo;

    public boolean isHighlight; // item高亮


}
