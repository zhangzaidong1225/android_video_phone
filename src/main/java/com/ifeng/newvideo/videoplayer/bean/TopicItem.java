package com.ifeng.newvideo.videoplayer.bean;

/**
 * Created by fanshell on 2016/8/3.
 */
public class TopicItem {
    public String thumbImageUrl;
    public String abstractDesc;
    public String itemId;
    public String memberType;
    public TopicPlayerItem memberItem;

}
