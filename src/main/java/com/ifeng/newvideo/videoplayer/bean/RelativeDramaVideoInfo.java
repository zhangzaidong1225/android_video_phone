package com.ifeng.newvideo.videoplayer.bean;

import java.util.List;


public class RelativeDramaVideoInfo {

    public List<RelativeVideoInfoItem> columnVideoInfo;

    @Override
    public String toString() {
        return "RelativeDramaVideoInfo{" +
                "columnVideoInfo=" + columnVideoInfo +
                '}';
    }
}
