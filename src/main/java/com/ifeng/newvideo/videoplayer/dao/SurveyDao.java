package com.ifeng.newvideo.videoplayer.dao;

import android.text.TextUtils;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.dao.db.dao.CommonDao;

/**
 * 调查系统
 * http://wiki.staff.ifeng.com/wiki/index.php?title=%E8%B0%83%E6%9F%A5%E7%B3%BB%E7%BB%9F&variant=zh-cn
 */
public class SurveyDao {
    public static final String TAG = SurveyDao.class.getName();
    private static final String URL_ACCUMULATOR = "http://survey.news.ifeng.com/accumulator_ext.php?key=%s&format=json";

    /**
     * 累加数据
     * 直播类都不报
     * 视频类报
     * 音频有guid的才报
     */
    public static void accumulatorPlayCount(String guid) {
        if (TextUtils.isEmpty(guid)) {
            return;
        }
        CommonDao.sendRequest(String.format(URL_ACCUMULATOR, guid), null, null, null,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }

    public static void cancelAll() {
        VolleyHelper.getRequestQueue().cancelAll(TAG);
    }

}
