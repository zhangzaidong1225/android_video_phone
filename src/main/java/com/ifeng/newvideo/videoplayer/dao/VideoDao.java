package com.ifeng.newvideo.videoplayer.dao;

import android.util.Log;
import com.android.volley.Response;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.bean.ColumnTime;
import com.ifeng.newvideo.videoplayer.bean.RelativeDramaVideoInfo;
import com.ifeng.newvideo.videoplayer.bean.RelativeVideoInformation;
import com.ifeng.newvideo.videoplayer.bean.VideoItem;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;

import java.util.Calendar;

/**
 * Created by fanshell on 2016/7/31.
 */
public class VideoDao {

    public static final String TAG = VideoDao.class.getName();

    /**
     * 通过guid获得单条视频详细信息
     * http://vcsp.ifeng.com/vcsp/appData/videoGuid.do?guid=0114431f-d214-4695-9b87-66af9b39c19b
     */

    public static void getVideoInformationById(String guid, String itemId, Response.Listener<VideoItem> sucListener,
                                               Response.ErrorListener errorListener) {
        String url = DataInterface.VIDEO_GUID;
        url = String.format(url, guid, "");
        Log.d("apiInterface", url);
        CommonDao.sendRequest(url,
                VideoItem.class,
                sucListener,
                errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }


    /**
     * 根据guid得到相关视频信息
     * http://vcsp.ifeng.com/vcsp/appData/getGuidRelativeVideoList.do
     *
     * @param guid
     */
    public static void getRelativeVideoById(String guid,
                                            Response.Listener<RelativeVideoInformation> sucListener,
                                            Response.ErrorListener errorListener) {
        String url = DataInterface.GET_GUID_RELATIVE_VIDEO_LIST;
        url = String.format(url, guid, "", DataInterface.PAGESIZE_20, new User(IfengApplication.getAppContext()).getUid(),
                PhoneConfig.userKey, SharePreUtils.getInstance().getInreview());
        Log.d("apiInterface", url);
        CommonDao.sendRequest(url,
                RelativeVideoInformation.class,
                sucListener, errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }

    public static void getRelativeVideoByIdForLianbo(String guid,
                                            Response.Listener<RelativeVideoInformation> sucListener,
                                            Response.ErrorListener errorListener) {
        String url = DataInterface.GET_GUID_RELATIVE_VIDEO_LIST;
        url = String.format(url, guid, "", DataInterface.PAGESIZE_6, new User(IfengApplication.getAppContext()).getUid(),
                PhoneConfig.userKey, SharePreUtils.getInstance().getInreview());
        Log.d("apiInterface", url);
        CommonDao.sendRequest(url,
                RelativeVideoInformation.class,
                sucListener, errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }

    /**
     * 根据 guid 得到剧集类相关信息
     */
    public static void getRelativeDramaVideoById(String guid, String weMediaId, String year, String month, final String count, String operation,
                                                 Response.Listener<RelativeDramaVideoInfo> sucListener,
                                                 Response.ErrorListener errorListener) {
        String url = DataInterface.GET_GUID_COLUMN_VIDEO_LIST;
        int yearCurrent;
        String monthAll;
        if (year == null || "".equals(year)) {
            Calendar now = Calendar.getInstance();
            yearCurrent = now.get(Calendar.YEAR);
        } else {
            yearCurrent = Integer.parseInt(year);
        }
        if (month == null || "".equals(month)) {
            monthAll = "all";
        } else {
            monthAll = month;
        }

        url = String.format(url, guid, "", count, yearCurrent + "",
                monthAll, weMediaId, SharePreUtils.getInstance().getInreview(), operation);
        Log.d("apiInterfaceDrama", url);
        CommonDao.sendRequest(url,
                RelativeDramaVideoInfo.class,
                sucListener,
                errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }

    /**
     * 获得剧集类时间表
     */
    public static void getColumnTimeList(String weMediaId, Response.Listener<ColumnTime> sucListener, Response.ErrorListener errorListener) {
        String url = DataInterface.GET__COLUMN_TIMER_LIST;
        url = String.format(url, weMediaId);
        Log.d("apiColumnTime", url);

        CommonDao.sendRequest(url,
                ColumnTime.class,
                sucListener,
                errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON,
                false,
                TAG);
    }


    public static void cancelAll() {
        VolleyHelper.getRequestQueue().cancelAll(TAG);
    }

}
