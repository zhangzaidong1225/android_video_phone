package com.ifeng.newvideo.videoplayer.dao;

import android.util.Log;
import com.android.volley.Response;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.bean.TopicDetailList;
import com.ifeng.newvideo.videoplayer.bean.TopicVoteItem;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;

/**
 * Created by fanshell on 2016/7/31.
 */
public class TopicVideoDao {
    public static final String TAG = TopicVideoDao.class.getName();

    /**
     * 专题详情列表接口
     */
    public static void getTopicDetailList(String itemId, String itemType,
                                          Response.Listener<TopicDetailList> sucListener,
                                          Response.ErrorListener errorListener) {
        String url = DataInterface.DETAIL_TOPIC_LIST_URL;
        url = String.format(url, itemId, itemType, new User(IfengApplication.getAppContext()).getUid(),
                PhoneConfig.userKey, SharePreUtils.getInstance().getInreview());
        Log.d("comment", "" + url);
        CommonDao.sendRequest(url,
                TopicDetailList.class,
                sucListener, errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }

    /**
     * 专题投票记录
     *
     * @param itemId topicId
     */
    public static void getTopicVoteInfo(String itemId,
                                        Response.Listener<TopicVoteItem> sucListener,
                                        Response.ErrorListener errorListener) {
        String url = DataInterface.TOPIC_CHECK_SINGLE_VOTE_RECORD;
        url = String.format(url, itemId);
        Log.d("vote", url + "");
        CommonDao.sendRequest(url,
                TopicVoteItem.class,
                sucListener,
                errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON,
                false,
                TAG);
    }

    /**
     * 投票结果
     *
     * @param surveyInfoId 调查的surveyId
     * @param questionId   问题id
     * @param itemId       答案id
     */
    public static void commitTopicVoteResult(String surveyInfoId, String questionId, String itemId, Response.Listener sucListener, Response.ErrorListener errorListener) {
        String url = DataInterface.TOPIC_VOTE_COMMIT;
        url = String.format(url, surveyInfoId, questionId, itemId);
        Log.d("vote", url + "");

        CommonDao.sendRequest(url,
                null,
                sucListener,
                errorListener,
                CommonDao.RESPONSE_TYPE_GET_JSON, false, TAG);
    }


    public static void cancelAll() {
        VolleyHelper.getRequestQueue().cancelAll(TAG);
    }

}
