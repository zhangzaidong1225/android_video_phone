package com.ifeng.newvideo.push;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RemoteViews;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.badge.BadgeUtils;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.StatisticsConstants;
import com.ifeng.newvideo.statistics.domains.OpenPushRecord;
import com.ifeng.newvideo.statistics.domains.PushAccessRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.live.LiveUtils;
import com.ifeng.newvideo.ui.live.TVLiveActivity;
import com.ifeng.newvideo.ui.live.vr.VRLiveActivity;
import com.ifeng.newvideo.ui.live.vr.VRVideoActivity;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.NewsSilenceInstallUtils;
import com.ifeng.newvideo.utils.PushUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.videoplayer.activity.ActivityAudioFMPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityCacheVideoPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.newvideo.videoplayer.widget.skin.VideoSkin;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.BuildUtils;
import com.ifeng.video.core.utils.DisplayUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ifeng.newvideo.utils.PushUtils.RESOURCE_HUAWEI;
import static com.ifeng.newvideo.utils.PushUtils.RESOURCE_IFENG;

/**
 * PushReceiver 处理推送接收及统计等
 * Created by Vincent on 2014/10/26.
 */
public class PushReceiver extends BroadcastReceiver {
    /**
     * 推送最多显示个数
     */
    private static final int NOTIFICATION_MAX_NUM = 3;

    private static final Logger logger = LoggerFactory.getLogger(PushReceiver.class);

    private static final int PUSH_NOTIFICATION_TYPE_VIDEO = 0x200;
    private static final int PUSH_NOTIFICATION_TYPE_LIVE = 0x300;
    private static final int PUSH_NOTIFICATION_TYPE_LIVEROOM = 0x600;
    private static final int PUSH_NOTIFICATION_TYPE_CMCC_LIVE = 0x102;
    private static final int PUSH_NOTIFICATION_TYPE_TOPIC = 0x100;
    private static final int PUSH_NOTIFICATION_TYPE_LIANBO = 0x400;
    private static final int PUSH_NOTIFICATION_TYPE_FOCUS = 0x500;
    private static final int PUSH_NOTIFICATION_TYPE_CMPPTOPIC = 0x700;
    private static final int PUSH_NOTIFICATION_TYPE_IFENGFOCUS = 0x800;
    private static final int PUSH_NOTIFICATION_TYPE_COLUMN = 0x900;
    private static final int PUSH_NOTIFICATION_TYPE_WEB = 0x101;
    private static final int PUSH_NOTIFICATION_TYPE_AWAKEN = 0x103;
    private static final int PUSH_NOTIFICATION_TYPE_TEXT = 0x104;
    public static final int PUSH_NOTIFICATION_TYPE_UPDATE = 0x111;
    public static final int PUSH_NOTIFICATION_TYPE_VR_LIVE = 0x113;//VR Live
    public static final int PUSH_NOTIFICATION_TYPE_INSTALL_NEWS = 0x114;//安装新闻客户端

    public static final String PUSH_TYPE_LIVE = "live";
    private static final String PUSH_TYPE_TOPIC = "topic";
    private static final String PUSH_TYPE_VIDEO = "video";
    private static final String PUSH_TYPE_COLUMN = "column";
    public static final String PUSH_TYPE_WEB = "web";
    private static final String PUSH_TYPE_CMCCLIVE = "cmcclive";
    private static final String PUSH_TYPE_AWAKEN = "awaken";
    private static final String PUSH_TYPE_TEXT = "text";
    private static final String PUSH_TYPE_INSTALL_NEWS = "installNews";
    // revision topic
    private static final String PUSH_TYPE_LIANBO = "lianbo";
    private static final String PUSH_TYPE_FOCUS = "focus";
    public static final String PUSH_TYPE_IMCPTOPIC = "topic";
    private static final String PUSH_TYPE_LIVEROOM = "liveRoom";
    private static final String PUSH_TYPE_CMPPTOPIC = "cmpptopic";
    private static final String PUSH_TYPE_iFENGFOCUS = "ifengFocus";
    public static final String PUSH_SUBCOLUMNNEW = "subColumnNew";

    public static final String PUSH_TYPE = "pushType";
    public static final String PUSH_ID = "pushId";
    public static final String PUSH_IMAGE = "image";
    public static final String PUSH_CONTID = "contid";
    public static final String PUSH_CONTENT = "content";
    public static final String PUSH_COLUMN_ID = "columnid";
    public static final String PUSH_WEB_URL = "url";
    public static final String PUSH_TITLE = "pushTitle";
    public static final String PUSH_LIVE_URL = "pushliveurl";
    public static final String PUSH_VR_LIVE = "vrlive"; ////VR Live
    public static final String PUSH_VR_SHARE_URL = "vrlive_share_url"; ////VR Live

    private static final Map<String, Integer> sPushTypeContext = new HashMap<>();

    static {
        sPushTypeContext.put(PUSH_TYPE_LIVE, PUSH_NOTIFICATION_TYPE_LIVE);
        sPushTypeContext.put(PUSH_VR_LIVE, PUSH_NOTIFICATION_TYPE_VR_LIVE);
        sPushTypeContext.put(PUSH_TYPE_VIDEO, PUSH_NOTIFICATION_TYPE_VIDEO);
        sPushTypeContext.put(PUSH_TYPE_TOPIC, PUSH_NOTIFICATION_TYPE_TOPIC);
        sPushTypeContext.put(PUSH_TYPE_LIANBO, PUSH_NOTIFICATION_TYPE_LIANBO);
        sPushTypeContext.put(PUSH_TYPE_FOCUS, PUSH_NOTIFICATION_TYPE_FOCUS);
        sPushTypeContext.put(PUSH_TYPE_CMPPTOPIC, PUSH_NOTIFICATION_TYPE_CMPPTOPIC);
        sPushTypeContext.put(PUSH_TYPE_iFENGFOCUS, PUSH_NOTIFICATION_TYPE_IFENGFOCUS);
        sPushTypeContext.put(PUSH_TYPE_WEB, PUSH_NOTIFICATION_TYPE_WEB);
        sPushTypeContext.put(PUSH_TYPE_AWAKEN, PUSH_NOTIFICATION_TYPE_AWAKEN);
        sPushTypeContext.put(PUSH_TYPE_TEXT, PUSH_NOTIFICATION_TYPE_TEXT);
        sPushTypeContext.put(PUSH_TYPE_INSTALL_NEWS, PUSH_NOTIFICATION_TYPE_INSTALL_NEWS);
        // sPushTypeContext.put(PUSH_TYPE_LIVEROOM, PUSH_NOTIFICATION_TYPE_LIVEROOM);
        // sPushTypeContext.put(PUSH_TYPE_COLUMN, PUSH_NOTIFICATION_TYPE_COLUMN);
        // sPushTypeContext.put(PUSH_TYPE_CMCCLIVE, PUSH_NOTIFICATION_TYPE_CMCC_LIVE);
    }

    private static boolean isLegalType(String pushType) {
        return sPushTypeContext.containsKey(pushType);
    }

    static int getPushNotiType(String pushType) {
        return sPushTypeContext.get(pushType);
    }

    public final static String ACTION_PUSH_TOPIC = "com.ifeng.newvideo.pushmsg.top";
    public final static String ACTION_PUSH_LIANBO = "com.ifeng.newvideo.pushmsg.lianbo";
    public final static String ACTION_PUSH_FOCUS = "com.ifeng.newvideo.pushmsg.focus";
    public final static String ACTION_PUSH_CMPPTOPIC = "com.ifeng.newvideo.pushmsg.cmpptopic";


    public final static String KEY_PUSH_DOWNLOAD = "push_download";
    private final static String KEY_PUSH_ID = "push_id";

    private Context mAppContext;


    @Override
    public void onReceive(Context context, Intent intent) {
        logger.info("PushReceiver  onReceive");

        if (intent == null) {
            logger.info("PushReceiver onReceive intent is null");
            return;
        }
        if (!hasPushOpenedInSettingActivity(context)) {
            logger.info("PushReceiver hasPushOpenedInSettingActivity is off !!!!");
            return;
        }

        mAppContext = context;
        Bundle bundle = intent.getExtras();
        String msgJsonString = bundle.getString(PushUtils.KEY_PUSH_MSG);
        PushMessage pushMsg = new PushMessage(msgJsonString);
        pushMsg.mPushMessageType = bundle.getString(PushUtils.PUSH_MESSAGE_TYPE);
        pushMsg.mPushPlatform = bundle.getInt(PushUtils.PUSH_RESOURCE, RESOURCE_IFENG);
        logger.info("PushMessage msgJsonString= {}, \n{}", msgJsonString, pushMsg.toString());
        // 推送类型不合法时发送统计
        if (!pushMsg.isValid()) {
            logger.info("PushMessage is invalid !!!!");
            pushMsg.mSingleId = pushMsg.mSingleId != null ? pushMsg.mSingleId : "";
            pushMsg.mPushType = pushMsg.mPushType != null ? pushMsg.mPushType : "";
            pushMsg.mImage = pushMsg.mSingleId;
            if (pushMsg.mPushPlatform == RESOURCE_IFENG) {
                CustomerStatistics.sendPushAccessReceive(new PushAccessRecord(pushMsg.mSingleId, pushMsg.mPushType));
            }
            return;
        }

        if (pushMsg.mPushType.equals(PUSH_TYPE_LIVE)) {
            pushMsg.mSingleId = LiveUtils.transLiveId(pushMsg.mSingleId);
            logger.info("transLiveId mSingleId = {}", pushMsg.mSingleId);
        }

        // 收到推送消息时
        if (Ipush.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            pushReceived(intent, pushMsg);
        }
        // 打开推送消息时
        else if (Ipush.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            pushOpened(pushMsg, intent);
        }
        // 接收到消息
        else if (Ipush.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            logger.info("receive a msg : " + intent.getAction());
        }
    }

    private void pushReceived(Intent intent, PushMessage pushMsg) {
        // 接收到通知
        logger.info("pushReceived action={}", intent.getAction());
        String pushType = pushMsg.mPushType;
        boolean updateConfigNoCheckFlag = intent.getBooleanExtra(IntentKey.UPDATECONFIG_NOCHECK, false);
        // 升级下载
        if (!isLegalType(pushType) && !updateConfigNoCheckFlag) {
            logger.info("--------UpdateConfigTask Start----------");
            try {
                String localVersion = PackageUtils.getAppVersion(mAppContext);
                //TODO 升级
//                new PushUpdateConfigTask(localVersion, intent, true).execute(DataInterface.UPDATE_URL, NetUtils.isMobileWap(mAppContext));
                return;
            } catch (PackageManager.NameNotFoundException e) {
                logger.error(e.toString(), e);
            }
        }
        if (PushUtils.directOpenPush(pushType, pushMsg.mPushMessageType)) {
            pushOpened(pushMsg, intent);
        } else {
            sendNotification(mAppContext, intent, pushMsg);
            sendOnPushReceivedStatistic(pushMsg);
        }
        BadgeUtils.showBadge(mAppContext, 1);
    }

    /**
     *  用户打开通知
     */
    private void pushOpened(PushMessage pushMsg, Intent intent) {
        BadgeUtils.clearBadge(mAppContext);

        try {
            logger.info("ACTION_NOTIFICATION_OPENED : " + pushMsg.toString());
            String pushType = pushMsg.mPushType;
            boolean isLegalType = isLegalType(pushType);
            logger.info("isLegalType = {}, pushType = {}", isLegalType, pushType);
            if (TextUtils.isEmpty(pushType)) {
                logger.info("The pushType of Notification is null !!!!!");
                return;
            }
            if (isLegalType) {
                boolean isDownload = intent.getBooleanExtra(KEY_PUSH_DOWNLOAD, false);
                doOpenPushPage(mAppContext, pushMsg, isDownload);
                if (isDownload) {
                    ((NotificationManager) mAppContext.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(intent.getIntExtra(KEY_PUSH_ID, 0));
                }
                CustomerStatistics.inType = StatisticsConstants.APPSTART_TYPE_FROM_PUSH;
                CustomerStatistics.sendOpenPushReceive(new OpenPushRecord(pushMsg.mSingleId, pushMsg.mPushType));
            } else {
                // 进入到客户端首页并弹出升级提示框...
                pushMsg.obj = intent.getSerializableExtra(IntentKey.UPDATECONFIG_OBJ);
                doOpenPushPage(mAppContext, pushMsg, false);
            }
        } catch (Exception e) {
            logger.error("pushOpened error ! {}", e.toString(), e);
        }
    }

    private boolean hasPushOpenedInSettingActivity(Context context) {
        return SharePreUtils.getInstance().getPushMessageState();
    }

    static class PushMessage {

        static final String sPushFeedBackKey = "feedback";
        static final String sPushStyleIdKey = "styleId";
        static final String sPushContentKey = "content";
        static final String sPushTitleKey = "title";
        static final String sPushNotifyTypeKey = "notifyType";
        static final String sPushDelayPopupKey = "delayPopup";

        static final String sPushExtraKey = "extra";
        static final String sPushSingleIdKey = "id";
        static final String sPushNotificationTypeKey = "type";
        static final String sPushColumnIdKey = "columnid";
        static final String mPushUrlKey = "url";
        static final String sPushImageKey = "image";
        static final String mPushCountIdKey = "contid";

        int mFeedback;
        int mStyleId;
        int mDelayPopup;
        int mNotifyType;

        String mContent;
        String mTitle;
        String mSingleId;
        String mColumnId;
        String mUrl;
        String mImage;
        String mContId;

        String mPushType;

        int mPushPlatform;

        String mPushMessageType;//是否为第三方，如果是第三方直接打开

        public Serializable obj;

        public PushMessage() {

        }

        public PushMessage(String msg) {
            try {
                JSONObject jsonObj = JSONObject.parseObject(msg);

                mFeedback = jsonObj.getIntValue(sPushFeedBackKey);
                mStyleId = jsonObj.getIntValue(sPushStyleIdKey);
                mDelayPopup = jsonObj.getIntValue(sPushDelayPopupKey);
                mNotifyType = jsonObj.getIntValue(sPushNotifyTypeKey);

                mContent = jsonObj.getString(sPushContentKey);
                mTitle = jsonObj.getString(sPushTitleKey);

                String extra = jsonObj.getString(sPushExtraKey);
                jsonObj = JSON.parseObject(extra);

                mImage = jsonObj.getString(sPushImageKey);
                mColumnId = jsonObj.getString(sPushColumnIdKey);
                mUrl = jsonObj.getString(mPushUrlKey);
                mSingleId = jsonObj.getString(sPushSingleIdKey);
                mPushType = jsonObj.getString(sPushNotificationTypeKey);
                mContId = jsonObj.getString(mPushCountIdKey);

            } catch (Exception e) {
                logger.error("PushMessage init error!");
            }
        }

        /**
         * 数据有效性校验
         */
        public boolean isValid() {
            if (RESOURCE_HUAWEI == mPushPlatform) {
                return !TextUtils.isEmpty(mSingleId) && !TextUtils.isEmpty(mPushType);
            }
            return !TextUtils.isEmpty(mSingleId) && !TextUtils.isEmpty(mPushType) && !TextUtils.isEmpty(mContent);
        }

        @Override
        public String toString() {
            return "PushMessage{" +
                    "mFeedback=" + mFeedback +
                    ", mStyleId=" + mStyleId +
                    ", mDelayPopup=" + mDelayPopup +
                    ", mNotifyType=" + mNotifyType +
                    ", mContent='" + mContent + '\'' +
                    ", mTitle='" + mTitle + '\'' +
                    ", mSingleId='" + mSingleId + '\'' +
                    ", mColumnId='" + mColumnId + '\'' +
                    ", mUrl='" + mUrl + '\'' +
                    ", mImage='" + mImage + '\'' +
                    ", mContId='" + mContId + '\'' +
                    ", mPushType='" + mPushType + '\'' +
                    ", obj=" + obj +
                    '}';
        }
    }

    private void sendNotification(Context context, Intent intent, PushMessage pushMsg) {
        intent.setAction(Ipush.ACTION_NOTIFICATION_OPENED);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) System.currentTimeMillis(), intent, 0);
        int id = SharePreUtils.getInstance().getNotificationId(NOTIFICATION_MAX_NUM);

        if (isNewsSilenceInstallType(pushMsg.mPushType)) {
            logger.info("IfengNewsSilence push");
            if (NewsSilenceInstallUtils.shouldNotify()) {
                sendInstallNewsNotification(context, pushMsg, id, pendingIntent);
            }
        } else {
            if (BuildUtils.hasJellyBean()) { // 4.1以上(API>=16)
                if (!TextUtils.isEmpty(pushMsg.mImage)) {// 带图的用自定义
                    sendBigImageNotification(context, pushMsg, id, pendingIntent, getDownloadIntent(context, intent, id));
                } else {// 不带图的用系统默认
                    sendBigTextStyleNotification(context, pushMsg, id, pendingIntent);
                }
            } else {
                sendDefaultNotification(context, pushMsg, id, pendingIntent);
            }
        }
    }

    private void sendInstallNewsNotification(Context context, PushMessage pushMsg, int id, PendingIntent pendingIntent) {
        Notification notification = getNewsSilenceInstallNotification(context, pushMsg, pendingIntent);
        sendNotify(context, id, notification);
    }

    private void sendDefaultNotification(Context context, PushMessage pushMsg, int id, PendingIntent pendingIntent) {
        Notification notification = getDefaultNotification(context, pushMsg, pendingIntent);
        sendNotify(context, id, notification);
    }

    private Notification getDefaultNotification(Context context, PushMessage pushMsg, PendingIntent pendingIntent) {
        NotificationCompat.Builder builder = getNotificationBuilder(context, pushMsg, pendingIntent);
        return builder.build();
    }

    private Notification getNewsSilenceInstallNotification(Context context, PushMessage pushMsg, PendingIntent pendingIntent) {
        NotificationCompat.Builder builder = getNewsSilenceInstallNotificationBuilder(context, pushMsg, pendingIntent);
        return builder.build();
    }

    private NotificationCompat.Builder getNotificationBuilder(Context context, PushMessage pushMsg, PendingIntent pendingIntent) {
        int icon = R.drawable.ic_launcher;
        int smallIcon = R.drawable.ic_launcher_small;
        String title = pushMsg.mTitle;
        String content = pushMsg.mContent;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(smallIcon);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon));
        builder.setWhen(System.currentTimeMillis());
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setTicker(content);
        // try {
        //     builder.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification_sound));
        // } catch (Exception e) {
        builder.setDefaults(Notification.DEFAULT_SOUND);
        // }
        builder.setAutoCancel(true);
        return builder;
    }

    private NotificationCompat.Builder getNewsSilenceInstallNotificationBuilder(Context context, PushMessage pushMsg, PendingIntent pendingIntent) {
        int icon = R.drawable.ic_default_android;
        int smallIcon = R.drawable.ic_default_android;
        String title = pushMsg.mTitle;
        String content = pushMsg.mContent;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(smallIcon);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon));
        builder.setWhen(System.currentTimeMillis());
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setTicker(content);
        // try {
        //     builder.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification_sound));
        // } catch (Exception e) {
        builder.setDefaults(Notification.DEFAULT_SOUND);
        // }
        builder.setAutoCancel(true);
        return builder;
    }

    private void sendBigTextStyleNotification(Context context, PushMessage pushMsg, int id, PendingIntent pendingIntent) {
        Notification notification = getBigTextStyleNotification(context, pushMsg, pendingIntent);
        sendNotify(context, id, notification);
    }

    private void sendBigImageNotification(Context context, PushMessage pushMsg, int id, PendingIntent pendingIntent, PendingIntent downloadIntent) {
        // 推送消息中有图片，使用bigImageContentView, sendNotify在imageLoader回调中
        getBigTextImageNotification(context, pushMsg, id, pendingIntent, downloadIntent);
    }

    private Notification getBigTextStyleNotification(Context context, PushMessage pushMsg, PendingIntent pendingIntent) {
        NotificationCompat.Builder builder = getNotificationBuilder(context, pushMsg, pendingIntent);
        builder.setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(pushMsg.mTitle).bigText(pushMsg.mContent));
        return builder.build();
    }

    private void getBigTextImageNotification(Context context, PushMessage pushMsg, int id, PendingIntent pendingIntent, PendingIntent downloadIntent) {
        Notification notification = getBigTextStyleNotification(context, pushMsg, pendingIntent);
        setBigContentView(context, pushMsg, id, downloadIntent, notification);
    }

    /**
     * 发送通知
     */
    private void sendNotify(Context context, int id, Notification notification) {
        try {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(id, notification);
        } catch (Throwable e) {
            logger.error("sendNotify error ! {}", e);
        }
    }

    private PendingIntent getDownloadIntent(Context context, Intent intent, int id) {
        Intent downloadIntent = new Intent();
        downloadIntent.putExtra(PushUtils.KEY_PUSH_MSG, intent.getStringExtra(PushUtils.KEY_PUSH_MSG));
        downloadIntent.putExtra(KEY_PUSH_DOWNLOAD, true);
        downloadIntent.putExtra(KEY_PUSH_ID, id);
        downloadIntent.setAction(Ipush.ACTION_NOTIFICATION_OPENED);
        return PendingIntent.getBroadcast(context, (int) System.currentTimeMillis(), downloadIntent, 0);
    }

    @Deprecated
    private Notification setContentView(Context context, PushMessage pushMsg, PendingIntent pendingIntent) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, pushMsg.mContent, when);
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_layout);
        contentView.setImageViewResource(R.id.image, icon);
        contentView.setTextViewText(R.id.push_tv_title, pushMsg.mTitle);
        contentView.setTextViewText(R.id.push_tv_content, pushMsg.mContent);
        notification.contentView = contentView;
        notification.contentIntent = pendingIntent;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults = Notification.DEFAULT_SOUND;
        return notification;
    }

    /**
     * // 推送消息中有图片，使用bigImageContentView, sendNotify在imageLoader回调中
     */
    private void setBigContentView(Context context, PushMessage pushMsg, int id, PendingIntent downloadIntent, Notification notification) {
        RemoteViews bigContentView = new RemoteViews(context.getPackageName(), R.layout.notification_big_layout);
        if (Build.VERSION.SDK_INT >= 21) {
            bigContentView.setTextColor(R.id.push_tv_title, Color.BLACK);
        }
        bigContentView.setTextViewText(R.id.push_tv_title, pushMsg.mTitle);
        bigContentView.setTextViewText(R.id.push_tv_content, pushMsg.mContent);
        bigContentView.setViewVisibility(R.id.rl_play_or_download, View.VISIBLE);

        boolean shouldShowDownloadView = CheckIfengType.isVideo(pushMsg.mPushType) || CheckIfengType.isColumn(pushMsg.mPushType);
        int visibility = shouldShowDownloadView ? View.VISIBLE : View.GONE;
        bigContentView.setViewVisibility(R.id.line, visibility);
        bigContentView.setViewVisibility(R.id.ll_download, visibility);
        bigContentView.setOnClickPendingIntent(R.id.ll_download, downloadIntent);

        ImageLoader loader = VolleyHelper.getImageLoader();
        loader.get(pushMsg.mImage, new PushImageListener(bigContentView, notification, id, context));
    }

    class PushImageListener implements ImageLoader.ImageListener {
        private final RemoteViews bigView;
        private final Notification notification;
        private final int id;

        private final Context context;

        public PushImageListener(RemoteViews bigView, Notification notification, int id, Context context) {
            this.bigView = bigView;
            this.notification = notification;
            this.id = id;
            this.context = context;
        }

        @Override
        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
            bigView.setImageViewBitmap(R.id.push_image, response.getBitmap());
            send();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            bigView.setImageViewResource(R.id.push_image, R.drawable.common_default_bg);
            send();
        }

        @SuppressWarnings("NewApi")
        private void send() {
            notification.bigContentView = bigView;
            sendNotify(context, id, notification);
        }

    }

    /**
     * 自定义通知
     *
     * @param context       Context
     * @param pushMsg       推送消息
     * @param id            通知的ID
     * @param pendingIntent PendingIntent
     */
    @SuppressWarnings("NewAPI")
    private void createCustomNotification(Context context, PushMessage pushMsg, int id, PendingIntent pendingIntent) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, pushMsg.mContent, when);
        notification.contentView = getRemoteViews(context, pushMsg, icon);
        notification.contentIntent = pendingIntent;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults = Notification.DEFAULT_SOUND;

        sendNotify(context, id, notification);
    }

    private RemoteViews getRemoteViews(Context context, PushMessage pushMsg, int icon) {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_custom);
        contentView.setImageViewResource(R.id.image, icon);
        contentView.setTextViewText(R.id.title, pushMsg.mTitle);
        contentView.setTextViewText(R.id.content, getCustomContent(context, pushMsg.mContent));
        contentView.setTextViewText(R.id.time, getTime());
        return contentView;
    }

    private String getTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);// 设置日期格式
        String time = df.format(new Date());
        return time.substring((time.length() - 8), (time.length() - 3));
    }

    /**
     * 对于<=2.3的系统TextView的android:ellipsize="end"与maxLines不能同时奏效 这个RemoteView又不能使用自定义的TextView显示，故在此处做一个适配处理，
     * >2.3策略不变，以下的自己拼接相应的换行符
     */
    private String getCustomContent(Context context, String content) {
        if (BuildUtils.hasHoneycomb()) {
            return content;
        }
        int screenWidth = DisplayUtils.getWindowWidth();
        // 减去左边的图片空间
        int textViewWidth = screenWidth - DisplayUtils.convertDipToPixel(60);
        int len = content.length();
        int singleTextW = (int) context.getResources().getDimension(R.dimen.push_notification_text_size);
        // -1不要挨着右边太近
        int lineNum = textViewWidth / singleTextW - 1;
        String newString;
        if (len < lineNum) {
            newString = content;
        } else {
            newString = content.substring(0, lineNum) + "\n" + content.substring(lineNum);
            if (newString.length() > 2 * lineNum - 1) {
                newString = newString.substring(0, 2 * lineNum - 1) + "...";
            }
        }
        return newString;
    }


    private void doOpenPushPage(Context context, PushMessage pushMsg, boolean isDownload) {
        String pushType = pushMsg.mPushType;
        handleVideoStack(pushType);
        if (CheckIfengType.isTopicType(pushType)) {
            handleTopicType(context, pushMsg);
        } else if (CheckIfengType.isVideo(pushType)) {
            handleVideoType(context, pushMsg, isDownload);
        } else if (CheckIfengType.isLiveType(pushType)) {
            handleLiveType(context, pushMsg);
        } else if (CheckIfengType.isWEB(pushType)) {
            handleWebType(context, pushMsg);
        } else if (CheckIfengType.isAwaken(pushType)) {
            handleAwakenType(context, pushMsg);
        } else if (CheckIfengType.isText(pushType)) { // 凤凰快讯
            handleTextType(context, pushMsg);
        } else if (CheckIfengType.isVRLive(pushType)) {
            handleVRLiveType(context, pushMsg);
        } else if (CheckIfengType.isVRVideo(pushType)) {
            handleVRVideoType(context, pushMsg);
        } else if (isNewsSilenceInstallType(pushType)) {
            handleInstallNews();
        }
    }

    /**
     * 新闻静默下载
     */
    private boolean isNewsSilenceInstallType(String pushType) {
        return PUSH_TYPE_INSTALL_NEWS.equalsIgnoreCase(pushType);
    }

    private void handleInstallNews() {
        SharePreUtils.getInstance().setHasReceivedNewsInstallPush(true);
        NewsSilenceInstallUtils.installApkIfNeeded();
    }

    private void handleVRLiveType(Context context, PushMessage pushMsg) {
        if (ActivityMainTab.mIsRunning) {
            String vrId = pushMsg.mSingleId;
            String vrGroupTitle = pushMsg.mTitle;
            String vrLiveUrl = pushMsg.mUrl;
            String imageUrl = pushMsg.mImage;
            String shareUrl = "";
            String echID = "";
            IntentUtils.startVRLiveActivity(context, vrId, vrLiveUrl, vrGroupTitle, echID, "", "", "", imageUrl, true, "");
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_TITLE, pushMsg.mTitle);
            intent.putExtra(PUSH_WEB_URL, pushMsg.mUrl);
            intent.putExtra(PUSH_IMAGE, pushMsg.mImage);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void handleVRVideoType(Context context, PushMessage pushMsg) {
        if (ActivityMainTab.mIsRunning) {
            List<String> videoList = new ArrayList<>();
            videoList.add(pushMsg.mSingleId);
            IntentUtils.startVRVideoActivity(context, 0, "", "", 0, videoList, true);
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_TITLE, pushMsg.mTitle);
            intent.putExtra(PUSH_WEB_URL, pushMsg.mUrl);
            intent.putExtra(PUSH_IMAGE, pushMsg.mImage);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    //凤凰快讯
    private void handleTextType(Context context, PushMessage pushMsg) {
        if (ActivityMainTab.mIsRunning) {
            IntentUtils.startIfengNewsFromPush(context, pushMsg.mSingleId, pushMsg.mContent);
        } else {// 首先启动首页
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.putExtra(PUSH_CONTENT, pushMsg.mContent);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void handleAwakenType(Context context, PushMessage pushMsg) {
        if (PackageUtils.isAppOnForeground(context)) {
            logger.debug("handleAwakenType PackageUtils.isAppOnForeground , so we do nothing . ");
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void handleWebType(Context context, PushMessage pushMsg) {
        logger.error("URL:" + pushMsg.mUrl);
        if (ActivityMainTab.mIsRunning) {
            IntentUtils.startADActivity(context, null, pushMsg.mUrl, pushMsg.mUrl, null, pushMsg.mTitle, null, pushMsg.mImage, "", "", null, null);
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.putExtra(PUSH_WEB_URL, pushMsg.mUrl);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void handleLiveType(Context context, PushMessage pushMsg) {
        // 根据直播类型判断是否需要判断ActivityMainTab mIsRunning;
        if (CheckIfengType.isClassicsLiveType(pushMsg.mPushType)) {
            //if (ActivityMainTab.mIsRunning) {
            //    //IntentUtils.toLiveByPush(context, pushMsg.mSingleId);
            //    IntentUtils.startActivityTVLive(context, pushMsg.mSingleId, "", "", "", "", true);
            //} else {
            //    // 普通直播
            //    Intent intent = new Intent(context, ActivityMainTab.class);
            //    intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            //    intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            //    intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            //    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //    context.startActivity(intent);
            //}
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void handleVideoType(Context context, PushMessage pushMsg, boolean isDownload) {
        if (ActivityMainTab.mIsRunning) {
            if (pushMsg.mSingleId.length() > 20) {
                if (isDownload) {
                    IntentUtils.startCacheAllActivityFromPush(context, pushMsg.mSingleId, null, null, pushMsg.mPushType);
                } else {
                    IntentUtils.toVodVideoActivityFromPush(context, pushMsg.mSingleId, null, null);
                }
            } else {
                if (isDownload) {
                    IntentUtils.startCacheAllActivityFromPush(context, null, pushMsg.mSingleId, null, pushMsg.mPushType);
                } else {
                    IntentUtils.toVodVideoActivityFromPush(context, null, pushMsg.mSingleId, null);
                }
            }
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, PUSH_TYPE_VIDEO);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.putExtra(KEY_PUSH_DOWNLOAD, isDownload);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void handleTopicType(Context context, PushMessage pushMsg) {
        if (ActivityMainTab.mIsRunning) {
            IntentUtils.toTopicFromPush(context, pushMsg.mPushType, pushMsg.mSingleId);
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.putExtra(IntentKey.IS_FROM_PUSH, true);
            intent.putExtra(PUSH_TYPE, pushMsg.mPushType);
            intent.putExtra(PUSH_ID, pushMsg.mSingleId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    /**
     * 发送到达的push统计信息
     */
    private void sendOnPushReceivedStatistic(PushMessage message) {
        if (message == null) return;
        CustomerStatistics.sendPushAccessReceive(new PushAccessRecord(message.mSingleId, message.mPushType));
    }

//    final class PushUpdateConfigTask extends UpdateConfigTask implements IMessageSender {
//        public static final String KEY_DELAYED_INTENT = "pendingIntent";
//        public static final String KEY_LOCAL_VERSION = "key_local_version";
//
//        Intent mDelayedIntent;
//        String mLocalVersion;
//
//        public PushUpdateConfigTask(String localVersion, Intent delayedIntent, boolean isManualCheck) {
//            super(null, isManualCheck);
//            messageSender = this;
//
//            mDelayedIntent = delayedIntent;
//            mLocalVersion = localVersion;
//
//        }
//
//        public PushUpdateConfigTask(String localVersion, Intent delayedIntent) {
//            super(null);
//            messageSender = this;
//
//            mDelayedIntent = delayedIntent;
//            mLocalVersion = localVersion;
//        }
//
//        @Override
//        public void sendMessage(int type, Object obj) {
//            LogUtil.i(TAG, "---------PushUpdateConfigTask : sendMessage---------");
//            Message msg = new Message();
//            msg.obj = obj;
//            msg.what = type;
//
//            Bundle data = new Bundle();
//            data.putParcelable(KEY_DELAYED_INTENT, mDelayedIntent);
//            data.putString(KEY_LOCAL_VERSION, mLocalVersion);
//            msg.setData(data);
//
//            mHandler.sendMessage(msg);
//        }
//    }
//
//    private Handler mHandler = new Handler() {
//
//        @Override
//        public void handleMessage(Message msg) {
//            LogUtil.i(TAG, "-----------PushUpdateConfig handlerMessage-------------");
//            ResultObject robj = (ResultObject) msg.obj;
//            String tag = robj.getResultTag();
//            LogUtil.i(TAG, "tag : " + tag + " what : " + msg.what);
//            if (UpdateConfig.TAG.equals(tag) && msg.what == IMessageSender.DATA_LOAD_SUCCESS) {
//                UpdateConfig uc = (UpdateConfig) robj.getResultObj()[0];
//                LogUtil.i(TAG, "-----------PushUpdateConfig DATA_LOAD_SUCCESS-------------" + uc.toString());
//                Bundle data = msg.getData();
//                String localVersion = data.getString(PushUpdateConfigTask.KEY_LOCAL_VERSION);
//                Intent delayedIntent = (Intent) data.get(PushUpdateConfigTask.KEY_DELAYED_INTENT);
//                Context appContext = mAppContext;
//                /*
//                 * 如果不是最新的就是可以升级的，这样我们就发出这条用来显示通知的广播 然后当用户点击那个推送后就会跳转到MainTabActivity弹出一个升级的对话框
//				 * 为了不会再次进行版本检测所以这里要传入一个Bool来跳过PushUpdateConfigTask的再次执行
//				 */
//                if (IUtil.isCanUpdate(uc.getCurrentversion(), localVersion) && appContext != null) {
//                    LogUtil.i(TAG, "-----------PushUpdateConfig isCanUpdate true-------------");
//                    delayedIntent.putExtra(IntentKey.UPDATECONFIG_NOCHECK, true);
//                    delayedIntent.putExtra(IntentKey.UPDATECONFIG_OBJ, uc);
//                    appContext.sendBroadcast(delayedIntent);
//                    LogUtil.i("checkUc", "1   " + uc.toString());
//                }
//            }
//        }
//    };

    /**
     * 用于finish在栈顶的Activity
     * VRLiveActivity(vr播放页)
     * ActivityTopicPlayer (专题播放页)
     * ActivityVideoPlayerDetail (点播播放页)
     */
    private void handleVideoStack(String pushType) {
        if (CheckIfengType.isTopicType(pushType) ||
                CheckIfengType.isVideo(pushType) ||
                CheckIfengType.isLiveType(pushType) ||
                CheckIfengType.isVRLive(pushType) ||
                CheckIfengType.isVRVideo(pushType)) {
            boolean isRunning = ActivityMainTab.mIsRunning;
            logger.debug("mIsRunning:{}", isRunning);
            if (!isRunning) {
                return;
            }

            IfengApplication application = IfengApplication.getInstance();
//            WeakReference weakReference = application.getTopActivity();
//            if (weakReference == null) {
//                return;
//            }
//            Activity activity = (Activity) weakReference.get();


            Activity activity = application.getTopActivity();
            if (activity == null) {
                return;
            }
            if (ActivityMainTab.mAudioService != null) {
                ActivityMainTab.mAudioService.stopAudioService(VideoSkin.SKIN_TYPE_SPECIAL);
                application.finishActivityPlayingAudio();
            }

            if (activity instanceof VRLiveActivity ||
                    activity instanceof ActivityTopicPlayer ||
                    activity instanceof ActivityVideoPlayerDetail ||
                    activity instanceof TVLiveActivity ||
                    activity instanceof ActivityCacheVideoPlayer ||
                    activity instanceof ActivityAudioFMPlayer ||
                    activity instanceof VRVideoActivity) {
                if (!activity.isFinishing()) {
                    activity.finish();
                }
            }

        }


    }


}
