package com.ifeng.newvideo.push.impl;

import com.huawei.android.pushagent.api.PushManager;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.utils.IPushUtils;

/**
 * Created by ll on 2017/9/4.
 */
public class IpushImpl implements PushBridge {
    @Override
    public void initPush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        //关闭华为push
        PushManager.enableReceiveNormalMsg(IfengApplication.getInstance(), false);
        PushManager.enableReceiveNotifyMsg(IfengApplication.getInstance(), false);

        IPushUtils.initIPush();
        IPushUtils.setIPushTag(false);
    }

    @Override
    public void startOrResumePush() {

    }

    @Override
    public void pausePush() {

    }

    @Override
    public void closePush() {

    }
}
