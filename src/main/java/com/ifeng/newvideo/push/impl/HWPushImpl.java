package com.ifeng.newvideo.push.impl;

import com.huawei.android.pushagent.api.PushManager;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.push.DealClientIdOfPush;
import com.ifeng.newvideo.push.HuaWeiPushReceiver;

/**
 * Created by ll on 2017/9/4.
 */
public class HWPushImpl implements PushBridge{
    @Override
    public void initPush() {

        if (IfengApplication.getInstance() == null) {
            return;
        }
        //关闭ipush
        Ipush.stopService(IfengApplication.getInstance());
        //启动华为推送
        PushManager.requestToken(IfengApplication.getInstance());
    }

    @Override
    public void startOrResumePush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        PushManager.enableReceiveNormalMsg(IfengApplication.getInstance(), true);
        PushManager.enableReceiveNotifyMsg(IfengApplication.getInstance(), true);
    }

    @Override
    public void pausePush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        PushManager.enableReceiveNormalMsg(IfengApplication.getInstance(), false);
        PushManager.enableReceiveNotifyMsg(IfengApplication.getInstance(), false);

        new DealClientIdOfPush(HuaWeiPushReceiver.PLATFORM).closePush(IfengApplication.getInstance());
    }

    @Override
    public void closePush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        PushManager.enableReceiveNormalMsg(IfengApplication.getInstance(), false);
        PushManager.enableReceiveNotifyMsg(IfengApplication.getInstance(), false);
    }
}
