package com.ifeng.newvideo.push.impl;

/**
 * Created by ll on 2017/9/4.
 */
public interface PushBridge {
    public void initPush();

    public void startOrResumePush();

    public void pausePush();

    public void closePush();
}
