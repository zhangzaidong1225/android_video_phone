package com.ifeng.newvideo.push.impl;

import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.IfengApplication;
import com.xiaomi.mipush.sdk.MiPushClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ll on 2017/10/20.
 */
public class MiPushImpl implements PushBridge {
    private static final String APP_ID = "2882303761517133549";
    private static final String APP_KEY = "5901713370549";
    private static final Logger logger = LoggerFactory.getLogger(MiPushImpl.class);
    @Override
    public void initPush() {

    }

    @Override
    public void startOrResumePush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        try {
            //关闭ipush
            Ipush.stopService(IfengApplication.getInstance());
            MiPushClient.registerPush(IfengApplication.getInstance(), APP_ID, APP_KEY);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public void pausePush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        try {
            MiPushClient.unregisterPush(IfengApplication.getInstance());
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public void closePush() {
        if (IfengApplication.getInstance() == null) {
            return;
        }
        try {
            MiPushClient.unregisterPush(IfengApplication.getInstance());
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
