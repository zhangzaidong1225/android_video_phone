package com.ifeng.newvideo.push;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.live.weblive.ActivityH5Live;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlarmReceiver extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(AlarmReceiver.class);

    /**
     * Bundle Extra
     */
    public static final String PUSH_EXTRA_BUNDLE = "extra.com.ifeng.newsvideo.push.bundle";
    public static final String SPORTS_LIVE_MESSAGE_ACTION = "action.com.ifeng.newsvideo.sportslive.message";
    public static final String ACTION_NOTIFICATION_RECEIVED = "com.ifeng.newvideo.intent.NOTIFICATION_RECEIVED";
    public static final String ACTION_NOTIFICATION_OPENED = "com.ifeng.newvideo.intent.NOTIFICATION_OPENED";
    public static final String ACTION_MESSAGE_RECEIVED = "com.ifeng.newvideo.intent.ACTION_MESSAGE_RECEIVED";

    private static final String H5_LIVE_URl = "http://izhibo.ifeng.com/live.html?liveid=%s";

    public String title, aid, type, url;
    public long datetime;


    @Override
    public void onReceive(Context context, Intent intent) {

        if (SPORTS_LIVE_MESSAGE_ACTION.equals(intent.getAction())) {
            logger.debug("jsb:{}", "sports_live_action");
            Bundle mPushBundle = intent
                    .getBundleExtra(PUSH_EXTRA_BUNDLE);
            type = mPushBundle.getString("type");
            title = mPushBundle.getString("title");
            aid = mPushBundle.getString("aid");
            datetime = mPushBundle.getLong("datatime");
            url = String.format(H5_LIVE_URl, aid);
            sendNotification(context, title, type, aid, datetime, intent);
        }

        // 收到推送消息时
        if (ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            logger.debug("jsb:{}", "action_notification_received");

        } else if (ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            logger.debug("jsb:{}", "action_notification_open");

            Bundle mPushBundle = intent
                    .getBundleExtra(PUSH_EXTRA_BUNDLE);
            type = mPushBundle.getString("type");
            title = mPushBundle.getString("title");
            aid = mPushBundle.getString("aid");
            datetime = mPushBundle.getLong("datatime");
            url = String.format(H5_LIVE_URl, aid);

            AlarmMsg alarmMsg = new AlarmMsg();
            alarmMsg.setTitle(title);
            alarmMsg.setAid(aid);
            alarmMsg.setType(type);
            alarmMsg.setUrl(url);
            alarmMsg.setDatetime(datetime);
            logger.debug("jsb:{}", "action_notification_open:" + alarmMsg.toString());

            handleWebType(context, alarmMsg);
        }

    }


    private void sendNotification(Context context, String title, String type, String aid, long time, Intent intent) {

        intent.setAction(ACTION_NOTIFICATION_OPENED);
        Bundle b = new Bundle();
        b.putCharSequence("title", title);
        b.putString("type", "plv");
        b.putString("aid", aid);
        b.putLong("datetime", time);
        intent.putExtra(PUSH_EXTRA_BUNDLE, b);
        NotificationManager barmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notice;

        PendingIntent contentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        int icon = R.drawable.ic_launcher;
        int smallIcon = R.drawable.ic_launcher_small;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(smallIcon);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon));
//        builder.setWhen(time);
        builder.setContentIntent(contentIntent);
        builder.setContentTitle(title);
//        builder.setContentText(title);
        builder.setTicker(title);
        builder.setDefaults(Notification.DEFAULT_SOUND);

        builder.setAutoCancel(true);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            logger.debug("jsb", "版本大于16");
        notice = builder.build();
        notice.flags = Notification.FLAG_AUTO_CANCEL;
        barmanager.notify(Integer.parseInt(aid), notice);
//        }
    }


    private void handleWebType(Context context, AlarmMsg alarmMsg) {
        logger.debug("jsb-URL:{}", alarmMsg.url);

        if (ActivityMainTab.mIsRunning) {
            Intent intent = new Intent(context, ActivityH5Live.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("type", alarmMsg.type);
            intent.putExtra("title", alarmMsg.title);
            intent.putExtra("aid", alarmMsg.aid);
            intent.putExtra("url", alarmMsg.url);
            context.startActivity(intent);
        } else {
            Intent intent = new Intent(context, ActivityMainTab.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.putExtra(IntentKey.IS_FROM_ALARM, true);
            intent.putExtra("type", alarmMsg.type);
            intent.putExtra("title", alarmMsg.title);
            intent.putExtra("aid", alarmMsg.aid);
            intent.putExtra("url", alarmMsg.url);
            context.startActivity(intent);
        }
    }

    public static class AlarmMsg {
        public String title;
        public String type;
        public String aid;
        public long datetime;
        public String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAid() {
            return aid;
        }

        public void setAid(String aid) {
            this.aid = aid;
        }

        public long getDatetime() {
            return datetime;
        }

        public void setDatetime(long datetime) {
            this.datetime = datetime;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "AlarmMsg{" +
                    "title='" + title + '\'' +
                    ", type='" + type + '\'' +
                    ", aid='" + aid + '\'' +
                    ", datetime=" + datetime +
                    ", url='" + url + '\'' +
                    '}';
        }
    }
}
