package com.ifeng.newvideo.push.bean;

import android.text.TextUtils;

import java.io.Serializable;

public class MiPushPayLoad implements Serializable {

	private static final long serialVersionUID = 1L;
	private String type;
	private String id;
	private String aid;

	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public MiPushPayLoad() {
		super();
	}

	public String getId() {
		return null == id ? "" : id.trim();
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return null == type ? "" : type.trim();
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isEmpty() {

		boolean result = false;
		if (TextUtils.isEmpty(type) || TextUtils.isEmpty(id)) {
			result = true;
		}
		return result;

	}

	@Override
	public String toString() {
		return "MiPushPayLoad [id=" + id + ", type=" + type + "]";
	}

}
