package com.ifeng.newvideo.push.bean;

import java.io.Serializable;

/**
 * Created by ll on 2017/9/4.
 */
public class UploadPushTokenBean implements Serializable {
    private static final long serialVersionUID = -2354342158782229627L;
    private String errCode;
    private boolean success;
    private String msg;


    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
