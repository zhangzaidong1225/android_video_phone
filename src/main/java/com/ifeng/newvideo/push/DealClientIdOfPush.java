package com.ifeng.newvideo.push;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.push.bean.UploadPushTokenBean;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.DistributionInfo;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.dao.db.dao.CommonDao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 将推送sdk获得的用户唯一id，上传到服务器
 * Created by ll on 2017/9/4.
 */
public class DealClientIdOfPush {
    private final static String HOST_TEST = "http://10.90.9.169/register";

    public final static String LOCAL_PUSH_CLIENT_ID_KEY = "local_push_client_id_key";
    public final static String UPLOAD_METHOD_PUT = "put";
    public final static String UPLOAD_METHOD_REPLACE = "replace";
    public final static String UPLOAD_METHOD_DELETE = "delete";

    private final static int REQUEST_TYPE_HTTP = 0;
    private final static int REQUEST_TYPE_HTTPS = 1;

    /**
     * 平台：hauwei、xiaomi、getui 等等
     */
    private String platform = "";

    public DealClientIdOfPush(String platform) {
        this.platform = platform;
    }

    /**
     * app 内关闭推送开关
     *
     * @param context
     */
    public void closePush(Context context) {
        String localToken = SharePreUtils.getStringByName(context, LOCAL_PUSH_CLIENT_ID_KEY, "");
        uploadToken(context, localToken, UPLOAD_METHOD_DELETE);
    }

    /**
     * app 内打开推送开关
     *
     * @param context
     */
    public void reOpenPush(Context context) {
        String localToken = SharePreUtils.getStringByName(context, LOCAL_PUSH_CLIENT_ID_KEY, "");
        if (TextUtils.isEmpty(localToken)) {
            return;
        }
        uploadToken(context, localToken, UPLOAD_METHOD_PUT);
    }

    /**
     * 处理token
     *
     * @param context
     * @param token
     */
    public void dealWithToken(Context context, String token) {
        if (TextUtils.isEmpty(token)) {
            return;
        }
        SharePreUtils.setStringByName(context, LOCAL_PUSH_CLIENT_ID_KEY, token);
        uploadToken(context, token, UPLOAD_METHOD_PUT);
    }

    /**
     * token 上报
     */
    private void uploadToken(final Context context, String token, String method) {
        if (TextUtils.isEmpty(token)) {
            Log.d("ifengVideo", "upload token, token is null");
            return;
        }

        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        String province = sharePreUtils.getProvince();
        String city = sharePreUtils.getCity();
        String district = sharePreUtils.getDistrict();

        HashMap<String, String> params = new HashMap<>();
        params.put("bundleId", context.getPackageName());
        params.put("token", token);
        params.put("method", method);
        params.put("tags", province + "," + city + "," + district);
        params.put("deviceId", PhoneConfig.UID);
        params.put("gv", PhoneConfig.softversion);
        params.put("av", PhoneConfig.softversion);
        params.put("uid", User.getUid());
        params.put("proid", "ifengvideo");
        params.put("os", PhoneConfig.mos);
        params.put("df", PhoneConfig.getDeviceFamily());
        params.put("vt", PhoneConfig.getVTForPush());
        params.put("screen", PhoneConfig.getScreenSizeForPush());
        params.put("platform", platform);

        String host;
        //TODO 测试环境接口
        if ("true".equals(DistributionInfo.isPushTest)) {
            host = HOST_TEST;
        } else {
            host = DataInterface.UPLOAD_PUSH_CLIENT_ID_URL;
        }
        final String httpUrl = getUrl(host, params);
        CommonDao.sendRequest(httpUrl, UploadPushTokenBean.class,
                new Response.Listener<UploadPushTokenBean>() {
                    @Override
                    public void onResponse(UploadPushTokenBean response) {
                        if (response == null || TextUtils.isEmpty(response.toString())) {
                            return;
                        }
                        if (!"0".equals(response.getErrCode())) {
                            SharePreUtils.setStringByName(context, LOCAL_PUSH_CLIENT_ID_KEY, "");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }, CommonDao.RESPONSE_TYPE_GET_JSON);
    }

    private String getUrl(String url, HashMap<String, String> params) {

        StringBuffer sb = new StringBuffer();
        sb.append(url);
        if (params == null || params.size() == 0) {
            return sb.toString();
        }

        int i = 0;
        Iterator it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
            if (i == 0) {
                sb.append("?");
            } else {
                sb.append("&");
            }
            sb.append(entry.getKey());
            sb.append("=");
            sb.append(entry.getValue());
            i++;
        }
        return sb.toString();
    }
}
