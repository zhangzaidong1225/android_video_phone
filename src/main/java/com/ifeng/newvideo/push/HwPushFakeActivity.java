package com.ifeng.newvideo.push;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.PushUtils;

/**
 * 华为的非透传(通知栏)推送策略：后台参数传的intent uri中指定这个activity，如不传则默认启动main activity
 * 有了这个参数就不用实现 HuaWeiPushReceiver onEvent
 * Created by ll on 2017/9/5.
 */
public class HwPushFakeActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hw_fake);
        Intent it = getIntent();
        Bundle mBundle = it.getExtras();

        String id = mBundle.getString("id");
        String type = mBundle.getString("type");
        String url = mBundle.getString("url");
        String msg = "{ \"extra\":{ \"id\":\"" + id + "\", \"type\":\"" + type + "\", \"url\":\"" + url + "\"}}";

        mBundle.putString(PushUtils.KEY_PUSH_MSG, msg);
        mBundle.putString(PushUtils.PUSH_MESSAGE_TYPE, PushUtils.TYPE_MESSAGE_NOTIFY);
        mBundle.putInt(PushUtils.PUSH_RESOURCE, PushUtils.RESOURCE_HUAWEI);

        Intent pushIntent = new Intent(Ipush.ACTION_NOTIFICATION_RECEIVED);
        pushIntent.setPackage(getPackageName());
        pushIntent.putExtras(mBundle);
        sendBroadcast(pushIntent);

        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
