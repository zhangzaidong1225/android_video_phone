package com.ifeng.newvideo.push;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.Gson;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.push.bean.MiPushPayLoad;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.PushUtils;
import com.ifeng.video.core.utils.EmptyUtils;
import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.ifeng.video.core.utils.DistributionInfo.isPushTest;

/**
 * Created by ll on 2017/10/20.
 */
public class MiPushMessageReceiver extends PushMessageReceiver {
    public static final String TAG = "com.ifengvideo.mipush";
    private static final String PUSH_SRC = "pushMsgSrc";
    private static final String ACTION_FROM_APP = "ifengVideoServer";
    private static final String INFO_KEY = "info";
    //push订阅的TAG
    //正式版本
    public static final String TAG_XIAOMI_PUSH_TOPIC = "ifengVideoXiaomiPush";
    //DEBUG版本
    public static final String TAG_XIAOMI_PUSH_DEBUG_TOPIC = "ifengVideoXiaomiPushDebug";
    private static final Logger logger = LoggerFactory.getLogger(MiPushMessageReceiver.class);
    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        Log.d(TAG, "onReceivePassThroughMessage is called. " + miPushMessage.toString());
        dealWithPushMessage(context, miPushMessage);
    }

    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        Log.d(TAG, "onNotificationMessageClicked is called. " + miPushMessage.toString());
        dealWithPushMessage(context, miPushMessage);
    }

    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        Log.d(TAG, "onNotificationMessageArrived is called. " + miPushMessage.toString());
    }

    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        Log.d(TAG, "onReceiveRegisterResult is called. " + miPushCommandMessage.toString());
    }

    @Override
    public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        // 客户端向服务器发送数据请求后接受到的反馈信息。如设置 topic成功，反馈的成功信息.
        Log.d(TAG, "onCommandResult is called. " + miPushCommandMessage.toString());
        String command = miPushCommandMessage.getCommand();
        String failReason = miPushCommandMessage.getReason();
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
            if (miPushCommandMessage.getResultCode() == ErrorCode.SUCCESS) {
                try {
                    MiPushClient.setAlias(context, PhoneConfig.UID, null);
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                }
                Log.d(TAG, "onCommandResult is called. command rigister success. ");
            } else {
                Log.e(TAG, "onCommandResult is called. command rigister fail. " + failReason);
            }
        } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command)) {
            if (miPushCommandMessage.getResultCode() == ErrorCode.SUCCESS) {
                Log.d(TAG, "onCommandResult is called. command set alias success. ");
                if ("true".equals(isPushTest)) {
                    try {
                        MiPushClient.subscribe(context, TAG_XIAOMI_PUSH_DEBUG_TOPIC, null);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                    Log.d(MiPushMessageReceiver.TAG, "mipush subscribe debug.");
                } else {
                    try {
                        MiPushClient.subscribe(context, TAG_XIAOMI_PUSH_TOPIC, null);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                    Log.d(MiPushMessageReceiver.TAG, "mipush subscribe normal.");
                }
            } else {
                Log.e(TAG, "onCommandResult is called. command set alias fail. " + failReason);
            }
        } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
            if (miPushCommandMessage.getResultCode() == ErrorCode.SUCCESS) {
                Log.d(TAG, "onCommandResult is called. command subscribe success. ");
            } else {
                Log.e(TAG, "onCommandResult is called. command subscribe fail. " + failReason);
            }
        }
    }

    /***
     * 判断获取到的推送消息是否 有价值.
     *
     * @param miPushMessage
     *            小米服务器端推送过来的消息。
     * @return true: 消息有价值； false：消息无价值，滤掉
     * */
    private boolean isMessageValue(MiPushMessage miPushMessage) {

        boolean result = false;

        if (null != miPushMessage
                && !TextUtils.isEmpty(miPushMessage.getTitle())
                && !TextUtils.isEmpty(miPushMessage.getContent())
                && !TextUtils.isEmpty(miPushMessage.getDescription())) {
            result = true;
        }
        return result;
    }

    public void dealWithPushMessage(Context context, MiPushMessage miPushMessage) {
        if (isMessageValue(miPushMessage)) {
            try {
                Gson gson = new Gson();
                MiPushPayLoad payLoadBean = gson.fromJson(miPushMessage.getContent(), MiPushPayLoad.class);
                    runPushNoti(context, payLoadBean, miPushMessage,
                            miPushMessage.getPassThrough());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * 封装推送的消息发送给UI去处理.
     *
     * @param mContext
     *            payLoadBean MiPushPayLoad,里面包含了 消息的id和type title 和 content
     *            显示在notification上的标题和简介
     * */
    private void runPushNoti(Context mContext, MiPushPayLoad payLoadBean,
                             MiPushMessage miPushMessage, int msgType) {
        if (!payLoadBean.isEmpty() && EmptyUtils.isNotEmpty(miPushMessage)) {
            // 如果id,type为空也不再接受 推送消息.
            Bundle mBundle = new Bundle();
            String title = EmptyUtils.isEmpty(miPushMessage.getTitle()) ? "" : miPushMessage.getTitle().trim();
            String content = EmptyUtils.isEmpty(miPushMessage.getDescription()) ? "" : miPushMessage.getDescription().trim();
            String url = EmptyUtils.isEmpty(miPushMessage.getExtra().get("url")) ? "" : miPushMessage.getExtra().get("url").trim();
            String msg = "{ \"extra\":{ \"id\":\"" + payLoadBean.getId() + "\"," +
                    " \"type\":\"" + payLoadBean.getType() + "\", \"url\":\"" + url + "\" }, \"title\":\"" + title + "\", \"content\":\"" + content + "\"}";

            mBundle.putString(PushUtils.KEY_PUSH_MSG, msg);

            mBundle.putString("sound", "notification_sound");
            if (0 == msgType) {
                mBundle.putString(PushUtils.PUSH_MESSAGE_TYPE, PushUtils.TYPE_MESSAGE_NOTIFY);
            } else if (1 == msgType) {
                mBundle.putString(PushUtils.PUSH_MESSAGE_TYPE, PushUtils.TYPE_MESSAGE_PASS_THROUGH);
            }
            mBundle.putInt(PushUtils.PUSH_RESOURCE, PushUtils.RESOURCE_XIAOMI);
            Intent pushIntent = new Intent(
                    Ipush.ACTION_NOTIFICATION_RECEIVED);
            pushIntent.putExtras(mBundle);
            mContext.sendBroadcast(pushIntent);
        }
    }
}
