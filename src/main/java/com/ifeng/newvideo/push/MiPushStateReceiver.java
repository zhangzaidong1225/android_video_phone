package com.ifeng.newvideo.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.utils.FilterUtil;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.xiaomi.mipush.sdk.MiPushClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ll on 2017/10/20.
 */
public class MiPushStateReceiver extends BroadcastReceiver {
    private static final Logger logger = LoggerFactory.getLogger(MiPushStateReceiver.class);
    @Override
    public void onReceive(Context context, Intent intent) {
        if (FilterUtil.isXiaoMiMobile()) {
            String mAction = intent.getAction();
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(mAction)
                    && NetUtils.isNetAvailable(IfengApplication.getInstance())) {
                // 用户网络连接状态发生了改变，针对小米手机 ： 一旦网络连接成功，若状态为关闭，执行关闭操作，否则执行开启操作.
                if (SharePreUtils.getInstance().getPushMessageState()) {
                    // 推送状态为 打开.
                    try {
                        MiPushClient.resumePush(context, null);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                } else {
                    // 推送状态为关闭.
                    try {
                        MiPushClient.pausePush(context, null);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            }
        }
    }
}
