package com.ifeng.newvideo.push;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.huawei.android.pushagent.api.PushEventReceiver;
import com.ifeng.ipush.client.Ipush;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.domains.PushAccessRecord;
import com.ifeng.newvideo.utils.PushUtils;
import com.ifeng.newvideo.utils.SharePreUtils;

/**
 * 华为推送
 */
public class HuaWeiPushReceiver extends PushEventReceiver {

    private static final String TAG = "hwpush";
    public final static String PLATFORM = "huawei";

    @Override
    public void onToken(Context context, String token, Bundle extras) {
        if (SharePreUtils.getInstance().getPushMessageState()) {
            new DealClientIdOfPush(PLATFORM).dealWithToken(context, token);
        }

    }


    @Override
    public boolean onPushMsg(Context mContext, byte[] msg, Bundle bundle) {
        try {
            String content = new String(msg, "UTF-8");
            content = content.replaceAll("[{}\\[\\]]", "");
            content = "{" + content + "}";
            Bundle mBundle = new Bundle();
            mBundle.putString(PushUtils.PUSH_MESSAGE_TYPE, PushUtils.TYPE_MESSAGE_PASS_THROUGH);
            mBundle.putInt(PushUtils.PUSH_RESOURCE, PushUtils.RESOURCE_HUAWEI);
            mBundle.putString(PushUtils.KEY_PUSH_MSG, content);
            Intent pushIntent = new Intent(Ipush.ACTION_NOTIFICATION_RECEIVED);
            pushIntent.setPackage(mContext.getPackageName());
            pushIntent.putExtras(mBundle);
            mContext.sendBroadcast(pushIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onEvent(Context context, Event event, Bundle extras) {
        Log.d(TAG, "HuaWeiPushReceiver onEvent");

        if (Event.NOTIFICATION_OPENED.equals(event) || Event.NOTIFICATION_CLICK_BTN.equals(event)) {
            //由于HwPushFakeActivity的存在，推送的通知会打开HwPushFakeActivity，并且有其打开正文，因此
            //此处不在处理通知的点击事件
            if (true) {
                return;
            }
        }

        super.onEvent(context, event, extras);
    }

    private void runPushNoti(Context mContext, String title, String content,
                             String aid, String newsType, String pushType) {
        Log.d(TAG, "HuaWeiPushReceiver runPushNoti");
        Bundle mBundle = new Bundle();
        mBundle.putString("title", title);
        mBundle.putString("message", content);
        mBundle.putString("aid", aid);
        mBundle.putString("sound", "notification_sound");
        mBundle.putString("type", newsType);
        mBundle.putString(PushUtils.PUSH_MESSAGE_TYPE, pushType);
        mBundle.putInt(PushUtils.PUSH_RESOURCE, PushUtils.RESOURCE_HUAWEI);
//        mBundle.putBoolean(PushMessageReceiver.RUN_ACCESS_STATISTIC, false);  //在本类里发送统计
        Intent pushIntent = new Intent(Ipush.ACTION_NOTIFICATION_RECEIVED);
        pushIntent.setPackage(mContext.getPackageName());
        pushIntent.putExtra(PushUtils.PUSH_EXTRA_BUNDLE, mBundle);
        mContext.sendBroadcast(pushIntent);

        record(aid, newsType);
    }

    /**
     * 统计接收到的推送
     */
    private void record(String id, String type) {
        //如果用户在设置内取消了“打开通知”，则不需要再发推送到达统计了。
        boolean isSysAppNotiSetOpened = SharePreUtils.getInstance().getPushMessageState();
        if (!isSysAppNotiSetOpened) {
            return;
        }
        CustomerStatistics.sendPushAccessReceive(new PushAccessRecord(id, type));
    }
}