package com.ifeng.newvideo.statistics;

import android.content.Context;
import android.text.TextUtils;
import com.ifeng.newvideo.cache.activity.CacheAllActivity;
import com.ifeng.newvideo.cache.activity.CachingActivity;
import com.ifeng.newvideo.coustomshare.EditPage;
import com.ifeng.newvideo.login.activity.BindingPhoneActivity;
import com.ifeng.newvideo.login.activity.CheckPhoneNumberActivity;
import com.ifeng.newvideo.login.activity.ForgetPwdActivity;
import com.ifeng.newvideo.login.activity.LoginMainActivity;
import com.ifeng.newvideo.login.activity.RegisterActivity;
import com.ifeng.newvideo.member.DealRecordActivity;
import com.ifeng.newvideo.member.MemberCenterActivity;
import com.ifeng.newvideo.member.OpenMemberActivity;
import com.ifeng.newvideo.member.PointTaskActivity;
import com.ifeng.newvideo.search.activity.SearchActivity;
import com.ifeng.newvideo.search.activity.SearchWeMediaActivity;
import com.ifeng.newvideo.setting.activity.AboutActivity;
import com.ifeng.newvideo.setting.activity.CachePathActivity;
import com.ifeng.newvideo.setting.activity.IfengNewsActivity;
import com.ifeng.newvideo.setting.activity.SettingActivity;
import com.ifeng.newvideo.setting.activity.UserFeedbackActivity;
import com.ifeng.newvideo.statistics.domains.ADInfoRecord;
import com.ifeng.newvideo.statistics.domains.ActionLiveRecord;
import com.ifeng.newvideo.statistics.domains.ActionRecord;
import com.ifeng.newvideo.statistics.domains.ActionSearchRecord;
import com.ifeng.newvideo.statistics.domains.AdVideoRecord;
import com.ifeng.newvideo.statistics.domains.LiveExposeRecord;
import com.ifeng.newvideo.statistics.domains.PageInfoRecord;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.statistics.domains.PageRecord;
import com.ifeng.newvideo.ui.ActivityChannelMore;
import com.ifeng.newvideo.ui.ActivityFmMore;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.ui.ActivityNewVerGuide;
import com.ifeng.newvideo.ui.ActivitySplash;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.newvideo.ui.live.vr.VRBaseActivity;
import com.ifeng.newvideo.ui.mine.activity.MySubscriptionActivity;
import com.ifeng.newvideo.ui.mine.favorites.ActivityFavorites;
import com.ifeng.newvideo.ui.mine.history.ActivityHistory;
import com.ifeng.newvideo.ui.subscribe.AllSubscribeActivity;
import com.ifeng.newvideo.ui.subscribe.AllWeMediaActivity;
import com.ifeng.newvideo.ui.subscribe.WeMediaHomePageActivity;
import com.ifeng.newvideo.videoplayer.activity.ActivityAudioFMPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityCacheVideoPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityTopicPlayer;
import com.ifeng.newvideo.videoplayer.activity.ActivityVideoPlayerDetail;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.dao.db.model.ChannelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 便于统一管理命名规则，Record不分散在各个界面添加
 */
public class PageActionTracker {

    private static final Logger logger = LoggerFactory.getLogger(PageActionTracker.class);
    /**
     * 来源页面保存
     */
    public static String lastPage = "";

    /**
     * 进入页面保存时间
     */
    public static void enterPage() {
        PageRecord.onPageStart();
    }

    /**
     * 页面不可见上报统计，页面停留时间
     */
    public static void endPage(Boolean isLandScape, Context context) {
        if (context == null) {
            return;
        }
        if (context instanceof ActivityCacheVideoPlayer) {
            endPageCachePlay();
        } else if (context instanceof ActivityAudioFMPlayer) {
            endPageFmPlay(isLandScape, lastPage, PageIdConstants.TAG_EMPTY);

        } else if (context instanceof ActivityTopicPlayer) {
            endPageTopicPlay(isLandScape, lastPage, PageIdConstants.TAG_EMPTY);

        } else if (context instanceof ActivityVideoPlayerDetail) {
            endPageVideoPlay(isLandScape, lastPage, PageIdConstants.TAG_EMPTY);

        } else if (context instanceof VRBaseActivity) {
            endPageVrPlay(lastPage, PageIdConstants.TAG_EMPTY);

        } else if (context instanceof SearchActivity) {
            //单独处理 endPageSearch()/ endPageSearchResult(enterTime)/ endPageSearchEmpty(enterTime);
        } else if (context instanceof ActivityMainTab) {
            //单独处理 endPageHomeCh / endPageChMag / endPageLive / endPageSub/ endPageMine
        } else if (context instanceof ADActivity) {
            //单独处理 endPageH5(title);
        } else {
            String page = getPageName(isLandScape, context);
            if (!TextUtils.isEmpty(page)) {
                endPage(page);
            }
        }
        //TODO  SettingInfoActivity
        // 没用上ActivityVideoPlayer BaseVideoPlayerActivity CheckEmailActivity ActivityLive  CommentActivity MobileNetAlert ShareVerticalActivity SignInActivity
        // DLNABasePlayerActivity DLNAPlayerActivity DLNAPlayerActivityForVideo
        // 7.2.0没用TVLiveActivity TVLiveListActivity
        // 基类已处理VRLiveActivity VRVideoActivity
    }


    /**
     * 播放器横竖屏切换上报，并重新计算页面停留时间
     */
    public static void configChangePage(Boolean isLandScape, Context context) {

        if (context instanceof ActivityAudioFMPlayer) {
            endPageFmPlay(!isLandScape, lastPage, PageIdConstants.TAG_EMPTY);
        } else if (context instanceof ActivityTopicPlayer) {
            endPageTopicPlay(!isLandScape, lastPage, PageIdConstants.TAG_EMPTY);
        } else if (context instanceof ActivityVideoPlayerDetail) {
            endPageVideoPlay(!isLandScape, lastPage, PageIdConstants.TAG_EMPTY);
        }
        //ActivityMainTab 大图and直播横屏 单独处理
        //----UniversalChannelFragment.onConfigurationChanged / FragmentLive.onConfigurationChanged

        PageRecord.onPageStart();
    }

    public static void rightSlipFinish(Context context) {
        if (context instanceof ActivityAudioFMPlayer) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK_DRAG, PageIdConstants.PLAY_FM_V);
        } else if (context instanceof ActivityTopicPlayer) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK_DRAG, PageIdConstants.PLAY_TOPIC_V);
        } else if (context instanceof ActivityVideoPlayerDetail) {
            PageActionTracker.clickBtn(ActionIdConstants.CLICK_BACK_DRAG, PageIdConstants.PLAY_VIDEO_V);
        }
    }


    //-----------------------------------------------page页面统计-----------------------------------------------

    public static void endPage(String page) {
        PageRecord.onPageEnd(new PageRecord(page), false);
    }

    //一级页面
    public static void endPageLive() {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PAGE_LIVE), false);
    }

    public static void endPageSub() {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PAGE_SUB), false);
    }

    public static void endPageMine() {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PAGE_MY), false);
    }


    //频道
    public static void endPageHomeCh(String channelId) {
        logger.error("endPageHome 结束计时  上报统计{}", channelId);
        String pageId = String.format(PageIdConstants.HOME_CHANNEL, channelId);
        PageRecord.onPageEnd(new PageRecord(pageId, PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_CH,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY), false);
    }

    //我的-消息-评论
    public static void endPageMessageComment(String refPageId) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.MY_MESSAGE_COMMENT,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_OTHER, refPageId,
                PageIdConstants.TAG_EMPTY), false);
    }

    //我的-消息-回复
    public static void endPageMessageReply() {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.MY_MESSAGE_REPLY,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_OTHER, PageIdConstants.MY_MESSAGE_COMMENT,
                PageIdConstants.TAG_EMPTY), false);
    }

    //频道管理
    public static void endPageChMag(long enterTime) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.HOME_CHANNEL_EDIT, PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_OTHER,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY, DateUtils.getCurrentTime() - enterTime), true);
    }

    //播放器二级页面
    public static void endPageComment(long enterTime) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PLAY_SEND_COMMENT, PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY, DateUtils.getCurrentTime() - enterTime), true);
    }

    /**
     * @param isLandScape Vertical/Horizontal
     * @param fromRef     是否是push/outside打开
     * @param fromTag     焦点图：focus_1{焦点图的第几个},其他都传“”
     */
    public static void endPageVideoPlay(Boolean isLandScape, String fromRef, String fromTag) {
        PageRecord.onPageEnd(new PageRecord(isLandScape ? PageIdConstants.PLAY_VIDEO_H : PageIdConstants.PLAY_VIDEO_V,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                fromRef, fromTag), false);
    }

    public static void endPageTopicPlay(Boolean isLandScape, String fromRef, String fromTag) {
        PageRecord.onPageEnd(new PageRecord(isLandScape ? PageIdConstants.PLAY_TOPIC_H : PageIdConstants.PLAY_TOPIC_V,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                fromRef, fromTag), false);
    }

    public static void endPageTvPlay(Boolean isLandScape, String fromRef, String fromTag) {
        PageRecord.onPageEnd(new PageRecord(isLandScape ? PageIdConstants.PLAY_LIVE_H : PageIdConstants.PLAY_LIVE_V,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                fromRef, fromTag), false);
    }

    public static void endPageVrPlay(String fromRef, String fromTag) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PLAY_VR,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                fromRef, fromTag), false);
    }

    public static void endPageCachePlay() {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PLAY_CACHE,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY), false);
    }

    public static void endPageFmPlay(Boolean isLandScape, String fromRef, String fromTag) {
        PageRecord.onPageEnd(new PageRecord(isLandScape ? PageIdConstants.PLAY_FM_H : PageIdConstants.PLAY_FM_V,
                PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_VIDEO,
                fromRef, fromTag), false);
    }

    //search
    public static void endPageSearch() {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.PAGE_SEARCH), false);
    }

    //searchresult
    public static void endPageSearchResult(long enterTime) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.SEARCH_RESULT, PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_OTHER,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY, DateUtils.getCurrentTime() - enterTime), true);
    }

    //searchresult empty
    public static void endPageSearchEmpty(long enterTime) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.SEARCH_RESULT_EMPTY, PageIdConstants.TITLE_EMPTY, PageIdConstants.TYPE_OTHER,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY, DateUtils.getCurrentTime() - enterTime), true);
    }


    /**
     * H5/广告详情页面
     *
     * @param AdTitle
     */
    public static void endPageH5(String AdTitle) {
        PageRecord.onPageEnd(new PageRecord(PageIdConstants.H5, AdTitle, PageIdConstants.TYPE_OTHER,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY), false);
    }


    /**
     * @param type      页面类别，分为： 1 播放页：video 2 频道列表页：ch 3 其他页：other
     * @param enterTime
     */
    public static void endPageShare(boolean shareOritation, int type, long enterTime) {
        String typeStr;
        switch (type) {
            case 1:
                typeStr = PageIdConstants.TYPE_VIDEO;
                break;
            case 2:
                typeStr = PageIdConstants.TYPE_CH;
                break;
            default:
                typeStr = PageIdConstants.TYPE_OTHER;
                break;
        }
        PageRecord.onPageEnd(new PageRecord(shareOritation ? PageIdConstants.SHARE_V : PageIdConstants.SHARE_H, PageIdConstants.TITLE_EMPTY, typeStr,
                PageIdConstants.REF_ID_EMPTY, PageIdConstants.TAG_EMPTY, DateUtils.getCurrentTime() - enterTime), true);
    }

    /**
     * 曝光
     */
    public static void focusPageItem(List<PageInfoRecord> pageInfoRecords) {
        PageInfoRecord.onFocusPageItem(pageInfoRecords);
    }

    /**
     * H5Live曝光
     */
    public static void focusPageItemLive(List<LiveExposeRecord> liveExposeRecords) {
        LiveExposeRecord.onFocusLivePageItem(liveExposeRecords);
    }

    /**
     * adinfo
     */
    public static void focusADItem(ADInfoRecord adInfoRecord) {
        ADInfoRecord.onFocusADItem(adInfoRecord);
    }

    /**
     * 视频广告
     */
    public static void sendAdVideo(AdVideoRecord adVideoRecord) {
        AdVideoRecord.sendAdVideoStatisticSession(adVideoRecord);
    }

    //focusActionLive
    public static void focusActionLive(ActionLiveRecord actionLiveRecord) {
        ActionLiveRecord.sendActionLiveStatisticSession(actionLiveRecord);
    }

    public static void focusPageLive(PageLiveRecord pageLiveRecord) {
        PageLiveRecord.sendPageLiveStatisticSession(pageLiveRecord);
    }
    //-------------------------------------action点击事件统计------------------------------------

    public static void clickHomeTab(int tabPosition) {
        String clickItem = "";
        switch (tabPosition) {
            case ActivityMainTab.POS_HOME:
                clickItem = ActionIdConstants.CLICK_HOME;
                break;
            case ActivityMainTab.POS_NEWS:
                clickItem = ActionIdConstants.CLICK_NEWS;
                break;
            case ActivityMainTab.POS_LIVE:
                clickItem = ActionIdConstants.CLICK_LIVE;
                break;
            case ActivityMainTab.POS_SUB:
                clickItem = ActionIdConstants.CLICK_SUB;
                break;
            case ActivityMainTab.POS_MY:
                clickItem = ActionIdConstants.CLICK_MINE;
                break;
            default:
                break;
        }

        CustomerStatistics.sendActionRecord(new ActionRecord(clickItem, ActionIdConstants.ACT_EMPTY, PageIdConstants.PAGE_TAB));
    }

    // 搜索内容点击事件
    public static void clickSearchItem(String position, String simId, String rToken) {
        CustomerStatistics.sendActionRecord(new ActionRecord(ActionIdConstants.CLICK_SEARCH_ITEM, ActionIdConstants.ACT_EMPTY, PageIdConstants.PAGE_SEARCH, position, simId, rToken));
    }

    public static void clickSearchItem(String position, String query, String simId, String rToken) {
        CustomerStatistics.sendActionSearchRecord(new ActionSearchRecord(ActionIdConstants.CLICK_SEARCH_ITEM, ActionIdConstants.ACT_EMPTY, PageIdConstants.PAGE_SEARCH, query, position, simId, rToken));
    }

    // 首页内容点击事件
    public static void clickHomeItem(String position, String simId, String rToken) {
        CustomerStatistics.sendActionRecord(new ActionRecord(ActionIdConstants.CLICK_HOME_ITEM, ActionIdConstants.ACT_EMPTY, PageIdConstants.PAGE_HOME, position, simId, rToken));
    }


    //频道的点击和“+”：page为home
    public static void clickCh() {
        CustomerStatistics.sendActionRecord(new ActionRecord(ActionIdConstants.CLICK_HOME_CHANNEL, ActionIdConstants.ACT_EMPTY, PageIdConstants.PAGE_HOME));
    }

    //频道的点击和“+”：page为home
    public static void clickChAdd() {
        CustomerStatistics.sendActionRecord(new ActionRecord(ActionIdConstants.CLICK_HOME_CH_ADD, ActionIdConstants.ACT_EMPTY, PageIdConstants.PAGE_HOME));
    }

    /**
     * 频道页action统计
     * 下拉刷新、加载更多
     */
    public static void pullCh(boolean isPullMore, String channelId) {
        String page = String.format(PageIdConstants.HOME_CHANNEL, channelId);
        String actionId = "";
        if (isPullMore) {
            actionId = ActionIdConstants.PULL_MORE;
        } else {
            actionId = ActionIdConstants.PULL_REFRESH;
        }
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, ActionIdConstants.ACT_EMPTY, page));
    }

    //首页fragment里的轮播图点击
    public static void clickHomeChFocusX(String channelId, int focusNum) {
        String page = String.format(PageIdConstants.HOME_CHANNEL, channelId);
        String actionId = String.format(ActionIdConstants.CLICK_FOCUS, focusNum);
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, ActionIdConstants.ACT_EMPTY, page));
    }


    public static void clickHomeChBtn(String actionId, String channelId) {
        String page = String.format(PageIdConstants.HOME_CHANNEL, channelId);
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, ActionIdConstants.ACT_EMPTY, page));
    }

    /**
     * 我的里的播放列表（看过，缓存，收藏）
     *
     * @param i
     * @param actionId
     */
    public static void clickMyList(int i, String actionId, Boolean actFlag) {
        String page = "";
        switch (i) {
            case 0:
                page = PageIdConstants.MY_CACHED;
                break;
            case 1:
                page = PageIdConstants.MY_CACHING;
                break;
            case 2:
                page = PageIdConstants.MY_COLLECTED;
                break;
            case 3:
                page = PageIdConstants.MY_PLAYED;
                break;
        }
        String act = ActionIdConstants.ACT_EMPTY;
        if (actFlag != null) {
            act = actFlag ? ActionIdConstants.ACT_YES : ActionIdConstants.ACT_NO;
        }
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, act, page));
    }

    /**
     * 所有简单点击事件
     *
     * @param actionId
     * @param page
     */
    public static void clickBtn(String actionId, String page) {
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, ActionIdConstants.ACT_EMPTY, page));
    }

    /**
     * 所有带开关的点击事件:yes/no
     *
     * @param actionId
     * @param actFlag
     * @param page
     */
    public static void clickBtn(String actionId, boolean actFlag, String page) {
        String act = actFlag ? ActionIdConstants.ACT_YES : ActionIdConstants.ACT_NO;
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, act, page));
    }

    /**
     * 所有带开关的点击事件:yes/no/cancel
     *
     * @param actionId
     * @param actFlag
     * @param page
     */
    public static void clickBtn(String actionId, String actFlag, String page) {
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, actFlag, page));
    }


    /**
     * 所有页面的订阅按钮点击
     */
    public static void clickWeMeidaSub(Boolean subFlag, String page) {
        String act = ActionIdConstants.ACT_EMPTY;
        if (subFlag != null) {
            act = subFlag ? ActionIdConstants.ACT_YES : ActionIdConstants.ACT_NO;
        }
        CustomerStatistics.sendActionRecord(new ActionRecord(ActionIdConstants.CLICK_WEMEDIA_SUB, act, page));
    }

    /**
     * 播放器里的点击事件
     *
     * @param actionId
     * @param actFlag
     */
    public static void clickPlayerBtn(String actionId, Boolean actFlag, String page) {
        String act = ActionIdConstants.ACT_EMPTY;
        if (actFlag != null) {
            act = actFlag ? ActionIdConstants.ACT_YES : ActionIdConstants.ACT_NO;
        }
        CustomerStatistics.sendActionRecord(new ActionRecord(actionId, act, page));
    }

    /**
     * 所以也没截屏事件
     */
    public static void clickSnapshot(boolean isLand, Context context) {
        clickBtn(ActionIdConstants.SNAPSHOT, getPageName(isLand, context));
    }

    public static String getPageName(boolean isLandScape, Context context) {
        String page = "";
        if (context instanceof ActivityMainTab) {
            if (ActivityMainTab.isChannelManageFragmentShow()) {
                page = PageIdConstants.HOME_CHANNEL_EDIT;
                return page;
            }
            if (ActivityMainTab.TAB_HOME.equals(ActivityMainTab.currentFragmentTag)) {
                if (isLandScape) {
                    page = PageIdConstants.PLAY_VIDEO_H;
                } else {
                    page = PageIdConstants.PAGE_HOME;
                }
            }
            if (ActivityMainTab.TAB_LIVE.equals(ActivityMainTab.currentFragmentTag)) {
                if (isLandScape) {
                    page = PageIdConstants.PLAY_LIVE_H;
                } else {
                    page = PageIdConstants.PLAY_LIVE_V;
                }
            }
            if (ActivityMainTab.TAB_SUB.equals(ActivityMainTab.currentFragmentTag)) {
                page = PageIdConstants.PAGE_SUB;
            }
            if (ActivityMainTab.TAB_MY.equals(ActivityMainTab.currentFragmentTag)) {
                page = PageIdConstants.PAGE_MY;
            }
        } else if (context instanceof ActivitySplash) {
            page = PageIdConstants.PAGE_SPLASH;
        } else if (context instanceof ActivityChannelMore) {
            page = PageIdConstants.HOME_CHANNEL_MORE;
        } else if (context instanceof ActivityFmMore) {
            page = PageIdConstants.FM_MORE;
        } else if (context instanceof ActivityCacheVideoPlayer) {
            page = PageIdConstants.PLAY_CACHE;
        } else if (context instanceof ActivityAudioFMPlayer) {
            if (isLandScape) {
                page = PageIdConstants.PLAY_FM_H;
            } else {
                page = PageIdConstants.PLAY_FM_V;
            }
        } else if (context instanceof ActivityTopicPlayer) {
            if (isLandScape) {
                page = PageIdConstants.PLAY_TOPIC_H;
            } else {
                page = PageIdConstants.PLAY_TOPIC_V;
            }
        } else if (context instanceof ActivityVideoPlayerDetail) {
            if (isLandScape) {
                page = PageIdConstants.PLAY_VIDEO_H;
            } else {
                page = PageIdConstants.PLAY_VIDEO_V;
            }
        } else if (context instanceof VRBaseActivity) {
            page = PageIdConstants.PLAY_VR;
        } else if (context instanceof AllSubscribeActivity) {
            page = PageIdConstants.SUB_ALL;
        } else if (context instanceof AllWeMediaActivity) {
            page = PageIdConstants.WEMEDIA_ALL;
        } else if (context instanceof WeMediaHomePageActivity) {
            page = PageIdConstants.WEMEDIA_HOME;
        } else if (context instanceof MySubscriptionActivity) {
            page = PageIdConstants.MY_SUB;
        } else if (context instanceof ActivityFavorites) {
            page = PageIdConstants.MY_COLLECTED;
        } else if (context instanceof ActivityHistory) {
            page = PageIdConstants.MY_PLAYED;
        } else if (context instanceof CacheAllActivity) {
            page = PageIdConstants.MY_CACHED;
        } else if (context instanceof CachePathActivity) {
            page = PageIdConstants.MY_SETUP_CACHE_PATH;
        } else if (context instanceof CachingActivity) {
            page = PageIdConstants.MY_CACHING;
        } else if (context instanceof SettingActivity) {
            page = PageIdConstants.MY_SETUP;
        } else if (context instanceof AboutActivity) {
            page = PageIdConstants.MY_SETUP_ABOUT;
        } else if (context instanceof ActivityNewVerGuide) {
            page = PageIdConstants.MY_SETUP_ABOUT_WELCOME;
        } else if (context instanceof UserFeedbackActivity) {
            page = PageIdConstants.MY_FEEDBACK;
        } else if (context instanceof LoginMainActivity) {
            page = PageIdConstants.MY_LOGIN;
        } else if (context instanceof BindingPhoneActivity) {
            page = PageIdConstants.MY_LOGIN_BINDING;
        } else if (context instanceof CheckPhoneNumberActivity) {
            page = PageIdConstants.MY_LOGIN_REG_VERIFY;
        } else if (context instanceof RegisterActivity) {
            page = PageIdConstants.MY_LOGIN_REG;
        } else if (context instanceof ForgetPwdActivity) {
            page = PageIdConstants.MY_LOGIN_FORGET;
        } else if (context instanceof SearchActivity) {
            page = PageIdConstants.PAGE_SEARCH;
        } else if (context instanceof SearchWeMediaActivity) {
            page = PageIdConstants.SEARCH_WEMEDIA;
        } else if (context instanceof IfengNewsActivity) {
            page = PageIdConstants.IFENG_NEWS;
        } else if (context instanceof EditPage) {
            page = PageIdConstants.SINA_SHARE;
        } else if (context instanceof ADActivity) {
            page = PageIdConstants.H5;
        } else if (context instanceof OpenMemberActivity) {
            page = PageIdConstants.VIP_USER_PAY_FOR;
        } else if (context instanceof MemberCenterActivity) {
            page = PageIdConstants.VIP_USER_CENTER;
        } else if (context instanceof DealRecordActivity) {
            page = PageIdConstants.VIP_USER_DEAL_RECORD;
        } else if (context instanceof PointTaskActivity) {
            page = PageIdConstants.MY_POINT_TASK;
        }
        return page;
    }

}

