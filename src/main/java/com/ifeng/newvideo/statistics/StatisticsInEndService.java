package com.ifeng.newvideo.statistics;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.ifeng.newvideo.IfengApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 发送in和end统计的后台服务
 * Created by hu on 2014/10/18.
 */
public class StatisticsInEndService extends Service {

    private static final Logger logger = LoggerFactory.getLogger(StatisticsInEndService.class);
    private IfengApplication app;
    private boolean isBind = true;
    /**
     * 上一次检测时应用是否处于栈顶
     */
    private boolean isPreAppTop = false;

    @Override
    public void onCreate() {
        super.onCreate();
        isPreAppTop = true;
        app = IfengApplication.getInstance();
        CustomerStatistics.enterApp();
        startListenerThread();
    }

    private void startListenerThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isBind) {
                    //应用是否处于栈顶
                    boolean flag = app.isTopApplication();
                    app.isForeground = flag;
                    if (flag != isPreAppTop) {
                        if (flag) {
                            CustomerStatistics.enterApp();
                            app.enterApp();
                        } else {
                            CustomerStatistics.exitApp();
                            app.exitApp();
                        }
                    }

                    isPreAppTop = flag;
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            }
        }).start();
    }


    @Override
    public IBinder onBind(Intent intent) {
        logger.debug("service is bind");
        isBind = true;
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        logger.debug("service is unbind");
        isBind = false;
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomerStatistics.exitApp();
    }
}
