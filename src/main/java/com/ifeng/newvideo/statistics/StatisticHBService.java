package com.ifeng.newvideo.statistics;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 发送联网日志的服务
 */
public class StatisticHBService extends Service {
    private static final Logger logger = LoggerFactory.getLogger(StatisticHBService.class);
    private Timer timer;
    public static final String ACTION = "com.ifeng.video.HB";
    private static final long TIME_INTERVAL = 2 * 60 * 60 * 1000;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        logger.info("start HB service");
        sendRecordHB();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 发送联网日志
     * 每隔两小时发送一次
     */
    private void sendRecordHB() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new HBTimerTask(), 100, TIME_INTERVAL);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    private static class HBTimerTask extends TimerTask {

        @Override
        public void run() {
            CustomerStatistics.sendHBRecord();
        }
    }
}
