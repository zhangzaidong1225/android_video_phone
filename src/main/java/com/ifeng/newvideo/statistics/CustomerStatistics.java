package com.ifeng.newvideo.statistics;


import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.domains.ADInfoRecord;
import com.ifeng.newvideo.statistics.domains.ActionLiveRecord;
import com.ifeng.newvideo.statistics.domains.ActionRecord;
import com.ifeng.newvideo.statistics.domains.ActionSearchRecord;
import com.ifeng.newvideo.statistics.domains.AdVideoRecord;
import com.ifeng.newvideo.statistics.domains.CollectRecord;
import com.ifeng.newvideo.statistics.domains.ColumnRecord;
import com.ifeng.newvideo.statistics.domains.ContinueSplayRecord;
import com.ifeng.newvideo.statistics.domains.DownloadRecord;
import com.ifeng.newvideo.statistics.domains.ExitAppRecord;
import com.ifeng.newvideo.statistics.domains.LiveExposeRecord;
import com.ifeng.newvideo.statistics.domains.LiveRecord;
import com.ifeng.newvideo.statistics.domains.LoginRecord;
import com.ifeng.newvideo.statistics.domains.NetWorkRecord;
import com.ifeng.newvideo.statistics.domains.NewsExitAppRecord;
import com.ifeng.newvideo.statistics.domains.NewsSDKInRecord;
import com.ifeng.newvideo.statistics.domains.OpenPushRecord;
import com.ifeng.newvideo.statistics.domains.PageInfoRecord;
import com.ifeng.newvideo.statistics.domains.PageLiveRecord;
import com.ifeng.newvideo.statistics.domains.PageRecord;
import com.ifeng.newvideo.statistics.domains.PushAccessRecord;
import com.ifeng.newvideo.statistics.domains.RegisterRecord;
import com.ifeng.newvideo.statistics.domains.ShareRecord;
import com.ifeng.newvideo.statistics.domains.SignInRecord;
import com.ifeng.newvideo.statistics.domains.StartAppRecord;
import com.ifeng.newvideo.statistics.domains.VErrRecord;
import com.ifeng.newvideo.statistics.domains.VideoRecord;
import com.ifeng.newvideo.statistics.domains.VodRecord;
import com.ifeng.newvideo.ui.ActivityMainTab;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.dao.db.constants.ChannelId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 自有统计发送日志类
 */
public class CustomerStatistics {
    private static final Logger logger = LoggerFactory.getLogger(CustomerStatistics.class);
    /**
     * 播放器播放的总时长
     */
    private static final String PLAY_DUR_KEY = "appPlayDur";
    /**
     * 进入app应用时的时间
     */
    private static final String ENTER_APP_TIME = "enterAppTime";
    /**
     * 退出app应用的时间
     */
    private static final String EXIT_APP_TIME = "exitAppTime";

    /**
     * 应用是否处于可见状态
     */
    public static boolean isAppVisible = false;

    /**
     * 发送in统计的打开方式
     */
    public static String inType = StatisticsConstants.APPSTART_TYPE_FROM_DIRECT;
    /**
     * 由第三方打开应用时的来源id
     */
    public static String openId = null;

    /**
     * 发送V统计次数
     */
    private static int sendVRecordCount = 0;
    /**
     * in end间隔 30s
     */
    private static final int IN_END_INTERVAL = 30;

    /**
     * 发送点播统计
     *
     * @param vRecord
     */
    public static void sendVODRecord(VodRecord vRecord) {

        if (VideoRecord.videoRecords != null) {
            logger.debug("when start send v, videoRecords size:{}", VideoRecord.videoRecords.size());
        }

        sendVRecordCount++;
        IfengApplication app = IfengApplication.getInstance();
        app.setAttribute(PLAY_DUR_KEY, ExitAppRecord.getAllPlayTime(app.getAttribute(PLAY_DUR_KEY), vRecord.getTotalPlayTime()));
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(vRecord, false);

        // 发送播放视频err统计
        String errId = StatisticsConstants.STATISTICS_ERR_SAMPLE
                + "-" + StatisticsConstants.STATISTICS_ERR_TOUCH_PLAY_BUTTON
                + (vRecord.isSuccessPlayFirstFrame() ? "-" + StatisticsConstants.STATISTICS_ERR_TOUCH_START_PLAY : "")
                + vRecord.getBNString()
                + (vRecord.getIsSeekErr() ? "-" + StatisticsConstants.STATISTICS_ERR_SEEK_STREAM_NOT_FOUND : "")
                + (vRecord.getIsErr() ? "-" + StatisticsConstants.STATISTICS_ERR_STREAM_NOT_FOUND : "")
                + vRecord.getDurInconsistency();

        if (VideoRecord.videoRecords != null && VideoRecord.videoRecords.size() > 0) {
            sendVideoErrRecord(new VErrRecord(errId, vRecord.getId(), ChannelId.isLongVideo(vRecord.getEchid()) ? "long"
                    : "short", vRecord.getPlayer()));
        }


        vRecord.reset();

        if (vRecord != null && vRecord instanceof VideoRecord) {
            synchronized (VideoRecord.recordsLock) {
                VideoRecord.videoRecords.remove(vRecord);
            }
        }

        if (VideoRecord.videoRecords != null) {
            logger.debug("when end send v, videoRecords size:{}", VideoRecord.videoRecords.size());
        }

    }

    /**
     * 发送直播统计
     *
     * @param liveRecord
     */
    public static void sendLiveRecord(LiveRecord liveRecord) {
        logger.debug("sendLiveRecord ----start---title========{}", liveRecord.getLiveTitle());
        IfengApplication app = IfengApplication.getInstance();
        app.setAttribute(PLAY_DUR_KEY, ExitAppRecord.getAllPlayTime(app.getAttribute(PLAY_DUR_KEY), liveRecord.getTotalPlayTime()));
        //发送直播统计
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(liveRecord, false);

        // 发送播放视频err统计
        String errId = StatisticsConstants.STATISTICS_ERR_SAMPLE
                + "-" + StatisticsConstants.STATISTICS_ERR_TOUCH_PLAY_BUTTON
                + (liveRecord.isSuccessPlayFirstFrame() ? "-" + StatisticsConstants.STATISTICS_ERR_TOUCH_START_PLAY : "")
                + liveRecord.getBNString()
                + (liveRecord.getIsErr() ? "-" + StatisticsConstants.STATISTICS_ERR_STREAM_NOT_FOUND : "");
        if (VideoRecord.videoRecords != null && VideoRecord.videoRecords.size() > 0) {
            sendVRecordCount++;
            sendVideoErrRecord(new VErrRecord(errId, liveRecord.getId(), "live", liveRecord.getPlayer()));
        }

        liveRecord.reset();

        if (liveRecord != null && liveRecord instanceof VideoRecord) {
            synchronized (VideoRecord.recordsLock) {
                VideoRecord.videoRecords.remove(liveRecord);
            }
        }

    }

    /**
     * 发送播放错误统计
     *
     * @param vErrRecord
     */
    private static void sendVideoErrRecord(VErrRecord vErrRecord) {
        // 发送播放err统计的采样次数
        int errStatisticRatio = SharePreUtils.getInstance().getErrStatisticRatio();
        // 默认-1，不发送
        if (errStatisticRatio == -1) {
            return;
        }
        //目前不做err统计，先注释掉
        if (sendVRecordCount >= errStatisticRatio) {// 播放10次视频发送一次vErr统计
            sendVRecordCount = 0;
            new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(vErrRecord, true);
        }
    }

    /**
     * 发送打开推送的统计
     * 发送时机：打开推送时发送
     *
     * @param openPushRecord
     */
    public static void sendOpenPushReceive(OpenPushRecord openPushRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(openPushRecord, false);
    }

    /**
     * 发送推送到达后的统计
     * 发送时机：推送到达时发送
     *
     * @param pushAccessRecord
     */
    public static void sendPushAccessReceive(PushAccessRecord pushAccessRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(pushAccessRecord, false);
    }

    /**
     * 发送下载统计
     * 发送时机：下载开始时发送，下载完成时发送，下载取消时发送
     *
     * @param downloadRecord
     */
    public static void sendDownload(DownloadRecord downloadRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(downloadRecord, false);
    }

    /**
     * 发送栏目订阅和退订的统计
     * 发送时机：订阅栏目和退订栏目时发送
     *
     * @param columnRecord
     */
    public static void sendColumnSubScribe(ColumnRecord columnRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(columnRecord, false);
    }

    /**
     * 发送首页频道订阅和退订的统计
     * 发送时机：当首页频道订阅情况发生变化时发送
     *
     * @param columnRecords
     */
    public static void sendChannelSubScribe(List<ColumnRecord> columnRecords) {

        if (columnRecords != null && columnRecords.size() > 0) {

            for (ColumnRecord columnRecord : columnRecords) {
                new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(columnRecord, false);
            }
        }
    }

    /**
     * 发送自媒体订阅和退订的统计
     * 发送时机：订阅或退订自媒体时发送
     *
     * @param columnRecord 订阅统计实体类
     */
    public static void sendWeMediaSubScribe(ColumnRecord columnRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(columnRecord, false);
    }

    /**
     * 发送联网日志的统计
     */
    public static void sendHBRecord() {
        NetWorkRecord netWorkRecord = new NetWorkRecord();
        new StatisticSession(IfengApplication.getInstance()).sendStatisticSession(netWorkRecord, false);
    }


    /**
     * 应用开启时发送统计
     */
    private static void sendStartApp() {
        logger.debug("send in statistics");
        IfengApplication app = IfengApplication.getInstance();
        StartAppRecord startAppRecord = null;

        boolean isLogin = new User(app.getApplicationContext()).isLogin();

        if (openId != null) {
            startAppRecord = new StartAppRecord(inType, openId, isLogin);
        } else {
            startAppRecord = new StartAppRecord(inType, isLogin);
        }

        new StatisticSession(app).sendStatisticSession(startAppRecord, false);
        //上报新闻客户端统计
        if (SharePreUtils.getInstance().isNewsStatisticsSend()) {
            new StatisticSession(app).sendNewsStatisticSession(startAppRecord);
        }

        //重置状态
        inType = StatisticsConstants.APPSTART_TYPE_FROM_DIRECT;
        openId = null;
    }


    /**
     * 应用退出时发送的统计
     */
    private static void sendQuitApp() {
        IfengApplication app = IfengApplication.getInstance();

        long appDur;
        //获取播放总时长
        long playDur = app.getAttribute(PLAY_DUR_KEY) != null ? Long.parseLong(app.getAttribute(
                PLAY_DUR_KEY).toString()) : 0;

        //获取打开app应用总时间
        if (app.getAttribute(ENTER_APP_TIME) == null) {
            return;
        } else {
            appDur = System.currentTimeMillis() / 1000
                    - Long.parseLong(app.getAttribute(ENTER_APP_TIME).toString());
        }

        //发送广告统计
        new StatisticSession(app).sendAdStatisticSession();

        //play audio not send end
        if (ActivityMainTab.mAudioService != null) {
            return;
        }
        ExitAppRecord exitAppRecord = new ExitAppRecord(appDur, playDur);
        logger.debug("send end statistics");
        new StatisticSession(app).sendStatisticSession(exitAppRecord, false);

        //上报新闻客户端统计
        if (SharePreUtils.getInstance().isNewsStatisticsSend()) {
            new StatisticSession(app).sendNewsStatisticSession(new NewsExitAppRecord(appDur));
        }
        //退出应用时播放时长清零
        app.setAttribute(PLAY_DUR_KEY, 0);

        app.removeAttribute(ENTER_APP_TIME);
    }

    public static void enterApp() {
        IfengApplication app = IfengApplication.getInstance();
        if (app.getAttribute(CustomerStatistics.EXIT_APP_TIME) == null) {
            sendStartApp();
        } else {
            long time = (System.currentTimeMillis() / 1000) - Long.parseLong(app.getAttribute(EXIT_APP_TIME).toString());
            //本次打开应用距离上次退出应用的时间超过30秒才发送in统计
            if (time > IN_END_INTERVAL) {
                sendStartApp();
                app.setAttribute(EXIT_APP_TIME, System.currentTimeMillis() / 1000);
            }
        }


        //记录进入应用时的时间
        if (app.getAttribute(CustomerStatistics.ENTER_APP_TIME) == null) {
            logger.debug("set start app time!!!");
            app.setAttribute(CustomerStatistics.ENTER_APP_TIME, System.currentTimeMillis() / 1000);
        }

        //重置状态
        inType = StatisticsConstants.APPSTART_TYPE_FROM_DIRECT;
    }

    public static void exitApp() {
        sendQuitApp();
        //记录退出应用时的时间
        IfengApplication.getInstance().setAttribute(EXIT_APP_TIME, System.currentTimeMillis() / 1000);
    }

    /**
     * 按钮点击事件统计
     * 发送时机：点击按钮时发送
     *
     * @param actionRecord
     */
    public static void clickBtnRecord(ActionRecord actionRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(actionRecord, false);
    }

    /**
     * 联通已订购超限播放dialog按键统计
     *
     * @param isPlay
     */
    public static void clickBtnRecordBeyondFlowPlayer(boolean isPlay) {
        clickBtnRecord(new ActionRecord("player_beyond", isPlay ? "yes" : "no", ""));
    }

    /**
     * 联通已订购超限下载dialog按键统计
     *
     * @param isPlay
     */
    public static void clickBtnRecordForDload(boolean isPlay) {
        clickBtnRecord(new ActionRecord("dload_beyond", isPlay ? "yes" : "no", ""));
    }

    /**
     * 发送用户注册统计
     * 发送时机：用户注册时发送
     *
     * @param registerRecord
     */
    public static void sendRegisterRecord(RegisterRecord registerRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(registerRecord, false);
    }

    /**
     * 发送用户登录统计
     * 发送时机：用户登录时发送
     *
     * @param loginRecord
     */
    public static void sendLoginRecord(LoginRecord loginRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(loginRecord, false);
    }

    /**
     * 发送分享统计
     * 发送时机：分享时发送
     *
     * @param shareRecord
     */
    public static void sendShareRecord(ShareRecord shareRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(shareRecord, false);
    }

    /**
     * 发送收藏统计
     * 发送时机：收藏/取消收藏
     *
     * @param collectRecord
     */
    public static void sendCollectRecord(CollectRecord collectRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(collectRecord, false);
    }

    /**
     * 发送签到统计
     * 发送时机：点击签到时发送
     *
     * @param signInRecord
     */
    public static void sendSignInRecord(SignInRecord signInRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(signInRecord, false);
    }

    public static void setStaticsIdAndTypeFromOutside(String name) {
        if (name.equals("WechatMoments")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_WEIXIN;
        } else if (name.equals("Wechat")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_WEIXIN;
        } else if (name.equals("QQ")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_QQ;
        } else if (name.equals("SinaWeibo")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_SINAWEIBO;
        } else if (name.equals("QZone")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_QZONE;
        } else if (name.equals("Alipay")) {
            CustomerStatistics.openId = StatisticsConstants.APPSTART_TYPE_FROM_ALIPAY;
        }
        CustomerStatistics.inType = StatisticsConstants.APPSTART_TYPE_FROM_OUTSIDE;
    }

    /**
     * 发送行为统计
     */
    public static void sendActionRecord(ActionRecord actionRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(actionRecord, false);
    }

    public static void sendActionSearchRecord(ActionSearchRecord actionRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(actionRecord, false);
    }
    /**
     * 发送页面统计
     */
    public static void sendPageRecord(PageRecord pageRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(pageRecord, false);
    }

    /**
     * 发送曝光统计
     */
    public static void sendFocusItem(List<PageInfoRecord> pageInfoRecords) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSessiongList(pageInfoRecords, false);
    }

    /**
     * H5曝光
     */
    public static void sendLiveFocusItem(List<LiveExposeRecord> liveExposeRecords) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSessiongLiveList(liveExposeRecords, false);
    }

    /**
     * 发送广告统计
     */
    public static void sendFocusADItem(ADInfoRecord adInfoRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(adInfoRecord, false);
    }

    public static void sendAdVideo(AdVideoRecord adVideoRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendAdVideoStatistic(adVideoRecord);
    }

    public static void sendActionLive(ActionLiveRecord actionLiveRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(actionLiveRecord, false);
    }

    public static void sendPageLive(PageLiveRecord pageLiveRecord) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(pageLiveRecord, false);
    }

    /**
     * 有连播行为时上报
     *
     * @param record
     */
    public static void sendContinueSplazyRecord(ContinueSplayRecord record) {
        new StatisticSession(IfengApplication.getAppContext()).sendStatisticSession(record, false);
    }

    /**
     * 凤凰新闻SDk in统计上报
     */
    public static void sendNewsSDKInRecode(NewsSDKInRecord record) {
        new StatisticSession(IfengApplication.getAppContext()).sendNewsSDKStatisticSession(record);
    }
}
