package com.ifeng.newvideo.statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 各个页面id,为统计定义
 * Created by hu on 2014/10/14.
 */
public class PageIdConstants {

    private static final Logger logger = LoggerFactory.getLogger(PageIdConstants.class);

    /**
     * 页面类别，分为：
     * 1 播放页：video
     * 2 频道列表页：ch
     * 3 直播页 live
     * 4 其他页：other
     */
    public static final String TYPE_CH = "ch";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_LIVE = "live";
    public static final String TYPE_OTHER = "other";
    /**
     * 来源页面id
     * 若为推送进入，来源为push
     * 若为第三方引入，来源为outside
     */
    public static final String REF_ID_EMPTY = "";
    public static final String REF_PUSH = "push";
    public static final String REF_OUTSIDE = "outside";
    /**
     * 标记，需要确定来自来源页面具体位置时发该字段
     * 焦点图：focus_1{焦点图的第几个}
     */
    public static final String TAG_EMPTY = "";

    public static final String TITLE_EMPTY = "";

    public static final String RTOKEN_EMPTY = "";
    public static final String REFTYPE_AI = "ai";
    public static final String REFTYPE_EDITOR = "editor";
    public static final String PINFO_SPACE = ":";


    /**
     * 7.2.0 添加page统计--------------------------------------------------
     */

    /**
     * 一级页面
     */
    public static final String PAGE_SPLASH = "launchAD";
    public static final String PAGE_TAB = "tab";
    public static final String PAGE_HOME = "home";
    public static final String PAGE_LIVE = "live";
    public static final String PAGE_SUB = "sub";
    public static final String PAGE_MY = "my";
    public static final String PAGE_SEARCH = "search";

    /**
     * 首页子频道页 (xx为频道ID)
     */
    public static final String HOME_CHANNEL = "home_ch_%s";
    public static final String HOME_CHANNEL_EDIT = "home_ch_edit";
    public static final String HOME_CHANNEL_MORE = "home_ch_more";

    public static final String SUB_ALL = "sub_all";
    public static final String WEMEDIA_ALL = "wemedia_all";
    public static final String WEMEDIA_HOME = "wemedia_home";
    public static final String LIVE_ALL = "live_all";

    public static final String MY_PLAYED = "my_played";
    public static final String MY_COLLECTED = "my_collected";
    public static final String MY_CACHED = "my_cached";
    public static final String MY_CACHING = "my_caching";
    public static final String MY_SUB = "my_sub";
    public static final String MY_SETUP = "my_setup";
    public static final String MY_SETUP_ABOUT = "my_setup_about";
    public static final String MY_SETUP_ABOUT_WELCOME = "my_setup_about_welcome";
    public static final String MY_SETUP_CACHE_PATH = "my_setup_cache_path";
    public static final String MY_LOGIN = "my_login";
    public static final String MY_LOGIN_FORGET = "my_login_forget";
    public static final String MY_LOGIN_REG = "my_login_reg";
    public static final String MY_LOGIN_REG_VERIFY = "my_login_reg_verify";
    public static final String MY_LOGIN_BINDING = "my_login_binding";
    public static final String MY_FEEDBACK = "my_feedback";

    public static final String PLAY_VIDEO_V = "play_video_v";
    public static final String PLAY_VIDEO_H = "play_video_h";
    public static final String PLAY_SEND_COMMENT = "play_send_comment";
    public static final String PLAY_TOPIC_V = "play_topic_v";
    public static final String PLAY_TOPIC_H = "play_topic_h";
    public static final String PLAY_LIVE_V = PAGE_LIVE;
    public static final String PLAY_LIVE_H = "play_live_h";
    public static final String PLAY_CACHE = "play_cache";
    public static final String PLAY_VR = "play_vr";
    public static final String PLAY_FM_V = "play_fm_v";
    public static final String PLAY_FM_H = "play_fm_h";

    public static final String SEARCH_RESULT = "search_result";
    public static final String SEARCH_WEMEDIA = "wemedia_related";
    public static final String SEARCH_RESULT_EMPTY = "search_result_empty";
    public static final String IFENG_NEWS = "ifeng_news";
    public static final String H5 = "h5";
    public static final String SHARE_H = "share_h";
    public static final String SHARE_V = "share_v";
    public static final String SINA_SHARE = "sina_share";
    public static final String FM_MORE = "fm_more";

    public static final String MY_MESSAGE_REPLY = "my_new_message"; // 消息->回复页面
    public static final String MY_MESSAGE_COMMENT = "my_comment_message"; // 消息->评论页面

    public static final String VIP_USER_CENTER = "my_vip";
    public static final String VIP_USER_DEAL_RECORD = "my_vip_trade_record";
    public static final String VIP_USER_PAY_FOR = "my_vip_buy";

    public static final String MY_POINT_TASK = "my_point_detail";


}
