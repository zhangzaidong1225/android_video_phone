package com.ifeng.newvideo.statistics;

import android.text.TextUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.model.ConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 启动后获取该配置信息，
 * 调用SDK API，传递信息，告知底层：
 * 1、该设备是否被选中，
 * 2、整体设备采样率大小（由devCode数量/10得出），
 * 3、以及单台设备采样率控制码(等于funCode值)，
 * 具体单台设备采样率由底层根据控制码设定。
 */
public class P2PStatisticRatioUtils {

    private static final Logger logger = LoggerFactory.getLogger(P2PStatisticRatioUtils.class);
    public static final int DEFAULT_P2P_STATISTICS_RATIO = 0;

    /**
     * 设置P2P统计信息上报配置
     *
     * @param fun 配置接口model
     */
    public static void setP2PConfig(ConfigModel.Fun fun) {
        if (fun == null || TextUtils.isEmpty(fun.getFunCode())) {
            setP2P(false, DEFAULT_P2P_STATISTICS_RATIO + "", DEFAULT_P2P_STATISTICS_RATIO);
            return;
        }
        try {
            String channelId = fun.getChannelId();
            String ver = fun.getVer();
            String devCode = fun.getDevCode();

            boolean isChannelIdOK = TextUtils.isEmpty(channelId) || channelId.contains(PhoneConfig.publishid);
            boolean isVersionOK = TextUtils.isEmpty(ver) || ver.contains(PhoneConfig.softversion);
            boolean isDevNumOK = TextUtils.isEmpty(devCode) || devCode.contains(StringUtils.getLastOneString(PhoneConfig.getLoginTimeForStatistic()));

            boolean isOpen = isChannelIdOK && isVersionOK && isDevNumOK;
            setP2P(isOpen, (count(devCode) / 10) + "", Integer.valueOf(fun.getFunCode()));

        } catch (NumberFormatException e) {
            setP2P(false, DEFAULT_P2P_STATISTICS_RATIO + "", DEFAULT_P2P_STATISTICS_RATIO);
        }
    }

    private static void setP2P(boolean isOpen, String totalRatio, int singleRatio) {
        SharePreUtils sharePreUtils = SharePreUtils.getInstance();
        sharePreUtils.setP2PStaRatioOnOff(isOpen);
        sharePreUtils.setP2PTotalStaRatio(totalRatio);
        sharePreUtils.setP2PSingleStaRatio(singleRatio);
        logger.debug("requestConfig P2PStatistic isOpen = {}, totalRatio  = {}, singleRatio = {}", isOpen, totalRatio, singleRatio);
    }

    /**
     * 用#分割后的个数
     *
     * @param devCode 如1#2#3
     * @return 3个
     */
    private static double count(String devCode) {
        if (TextUtils.isEmpty(devCode)) {
            return 0;
        }
        return devCode.split("#").length;
    }
}
