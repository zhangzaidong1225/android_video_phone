package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.Record;

/**
 * 收藏与取消收藏统计
 *
 * @author zhangss
 */
public class CollectRecord extends Record {

    /**
     * 编辑id
     */
    private final String echid;

    /**
     * 媒资id
     */
    private final String chid;

    /**
     * 自媒体id
     */
    private final String wmid;

    /**
     * 收藏成功：yes
     * 取消收藏/收藏失败：no
     */
    private final boolean yn;
    /**
     * 所在页面id
     */
    private final String page;

    /**
     * @param id    视频的id
     * @param echid 编辑id
     * @param chid  媒资id
     * @param wmid  自媒体id
     * @param yn    收藏成功/失败:yes/no
     * @param page  所在页面id
     */
    public CollectRecord(String id, String echid, String chid, String wmid, boolean yn, String page) {
        this.id = id;
        this.echid = echid;
        this.chid = chid;
        this.wmid = wmid;
        this.yn = yn;
        this.page = page;
    }

    public String getEchid() {
        return echid != null ? echid : "";
    }

    public String getChid() {
        return chid != null ? chid : "";
    }

    public String getWmid() {
        return wmid != null ? wmid : "";
    }

    public boolean isYn() {
        return yn;
    }

    public String getPage() {
        return page != null ? page : "";
    }

    @Override
    public String getRecordType() {
        return RECORD_TYPE_STORE;
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("id=").append(getId()).append(PARAMS_SEPARATOR)
                .append("echid=").append(getEchid()).append(PARAMS_SEPARATOR)
                .append("chid=").append(getChid()).append(PARAMS_SEPARATOR)
                .append("wmid=").append(getWmid()).append(PARAMS_SEPARATOR)
                .append("yn=").append(yn ? "yes" : "no").append(PARAMS_SEPARATOR)
                .append("page=").append(getPage())
                .toString().replaceAll("null", "");
    }
}
