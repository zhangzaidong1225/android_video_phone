package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.Record;

/**
 * Created by fanshell on 2016/12/1.
 */
public class ContinueSplayRecord extends Record {


    /**
     * 横屏播放器
     */
    public static final String TYPE_H_PLAYER = "hPlayer";
    /**
     * 竖屏播放器
     */
    public static final String TYPE_V_PLAYER = "vPlayer";
    /**
     * 大图播放器
     */
    public static final String TYPE_PIC_PLAYER = "picPlayer";
    /**
     * 画中画播放器
     */
    public static final String TYPE_PIP_PLAYER = "pPlayer";
    /**
     * 横屏播放器:hPlayer
     * 竖屏播放器：vPlayer
     * 大图播放器：picPlayer
     */
    public String type;
    /**
     * 次数，2开始 2-N
     */
    public int number;

    /**
     * 编辑ID
     */
    public String echid;
    /**
     * 媒资ID
     */
    public String chid;
    /**
     * 持续时间 秒为单位
     */
    public String duration;

    public ContinueSplayRecord() {
        type = "";
        number = 0;
        echid = "";
        chid = "";
    }


    @Override
    public String getRecordType() {
        return "continuesplay";
    }

    @Override
    protected String getRecordContent() {
        return new StringBuilder()
                .append("type=").append(type).append(PARAMS_SEPARATOR)
                .append("num=").append(number).append(PARAMS_SEPARATOR)
                .append("dur=").append(duration).append(PARAMS_SEPARATOR)
                .append("echid=").append(echid).append(PARAMS_SEPARATOR)
                .append("chid=").append(chid)
                .toString().replaceAll("null", "");
    }

    /**
     * 当前播放的路径,用以判断播放的number
     */
    public String path;

    public long startTime;


    public void startCountTime() {
        duration = "";
        if (startTime <= 0L) {
            startTime = System.currentTimeMillis();
        }
    }

    public void endCountTime() {
        long currentTime = System.currentTimeMillis();
        duration = String.valueOf((currentTime - startTime ) / 1000);
        startTime = 0L;
    }

    //    private long totalTime;

//    public void pauseTime() {
//        long currentTime = System.currentTimeMillis();
//        totalTime = totalTime + currentTime - startTime;
//        startTime = 0;
//    }
}
