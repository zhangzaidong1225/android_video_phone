package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 用户登录统计类
 */
public class LoginRecord extends Record {

    /**
     * 登陆客户端所使用的账号类型，分为4种： 新浪微博：weibo 腾讯微博：qqweibo 凤凰通行证：ifeng qq空间：qzone
     */
    private final String type;
    /**
     * 成功注册yes, 注册失败/用户中断注册：no
     */
    private final boolean yn;

    /**
     * @param type 登陆客户端所使用的账号类型，分为4种： 新浪微博：weibo 腾讯微博：qqweibo 凤凰通行证：ifeng qq空间：qzone
     * @param yn   成功注册yes, 注册失败/用户中断注册：no
     */
    public LoginRecord(String type, boolean yn) {

        this.type = type;
        this.yn = yn;
    }

    @Override
    public String getRecordType() {
        return "login";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("type=").append(PARAMS_SEPARATOR)
                .append("yn=").append(yn)
                .toString().replaceAll("null", "");
    }
}
