package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 播放过程中的err统计类
 */
public class VErrRecord extends Record {

    /**
     * 错误id
     */
    private final String errId;
    /**
     * 视频id
     */
    private final String vid;
    /**
     * 视频类型
     */
    private final String pType;
    /**
     * 播放器 zy(默认)、ijk
     */
    private final String player;
    /**
     * zy播放器状态码
     */
    private final String psta = "";


    public VErrRecord(String errId, String vid, String pType, String player) {
        this.errId = errId;
        this.vid = vid;
        this.pType = pType;
        this.player = player;
    }

    @Override
    public String getRecordType() {
        return "err";
    }

    private boolean isIfengPlayer() {
        return "zy".equals(player);
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("errid=").append(this.errId).append(PARAMS_SEPARATOR)
                .append("vid=").append(this.vid).append(PARAMS_SEPARATOR)
                .append("ptype=").append(this.pType).append(PARAMS_SEPARATOR)
                .append("player=").append(this.player).append(PARAMS_SEPARATOR)
//                .append("psta=").append((isIfengPlayer() ? IfengMediaPlayer.getMediaSdkInfo() : ""))
                .append("psta=").append("")
                .toString().replaceAll("null", "");
    }

}
