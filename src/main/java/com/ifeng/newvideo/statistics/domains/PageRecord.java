package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.Record;
import com.ifeng.video.core.utils.DateUtils;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

/**
 * 页面跳转统计类
 */
public class PageRecord extends Record {

    private static final Logger logger = LoggerFactory.getLogger(PageRecord.class);
    /**
     * 仅是广告页发，其他都传“”
     */
    private final String title;
    /**
     * 页面类别，分为： 1 播放页：video 2 频道列表页：ch 3 其他页：other
     */
    private final String type;
    /**
     * 来源页面id 若为推送进入，来源为push 若为第三方引入，来源为outside
     */
    private String ref;
    /**
     * 标记，需要确定来自来源页面具体位置时发该字段 焦点图：focus_1{焦点图的第几个},其他都传“”
     */
    private final String tag;
    /**
     * 在页面停留时长
     */
    private long dur;

    private String simid;

    private String rToken;

    /**
     * @param id:   播放页id根据命名规则;频道列表页id：频道/栏目id，广告id 广告页
     * @param title 仅是广告页发，其他都传“”
     * @param type  页面类别，分为： 1 播放页：video 2 频道列表页：ch 3 其他页：other
     * @param ref   来源页面id 若为推送进入，来源为push 若为第三方引入，来源为outside
     * @param tag   标记，需要确定来自来源页面具体位置时发该字段 焦点图：focus_1{焦点图的第几个},其他都传“”
     */
    public PageRecord(String id, String title, String type, String ref, String tag, long dur) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.ref = ref;
        this.tag = tag;
        this.dur = dur;
    }

    /**
     * @param id:   播放页id根据命名规则;频道列表页id：频道/栏目id，广告id 广告页
     * @param title 仅是广告页发，其他都传“”
     * @param type  页面类别，分为： 1 播放页：video 2 频道列表页：ch 3 其他页：other
     * @param ref   来源页面id 若为推送进入，来源为push 若为第三方引入，来源为outside
     * @param tag   标记，需要确定来自来源页面具体位置时发该字段 焦点图：focus_1{焦点图的第几个},其他都传“”
     */
    public PageRecord(String id, String title, String type, String ref, String tag) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.ref = ref;
        this.tag = tag;
        this.simid = PageIdConstants.TAG_EMPTY;
        this.rToken = PageIdConstants.TAG_EMPTY;
    }

    public PageRecord(String id, String title, String type, String ref, String tag, String simid, String rToken) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.ref = ref;
        this.tag = tag;
        this.simid = simid;
        this.rToken = rToken;
    }

    /**
     * @param id: 播放页id根据命名规则;频道列表页id：频道/栏目id，广告id 广告页
     */
    public PageRecord(String id) {
        this.id = id;
        this.title = PageIdConstants.TITLE_EMPTY;
        this.type = PageIdConstants.TYPE_OTHER;
        this.ref = PageIdConstants.REF_ID_EMPTY;
        this.tag = PageIdConstants.TAG_EMPTY;
    }

    @Override
    public String getRecordType() {
        return "page";
    }

    @Override
    public String getRecordContent() {
        //2017-06-21+15:25:13#page#id=home_ch_58$title=$type=ch$ref=home_ch_23$tag=$dur=4.779131
        return new StringBuilder()
                .append("id=").append(id).append(PARAMS_SEPARATOR)
                .append("type=").append(type).append(PARAMS_SEPARATOR)
                .append("ref=").append(ref).append(PARAMS_SEPARATOR)
                .append("tag=").append(tag).append(PARAMS_SEPARATOR)
                .append("dur=").append(Math.round(((int) (dur / 100)) / 10.0)).append(PARAMS_SEPARATOR)
                .append("title=").append(title).append(PARAMS_SEPARATOR)
                .append("simid=").append(simid).append(PARAMS_SEPARATOR)
                .append("rToken=").append(rToken)
                .toString().replaceAll("null", "");
    }


    public long getDur() {
        return dur;
    }

    public void setDur(long dur) {
        this.dur = dur;
    }

    /**
     * 进入页面时的时间
     */
    public static final String ENTER_PAGE_TIME = "enterPageTime";


    /**
     * 发送page统计
     * 发送时机：退出页面时发送
     *
     * @param pageRecord
     */
    private static void onPageEnd(PageRecord pageRecord) {
        if (IfengApplication.getInstance().getAttribute(PageRecord.ENTER_PAGE_TIME) == null) {
            return;
        }

        long dur = DateUtils.getCurrentTime() - (long) IfengApplication.getInstance().getAttribute(ENTER_PAGE_TIME);
        if (dur <= 0) {
            return;
        }

        pageRecord.setDur(dur);
        CustomerStatistics.sendPageRecord(pageRecord);
        IfengApplication.getInstance().removeAttribute(ENTER_PAGE_TIME);
    }

    /**
     * 进入页面时添加此方法
     */
    public static void onPageStart() {
        IfengApplication.getInstance().setAttribute(ENTER_PAGE_TIME, DateUtils.getCurrentTime());
    }

    /**
     * 发送page统计
     * 发送时机：退出页面时发送
     *
     * @param pageRecord
     * @param isFull     参数是否完整
     */
    public static void onPageEnd(PageRecord pageRecord, boolean isFull) {
        if ("".equals(pageRecord.ref)) {
            pageRecord.ref = PageActionTracker.lastPage;
        }
        PageActionTracker.lastPage = pageRecord.id;
        if (isFull) {
            CustomerStatistics.sendPageRecord(pageRecord);
        } else {
            onPageEnd(pageRecord);
        }
    }


}
