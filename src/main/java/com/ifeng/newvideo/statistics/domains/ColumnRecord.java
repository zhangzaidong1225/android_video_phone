package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.Record;

/**
 * 增加栏目订阅和退订的统计
 *
 * @author huhailong
 */
public class ColumnRecord extends Record {
    /**
     * 订阅内容类别，分3种：栏目：coloum  频道：ch 自媒体:wm
     */
    private String type;
    /**
     * 订阅具体操作，分2种：订阅：yes  退订：no
     */
    private boolean sub;
    /**
     * 栏目、频道、自媒体名称
     */
    private String title;

    /**
     * @param _id    栏目或者频道id
     * @param _type  订阅内容类别，分3种：栏目：coloum  频道：ch 自媒体 wm
     * @param _sub   订阅具体操作，分2种：订阅：yes  退订：no
     * @param _title 视频标题
     */
    public ColumnRecord(String _id, String _type, boolean _sub,
                        String _title) {
        this.id = _id;
        this.type = _type;
        this.sub = _sub;
        this.title = _title;

    }


    //---------------------操作的内容---------------------------------//
    /**
     * 订阅类型分3种，栏目：coloum  频道：ch 自媒体 wm
     */
    public static final String TYPE_COLOUM = "coloum";
    public static final String TYPE_CH = "ch";
    public static final String TYPE_WM = "wm";

    /**
     * 订阅具体操作，分2种：
     * 订阅：yes
     * 退订：no
     */
    private static final String YES = "yes";
    private static final String NO = "no";

    @Override
    public String getRecordType() {
        return RECORD_TYPE_SUB;
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("type=").append(type).append(PARAMS_SEPARATOR)
                .append("sub=").append(sub ? YES : NO).append(PARAMS_SEPARATOR)
                .append("id=").append(getId()).append(PARAMS_SEPARATOR)
                .append("title=").append(title)
                .toString().replaceAll("null", "");
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSub() {
        return sub;
    }

    public void setSub(boolean sub) {
        this.sub = sub;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
