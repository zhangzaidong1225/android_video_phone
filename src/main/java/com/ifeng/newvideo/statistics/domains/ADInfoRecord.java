package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.Record;

public class ADInfoRecord extends Record {

    private ADInfo adInfo;
    private String first;

    public ADInfoRecord(ADInfo adInfo, String first) {
        this.adInfo = adInfo;
        this.first = first;
    }


    @Override
    protected String getRecordType() {
        return "adinfo";
    }

    @Override
    protected String getRecordContent() {
        return new StringBuilder()
                .append("ainfo=").append(adInfo.getRecordContent()).append(Record.PARAMS_SEPARATOR)
                .append("first=").append(first)
                .toString().replaceAll("null", "");
    }

    public static void onFocusADItem(ADInfoRecord adInfoRecord) {
        CustomerStatistics.sendFocusADItem(adInfoRecord);
    }


    public static class ADInfo {
        private String loc;
        private String id;
        private String ch;

        public ADInfo(String loc, String id, String ch) {
            this.loc = loc;
            this.id = id;
            this.ch = ch;
        }

        public String getRecordContent() {
            return new StringBuilder()
                    .append(loc).append(PageIdConstants.PINFO_SPACE)
                    .append(id).append(PageIdConstants.PINFO_SPACE)
                    .append(ch)
                    .toString();
        }

        @Override
        public String toString() {
            return "ADInfo{" +
                    "loc='" + loc + '\'' +
                    ", id='" + id + '\'' +
                    ", ch='" + ch + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object object) {
            boolean sameSame = false;
            if (object != null && object instanceof ADInfo) {
                sameSame = this.id.equals(((ADInfo) object).id);
            }
            return sameSame;
        }
    }
}
