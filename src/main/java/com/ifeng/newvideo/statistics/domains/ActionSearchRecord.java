package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.Record;

/**
 * 按钮统计项 Created by guoxt on 2014/9/9
 */
public class ActionSearchRecord extends Record {

    /**
     * 开关按钮 yes/no 没有选项时为空即可
     */
    private final String act;
    /**
     * 按钮所在页面id
     */
    private final String page;

    // item 所在位置
    private String offset;

    private String simid;

    private String rToken;

    private String query;

    /**
     * @param id   按钮自定义id
     * @param act  开关按钮 yes/no
     * @param page 按钮所在页面id
     *
     */
    public ActionSearchRecord(String id, String act, String page) {
        this.id = id;
        this.act = act;
        this.page = page;
        this.simid = PageIdConstants.TAG_EMPTY;
        this.rToken = PageIdConstants.TAG_EMPTY;
    }


    public ActionSearchRecord(String id, String act, String page, String query,String offset, String simid, String rToken) {
        this.id = id;
        this.act = act;
        this.page = page;
        this.query = query;
        this.offset = offset;
        this.simid = simid;
        this.rToken = rToken;
    }

    @Override
    public String getRecordType() {
        return "action";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("id=").append(id).append(PARAMS_SEPARATOR)
                .append("act=").append((act)).append(PARAMS_SEPARATOR)
                .append("page=").append(page).append(PARAMS_SEPARATOR)
                .append("query=").append(query).append(PARAMS_SEPARATOR)
                .append("offset=").append(offset).append(PARAMS_SEPARATOR)
                .append("simid=").append(simid).append(PARAMS_SEPARATOR)
                .append("rToken=").append(rToken)
                .toString().replaceAll("null", "");
    }
}
