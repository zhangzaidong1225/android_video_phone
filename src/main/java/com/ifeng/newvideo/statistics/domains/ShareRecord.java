package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 分享统计类
 */
public class ShareRecord extends Record {
    /**
     * 编辑id
     */
    private final String echid;

    /**
     * 媒资id
     */
    private final String chid;

    /**
     * 自媒体id
     */
    private final String wmid;

    /**
     * 分享的平台名称
     * <p/>
     * id定义：
     * 1：weibo：新浪weibo
     * 2：weixin：微信、微信朋友圈
     * 3：qq：QQ
     * 4：h5：视频h5页面
     * 5：zfb：支付宝朋友
     * 6：renren：人人网
     * 7：qzone：QQ空间
     * 8：qqweibo：腾讯微博
     * 9：newsapp：新闻客户端
     */
    private final String share;
    /**
     * 分享成功yes/no
     */
    private final boolean yn;
    /**
     * 所在页面id
     */
    private final String page;

    /**
     * @param id    视频的id
     * @param echid 编辑id
     * @param chid  媒资id
     * @param wmid  自媒体id
     * @param share 分享的平台名称
     * @param yn    分享成功yes/no
     * @param page  所在页面id
     */
    public ShareRecord(String id, String echid, String chid, String wmid, String share, boolean yn, String page) {
        this.id = id;
        this.echid = echid;
        this.chid = chid;
        this.wmid = wmid;
        this.share = share;
        this.yn = yn;
        this.page = page;
    }

    public String getEchid() {
        return echid != null ? echid : "";
    }

    public String getChid() {
        return chid != null ? chid : "";
    }

    public String getWmid() {
        return wmid != null ? wmid : "";
    }

    public String getShare() {
        return share != null ? share : "";
    }

    public boolean isYn() {
        return yn;
    }

    public String getPage() {
        return page != null ? page : "";
    }

    @Override
    public String getRecordType() {
        return RECORD_TYPE_TS;
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("id=").append(getId()).append(PARAMS_SEPARATOR)
                .append("echid=").append(getEchid()).append(PARAMS_SEPARATOR)
                .append("chid=").append(getChid()).append(PARAMS_SEPARATOR)
                .append("wmid=").append(getWmid()).append(PARAMS_SEPARATOR)
                .append("share=").append(getShare()).append(PARAMS_SEPARATOR)
                .append("yn=").append(isYn() ? "yes" : "no").append(PARAMS_SEPARATOR)
                .append("page=").append(getPage())
                .toString().replaceAll("null", "");
    }
}
