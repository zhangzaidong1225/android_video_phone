package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.Record;
import com.ifeng.newvideo.statistics.StatisticsConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Vector;

/**
 * V统计基类
 */
public class VideoRecord extends Record {
    private static final Logger logger = LoggerFactory.getLogger(VideoRecord.class);
    /**
     * 是否成功播放视频第一帧
     */
    private boolean isSuccessPlayFirstFrame = false;
    public static final Object recordsLock = new Object();
    public static final Vector<VideoRecord> videoRecords = new Vector<VideoRecord>();
    /**
     * 播放器有两种: 自有播放器和ijk播放器
     * zy、ijk
     */
    private String player;
    boolean useIJKPlayer;
    /**
     * 卡顿次数
     */
    protected int BN = 0;
    // 自媒体id
    protected String wmid;
    // 自媒体名称
    protected String wmname;

    VideoRecord() {
        videoRecords.add(this);
    }

    VideoRecord(VideoRecord vRecord) {
        this();
        synchronized (VideoRecord.recordsLock) {
            videoRecords.remove(vRecord);
        }
    }

    @Override
    public String getRecordType() {
        return "";
    }

    public String getWmid() {
        return wmid == null ? "" : wmid;
    }

    public String getWmname() {
        return wmname == null ? "" : wmname;
    }

    public void setWmid(String wmid) {
        this.wmid = wmid;
    }

    public void setWmname(String wmname) {
        this.wmname = wmname;
    }


    /**
     * 记录卡顿次数
     */
    public void statisticBN() {
        BN++;
        logger.debug("VRecord bn={}", BN);
    }

    /**
     * 设置卡顿次数
     *
     * @param bN
     */
    public void setBN(int bN) {
        this.BN = bN;
    }

    protected int getBN() {
        return BN;
    }

    /**
     * 通过卡顿次数生成错误码
     *
     * @return
     */
    public String getBNString() {
        String str = null;
        switch (BN) {
            case 0:
                str = "";
                break;
            case 1:
                str = '-' + StatisticsConstants.STATISTICS_ERR_PLAY_STUCK_ONE;
                break;
            case 2:
                str = '-' + StatisticsConstants.STATISTICS_ERR_PLAY_STUCK_TWO;
                break;
            case 3:
                str = '-' + StatisticsConstants.STATISTICS_ERR_PLAY_STUCK_THREE;
                break;
            case 4:
                str = '-' + StatisticsConstants.STATISTICS_ERR_PLAY_STUCK_FOUR_OR_MORE;
                break;
            default:
                str = '-' + StatisticsConstants.STATISTICS_ERR_PLAY_STUCK_FOUR_OR_MORE;
                break;
        }
        return str;
    }

    /**
     * 开始计算播放时长
     */
    public void startPlayTime() {
    }

    /**
     * 停止计算播放时长
     */
    public void stopPlayTime() {
    }

    public void setSuccessPlayFirstFrame(boolean flag) {
        this.isSuccessPlayFirstFrame = flag;
    }

    public boolean isSuccessPlayFirstFrame() {
        return this.isSuccessPlayFirstFrame;
    }

    public long getTotalPlayTime() {
        return 0;
    }

    public String getPlayer() {
        return this.player = useIJKPlayer ? "ijk" : "zy";
    }


    public void reset() {
    }

}
