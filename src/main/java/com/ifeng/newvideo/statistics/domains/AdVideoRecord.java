package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.Record;

/**
 * Created by liusd on 2016/3/28.
 * 前贴片广告统计
 */
public class AdVideoRecord extends Record {
    //用户id
    public String uid;
    //广告id
    public String adId;
    //播放完成 0--未完成  1---完成
    public int pcomplete;
    //播放时长(s)
    public int ptime;
    //暂停次数
    public int pstop;
    //重播次数
    public int preplay;
    //订单id
    public String oid;
    //广告产品id
    public String pid;

    private int pauseNum;
    private int completeNum;
    private int prePlayNum;

    private long startPlaytime;
    private long endPlayTime;
    public long duration;

    public AdVideoRecord(String uid, String adId, int pcomplete, int ptime, int pstop, int preplay, String oid, String pid, long duration) {
        this.adId = adId;
        this.pcomplete = pcomplete;
        this.ptime = ptime;
        this.pstop = pstop;
        this.preplay = preplay;
        this.oid = oid;
        this.pid = pid;
        this.uid = uid;
        this.duration = duration;
    }

    public AdVideoRecord(String uid, String adId, String oid, String pid, long duration) {
        this.uid = uid;
        this.adId = adId;
        this.oid = oid;
        this.pid = pid;
        this.duration = duration;
    }

    public AdVideoRecord(String adId, long duration) {
        this.adId = adId;
        this.duration = duration;
    }

    public AdVideoRecord() {
    }

    public int setPauseNum() {
        return pauseNum += 1;
    }

    public int setCompleteNum() {
        return completeNum += 1;
    }

    public int setPrePlayNum() {
        return prePlayNum += 1;
    }

    public int getPauseNum() {
        return pauseNum;
    }

    public int getPrePlayNum() {
        return prePlayNum;
    }

    public int getCompleteNum() {
        return completeNum;
    }

    public void resetNum() {
        pauseNum = 0;
        completeNum = 0;
        prePlayNum = 0;
    }

    /**
     * 开始计算播放时长
     */
    public void startPlayTime() {
        if (startPlaytime == 0) {
            startPlaytime = System.currentTimeMillis() / 1000;
        }
//        endPlayTime = 0;
    }

    /**
     * 停止计算播放时长
     */
    public void stopPlayTime() {
        if (startPlaytime != 0) {
            endPlayTime = System.currentTimeMillis() / 1000;
            ptime += endPlayTime - startPlaytime;
            startPlaytime = 0;
        }
    }

    public String getRecord() {
        return new StringBuilder()
                .append("uid=").append(uid).append(Record.SPLICE_SEPARATOR)
                .append("adid=").append(adId).append(Record.SPLICE_SEPARATOR)
                .append("pcomplete=").append(pcomplete).append(Record.SPLICE_SEPARATOR)
                .append("ptime=").append(ptime).append(Record.SPLICE_SEPARATOR)
                .append("pstop=").append(pstop).append(Record.SPLICE_SEPARATOR)
                .append("preplay=").append(preplay).append(Record.SPLICE_SEPARATOR)
                .append("oid=").append(oid).append(Record.SPLICE_SEPARATOR)
                .append("pid=").append(pid)
                .toString().replaceAll("null", "");
    }

    /**
     * 发送视频广告上报接口
     */
    public static void sendAdVideoStatisticSession(AdVideoRecord adVideoRecord) {
        CustomerStatistics.sendAdVideo(adVideoRecord);
    }


    @Override
    protected String getRecordType() {
        return "advideo";
    }

    @Override
    public String toString() {
        return "AdVideoRecord{" +
                "uid='" + uid + '\'' +
                ", adId='" + adId + '\'' +
                ", pcomplete=" + pcomplete +
                ", ptime=" + ptime +
                ", pstop=" + pstop +
                ", preplay=" + preplay +
                ", oid='" + oid + '\'' +
                ", pid='" + pid + '\'' +
                '}';
    }
}
