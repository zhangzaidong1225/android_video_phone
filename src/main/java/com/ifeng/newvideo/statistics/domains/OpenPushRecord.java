package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;
import com.ifeng.newvideo.statistics.StatisticsConstants;

/**
 * 打开推送的统计类
 */
public class OpenPushRecord extends Record {

    /**
     * 推送资源的ID*
     */
    private final String id;
    /**
     * 推送类型
     */
    private String type;

    /**
     * @param id   推送資源的ID
     * @param type 推送類型
     */
    public OpenPushRecord(String id, String type) {
        this.id = id;
        this.type = type;
        if (this.type.equalsIgnoreCase(StatisticsConstants.PUSH_TYPE_IMCPTOPIC) || this.type.equals(StatisticsConstants.PUSH_TYPE_CMPPTOPIC) || this.type.equals(StatisticsConstants.PUSH_TYPE_FOCUS) || this.type.equals(StatisticsConstants.PUSH_TYPE_LIANBO)) {
            this.type = StatisticsConstants.PUSH_TYPE_TOPIC;
        }
        if (this.type.equals(StatisticsConstants.PUSH_TYPE_LIVEROOM)) {
            this.type = StatisticsConstants.PUSH_TYPE_LIVE;
        }
    }

    @Override
    public String getRecordType() {
        return "openpush";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder().
                append("id=").append(id).append(PARAMS_SEPARATOR).
                append("type=").append(type).
                toString().replaceAll("null", "");
    }

}
