package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 签到统计
 */
public class SignInRecord extends Record {

    private final String add;
    /**
     * 按钮所在页面id
     */
    private final String page;

    /**
     * @param id   按钮自定义id
     * @param add
     * @param page 按钮所在页面id
     */
    public SignInRecord(String id, String add, String page) {
        this.id = id;
        this.add = add;
        this.page = page;
    }

    @Override
    public String getRecordType() {
        return "action";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("id=").append(id).append(PARAMS_SEPARATOR)
                .append("add=").append(add).append(PARAMS_SEPARATOR)
                .append("page=").append(page)
                .toString().replaceAll("null", "");
    }
}
