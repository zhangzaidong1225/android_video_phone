package com.ifeng.newvideo.statistics.domains;

import android.text.TextUtils;
import com.ifeng.newvideo.statistics.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by liusd on 2016/3/23.
 */
public class ADRecord extends Record {

    private static final Logger logger = LoggerFactory.getLogger(ADRecord.class);
    private static final Map<String, AdRecordModel> adMap = new ConcurrentHashMap<>();

    /**
     * 广告展示时调用
     *
     * @param id
     * @param type
     */
    public static void addAdShow(String id, String type) {
        if (TextUtils.isEmpty(id)) {
            logger.debug("in adRecord the addAdShow id === null");
            return;
        }
        logger.debug("in adRecord the addAdShow id = {} and the type = {}", id, type);
        AdRecordModel adRecordModel = adMap.get(id);
        if (adRecordModel != null) {//存在该广告，只需在展示次数上加1
            adRecordModel.setPlayNum(adRecordModel.getPlayNum() + 1);
        } else {//初始化该广告实例
            adRecordModel = new AdRecordModel(id, type);
            adRecordModel.setPlayNum(1);
        }
        adMap.put(id, adRecordModel);
    }

    /**
     * 广告点击时调用
     *
     * @param id
     */
    public static void addAdClick(String id, String type) {
        if (TextUtils.isEmpty(id)) {
            logger.debug("in adrecord the addAdClick id === null");
            return;
        }
        logger.debug("in adrecord the addAdClick id === {} and the type ==={}", id, type);
        AdRecordModel adRecordModel = adMap.get(id);
        if (adRecordModel != null) {//存在该广告，只需在点击次数上加1
            adRecordModel.setClickNum(adRecordModel.getClickNum() + 1);
        } else {//初始化该广告实例
            adRecordModel = new AdRecordModel(id, type);
            adRecordModel.setClickNum(1);
        }
        adMap.put(id, adRecordModel);
    }

    /**
     * 视频广告播放时调用
     *
     * @param id
     * @param playTime
     * @param duration
     */
    public static void addPlayTime(String id, long playTime, long duration) {
        if (TextUtils.isEmpty(id)) {
            logger.debug("in adrecord the addPlayTime id === null");
            return;
        }
        logger.debug("in adrecord the addPlayTime id === {} and the playTime ==={} and the duration =={}", id, playTime, duration);
        if (playTime > duration) {
            playTime = duration;
        }
        AdRecordModel adRecordModel = adMap.get(id);
        if (adRecordModel != null) {//存在该广告，只需在点击次数上加1
            adRecordModel.setPlayNum(adRecordModel.getPlayNum() + 1);
            adRecordModel.setPlayTime(adRecordModel.getPlayTime() + playTime);
        } else {//初始化该广告实例
            adRecordModel = new AdRecordModel(id, AdRecordModel.ADTYPE_VIDEO);
            adRecordModel.setPlayNum(1);
            adRecordModel.setPlayTime(playTime);
        }
        adRecordModel.setDuration(duration);
        adMap.put(id, adRecordModel);
    }

    public static void resetAdRecord() {
        adMap.clear();
    }

    /**
     * 获取统计信息，发送时调用
     *
     * @return
     */
    public static String getRecord() {
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, AdRecordModel> entry : adMap.entrySet()) {
            if (TextUtils.isEmpty(str)) {
                str.append(entry.getValue().toString());
            } else {
                str.append(';').append(entry.getValue().toString());
            }
        }
        logger.debug("in adRecord the str === {}", str.toString());
        return str.toString().replaceAll("null", "");
    }

    @Override
    protected String getRecordType() {
        return "ad";
    }


    public static class AdRecordModel {

        public static final String ADTYPE_INFO = "info";
        public static final String ADTYPE_VIDEO = "video";

        public AdRecordModel(String id, String adType) {
            this.id = id;
            this.adType = adType;
        }

        private String id;//广告id
        private String adType;//广告类型 info/video
        private int playNum = 0;//展示/播放次数
        private int clickNum = 0;//点击次数
        //视频广告上报
        private long playTime = 0;//播放时长
        private long duration = 0;//总时长
        private String SEP = ",";//间隔符

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAdType() {
            return adType;
        }

        public void setAdType(String adType) {
            this.adType = adType;
        }

        public int getPlayNum() {
            return playNum;
        }

        public void setPlayNum(int playNum) {
            this.playNum = playNum;
        }

        public int getClickNum() {
            return clickNum;
        }

        public void setClickNum(int clickNum) {
            this.clickNum = clickNum;
        }

        public long getPlayTime() {
            return playTime;
        }

        public void setPlayTime(long playTime) {
            this.playTime = playTime;
        }

        public long getDuration() {
            return duration;
        }

        public void setDuration(long duration) {
            this.duration = duration;
        }


        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(adType).append(SEP).append(id).append(SEP).append(playNum).append(SEP).append(clickNum);

            if (!"info".equals(adType)) {
                sb.append(SEP).append(playTime).append(SEP).append(duration);
            }
            return sb.toString().replaceAll("null", "");
        }
    }
}
