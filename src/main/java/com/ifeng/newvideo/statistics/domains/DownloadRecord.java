package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 下载统计相关的类
 *
 * @author huhailong
 */
public class DownloadRecord extends Record {

    /**
     * 开始下载：start下载完成：end 取消下载：cancel *
     */
    private final String done;
    /**
     * 下载的programid *
     */
    private final String id;
    /**
     * 下载标题
     */
    private final String title;

    /**
     * 页面id，在哪个页面点缓存
     */
    private final String page;

    /**
     * @param id    下载视频id
     * @param title 下载视频标题
     * @param done  分三种情况赋值：开始下载：start 下载完成：end （下载过程中删除下载）取消下载：cancel
     * @param page  页面id(在哪个页面点缓存)
     */
    public DownloadRecord(String id, String title, String done, String page) {
        this.id = id;
        this.title = title;
        this.done = done;
        this.page = page;
    }

    @Override
    public String getRecordType() {
        return "od";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("done=").append(done).append(PARAMS_SEPARATOR)
                .append("id=").append(id).append(PARAMS_SEPARATOR)
                .append("title=").append(title).append(PARAMS_SEPARATOR)
                .append("page=").append(page)
                .toString().replaceAll("null", "");
    }

}
