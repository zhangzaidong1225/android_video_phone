package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.Record;

import java.util.List;

public class LiveExposeRecord extends Record {

    private PInfo pInfo;

    public LiveExposeRecord(PInfo pInfo) {
        this.pInfo = pInfo;
    }

    @Override
    protected String getRecordContent() {
        return new StringBuilder()
                .append("pinfo=").append(pInfo.getRecordContent())
                .append(":")
                .toString().replaceAll("null", "");
    }

    @Override
    protected String getRecordType() {
        return "pageinfo";
    }

    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;

        if (object != null && object instanceof LiveExposeRecord) {
            sameSame = this.pInfo.equals(((LiveExposeRecord) object).pInfo);
        }

        return sameSame;
    }

//    @Override
//    public String toString() {
//        return "LiveExposeRecord{" +
//                "pInfo=" + pInfo +
//                '}';
//    }

    public static void onFocusLivePageItem(List<LiveExposeRecord> liveExposeRecords) {
        CustomerStatistics.sendLiveFocusItem(liveExposeRecords);
    }

    public static class PInfo {
        private String id;
        private String rnum;
        private String reftype;
        private String ch;
        private String src;

        public PInfo(String id, String rnum, String reftype, String ch, String src) {
            this.id = id;
            this.rnum = rnum;
            this.reftype = reftype;
            this.ch = ch;
            this.src = src;
        }

        public PInfo(String id, String rnum, String reftype, String ch) {
            this.id = id;
            this.rnum = rnum;
            this.reftype = reftype;
            this.ch = ch;
        }

        public String getRecordContent() {
            return new StringBuilder()
                    .append(id).append(PageIdConstants.PINFO_SPACE)
                    .append(rnum).append(PageIdConstants.PINFO_SPACE)
                    .append(reftype).append(PageIdConstants.PINFO_SPACE)
                    .append(ch).append(PageIdConstants.PINFO_SPACE)
                    .append(src)
                    .toString();
        }

        @Override
        public String toString() {
            return "PInfo{" +
                    "id='" + id + '\'' +
                    ", rnum='" + rnum + '\'' +
                    ", reftype='" + reftype + '\'' +
                    ", ch='" + ch + '\'' +
                    ", src='" + src + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object object) {
            boolean sameSame = false;

            if (object != null && object instanceof LiveExposeRecord.PInfo) {
                sameSame = this.id.equals(((LiveExposeRecord.PInfo) object).id);
            }

            return sameSame;
        }
    }
}
