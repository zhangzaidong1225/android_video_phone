package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;
import com.ifeng.newvideo.statistics.StatisticsConstants;

/**
 * 凤凰新闻SDK  in统计
 */

public class NewsSDKInRecord extends Record {

    /**
     * 打开方式
     * 直接打开：direct, 推送打开：push, 第三方打开：outside（即从第三方分享或应用中打开）
     */
    private String type;
    /**
     * 登陆状态，分2种：已登录：on, 未登录：off
     */
    private boolean status;

    public NewsSDKInRecord() {
        this.type = StatisticsConstants.APPSTART_TYPE_FROM_DIRECT;
    }

    /**
     * 适用于直接打开或者推送打开的情况
     *
     * @param type
     * @param status
     */
    public NewsSDKInRecord(String type, boolean status) {
        this.type = type;
        this.status = status;
    }

    /**
     * 适用于第三方来源打开的情况
     *
     * @param type
     * @param id     判断第三方引入的id： (新闻客户端引入)：newsapp (H5引入):h5, 其余id可自由定义，均小写，只限字母和数字组成
     * @param status
     */
    public NewsSDKInRecord(String type, String id, boolean status) {
        this.type = type;
        this.id = id;
        this.status = status;
    }


    @Override
    public String getRecordType() {
        return "in";
    }

    @Override
    public String getRecordContent() {
        StringBuilder sb = new StringBuilder();
        sb.append("type=").append(type); // .append(PARAMS_SEPARATOR);

        //由第三方来源打开应用需要传id
        if (type.equalsIgnoreCase(StatisticsConstants.APPSTART_TYPE_FROM_OUTSIDE)) {
            sb.append("id=").append(id).append(PARAMS_SEPARATOR);
        }
        // sb.append("status=").append((status ? "on" : "off"));
        return sb.toString().replaceAll("null", "");
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


}
