package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.Record;

/**
 * 直播action 统计
 */
public class ActionLiveRecord extends Record {

    private String act;
    public String page;
    public String add;

    public ActionLiveRecord(String id, String page, String add) {
        this.id = id;
        this.page = page;
        this.add = add;
        this.act = "";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("id=").append(id).append(PARAMS_SEPARATOR)
                .append("act=").append(act).append(PARAMS_SEPARATOR)
                .append("page=").append(page).append(PARAMS_SEPARATOR)
                .append("add=").append(add)
                .toString().replaceAll("null", "");
    }

    public static void sendActionLiveStatisticSession(ActionLiveRecord actionLiveRecord) {
        CustomerStatistics.sendActionLive(actionLiveRecord);
    }

    @Override
    protected String getRecordType() {
        return "action";
    }
}
