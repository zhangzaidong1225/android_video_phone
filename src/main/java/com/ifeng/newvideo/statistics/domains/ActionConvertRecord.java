package com.ifeng.newvideo.statistics.domains;

/**
 * 直播action 统计转换类
 */
public class ActionConvertRecord {

    private String type;
    private String id;
    private String psnum;

    public ActionConvertRecord(String type, String id, String psnum) {
        this.type = type;
        this.id = id;
        this.psnum = psnum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPsnum() {
        return psnum;
    }

    public void setPsnum(String psnum) {
        this.psnum = psnum;
    }

    @Override
    public String toString() {
        return "ActionConvertRecord{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", psnum='" + psnum + '\'' +
                '}';
    }
}
