package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 用户注册统计类
 */
public class RegisterRecord extends Record {
    /**
     * 注册方式： 手机注册：phone 邮箱注册：email
     */
    private final String type;
    /**
     * 成功注册yes, 注册失败/用户中断注册：no
     */
    private final boolean yn;

    /**
     * @param type 注册方式： 手机注册：phone 邮箱注册：email
     * @param yn   成功注册yes, 注册失败/用户中断注册：no
     */
    public RegisterRecord(String type, boolean yn) {
        this.type = type;
        this.yn = yn;
    }

    @Override
    public String getRecordType() {
        return "register";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("type=").append(type).append(PARAMS_SEPARATOR)
                .append("yn=").append(yn)
                .toString().replaceAll("null", "");
    }
}
