package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;

/**
 * 退出app时间统计
 *
 * @author: 胡海龙
 * @version: v1.0.0
 * @since: 2012-4-18
 */
public class ExitAppRecord extends Record {

    /**
     * 此次启动的总使用时长(即打开时长),单位秒
     */
    final long openDuration;
    /**
     * 此次启动的总播放时长,单位秒
     */
    public long playDuration;

    public boolean hasLocked;

    /**
     * @param openDuration 此次启动的总使用时长(即打开时长),单位秒
     * @param playDuration 此次启动的总播放时长,单位秒
     */
    public ExitAppRecord(long openDuration, long playDuration) {
        this.openDuration = openDuration;
        this.playDuration = playDuration;
    }

    @Override
    public String getRecordType() {
        return "end";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("odur=").append(Math.round(openDuration * 10) / 10.0).append(PARAMS_SEPARATOR)
                .append("pdur=").append(Math.round(playDuration * 10) / 10.0)
                .toString().replaceAll("null", "");
    }

    /**
     * 根据已知数据返回当前播放总时长
     *
     * @param nowPlayDuration 用户当前播放的总时长
     * @param duration        刚播放完毕的视频播放时长
     * @return 加入完刚播放完毕视频的播放总时长
     */
    public static long getAllPlayTime(Object nowPlayDuration, long duration) {
        if (nowPlayDuration == null) {
            return duration;
        } else {
            return Long.parseLong(nowPlayDuration.toString()) + duration;
        }
    }

}
