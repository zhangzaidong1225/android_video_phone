package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.Record;

/**
 * dlna统计相关类
 */
public class DlnaRecord extends Record {

    @Override
    public String getRecordType() {
        return "dlna";
    }

}
