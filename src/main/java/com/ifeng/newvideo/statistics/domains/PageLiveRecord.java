package com.ifeng.newvideo.statistics.domains;

import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.Record;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

public class PageLiveRecord extends Record {
    private static final Logger logger = LoggerFactory.getLogger(PageLiveRecord.class);

    public String ref;
    public String type;
    public String title;
    public String tag;
    public String dur;

    public PageLiveRecord(String id, String ref, String type) {
        this.id = id;
        this.ref = ref;
        this.type = type;
        this.dur = "";
        this.title = "";
        this.tag = "";
    }

    public PageLiveRecord(String id, String ref, String type, String dur) {
        this.id = id;
        this.ref = ref;
        this.type = type;
        this.dur = dur;
        this.title = "";
        this.tag = "";
    }

    @Override
    protected String getRecordContent() {
        return new StringBuilder()
                .append("id=").append(id).append(PARAMS_SEPARATOR)
                .append("title=").append(title).append(PARAMS_SEPARATOR)
                .append("ref=").append(ref).append(PARAMS_SEPARATOR)
                .append("type=").append(type).append(PARAMS_SEPARATOR)
                .append("tag=").append(tag).append(PARAMS_SEPARATOR)
                .append("dur=").append(dur)
                .toString().replaceAll("null", "");

    }

    public static void sendPageLiveStatisticSession(PageLiveRecord pageLiveRecord) {
        CustomerStatistics.sendPageLive(pageLiveRecord);
    }
//
//    @Override
//    public String toString() {
//        return "PageLiveRecord{" +
//                "id='" + id + '\'' +
//                ", ref='" + ref + '\'' +
//                ", type='" + type + '\'' +
//                '}';
//    }

    @Override
    protected String getRecordType() {
        return "page";
    }
}
