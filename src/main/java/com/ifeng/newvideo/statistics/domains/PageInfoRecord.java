package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.CustomerStatistics;
import com.ifeng.newvideo.statistics.PageIdConstants;
import com.ifeng.newvideo.statistics.Record;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.util.List;

public class PageInfoRecord extends Record {

    private static final Logger logger = LoggerFactory.getLogger(PageInfoRecord.class);

    private PInfo pInfo;
    private String rToken;
    private String simid;
    private String payload;


    public PageInfoRecord(PInfo pInfo) {
        this.pInfo = pInfo;
        this.rToken = PageIdConstants.TAG_EMPTY;
        this.simid = PageIdConstants.TAG_EMPTY;
        this.payload = PageIdConstants.TAG_EMPTY;
    }

    public PageInfoRecord(PInfo pInfo, String simid, String rToken, String payload) {
        this.pInfo = pInfo;
        this.simid = simid;
        this.rToken = rToken;
        this.payload = payload;
    }


    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;

        if (object != null && object instanceof PageInfoRecord) {
            sameSame = this.pInfo.equals(((PageInfoRecord) object).pInfo);
        }

        return sameSame;
    }

    @Override
    protected String getRecordType() {
        return "pageinfo";
    }

    @Override
    protected String getRecordContent() {
        return new StringBuilder()
                .append("pinfo=").append(pInfo.getRecordContent()).append(PARAMS_SEPARATOR)
                .append("rToken=").append(rToken).append(PARAMS_SEPARATOR)
                .append("simid=").append(simid).append(PARAMS_SEPARATOR)
                .append("payload=").append(payload)
                .toString().replaceAll("null", "");
    }

    public static void onFocusPageItem(List<PageInfoRecord> pageInfoRecords) {
        CustomerStatistics.sendFocusItem(pageInfoRecords);
    }

    public static class PInfo {
        private String id; //播放页id根据命名规则;频道列表页id：频道/栏目id，广告id 广告页
        private String rnum; //文章位置
        private String reftype;  //区分算法推荐和编辑推荐  editor/ai/sub/bs
        private String ch; //上一级跳转页面

        public PInfo(String id, String rnum, String reftype, String ch) {
            this.id = id;
            this.rnum = rnum;
            this.reftype = reftype;
            this.ch = ch;
        }

        public String getRecordContent() {
            return new StringBuilder()
                    .append(id).append(PageIdConstants.PINFO_SPACE)
                    .append(rnum).append(PageIdConstants.PINFO_SPACE)
                    .append(reftype).append(PageIdConstants.PINFO_SPACE)
                    .append(ch)
                    .toString();
        }

        @Override
        public String toString() {
            return "PInfo{" +
                    "id='" + id + '\'' +
                    ", rnum='" + rnum + '\'' +
                    ", reftype='" + reftype + '\'' +
                    ", ch='" + ch + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object object) {
            boolean sameSame = false;

            if (object != null && object instanceof PInfo) {
                sameSame = this.id.equals(((PInfo) object).id);
            }

            return sameSame;
        }
    }

}
