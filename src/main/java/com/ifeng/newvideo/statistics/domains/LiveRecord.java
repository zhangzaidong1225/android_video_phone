package com.ifeng.newvideo.statistics.domains;


/**
 * 直播统计类
 */
public class LiveRecord extends VideoRecord {

    // 直播标题 当前播放的节目名称
    private String liveTitle;
    // 资源播放时长（L or S ）长视频、短视频
    private String vtype;
    // 资源的所属ID（分类）如军事等
    private String chid;
    // 节目时长（秒）该节目总时长
    private String RTime;
    // 播放时长（秒）该节目播放时长
    private String pdur;
    // 所属编辑系统频道id：一级频道_二级频道_三级频道
    private String echid;
    // 节目播放类型（LV，LA，RV，RA）直播视频\直播音频\点播视频\点播音频
    private String ptype;
    // 直播台是哪个台（中文台、资讯台...）
    private String LT;
    // 是否是下载资源（YES/NO）
    private String op;
    // 是否成功播放第一帧
    private boolean yn;
    // 视频提供商 （cpName）如果没有该字段传入null
    private String sp;
    private long startPlaytime;
    private long endPlayTime;
    private long totalPlayTime = 0;

    private String simid;

    private String rToken;
    /**
     * 用来计算等待成功播放第一帧的时长
     */
    private long startPrepareTime;
    private float totalPrepareTime = 0;

    private Boolean isAudio = false;
    private Boolean isErr = false;

    /**
     * @param id           视频的guid（id）
     * @param title        视频标题
     * @param chid         所属频道id：一级频道_二级频道_三级频道 (专题id：12765_topicid 联播台id：12766_topicid 焦点id：12767_topicid cmpp专题id：12768_topicid)
     * @param ptype        节目播放类型（lv，la，rv，ra）直播视频\直播音频\点播视频\点播音频
     * @param sp           视频提供商 （cpName）如果没有该字段传入null
     * @param useIJKPlayer
     * @param wmid         自媒体id
     * @param wmname       自媒体名称
     */
    public LiveRecord(String id, String title, String chid, String ptype, String sp, LiveRecord record, String echid, boolean useIJKPlayer, String wmid, String wmname, String simid, String rToken) {
        super(record);
        this.id = id;
        this.liveTitle = title;
        this.chid = chid;
        this.ptype = ptype;
        this.wmid = wmid;
        this.wmname = wmname;
        this.op = "";
        this.sp = sp;
        this.vtype = "";
        this.echid = echid;
        startPrepareTime();
        this.useIJKPlayer = useIJKPlayer;
        this.simid = simid;
        this.rToken = rToken;

    }

    public void reset() {
        totalPlayTime = 0;
        totalPrepareTime = 0;
        startPlaytime = 0;
        endPlayTime = 0;
        setBN(0);
        isAudio = false;
        isErr = false;

    }

    @Override
    public String getRecordContent() {

        return new StringBuilder()
                .append("vid=").append(getId()).append(PARAMS_SEPARATOR)
                .append("chid=").append(getChid()).append(PARAMS_SEPARATOR)
                .append("wmid=").append(getWmid()).append(PARAMS_SEPARATOR)
                .append("wmname=").append(getWmname()).append(PARAMS_SEPARATOR)
                .append("title=").append(getLiveTitle()).append(PARAMS_SEPARATOR)
                .append("vtype=").append(getVtype()).append(PARAMS_SEPARATOR)
                .append("pdur=").append(Math.round(getTotalPlayTime() * 10) / 10.0).append(PARAMS_SEPARATOR)
                .append("vlen=").append(PARAMS_SEPARATOR)
                .append("ptype=").append(getPtype()).append(PARAMS_SEPARATOR)
                .append("op=").append(getOp()).append(PARAMS_SEPARATOR)
                .append("yn=").append((isSuccessPlayFirstFrame() ? "yes" : "no")).append(PARAMS_SEPARATOR)
                .append("ynw=").append(getYnw()).append(PARAMS_SEPARATOR)
                .append("sp=").append(getSp()).append(PARAMS_SEPARATOR)
                .append("bn=").append(getBN()).append(PARAMS_SEPARATOR)
                .append("echid=").append(getEchid()).append(PARAMS_SEPARATOR)
                .append("player=").append(getPlayer()).append(PARAMS_SEPARATOR)
                .append("tag=").append("").append(PARAMS_SEPARATOR)
                .append("simid=").append(getSimid()).append(PARAMS_SEPARATOR)
                .append("rToken").append(getrToken())
                .toString().replaceAll("null", "");
    }

    /**
     * 开始计算播放时长
     */
    public void startPlayTime() {
        if (startPlaytime == 0) {
            startPlaytime = System.currentTimeMillis() / 1000;
        }
        //        endPlayTime = 0;
    }

    /**
     * 停止计算播放时长
     */
    public void stopPlayTime() {
        if (startPlaytime != 0) {
            endPlayTime = System.currentTimeMillis() / 1000;
            totalPlayTime += endPlayTime - startPlaytime;
            startPlaytime = 0;
        }
    }

    /**
     * 等待播放第一帧的时长
     * 注意：无联网进播放时长为空；
     * 加载失败时为加载失败的等待时长，浮点型
     *
     * @return
     */
    public float getYnw() {
        return totalPrepareTime;
    }

    /**
     * 开始计算等待播放第一帧的时长
     */
    public void startPrepareTime() {
        if (startPrepareTime == 0) {
            startPrepareTime = System.currentTimeMillis();
        }
    }

    /**
     * 停止计算等待播放第一帧的时长
     */
    public void stopPrepareTime() {
        if (startPrepareTime != 0) {
            long endtime = System.currentTimeMillis();
            totalPrepareTime = (float) (endtime - startPrepareTime) / 1000.0f;
            startPrepareTime = 0;
        }
    }

    private String getEchid() {
        if (echid == null) {
            return "";
        }
        return echid;
    }

    public void setEchid(String echid) {
        this.echid = echid;
    }

    public void setTotalPlayTime(long totalPlayTime) {
        this.totalPlayTime = totalPlayTime;
    }

    /**
     * 获取刚播放完视频的播放时长
     *
     * @return
     */
    public long getTotalPlayTime() {
        return totalPlayTime;
    }

    private String getVtype() {
        return vtype == null ? "" : vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    private String getChid() {
        return chid == null ? "" : chid;
    }

    public void setChid(String chid) {
        this.chid = chid;
    }

    public String getPdur() {
        return pdur == null ? "" : pdur;
    }

    public void setPdur(String pdur) {
        this.pdur = pdur;
    }

    private String getPtype() {
        return ptype == null ? "" : ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getLT() {
        return LT == null ? "" : LT;
    }

    public void setLT(String lT) {
        LT = lT;
    }

    private String getOp() {
        return op == null ? "" : op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public boolean isYn() {
        return yn;
    }

    public void setYn(boolean yn) {
        this.yn = yn;
    }

    private String getSp() {
        return sp == null ? "" : sp;
    }

    public void setSp(String sp) {
        this.sp = sp;
    }

    public Boolean getIsAudio() {
        return this.isAudio;
    }

    public void setAudio(Boolean flag) {
        this.isAudio = flag;
    }

    public Boolean getIsErr() {
        return this.isErr;
    }

    public void setErr(Boolean flag) {
        this.isErr = flag;
    }

    public String getLiveTitle() {
        return liveTitle == null ? "" : liveTitle;
    }

    public void setLiveTitle(String liveTitle) {
        this.liveTitle = liveTitle;
    }

    public String getSimid() {
        return simid;
    }

    public void setSimid(String simid) {
        this.simid = simid;
    }

    public String getrToken() {
        return rToken;
    }

    public void setrToken(String rToken) {
        this.rToken = rToken;
    }

    @Override
    public String getRecordType() {
        return "v";
    }


}
