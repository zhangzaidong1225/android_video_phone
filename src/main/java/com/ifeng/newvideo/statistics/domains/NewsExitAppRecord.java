package com.ifeng.newvideo.statistics.domains;


/**
 * 新闻退出app时间统计
 */
public class NewsExitAppRecord extends ExitAppRecord {

    /**
     * @param openDuration 此次启动的总使用时长(即打开时长),单位秒
     */
    public NewsExitAppRecord(long openDuration) {
        super(openDuration, 0);
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("odur=").append(Math.round(openDuration * 10) / 10.0)
                .toString().replaceAll("null", "");
    }

}
