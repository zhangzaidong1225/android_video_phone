package com.ifeng.newvideo.statistics.domains;


import com.ifeng.newvideo.statistics.Record;
import com.ifeng.video.core.utils.DateUtils;

/**
 * 联网日志统计
 */
public class NetWorkRecord extends Record {

    @Override
    public String getRecordType() {
        return "hb";
    }

    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("tm=").append(DateUtils.getCurrentTime())
                .toString().replaceAll("null", "");
    }
}
