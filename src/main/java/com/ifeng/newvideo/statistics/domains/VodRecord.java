package com.ifeng.newvideo.statistics.domains;


import android.text.TextUtils;
import com.ifeng.video.dao.db.constants.CheckIfengType;

/**
 * 点播统计类
 */
public class VodRecord extends VideoRecord {

    // 资源名称
    private String title;
    // 资源播放时长（L or S ）长视频、短视频
    private String vtype;
    // 资源的所属ID（分类）如军事等
    private String chid;
    // 节目播放类型（LV，LA，RV，RA，fm）直播视频\直播音频\点播视频\点播音频\FM
    private String ptype;
    // 是否是下载资源（YES/NO）
    private String op;
    //是否成功播放第一帧
    private boolean yn;
    // 视频提供商 （cpName）如果没有该字段传入""
    private String sp;
    // 所属编辑系统频道id：一级频道_二级频道_三级频道
    private String echid;
    /**
     * 标记发送情况
     * 相关播放：relate 热点播放：hot 排行播放：rank 专题相关播放：topic
     * fm音频播放的来源: 频道首页：fm_hpage，查看更多：fm_more，最近播放：fm_his，下载缓存：fm_dload，收藏：fm_collect
     */
    private String tag;
    /**
     * 临时变量，用来计算实际播放总时长
     */
    private long startPlaytime;
    private long endPlayTime;
    private long totalPlayTime = 0;
    /**
     * 用来计算等待成功播放第一帧的时长
     */
    private long startPrepareTime;
    private float totalPrepareTime = 0;
    /**
     * 节目时长
     */
    private String programDur;

    /**
     * 实际视频流长度和xml长度不一致的错误码
     */
    private String durInconsistency = "";
    /**
     * 是否播放的是音频流
     */
    private Boolean isAudio = false;
    /**
     * 是否在seek操作后产生err
     */
    private Boolean isSeekErr = false;
    /**
     * 正常播放过程中是否产生err
     */
    private Boolean isErr = false;

    private String simid;

    private String rToken;

    @Override
    public String getRecordType() {
        return "v";
    }

    public VodRecord(VodRecord vRecord) {
        super(vRecord);
    }

    /**
     * 该构造提供给点播使用
     *
     * @param vid          视频的guid（id）
     * @param title        视频标题
     * @param chid         所属频道id：点播传入searchPath，
     * @param echid        来源频道Id  如从首页精选跳入播放页，就填精选的频道 channelId
     * @param wmid         自媒体id
     * @param wmname       自媒体名称
     * @param programDur   节目时长（秒）该节目总时长
     * @param ptype        节目播放类型（lv，la，rv，ra）直播视频\直播音频\点播视频\点播音频
     * @param op           是否是下载资源（yes/no）(除了离线播放器，其他都为no)
     * @param sp           视频提供商 （cpName）如果没有该字段传入""
     * @param tag          相关播放：relate
     *                     热点播放：hot
     *                     排行播放：rank
     *                     专题相关播放：topic
     *                     栏目往期/选集：previous
     *                     fm音频播放的来源:
     *                     ① 频道首页：fm_hpage
     *                     ② 查看更多：fm_more
     *                     ③ 最近播放：fm_his
     *                     ④ 下载缓存：fm_dload
     * @param useIJKPlayer 播放器
     */
    public VodRecord(String vid, String title, String chid, String echid, String wmid, String wmname, String programDur, String ptype, String op,
                     String sp, VodRecord vRecord, String tag, boolean useIJKPlayer, String simid, String rToken) {
        super(vRecord);
        this.id = vid;
        this.title = title;
        this.chid = convertVodChid(chid);
        this.wmid = wmid;
        this.wmname = wmname;
        this.ptype = ptype;
        this.op = op;
        this.sp = sp;
        this.programDur = programDur;
        this.echid = echid;
        this.tag = tag;
        getVideoVtype(programDur);
        startPrepareTime();
        this.useIJKPlayer = useIJKPlayer;
        this.simid = simid;
        this.rToken = rToken;
    }



    /**
     * 该构造主要提供专题使用
     *
     * @param id           视频的guid（id）
     * @param title        视频标题(仅直播和广告发)
     * @param type         所属专题类型（例：topic，cmpptopic）
     * @param topicId      topicId
     * @param wmid         自媒体id
     * @param wmname       自媒体名称
     * @param programDur   节目时长（秒）该节目总时长
     * @param ptype        节目播放类型（lv，la，rv，ra）直播视频\直播音频\点播视频\点播音频
     * @param op           是否是下载资源（yes/no）(除了离线播放器，其他都为no)
     * @param sp           视频提供商 （cpName）如果没有该字段传入""
     * @param echid        来源频道Id  如从首页精选跳入播放页，就填精选的频道 channelId
     * @param tag          直接赋值为topic
     * @param useIJKPlayer
     */
    public VodRecord(String id, String title, String type, String topicId, String wmid, String wmname, String programDur, String ptype, String op,
                     String sp, VodRecord vRecord, String echid, String tag, boolean useIJKPlayer, String simid, String rToken) {
        super(vRecord);
        this.id = id;
        this.title = title;
        this.chid = convertTopicChid(topicId, type);
        this.wmid = wmid;
        this.wmname = wmname;
        this.ptype = ptype;
        this.op = op;
        this.sp = sp;
        this.programDur = programDur;
        this.echid = echid;
        this.tag = tag;
        getVideoVtype(programDur);
        startPrepareTime();
        this.useIJKPlayer = useIJKPlayer;
        this.simid = simid;
        this.rToken = rToken;
    }


    @Override
    public String getRecordContent() {
        return new StringBuilder()
                .append("vid=").append(getId()).append(PARAMS_SEPARATOR)
                .append("chid=").append(getChid()).append(PARAMS_SEPARATOR)
                .append("echid=").append(getEchid()).append(PARAMS_SEPARATOR)
                .append("wmid=").append(getWmid()).append(PARAMS_SEPARATOR)
                .append("wmname=").append(getWmname()).append(PARAMS_SEPARATOR)
                .append("title=").append(title).append(PARAMS_SEPARATOR)
                .append("tag=").append(tag).append(PARAMS_SEPARATOR)
                .append("vtype=").append(getVtype()).append(PARAMS_SEPARATOR)
                .append("pdur=").append(Math.round(getTotalPlayTime() * 10) / 10.0).append(PARAMS_SEPARATOR)
                .append("vlen=").append((TextUtils.isEmpty(programDur) ? "" : programDur)).append(PARAMS_SEPARATOR)
                .append("ptype=").append(getPtype()).append(PARAMS_SEPARATOR)
                .append("op=").append(getOp()).append(PARAMS_SEPARATOR)
                .append("yn=").append((isSuccessPlayFirstFrame() ? "yes" : "no")).append(PARAMS_SEPARATOR)
                .append("ynw=").append(getYnw()).append(PARAMS_SEPARATOR)
                .append("sp=").append(getSp()).append(PARAMS_SEPARATOR)
                .append("bn=").append(getBN()).append(PARAMS_SEPARATOR)
                .append("player=").append(getPlayer()).append(PARAMS_SEPARATOR)
                .append("simid=").append(getSimid()).append(PARAMS_SEPARATOR)
                .append("rToken=").append(getrToken())
                .toString().replaceAll("null", "");
    }

    /**
     * 此方法主要是提供点播转换chid使用
     *
     * @param chid
     * @return
     */
    private String convertVodChid(String chid) {
        if (!TextUtils.isEmpty(chid)) {
            if ("-".equalsIgnoreCase(chid.substring(chid.length() - 1, chid.length()))) {
                String substring = chid.substring(0, chid.length() - 1);
                chid = substring.replace("-", "_");
            } else {
                chid = chid.replace("-", "_");
            }
        } else {
            return "";
        }
        return chid;
    }

    /**
     * 此方法主要是提供专题转换chid使用
     *
     * @param topicId
     * @param type
     */
    public static String convertTopicChid(String topicId, String type) {
        if (CheckIfengType.isCmppTopic(type)) {
            return "12768_" + topicId;
        }
        if (CheckIfengType.isLianBo(type)) {
            return "12766_" + topicId;
        }
        if (CheckIfengType.isFocus(type)) {
            return "12767_" + topicId;
        }
        if (CheckIfengType.isImcpTopic(type)) {
            return "12765_" + topicId;
        }
        return "";
    }


    /**
     * 此方法主要是提供判断长视频，短视频使用
     *
     * @param duration 如HH:mm:ss
     */
    private void getVideoVtype(String duration) {
        if (duration != null) {
            this.vtype = transformTime(duration) >= 5 * 60 ? "l" : "s";
        } else {
            this.vtype = "";
        }
    }

    /**
     * 将HH:MM:SS转换成秒
     */
    private static long transformTime(String time) {
        try {
            String[] my = time.split(":");
            if (my == null) {
                return Long.parseLong(time);
            }
            if (my.length == 3) {
                int hour = Integer.parseInt(my[0]);
                int min = Integer.parseInt(my[1]);
                int sec = Integer.parseInt(my[2]);
                return hour * 3600 + min * 60 + sec;
            } else if (my.length == 2) {
                int min = Integer.parseInt(my[0]);
                int sec = Integer.parseInt(my[1]);
                return min * 60 + sec;
            } else {
                return Long.parseLong(time);
            }
        } catch (Exception e) {
            return -1;
        }
    }


    /**
     * 开始计算播放时长
     */
    public void startPlayTime() {
        if (startPlaytime == 0) {
            startPlaytime = System.currentTimeMillis() / 1000;
        }
//        endPlayTime = 0;
    }

    /**
     * 停止计算播放时长
     */
    public void stopPlayTime() {
        if (startPlaytime != 0) {
            endPlayTime = System.currentTimeMillis() / 1000;
            totalPlayTime += endPlayTime - startPlaytime;
            startPlaytime = 0;
        }
    }

    /**
     * 等待播放第一帧的时长
     * 注意：无联网进播放时长为空；
     * 加载失败时为加载失败的等待时长，浮点型
     *
     * @return
     */
    public String getYnw() {
        return String.valueOf(totalPrepareTime);
    }

    /**
     * 开始计算等待播放第一帧的时长
     */
    public void startPrepareTime() {
        if (startPrepareTime == 0) {
            startPrepareTime = System.currentTimeMillis();
        }
    }

    /**
     * 停止计算等待播放第一帧的时长
     */
    public void stopPrepareTime() {
        if (startPrepareTime != 0) {
            long endTime = System.currentTimeMillis();
            totalPrepareTime = (float) (endTime - startPrepareTime) / 1000.0f;
            startPrepareTime = 0;
        }
    }


    /**
     * 重置一些变量
     */
    public void reset() {
        totalPlayTime = 0;
        totalPrepareTime = 0;
        startPlaytime = 0;
        endPlayTime = 0;
        durInconsistency = "";
        isAudio = false;
        isSeekErr = false;
        isErr = false;
        setBN(0);
    }

    public String getDurInconsistency() {
        return this.durInconsistency;
    }

    public void setDurInconsistency(String errCode) {
        this.durInconsistency = errCode;
    }

    public Boolean getIsAudio() {
        return this.isAudio;
    }

    public void setAudio(Boolean flag) {
        this.isAudio = flag;
    }

    public Boolean getIsSeekErr() {
        return this.isSeekErr;
    }

    public void setSeekErr(Boolean flag) {
        this.isSeekErr = flag;
    }

    public Boolean getIsErr() {
        return this.isErr;
    }

    public void setErr(Boolean flag) {
        this.isErr = flag;
    }

    public void setTotalPlayTime(long totalPlayTime) {
        this.totalPlayTime = totalPlayTime;
    }

    /**
     * 获取刚播放完视频的播放时长
     *
     * @return
     */
    public long getTotalPlayTime() {
        if (!TextUtils.isEmpty(programDur) && transformTime(programDur) < totalPlayTime) {
            return transformTime(programDur);
        }
        return totalPlayTime;
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEchid() {
        return echid == null ? "" : echid;
    }

    public void setEchid(String echid) {
        this.echid = echid;
    }

    private String getVtype() {
        return vtype == null ? "" : vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    private String getChid() {
        return chid == null ? "" : chid;
    }

    public void setChid(String chid) {
        this.chid = chid;
    }


    private String getPtype() {
        return ptype == null ? "" : ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }


    private String getOp() {
        return op == null ? "" : op;
    }

    public void setOp(String op) {
        this.op = op;
    }


    public boolean isYn() {
        return yn;
    }

    public void setYn(boolean yn) {
        this.yn = yn;
    }

    private String getSp() {
        return sp == null ? "" : sp;
    }

    public void setSp(String sp) {
        this.sp = sp;
    }

    public String getTag() {
        return tag == null ? "" : tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSimid() {
        return simid;
    }

    public void setSimid(String simid) {
        this.simid = simid;
    }

    public String getrToken() {
        return rToken;
    }

    public void setrToken(String rToken) {
        this.rToken = rToken;
    }

    // v统计 ptype 直播 video
    public static final String P_TYPE_LV = "lv";
    // v统计 ptype 直播 audio
    public static final String P_TYPE_LA = "la";
    // v统计 ptype 互动直播间 video
    public static final String P_TYPE_HDV = "hdv";
    public static final String P_TYPE_H5 = "h5";
    // v统计 ptype 互动直播间 audio
    public static final String P_TYPE_HDA = "hda";
    // v统计 ptype 点播 video
    public static final String P_TYPE_RV = "rv";
    // v统计 ptype 点播 fm
    public static final String P_TYPE_RA = "ra";
    // v统计 ptype FM
    public static final String P_TYPE_FM = "fm";
    // v统计 tag FM 首页
    public static final String V_TAG_FM_HPAGE = "fm_hpage";
    // v统计 tag FM 更多
    public static final String V_TAG_FM_MORE = "fm_more";
    // v统计 tag FM 看过
    public static final String V_TAG_FM_HIS = "fm_his";
    // v统计 tag FM 下载
    public static final String V_TAG_FM_DLOAD = "fm_dload";
    // v统计 tag FM 收藏
    public static final String V_TAG_FM_COLLECT = "fm_collect";
    // v统计 tag  排行
    public static final String V_TAG_RANK = "rank";
    // v统计 tag  热点
    public static final String V_TAG_HOT = "hot";
    // v统计 tag  相关
    public static final String V_TAG_RELATE = "relate";
    // v统计 tag  栏目选集、往期
    public static final String V_TAG_COLUMN = "previous";
    // v统计 tag  专题
    public static final String V_TAG_TOPIC = "topic";
    // v统计 tag  大图
    public static final String V_TAG_PICTURE = "picture";
    // v统计 tag  空
    public static final String V_TAG_EMPTY = "";
}
