package com.ifeng.newvideo.statistics;

import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.DateUtils;

/**
 * 记录基类
 *
 * @author: 胡海龙
 * @since: 2014-9-22
 * * 统计文档的地址:
 * http://10.32.21.52/doc/videoapp/
 */
public abstract class Record {

    protected String id;
    /**
     * 上报类型
     */
    protected static final String RECORD_TYPE_V = "v";
    protected static final String RECORD_TYPE_SUB = "sub";
    protected static final String RECORD_TYPE_TS = "ts";
    protected static final String RECORD_TYPE_STORE = "store";

    private static final String SEPARATOR = "#";
    protected static final String PARAMS_SEPARATOR = "$";
    protected static final String SESSION_SEPARATOR = "@";
    protected static final String SPLICE_SEPARATOR = "&";

    public String getId() {
        return id == null ? "" : id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected abstract String getRecordType();

    protected String getRecordContent() {
        return "";
    }

    /**
     * 发送统计时添加session后拼接该方法
     */
    public String toString() {
        return new StringBuilder()
                .append(DateUtils.getFormatCurTime()).append(SEPARATOR)
                .append(getRecordType()).append(SEPARATOR)
                .append(getRecordContent())
                .toString().replaceAll("null", "");
    }

    public String toLocationString() {
        // loc={City="北京市";Country="中国";Name="中轻大厦(溪阳东路)";State="北京市";Street="望京东路1号院";SubLocality="朝阳区"}
        return new StringBuilder()
                .append(toString())
                .append(PARAMS_SEPARATOR)
                .append("loc=").append(SharePreUtils.getInstance().getLocationForNews())
                .toString().replaceAll("null", "");
    }

    public String toListString() {
        return new StringBuilder()
                .append(DateUtils.getFormatCurTime()).append(SEPARATOR)
                .append(getRecordType()).append(SEPARATOR)
                .append(getRecordContent())
                .toString();
    }
}
