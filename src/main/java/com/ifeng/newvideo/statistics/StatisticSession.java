package com.ifeng.newvideo.statistics;


import android.content.Context;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.newvideo.statistics.domains.ADInfoRecord;
import com.ifeng.newvideo.statistics.domains.ADRecord;
import com.ifeng.newvideo.statistics.domains.AdVideoRecord;
import com.ifeng.newvideo.statistics.domains.ExitAppRecord;
import com.ifeng.newvideo.statistics.domains.LiveExposeRecord;
import com.ifeng.newvideo.statistics.domains.NewsExitAppRecord;
import com.ifeng.newvideo.statistics.domains.PageInfoRecord;
import com.ifeng.newvideo.statistics.domains.StartAppRecord;
import com.ifeng.newvideo.statistics.domains.VideoRecord;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.net.RequestString;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.ParametersUtils;
import com.ifeng.video.dao.db.dao.StatisticDAO;
import com.ifeng.video.dao.db.model.StatisticModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


/**
 * 统计接口需要的数据拼接类
 *
 * @author: huhailong
 */
class StatisticSession {

    private static final Logger logger = LoggerFactory.getLogger(StatisticSession.class);
    private static final String SESSION = "session";
    private final StatisticDAO statisticDao;
    private final Context mContext;

    /**
     * 获取发送自有统计的基本参数
     */
    private static String getOtherParamsString(Context context) {
        PhoneConfig.getInstance();
        return PhoneConfig.getCombineString(NetUtils.getNetType(context), context);
    }

    /**
     * 获取发送新闻统计的基本参数
     */
    private static String getOtherParamsStringForNews(Context context) {
        PhoneConfig.getInstance();
        return PhoneConfig.getCombineStringForNews(NetUtils.getNetType(context), context);
    }

    /**
     * 获取发送新闻SDK 统计的基本参数
     */
    private static String getOtherParamsStringForNewsSDK(Context context) {
        PhoneConfig.getInstance();
        return PhoneConfig.getCombineStringForNewsSDK(NetUtils.getNetType(context), context);
    }

    /**
     * 获取发送AD统计的基本参数
     */
    private static String getOtherParamsStringForAd(Context context) {
        PhoneConfig.getInstance();
        return PhoneConfig.getCombineStringForAd(NetUtils.getNetType(context), context);
    }

    /**
     * 获取发送播放Err统计的基本参数
     */
    private static String getOtherParamsStringForErr(Context context) {
        PhoneConfig.getInstance();
        return PhoneConfig.getCombineStringForErr(NetUtils.getNetType(context), context);
    }

    /**
     * 获取userkey
     */
    private static String getSessionId() {
        PhoneConfig.getInstance();
        return PhoneConfig.userKey;
    }

    public StatisticSession(Context context) {
        this.mContext = context;
        statisticDao = new StatisticDAO(context);
    }

    /**
     * 发送一条统计数据
     *
     * @param record
     * @param isErrRecord 是否发送播放错误统计
     */
    public void sendStatisticSession(Record record, Boolean isErrRecord) {
        if (record != null && record instanceof VideoRecord) {
            if (VideoRecord.videoRecords != null && !VideoRecord.videoRecords.contains(record)) {
                return;
            }
        }
        if (record instanceof ExitAppRecord) {
            Iterator<VideoRecord> it = VideoRecord.videoRecords.iterator();
            if (VideoRecord.videoRecords != null) {
                logger.debug("end statistics, contain V statistics size:{}", VideoRecord.videoRecords.size());
            }
            synchronized (VideoRecord.recordsLock) {
                while (it.hasNext()) {
                    VideoRecord vrRecord = it.next();
                    vrRecord.stopPlayTime();
                    if (vrRecord.getTotalPlayTime() <= 0) {
                        continue;
                    }
                    ((ExitAppRecord) record).playDuration += vrRecord.getTotalPlayTime();
                    sendRecord(vrRecord, isErrRecord);
                    vrRecord.reset();
                }
            }
            if (!((ExitAppRecord) record).hasLocked) {
                VideoRecord.videoRecords.clear();
            }
        }

        sendRecord(record, isErrRecord);
    }

    public void sendStatisticSessiongList(List<PageInfoRecord> pageInfoRecords, Boolean isErrRecord) {
        if (!ListUtils.isEmpty(pageInfoRecords)) {
            sendPageInfoRecord(pageInfoRecords, isErrRecord);
        }
    }

    public void sendStatisticSessiongLiveList(List<LiveExposeRecord> liveExposeRecords, Boolean isErrRecord) {
        if (!ListUtils.isEmpty(liveExposeRecords)) {
            sendPageInfoLiveRecord(liveExposeRecords, isErrRecord);
        }
    }

    private void sendRecord(Record record, Boolean isErrRecord) {
        String url = null;
        if (record instanceof ADInfoRecord) {
            url = StatisticsConstants.STATISTIC_POST + "?" + getOtherParamsString(mContext);
        } else {
            url = generateStatisticsUrl(isErrRecord);
        }
        //url encoding
        if (url != null) {
            String statisticStr;
            if (record instanceof StartAppRecord) {
                statisticStr = record.toLocationString();
            } else {
                statisticStr = record.toString();
            }
            url = url + "&" + ParametersUtils.encodeParamsInUTF8(SESSION, statisticStr);
            url = com.ifeng.signature.US.s(url);
            logger.debug("session:{}", url);
        }
        new Thread(new SendStatisticSessionTask(mContext, url, record), "statisticThread").start();
    }

    /**
     * 发送PageInfo,对list进行拼接
     */
    private void sendPageInfoRecord(List<PageInfoRecord> records, Boolean isErrRecord) {
        String url = StatisticsConstants.STATISTIC_POST + "?" + getOtherParamsString(mContext);
        //url encoding
        String statisticStr = records.get(0).toListString();
        StringBuilder pageInfo = new StringBuilder(statisticStr);
        if (records.size() > 1) {
            for (int i = 1; i < records.size(); i++) {
                pageInfo.append(Record.SESSION_SEPARATOR)
                        .append(records.get(i).toListString());
            }
        }
        String statisticPageInfo = pageInfo.toString().replaceAll("null", "");
        url = url + "&" + ParametersUtils.encodeParamsInUTF8(SESSION, statisticPageInfo);
        url = com.ifeng.signature.US.s(url);
        logger.debug("session:{}", url);

        new Thread(new SendStatisticSessionTask(mContext, url, records.get(0)), "statisticThread").start();
    }

    private void sendPageInfoLiveRecord(List<LiveExposeRecord> records, Boolean isErrRecord) {
        String url = StatisticsConstants.STATISTIC_POST + "?" + getOtherParamsString(mContext);
        //url encoding
        String statisticStr = records.get(0).toListString();
        StringBuilder pageInfo = new StringBuilder(statisticStr);
        if (records.size() > 1) {
            for (int i = 1; i < records.size(); i++) {
                pageInfo.append(Record.SESSION_SEPARATOR)
                        .append(records.get(i).toListString());
            }
        }
        String statisticPageInfo = pageInfo.toString().replaceAll("null", "");
        url = url + "&" + ParametersUtils.encodeParamsInUTF8(SESSION, statisticPageInfo);
        url = com.ifeng.signature.US.s(url);
        logger.debug("session:{}", url);

        new Thread(new SendStatisticSessionTask(mContext, url, records.get(0)), "statisticThread").start();
    }

    /**
     * 发送一条新闻客户端统计
     *
     * @param
     */
    public void sendNewsStatisticSession(Record record) {
        String url = StatisticsConstants.STATISTIC + "?" + getOtherParamsStringForNews(mContext);
        String statisticStr = "";
        if (record instanceof StartAppRecord) {
            statisticStr = record.toLocationString();
        } else if (record instanceof NewsExitAppRecord) {
            statisticStr = record.toString();
        }
        url = url + "&" + ParametersUtils.encodeParamsInUTF8(SESSION, statisticStr);
        url = url + "&sver=V1&sig=" + getSigForNews();
        new Thread(new SendStatisticSessionTask(mContext, url), "statisticThread").start();
    }

    /**
     * 发送一条新闻客户端统计
     *
     * @param
     */
    public void sendNewsSDKStatisticSession(Record record) {
        String url = StatisticsConstants.STATISTIC + "?" + getOtherParamsStringForNewsSDK(mContext);
        String statisticStr = record.toLocationString();
        url = url + "&" + ParametersUtils.encodeParamsInUTF8(SESSION, statisticStr);
        url = com.ifeng.signature.US.s(url);
        new Thread(new SendStatisticSessionTask(mContext, url), "statisticThread").start();
    }

    public void sendAdVideoStatistic(AdVideoRecord adVideoRecord) {
        String url = StatisticsConstants.STATISSTIC_VIDEO_AD + "?";
        String statictisUrl = url + adVideoRecord.getRecord();
        new Thread(new SendStatisticSessionTask(mContext, statictisUrl), "statisticThread").start();
    }

    private String getSigForNews() {
        String sig = SharePreUtils.getInstance().getSigForNews();
        if (!TextUtils.isEmpty(sig)) {
            return sig;
        }
        StringBuilder sigBuilder = new StringBuilder();
        // 生成sigNum
        Random random = new Random();
        BigDecimal primeNum = new BigDecimal(9999889);
        BigDecimal rdm = new BigDecimal(random.nextInt(50000) + 1);
        BigDecimal times = new BigDecimal(1000000);
        String sigNum = String.valueOf(primeNum.multiply(rdm).multiply(times));
        char[] sigArray = sigNum.toCharArray();
        int numSize = sigArray.length;
        int numIndex = 0;

        // 26个小写字母
        String[] letterArray = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        int letterIndex = 0;

        // 在sigNum随机插入字母，补全47位
        while (sigBuilder.toString().length() < 47) {
            if (numSize > numIndex) {
                sigBuilder.append(sigArray[numIndex]);
                numIndex++;
            }
            letterIndex = random.nextInt(letterArray.length);
            sigBuilder.append(letterArray[letterIndex]);
        }
        SharePreUtils.getInstance().setSigForNews(sigBuilder.toString());
        return sigBuilder.toString();
    }

    /**
     * 发送一条广告统计数据
     *
     * @param
     */
    public void sendAdStatisticSession() {
        String url = StatisticsConstants.STATISTIC_FOR_AD + "?" + getOtherParamsStringForAd(mContext);
        logger.debug("adRecord ----> the url === {}", url);
        String statisticUrl = ADRecord.getRecord();
        logger.debug("adRecord ----> the statisticUrl === {}", statisticUrl);
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(statisticUrl)) {
            return;
        }
        url = url + "&" + ParametersUtils.encodeParamsInUTF8(SESSION, statisticUrl);
        url = com.ifeng.signature.US.s(url);
        if (url != null) {
            logger.debug("adRecord session:{}", url);
        }
        ADRecord.resetAdRecord();
        new Thread(new SendStatisticSessionTask(mContext, url), "statisticThread").start();
    }


    private String generateStatisticsUrl(Boolean isErrRecord) {
        String url;
        if (isErrRecord) {//是否发送播放错误统计
            url = StatisticsConstants.STATISTIC_FOR_ERR + "?" + getOtherParamsStringForErr(mContext);
        } else {
            url = StatisticsConstants.STATISTIC + "?" + getOtherParamsString(mContext);
        }
        return url;
    }


    /**
     * 发送当前这条统计
     */
    private class SendStatisticSessionTask implements Runnable {
        private final Context context;
        private final String url;
        private Record record;

        public SendStatisticSessionTask(Context context, String url) {
            this.context = context;
            this.url = url;
        }

        public SendStatisticSessionTask(Context context, String url, Record record) {
            this.context = context;
            this.url = url;
            this.record = record;
        }

        @Override
        public void run() {
            // 如果设备ID为null 那么不做任何统计 不提交 不存储到本地
            if (getSessionId() == null) {
                logger.error("session id is invalid, abandon to submit statisticSession!");
                return;
            }
            if (record instanceof PageInfoRecord || record instanceof ADInfoRecord || record instanceof LiveExposeRecord) {
                postRequest(url, new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        logger.debug("send current statistics successful!!! {}", url);
                        //如果发送成功，检查数据库中是否存在之前未发送出去的统计，若有则发送
                        if (!isEmptyDB()) {
                            new Thread(new SendDBStatisticSessionTask(record), "dbStatisticThread").start();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("send current statistics failed!!!{} ,  {}", error, url);
                        //如果发送统计失败，则保存到数据库中以便下次发送
                        StatisticModel model = new StatisticModel();
                        model.setPostUrl(url);
                        statisticDao.saveStatisticData(model);
                    }
                });
            } else {
                //发送统计
                getRequest(url, new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        logger.debug("send current statistics successful!!! {}", url);
                        //如果发送成功，检查数据库中是否存在之前未发送出去的统计，若有则发送
                        if (!isEmptyDB()) {
                            new Thread(new SendDBStatisticSessionTask(), "dbStatisticThread").start();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("send current statistics failed!!!{} ,  {}", error, url);
                        //如果发送统计失败，则保存到数据库中以便下次发送
                        StatisticModel model = new StatisticModel();
                        model.setPostUrl(url);
                        statisticDao.saveStatisticData(model);
                    }
                });
            }

        }

    }

    /**
     * 发送数据库中存留的统计
     */
    private class SendDBStatisticSessionTask implements Runnable {
        private Record record;

        public SendDBStatisticSessionTask() {
        }

        public SendDBStatisticSessionTask(Record record) {
            this.record = record;
        }

        private List<StatisticModel> getDBSessions() {
            try {
                return statisticDao.getAllStatisticData();
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
            return null;
        }

        @Override
        public void run() {
            List<StatisticModel> sessionsUrls = getDBSessions();
            if (sessionsUrls != null && sessionsUrls.size() > 0) {
                for (final StatisticModel model : sessionsUrls) {
                    if (model.getPostUrl() != null) {
                        logger.debug("DB!!!SessionUrl={}", model.getPostUrl());
                    }
                    if (record instanceof PageInfoRecord || record instanceof ADInfoRecord
                            || record instanceof LiveExposeRecord) {
                        postRequest(model.getPostUrl(), new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                logger.debug("send DB statistics successful!!!");
                                //发送成功则删除数据库相应数据
                                try {
                                    statisticDao.delete(model.getId());
                                } catch (Exception e) {
                                    logger.error(e.toString(), e);
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                    } else {
                        getRequest(model.getPostUrl(), new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                logger.debug("send DB statistics successful!!!");
                                //发送成功则删除数据库相应数据
                                try {
                                    statisticDao.delete(model.getId());
                                } catch (Exception e) {
                                    logger.error(e.toString(), e);
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                logger.debug("send DB statistics failed!!!");
                            }
                        });
                    }
                }
            }
        }
    }

    /**
     * 发送请求
     */
    private static void getRequest(String url, Response.Listener listener, Response.ErrorListener errorListener) {
        RequestString requestString = new RequestString(Request.Method.GET, url, null, listener, errorListener);
        VolleyHelper.getRequestQueue().add(requestString);
    }

    /**
     * 发送请求 post
     */
    private static void postRequest(String url, Response.Listener listener, Response.ErrorListener errorListener) {
        RequestString requestString = new RequestString(Request.Method.POST, url, null, listener, errorListener);
        VolleyHelper.getRequestQueue().add(requestString);
    }

    /**
     * 检查统计数据库是否为空，即是否有未发送出去的统计
     */
    private boolean isEmptyDB() {
        try {
            return statisticDao.isEmpty();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return false;
    }

}
