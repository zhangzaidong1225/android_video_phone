package com.ifeng.newvideo.statistics.smart.domains;

import android.content.Context;

/**
 * Created by fanshell on 2016/10/21.
 */
public class UserOperator extends SmartBaseModel {
    private String operation;
    private String type;
    private String act;
    private String duration;
    private String keyword;
    private String title;
    public UserOperator(Context context){
        this(context,"","","","","","");
    }
    public UserOperator(Context context, String operation, String type, String act, String duration, String keyword, String title) {
        super(context);
        this.operation = operation == null ? "" : operation;
        this.type = type == null ? "" : type;
        this.act = act == null ? "" : act;
        this.duration = duration == null ? "" : duration;
        this.keyword = keyword == null ? "" : keyword;
        this.title = title == null ? "" : title;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "UserOperator{" +
                "operation='" + operation + '\'' +
                ", type='" + type + '\'' +
                ", act='" + act + '\'' +
                ", duration='" + duration + '\'' +
                ", keyword='" + keyword + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
