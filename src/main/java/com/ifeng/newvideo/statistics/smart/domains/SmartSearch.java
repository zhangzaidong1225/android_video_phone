package com.ifeng.newvideo.statistics.smart.domains;

import android.content.Context;

/**
 * 搜索智能推荐实体类
 * Created by Administrator on 2016/10/20.
 */
public class SmartSearch extends SmartBaseModel {

    private String keyword;

    public SmartSearch(Context context, String keyword) {
        super(context);
        this.keyword = keyword == null ? "" : keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return "Search{" +
                "keyword='" + keyword + '\'' +
                '}';
    }
}
