package com.ifeng.newvideo.statistics.smart.domains;

/**
 * 智能推荐统计常量
 * Created by fanshell on 2016/10/21.
 */
public class UserOperatorConst {

    public static final String DATA_TYPE = "videoapp";

    public static final String OPERATION_SHARE = "share";
    public static final String OPERATION_COLLECTION = "collection";
    public static final String OPERATION_SUBSCRIPTION = "subscription";

    public static final String OPERATION_ADDCHANNEL = "addChannel";
    public static final String OPERATION_COMMENT = "comment";
    public static final String OPERATION_PLAY = "play";
    public static final String OPERATION_DOWNLOAD = "download";


    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_LIANBO = "lianbo";
    public static final String TYPE_FOCUS = "focus";
    public static final String TYPE_CMPPTOPIC = "cmpptopic";
    public static final String TYPE_WEMEDIA = "weMedia";
    public static final String TYPE_LIVE = "live";
    public static final String TYPE_VRLIVE = "vrLive";
    public static final String TYPE_CHANNEL = "channel";
    public static final String TYPE_LIVEROOM = "liveRoom";
    public static final String TYPE_WEB = "web";

}
