package com.ifeng.newvideo.statistics.smart.domains;

/**
 * 网络变化实体类
 * Created by Administrator on 2016/10/24.
 */
public class NetChange extends SmartBaseModel {

    private String net;
    private String carrierName;

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    @Override
    public String toString() {
        return "NetChange{" +
                "net='" + net + '\'' +
                ", carrierName='" + carrierName + '\'' +
                '}';
    }
}
