package com.ifeng.newvideo.statistics.smart.domains;

/**
 * 详见智能统计文档 2.1.1 启动
 * <p>
 * userId : xxxxxxx
 * deviceId : xxxxxx
 * dataType : videoApp
 * mos : iPhone_10.0
 * screen : 1242x2208
 * publishid : 20002
 * net : wifi/2G/3G/4G/none
 * gv : 7.0.1
 * district : 朝阳
 * city : 北京
 * province : 河北
 * country : 中国
 * longitude : xxx.xx
 * latitude : xx.xx
 * date : 20160801
 * device : iPhone 5/hm+2a
 * carrierName : cmcc
 * <p>
 * Created by android-dev on 2016/10/21 0021.
 */

public class SmartLaunch extends SmartBaseModel {

    private String mos;
    private String screen;
    private String publishid;
    private String net;
    private String gv;
    private String district;
    private String city;
    private String province;
    private String country;
    private String longitude;
    private String latitude;
    private String date;
    private String device;
    private String carrierName;

    public String getMos() {
        return mos;
    }

    public void setMos(String mos) {
        this.mos = mos;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getPublishid() {
        return publishid;
    }

    public void setPublishid(String publishid) {
        this.publishid = publishid;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getGv() {
        return gv;
    }

    public void setGv(String gv) {
        this.gv = gv;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    @Override
    public String toString() {
        return "SmartLaunch{" +
                ", mos='" + mos + '\'' +
                ", screen='" + screen + '\'' +
                ", publishid='" + publishid + '\'' +
                ", net='" + net + '\'' +
                ", gv='" + gv + '\'' +
                ", district='" + district + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", country='" + country + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", date='" + date + '\'' +
                ", device='" + device + '\'' +
                ", carrierName='" + carrierName + '\'' +
                '}';
    }
}
