package com.ifeng.newvideo.statistics.smart.domains;

/**
 * 发送智能统计响应类
 * Created by Administrator on 2016/10/21.
 */
public class SmartStatisticResponse {
    private boolean success;//是否成功，

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "SmartStatisticResponse{" +
                "success=" + success +
                '}';
    }
}
