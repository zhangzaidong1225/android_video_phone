package com.ifeng.newvideo.statistics.smart.domains;

import android.content.Context;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.utils.PhoneConfig;

/**
 * 智能统计基类实体类
 * Created by Administrator on 2016/10/21.
 */
public class SmartBaseModel {
    private String userId;//用户id
    private String dataType;//应用类型
    private String deviceId;//设备id

    public SmartBaseModel() {

    }

    public SmartBaseModel(Context context) {
        this.userId = new User(context).getUid();
        this.dataType = UserOperatorConst.DATA_TYPE;
        this.deviceId = PhoneConfig.userKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public String toString() {
        return "SmartBaseModel{" +
                "userId='" + userId + '\'' +
                ", dataType='" + dataType + '\'' +
                ", deviceId='" + deviceId + '\'' +
                '}';
    }
}
