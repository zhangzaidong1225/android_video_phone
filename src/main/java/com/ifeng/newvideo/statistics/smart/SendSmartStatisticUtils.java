package com.ifeng.newvideo.statistics.smart;

import android.content.Context;
import android.os.Build;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.smart.domains.SmartLaunch;
import com.ifeng.newvideo.statistics.smart.domains.UserOperator;
import com.ifeng.newvideo.statistics.smart.domains.UserOperatorConst;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.video.core.utils.NetUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 发送智能统计工具类
 * Created by Administrator on 2016/10/21.
 */
public class SendSmartStatisticUtils {

    /**
     * 发送自媒体操作统计
     *
     * @param keyword 搜索关键字
     */
    public static void sendWeMediaOperatorStatistics(Context context, boolean isSubscribe, String keyword, String title) {
        String act = isSubscribe ? "yes" : "no";
        UserOperator userOperator = new UserOperator(context, UserOperatorConst.OPERATION_SUBSCRIPTION, UserOperatorConst.TYPE_WEMEDIA, act, "", keyword, title);
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.OPERATION_URL, userOperator);
    }

    /**
     * 发送下载统计
     */
    public static void sendDownloadOperatorStatistics(Context context, String weMediaName, String title) {
        SmartStatistic.getInstance().sendSmartStatistic(
                SmartStatistic.OPERATION_URL,
                new UserOperator(context, UserOperatorConst.OPERATION_DOWNLOAD, UserOperatorConst.TYPE_VIDEO, "", "", weMediaName, title));
    }

    /**
     * 发送收藏统计
     */
    public static void sendCollectionOperatorStatistics(Context context, boolean isCollection, String weMediaName, String title) {
        String act = isCollection ? "yes" : "no";
        SmartStatistic.getInstance().sendSmartStatistic(
                SmartStatistic.OPERATION_URL,
                new UserOperator(context, UserOperatorConst.OPERATION_COLLECTION, UserOperatorConst.TYPE_VIDEO, act, "", weMediaName, title));
    }

    /**
     * 发送分享统计
     */
    public static void sendShareOperatorStatistics(Context context, String type, String weMediaName, String title) {
        SmartStatistic.getInstance().sendSmartStatistic(
                SmartStatistic.OPERATION_URL,
                new UserOperator(context, UserOperatorConst.OPERATION_SHARE, type, "", "", weMediaName, title));
    }

    /**
     * 发送频道订阅、编辑统计
     */
    public static void sendChannelOperatorStatistics(Context context, boolean isSub, String channelName) {
        String act = isSub ? "yes" : "no";
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.OPERATION_URL,
                new UserOperator(context, UserOperatorConst.OPERATION_ADDCHANNEL, UserOperatorConst.TYPE_CHANNEL, act, "", channelName, ""));
    }

    /**
     * 发送播放器统计
     */
    public static void sendPlayOperatorStatistics(Context context, String type, String duration, String weMediaName, String title) {
        SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.OPERATION_URL,
                new UserOperator(context, UserOperatorConst.OPERATION_PLAY, type, "", duration, weMediaName, title));
    }


    /**
     * 智能推荐统计 启动
     */
    public static void sendSmartLaunchStatistics() {
        try {
            SmartLaunch smartLaunch = new SmartLaunch();
            smartLaunch.setUserId(new User(IfengApplication.getInstance()).getUid());
            smartLaunch.setDeviceId(PhoneConfig.userKey);
            smartLaunch.setDataType(UserOperatorConst.DATA_TYPE);
            smartLaunch.setMos(PhoneConfig.mos);
            smartLaunch.setScreen(PhoneConfig.getScreenForSmartStatistics());
            smartLaunch.setPublishid(PhoneConfig.publishid);
            smartLaunch.setNet(NetUtils.getNetTypeForSmartStatistics(IfengApplication.getInstance()));
            smartLaunch.setGv(PhoneConfig.softversion);
            SharePreUtils sharePreUtils = SharePreUtils.getInstance();
            smartLaunch.setDistrict(sharePreUtils.getDistrict());
            smartLaunch.setCity(sharePreUtils.getCity());
            smartLaunch.setProvince(sharePreUtils.getProvince());
            smartLaunch.setCountry(sharePreUtils.getCountry());
            smartLaunch.setLongitude(sharePreUtils.getLogAndLatForSmartStatistics(sharePreUtils.getLongitude()));
            smartLaunch.setLatitude(sharePreUtils.getLogAndLatForSmartStatistics(sharePreUtils.getLatitude()));
            smartLaunch.setDate(new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date()));
            smartLaunch.setDevice(Build.MODEL);
            smartLaunch.setCarrierName(NetUtils.getNetName(IfengApplication.getInstance()));

            SmartStatistic.getInstance().sendSmartStatistic(SmartStatistic.LANNCH_URL, smartLaunch);
        } catch (Exception e) {
            // logger.error("sendSmartLaunchStatistics error ! {}", e);
        }
    }
}
