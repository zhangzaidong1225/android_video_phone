package com.ifeng.newvideo.statistics.smart;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.statistics.smart.domains.SmartStatisticResponse;
import com.ifeng.video.core.net.JsonPostRequest;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.ListUtils;
import com.ifeng.video.dao.db.dao.SmartStatisticDAO;
import com.ifeng.video.dao.db.model.SmartStatisticModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 智能统计
 * Created by Administrator on 2016/10/20.
 */
public class SmartStatistic {
    private static final Logger logger = LoggerFactory.getLogger(SmartStatistic.class);
    private static final String URL = "http://vcis.ifeng.com/api/submitAction?";
    private static final String PROTOCOL = "&protocol=1.0.0";

    //2.1.1 客户端激活上报接口
    public static final String LANNCH_URL = URL + "type=launch" + PROTOCOL;
    //2.1.2 用户操作
    public static final String OPERATION_URL = URL + "type=operation" + PROTOCOL;
    //2.1.3 状态切换（网络切换）
    public static final String CHANGE_URL = URL + "type=change" + PROTOCOL;
    //2.1.4 搜索
    public static final String SEARCH_URL = URL + "type=search" + PROTOCOL;
    //2.1.5 获取用户推荐数据
    public static final String USER_RECOMMEND_URL = URL + "type=" + PROTOCOL;//该URL未确定

    private volatile static SmartStatistic instance;


    private SmartStatistic() {
    }

    public static SmartStatistic getInstance() {
        if (instance == null) {
            synchronized (SmartStatistic.class) {
                if (instance == null)
                    instance = new SmartStatistic();
            }
        }
        return instance;
    }

    /**
     * 发送智能统计
     *
     * @param url    统计URL
     * @param object 统计实体类
     */
    public void sendSmartStatistic(String url, Object object) {
        String jsonString = com.alibaba.fastjson.JSONObject.toJSONString(object);
        logger.debug("url:{},param:{}", url, jsonString);
        sendSmartStatistic(url, jsonString);
    }

    /**
     * 发送智能统计
     *
     * @param url        统计URL
     * @param jsonString 统计json请求体
     */
    private void sendSmartStatistic(final String url, final String jsonString) {
        JsonRequest jsonRequest = new JsonPostRequest<>(Request.Method.POST, url, jsonString, SmartStatisticResponse.class,
                new Response.Listener<SmartStatisticResponse>() {
                    @Override
                    public void onResponse(SmartStatisticResponse response) {
                        logger.debug("sendSmartStatistic() response={}", response);
                        if (!isDBEmpty()) {//如果数据库中有未发送的记录
                            new Thread(new SendDBStatisticRecord()).start();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("sendSmartStatistic() error={}", error);
                        if (!isDBEmpty()) {//如果数据库中有未发送的记录
                            new Thread(new SendDBStatisticRecord()).start();
                        }
                        //如果发送统计失败，则保存到数据库中以便下次发送
                        SmartStatisticModel model = new SmartStatisticModel();
                        model.setPostUrl(url);
                        model.setRequestBody(jsonString);
                        getSmartStatisticDAO().saveStatisticData(model);
                    }
                });
        VolleyHelper.getRequestQueue().add(jsonRequest);
    }

    /**
     * 使用线程发送统计
     */
    private class SendDBStatisticRecord implements Runnable {

        @Override
        public void run() {
            synchronized (SendDBStatisticRecord.class) {
                List<SmartStatisticModel> records = getDBRecords();
                if (!ListUtils.isEmpty(records)) {
                    for (SmartStatisticModel model : records) {
                        sendDBRecord(model);
                    }
                }
            }
        }
    }

    /**
     * 发送数据库中未发送的记录
     */
    private void sendDBRecord(final SmartStatisticModel model) {
        JsonRequest jsonRequest = new JsonPostRequest<>(Request.Method.POST, model.getPostUrl(), model.getRequestBody(), SmartStatisticResponse.class,
                new Response.Listener<SmartStatisticResponse>() {
                    @Override
                    public void onResponse(SmartStatisticResponse response) {
                        logger.debug("sendDBRecord()  response={}", response);
                        try {//发送成功则删除数据库相应数据
                            SmartStatisticDAO.getInstance(IfengApplication.getAppContext()).delete(model.getId());
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("sendDBRecord()  error={}", error);
                    }
                });
        VolleyHelper.getRequestQueue().add(jsonRequest);
    }

    /**
     * 数据库中是否有未发送的统计记录
     */
    private boolean isDBEmpty() {
        try {
            return getSmartStatisticDAO().isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取数据库中未发送的记录
     */
    private List<SmartStatisticModel> getDBRecords() {
        try {
            return getSmartStatisticDAO().getAllStatisticData();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return null;
    }

    private SmartStatisticDAO getSmartStatisticDAO() {
        return SmartStatisticDAO.getInstance(IfengApplication.getAppContext());
    }
}
