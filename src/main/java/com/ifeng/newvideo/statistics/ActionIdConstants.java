package com.ifeng.newvideo.statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 按钮点击id
 * Created by hu on 2014/10/14.
 */
public class ActionIdConstants {

    private static final Logger logger = LoggerFactory.getLogger(ActionIdConstants.class);

    public static final String ACT_CANCEL = "cancel";
    public static final String ACT_YES = "yes";
    public static final String ACT_NO = "no";
    public static final String ACT_EMPTY = "";

    // 所有页面打开分享
    public static final String CLICK_BACK = "back";
    // 所有页面打开分享
    public static final String CLICK_COPY = "copy";
    // 所有页面打开分享
    public static final String CLICK_SHARE = "share";
    //截屏
    public static final String SNAPSHOT = "snapshot";
    // 首页	搜索框	搜索框点击事件
    public static final String CLICK_SEARCH_HOME = "search_entry";

    public static final String CLICK_APP_NEWS_INSTALL = "app_news_install";

    public static final String ACTION_PULL_SCROLL_UP = "action.pull.scroll.up";
    public static final String ACTION_PULL_SCROLL_DOWN = "action.pull.scroll.down";

    public static final String AUDIO_NET_3G = "AUDIO_NET_3G";
    public static final String CLICK_UPGRADE = "new_ver";
    /**
     * 7.2.0 action统计
     */
    public static final String CLICK_SPLASH = "launch";
    public static final String CLICK_SPLASH_JUMP_AD = "jump_launch_ad";
    // 底部导航	首页	点击首页事件
    public static final String CLICK_HOME = "home";
    // 直播	点击直播事件
    public static final String CLICK_LIVE = "live";
    // 订阅	点击订阅事件
    public static final String CLICK_SUB = "sub";
    // 我的	点击我的事件
    public static final String CLICK_MINE = "mine";
    // 新闻	点击新闻事件
    public static final String CLICK_NEWS = "news";

    // 搜索内容点击事件
    public static final String CLICK_SEARCH_ITEM = "search_item_click";
    // 首页内容点击事件
    public static final String CLICK_HOME_ITEM = "home_item_click";

    // 频道导航条	各频道切换点击事件 （xx为频道ID）
    public static final String CLICK_HOME_CHANNEL = "ch_nav";
    // 频道订阅入口	频道订阅入口点击事件
    public static final String CLICK_HOME_CH_ADD = "ch_add";
    // 各频道下拉刷新次数 （xx为频道ID）
    public static final String PULL_REFRESH = "pull_refresh";
    // 频道页	各频道上拉刷新事件 （xx为频道ID）
    public static final String PULL_MORE = "pull_more";
    //音频悬浮入口
    public static final String CLICK_AUDIO_ENTRY = "audio_entry";
    //凤凰卫视引导按钮
    public static final String CLICK_TV_GUIDE = "tv_guide";
    //凤凰卫视-栏目选择点击
    public static final String CLICK_TV_ITEM = "tv_item";
    //凤凰卫视-查看全部栏目
    public static final String CLICK_TV_ALL = "tv_all";
    //凤凰卫视-轮播图点击(XX,第几个图）
    public static final String CLICK_FOCUS = "focus_%s";
    //fm电台-相声小品更多按钮
    public static final String CLICK_FM_MORE = "fm_more";
    //大图频道-自媒体图标和文字点击跳自媒体详情页
    public static final String CLICK_WEMEDIA = "wemedia";
    //大图频道-评论按钮点击
    public static final String CLICK_COMMENT_ICON = "comment_icon";


    //频道编辑页 "X"关闭按钮
    public static final String CLICK_CH_CLOSE = "ch_close";
    //编辑按钮
    public static final String CLICK_CH_EDIT = "ch_edit";
    //频道编辑页 更多按钮
    public static final String CLICK_CH_MORE = "ch_more";
    //频道编辑页 我的频道的item点击编辑状态/非编辑状态 yes/no
    public static final String CLICK_CH_MY_ITEM = "ch_my_item";
    //频道编辑页 我的频道的item长按拖动
    public static final String DRAG_CH_MY_ITEM = "ch_my_item_drag";
    //频道编辑页 推荐频道的item点击/长按拖动
    public static final String CLICK_CH_REC_ITEM = "ch_rec_item";

    //直播tab 电视台item点击
    public static final String CLICK_LIVE_TV_ITEM = "live_tv_item";
    //直播tab 节目单按钮
    public static final String CLICK_LIVE_EPG = "live_epg";
    //直播tab 节目单关闭按钮
    public static final String CLICK_LIVE_EPG_CLOSE = "live_epg_close";

    //订阅tab 点击登录按钮
    public static final String CLICK_SUB_LOGIN = "sub_login";
    //订阅tab 订阅更新的item点击
    public static final String CLICK_SUB_ITEM = "sub_item";
    //订阅tab 查看全部订阅
    public static final String CLICK_SUB_ALL = "sub_all";
    //订阅tab 查看自媒体按钮
    public static final String CLICK_SUB_MEDIA = "sub_media";
    //订阅tab 凤凰卫视-全部按钮
    public static final String CLICK_SUB_IFENG_ALL = "sub_ifeng_all";
    //我的订阅 全部入口
    public static final String CLICK_SUB_WEMEDIA_ALL = "sub_look_all";
    //订阅tab 凤凰卫视item点击
    public static final String CLICK_SUB_IFENG_ITEM = "sub_ifeng_item";
    //订阅tab 推荐-更多按钮
    public static final String CLICK_SUB_REC_MORE = "sub_rec_more";
    //订阅tab 推荐-自媒体标题和头像的item
    public static final String CLICK_SUB_REC_WEMEDIA = "sub_rec_wemedia";
    //订阅tab 推荐-自媒体下视频item
    public static final String CLICK_SUB_REC_ITEM = "sub_rec_wemedia_item";
    //自媒体分类导航条 (XX:分类Id)
    public static final String CLICK_WEMEDIA_CID = "wemedia_cid_%s";
    //自媒体详情 排序最新
    public static final String CLICK_WEMEDIA_NEW = "wemedia_sort_new";
    //自媒体详情 排序最热
    public static final String CLICK_WEMEDIA_HOT = "wemedia_sort_hot";
    // 所有页面订阅按钮（订阅yes/取消no）
    public static final String CLICK_WEMEDIA_SUB = "wemedia_sub";


    //我的tab 已登录/未登录头像点击
    public static final String CLICK_MY_AVATAR = "my_avatar";
    //我的tab 头像下的登录按钮
    public static final String CLICK_MY_LOGIN_TEXT = "my_login_text";
    //我的tab 看过的点击
    public static final String CLICK_MY_HISTORY = "my_history";
    //我的tab 缓存的点击
    public static final String CLICK_MY_CACHE = "my_cache";
    //我的tab 收藏的点击
    public static final String CLICK_MY_FAVORITE = "my_favorite";
    //我的tab 消息
    public static final String CLICK_MY_MSG = "my_message";
    //查看更多消息
    public static final String CLICK_MSG_LOOK_MORE = "my_message_look_more";
    //查看具体消息
    public static final String CLICK_MSG_ITEM = "my_message_look";
    //我的tab 反馈的点击
    public static final String CLICK_MY_FEEDBACK = "feedback";
    //我的tab 我的订阅
    public static final String CLICK_MY_SUB = "my_sub";
    //我的tab 我的订阅下登录按钮
    public static final String CLICK_MY_SUB_LOGIN = "my_sub_login";
    //我的tab 我的订阅下的item点击
    public static final String CLICK_MY_SUB_ITEM = "my_sub_item";
    //我的tab 戳我领红包/意见反馈等 (XX为接口返回配置的第x个）
    public static final String CLICK_MY_H5 = "my_h5_";
    //我的tab 我来打分
    public static final String CLICK_MY_MARK = "my_mark";
    //我的tab 设置
    public static final String CLICK_MY_SETTING = "my_setting";
    //我的tab 凤凰视频会员
    public static final String CLICK_MY_IFENG_VIP = "my_vip";
    //我的tab 吐槽反馈提交
    public static final String CLICK_FEEDBACK_SUBMIT = "feedback_submit";
    //我的tab 吐槽反馈QQ群拉起
    public static final String CLICK_QQGROP = "qqgroup";
    //我的tab 视频tab
    public static final String CLICK_TAB_VIDEO = "tab_video";
    //我的tab 音频tab
    public static final String CLICK_TAB_AUDIO = "tab_audio";
    //我的tab 完成
    public static final String CLICK_MY_EDIT = "my_edit";
    //我的tab 编辑
    public static final String CLICK_MY_EDIT_FINISH = "my_edit_finish";
    //我的tab 选择或取消item
    public static final String CLICK_MY_CHOOSE = "my_choose";
    //我的tab 清空
    public static final String CLICK_MY_CLEAR = "my_clear";
    //我的tab 删除
    public static final String CLICK_MY_DELETE = "my_delete";
    //我的tab 正在缓存页-全部开始
    public static final String CLICK_CACHE_START_ALL = "cache_start_all";
    //我的tab 正在缓存页-全部暂停
    public static final String CLICK_CACHE_PAUSE_ALL = "cache_pause_all";

    //我的tab 允许运营商网络缓存开启/取消
    public static final String CLICK_SET_MOBILE_CACHE = "set_mobile_cache";
    //我的tab 视频缓存路径
    public static final String CLICK_SET_CACHE_PATH = "set_cache_path";
    //我的tab 消息推送
    public static final String CLICK_SET_PUSH = "set_push";
    //我的tab 清除软件缓存
    public static final String CLICK_SET_CACHE_CLEAR = "set_cache_clear";
    //我的tab 检测新版本
    public static final String CLICK_SET_CHECK_VER = "set_check_ver";
    //我的tab 关于
    public static final String CLICK_ABOUT = "about";
    //我的tab 退出登录
    public static final String CLICK_LOGOUT = "logout";
    //我的tab 关于页-欢迎页按钮
    public static final String CLICK_ABOUT_WELCOME = "about_welcome";


    //注册按钮
    public static final String CLICK_LOGIN_REG = "login_reg";
    //微信登录
    public static final String CLICK_LOGIN_WX = "login_wx";
    //QQ登录
    public static final String CLICK_LOGIN_QQ = "login_qq";
    //新浪登录
    public static final String CLICK_LOGIN_SINA = "login_sina";
    //账号输入(登录、注册）
    public static final String CLICK_ACCOUNT_EDIT = "account_edit";
    // 账号输入删除按钮
    public static final String CLICK_ACCOUNT_CLEAR = "account_clear";
    //密码输入
    public static final String CLICK_PWD_EDIT = "pwd_edit";
    //密码输入删除按钮
    public static final String CLICK_PWD_CLEAR = "pwd_clear";
    //验证码输入（登录、注册、手机号验证）
    public static final String CLICK_VERIFY_EDIT = "verify_edit";
    //验证码删除按钮
    public static final String CLICK_VERIFY_CLEAR = "verify_clear";
    //验证码刷新按钮
    public static final String CLICK_VERIFY_REFRESH = "verify_refresh";
    //登录-忘记密码
    public static final String CLICK_PWD_FORGET = "pwd_forget";
    //输入账号密码后登录按钮
    public static final String CLICK_LOGIN_ENTER = "login_enter";

    //同意协议按钮yes/no
    public static final String CLICK_REG_PROTOCOL = "reg_protocol";
    //协议名称点击
    public static final String CLICK_REG_PROTOCOL_VIEW = "reg_protocol_view";
    // 《个人信息保护政策》协议点击
    public static final String CLICK_REG_PRIVACY_PROTOCOL_VIEW = "reg_privacy_protocol_view";
    //获取短信验证码
    public static final String CLICK_REG_GETSMS = "reg_getsms";
    //手机号验证-完成注册按钮
    public static final String CLICK_REG_FINISH = "reg_finish";

    //搜索编辑框
    public static final String CLICK_SEARCH_EDIT = "search_edit";
    //搜索编辑框清空X
    public static final String CLICK_SEARCH_CLEAR = "search_clear";
    //搜索按钮
    public static final String CLICK_SEARCH = "search";
    //搜索取消按钮
    public static final String CLICK_SEARCH_CANCEL = "search_cancel";

    //搜索历史词点击(XX，第X个历史记录)=
    public static final String CLICK_SEARCH_HIS = "search_his";
    //历史词清空按钮
    public static final String CLICK_SEARCH_HIS_CLEAR = "search_his_clear";

    //搜索推荐词点击(XX，第X个推荐词)
    public static final String CLICK_SEARCH_REC = "search_rec";

    //搜索结果页自媒体item XX(XX，第X个自媒体)
    public static final String CLICK_SEARCH_WEMEDIA = "search_wemedia_";
    //搜索结果页自媒体全部按钮
    public static final String CLICK_SEARCH_WEMEDIA_ALL = "search_wemedia_all";


    //点播页 标题右边简介展开点击
    public static final String CLICK_VIDEO_BRIEF = "video_brief";
    //点播页  评论按钮
    // public static final String CLICK_COMMENT_ICON = "comment_icon";
    //点播页 快捷分享微信
    public static final String CLICK_SHARE_WX_QUICK = "share_wx_quick";
    //点播页 快捷分享朋友圈
    public static final String CLICK_SHARE_WXF_QUICK = "share_wxf_quick";
    //点播页 快捷分享新浪
    public static final String CLICK_SHARE_SINA_QUICK = "share_sina_quick";
    //点播页 快捷分享QQ
    public static final String CLICK_SHARE_QQ_QUICK = "share_qq_quick";
    //点播页 自媒体名称头像行点击
    // public static final String CLICK_WEMEDIA = "wemedia";
    //点播页 展开更多相关视频按钮
    public static final String CLICK_PLAY_ITEM_MORE = "play_item_more";
    //点播页 评论区域回复
    public static final String CLICK_REPLY = "reply";
    //点播页 评论区域点赞按钮
    public static final String CLICK_PRAISE = "praise";
    //点播页 评论加载失败重试按钮
    public static final String CLICK_COMMENT_RETRY = "comment_retry";
    //点播页 底部评论框
    public static final String CLICK_COMMENT_INPUT = "comment_input";
    //点播页 评论发布按钮
    public static final String CLICK_COMMENT_SEND = "comment_send";
    //点播页 缓存
    public static final String CLICK_CACHE = "cache";
    //点播页 收藏点击事件
    public static final String CLICK_COLLECT = "collect";
    //fm选集
    public static final String CLICK_EPISODE = "episode";
    //fm详情
    public static final String CLICK_DETAIL = "detail";

    //广告静音按钮
    public static final String CLICK_MUTE = "mute";
    //广告查看详情按钮
    public static final String CLICK_PLAY_AD_DETAIL = "play_ad_detail";
    //广告时点击视频
    public static final String CLICK_PLAY_AD = "play_ad_click";
    //音频视频切换yes/no(音频yes,视频no)
    public static final String CLICK_A_V_SWITCH = "a_v_switch";
    //播放/暂停按钮
    public static final String CLICK_PLAY_PAUSE = "play_pause";
    //无网点击重试
    public static final String CLICK_PLAY_RETRY = "play_retry";
    //运营商网络继续播放按钮
    public static final String CLICK_PLAY_CONTINUE = "play_continue";
    //进度的拖动
    public static final String CLICK_PROGRESS_DRAG = "progress_drag";
    //全屏按钮点击
    public static final String CLICK_FULL_SCREEN = "full_screen";
    //全屏状态-上下划音量
    public static final String CLICK_VOLUME_DRAG = "volume_drag";
    //全屏状态-上下划亮度
    public static final String CLICK_BRIGHT_DRAG = "brightness_drag";
    //全屏状态-左右划进度（除直播）
    public static final String CLICK_PLAY_GESTURE_DRAG = "play_gesture_drag";
    //全屏状态-清晰度点击
    public static final String CLICK_DEF = "def";
    //全屏状态-侧栏清晰度item点击
    public static final String CLICK_DEF_ITEM = "def_item";
    //全屏状态-VR眼镜开关yes/no
    public static final String CLICK_VR_GLASS = "vr_glass";
    //全屏状态-VR感应开关yes/no
    public static final String CLICK_VR_MOVE = "vr_move";
    //播放器以下滑动返回
    public static final String CLICK_BACK_DRAG = "back_drag";


    //微信好友
    public static final String CLICK_SHARE_WX = "share_wx";
    // 朋友圈
    public static final String CLICK_SHARE_WXF = "share_wxf";
    //新浪微博
    public static final String CLICK_SHARE_SINA = "share_sina";
    //QQ
    public static final String CLICK_SHARE_QQ = "share_qq";
    //QQ空间
    public static final String CLICK_SHARE_QQZONE = "share_qqzone";
    //支付宝
    public static final String CLICK_SHARE_ALIPAY = "share_alipay";
    //复制链接
    public static final String CLICK_SHARE_COPY = CLICK_COPY;
    //浏览器
    public static final String CLICK_SHARE_BROWSER = "h5_browser";
    //刷新
    public static final String CLICK_SHARE_REFRESH = "h5_refresh";
    //取消
    public static final String CLICK_SHARE_CANCEL = "h5_cancel";
    //h5 ...more
    public static final String CLICK_SHARE_MORE = "h5_more";
    //sina分享页 发布按钮
    public static final String CLICK_SINA_SEND = "sina_send";
    //sina分享页 发布按钮
    public static final String CLICK_SINA_PLATFORM = "sina_platform";

    //拉起新闻客户端 ,应用内列表,cell点击跳转 （打开yes/引用内跳转商店页no）
    public static final String CLICK_JUMP_NEWS_APP = "jumpNewsApp";

    public static final String NEWS_SILENCE_DOWNLOAD_COMPLETE = "news_silence_download_complete";
    public static final String NEWS_SILENCE_INSTALL = "news_silence_install";

    //直播页面 预约
    public static final String LIVE_SET_ALARM = "lorder";
    //直播页面 取消预约
    public static final String LIVE_CANCEL_ALARM = "lunorder";
    //直播页面 滚屏
    public static final String LIVE_UP_SCREEN = "upscreen";
    //卫视入口
    public static final String LIVE_TV = "live_tv_click";

    //视频会员 点击立即续费
    public static final String CLICK_VIP_CENTER_RENEW_VIP = "my_vip_continue_now";
    //视频会员 会员购买/续费页面右下角确认支付
    public static final String CLICK_VIP_CENTER_CONFIRM_PAY = "my_vip_buy";
    //视频会员 会员购买/续费成功
    public static String CLICK_VIP_CENTER_BUY_VIP_SUCCESS = "my_vip_buy_suc_";

    //广告跳过按钮
    public static String CLICK_AD_SKIP = "jump_video_ad";
    //我的积分点击
    public static String MY_CLICK_POINT = "my_point";
    //用户签到
    public static String MY_CLICK_SIGN_IN = "my_checkin";
    //点击查看积分说明
    public static String POINT_TASK_CLICK_GET_POINT = "my_get_point";
    //用户名点击
    public static String CLICK_USER_NAME = "my_nick_name";
    //弹幕发布按钮
    public static final String CLICK_DANMA_SEND = "barrage_send";
    //弹幕关闭
    public static final String CLICK_DANMA_CLOSED = "barrage_close";
    //弹幕开启
    public static final String CLICK_DANMA_OPEN = "barrage_open";
    //签到按钮
    public static final String CLICK_ONLINE_POINT = "my_sigin";
    //立即更新
    public static final String CLICK_NOW_UPDATE = "my_updata_version";

    //画中画 重试播放视频
    public static final String PIP_CLICK_PLAY_RETRY = "p_in_p_replay";
    //画中画 进入播放底页
    public static final String PIP_CLICK_TO_PLAY_DETAIL = "p_in_p_tap";
    //画中画 关闭
    public static final String PIP_CLICK_TO_CLOSE = "p_in_p_closed";
    //画中画 移动
    public static final String PIP_CLICK_TO_MOVE = "p_in_p_move";
    //画中画 播放设置
    public static final String PIP_SETTING_SWITCH = "picture_in_picture";
    //默认缓存清晰度
    public static final String DEFAULT_VIDEO_RESOLUTION = "alter_video_resolution";
    //RSA验证失败的统计
    public static final String RSA_CHECK_FAILED = "rsa_check_failed";
    //弹幕发送统计
    public static final String DANMA_SEND_REQUEST = "barrage_send_request";


}


