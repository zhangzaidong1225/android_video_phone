package com.ifeng.newvideo.statistics;

/**
 * 统计模块常量定义
 */
public class StatisticsConstants {


    /** ----------------------------------------------统计接口---------------------------------------------------- */
    /**
     * 自有统计接口
     */
    public static String STATISTIC = "http://stadig.ifeng.com/appsta.js";
    public static String STATISTIC_POST = "http://stadig0.ifeng.com/appsta.js";
    /**
     * 广告统计接口
     */
    public static String STATISTIC_FOR_AD = "http://stadig.ifeng.com/appap.js";
    /**
     * 播放Err统计接口
     */
    public static String STATISTIC_FOR_ERR = "http://stadig.ifeng.com/apperr.js";

    /**
     * 视频信息流广告上报接口
     */
    public static String STATISSTIC_VIDEO_AD = "http://ifengad.3g.ifeng.com/ad/video.php";


    /** ----------------------------------------------播放Err常量---------------------------------------------------- */
    /**
     * 采样标识（按播放次数1/10采样）
     */
    public static final String STATISTICS_ERR_SAMPLE = "100000";

    /**
     * Video request, 代码调用play(videoUrl)时发送
     */
    public static final String STATISTICS_ERR_TOUCH_PLAY_BUTTON = "208000";

    /**
     * 首画面呈现
     */
    public static final String STATISTICS_ERR_TOUCH_START_PLAY = "303000";

    /**
     * 缓冲一次(即卡)
     */
    public static final String STATISTICS_ERR_PLAY_STUCK_ONE = "304001";
    /**
     * 缓冲两次(即卡)
     */
    public static final String STATISTICS_ERR_PLAY_STUCK_TWO = "304001-304002";
    /**
     * 缓冲三次(即卡)
     */
    public static final String STATISTICS_ERR_PLAY_STUCK_THREE = "304001-304002-304003";
    /**
     * 缓冲四次或更多(即卡)
     */
    public static final String STATISTICS_ERR_PLAY_STUCK_FOUR_OR_MORE = "304001-304002-304003-304004";

    /**
     * Seek时的StreamNotFound
     */
    public static final String STATISTICS_ERR_SEEK_STREAM_NOT_FOUND = "301010";

    /**
     * 非Seek时的StreamNotFound
     */
    public static final String STATISTICS_ERR_STREAM_NOT_FOUND = "301020";

    /**
     * 实际视频流长度和xml长度不一致
     */
    public static final String STATISTICS_ERR_DUR_INCONSISTENCY = "301030";


    /** ----------------------------------------------推送常量---------------------------------------------------- */
    /**
     * 直播
     */
    public static final String PUSH_TYPE_LIVE = "live";
    /**
     * 专题
     */
    public static final String PUSH_TYPE_TOPIC = "topic";
    /**
     * 点播
     */
    public static final String PUSH_TYPE_VIDEO = "video";
    /**
     * 联播台
     */
    public static final String PUSH_TYPE_LIANBO = "lianbo";
    /**
     * 焦点台
     */
    public static final String PUSH_TYPE_FOCUS = "focus";
    /**
     * IMCP专题
     */
    public static final String PUSH_TYPE_IMCPTOPIC = "topic";
    /**
     * 互动直播间
     */
    public static final String PUSH_TYPE_LIVEROOM = "liveroom";
    /**
     * CMPP专题
     */
    public static final String PUSH_TYPE_CMPPTOPIC = "cmpptopic";
    /**
     * 焦点台
     */
    public static final String PUSH_TYPE_IFENGFOCUS = "ifengFocus";
    /**
     * VR LIVE
     */
    public static final String PUSH_TYPE_VRLIVE = "vrlive";


    /** ----------------------------------------------启动应用的打开方式---------------------------------------------------- */
    /**
     * 直接打开
     */
    public static final String APPSTART_TYPE_FROM_DIRECT = "direct";
    /**
     * 推送打开
     */
    public static final String APPSTART_TYPE_FROM_PUSH = "push";
    /**
     * 第三方应用打开
     */
    public static final String APPSTART_TYPE_FROM_OUTSIDE = "outside";

    /** ----------------------------------------------第三方id定义---------------------------------------------------- */
    /**
     * 新闻客户端
     */
    public static final String APPSTART_TYPE_FROM_NEWS = "newsapp";
    /**
     * H5，Html5页面
     */
    public static final String APPSTART_TYPE_FROM_H5 = "h5";
    /**
     * SF 手机凤凰网，“手凤”
     */
    public static final String APPSTART_TYPE_FROM_SF = "sf";
    /**
     * 微信、微信朋友圈
     */
    public static final String APPSTART_TYPE_FROM_WEIXIN = "weixin";
    /**
     * 腾讯微博
     */
    public static final String APPSTART_TYPE_FROM_TECENTWEIBO = "qqweibo";
    /**
     * 新浪微博
     */
    public static final String APPSTART_TYPE_FROM_SINAWEIBO = "weibo";
    /**
     * 人人网
     */
    public static final String APPSTART_TYPE_FROM_RENREN = "renren";
    /**
     * QQ
     */
    public static final String APPSTART_TYPE_FROM_QQ = "qq";
    /**
     * QQ空间
     */
    public static final String APPSTART_TYPE_FROM_QZONE = "qzone";
    /**
     * 支付宝朋友
     */
    public static final String APPSTART_TYPE_FROM_ALIPAY = "zfb";

}
