package com.ifeng.newvideo.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.ifeng.newvideo.member.OpenMemberActivity;
import com.ifeng.newvideo.statistics.ActionIdConstants;
import com.ifeng.newvideo.statistics.PageActionTracker;
import com.ifeng.newvideo.utils.UserPointManager;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * Created by ll on 2017/7/5.
 */
public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
    public static String ACTION_VIDEO_WX_PAY = "com.ifeng.video.ACTION_VIDEO_WX_PAY";
    public static final String PAY_RESULT_SUCCESS = "success";
    public static final String PAY_RESULT_CANCEL = "cancel";
    public static final String PAY_RESULT_ERROR = "error";
    private static final String TAG = WXPayEntryActivity.class.getSimpleName();
    private IWXAPI mApi;

    // 必须跟ShareSDK.xml文件中一致
    public static final String APP_ID = "wx0654b7781c27c62f";

    public static final String PAY_TYPE_WEI_XIN = "wei_xin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApi = WXAPIFactory.createWXAPI(this, APP_ID);
        mApi.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        mApi.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    /**
     * get callback of pay
     *
     * @param baseResp
     */
    @Override
    public void onResp(BaseResp baseResp) {
        if (baseResp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            int nErrorCode = baseResp.errCode;
            String payResult = PAY_RESULT_ERROR;
            Log.d(TAG, "onPayFinish, errCode = " + nErrorCode);// 支付结果码
            // ToastUtils.getInstance().showShortToast("支付结果码 " + nErrorCode);

            switch (nErrorCode) {
                case BaseResp.ErrCode.ERR_OK:
                    //成功:展示成功页面
                    payResult = PAY_RESULT_SUCCESS;
                    UserPointManager.resetUserPointStatusForVip();
                    PageActionTracker.clickBtn(ActionIdConstants.CLICK_VIP_CENTER_BUY_VIP_SUCCESS + OpenMemberActivity.mSelectedPrice, "");
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    payResult = PAY_RESULT_CANCEL;
                    break;
                case BaseResp.ErrCode.ERR_COMM://错误： 可能的原因：签名错误、未注册APPID、项目设置APPID
                    // 不正确、注册的APPID与设置的不匹配、其他异常等。
                    payResult = PAY_RESULT_ERROR;
                    break;
                default:
                    //用户取消: 无需处理。发生场景：用户不支付了，点击取消，返
                    // 回APP。
                    break;
            }
            Intent intent = new Intent(WXPayEntryActivity.ACTION_VIDEO_WX_PAY);
            intent.putExtra("pay_type", PAY_TYPE_WEI_XIN);
            intent.putExtra("pay_result", payResult);
            sendBroadcast(intent);

            this.finish();
        }
    }
}
