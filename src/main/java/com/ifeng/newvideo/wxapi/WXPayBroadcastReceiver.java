package com.ifeng.newvideo.wxapi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.member.OpenMemberActivity;
import com.ifeng.video.core.utils.ToastUtils;

import static com.ifeng.newvideo.wxapi.WXPayEntryActivity.PAY_RESULT_CANCEL;
import static com.ifeng.newvideo.wxapi.WXPayEntryActivity.PAY_RESULT_ERROR;
import static com.ifeng.newvideo.wxapi.WXPayEntryActivity.PAY_RESULT_SUCCESS;

/**
 * Created by ll on 2017/7/5.
 */
public class WXPayBroadcastReceiver extends BroadcastReceiver {
    public WXPayBroadcastReceiver() {
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        String msg = "";
        if (intent.getStringExtra("pay_result").equals(PAY_RESULT_SUCCESS)) {
            msg = context.getResources().getString(R.string.pay_for_msg_by_weixin_msg_ok);
            User.updateUserVipInfo(context);
            if (context instanceof OpenMemberActivity) {
                ((OpenMemberActivity) context).finish();
            }

        } else if (intent.getStringExtra("pay_result").equals(PAY_RESULT_CANCEL)) {
            msg = context.getResources().getString(R.string.pay_for_msg_by_weixin_msg_cancel);
        } else if (intent.getStringExtra("pay_result").equals(PAY_RESULT_ERROR)) {
            msg = context.getResources().getString(R.string.pay_for_msg_by_weixin_msg_error);
        }
        ToastUtils.getInstance().showShortToast(String.format(context.getResources().getString(R.string.pay_for_msg_by_weixin_msg), msg));
    }
}
