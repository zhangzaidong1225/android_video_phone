package com.ifeng.newvideo.dlna;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifeng.newvideo.R;

import java.util.List;


/**
 * 选集列表
 */
public class AdapterSelection extends BaseAdapter {

    private final DLNABasePlayerActivity activity;
    private final List<String> data;
    private int index;
    private TextView tvPopChannelListItem;

    public AdapterSelection(DLNABasePlayerActivity activity, List<String> data) {
        this.activity = activity;
        this.data = data;
    }

    /**
     * 当前播放
     *
     * @param index
     */
    public void setPlayIndex(int index) {
        if(index >= data.size()){
            index -= data.size();
        }
        this.index = index;
        notifyDataSetChanged();
    }

    public int getPlayIndex(){
        return index;
    }

    @Override
    public int getCount() {
        if(data == null){
            return 0;
        }
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.common_pop_channel_list_item, null);
            tvPopChannelListItem = (TextView) convertView.findViewById(R.id.pop_channel_list_item_tv);
            convertView.setTag(tvPopChannelListItem);
        } else {
            tvPopChannelListItem = (TextView) convertView.getTag();
        }
        tvPopChannelListItem.setText(data.get(position));
        tvPopChannelListItem.setTextColor(Color.WHITE);
        if (index == position)
            tvPopChannelListItem.setTextColor(activity.getResources().getColor(R.color.live_channel_red));
        return convertView;
    }
}
