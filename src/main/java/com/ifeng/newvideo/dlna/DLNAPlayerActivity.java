package com.ifeng.newvideo.dlna;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * DLNA播放界面
 */
public class DLNAPlayerActivity extends DLNABasePlayerActivity {
    private static final Logger logger = LoggerFactory.getLogger(DLNAPlayerActivity.class);

    @Override
    protected void init() {
        super.init();
        adapterSelection = new AdapterSelection(this, getPlayerInfoName(playList));
        listViewPopChannel.setAdapter(adapterSelection);
        listViewPopChannel.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playIndex = position;
                playSelection();
                hidenControl();
            }
        });
    }


    @Override
    protected void initState() {
        adapterSelection.setPlayIndex(playIndex);
        super.initState();
    }

    private List<String> getPlayerInfoName(List<PlayerInfoModel> plList) {
        List<String> list = new ArrayList<String>();
        for (PlayerInfoModel playerInfoModel : plList) {
            list.add(playerInfoModel.getName());
        }
        return list;
    }

    @Override
    protected void playNext() {
        handler.removeMessages(DLNAConstants.FIRSTPLAY);
        removeChangeMessage();
        playIndex++;
        if (playIndex > playList.size() - 1) {
            playIndex = 0;
        }
        dlnaPlayerControl.firstPlay(getCurUrl());
    }

    private void playSelection() {
        handler.removeMessages(DLNAConstants.FIRSTPLAY);
        removeChangeMessage();
        if (playIndex > playList.size() - 1) {
            playIndex = 0;
        }
        dlnaPlayerControl.firstPlay(getCurUrl());
    }

    @Override
    protected void playPre() {
        removeChangeMessage();
        playIndex--;
        if (playIndex < 0) {
            playIndex = playList.size() - 1;
        }
        dlnaPlayerControl.firstPlay(getCurUrl());
    }


}
