package com.ifeng.newvideo.dlna;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import com.ifeng.newvideo.videoplayer.widget.SeekBarVer;
import com.ifeng.video.core.utils.DateUtils;
import com.ifeng.video.core.utils.JsonUtils;
import com.ifeng.video.core.utils.StringUtils;
import com.ifeng.video.dao.db.constants.MediaConstants;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.cybergarage.upnp.ControlPoint;
import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.DeviceList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Executors;

/**
 * 播放页面基类，新增，将俩个页面共同部分统一一下
 * Created by cuihz on 2014/9/23.
 */
public class DLNABasePlayerActivity extends BaseFragmentActivity implements OnClickListener {

    private static final Logger logger = LoggerFactory.getLogger(DLNABasePlayerActivity.class);

    private static final int POSITION_ACTION_SEEK = 0;
    private static final int POSITION_ACTION_AUTO = 1;

    DLNAPlayerControl dlnaPlayerControl;

    private DLNADeviceManager dlnaDeviceManager;
    private ControlPoint mControlPoint;
    private String renderName;
    private Device mediaRenderDevice;

    AdapterSelection adapterSelection;
    private TextView tv_title;
    private TextView tv_current;
    private TextView tv_total;
    private SeekBar sb_progress;
    private FrameLayout fl_play_switch;
    private ImageView iv_pause;
    private ImageView iv_next;
    private SeekBarVer sb_voice;
    private FrameLayout fl_volume;
    private ImageView iv_back;
    private TextView tv_device;
    private ImageView iv_play;
    private LinearLayout right_layer_listview;
    ListView listViewPopChannel;
    private ImageView iv_mute;
    private ImageView iv_volume;

    private RelativeLayout lay_volume;
    private RelativeLayout rl_bottom;
    private Button btn_selection;
    private boolean isGoon;
    List<PlayerInfoModel> playList;
    int playIndex;
    private String totalLength = "00:00:00";
    private int total;
    private boolean isAutoPlay;
    private boolean isPlaying;
    private boolean isSelection;
    private String lastPosition = "00:00:00";
    private boolean isFast;
    private boolean isWifiEnable = true;
    private WifiStateReceiver receiver;
    private long preSeekTime; // 用于处理快速拖动进度，导致电视闪退
    private ScreenActionReceiver screenActionReceiver;
    private long lastFastTime;
    private long lastPlayTime;
    private boolean isQuit;
    private int currentVoice;
    private DLNAExecutorManager dlnaExecutorManager;
    private long startTime;
    private int stopCount;

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (isQuit) {
                return;
            }
            int what = msg.what;
            switch (what) {
                case DLNAConstants.UPDATE_CURRENT:
                    if (!isFast && isPlaying) {
                        removeChangeMessage();
                        dlnaPlayerControl.getPositionInfo(msg.arg1);
                        sendChangeMessage(POSITION_ACTION_AUTO);
                    }
                    break;

                case DLNAConstants.AUTO_PLAY:
                    if (isQuit) {
                        return;
                    }
                    if (isAutoPlay) {
                        autoPlay();
                    }
                    break;
                case DLNAConstants.GET_VOLUME_DB_RANGE:
                    String volumeDbRange = (String) msg.obj;
                    sb_voice.setMax(getMaxVolume(volumeDbRange));
                    break;
                case DLNAConstants.FIRSTPLAY:
                    if (isQuit) {
                        return;
                    }
                    initState();
                    isPlaying = true;
                    showPause();
                    sendChangeMessage(POSITION_ACTION_AUTO);
                    dlnaPlayerControl.getTransportState();
                    break;
                case DLNAConstants.GETVOICE:
                    int voice = (Integer) msg.obj;
                    sb_voice.setProgress(voice);
                    currentVoice = sb_voice.getProgress();
                    break;
                case DLNAConstants.GETMUTE:
                    String mute = (String) msg.obj;
                    initMuteImg(mute);
                    break;
                case DLNAConstants.SETMUTE:
                    boolean isSucces = (Boolean) msg.obj;
                    if (!isSucces) {
                        // 不成功
                        if (iv_mute.getVisibility() != View.VISIBLE) {
                            // 当前正常，要设置为静音
                            iv_mute.setVisibility(View.VISIBLE);
                            iv_volume.setVisibility(View.GONE);
                        } else {
                            iv_mute.setVisibility(View.GONE);
                            iv_volume.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                case DLNAConstants.GOON:
                    isPlaying = true;
                    sendChangeMessage(POSITION_ACTION_AUTO);
                    break;
                case DLNAConstants.PAUSE:
                    isPlaying = false;
                    removeChangeMessage();
                    break;
                case DLNAConstants.SEEK:
                    isFast = false;
                    if (isPlaying) {
                        sendChangeMessage(POSITION_ACTION_SEEK);
                    }
                    break;
                case DLNAConstants.GO_GET_STATE:
                    dlnaPlayerControl.getTransportState();
                    break;
                case DLNAConstants.GET_TRANSPORT_STATE:
                    String state = (String) msg.obj;
                    // 处理电视端停止后，手机端扔显示播放的问题
                    if (DLNAConstants.TRANSPORT_STATE_STOPPED.equals(state)) {
                        stopCount++;
                        if (stopCount > 2) {
                            initState();
                            showPlay();
                            handler.removeMessages(DLNAConstants.GO_GET_STATE);
                            handler.removeMessages(DLNAConstants.GET_TRANSPORT_STATE);
                            stopCount = 0;
                        }
                        return;
                    } else {
                        stopCount = 0;
                    }
                    if (isWifiEnable) showPause();

                    handler.sendEmptyMessageDelayed(DLNAConstants.GO_GET_STATE, 2000);

                    if (DLNAConstants.TRANSPORT_STATE_PAUSED_PLAYBACK.equals(state)) {
                        showPlay();
                    }
                    if (DLNAConstants.TRANSPORT_STATE_TRANSITIONING.equals(state)) {
                        dlnaPlayerControl.stop();
                    }
                    break;
                case DLNAConstants.GET_POSITION_INFO:
                    if (!isPlaying) {
                        return;
                    }

                    String s = (String) msg.obj;
                    if (s == null || DLNAConstants.POSITION_NOT_IMPLEMENTED.equals(s)) {
                        return;
                    }
                    // 开始的时候会出现一个空的值
                    if (TextUtils.isEmpty(s)) {
                        return;
                    }
                    // 新加，防止没播放起来，但是图片却一直显示扔在播放
                    if (lastPosition.equals(s) && isPlaying) {
                        // 一次了，如果出现
                        if (startTime == 0) startTime = System.currentTimeMillis();
                    } else {
                        startTime = 0;
                    }
                    int current = DateUtils.getTimeInSeconds(s);
                    if (msg.arg1 == POSITION_ACTION_AUTO) {
                        int change = current - DateUtils.getTimeInSeconds(lastPosition);
                        if (change > 20) {
                            return;
                        }
                    }
                    lastPosition = s;
                    if (startTime != 0 && (System.currentTimeMillis() - startTime) > 10000) {
                        startTime = 0;
                        showPlay();
                    }
                    sb_progress.setProgress(current);
                    if (current > total) {
                        return;
                    }
                    if (isAutoPlay) {
                        return;
                    }
                    if (current >= total - 3 && total > 0) {
                        if (isAutoPlay) {
                            return;
                        } else {
                            logger.debug("dlna 这条快播放完成，准备播放下一条");
                            isAutoPlay = true;
                            handler.removeMessages(DLNAConstants.AUTO_PLAY);
                            handler.sendEmptyMessageDelayed(DLNAConstants.AUTO_PLAY, (total - current) * 1000);
                        }
                    }
                    break;
            }
        }

    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dlna_activity_layout);
        findView();
        init();
        setUp();
    }

    private void findView() {
        tv_title = (TextView) findViewById(R.id.title);
        tv_current = (TextView) findViewById(R.id.tv_current);
        tv_total = (TextView) findViewById(R.id.tv_total);
        sb_progress = (SeekBar) findViewById(R.id.sb_progress);
        fl_play_switch = (FrameLayout) findViewById(R.id.fl_play_switch);
        iv_pause = (ImageView) findViewById(R.id.iv_pause);
        iv_play = (ImageView) findViewById(R.id.iv_play);
        lay_volume = (RelativeLayout) findViewById(R.id.lay_volume);
        rl_bottom = (RelativeLayout) findViewById(R.id.rl_bottom);
        iv_next = (ImageView) findViewById(R.id.iv_next);
        sb_voice = (SeekBarVer) findViewById(R.id.sb_voice);
        fl_volume = (FrameLayout) findViewById(R.id.fl_volume);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_device = (TextView) findViewById(R.id.tv_device);
        iv_mute = (ImageView) findViewById(R.id.iv_mute);
        iv_volume = (ImageView) findViewById(R.id.iv_volume);
        right_layer_listview = (LinearLayout) findViewById(R.id.right_layer_listView);
        btn_selection = (Button) findViewById(R.id.btn_selection);
        listViewPopChannel = (ListView) findViewById(R.id.listViewPopChannel);
    }


    void init() {
        setResult(DLNAConstants.GO_PLAY);
        dlnaExecutorManager = DLNAExecutorManager.getInstance();
        if (DLNAExecutorManager.executorService == null) {
            DLNAExecutorManager.executorService = Executors.newSingleThreadExecutor();
        }
        dlnaDeviceManager = DLNADeviceManager.getInstance();
        mControlPoint = dlnaExecutorManager.getControlPoint();
        if (mControlPoint == null) {
            mControlPoint = new ControlPoint();
        }
        playIndex = dlnaDeviceManager.playIndex;
        playList = dlnaDeviceManager.playList;
        renderName = dlnaDeviceManager.getRenderName();
        DeviceList deviceList = mControlPoint.getDeviceList();
        for (int i = 0; i < deviceList.size(); i++) {
            Device device = deviceList.getDevice(i);
            if (device.getDeviceType().equals(DLNAConstants.MEDIA_RENDER_DEVICE)) {
                if (device.getFriendlyName().equals(renderName)) {
                    mediaRenderDevice = device;
                    break;
                } else {
                    mediaRenderDevice = mControlPoint.getDevice(DLNAConstants.MEDIA_RENDER_DEVICE);
                }
            }
        }
        dlnaPlayerControl = new DLNAPlayerControl(dlnaExecutorManager, handler, mediaRenderDevice);
        registReceiver();
        //TODO
    }

    private void hideOrShowControl(boolean isHiden) {
        if (isHiden) {
            showControl();
        } else {
            hidenControl();
        }
    }

    private void showControl() {
        lay_volume.setVisibility(View.GONE);
        rl_bottom.setVisibility(View.GONE);
        btn_selection.setSelected(true);
        isSelection = true;
        right_layer_listview.setVisibility(View.VISIBLE);
        listViewPopChannel.setSelection(adapterSelection.getPlayIndex());
    }

    void hidenControl() {
        lay_volume.setVisibility(View.VISIBLE);
        rl_bottom.setVisibility(View.VISIBLE);
        btn_selection.setSelected(false);
        isSelection = false;
        right_layer_listview.setVisibility(View.GONE);
    }


    private void setUp() {
        sb_progress.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isAutoPlay = false;
                int progress = seekBar.getProgress();
                dlnaPlayerControl.seek(StringUtils.changeDuration(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                removeChangeMessage();
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tv_current.setText(getCurrentTime(StringUtils.changeDuration(progress)));
                if (fromUser) {
                    removeChangeMessage();
                    if (System.currentTimeMillis() - preSeekTime >= 1000) {
                        preSeekTime = System.currentTimeMillis();
                    }
                }
            }
        });

        dlnaPlayerControl.getVolumeDbRange();
        dlnaPlayerControl.getVoice();
        dlnaPlayerControl.getMute();

        sb_voice.setOnSeekBarChangeListener(new SeekBarVer.OnSeekBarChangeListenerVer() {
            @Override
            public void onStopTrackingTouch(SeekBarVer seekBar) {
                dlnaPlayerControl.setVoice(seekBar.getProgress());
                if (iv_mute.getVisibility() == View.VISIBLE) {
                    dlnaPlayerControl.setMute(DLNAConstants.VOICE_ON);
                }
                if (seekBar.getProgress() <= 1) {
                    dlnaPlayerControl.setMute(DLNAConstants.NO_VOICE);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBarVer VerticalSeekBar) {
            }

            @Override
            public void onProgressChanged(SeekBarVer seekBar, int progress, boolean fromUser) {
                // 声音为0的时候让其静音
                if (seekBar.getProgress() <= 1) {
                    iv_mute.setVisibility(View.VISIBLE);
                    iv_volume.setVisibility(View.GONE);
                    dlnaPlayerControl.setMute(DLNAConstants.NO_VOICE);
                }
                if (seekBar.getProgress() >= 2) {
                    iv_mute.setVisibility(View.GONE);
                    iv_volume.setVisibility(View.VISIBLE);
                }
            }
        });
        initState();
        fl_play_switch.setOnClickListener(this);
        iv_next.setOnClickListener(this);
        fl_volume.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        btn_selection.setOnClickListener(this);

        tv_device.setText(dlnaDeviceManager.getRenderName());
        btn_selection.setText(getString(R.string.common_video_selector));
        // 进来后就开始播放
        dlnaPlayerControl.firstPlay(getCurUrl());
    }

    /**
     * 状态初始化，在每次开始播放一个视频的时候都需要调用
     */
    void initState() {
        startTime = 0;
        isAutoPlay = false;
        listViewPopChannel.setSelection(adapterSelection.getPlayIndex());
        handler.removeMessages(DLNAConstants.AUTO_PLAY);
        tv_title.setText(getTitle(playList, playIndex));
        totalLength = getTotalLength(playList, playIndex);
        tv_current.setText(getCurrentTime("00:00:00"));
        if (totalLength != null) {
            total = DateUtils.getTimeInSeconds(totalLength);
            tv_total.setText(totalLength);
            sb_progress.setMax(total);
            sb_progress.setProgress(0);
        }
        showPlay();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_selection:
                hideOrShowControl(!isSelection);
                break;
            case R.id.iv_next:
                playNext();
                break;
            case R.id.fl_volume:
                setMute();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.fl_play_switch:
                if (System.currentTimeMillis() - lastPlayTime < 1000) {
                    break;
                }
                lastPlayTime = System.currentTimeMillis();
                // 点击的
                handler.removeMessages(DLNAConstants.GO_GET_STATE);
                if (iv_pause.getVisibility() != View.VISIBLE) {
                    // 在暂停状态的时候点了一下。 要开始播放了
                    showPause();
                    if (isGoon) {
                        isGoon = false;
                        // 续播
                        String pausePosition = tv_current.getText().toString();
                        if (pausePosition.length() == 5) {
                            pausePosition = "00:" + pausePosition;
                        }
                        dlnaPlayerControl.goon(pausePosition);
                    } else {
                        // 第一次播放
                        dlnaPlayerControl.firstPlay(getCurUrl());
                    }
                } else {
                    // 暂停
                    handler.removeMessages(DLNAConstants.GO_GET_STATE);
                    showPlay();
                    isGoon = true;
                    dlnaPlayerControl.pause();
                }
                break;
            default:
                break;
        }
    }

    private void setMute() {
        if (iv_mute.getVisibility() != View.VISIBLE) {
            // 当前正常，要设置为静音
            iv_mute.setVisibility(View.VISIBLE);
            iv_volume.setVisibility(View.GONE);
            currentVoice = sb_voice.getProgress();
            dlnaPlayerControl.setMute(DLNAConstants.NO_VOICE);
            sb_voice.setProgress(0);
        } else {
            iv_mute.setVisibility(View.GONE);
            iv_volume.setVisibility(View.VISIBLE);
            dlnaPlayerControl.setMute(DLNAConstants.VOICE_ON);
            sb_voice.setProgress(currentVoice);
            // getVoice();
        }
    }

    /**
     * 初始化静音图标
     */
    private void initMuteImg(String mute) {
        if (DLNAConstants.NO_VOICE.equals(mute)) {
            iv_mute.setVisibility(View.VISIBLE);
            iv_volume.setVisibility(View.GONE);
            sb_voice.setProgress(0);
        } else if (DLNAConstants.VOICE_ON.equals(mute)) {
            iv_mute.setVisibility(View.GONE);
            iv_volume.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 快进或快退
     *
     * @param isGo true表示快进，false为快退
     */
    private void fastGoOrBack(boolean isGo) {
        removeChangeMessage();
        if (System.currentTimeMillis() - lastFastTime < 500) {
            return;
        }
        lastFastTime = System.currentTimeMillis();
        isFast = true;
        String position = tv_current.getText().toString();
        int targetLength;
        if (isGo) {
            targetLength = DateUtils.getTimeInSeconds(position) + 10;
            if (targetLength > total) {
                targetLength = total;
            }
        } else {
            targetLength = DateUtils.getTimeInSeconds(position) - 10;
            isAutoPlay = false;
            if (targetLength < 0) {
                targetLength = 0;
            }
        }
        sb_progress.setProgress(targetLength);
        dlnaPlayerControl.seek(StringUtils.changeDuration(targetLength));
    }


    void playNext() {
        isPlaying = false;
        //TODO
    }

    protected void playPre() {
        //TODO
    }

    private void showPlay() {
        iv_pause.setVisibility(View.GONE);
        iv_play.setVisibility(View.VISIBLE);
    }

    private void showPause() {
        iv_pause.setVisibility(View.VISIBLE);
        iv_play.setVisibility(View.GONE);
    }

    private void autoPlay() {
        logger.debug("dlna 自动续播");
        playNext();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isQuit = true;
        unregisterReceiver(receiver);
        unregisterReceiver(screenActionReceiver);
    }

    String getCurUrl() {
        PlayerInfoModel playerInfoModel = getCurPlayProgram(playIndex, playList);
        return getAvailableHighVideoURL(playerInfoModel);
    }

    PlayerInfoModel getCurPlayProgram(int index, List<PlayerInfoModel> playList) {
        if (playList != null && playList.size() != 0) {
            return playList.get(index);
        } else {
            return null;
        }
    }

    // 暂时先不加本地视频的地址，用来进行缓存文件的dlna投放
    private String getAvailableHighVideoURL(PlayerInfoModel curProgram) {
        String url = null;
        // TODO 是否支持高清
        // if ((Boolean) dlnaExecutorManager
        // .getAttribute(IfengVideoApplication.PLAY_STREAM_MID)) {
        if (false) {
            url = curProgram.changeVideoURL(MediaConstants.STREAM_MID);
            if (!JsonUtils.checkDataInJSONObject(url)) {
                url = curProgram.changeVideoURL(MediaConstants.STREAM_LOW);
            }
        } else {
            url = curProgram.getAvailableHighVideoURL();
        }

        return url;
    }

    /**
     * 监控Wifi状态的广播接收器
     */
    protected final class WifiStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            Bundle bundle = intent.getExtras();
            int statusInt = bundle.getInt(WifiManager.EXTRA_WIFI_STATE);
            switch (statusInt) {
                case WifiManager.WIFI_STATE_ENABLED:
                    if (!isWifiEnable) {
                        isWifiEnable = true;
                        // 断网后又连上了
                        isGoon = false;
                        dlnaPlayerControl.firstPlay(getCurUrl());
                    }
                    break;
                case WifiManager.WIFI_STATE_DISABLED:
                    isWifiEnable = false;
                    showPlay();
                    break;
                default:
                    break;
            }
        }
    }


    private void registReceiver() {
        receiver = new WifiStateReceiver();
        IntentFilter filter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(receiver, filter);

        screenActionReceiver = new ScreenActionReceiver();
        IntentFilter scrrenFilter = new IntentFilter();
        scrrenFilter.addAction(Intent.ACTION_SCREEN_ON);
        registerReceiver(screenActionReceiver, scrrenFilter);
    }

    protected class ScreenActionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_SCREEN_ON)) {
                tv_title.setText(getTitle(playList, playIndex));
                totalLength = getTotalLength(playList, playIndex);
                if (totalLength != null) {
                    total = DateUtils.getTimeInSeconds(totalLength);
                    tv_total.setText(totalLength);
                }
            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }


    private void sendChangeMessage(int action) {
        Message message = Message.obtain();
        message.what = DLNAConstants.UPDATE_CURRENT;
        message.arg1 = action;
        handler.sendMessageDelayed(message, 1000);
    }

    void removeChangeMessage() {
        handler.removeMessages(DLNAConstants.UPDATE_CURRENT);
        handler.removeMessages(DLNAConstants.GET_POSITION_INFO);
    }

    private String getTitle(List<PlayerInfoModel> list, int index) {
        if (list != null && index < list.size()) {
            return list.get(index).getName();
        } else {
            return "";
        }
    }

    private String getTotalLength(List<PlayerInfoModel> list, int index) {
        if (list != null && index < list.size()) {
            return StringUtils.changeDuration(list.get(index).duration);
        }
        return "00:00:00";
    }

    /**
     * 格式为：min:max，如果获取失败的话就默认返回100
     */
    private int getMaxVolume(String volumeDbRange) {
        int max = 0;
        if (TextUtils.isEmpty(volumeDbRange)) {
            String[] split = volumeDbRange.split(":");
            try {
                max = Integer.parseInt(split[1]);
            } catch (NumberFormatException e) {
                logger.error(e.toString(), e);
            }
        }
        if (max <= 0) {
            max = 100;
        }
        return max;
    }

    /**
     * 视频长度不走一小时的就显示 分钟：秒 ，超过一小时的显示 小时：分钟：秒
     */
    private String getCurrentTime(String s) {
        if (totalLength.length() < s.length()) {
            return s.substring(s.length() - totalLength.length());
        } else {
            return s;
        }
    }

}
