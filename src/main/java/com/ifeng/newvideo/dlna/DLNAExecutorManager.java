package com.ifeng.newvideo.dlna;


import org.cybergarage.upnp.ControlPoint;

import java.util.concurrent.ExecutorService;

/**
 * 线程管理类，单例模式
 */
public class DLNAExecutorManager {

    private static DLNAExecutorManager dlnaExecutorManager;
    private ControlPoint mControlPoint;
    /**
     * 线程池，保证推出页面后能自动续播
     */
    public static ExecutorService executorService;

    private DLNAExecutorManager() {
    }

    public static DLNAExecutorManager getInstance() {
        if (dlnaExecutorManager == null) {
            dlnaExecutorManager = new DLNAExecutorManager();
        }
        return dlnaExecutorManager;
    }


    public ControlPoint getControlPoint() {
        return mControlPoint;
    }

    public void setControlPoint(ControlPoint mControlPoint) {
        this.mControlPoint = mControlPoint;
    }

}
