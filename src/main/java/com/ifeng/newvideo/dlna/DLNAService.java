package com.ifeng.newvideo.dlna;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.ifeng.video.core.utils.NetUtils;
import org.cybergarage.upnp.ControlPoint;
import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.device.DeviceChangeListener;
import org.cybergarage.upnp.ssdp.SSDPPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 设备搜索服务
 */
public class DLNAService extends Service implements DeviceChangeListener {

    private static final Logger logger = LoggerFactory.getLogger(DLNAService.class);

    public static final String SEARCH_DEVICES = "com.ifeng.video.search";
    public static final String RESET_SEARCH_DEVICES = "com.ifeng.video.reset";

    private static final int NETWORK_CHANGE = 1000;
    private boolean firstReceiveNetworkChangeBR = true;
    private NetworkStatusChangeBR mNetworkStatusChangeBR;

    private ControlPoint mControlPoint;
    private DLNASearchThread dlnaSearchThread;
    private DLNADeviceManager dlnaDeviceManager;
    private Handler mHandler;
    private DLNAExecutorManager dlnaExecutorManager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            if (dlnaSearchThread != null) {
                dlnaSearchThread.setCount(0);
            }
            if (DLNAService.SEARCH_DEVICES.equals(action)) {
                awakeSearchThread();
            } else if (DLNAService.RESET_SEARCH_DEVICES.equals(action)) {
                restartSearchThread();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        unInit();
        super.onDestroy();
    }


    /**
     * 初始化
     */
    private void init() {
        dlnaExecutorManager = DLNAExecutorManager.getInstance();
        dlnaDeviceManager = DLNADeviceManager.getInstance();
        mControlPoint = new ControlPoint();
        dlnaExecutorManager.setControlPoint(mControlPoint);
        mControlPoint.addDeviceChangeListener(this);
        mControlPoint.addSearchResponseListener(new SearchResponseListener());
        dlnaSearchThread = new DLNASearchThread(this, mControlPoint);
        mHandler = new Handler() {

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case NETWORK_CHANGE:
                        if (NetUtils.isNetAvailable(getApplicationContext())) {
                            dlnaDeviceManager.startSearch(DLNAService.this);
                        } else {
                            dlnaDeviceManager.resetSearch(DLNAService.this);
                        }
                        break;
                }
            }

        };
        registerNetworkStatusBR();
    }


    /**
     * 注销广播，停止线程等
     */
    private void unInit() {
        unRegisterNetworkStatusBR();
        dlnaExecutorManager.setControlPoint(null);
        dlnaSearchThread.exit();
    }


    /**
     * 重新搜索线程
     *
     * @return
     */
    private boolean restartSearchThread() {
        dlnaSearchThread.reset();
        return true;
    }


    @Override
    public void deviceAdded(Device dev) {
        dlnaDeviceManager.addDevice(dev);
    }


    @Override
    public void deviceRemoved(Device dev) {
        dlnaDeviceManager.removeDevice(dev);
    }


    /**
     * 唤醒搜索线程
     */
    private void awakeSearchThread() {
        if (dlnaSearchThread.isAlive()) {
            dlnaSearchThread.awakeThread();
        } else {
            dlnaSearchThread.start();
        }
    }

    /**
     * 停止搜索线程
     */
    private void exitSearchThread() {
        if (dlnaSearchThread != null && dlnaSearchThread.isAlive()) {
            dlnaSearchThread.exit();
            while (dlnaSearchThread.isAlive()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    logger.error(e.toString(), e);
                }
            }
            dlnaSearchThread = null;
        }
    }

    private static class SearchResponseListener implements org.cybergarage.upnp.device.SearchResponseListener {
        public void deviceSearchResponseReceived(SSDPPacket ssdpPacket) {
        }
    }

    private class NetworkStatusChangeBR extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (action != null) {
                    if (action.equalsIgnoreCase(ConnectivityManager.CONNECTIVITY_ACTION)) {
                        sendNetworkChangeMessage();
                    }
                }
            }
        }
    }


    /**
     * 注册网络状态改变的广播
     */
    private void registerNetworkStatusBR() {
        if (mNetworkStatusChangeBR == null) {
            mNetworkStatusChangeBR = new NetworkStatusChangeBR();
            registerReceiver(mNetworkStatusChangeBR, new IntentFilter(
                    ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }


    /**
     * 注销网络状态改变的广播
     */
    private void unRegisterNetworkStatusBR() {
        if (mNetworkStatusChangeBR != null) {
            unregisterReceiver(mNetworkStatusChangeBR);
        }
    }


    /**
     * 发送消息通知网络状态改变
     */
    private void sendNetworkChangeMessage() {
        if (firstReceiveNetworkChangeBR) {
            firstReceiveNetworkChangeBR = false;
            return;
        }
        mHandler.removeMessages(NETWORK_CHANGE);
        if (NetUtils.isNetAvailable(getApplicationContext())) {
            dlnaDeviceManager.exitSearch(DLNAService.this);
        }
        mHandler.sendEmptyMessageDelayed(NETWORK_CHANGE, 500);
    }
}
