package com.ifeng.newvideo.dlna;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.ifeng.newvideo.R;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.cybergarage.upnp.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * DLNA设备的操作类，单例模式，提供了获取、设置及增删设备的方法
 */
public class DLNADeviceManager {
    private List<Device> devices;
    private Device selectedDevice;
    private volatile  static DLNADeviceManager sharedDeviceOperator;
    private String renderName;
    public List<PlayerInfoModel> playList;
    public int playIndex;
    public Activity activity;
    private static final Logger logger = LoggerFactory.getLogger(DLNADeviceManager.class);

    private DLNADeviceManager() {
        devices = new ArrayList<Device>();
    }

    public static DLNADeviceManager getInstance() {
        if (sharedDeviceOperator == null) {
            synchronized (DLNADeviceManager.class) {
                if (sharedDeviceOperator == null) {
                    sharedDeviceOperator = new DLNADeviceManager();
                }
            }
        }
        return sharedDeviceOperator;
    }


    public boolean hasDevice() {
        logger.debug("DLNA devices = {}, size = {}", getDevicesName(), getDevices().size());
        return this.getDevices().size() > 0;
    }

    public String[] getDevicesName() {
        List<Device> deviceList = this.getDevices();
        String[] arr = new String[deviceList.size()];
        for (int j = 0; j < deviceList.size(); j++) {
            Device device = deviceList.get(j);
            arr[j] = device.getFriendlyName();
        }
        logger.debug("DLNAService getDevicesName {}", Arrays.toString(arr));
        return arr;
    }

    public void showDialog(Context context, String[] arr, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.dlna_select_device));
        builder.setItems(arr, onClickListener);
        builder.show();
    }

    public Device getSelectedDevice() {
        return selectedDevice;
    }

    /**
     * 添加设备
     *
     * @param device
     */
    public synchronized void addDevice(Device device) {
        if (!isMediaRenderDevice(device)) return;
        for (Device dev : devices) {
            String udnString = dev.getUDN();
            if (device.getUDN().equalsIgnoreCase(udnString)) {
                return;
            }
        }
        devices.add(device);
    }

    /**
     * 移除设备
     */
    public synchronized void removeDevice(Device device) {
        if (!isMediaRenderDevice(device)) {
            return;
        }
        int size = devices.size();
        for (int i = 0; i < size; i++) {
            String udnString = devices.get(i).getUDN();
            if (device.getUDN().equalsIgnoreCase(udnString)) {
                devices.remove(i);
                boolean ret = false;
                if (selectedDevice != null) {
                    ret = selectedDevice.getUDN().equalsIgnoreCase(device.getUDN());
                }
                if (ret) {
                    setSelectedDevice(null);
                }
                break;
            }
        }
    }

    /**
     * 设置选择的设备
     */
    private synchronized void setSelectedDevice(Device device) {
        selectedDevice = device;
    }

    /**
     * 获取设备列表
     */
    private synchronized List<Device> getDevices() {
        return devices;
    }

    public synchronized void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    private synchronized void clear() {
        devices.clear();
        selectedDevice = null;
    }

    /**
     * 获取连接的设备名
     */
    public String getRenderName() {
        return renderName;
    }

    /**
     * 设置要连接的设备名
     */
    public void setRenderName(String renderName) {
        this.renderName = renderName;
    }

    /**
     * 开始搜索
     */
    public void startSearch(Context context) {
        logger.debug("!!!!!!!! DLNAService  startSearch");
        IntentUtils.startService(context, new Intent(DLNAService.SEARCH_DEVICES));
    }

    /**
     * 重启搜索服务
     */
    public void resetSearch(Context context) {
        logger.debug("!!!!!!!! DLNAService  resetSearch");
        IntentUtils.startService(context, new Intent(DLNAService.RESET_SEARCH_DEVICES));
        clear();
    }

    /**
     * 停止搜索服务
     */
    public void exitSearch(Context context) {
        logger.debug("!!!!!!!! DLNAService  exitSearch");
        context.stopService(new Intent(context, DLNAService.class));
        clear();
    }

    /**
     * 是否为播放设备
     */
    private boolean isMediaRenderDevice(Device device) {
        return DLNAConstants.MEDIA_RENDER_DEVICE.equalsIgnoreCase(device.getDeviceType());
    }

}
