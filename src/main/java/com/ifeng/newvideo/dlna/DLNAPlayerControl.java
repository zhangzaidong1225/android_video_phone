package com.ifeng.newvideo.dlna;

import android.os.Handler;
import android.os.Message;
import org.cybergarage.upnp.Action;
import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DLNA控制类，包括播放控制及声音控制等
 * Created by cuihz on 2014/9/23.
 */
public class DLNAPlayerControl {

    private static final Logger logger = LoggerFactory.getLogger(DLNAPlayerControl.class);

    private final DLNAExecutorManager dlnaExecutorManager;
    private final Handler handler;
    private final Device mediaRenderDevice;

    public DLNAPlayerControl(DLNAExecutorManager dlnaExecutorManager1, Handler handler, Device device) {
        this.dlnaExecutorManager = dlnaExecutorManager1;
        this.handler = handler;
        this.mediaRenderDevice = device;
    }


    /**
     * 包含两部分，一个是设置播放地址，一个是开始播放
     */
    public void firstPlay(final String url) {
        logger.debug("开始播放，发包！");
        DLNAExecutorManager.executorService.execute(new Thread() {
            @Override
            public void run() {
                if (mediaRenderDevice == null) return;

                Service service = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);

                if (service == null) {
                    return;
                }

                final Action action = service.getAction(DLNAConstants.SET_AVTRANSPORT_URI);
                if (action == null) {
                    return;
                }
                final Action playAction = service.getAction(DLNAConstants.ACTION_PLAY);
                if (playAction == null) {
                    return;
                }
                if (url == null) {
                    return;
                }
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, 0);
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CURRENT_URI, url);
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CURRENT_URI_META_DATA, 0);

                boolean postControlAction = action.postControlAction();
                if (!postControlAction) {
                    // 播放失败
                }
                playAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, 0);
                playAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_SPEED, "1");
                boolean playPost = playAction.postControlAction();
                if (!playPost) {
                }
                handler.sendEmptyMessage(DLNAConstants.FIRSTPLAY);
            }
        });
    }

    /**
     * 继续播放，目前继续播放后会往回跳六七秒时间
     *
     * @param pausePosition 继续播放的位置，格式必须为00:00:00
     */
    public void goon(final String pausePosition) {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service localService = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);
                if (localService == null) return;

                final Action localAction = localService.getAction(DLNAConstants.ACTION_SEEK);
                if (localAction == null) return;
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                // 测试解决播放暂停后时间不准确
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_UNIT, "ABS_TIME");
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_TARGET, pausePosition);
                localAction.postControlAction();

                Action playAction = localService.getAction(DLNAConstants.ACTION_PLAY);
                if (playAction == null) {
                    return;
                }

                playAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, 0);
                playAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_SPEED, "1");
                playAction.postControlAction();
                handler.sendEmptyMessage(DLNAConstants.GOON);
            }

        });
    }

    /**
     * 得到当前设备的播放状态 “STOPPED” // “PLAYING” // “TRANSITIONING”// ”PAUSED_PLAYBACK”// “PAUSED_RECORDING”// “RECORDING” //
     * “NO_MEDIA_PRESENT”//
     */
    public void getTransportState() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            @Override
            public void run() {
                if (mediaRenderDevice == null) return;

                Service localService = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);
                if (localService == null) {
                    return;
                }

                final Action localAction = localService.getAction(DLNAConstants.ACTION_GET_TRANSPORTINFO);
                if (localAction == null) {
                    return;
                }

                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");

                localAction.postControlAction();
                String transportState = localAction.getArgumentValue(DLNAConstants.ARGUMENT_KEY_CURRENT_TRANSPORT_STATE);
                Message msg = Message.obtain();
                msg.what = DLNAConstants.GET_TRANSPORT_STATE;
                msg.obj = transportState;
                handler.sendMessage(msg);
            }
        });
    }

    // 得到当前的声音的范围 min:max
    public void getVolumeDbRange() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service localService = mediaRenderDevice.getService(DLNAConstants.RENDERING_CONTROL_SERVICE);
                if (localService == null) {
                    return;
                }
                Action localAction = localService.getAction(DLNAConstants.ACTION_GET_VOLUME_DB_RANGE);
                if (localAction == null) {
                    return;
                }
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CHANNEL, "Master");
                localAction.postControlAction();
                String str2 = String.valueOf(localAction.getArgumentValue(DLNAConstants.ARGUMENT_KEY_MIN_VALUE));
                StringBuilder localStringBuilder = new StringBuilder();
                localStringBuilder.append(str2).append(":");
                String str3 = localAction.getArgumentValue(DLNAConstants.ARGUMENT_KEY_MAX_VALUE);
                localStringBuilder.append(str3);
                String volumeDbRange = localStringBuilder.toString();
                Message msg = Message.obtain();
                msg.what = DLNAConstants.GET_VOLUME_DB_RANGE;
                msg.obj = volumeDbRange;
                handler.sendMessage(msg);
            }
        });
    }

    public void seek(final String paramString) {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service localService = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);
                if (localService == null) return;

                Action localAction = localService.getAction(DLNAConstants.ACTION_SEEK);
                if (localAction == null) {
                    return;
                }
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_UNIT, "ABS_TIME");
                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_TARGET, paramString);
                boolean postControlAction = localAction.postControlAction();
                if (!postControlAction) {
                    localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_UNIT, "REL_TIME");
                    localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_TARGET, paramString);
                    postControlAction = localAction.postControlAction();
                }
                handler.sendEmptyMessage(DLNAConstants.SEEK);
            }

        });
    }

    // 能够得到当前的播放位置
    public void getPositionInfo(int action) {
        DLNAExecutorManager.executorService.execute(new PositionThread(action));
    }


    class PositionThread extends Thread{
        private final int action;
        public PositionThread(int action){
            this.action = action;
        }
        @Override
        public void run() {
            if (mediaRenderDevice == null) return;

            Service localService = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);

            if (localService == null) return;

            final Action localAction = localService.getAction(DLNAConstants.ACTION_GET_POSITION_INFO);
            if (localAction == null) {
                return;
            }

            localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
            localAction.postControlAction();
            String str = localAction.getArgumentValue(DLNAConstants.ARGUMENT_KEY_ABS_TIME);
            Message message = Message.obtain();
            message.what = DLNAConstants.GET_POSITION_INFO;
            message.obj = str;
            message.arg1 = action;
            handler.sendMessage(message);
        }
    }

    // 获得当前的媒体的总的时长
    public void getMediaDuration() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service localService = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);
                if (localService == null) {
                    return;
                }

                final Action localAction = localService.getAction(DLNAConstants.ACTION_GET_MEDIA_INFO);
                if (localAction == null) {
                    return;
                }

                localAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                localAction.postControlAction();
                String duration = localAction.getArgumentValue(DLNAConstants.ARGUMENT_KEY_MEDIA_DURATION);
                Message message = Message.obtain();
                message.what = DLNAConstants.GET_MEDIA_DURATION;
                message.obj = duration;
                handler.sendMessage(message);
            }

        });
    }

    /**
     * 1就是静音 0就是正常的时候
     *
     * @param targetValue
     * @return
     */
    public void setMute(final String targetValue) {
        DLNAExecutorManager.executorService.execute(new Thread() {
            @Override
            public void run() {
                if (mediaRenderDevice == null) return;

                Service service = mediaRenderDevice.getService(DLNAConstants.RENDERING_CONTROL_SERVICE);
                if (service == null) {
                    return;
                }
                final Action action = service.getAction(DLNAConstants.ACTION_SET_MUTE);
                if (action == null) {
                    return;
                }

                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CHANNEL, "Master");
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_DESIRED_MUTE, targetValue);
                boolean isSucces = action.postControlAction();
                Message msg = Message.obtain();
                msg.what = DLNAConstants.SETMUTE;
                msg.obj = isSucces;
                handler.sendMessage(msg);
            }
        });
    }

    // 获取当前是否是静音·
    public void getMute() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service service = mediaRenderDevice.getService(DLNAConstants.RENDERING_CONTROL_SERVICE);
                if (service == null) {
                    return;
                }

                final Action getAction = service.getAction(DLNAConstants.ACTION_GET_MUTE);
                if (getAction == null) {
                    return;
                }
                getAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                getAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CHANNEL, "Master");
                getAction.postControlAction();
                String mute = getAction.getArgumentValue(DLNAConstants.ARGUMENT_KEY_CURRENT_MUTE);
                Message message = Message.obtain();
                message.what = DLNAConstants.GETMUTE;
                message.obj = mute;
                handler.sendMessage(message);
            }

        });
    }

    public void setVoice(final int value) {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;
                Service service = mediaRenderDevice.getService(DLNAConstants.RENDERING_CONTROL_SERVICE);
                if (service == null) {
                    return;
                }

                final Action action = service.getAction(DLNAConstants.ACTION_SET_VOLUME);
                if (action == null) {
                    return;
                }

                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CHANNEL, "Master");
                action.setArgumentValue(DLNAConstants.ARGUMENT_KEY_DESIRED_VOLUME, value);
                action.postControlAction();
                handler.sendEmptyMessage(DLNAConstants.SETVOICE);
            }
        });
    }

    public void getVoice() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service service = mediaRenderDevice.getService(DLNAConstants.RENDERING_CONTROL_SERVICE);
                if (service == null) {
                    return;
                }

                final Action getAction = service.getAction(DLNAConstants.ACTION_GET_VOLUME);
                if (getAction == null) {
                    return;
                }
                getAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, "0");
                getAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_CHANNEL, "Master");
                getAction.postControlAction();
                int voice = getAction.getArgumentIntegerValue(DLNAConstants.ARGUMENT_KEY_CURRENT_VOLUME);
                Message msg = Message.obtain();
                msg.what = DLNAConstants.GETVOICE;
                msg.obj = voice;
                handler.sendMessage(msg);
            }

        });
    }

    public void stop() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            public void run() {
                if (mediaRenderDevice == null) return;

                Service service = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);

                if (service == null) {
                    return;
                }
                final Action stopAction = service.getAction(DLNAConstants.ACTION_STOP);
                if (stopAction == null) {
                    return;
                }

                stopAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, 0);
                stopAction.postControlAction();
                handler.sendEmptyMessage(DLNAConstants.STOP);
            }
        });
    }

    public void pause() {
        DLNAExecutorManager.executorService.execute(new Thread() {
            @Override
            public void run() {
                if (mediaRenderDevice == null) return;

                Service service = mediaRenderDevice.getService(DLNAConstants.AVTRANSPORT_SERVICE);
                if (service == null) {
                    return;
                }
                final Action pauseAction = service.getAction(DLNAConstants.ACTION_PAUSE);
                if (pauseAction == null) {
                    return;
                }
                pauseAction.setArgumentValue(DLNAConstants.ARGUMENT_KEY_INSTANCE_ID, 0);
                pauseAction.postControlAction();
                handler.sendEmptyMessage(DLNAConstants.PAUSE);
            }
        });
    }


}
