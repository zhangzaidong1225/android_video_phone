package com.ifeng.newvideo.dlna;

import android.content.Context;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.dao.VideoPlayDao;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import com.ifeng.video.dao.db.model.RankListVideoInfo;
import com.ifeng.video.dao.db.model.RelativeVideoListInfo;
import com.ifeng.video.dao.db.model.SubColumnVideoListInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * DLNA针对非专题类视频播放辅助类
 */
class VideoPlayerByDlna {
    private static final Logger logger = LoggerFactory.getLogger(VideoPlayerByDlna.class);
    private final IfengType.ListTag currentPlayingTag;
    private int curProgramIndex;
    private SubColumnVideoListInfo mSubColumnVideoListInfo;
    private RelativeVideoListInfo mGuidRelativeVideoListInfo;
    private RankListVideoInfo mRankListVideoInfo;
    private RankListVideoInfo mHostListVideoInfo;
    private PlayerInfoModel mCurrentPlayerInfoModel;
    private List<PlayerInfoModel> mplayInfoModels;
    private String mCurrentGuid;
    private final Context mContext;
    private VideoPlayerByDlnaCallBack mVodPlayerByDlnaCallBack;

    public VideoPlayerByDlna(Context context, IfengType.ListTag type, Object info, PlayerInfoModel program,
                             VideoPlayerByDlnaCallBack mVodPlayerByDlnaCallBack) {
        mContext = context;
        currentPlayingTag = type;
        mCurrentPlayerInfoModel = program;
        this.mVodPlayerByDlnaCallBack = mVodPlayerByDlnaCallBack;
        initInfoList(info);
        initCurrentIndex();
    }

    private void initCurrentIndex() {
        curProgramIndex = -1;
        switch (currentPlayingTag) {
            case related:
                for (int i = 0; i < mGuidRelativeVideoListInfo.getGuidRelativeVideoInfo().size(); i++) {
                    if (mGuidRelativeVideoListInfo.getGuidRelativeVideoInfo().get(i).getGuid()
                            .equalsIgnoreCase(mCurrentPlayerInfoModel.getGuid())) {
                        curProgramIndex = i;
                    }
                }
                break;
            case hotspot:
                for (int i = 0; i < mHostListVideoInfo.getBodyList().size(); i++) {
                    if (mHostListVideoInfo.getBodyList().get(i).getMemberItem().getGuid().equalsIgnoreCase(mCurrentPlayerInfoModel.getGuid())) {
                        curProgramIndex = i;
                    }
                }
                break;
            case columnplayed:
                for (int i = 0; i < mSubColumnVideoListInfo.getSubVideoListList().size(); i++) {
                    if (mSubColumnVideoListInfo.getSubVideoListList().get(i).getGuid().equalsIgnoreCase(mCurrentPlayerInfoModel.getGuid())) {
                        curProgramIndex = i;
                    }
                }
                break;
            case ranking:
                for (int i = 0; i < mRankListVideoInfo.getBodyList().size(); i++) {
                    if (mRankListVideoInfo.getBodyList().get(i).getMemberItem().getGuid().equalsIgnoreCase(mCurrentPlayerInfoModel.getGuid())) {
                        curProgramIndex = i;
                    }
                }
            case picture:
                for (int i = 0; i < mplayInfoModels.size(); i++) {
                    if (mCurrentPlayerInfoModel.getGuid().equalsIgnoreCase(mplayInfoModels.get(i).getGuid())) {
                        curProgramIndex = i;
                    }
                }
                break;

            default:
                break;
        }
    }

    private void initInfoList(Object info) {
        switch (currentPlayingTag) {
            case related:
                setVodList((RelativeVideoListInfo) info);
                break;
            case hotspot:
                setHotList((RankListVideoInfo) info);
                break;
            case columnplayed:
                setColumnList((SubColumnVideoListInfo) info);
                break;
            case ranking:
                setRankingList((RankListVideoInfo) info);
                break;
            case picture:
                setPicList((List<PlayerInfoModel>) info);
                break;
            default:
                break;
        }
    }

    public VideoPlayerByDlnaCallBack getmVodPlayerByDlnaCallBack() {
        return mVodPlayerByDlnaCallBack;
    }

    public void setmVodPlayerByDlnaCallBack(VideoPlayerByDlnaCallBack mVodPlayerByDlnaCallBack) {
        this.mVodPlayerByDlnaCallBack = mVodPlayerByDlnaCallBack;
    }

    private void getPlayerInfoByGuid(String guid) {
        VideoPlayDao.getSingleVideoByGuid(guid, mCurrentPlayerInfoModel, new Response.Listener<PlayerInfoModel>() {
            @Override
            public void onResponse(PlayerInfoModel response) {
                if (response == null) {
                    logger.error("dlna 获取下一条为空，重新获取");
                    getPlayerInfoByGuid(mCurrentGuid);
                }
                logger.debug("dlna 通过网络获取下条信息");
                mCurrentPlayerInfoModel = response;
                mCurrentGuid = mCurrentPlayerInfoModel.guid;
                PlayVideo(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("dlna 获取下一条失败，重新获取", error);
                getPlayerInfoByGuid(mCurrentGuid);
            }
        });
    }


    public void autoSwitchNextVideo() {
        logger.debug("dlna 自动切换下一条");
        String guid = null;
        switch (currentPlayingTag) {
            case related:
                guid = getNextVodGuid();
                break;
            case hotspot:
                guid = getNextHotGuid();
                break;
            case columnplayed:
                guid = getNextColumnGuid();
                break;
            case ranking:
                guid = getNextRankGuid();
                break;
            case picture:
                guid = getNextPicGuid();
                break;
            default:
                break;
        }
        if (guid != null) {
            PlayerInfoModel playerInfoModel = mVodPlayerByDlnaCallBack.getPlayerInfoModelFromList(guid);
            if (playerInfoModel != null) {
                mCurrentPlayerInfoModel = playerInfoModel;
                mCurrentGuid = mCurrentPlayerInfoModel.guid;
                PlayVideo(false);
            } else {
                getPlayerInfoByGuid(guid);
            }
        } else {
            logger.error("dlna guid 为空");
        }
    }


    public void autoSwitchPreVideo() {
        curProgramIndex = curProgramIndex - 2;
        if (curProgramIndex == -2) {
            curProgramIndex = getCurrentPlayListSize() - 2;
        }
        autoSwitchNextVideo();
    }

    public int getCurrentIndex() {
        return curProgramIndex;
    }

    private String getNextColumnGuid() {
        curProgramIndex++;
        List<SubColumnVideoListInfo.VideoItem> list = mSubColumnVideoListInfo.getSubVideoListList();
        if (curProgramIndex > list.size() - 1) {
            curProgramIndex = 0;
        }
        String guid = list.get(curProgramIndex).guid;
        mCurrentGuid = guid;
        return guid;
    }

    private String getNextRankGuid() {
        curProgramIndex++;
        List<RankListVideoInfo.BodyItem> list = mRankListVideoInfo.getBodyList();
        if (curProgramIndex > list.size() - 1) {
            curProgramIndex = 0;
        }
        String guid = list.get(curProgramIndex).getMemberItem().getGuid();
        mCurrentGuid = guid;
        return guid;
    }

    private String getNextPicGuid() {
        curProgramIndex++;
        if (curProgramIndex > mplayInfoModels.size() - 1) {
            curProgramIndex = 0;
        }
        String guid = mplayInfoModels.get(curProgramIndex).getGuid();
        mCurrentGuid = guid;
        return guid;
    }

    private String getNextHotGuid() {
        curProgramIndex++;
        List<RankListVideoInfo.BodyItem> list = mHostListVideoInfo.getBodyList();
        if (curProgramIndex > list.size() - 1) {
            curProgramIndex = 0;
        }
        String guid = list.get(curProgramIndex).getMemberItem().getGuid();
        mCurrentGuid = guid;
        return guid;
    }

    private String getNextVodGuid() {
        curProgramIndex++;
        List<RelativeVideoListInfo.RelativeVideoInfoItem> list = mGuidRelativeVideoListInfo.getGuidRelativeVideoInfo();
        if (curProgramIndex > list.size() - 1) {
            curProgramIndex = 0;
        }
        String guid = list.get(curProgramIndex).guid;
        mCurrentGuid = guid;
        return guid;
    }

    private void PlayVideo(boolean isNew) {
        if (mCurrentPlayerInfoModel == null) {
            logger.error("dlna 播放下一条失败，退出");
            return;
        }
        if (mVodPlayerByDlnaCallBack != null) {
            if (isNew) {
                mVodPlayerByDlnaCallBack.updatePlayerInfo(mCurrentPlayerInfoModel);
            }
            mVodPlayerByDlnaCallBack.prepareToPlay();
        } else {
            logger.error("mVodPlayerByDlnaCallBack 是空！");
        }
    }

    // 播放广告时候给第一条视频加载数据
    public void setVideoData() {
        if (mCurrentPlayerInfoModel == null) {
            return;
        }
        if (mVodPlayerByDlnaCallBack != null) {
            mVodPlayerByDlnaCallBack.updatePlayerInfo(mCurrentPlayerInfoModel);
        }
    }


    public IfengType.ListTag getCurrentPlayingTag() {
        return currentPlayingTag;
    }


    private int getCurrentPlayListSize() {
        switch (currentPlayingTag) {
            case related:
                return mGuidRelativeVideoListInfo.getGuidRelativeVideoInfo().size();
            case hotspot:
                return mHostListVideoInfo.getBodyList().size();
            case columnplayed:
                return mSubColumnVideoListInfo.getSubVideoListList().size();
            case ranking:
                return mRankListVideoInfo.getBodyList().size();
            case picture:
                return mplayInfoModels.size();
            default:
                break;
        }
        return 0;
    }

    /**
     * 获得播放列表的标题
     *
     * @return
     */
    public List<String> getPlayList() {
        ArrayList<String> playlist = new ArrayList<String>();
        switch (currentPlayingTag) {
            case related:
                List<RelativeVideoListInfo.RelativeVideoInfoItem> list = mGuidRelativeVideoListInfo.getGuidRelativeVideoInfo();
                for (int i = 0; i < list.size(); i++) {
                    playlist.add(list.get(i).getName());
                }
                break;
            case hotspot:
                List<RankListVideoInfo.BodyItem> list1 = mHostListVideoInfo.getBodyList();
                for (int i = 0; i < list1.size(); i++) {
                    playlist.add(list1.get(i).getTitle());
                }
                break;
            case columnplayed:

                List<SubColumnVideoListInfo.VideoItem> list2 = mSubColumnVideoListInfo.getSubVideoListList();
                for (int i = 0; i < list2.size(); i++) {
                    playlist.add(list2.get(i).getName());
                }
                break;
            case ranking:
                List<RankListVideoInfo.BodyItem> list3 = mRankListVideoInfo.getBodyList();
                for (int i = 0; i < list3.size(); i++) {
                    playlist.add(list3.get(i).getTitle());
                }
                break;
            case picture:
                for (PlayerInfoModel playInfoModel : mplayInfoModels) {
                    playlist.add(playInfoModel.getName());
                }
                break;
            default:
                break;
        }
        return playlist;
    }

    /**
     * 播放你点击的视频
     *
     * @param position
     */
    public void playIndexVideo(int position) {
        curProgramIndex = position - 1;
        autoSwitchNextVideo();
    }

    private void setVodList(RelativeVideoListInfo Info) {
        mGuidRelativeVideoListInfo = Info;
    }

    private void setColumnList(SubColumnVideoListInfo Info) {
        mSubColumnVideoListInfo = Info;
    }


    private void setRankingList(RankListVideoInfo Info) {
        mRankListVideoInfo = Info;
    }

    private void setHotList(RankListVideoInfo Info) {
        mHostListVideoInfo = Info;
    }

    private void setPicList(List<PlayerInfoModel> playerInfoModels) {
        mplayInfoModels = playerInfoModels;
    }

}
