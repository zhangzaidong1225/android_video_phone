package com.ifeng.newvideo.dlna;

import android.content.Context;
import com.ifeng.video.core.utils.NetUtils;
import org.cybergarage.upnp.ControlPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DLNA搜索线程，扫描附近dlna设备
 */
class DLNASearchThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(DLNASearchThread.class);

    private static final int REFRESH_DEVICES_INTERVAL = 15 * 1000;
    private static final int LONG_REFRESH_DEVICES_INTERVAL = 120 * 1000;

    private ControlPoint mCP = null;
    private Context mContext = null;
    /**
     * 是否已经启动 *
     */
    private boolean mStartComplete = false;
    /**
     * 是否已退出线程 *
     */
    private boolean mIsExit = false;
    /**
     * 已搜索次数 *
     */
    private int count = 0;

    public DLNASearchThread(Context context, ControlPoint controlPoint) {
        mContext = context;
        mCP = controlPoint;
    }

    private void setCompleteFlag(boolean flag) {
        mStartComplete = flag;
    }


    /**
     * 唤醒线程
     */
    public void awakeThread() {
        synchronized (this) {
            notifyAll();
        }
    }

    /**
     * 重启线程
     */
    public void reset() {
        setCompleteFlag(false);
        awakeThread();
    }


    /**
     * 停止线程
     */
    public void exit() {
        mIsExit = true;
        awakeThread();
    }

    @Override
    public void run() {
        while (true) {
            if (mIsExit) {
                mCP.stop();
                break;
            }
            refreshDevices();
            synchronized (this) {
                try {
                    count++;
                    /**
                     * 搜索次数，小于5次每次间隔时间较短，大于5次间隔时间增长，节省系统开销
                     */
                    if (count >= 5) {
                        wait(LONG_REFRESH_DEVICES_INTERVAL);
                    } else {
                        wait(REFRESH_DEVICES_INTERVAL);
                    }
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                }
            }
        }

    }


    /**
     * 刷新附近的设备
     */
    private void refreshDevices() {
        if (!NetUtils.isNetAvailable(mContext) || mCP == null) {
            return;
        }
        try {
            if (mStartComplete) {
                mCP.search();
            } else {
                mCP.stop();
                boolean startRet = mCP.start();
                if (startRet) {
                    mStartComplete = true;
                }
            }
        } catch (Exception e) {
            logger.error("refreshDevices error ! {}", e.getMessage());
        }

    }


    /**
     * 设置已搜索次数
     *
     * @param count
     */
    public synchronized void setCount(int count) {
        this.count = count;
    }

}
