package com.ifeng.newvideo.dlna;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DLNA常量
 * Created by cuihz on 2014/9/22.
 */
class DLNAConstants {

    private static final Logger logger = LoggerFactory.getLogger(DLNAConstants.class);


    /**
     * handler指令 *
     */
    public static final int UPDATE_CURRENT = 801;
    public static final int AUTO_PLAY = 802;
    public static final int GO_PLAY = 803;
    public static final int PAUSE = 804;
    public static final int STOP = 805;
    public static final int GETVOICE = 806;
    public static final int SETVOICE = 807;
    public static final int GETMUTE = 808;
    public static final int SETMUTE = 809;
    public static final int GET_MEDIA_DURATION = 810;
    public static final int GET_POSITION_INFO = 811;
    public static final int SEEK = 812;
    public static final int GET_VOLUME_DB_RANGE = 813;
    public static final int GET_TRANSPORT_STATE = 814;
    public static final int GOON = 815;
    public static final int PLAY = 816;
    public static final int FIRSTPLAY = 817;
    static final int GO_GET_STATE = 818;


    /**
     * 设备 *
     */
    public static final String MEDIA_RENDER_DEVICE = "urn:schemas-upnp-org:device:MediaRenderer:1";

    /**
     * service *
     */
    public static final String AVTRANSPORT_SERVICE = "urn:schemas-upnp-org:service:AVTransport:1";
    public static final String RENDERING_CONTROL_SERVICE = "urn:schemas-upnp-org:service:RenderingControl:1";

    /**
     * action of AVTransport service *
     */
    public static final String SET_AVTRANSPORT_URI = "SetAVTransportURI";
    public static final String ACTION_PLAY = "Play";
    public static final String ACTION_STOP = "Stop";
    public static final String ACTION_PAUSE = "Pause";
    public static final String ACTION_SEEK = "Seek";
    public static final String ACTION_GET_TRANSPORTINFO = "GetTransportInfo";
    public static final String ACTION_GET_POSITION_INFO = "GetPositionInfo";
    public static final String ACTION_GET_MEDIA_INFO = "GetMediaInfo";


    /**
     * action of RenderingControl service   *
     */
    public static final String ACTION_GET_VOLUME_DB_RANGE = "GetVolumeDBRange";
    public static final String ACTION_SET_MUTE = "SetMute";
    public static final String ACTION_GET_MUTE = "GetMute";
    public static final String ACTION_SET_VOLUME = "SetVolume";
    public static final String ACTION_GET_VOLUME = "GetVolume";


    /**
     * argument key of action *
     */
    public static final String ARGUMENT_KEY_INSTANCE_ID = "InstanceID";
    public static final String ARGUMENT_KEY_CURRENT_URI = "CurrentURI";
    public static final String ARGUMENT_KEY_CURRENT_URI_META_DATA = "CurrentURIMetaData";
    public static final String ARGUMENT_KEY_SPEED = "Speed";
    public static final String ARGUMENT_KEY_UNIT = "Unit";
    public static final String ARGUMENT_KEY_TARGET = "Target";
    public static final String ARGUMENT_KEY_CURRENT_TRANSPORT_STATE = "CurrentTransportState";
    public static final String ARGUMENT_KEY_CHANNEL = "Channel";
    public static final String ARGUMENT_KEY_MIN_VALUE = "MinValue";
    public static final String ARGUMENT_KEY_MAX_VALUE = "MaxValue";
    public static final String ARGUMENT_KEY_ABS_TIME = "AbsTime";
    public static final String ARGUMENT_KEY_MEDIA_DURATION = "MediaDuration";
    public static final String ARGUMENT_KEY_DESIRED_MUTE = "DesiredMute";
    public static final String ARGUMENT_KEY_CURRENT_MUTE = "CurrentMute";
    public static final String ARGUMENT_KEY_DESIRED_VOLUME = "DesiredVolume";
    public static final String ARGUMENT_KEY_CURRENT_VOLUME = "CurrentVolume";


    /**
     * 非静音 *
     */
    public static final String VOICE_ON = "0";
    /**
     * 静音 *
     */
    public static final String NO_VOICE = "1";


    /**
     * 端口状态 *
     */
    public static final String TRANSPORT_STATE_STOPPED = "STOPPED";
    public static final String TRANSPORT_STATE_PAUSED_PLAYBACK = "PAUSED_PLAYBACK";
    public static final String TRANSPORT_STATE_TRANSITIONING = "TRANSITIONING";

    /**
     * 播放位置信息有误 *
     */
    public static final String POSITION_NOT_IMPLEMENTED = "NOT_IMPLEMENTED";


}
