package com.ifeng.newvideo.dlna;


import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.video.dao.db.constants.IfengType;
import com.ifeng.video.dao.db.model.PlayerInfoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DLNA播放界面，针对非专题类视频
 */
public class DLNAPlayerActivityForVideo extends DLNABasePlayerActivity implements VideoPlayerByDlnaCallBack {
    private static final Logger logger = LoggerFactory.getLogger(DLNAPlayerActivityForVideo.class);

    private VideoPlayerByDlna videoPlayerByDlna;
    private IfengType.ListTag currentPlayingTag;
    private Object mDataInfo;


    @Override
    protected void init() {
        super.init();
        currentPlayingTag = (IfengType.ListTag) getIntent().getExtras().getSerializable(IntentKey.LIST_TAG);
        mDataInfo = getIntent().getExtras().getSerializable(IntentKey.DATA_INFO);

        videoPlayerByDlna = new VideoPlayerByDlna(this, currentPlayingTag, mDataInfo, getCurPlayProgram(playIndex, playList), this);

        adapterSelection = new AdapterSelection(this, videoPlayerByDlna.getPlayList());
        listViewPopChannel.setAdapter(adapterSelection);
        listViewPopChannel.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                videoPlayerByDlna.playIndexVideo(position);
                adapterSelection.setPlayIndex(position);
                hidenControl();
            }
        });
    }

    @Override
    protected void playNext() {
        handler.removeMessages(DLNAConstants.FIRSTPLAY);
        removeChangeMessage();
        videoPlayerByDlna.autoSwitchNextVideo();
    }

    @Override
    protected void initState() {
        adapterSelection.setPlayIndex(videoPlayerByDlna.getCurrentIndex());
        super.initState();
    }

    @Override
    protected void playPre() {
        removeChangeMessage();
        videoPlayerByDlna.autoSwitchPreVideo();
    }


    @Override
    public void prepareToPlay() {
        logger.debug("dlna 准备播放下一条");
        playIndex = 0;
        dlnaPlayerControl.firstPlay(getCurUrl());
    }

    @Override
    public PlayerInfoModel getPlayerInfoModelFromList(String guid) {
        for(PlayerInfoModel playerInfoModel : playList){
            if(playerInfoModel.getGuid().equals(guid)){
                return playerInfoModel;
            }
        }
        return null;
    }

    @Override
    public void updatePlayerInfo(PlayerInfoModel program) {
        if (playList.size() > 0) {
            playList.clear();
        }
        playList.add(program);
    }

}
