package com.ifeng.newvideo.dlna;


import com.ifeng.video.dao.db.model.PlayerInfoModel;

public interface VideoPlayerByDlnaCallBack {
    //更新数据
    void updatePlayerInfo(PlayerInfoModel program);

    //播放currentV7program视频
    void prepareToPlay();

    PlayerInfoModel getPlayerInfoModelFromList(String guid);
}
