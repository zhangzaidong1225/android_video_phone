package com.ifeng.newvideo.badge;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * badge(桌面角标)工具类
 */
public class BadgeUtils {

    private static final Logger logger = LoggerFactory.getLogger(BadgeUtils.class);

    public static void showBadge(Context context, int count) {
        if (shouldShowBadge(context)) {
            setBadge(context, count);
        }
    }

    public static void clearBadge(Context context) {
        setBadge(context, 0);
        logger.debug("clearBadge");
    }

    /**
     * 设置角标显示的数量，0不显示
     *
     * @param context context
     * @param count   最大值999999999
     *                清除设置为0
     */
    private static void setBadge(Context context, int count) {
        if (context == null) {
            logger.debug("setBadge context = null");
            return;
        }

        try {
            Intent intent = new Intent(context, BadgeService.class);
            intent.putExtra("msg_count", count);
            context.startService(intent);
            logger.debug("setBadge count {}", count);
        } catch (Exception e) {
            logger.error("setBadge error! {} ", e);
        }
    }

    /**
     * 启动接口inreview值为0时，返回true，否则false
     */
    private static boolean shouldShowBadge(Context context) {
        if (context == null) {
            logger.debug("shouldShowBadge context = null");
            return false;
        }

        final String inreview = SharePreUtils.getInstance().getInreview();
        logger.debug("shouldShowBadge inreview = {}", inreview);
        return !TextUtils.isEmpty(inreview) && Integer.valueOf(inreview) == 0;
    }
}