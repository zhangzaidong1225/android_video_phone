package com.ifeng.newvideo.badge;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.ui.ActivitySplash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * BadgeService，通知系统显示badge
 */
public class BadgeService extends IntentService {
    private static final Logger logger = LoggerFactory.getLogger(BadgeService.class);

    private static final String SPLASH_ACTIVITY = ActivitySplash.class.getName();
    private static final String PACKAGE_NAME = IfengApplication.PACKAGE_NAME;

    public BadgeService() {
        super("BadgeService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
//        int badgeNumber = new Random().nextInt(10000);
        int badgeNumber = intent.getIntExtra("msg_count", 0);
        logger.debug("ifeng BadgeService badgeNumber {}", badgeNumber);

        String manufactureStr = Build.MANUFACTURER;
        logger.debug("ifeng manufacture {} ", manufactureStr);
        if (TextUtils.isEmpty(manufactureStr)) {
            return;
        }

        boolean isHTC = manufactureStr.toLowerCase(Locale.US).contains("htc");
        boolean isSONY = manufactureStr.toLowerCase(Locale.US).contains("sony");
        boolean isSAMSUNG = manufactureStr.toLowerCase(Locale.US).contains("samsung");

        // Sony Ericssion
        if (isSONY) {
            try {
                intent.setAction("com.sonyericsson.home.action.UPDATE_BADGE");
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME", SPLASH_ACTIVITY);
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE", badgeNumber != 0);
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.MESSAGE", badgeNumber);
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME", PACKAGE_NAME);
                sendBroadcast(intent);
            } catch (Exception localException) {
                logger.error("ifeng  Sony {} ", localException.getLocalizedMessage());
            }
        }

        // HTC
        if (isHTC) {
            try {
                Intent localIntent1 = new Intent("com.htc.launcher.action.UPDATE_SHORTCUT");
                localIntent1.putExtra("packagename", PACKAGE_NAME);
                localIntent1.putExtra("count", badgeNumber);
                sendBroadcast(localIntent1);

                Intent localIntent2 = new Intent("com.htc.launcher.action.SET_NOTIFICATION");
                ComponentName localComponentName = new ComponentName(this, SPLASH_ACTIVITY);
                localIntent2.putExtra("com.htc.launcher.extra.COMPONENT", localComponentName.flattenToShortString());
                localIntent2.putExtra("com.htc.launcher.extra.COUNT", badgeNumber);
                sendBroadcast(localIntent2);
            } catch (Exception localException) {
                logger.error("ifeng HTC {} ", localException.getLocalizedMessage());
            }
        }

        // Samsung
        if (isSAMSUNG) {
            try {
                ContentResolver localContentResolver = getContentResolver();
                Uri localUri = Uri.parse("content://com.sec.badge/apps");
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("package", PACKAGE_NAME);
                localContentValues.put("class", SPLASH_ACTIVITY);
                localContentValues.put("badgecount", badgeNumber);
                String str = "package=? AND class=?";
                String[] arrayOfString = new String[2];
                arrayOfString[0] = PACKAGE_NAME;
                arrayOfString[1] = SPLASH_ACTIVITY;

                int update = localContentResolver.update(localUri, localContentValues, str, arrayOfString);
                if (update == 0) {
                    localContentResolver.insert(localUri, localContentValues);
                }

            } catch (IllegalArgumentException localIllegalArgumentException) {
                logger.error("ifeng Samsung1F {} ", localIllegalArgumentException.getLocalizedMessage());
            } catch (Exception localException) {
                logger.error("ifeng Samsung {} ", localException.getLocalizedMessage());
            }
        }
    }
}
