package com.ifeng.newvideo.keepAlive;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.ifeng.newvideo.IfengApplication;
import com.ifeng.newvideo.ui.basic.BaseFragmentActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class KeepAliveJobService extends JobService {
    private static final Logger logger = LoggerFactory.getLogger(BaseFragmentActivity.class);

    public final static String TAG = "keepalive";
    public static final int JOB_ID = 123;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
//            Toast.makeText(LocalJobService.this, "LocalJobService", Toast.LENGTH_SHORT).show();
            try {
                JobParameters param = (JobParameters) msg.obj;
                jobFinished(param, true);
                Intent intent = new Intent(KeepAliveReceiver.ACTION_PUSH_KEEPALIVE);
                sendBroadcast(intent);
            } catch (Exception e) {
                logger.error("handleMessage error! ", e);
            }
            return true;
        }
    });

    public static void startJobScheduler() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                JobScheduler jobScheduler = (JobScheduler) IfengApplication.getInstance().getSystemService(JOB_SCHEDULER_SERVICE);
                JobInfo jobInfo = new JobInfo.Builder(JOB_ID, new ComponentName(
                        IfengApplication.getInstance().getPackageName(), KeepAliveJobService.class.getName()))
                        .setPeriodic(15 * 60 * 1000)
                        // .setPeriodic(10)
                        .setPersisted(true)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();
                jobScheduler.schedule(jobInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        startJobScheduler();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "LocalAliveService onStartCommand");
        return START_STICKY;
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "LocalAliveService onStartJob");
        if (params != null) {
            Message m = Message.obtain();
            m.obj = params;
            handler.sendMessage(m);
        }
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "LocalAliveService onStopJob");
        handler.removeCallbacksAndMessages(null);
        return false;
    }
}
