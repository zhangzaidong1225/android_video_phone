package com.ifeng.newvideo;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;
import com.ifeng.core.DisplayConfiguration;
import com.ifeng.core.IfengEngine;
import com.ifeng.ifadvertsdk.IFAdvertSdk;
import com.ifeng.newvideo.cache.CacheManager;
import com.ifeng.newvideo.fontsOverride.CalligraphyConfig;
import com.ifeng.newvideo.login.entity.User;
import com.ifeng.newvideo.statistics.StatisticHBService;
import com.ifeng.newvideo.utils.GrayInstallUtils;
import com.ifeng.newvideo.utils.IntentUtils;
import com.ifeng.newvideo.utils.NewsSilenceInstallUtils;
import com.ifeng.newvideo.utils.PhoneConfig;
import com.ifeng.newvideo.utils.PushUtils;
import com.ifeng.newvideo.utils.SharePreUtils;
import com.ifeng.newvideo.utils.UserPointManager;
import com.ifeng.newvideo.videoplayer.player.audio.AudioService;
import com.ifeng.params.IFInitParam;
import com.ifeng.video.core.net.VolleyHelper;
import com.ifeng.video.core.utils.DistributionInfo;
import com.ifeng.video.core.utils.NetUtils;
import com.ifeng.video.core.utils.PackageUtils;
import com.ifeng.video.core.utils.StorageUtils;
import com.ifeng.video.core.utils.ToastUtils;
import com.ifeng.video.dao.db.constants.DataInterface;
import com.ifeng.video.upgrade.grayupgrade.GrayUpgradeUtils;
import com.tencent.bugly.crashreport.CrashReport;
import com.umeng.analytics.MobclickAgent;
import imageload.ImageLoaderManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * IfengApplication class
 */
public class IfengApplication extends Application {
    private static final Logger logger = LoggerFactory.getLogger(IfengApplication.class);

    public static final String PACKAGE_NAME = "com.ifeng.newvideo";

    public static final String BUGLY_APPID = "f2ca7283c3";

    /**
     * 凤凰新闻SDK渠道号，用于新闻详情页去头尾的广告
     */
    public static final String IFENG_NEWS_SDK_PUBLISH_ID = "8146";

    private boolean isScreenOff = false;

    private boolean showContinueCacheVideoDialog = true;

    private static IfengApplication sIfengVideoApplication;

    private final HashMap<Object, Object> mAttribute = new HashMap<>();

    private final HashMap<String, String> mShareUrl = new HashMap<>();
    //标记是否允许运营商网络播放
    public static boolean mobileNetCanPlay = false;

    /**
     * 存储所有Activity的当前生命周期状态
     */
    private final Map<Activity, Boolean> mActivityState = new ConcurrentHashMap<>();
    /**
     * 此处添加这个主要是为解决，分享编辑页面拉起liveRoom Back建的BUG。
     */
    private boolean mIsShareEditPageShow = false;

    public boolean isForeground = true;

    /**
     * 当前栈顶的activity
     */
    // private Activity mActivityQueue;
    // private WeakReference<Activity> mActivityQueue;

//    private List<WeakReference<Activity>> mActivityQueue = new ArrayList<>();
    private List<Activity> mActivityQueue = new ArrayList<>();

    public static IfengApplication getInstance() {
        return sIfengVideoApplication;
    }

    public static Context getAppContext() {
        return sIfengVideoApplication;
    }

    /**
     * 点播播放次数，for广告播放规则计数
     */
    public int mVodPlayCount = 0;

    /**
     * 用以记录离开精选频道的时间戳
     */
    private long mWellChosenTimestamp = 0;

    /**
     * 记录免流量toast 弹出的次数
     */
    public int mobileToastNum = 0;
    /**
     * 全局弹幕开关控制
     */
    public static boolean danmaSwitchStatus = true;


    /**
     * 由于当前ShareVerticalActivity的打开与关闭会影响到播放基页的生命同期，进而影响到统计的上报，为了
     * 消除这种影响，在此加了一个全局的标志，用来标明播放基页的生命周期变化是由谁引起的，如果是ShareVerticalActivity
     * 引起的那么在播放基页相应的生命周期方法里做相应的处理
     */
    public static boolean isShareVertialActivityFinish = false;

    public boolean getShowContinueCacheVideoDialog() {
        return showContinueCacheVideoDialog;
    }

    public void setShowContinueCacheVideoDialog(boolean showContinueCacheVideoDialog) {
        this.showContinueCacheVideoDialog = showContinueCacheVideoDialog;
    }

    @Override
    public void onCreate() {
        if (shouldInitApp()) {
            initStrictMode();
            sIfengVideoApplication = this;
            DataInterface.context = this;// 设置dao DataInterface context
            StorageUtils.getInstance().init(this);
            if (!BuildConfig.DEBUG) {
                IfengCrashHandler.getInstance().init(this);
            }
            VolleyHelper.init(this);
            ImageLoaderManager.getInstance().init(this);

            PhoneConfig.init(this);

            clearUserInfoIfNeeded();

            PushUtils.initPush();

            configUmengAnalytics();
            // startChinaNetCenterService();
            restartOrPauseCachingVideo();
            startHBService();
            initAdExposeSdk();
            ToastUtils.getInstance().init(this);
            initFont();

            initIfengNewsSDK();

            UserPointManager.resetUserPointStatusForTime();

            initBugly();

            initGrayUpgrade();
            logger.debug("IfengApplication onCreate over !");
        }
        super.onCreate();
    }

    /**
     * Bugly SDK初始化
     */
    public void initBugly() {
        CrashReport.initCrashReport(this, BUGLY_APPID, BuildConfig.DEBUG);
        CrashReport.setIsDevelopmentDevice(this, BuildConfig.DEBUG);
        CrashReport.setUserId(User.getUid());

        CrashReport.UserStrategy userStrategy = new CrashReport.UserStrategy(this);
        userStrategy.setAppChannel(PhoneConfig.publishid);

    }

    /**
     * 需要清用户数据的情况:
     * 1、第一次覆盖升级到740时 ，因为7.4.0 改了三方账号appId，统一成了ios的，
     * 会导致登录后的用户guid不一致，影响积分等累加，所以需要清除一次旧的用户guid等数据
     * ~
     * 2、在7.4.2 加了用户昵称修改，为防止之前的用户名包含特殊字符或本身名字就不符合标准，升级后重新登录，自动修改昵称
     */
    private void clearUserInfoIfNeeded() {
        try {
            int lastVersionCode = SharePreUtils.getInstance().getLastVersionCode();
            boolean isLastUnder742 = lastVersionCode <= 7042;
            if (isLastUnder742) {
                User.clearUserInfo();
            }
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int curVersionCode = packageInfo.versionCode;
            if (curVersionCode != lastVersionCode) {
                SharePreUtils.getInstance().setLastVersionCode(curVersionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            logger.error("getPackageInfo error !", e);
        }
    }

    /**
     * //初始化新闻SDK
     */
    private void initIfengNewsSDK() {
        try {
            IfengEngine.Builder builder = new IfengEngine.Builder();
            builder.setKey(IFENG_NEWS_SDK_PUBLISH_ID).setContext(this).builder();
            IfengEngine engine = builder.builder();

            // 频道页设置
            DisplayConfiguration configuration = engine.getConfiguration();
            configuration.setMenuBackgroundColorResID(R.color.main_pager_tab_bg);
            configuration.setMenuHeightResId(R.dimen.top_nav_height);
            configuration.setTextUncheckedColorResID(R.color.main_pager_tab_normal_color);
            configuration.setTextCheckedColorResID(R.color.main_pager_tab_selected_color);
            configuration.setLineCheckedColorResID(R.color.main_pager_tab_selected_color);
            // configuration.setLineCheckedColor(R.color.transparent);
            configuration.setLineUncheckedColorResID(R.color.transparent);
            // configuration.setShowChannelListIconResID(R.drawable.sub_entrance);

            // 详情页设置
            configuration.setShowHead(false);
            configuration.setDetailbottomBarLocation(DisplayConfiguration.TOP);
            configuration.setDetailBottomBarHeightResId(R.dimen.common_title_bar_height);
            // configuration.setDetailActivityBackIconResID(R.drawable.ad_back);
            // configuration.setDetailActivityShareIconResID(R.drawable.video_player_bottom_share_selector);
        } catch (Throwable e) {
            logger.error("initIfengNewsSDK  error !  {}", e);
        }

        NewsSilenceInstallUtils.checkHasInstalledNews();
        NewsSilenceInstallUtils.setDownloadCompleteCallback();
    }

    private void initGrayUpgrade() {

        GrayUpgradeUtils.Builder builder = new GrayUpgradeUtils.Builder();
        builder.setContext(this).builder();
        GrayUpgradeUtils grayUpgradeUtils = builder.builder();

        GrayInstallUtils.setDownloadCompleteCallback();
    }

    private void initStrictMode() {
        if (BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
        }
    }

    private void initFont() {
        if (Build.MANUFACTURER.toLowerCase(Locale.US).contains(getString(R.string.manufacturer_xiaomi))) {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath(getString(R.string.font_for_xiaomi))
                    .setFontAttrId(R.attr.fontPath)
                    .build());
        }
    }

    /**
     * 是否应执行Application初始化方法
     * 为了解决app manifest里设置了process之后引起的多次执行Application.onCreate()
     */
    private boolean shouldInitApp() {
        String processName = getProcessName(this);
        logger.debug("进程名称 {}", processName);
        return processName == null || PACKAGE_NAME.equals(processName);
    }

    /**
     * 获取当前应用进程名称
     *
     * @return null may be returned if the specified process not found
     */
    private static String getProcessName(Context cxt) {
        String processName = null;
        ActivityManager am = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        am = null;
        if (runningApps == null) {
            return processName;
        }
        for (ActivityManager.RunningAppProcessInfo processInfo : runningApps) {
            if (processInfo != null && processInfo.pid == android.os.Process.myPid()) {
                processName = processInfo.processName;
                break;
            }
        }
        runningApps.clear();
        return processName;
    }

    private void restartOrPauseCachingVideo() {
        //当应用进程被杀死霍覆盖升级时将正在缓存的视频改为等待缓存，保证其可以再次被开始
        CacheManager.waitingCaching(this);

        if (NetUtils.isNetAvailable(this)) {
            if (NetUtils.isMobile(this)) {
                boolean isMobileOpen = SharePreUtils.getInstance().getCacheVideoState();
                if (isMobileOpen) {
                    logger.debug("允许运营商网络缓存开关---开启");
                    logger.debug("其他运营商网络或联通未超量，开启缓存");
                    CacheManager.startCaching(this);
                } else {
                    logger.debug("允许运营商网络缓存开关---关闭---开启缓存");
                    CacheManager.startCaching(this);
                }
            } else {
                logger.debug("wifi 网络---开启缓存");
                CacheManager.startCaching(this);
            }
        } else {
            logger.debug("无网---暂停缓存");
            CacheManager.pauseCaching(this);
        }
    }

    /**
     * 首页第10条广告曝光地址统计sdk初始化
     */
    private void initAdExposeSdk() {
        try {
            IFInitParam param = new IFInitParam();
            param.uid = PhoneConfig.UID;
            logger.info("imei = {}", param.uid);
            param.av = PhoneConfig.softversion;
            param.os = "android_" + Build.VERSION.SDK_INT;
            param.proid = "ifengvideo";
            param.screen = "" + PhoneConfig.getScreenWidth() + "x" + PhoneConfig.getScreenHeight();
            param.df = "androidphone";
            IFAdvertSdk.getAdvertSdk().initNetParams(this, param);
        } catch (Exception e) {
            logger.error("initAdExposeSdk error{}", e.getMessage());
        }
    }

    /**
     * 设置友盟统计appKey  渠道名称channel
     */
    private void configUmengAnalytics() {
        MobclickAgent.startWithConfigure(new MobclickAgent.UMAnalyticsConfig(this, DistributionInfo.umeng_appkey, DistributionInfo.umeng_channel));
        MobclickAgent.openActivityDurationTrack(false);
        MobclickAgent.setSessionContinueMillis(30 * 1000);
    }

    /**
     * 启动后台监测联网日志服务
     */
    private void startHBService() {
        IntentUtils.startService(this, new Intent(StatisticHBService.ACTION));
    }

    public Object getAttribute(Object key) {
        return mAttribute.get(key);
    }

    public void setAttribute(Object key, Object value) {
        mAttribute.put(key, value);
    }

    public void removeAttribute(Object key) {
        mAttribute.remove(key);
    }

    public boolean isEditShow() {
        return mIsShareEditPageShow;
    }

    public void setEditShow(boolean isEditShow) {
        this.mIsShareEditPageShow = isEditShow;
    }

    public boolean getIsScreenOff() {
        return this.isScreenOff;
    }

    public void setIsScreenOff(boolean isScreenOff) {
        this.isScreenOff = isScreenOff;
    }

    public void setShareUrl(String channelId, String shareUrl) {
        if (channelId != null) {
            mShareUrl.put(channelId.toLowerCase(), shareUrl);
        }
    }

    public String getShareUrl(String channelId) {
        return mShareUrl.get(channelId);
    }

    public Map<Activity, Boolean> getActivityState() {
        return mActivityState;
    }

    public void removeActivityState(Activity activity) {
        mActivityState.remove(activity);
    }

    public void setActivityState(Activity activity, Boolean isVisible) {
        mActivityState.put(activity, isVisible);
    }

    /**
     * 判断应用程序是否位于栈顶
     * 1、优先用自己记录这种方式来判断
     * 2、其次用进程包名判断（因为有些手机系统（如小米4）上不准）
     */
    public boolean isTopApplication() {
        // 视频自己的activity
        for (Map.Entry<Activity, Boolean> entry : mActivityState.entrySet()) {
            if (entry.getValue()) {
                return true;
            }
        }
        // 非视频的activity，如新闻SDK详情页
        return PackageUtils.isTopApplication(IfengApplication.getInstance());
    }

    public void killAllActivity() {
        for (Map.Entry<Activity, Boolean> entry : mActivityState.entrySet()) {
            if (entry.getValue()) {
                entry.getKey().finish();
            }
        }
    }

    /**
     * 从后台启动时调用此方法
     */
    public void enterApp() {
    }

    /**
     * 从前台变为后台运行时调用
     */
    public void exitApp() {
    }

    public void addTopActivity(Activity activity) {
        mActivityQueue.add(activity);
    }

    public void removeTopActivity(Activity activity) {
        if (mActivityQueue.contains(activity)) {
            mActivityQueue.remove(activity);
        }
    }

    public Activity getTopActivity() {
        int size = mActivityQueue.size();
        if (size > 0) {
            return mActivityQueue.get(size - 1);
        }
        return null;
    }

    public long getWellChosenTimestamp() {
        return mWellChosenTimestamp;
    }

    public void setWellChosenTimestamp(long mWellChosenTimestamp) {
        this.mWellChosenTimestamp = mWellChosenTimestamp;
    }

    public void finishActivityPlayingAudio() {
        for (Activity activity : mActivityQueue) {
            if ((activity instanceof AudioService.CanPlayAudio) &&
                    ((AudioService.CanPlayAudio) activity).isPlayAudio() &&
                    !activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        try {
            ImageLoaderManager.getInstance().trimMemory(this, level);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            ImageLoaderManager.getInstance().cleanMemory(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
