package com.ifeng.newvideo.bean;

import android.view.View;
import com.ifeng.newvideo.R;

/**
 * Created by guolc on 2016/8/5.
 */
public abstract class OnInnerItemClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        onItemInnerClick(v, (int) v.getTag(R.id.tag_key_click));
    }

    public abstract void onItemInnerClick(View view, int positon);
}