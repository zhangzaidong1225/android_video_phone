package com.ifeng.newvideo.bean;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;
import com.ifeng.newvideo.ui.maintab.adapter.ChannelDragView;

/**
 * Created by guolc on 2016/8/4.
 */

//为 频道管理也定制的view,引用时布局结构勿改动
public class CustomScrollView extends ScrollView{

    public ChannelDragView getmChannelDragView_top() {
        return mChannelDragView_top;
    }

    public void setmChannelDragView_top(ChannelDragView mChannelDragView_top) {
        this.mChannelDragView_top = mChannelDragView_top;
    }

    public ChannelDragView getmChannelDragView_bottop() {
        return mChannelDragView_bottop;
    }

    public void setmChannelDragView_bottop(ChannelDragView mChannelDragView_bottop) {
        this.mChannelDragView_bottop = mChannelDragView_bottop;
    }

    private ChannelDragView mChannelDragView_top;
    private ChannelDragView mChannelDragView_bottop;


    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(mChannelDragView_bottop!=null&&mChannelDragView_top!=null){
            if(mChannelDragView_bottop.isDragImageExist()||mChannelDragView_top.isDragImageExist())
                return false;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }
}
