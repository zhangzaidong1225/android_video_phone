package com.ifeng.newvideo.appstore;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.Service;
import android.content.*;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;
import com.ifeng.newvideo.constants.IntentKey;
import com.ifeng.newvideo.ui.ad.ADActivity;
import com.ifeng.video.dao.db.dao.AdvertExposureDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


/**
 * 应用商店app下载service
 * Created by fengfan on 2015/9/22,10:51.
 */
public class StoreDownLoadService extends Service {

    private static final Logger logger = LoggerFactory.getLogger(StoreDownLoadService.class);

    private static DownloadManager mDownloadManager;

    private static String mAdId;
    private static ArrayList<String> mDownloadCompletedUrls;
    private static ArrayList<String> mAsyncdownloadUrls;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            startDownLoadApp(this, intent.getStringExtra(IntentKey.STORE_APP_URL), intent.getStringExtra(IntentKey.STORE_APP_NAME));
            mAdId = intent.getStringExtra(ADActivity.KEY_AD_ID);
            mDownloadCompletedUrls = intent.getStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_COMPLETED_URL);

            mAsyncdownloadUrls = intent.getStringArrayListExtra(ADActivity.KEY_AD_DOWNLOAD_START_URL);
            AdvertExposureDao.sendAdvertClickReq(mAdId, mAsyncdownloadUrls);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 下载应用
     */
    @TargetApi(11)
    public static void startDownLoadApp(Context context, String downloadUrl, final String apkName) {
        logger.debug("AppStore---startDownLoadApp--> downloadUrl={}, downloadName={}", downloadUrl, apkName);
        if (TextUtils.isEmpty(downloadUrl) || TextUtils.isEmpty(apkName)) {
            return;
        }
        try {
            mDownloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(downloadUrl);
            DownloadManager.Request downloadRequest = new DownloadManager.Request(uri);
            downloadRequest.setTitle(apkName);
            downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            //设置下载路径
            downloadRequest.setDestinationInExternalPublicDir("/download/", apkName);
            //执行下载
            final long downloadId = mDownloadManager.enqueue(downloadRequest);
            logger.debug("AppStore---startDownLoadApp--> downloadId={}", downloadId);
            registerDownloadCompleteReceiver(context, downloadId);

        } catch (Exception e) {
            logger.error("AppStore--startDownLoadApp error---{}", e);
        }
    }

    @TargetApi(9)
    private static void registerDownloadCompleteReceiver(Context context, long downloadId) {
        BroadcastReceiver downloadSuccessReceiver = new DownloadSuccessReceiver(downloadId);
        IntentFilter loadFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        context.registerReceiver(downloadSuccessReceiver, loadFilter);
    }

    /**
     * 下载成功广播接收
     */
    @TargetApi(11)
    static class DownloadSuccessReceiver extends BroadcastReceiver {
        long downloadId;

        DownloadSuccessReceiver(long downloadId) {
            this.downloadId = downloadId;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }
            try {
                long installId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (installId == downloadId) { //如果下载ID和安装Id相同时，进行安装，否则会多次安装同一个apk
                    Uri installUri = mDownloadManager.getUriForDownloadedFile(installId);
                    logger.debug("AppStore--startInstallAPK ->installUri is {} ", installUri);
                    if (installUri == null) {
                        return;
                    }
                    // uri不为空时，上报下载完成统计
                    AdvertExposureDao.sendAdvertClickReq(mAdId, mDownloadCompletedUrls);

                    Intent installIntent = new Intent(Intent.ACTION_VIEW);
                    installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    installIntent.setDataAndType(installUri, "application/vnd.android.package-archive");
                    context.startActivity(installIntent);
                }
            } catch (Exception exception) {
                logger.error("DownloadSuccessReceiver  error-- {}", exception);
            }
        }
    }
}
