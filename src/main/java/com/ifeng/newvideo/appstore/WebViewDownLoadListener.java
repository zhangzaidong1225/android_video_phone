package com.ifeng.newvideo.appstore;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.webkit.DownloadListener;
import com.ifeng.newvideo.utils.DownLoadUtils;
import com.ifeng.video.core.utils.AlertUtils;
import com.ifeng.video.core.utils.NetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * webView下载监听
 */
public class WebViewDownLoadListener implements DownloadListener {

    private static final Logger logger = LoggerFactory.getLogger(WebViewDownLoadListener.class);

    private Context mContext;
    private String mAdId;
    private ArrayList<String> mDownloadCompletedUrls;
    private ArrayList<String> mAsyncDownloadUrls;

    private Dialog mDialog = null;

    public WebViewDownLoadListener(Context context) {
        mContext = context;
    }

    public void setAdId(String adId) {
        this.mAdId = adId;
    }

    public void setDownloadCompletedUrls(ArrayList<String> downloadCompletedUrls) {
        this.mDownloadCompletedUrls = downloadCompletedUrls;
    }

    public void setAsyncDownloadUrls(ArrayList<String> asyncDownloadUrls) {
        this.mAsyncDownloadUrls = asyncDownloadUrls;
    }

    @Override
    public void onDownloadStart(final String downloadUrl, String userAgent, String contentDisposition, String mimeType, long contentLength) {
        if (NetUtils.isMobile(mContext)) {
            mDialog = AlertUtils.getInstance().showTwoBtnDialog(mContext, "您将在运营商网络下下载，是否继续？",
                    "取消", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                        }
                    },
                    "确定", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                            DownLoadUtils.download(mContext, mAdId, downloadUrl, mAsyncDownloadUrls, mDownloadCompletedUrls);
                        }
                    });

        } else if (NetUtils.isWifi(mContext)) {
            DownLoadUtils.download(mContext, mAdId, downloadUrl, mAsyncDownloadUrls, mDownloadCompletedUrls);
        }
    }

}