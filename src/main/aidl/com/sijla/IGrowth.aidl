// IGrowth.aidl
package com.sijla;

// Declare any non-default types here with import statements

interface IGrowth {
    String growth(String str);
    void s(String str);
    void n(String str);
}
